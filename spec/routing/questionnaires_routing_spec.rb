require "spec_helper"

describe QuetionnairesController do
  describe "routing" do

    it "routes to #index" do
      get("/quetionnaires").should route_to("quetionnaires#index")
    end

    it "routes to #new" do
      get("/quetionnaires/new").should route_to("quetionnaires#new")
    end

    it "routes to #show" do
      get("/quetionnaires/1").should route_to("quetionnaires#show", :id => "1")
    end

    it "routes to #edit" do
      get("/quetionnaires/1/edit").should route_to("quetionnaires#edit", :id => "1")
    end

    it "routes to #create" do
      post("/quetionnaires").should route_to("quetionnaires#create")
    end

    it "routes to #update" do
      put("/quetionnaires/1").should route_to("quetionnaires#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/quetionnaires/1").should route_to("quetionnaires#destroy", :id => "1")
    end

  end
end
