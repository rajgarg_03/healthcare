require "spec_helper"

describe RelevancesController do
  describe "routing" do

    it "routes to #index" do
      get("/relevances").should route_to("relevances#index")
    end

    it "routes to #new" do
      get("/relevances/new").should route_to("relevances#new")
    end

    it "routes to #show" do
      get("/relevances/1").should route_to("relevances#show", :id => "1")
    end

    it "routes to #edit" do
      get("/relevances/1/edit").should route_to("relevances#edit", :id => "1")
    end

    it "routes to #create" do
      post("/relevances").should route_to("relevances#create")
    end

    it "routes to #update" do
      put("/relevances/1").should route_to("relevances#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/relevances/1").should route_to("relevances#destroy", :id => "1")
    end

  end
end
