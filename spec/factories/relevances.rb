# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :relevance do
    child_gender "MyString"
    age_group "MyString"
    resident_countries "MyString"
  end
end
