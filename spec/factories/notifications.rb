# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :notification do
    link "#"
    user nil
    title "MyText"
    checked false
  end
end
