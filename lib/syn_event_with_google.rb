class SynEventWithGoogle < Struct.new(:user_id,:event)
  def perform
    user = User.find(user_id)
    member = user.member
    gcal = GoogleCal.where(user_id:user.id).first
    if event.present?
      gcal.create_google_event(event)
    else
      member_ids = member.childern_ids
      ids =  member_ids << user.member.id
      Event.all.where({'member_id' => { "$in" => ids} }).each do |event|
        gcal.create_google_event(event)
      end
      vaccinations = Vaccination.all.where({'member_id' => { "$in" => member_ids} }).where(opted:"true").includes(:jabs)
      vaccinations.each do |vaccin|
         @jab_ids = vaccin.jabs.map(&:id)
         
        vaccin.jabs.where(status: "Planned").not_in(:due_on => [nil]).each do |jab|
          gcal.create_google_event(jab,"jab","create",{name:vaccin.get_name, index: (@jab_ids.index(jab.id)+1).to_s})
        end
      end


    end
  
  end
end