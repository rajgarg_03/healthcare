require 'singleton'
class AwsSts
  include Singleton
  def initialize
     create_session
  end

  def create_session
    sts = AWS::STS.new(:access_key_id =>APP_CONFIG["s3_access_key_id"],
                   :secret_access_key => APP_CONFIG["s3_secret_access_key"])
    session = sts.new_session(:duration => GlobalSettings::StsSessionExpireDuration)
    @expire_at = session.expires_at
    s3 = AWS::S3.new(session.credentials)
    bucket = s3.buckets[APP_CONFIG['s3_bucket']]
    @sts_bucket = bucket
  end

  def get_object(s3_object_path,options={})
    if @expire_at < (Time.now.utc + 14.minute)
      create_session
    else
      get_sts_bucket.objects[s3_object_path]
    end
  end

  def get_sts_bucket
    @sts_bucket
  end

end