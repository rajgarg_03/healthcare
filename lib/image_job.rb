class ImageJob < Struct.new(:image_id, :class_str)
  def perform
    puts "Processing images to upload to s3 "
    image = class_str.constantize.find image_id
    image.upload_to_s3
    image.local_image.destroy
  end
end
