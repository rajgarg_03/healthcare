class Date

  #03-02-2019
  def to_date1
    self.strftime("%d-%m-%Y")
  end

  #Sun, 03 Feb 2019
  def to_api_date1
    # self.strftime("%d-%b-%Y")
    self.to_date
  end

  #03-Feb-2019
  def to_date2
    self.strftime("%d-%b-%Y")
  end

  #03 February 2019
  def to_date3
    self.strftime("%d %B %Y")
  end

  # 03 Feb 2019
  def to_date4
    self.strftime("%d %b %Y")
  end
  
  

  #Feb 2019
  def to_date5
    self.strftime("%b %Y")
  end

  #2019-03-22
  def to_date6
    self.strftime("%Y-%m-%d")
  end
  
  #03 Feb 19
  def to_date7
    self.strftime("%d %b %y")
  end

  #Convert date to time with given value of h and m in format eg. "11:22"
  def to_time1(h_m_str="")
    begin
    st_hour,st_min = h_m_str.present? ? h_m_str.split(":") : [00,00]
    self.to_time.change(hour:st_hour,min:st_min)
    # end_time =  est_time.change(hour:et_hour, min:et_min)
    rescue
      self.to_time 
    end
  end

end

class Time
  def to_api_date1
    # self.strftime("%d-%b-%Y")
    self.to_time
  end
  def to_api_date2
    self.strftime("%d %B %Y")
  end

  def to_api_date3
    self.strftime("%d %b %y")
  end

  def convert_to_ago
    ApplicationController.helpers.time_ago_in_words(self).split(",")[0].split("and")[0] + " ago"   rescue Date.today.strftime("%d %b %Y")
  end
  
  def to_millisecond
    (self.to_f.round(3)*1000).to_i
  end

  def convert_to_ago2
    distance_in_minutes = (((Time.now - self).abs)/60).round
    case distance_in_minutes
      when 0..1439         then 'today'
      when 1440..2529      then 'yesterday'
      when 2530..43199     then (((distance_in_minutes.to_f / 1440.0).round).to_s + ' days ago')
      when 43200..86399    then '1 month ago'
      when 86400..525599   then (((distance_in_minutes.to_f / 43200.0).round).to_s + ' months ago')
      when 525600..1051199 then '1 year ago'
      else
        distance_in_years           = distance_in_minutes / 525600
        distance_in_years.to_s + ' years ago'
    end
  end

end


class DateTime
  def to_api_date1
    # self.strftime("%d-%b-%Y")
    self.to_datetime
  end

  def to_api_date2
    self.strftime("%d %B %Y")
  end
end

class Object
  def deep_copy
    Marshal.load(Marshal.dump(self))
  end
end



class String
  def milli_second_string_to_time
    Time.at(self.to_f/1000)
  end
  def milli_second_string_to_date
    Time.at(self.to_f/1000).to_date
  end

  def longest_substring(str)
    return '' if [self, str].any?(&:empty?)
    
    matrix = Array.new(self.length) { Array.new(str.length) { 0 } }
    intersection_length = 0
    intersection_end    = 0
    self.length.times do |x|
      str.length.times do |y|
        next unless self[x] == str[y]
        matrix[x][y] = 1 + (([x, y].all?(&:zero?)) ? 0 : matrix[x-1][y-1])

        next unless matrix[x][y] > intersection_length
        intersection_length = matrix[x][y]
        intersection_end    = x
      end
    end
    intersection_start = intersection_end - intersection_length + 1

    slice(intersection_start..intersection_end)
  end
end


class Float
  def round_to(x=nil)
     round(x.to_i)
  end

  def round1
    self.to_d.truncate(1).to_f
  end

  def round2
    self.to_d.truncate(2).to_f
  end

  def round_to1(x=nil)
    if x.present?
      ("%.#{x}f" %self)
    else
      self.to_s
    end
  end

end


class Hash
  def delete_blank
    delete_if{|k, v| v.blank? or v.instance_of?(Hash) && v.delete_blank.empty?}
  end
end