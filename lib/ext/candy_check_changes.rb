module CandyCheck
  module AppStore
    class Verification
      def call!
        verify!
        if valid?
          Receipt.new(@response['receipt'].merge({"pending_renewal_info"=>@response['pending_renewal_info']}))
        else
          VerificationFailure.fetch(@response['status'])
        end
      end
    end
  end
end