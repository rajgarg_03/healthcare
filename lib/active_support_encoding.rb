class ActiveSupportEncoding
  class << self
    def escape(string)
      begin
        if string.respond_to?(:force_encoding)
          string = string.encode(::Encoding::UTF_8, :undef => :replace).force_encoding(::Encoding::BINARY)
        end
        json = string.gsub(/\\u([0-9a-z]{4})/) { |s| [$1.to_i(16)].pack("U") }
        json.force_encoding(::Encoding::UTF_8) if json.respond_to?(:force_encoding)
        json
      rescue Exception=>e 
        string
      end
    end
  end
end
