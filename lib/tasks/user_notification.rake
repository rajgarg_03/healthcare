#rake create_system_greeting 
  #rake create_system_greeting RAILS_ENV=production 
  task :create_system_greeting => :environment do
    q1 = Roo::Excelx.new("./lib/Greetings.xlsx")
    q1.default_sheet = q1.sheets[0]
    SysGreeting.delete_all
    q1.entries[1..-1].each do |entry|
      name = entry[4].gsub('\\\\','\\') rescue ""
      # if entry[17] == "Changed" || entry[17] == "changed"
        # SysGreeting.where(custome_id:entry[0].to_i).delete
        SysGreeting.create(
          name:name,
          custome_id: entry[0].to_i,
          description:name,
          category:entry[2],
          criteria: entry[6]
        )
        puts "#{entry[3]} and name is #{name}"
      # end
    end
  end
  
  # Only use for action for home,
  # TODO: Remove for home action 
  #rake create_user_notification["uk","pointer"]
  #rake create_user_notification["uk","pointer/action_for_home"] RAILS_ENV=production 
  task :create_user_notification,[:country_code,:notification_type] => :environment do |t,arg|
    identifier = arg[:notification_type] 
    @notification = BatchNotification::BatchNotificationManager.where(:identifier=>/^#{identifier}/i,status:"Active").last
    # check pointer added in batchnotification with active status
    if @notification.present? && @notification.is_valid_notification?
      notify_by_push  = UserNotification.notify_by_push?(@notification.notify_by)
      if arg[:country_code].present? && arg[:country_code] != "all"
        code = [arg[:country_code], arg[:country_code].downcase]
        code << ["gb","GB"] if arg[:country_code].downcase == "uk"
        code << ["INDIA","india"] if arg[:country_code].downcase == "in"
        users = User.in(country_code:code.flatten)
      elsif  arg[:country_code] == "all" #&& (arg[:notification_type] == "pointer" || arg[:notification_type] == "pointers")
        if notify_by_push
          # Assign pointer only mobile user if only push is to be sent
          member_ids = Device.not_in(token:nil).pluck(:member_id)
          members = ParentMember.in(:id=>member_ids)
        else
          members = ParentMember.all
        end
      else 
        #other countries
        users = User.not_in(:country_code=>/#{GlobalSettings::CountryListForPushNotification}/i).no_timeout
      end
      if arg[:country_code] != "all"
        users = users.in(member_id:member_ids) if notify_by_push
        members = Member.in(id:users.pluck(:member_id)) 
      end

      members.batch_size(100).no_timeout.each do |current_user|
        batch_notification_manager = @notification
        break if !batch_notification_manager.valid_total_allowed_count?(current_user)
        # Check user not deleted
        if current_user.user_id.present?
          if arg[:notification_type] == "pointer" || arg[:notification_type] == "pointers"
            pointer_limit = GlobalSettings::MaxAllowedPushCountTypeWise["pointers"]
            UserNotification.add_pointer_notification(current_user,@notification,pointer_limit)
          end
          if arg[:notification_type] == "action_for_home"
            UserNotification.add_home_action(current_user,@notification)
          end
        end
      end
    else
      # Delete all assigned pending pointer notification to stop push notification
      type = @notification.identifier
      delete_count = UserNotification.where(:type=>type).where(:status.in=>["pending",nil],:email_status.in=>["pending",nil]).delete_all
      Rails.logger.info "Total pending notification deleted on #{Date.today} is ...... #{delete_count}"
    end
  end
  
  #Not in use. sending push by architecture flow 
  #rake push_user_notifications["uk","pointer",1] 
  #rake push_user_notifications["uk","pointer/action_for_home",1] RAILS_ENV=production 
  task :push_user_notifications,[:country_code,:notification_type,:count] => :environment do |t,arg|
    # puts "selected user for country#{arg[:country_code]}"
     # members = Device.all.map(&:member).uniq
     member_ids = Device.not_in(token:nil).pluck(:member_id)
     if arg[:country_code].present?
      code = [arg[:country_code], arg[:country_code].downcase]
      code << ["gb","GB"] if arg[:country_code].downcase == "uk"
      code << ["INDIA","india"] if arg[:country_code].downcase == "in"
       members = User.in(member_id:member_ids).in(country_code:code.flatten).uniq.map(&:member)
     
     else
       members = User.in(member_id:member_ids).not_in(country_code:GlobalSettings::CountryListForPushNotification).uniq.map(&:member)
     end
     pusher = UserNotification.notification_pusher
     members.each do |member|
      begin
        user = member.user
        notification_setting = MemberSetting.where(member_id:member.id, name:"subscribe_notification").last.status rescue true
        if user["app_login_status"] == true && notification_setting.to_s == "true"
          devices = member.devices
          if arg[:notification_type] == "pointer"
            pointer_notifications = UserNotification.where(user_id:user.id,status:"pending",type:"pointers").limit(arg[:count].to_i).order_by("score DESC")
            pointer_notifications.each do |notification|
              begin
                status  = "pending"
                notification_obj = UserNotification.push_notification(notification,member,pusher,devices) 
                status  = "sent" if notification_obj.present?
              rescue Exception=> e 
                @error = e.message
                status =  "pending"              
              end
              notification.status = status
              notification.push_error_message = @error
              notification.save
            end
          end
          if arg[:notification_type] == "action_for_home"
            home_action_notifications = UserNotification.where(user_id:user.id,status:"pending",type:"action_for_home").limit(arg[:count].to_i).order_by("score DESC")
            home_action_notifications.each do |notification|
              status  = "pending"
              begin 
                notification_obj = UserNotification.push_notification(notification,member,pusher,devices)
                status = "sent" if notification_obj.present?
              rescue Exception=>e 
                status =  "pending" 
                @error = e.message             
              end
              notification.status = status
              notification.push_error_message = @error
              notification.save
            end
          end
          
        end
      rescue Exception=>e
        Rails.logger.info e.message
        puts e.message
      end
    end
  end


 
