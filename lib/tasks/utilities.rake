#encoding: utf-8
namespace :utilities do
    
  task :upload_images_to_s3 => :environment do
    service = AWS::S3.new(:access_key_id => APP_CONFIG['s3_access_key_id'],:secret_access_key => APP_CONFIG['s3_secret_access_key'])
    bucket_name = APP_CONFIG['s3_bucket']
    #if(service.buckets.include?(bucket_name))
      bucket = service.buckets[bucket_name]
    # else
    #   bucket = service.buckets.create(bucket_name)
    # end
    # bucket.acl = :public_read
    dirpath = "/home/aman/personal/rails_projects/milestones/latest"
    Dir.foreach(dirpath) do |fname|
      unless fname == '.' || fname == '..'
        key = "default/milestones/" + fname
        image_location = dirpath + '/' + fname
        s3_file = service.buckets[bucket_name].objects[key].write(:file => image_location)
        s3_file.acl = :public_read
        puts s3_file.public_url.to_s
      end
    end
  end

  task :upload_small_images_to_s3 => :environment do
    service = AWS::S3.new(:access_key_id => APP_CONFIG['s3_access_key_id'],:secret_access_key => APP_CONFIG['s3_secret_access_key'])
    bucket_name = APP_CONFIG['s3_bucket']
    bucket = service.buckets[bucket_name]
    dirpath = "/home/aman/personal/rails_projects/milestones/latest_small"
    Dir.foreach(dirpath) do |fname|
      unless fname == '.' || fname == '..'
        key = "default/milestones/small/" + fname
        image_location = dirpath + '/' + fname
        s3_file = service.buckets[bucket_name].objects[key].write(:file => image_location)
        s3_file.acl = :public_read
        puts s3_file.public_url.to_s
      end
    end
  end

  task update_system_milestone_title: :environment do
    sys_milestone = SystemMilestone.where(title: "iImmitates mannerisms and activities").first
    if sys_milestone
      sys_milestone.update_attribute(:title,"Imitates mannerisms and activities") 
      puts "system milestone title updated to #{sys_milestone.title}"
    end    
    sys_milestone = SystemMilestone.where(title: "Point finger at thing they want").first
    if sys_milestone
      sys_milestone.update_attribute(:title,"Points finger at a thing they want")
      puts "system milestone title updated to #{sys_milestone.title}"
    end
    sys_milestone = SystemMilestone.where(title: "Says words like “I,” “me,” “we,” and “you”").first
    if sys_milestone
      sys_milestone.update_attribute(:title, "Says words like 'I', 'me', 'we', and 'you'")
      puts "system milestone title updated to #{sys_milestone.title}"
    end  
    sys_milestone = SystemMilestone.where(title: "Responds to other people’s emotions").first
    sys_milestone.update_attribute(:title, "Responds to other people's emotions") if sys_milestone

    sys_milestone = SystemMilestone.where(title: "Strings vowels together (“ah,” “eh,” “oh”)").first
    sys_milestone.update_attribute(:title, "Strings vowels together 'ah','eh','oh'") if sys_milestone

    sys_milestone = SystemMilestone.where(title: "Understands the idea of “same” and “different”").first
    sys_milestone.update_attribute(:title, "Understands the idea of 'same' and 'different'") if sys_milestone

    sys_milestone = SystemMilestone.where(title: "Says and shakes head “no”").first
    sys_milestone.update_attribute(:title, "Says and shakes head 'no'") if sys_milestone

    puts "done"
  end

  task :update_default_system_milestone_image_status => :environment do
    SystemMilestone.all.each do |system_milestone|
      unless system_milestone.default_image_exists?
        begin
          # uri = URI("#{APP_CONFIG["s3_base_url"]}/default/milestones/#{system_milestone.title.gsub('/','_').gsub(',','').gsub(' ','%20').gsub(/'/,'')}.jpg")
          uri = URI("#{APP_CONFIG["s3_base_url"]}/default/milestones/#{system_milestone.title.gsub('/','_').gsub(',','').gsub(' ','%20').gsub(/'/,'')}.jpg")
          request = Net::HTTP.new uri.host
          response= request.request_head uri.path
          if response.code.to_i == 200
            puts uri
            system_milestone.update_attribute(:default_image_exists, true)
          else
            puts "not present, title: #{system_milestone.title}, category-#{system_milestone.category}"
          end
        rescue
          puts "rescued #{system_milestone.title}"
        end
      end  
    end      
  end

  task :update_default_system_timeline_image_status => :environment do
    SystemTimeline.all.each do |system_timeline|
      begin
        uri = URI("#{APP_CONFIG["s3_base_url"]}/default/timelines/small/#{system_timeline.name.downcase.gsub(' ','_')}.jpg")
        request = Net::HTTP.new uri.host
        response= request.request_head uri.path
        if response.code.to_i == 200
          puts uri
          system_timeline.update_attribute(:default_image_exists, true)
        else
          puts "not present"
        end
      rescue
        puts "rescued #{system_timeline.name}"
      end
    end      
  end  

  task rename_files: :environment do
    puts "Renaming files..."

    folder_path = "/home/aman/personal/rails_projects/milestones/latest_small"
    Dir.glob(folder_path + "/*").sort.each do |f|
        filename = File.basename(f, File.extname(f))
        # File.rename(f, filename.split('_').join(' ').capitalize + File.extname(f))
        File.rename(f, folder_path + "/" + filename.split('_').join(' ').capitalize + File.extname(f))
        puts "Renamed to #{filename}"
    end
  end  
  
  task :upload_documents_to_s3 => :environment do
    service = AWS::S3.new(:access_key_id => APP_CONFIG['s3_access_key_id'],:secret_access_key => APP_CONFIG['s3_secret_access_key'])
    bucket_name = APP_CONFIG['s3_bucket']
    bucket = service.buckets[bucket_name]
    # dirpath = "/home/aman/personal/rails_projects/milestones_small/small"
    Document.all.each do |document|
      begin
        if document.file.present?
          if File.exists?(document.file.path)
            file_location = document.file.path
            key = "documents/#{document.id}/original/#{document.file_file_name}"
            key = (key + '.') if file_location.last == '.'
            s3_file = service.buckets[bucket_name].objects[key].write(:file => file_location)
            s3_file.acl = :public_read
            puts s3_file.public_url.to_s
          else
            puts "File does not exist"  
          end
        end
      rescue
        puts "rescued document with id #{document.id}"
      end  
    end
  end

  task :create_system_timelines => :environment do
    system_timeline_values = [{:name=>"First month birthday", :duration=>"1.months", :default_image_exists=>true}, {:name=>"Second month birthday", :duration=>"2.months", :default_image_exists=>true}, {:name=>"Third month birthday", :duration=>"3.months", :default_image_exists=>true}, {:name=>"Fourth month birthday", :duration=>"4.months", :default_image_exists=>true}, {:name=>"Fifth month birthday", :duration=>"5.months", :default_image_exists=>true}, {:name=>"Sixth month birthday", :duration=>"6.months", :default_image_exists=>true}, {:name=>"Seventh month birthday", :duration=>"7.months", :default_image_exists=>true}, {:name=>"Eighth month birthday", :duration=>"8.months", :default_image_exists=>true}, {:name=>"Ninth month birthday", :duration=>"9.months", :default_image_exists=>true}, {:name=>"Tenth month birthday", :duration=>"10.months", :default_image_exists=>true}, {:name=>"Eleventh month birthday", :duration=>"11.months", :default_image_exists=>true}, {:name=>"First birthday", :duration=>"1.years", :default_image_exists=>true}, {:name=>"Second birthday", :duration=>"2.years", :default_image_exists=>true}, {:name=>"Third birthday", :duration=>"3.years", :default_image_exists=>true}, {:name=>"Fourth birthday", :duration=>"4.years", :default_image_exists=>true}, {:name=>"Fifth birthday", :duration=>"5.years", :default_image_exists=>true}, {:name=>"Sixth birthday", :duration=>"6.years", :default_image_exists=>nil}, {:name=>"Seventh birthday", :duration=>"7.years", :default_image_exists=>nil}, {:name=>"Eighth birthday", :duration=>"8.years", :default_image_exists=>nil}, {:name=>"Ninth birthday", :duration=>"9.years", :default_image_exists=>nil}, {:name=>"Tenth birthday", :duration=>"10.years", :default_image_exists=>nil}, {:name=>"Birth", :duration=>"0.months", :default_image_exists=>nil}]
    system_timeline_values.each do |st|
      system_timeline = SystemTimeline.where(name: st[:name], duration: st[:duration]).first_or_initialize(name: st[:name], duration: st[:duration], default_image_exists: st[:default_image_exists])
      if system_timeline.new_record?
        system_timeline.save
        puts "Created system timeline #{system_timeline.name}"
      else
        puts "Already exists system timeline #{system_timeline.name}"
      end
    end
  end

  task :update_system_timeline_sequence => :environment do
    SystemTimeline.all.sort_by{|st| eval(st.duration)}.each_with_index do |system_timeline,idx|
      system_timeline.sequence = idx + 1
      system_timeline.save!
      puts "SystemTimeline #{system_timeline.name} sequence updated to #{system_timeline.sequence}"
    end
  end  

  task :update_system_milestone_sequence => :environment do
    categories = {"0-2" =>1, "2-4" => 2, "4-6" => 3, "7-9" => 4, "9-12" => 5, "13-18" => 6, "19-24" => 7, "24-36" => 8, "37-48" => 9, "49-60" => 10}
    categories.each do |category,sequence|
      system_milestones = SystemMilestone.where(category: category).update_all(sequence: sequence)
      puts "SystemMilestone sequence updated to #{sequence}"
    end    
  end  
    
end    