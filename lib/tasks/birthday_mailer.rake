 # usage: rake nurturey:send_birthday_email RAILS_ENV=production
namespace :nurturey do
  task :send_birthday_email => :environment do
    childerns = ChildMember.all
    childerns.batch_size(100).each do |child|
      if Pregnancy.where(expected_member_id:child.id.to_s).pluck("id").blank?
        if (child.birth_date.present? && child.birth_date.month == Date.today.month && child.birth_date.day == Date.today.day )
          age = (Date.today.year - child.birth_date.year).to_i #rescue 0
          child.family_elder_members.each do |parent_id|
            parent_member = Member.where(id:parent_id).first
            parent = User.where(id:parent_member.user_id).or({:status => 'confirmed'},{:status => 'approved'}).first rescue nil
            if parent && parent.subscribed_to_mailer?
              begin
                send_mail(parent,child,age) #if parent.present?
                # puts("Sent birthday email to #{parent.email}")
                Rails.logger.warn("Sent Birthday email to #{parent.email}")
              rescue
                Rails.logger.warn("Not Sent Birthday email to #{parent.inspect}")
              end
            end
          end
        end
      end
    end
  end

  def send_mail(parent,child,child_age={})
    if child_age == 1
     UserMailer.first_birthday_email(parent,child,child_age).deliver!
    elsif child_age == 2
     UserMailer.second_birthday_email(parent,child,child_age).deliver!
    elsif child_age == 3
      UserMailer.third_birthday_email(parent,child,child_age).deliver!
    elsif child_age == 4
     UserMailer.fourth_birthday_email(parent,child,child_age).deliver!
    elsif child_age >= 5
      UserMailer.generic_birthday_email(parent,child,child_age).deliver!
    else
      UserMailer.generic_birthday_email(parent,child,child_age).deliver!
    end
     
   puts("Sent birthday email to #{parent.email}")
  end
end
