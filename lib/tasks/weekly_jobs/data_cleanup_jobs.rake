#Delete skiphandhold action not update since 3 month
task :clean_handhold_skip_details => :environment do 
  SkipHandHoldAction.where(:skipped_up_to.lte=> (Date.today-90.days)).delete_all 
end

# Update_activity_status for user and Family Inactive/defunct
# rake update_activity_status["6.months","12.months","12.month","32.month"] RAILS_ENV=production
task :update_activity_status,[:inactive_start_duration,:inactive_end_duration, :defunct_start_duration,:defunct_end_duration] => :environment do |t,arg|
  # set user last_sign_at if nil
  User.where(:last_sign_in_at=>nil).no_timeout.batch_size(200).each do |user|
    user.update_attribute(:last_sign_in_at,user.created_at) rescue nil
  end

  # Not sigin in last 6 month to 1 year
  Utility.mark_inactive_users(arg[:inactive_start_duration],arg[:inactive_end_duration],"Inactive")
  Utility.mark_inactive_families("Inactive")

  # Not sigin in last 12 month and before
  Utility.mark_inactive_users(arg[:defunct_start_duration],arg[:defunct_end_duration],"Defunct")
  Utility.mark_inactive_families("Defunct")

end

# Mark user email domain valid/invalid to reduce bounce email rate
# rake mark_user_email_domain RAILS_ENV=production
task :mark_user_email_domain=> :environment do
  System::BounceEmail.mark_user_having_fake_domain
end

# Delete user email domain having invalid status to reduce bounce email rate
# rake delete_user_with_email_domain_invalid RAILS_ENV=production
task :delete_user_with_email_domain_invalid=> :environment do
  users = User.where(email_domain_status:"invalid")
  users.batch_size(100).each do |user|
    user.delete_account(nil) rescue nil
  end
end


# Delete user with email domain mailinator to reduce bounce email rate
# rake delete_user_with_email_domain_mailinator RAILS_ENV=production
task :delete_user_with_email_domain_mailinator => :environment do 
  users = User.where(email:/@mailinator/i)
  users.batch_size(100).each do |user|
    user.delete_account(nil) rescue nil
  end
end

