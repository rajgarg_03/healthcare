# Get Syndicated content from NHS at regular interval
task :get_nhs_syndicated_data => :environment do 
	options = {}
	::Nhs::SyndicatedContent.where(status:'Active').each do |syndicated_content|
    begin
      ::Nhs::NhsApi.get_nhs_syndicated_data(syndicated_content.api_link,syndicated_content.custom_id,options )
    rescue Exception => e
    	Rails.logger.info 'Error inside get_nhs_syndicated_data'
    	Rails.logger.info e.message
    end
	end
end