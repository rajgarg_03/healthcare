  # Report::ApiLog.delete_family_duplicate_event1(event_id:event_id,event_id1:event_id1,family_id:family_id)
  # task :delete_family_duplicate_events,[:event_ids] =>  :environment do |t,arg|
  #   begin
  #     source_database = "api_logs"
  #     event_ids = arg[:event_ids]
  #     destination_database = APP_CONFIG["database_name"]
  #     Mongoid.override_database(source_database)
  #     event_records = ::Report::ApiLog.where(:event_custom_id.in=>event_ids).where(:family_id=>family_id).only(:id,:event_custom_id,:created_at).entries
  #     duplicate_ids = []
  #     custom_event_id = event_records.first.event_custom_id
  #     event_records.each_with_index do |record,index|
  #       if event_records[index+1].present?
  #         if custom_event_id == event_records[index+1].event_custom_id
  #           duplicate_ids << event_records[index+1].id
  #         else
  #           custom_event_id = event_records[index+1].event_custom_id
  #         end
  #       end
  #     end
  #     if delete_record
  #       ::Report::ApiLog.where(:id.in=>duplicate_ids).delete_all
  #     end
  #     Mongoid.override_database(destination_database)
  #   rescue Exception => e
  #     Mongoid.override_database(destination_database)
  #   end
  # end


    # Report::ApiLog.delete_family_duplicate_event1(event_id:event_id,event_id1:event_id1,family_id:family_id)
    task :delete_user_duplicate_events,[:event_ids,:user_country_code] => :environment do |t,arg|
      begin
        event_ids = [arg[:event_ids]].flatten
        user_country_code = arg[:user_country_code] || "uk"
        event_custom_id = event_ids.first
        marked_user_ids_in_this_week = ::Report::ApiLog.get_log_event(event_custom_id,nil,nil,{:user_country_code=>user_country_code, :start_date=>Date.today - 7.day,:end_date=>Date.today,:selected_field=>"user_id"}).map(&:user_id).uniq.reverse
        delete_record = true
        source_database = "api_logs"
        destination_database = APP_CONFIG["database_name"]
        Mongoid.override_database(source_database)
        marked_user_ids_in_this_week.each do |user_id|
           begin
            event_records = ::Report::ApiLog.where(:created_at.gte=>Date.today - 6.month).where(:event_custom_id.in=>event_ids).where(:user_id=>user_id).only(:id,:event_custom_id,:created_at).entries
             duplicate_ids = []
             custom_event_id = event_records.first.event_custom_id
             event_records.each_with_index do |record,index|
                   if event_records[index+1].present?
                     if custom_event_id == event_records[index+1].event_custom_id
                         duplicate_ids << event_records[index+1].id
                       else
                           custom_event_id = event_records[index+1].event_custom_id
                       end
                   end
               end
             if delete_record
                 ::Report::ApiLog.where(:id.in=>duplicate_ids).delete_all
               end
           rescue Exception=> e
             Rails.logger.info "error #{e.message} for #{user_id}"
           end
       end
         Mongoid.override_database(destination_database)
       rescue Exception => e
         Rails.logger.info  e.message
         Mongoid.override_database(destination_database)
       end
    end
   

      # Report::ApiLog.delete_family_duplicate_event1(event_id:event_id,event_id1:event_id1,family_id:family_id)
    task :delete_family_duplicate_events,[:event_ids,:user_country_code] =>:environment do |t,arg|
      begin
        event_ids = [arg[:event_ids]].flatten
        user_country_code = arg[:user_country_code] || "uk"
        event_custom_id = event_ids.first
        marked_family_ids_in_this_week = ::Report::ApiLog.get_log_event(event_custom_id,nil,nil,{:user_country_code=>user_country_code, :start_date=>Date.today - 1.month,:end_date=>Date.today,:selected_field=>"family_id"}).map(&:family_id).uniq
        delete_record = true
        source_database = "api_logs"
        destination_database = APP_CONFIG["database_name"]
        Mongoid.override_database(source_database)
        marked_family_ids_in_this_week.each do |family_id|
           begin
            event_records = ::Report::ApiLog.where(:created_at.gte=>Date.today - 6.month).where(:event_custom_id.in=>event_ids).where(:family_id=>family_id).only(:id,:event_custom_id,:created_at).entries
             duplicate_ids = []
             custom_event_id = event_records.first.event_custom_id
             event_records.each_with_index do |record,index|
                   if event_records[index+1].present?
                     if custom_event_id == event_records[index+1].event_custom_id
                         duplicate_ids << event_records[index+1].id
                       else
                           custom_event_id = event_records[index+1].event_custom_id
                       end
                   end
               end
             if delete_record
                 ::Report::ApiLog.where(:id.in=>duplicate_ids).delete_all
               end
           rescue Exception=> e
             Rails.logger.info "error #{e.message} for #{family_id}"
           end
       end
         Mongoid.override_database(destination_database)
       rescue Exception => e
         Rails.logger.info  e.message
         Mongoid.override_database(destination_database)
       end
    end

  # rake update_activity_status_v2_for_completed_prganancy RAILS_ENV=production
  task :update_activity_status_v2_for_completed_prganancy =>:environment do 
    Pregnancy.where(status:"completed").each do |pregnancy|
      begin
        options = {event_source:"batch_job_cleanup"}  
        options[:update_activity_status] = false
        member = Member.where(id:pregnancy.expected_member_id).last
        if member.blank?
          last_activity_done_on = pregnancy.updated_at
        else
          last_activity_done_on = pregnancy.last_activity_done_on || member.last_activity_done_on || pregnancy.updated_at
        end
        # Update last_activity_done_on status
        pregnancy.update_attributes(:last_activity_done_on=>last_activity_done_on)
        # check if last_activity_done_on is less than 30 day and should be mark inactive
        if last_activity_done_on < Date.today - 30.day
          pregnancy.update_attributes(:activity_status_v2=>"Inactive")
          if member.present?
            member.update_attribute(:activity_status_v2,"Inactive") if member.activity_status_v2 != "Inactive"
            
            family = FamilyMember.where(member_id:member.id).last.family
            #check if family should mark inactive
            
            family_child_member_ids = FamilyMember.where(family_id:family.id).in(role:FamilyMember::CHILD_ROLES).pluck("member_id")
            
            active_member = ChildMember.where(:id.in=>family_child_member_ids,:activity_status_v2=>"Active").limit(1).count
            
            activity_status = active_member > 0 ? "Active" : "Inactive"
            
            latest_activity_done_on = ChildMember.where(:id.in=>family_child_member_ids).pluck(:last_activity_done_on).compact.sort.last
            
            options[:created_at] = latest_activity_done_on + 1.month
            # Update family activity status v2
            if activity_status != family.activity_status_v2
              family.update_attributes(:last_activity_done_on=> latest_activity_done_on) 
              family.mark_family_activity_status_v2(activity_status,options) rescue nil
              # Update all family elder member update
              User.where(:member_id.in=>family.family_elder_member_ids ).each do |user|
                if user.last_sign_in_at.present?  && user.last_sign_in_at >= (Date.today - 6.months)
                  activity_status = user.has_active_family? ? "Active" : "Inactive"
                else
                  activity_status = "Inactive"
                end
                if activity_status != user.activity_status_v2 
                  user.mark_user_activity_status_v2(activity_status,options)
                end
              end
            end

          end
        end
      rescue Exception=>e 
        Rails.logger.info "#{pregnancy.id}............. #{e.message}"
      end
    end
  end