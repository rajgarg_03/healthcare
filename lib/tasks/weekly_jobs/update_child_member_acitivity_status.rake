# rake update_child_member_activity_status RAILS_ENV=production

task :update_child_member_activity_status =>:environment do
  # Mark ChildMember for Group age
  # Update Member Activtity_status and last activity_status for new added
  ChildMember.where(:created_at.gte=>Date.today - 30.day,:activity_status_v2=>nil).update_all(activity_status_v2:"Active")
  Utility.update_child_member_activity_v2("Inactive","0-360","31+")# 0-360 age in between 0 - (360/30 = 12 month) activity in days
  Utility.update_child_member_activity_v2("Inactive","361-1080","61+")
  Utility.update_child_member_activity_v2("Inactive","1081-1800","91+")
  Utility.update_child_member_activity_v2("Inactive","1801+","181+")
  pregnancy_week = nil
  Pregnancy.where(status:"completed").or({last_activity_done_on:nil},{:last_activity_done_on.lte=>Date.today - 2.month}).update_all(activity_status_v2:"Inactive")
  completed_pregnancy_ids = Pregnancy.where(status:"completed").or({last_activity_done_on:nil},{:last_activity_done_on.lte=>Date.today - 2.month}).only(:expected_member_id).pluck(:expected_member_id)
  ChildMember.where(:id.in=>completed_pregnancy_ids).update_all(activity_status_v2:"Inactive")
  Utility.update_pregnancy_activity_status_v2("Inactive",pregnancy_week,"31+") #last ativity done in more than 30 days before
  Utility.update_family_activity_status_v2
  acitivty_duration_to_check_in_month = 6 # 6 month
  Utility.update_user_activity_status_v2(acitivty_duration_to_check_in_month)
end