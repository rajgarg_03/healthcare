  #rake create_system_article_ref 
  #rake create_system_article_ref RAILS_ENV=production 
  task :create_system_article_ref => :environment do
    q1 = Roo::Excelx.new("./lib/system_references.xlsx")
    q1.default_sheet = q1.sheets[0]
    # SysArticleRef.delete_all
    q1.entries[1..-1].each do |entry|
      name = entry[5].gsub('\\\\','\\') rescue ""
      if entry[17] == "Changed" || entry[17] == "changed"
        SysArticleRef.where(custome_id:entry[0].to_i).delete
        SysArticleRef.create(
          name:name,
          custome_id: entry[0].to_i,
          status: entry[1],
          description:entry[8],
          category:entry[3],
          criteria: entry[7],
          score:entry[9], 
          ref_start_date:entry[11],
          ref_peak_rel_date:entry[13],
          ref_expire_date:entry[15],
          N_factor: entry[16], 
          valid_for_role: entry[18],
          stage: entry[2] ,
          scenarios: entry[4] ,
          condition: entry[6],
          topic: entry[8],
          ds: entry[10],
          dp: entry[12],
          dexp: entry[14]
        )
        puts "#{entry[3]} and name is #{name}"
      end
    end
  end



  #rake create_system_article_ref_with_sheet_column 
  #rake create_system_article_ref_with_sheet_column RAILS_ENV=production 
  task :create_system_article_ref_with_sheet_column => :environment do
    q1 = Roo::Excelx.new("./lib/system_references.xlsx")
    q1.default_sheet = q1.sheets[0]
     SysArticleRef.delete_all
    q1.entries[1..-1].each do |entry|
      name = entry[5].gsub('\\\\','\\') rescue ""
       
        SysArticleRef.where(custome_id:entry[0].to_i).delete
        SysArticleRef.create(
          name:name,
          custome_id: entry[0].to_i,
          status: entry[1],
          description:entry[8],
          category:entry[3],
          criteria: entry[7],
          score:entry[9], 
          ref_start_date:entry[11],
          ref_peak_rel_date:entry[13],
          ref_expire_date:entry[15],
          N_factor: entry[16], 
          valid_for_role: entry[18],
          stage: entry[2] ,
          scenarios: entry[4] ,
          condition: entry[6],
          topic: entry[8],
          ds: entry[10],
          dp: entry[12],
          dexp: entry[14]
        )
        puts "#{entry[3]} and name is #{name}"
       
    end
  end


  #rake create_system_article_ref_link
  #rake create_system_article_ref_link RAILS_ENV=production 
  task :create_system_article_ref_link => :environment do
    q1 = Roo::Excelx.new("./lib/system_references.xlsx")
    sheets = q1.sheets
     SysArticleRefLink.delete_all
    (1..4).each do |index| 
        q1.sheet(index).entries[1..-1].each do |entry|
          SysArticleRefLink.create(
            custome_id:entry[0],
            link_order: entry[1].to_i,
            name:entry[2],
            source:entry[3],
            title: entry[4],
            type:entry[5], 
            image_url:entry[6],
            country: sheets[index].downcase
          )
          puts "#{entry[0]} and name is #{entry[4]}"
          puts sheets[index].downcase
      end
    end
  end

  #rake update_assigned_referenceslinks_to_member[cutom_id,country]
  #Eg. rake update_assigned_referenceslinks_to_member[173,"uk"]
  #rake update_assigned_referenceslinks_to_member[173,"uk"] RAILS_ENV=production 
  task :update_assigned_referenceslinks_to_member,[:custom_id,:country] => :environment do |t,arg|
    if arg[:custom_id].present? && arg[:country].present?
      ArticleRef.update_links(arg[:custom_id].to_i,arg[:country])
    else
      puts "Invalid custom_id or country: #{arg.inspect}"
    end
  end
  
  # Not in use. Assign pointer through Assign_scenario job 
  #rake assign_references_to_member
  #rake assign_references_to_member RAILS_ENV=production 
  task :assign_references_to_member,[:skip_type] => :environment do |t,arg|
    ChildMember.where(:last_activity_done_on.gte=>Date.today - 12.month).batch_size(500).no_timeout.each do |member|
      ArticleRef.assign_article_ref(member.id,{:member_obj=>member}) 
      ArticleRef.where(member_id:member.id, :updated_at.lt=>Date.today,:status=>"Active", :shortlisted=> false).delete_all
    end
    ArticleRef.where(:status=>"deleted", :ref_expire_date.lt=> Date.today).delete_all

    # save 30 day assigned data for reporting
    Report::PointerAssignedData.save_assigned_data rescue nil
  end


  #rake delete all assign references to member
  #rake delete_assign_references_to_member 
  #rake delete_assign_references_to_member RAILS_ENV=production 
  task :delete_assign_references_to_member => :environment do
    ArticleRef.delete_all 
  end

  

# name: "A new born's vision takes longer to develop than all other senses", 
# description: "Read about how you can help develop <child name>'s vision",
# score: "member.birh_date+ 1.month ==Date.today", 
# N_factor: nil, 
# ref_start_date: nil,
# ref_peak_rel_date: nil,
# ref_expire_date: nil, 
# start_day_rel: nil,
# peak_day_rel: nil,
# remark: nil, 
# category: nil, 
# link_url: nil