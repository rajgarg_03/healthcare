# Update column name in System Jab and System Vaccine
# rake rename_column_name_forsystem_jab
# rake rename_column_name_forsystem_jab RAILS_ENV=production
  task :rename_column_name_forsystem_jab => :environment do
    SystemJab.all.rename(:manufacturer, :trade_name)
    # SystemJab.all.rename(:jab_location, :site)
    # SystemJab.all.rename(:jab_type, :jab_route)
    # Vaccination.all.rename(:category, :route)
    SystemVaccin::RouteType
   end

# Update system vaccine vaccin_type data
# rake update_data_in_vaccin_type_for_system_jab
# rake update_data_in_vaccin_type_for_system_jab RAILS_ENV=production
  task :update_data_in_vaccin_type_for_system_jab => :environment do
    SystemVaccin.where(:vaccin_type=>"Recommended").update_all(:vaccin_type=>"Routine")
  end

# Update Flu vaccination jab
# rake update_flu_vaccine_jabs
# rake update_flu_vaccine_jabs RAILS_ENV=production
  task :update_flu_vaccine_jabs => :environment do
    Vaccination.update_flu_vaccine_jabs
  end
  
  # API-613-remove-2nd-jab-of-hib-menc-for-a (UK users)
  # rake delete_jab_from_vaccine
  # rake delete_jab_from_vaccine RAILS_ENV=production
  task :delete_jab_from_vaccine => :environment do
    ChildMember.where(country_code:nil).map(&:get_country_code) rescue nil
    country_code = Member.sanitize_country_code("uk")
    ChildMember.where(:country_code.in=>country_code).batch_size(500).no_timeout.each do |member|
      begin
        vaccine = Vaccination.where(name:"Hib/MenC",member_id:member.id).last
        if vaccine.present?
          vaccine.delete_system_estimated_jab_from_vaccine(2)
        end
      rescue Exception=>e 
        Rails.logger.info "Error inside delete_jab_from_vaccine ......   #{e.message}"
      end
    end
  end
