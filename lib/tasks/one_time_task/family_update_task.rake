  #rake update_family_segment
  #rake update_family_segment RAILS_ENV=production 
  task :update_family_segment=> :environment do 
    Family.batch_size(20).each do |family|
      begin
        family.update_family_segment
      rescue Exception => e 
        UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,{},"Error inside update_family_segment rake job")
      end
    end
  end


  #rake update_family_trial_days
  #rake update_family_trial_days RAILS_ENV=production 
  task :update_family_trial_days=> :environment do 
    Family.batch_size(20).each do |family|
      begin
        family.update_family_trial_days
      rescue Exception => e 
        UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,{},"Error inside update_family_trial_days rake job")
      end
    end
  end

  #rake update_family_pricing_stage
  #rake update_family_pricing_stage RAILS_ENV=production 
  task :update_family_pricing_stage=> :environment do 
    Family.batch_size(20).each do |family|
      begin
        family.update_family_pricing_stage
      rescue Exception => e 
        UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,{},"Error inside update_family_trial_days rake job")
      end
    end
  end


  #rake update_family_pricing_exemption
  #rake update_family_pricing_exemption RAILS_ENV=production 
  task :update_family_pricing_exemption=> :environment do 
    Family.batch_size(20).each do |family|
      begin
        family.update_family_pricing_exemption
      rescue Exception => e 
        UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,{},"Error inside update_family_trial_days rake job")
      end
    end
  end


  #rake update_child_member_activity_status_v2
  #rake update_child_member_activity_status_v2 RAILS_ENV=production 
  task :update_child_member_activity_status_v2=> :environment do 
    activity_status = "Active"
    ChildMember.where(:activity_status_v2.ne=>"Active").batch_size(20).each do |member|
      begin
        event = Report::ApiLog.get_member_log_event(member.id,nil,{:limit=>1}).first
        if event.present? || member.created_at < 30.days # new added record
          member.update_attributes({activity_status_v2:activity_status ,:last_activity_done_on=>event.created_at})
        end
      rescue Exception => e 
        Rails.logger.info "Error inside update_child_member_activity_status_v2  #{e.message}"
        UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,{},"Error inside update_child_member_activity_status rake job")
      end
    end
  end

  #rake update_family_country_code RAILS_ENV=production 
  task :update_family_country_code=> :environment do 
    Family.all.batch_size(20).each do |family|
      begin
        owner_country_code = family.owner.get_country_code
        family.update_attribute(:country_code,owner_country_code)
      rescue Exception => e 
        Rails.logger.info "Error inside update_family_country_code  #{e.message}"
        UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,{},"Error inside update_family_country_code rake job")
      end
    end
  end
  

