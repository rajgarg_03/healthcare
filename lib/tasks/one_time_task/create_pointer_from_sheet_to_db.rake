#rake create_pointer_from_sheet_to_db 
  #rake create_pointer_from_sheet_to_db RAILS_ENV=production 
  task :create_pointer_from_sheet_to_db => :environment do
    q1 = Roo::Excelx.new("./lib/system_pointers_lot_7.xlsx")
    q1.default_sheet = q1.sheets[0]
    pregnancy = Pregnancy.last
    member = Member.last
    milestone = Milestone.last
    health = Health.last
    vaccination = Vaccination.last
    system_vaccin = SystemVaccin.find(vaccination.sys_vaccin_id)
    jab = Jab.last
     
    q1.entries[1..-1].each do |entry|
      name = entry[6].gsub('\\\\','\\') rescue ""
        puts entry[18]
        SysArticleRef.where(custome_id:entry[0].to_i).delete
        if ( (entry[1].downcase == "ready" rescue false)) 
          begin
            sys_pointer = SysArticleRef.new(
              name:name,
              custome_id: entry[0].to_i,
              status: entry[2],
              stage: entry[3] ,
              category:entry[4],
              scenarios: entry[5] ,
              condition: (entry[7].blank? ? "All":entry[7]) ,
              criteria: entry[8],
              topic: entry[9],
              description:entry[9],
              score:entry[10], 
              ds: entry[11],
              ref_start_date:entry[12],
              dp: entry[13],
              ref_peak_rel_date:entry[14],
              dexp: entry[15],
              ref_expire_date:entry[16],
              N_factor: entry[17], 
              valid_for_role: nil
            )
            sys_pointer.save!
             begin
              pointer_name = sys_pointer.name.gsub("sleep","sileep")
              name =  eval(pointer_name)
                
            rescue Exception=>e
              name = sys_pointer.name
            end
            begin
              pointer_description = sys_pointer.description.gsub("sleep","sileep")
              description =  eval(entry[9])
              name.gsub!("sileep","sleep") rescue nil
            rescue Exception=>e
              description =  (sys_pointer.description)
            end
             if entry[19].present?
               
              link = SysArticleRefLink.new(
                custome_id:entry[0],
                link_order: 1,
                name:"",
                source:entry[19],
                title: "",
                type:"Article", 
                image_url:"",
                status: "Draft",
                country: "other".downcase
              )
              link.save!
            end

            puts "pointer name:#{name}  - , Pointer description: #{description}"
          rescue Exception=>e 
            puts "#{entry[0]} has an error while saving- #{e.message}"
          end
        else
          puts "#{entry[0]} not added. not in ready state"
        end
    end
  end

  #rake update_vaccin_scenario_for_pointer 
  #rake update_vaccin_scenario_for_pointer RAILS_ENV=production 
  task :update_vaccin_scenario_for_pointer => :environment do
    System::Scenario::ScenarioDetails.update_scenario_condition_for_vaccination_category
  end

 