  # rake update_activity_status_v2_with_saved_activity_status
  # rake update_activity_status_v2_with_saved_activity_status RAILS_ENV=production 
  task :update_activity_status_v2_with_saved_activity_status => :environment do 
    Member.all.no_timeout.batch_size(100).each do |member|
      begin
        activity_status = member.activity_status
        member.update_attribute(:activity_status_v2, activity_status)
      rescue Exception => e 
        Rails.logger.info e.message
      end
    end
    Family.all.no_timeout.batch_size(100).each do |family|
      begin 
        activity_status = family.activity_status
        family.update_attribute(:activity_status_v2, activity_status)
      rescue Exception => e 
        Rails.logger.info e.message
      end
    end
    User.all.no_timeout.batch_size(100).each do |user|
      begin
        activity_status = user.activity_status
        user.update_attribute(:activity_status_v2, activity_status)
      rescue Exception => e 
        Rails.logger.info e.message
      end
    end

    FamilyMember.all.no_timeout.batch_size(100).each do |family_member|
      begin
        activity_status = family_member.activity_status
        family_member.update_attribute(:activity_status_v2, activity_status)
      rescue Exception => e 
        Rails.logger.info e.message
      end
    end

  end