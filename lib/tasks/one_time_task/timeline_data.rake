#encoding: utf-8
namespace :nurturey do
  # rake nurturey:create_system_preg_timeline RAILS_ENV=production
  task :create_system_preg_timeline => :environment do
    SystemTimeline.all.each do |timeline|
      puts "saved" if !timeline.has_attribute?(:category) && timeline.update_attributes(category: 'child')
    end
    q1 = Roo::Excelx.new("./public/preg_timeline_entries.xlsx")
    q1.default_sheet = q1.sheets[0]
    index = 1
    q1.entries.each do |entry|
      SystemTimeline.create(name:entry[0],duration:entry[1],comment:entry[2], category:"pregnancy",sequence: index)
      index = index + 1
      puts "#{entry[0]} and name is #{entry[1]}"
    end
  end

  # rake nurturey:create_system_born_timeline
  task :create_system_born_timeline => :environment do
    SystemTimeline.all.each do |timeline|
      puts "saved" if !timeline.has_attribute?(:category) && timeline.update_attributes(category: 'child')
    end
    q1 = Roo::Excelx.new("./public/Timeline_entries.xlsx")
    q1.default_sheet = q1.sheets[0]
    index = 1
    q1.entries.each do |entry|
      SystemTimeline.create(name:entry[0],duration:entry[1],comment:entry[2], category:"child",sequence: index)
      index = index + 1
      puts "#{entry[0]} and name is #{entry[1]}"
    end
  end
end

#rake update_system_timeline_category RAILS_ENV=production
task :update_system_timeline_category => :environment do 
  SystemTimeline.where(category:"born").update_all(category:"child")
  SystemTimeline.where(category:"preg").update_all(category:"pregnancy")
end