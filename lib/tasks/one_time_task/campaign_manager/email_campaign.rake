  #rake send_email_to_user_not_loggedin_since_n_days["in","12-2-2016"]
  #rake send_email_to_user_not_loggedin_since_n_days["in","24-5-2017"] RAILS_ENV=production 

  
  task :send_email_to_user_not_loggedin_since_n_days,[:country_code,:date_data] => :environment do |t,arg|
    uninstall_callback_campaign_logger = Logger.new('log/uninstall_callback_campaign.log')
    uninstall_callback_campaign_logger.info "send_email_to_user_not_loggedin_since_n_days run on #{Date.today} for country- #{arg[:country_code]}"
    user_country_code = arg[:country_code].downcase
    job_started_at = Time.now
    country_code =  get_county_code(arg)
    uninstall_callback_campaign_mail_sent_to_users = CampaignManager::UninstallCallbackCampaign.where(mail_sent_status:true).in(user_country_code:country_code).pluck(:member_id).map(&:to_s)
    users = User.in(:status=>["approved","confirmed"],country_code:country_code).not_in(member_id:uninstall_callback_campaign_mail_sent_to_users).where(:current_sign_in_at.lte=>(arg[:date_data].to_date))
    total_mail_to_sent_counter = 0
    mail_sent_counter = 0
    mail_not_sent_counter = 0
    if users.present?
      users.batch_size(50).each do |user|
        begin
          if user.member_id.blank?
            member_id = Member.where(user_id:user.id.to_s).id
            user.member_id = member_id.to_s
            user.save
          end
          if user.subscribed_to_mailer?
            total_mail_to_sent_counter = total_mail_to_sent_counter + 1
            callback_campagin = CampaignManager::UninstallCallbackCampaign.create(last_mail_sent_date:Date.today, member_id:user.member_id,mail_sent_status:true,mail_sent_count:1,user_country_code:user_country_code)
            CampaignMailer.uninstall_callback_campaign(user,callback_campagin).deliver
            mail_sent_counter = mail_sent_counter + 1
          end
        rescue Exception=>e 
          mail_not_sent_counter = mail_not_sent_counter + 1
          uninstall_callback_campaign_logger.info "Mail Not sent to #{user.email} for  #{arg[:country_code]} - error msg is :#{e.message}"
        end
      end
    end
      job_finished_at = Time.now

      Report::BatchJobMailerData.create(mail_not_sent_count:mail_not_sent_counter, total_mail_to_sent:total_mail_to_sent_counter, mail_sent_count:mail_sent_counter, country_code:user_country_code ,argument_list: arg.to_hash, started_at:job_started_at, finished_at:job_finished_at, batch_job_name:"send_email_to_user_not_loggedin_since_n_days")
  end


    def get_county_code(arg)
      code = [arg[:country_code], arg[:country_code].downcase]
      code << ["gb","GB"] if arg[:country_code].downcase == "uk"
      code << ["INDIA","india"] if arg[:country_code].downcase == "in"
      code
    end