  # Set sort_order for topic
  #rake set_sort_order_for_topic
  #rake set_sort_order_for_topic RAILS_ENV=production 
  task :set_sort_order_for_topic => :environment do
    counter = 0
    System::Faq::Topic.all.each do |topic|
      counter = counter + 1
      topic[:sort_order] = counter
      topic.save

    end
  end 

  # Set sort_order for subtopic
  #rake set_sort_order_for_subtopic
  #rake set_sort_order_for_subtopic RAILS_ENV=production 
  task :set_sort_order_for_subtopic => :environment do
    data = System::Faq::SubTopic.all.group_by{ |subtopic| subtopic.topic_id }
    data.each  do |topic_id,subtopics|
      counter = 0
      subtopics.each do |subtopic|
        counter = counter + 1
        subtopic[:sort_order] = counter
        subtopic.save
      end
    end
  end 


  # Set sort_order for Question
  #rake set_sort_order_for_question
  #rake set_sort_order_for_question RAILS_ENV=production 
  task :set_sort_order_for_question => :environment do
    data = System::Faq::Question.all.group_by{ |question| question.sub_topic_id }
    data.each  do |sub_topic_id,questions|
      counter = 0
      questions.each do |question|
        counter = counter + 1
        question[:sort_order] = counter
        question.save
      end
    end
  end 

