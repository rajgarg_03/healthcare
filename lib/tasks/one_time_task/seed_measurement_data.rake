# encoding: utf-8
# WHO data for us metric
namespace :nurturey do
  # rake nurturey:who_data_for_us_metric
  task :who_data_for_us_metric => :environment do
    WhoHcf.not_in(measure_unit: "inches").each do |obj1|
      obj = WhoHcf.new(obj1.attributes)
      obj.P01 = (obj.P01 * Health::UNIT_VAL["inches"]).round(2)
      obj.P1 =  (obj.P1 *  Health::UNIT_VAL["inches"]).round(2)
      obj.P3 =  (obj.P3 *  Health::UNIT_VAL["inches"]).round(2)
      obj.P5 =  (obj.P5 *  Health::UNIT_VAL["inches"]).round(2)
      obj.P10 = (obj.P10 * Health::UNIT_VAL["inches"]).round(2)
      obj.P15 = (obj.P15 * Health::UNIT_VAL["inches"]).round(2)
      obj.P25 = (obj.P25 * Health::UNIT_VAL["inches"]).round(2)
      obj.P50 = (obj.P50 * Health::UNIT_VAL["inches"]).round(2)
      obj.P75 = (obj.P75 * Health::UNIT_VAL["inches"]).round(2)
      obj.P85 = (obj.P85 * Health::UNIT_VAL["inches"]).round(2)
      obj.P90 = (obj.P90 * Health::UNIT_VAL["inches"]).round(2)
      obj.P95 = (obj.P95 * Health::UNIT_VAL["inches"]).round(2)
      obj.P97 = (obj.P97 * Health::UNIT_VAL["inches"]).round(2)
      obj.P99 = (obj.P99 * Health::UNIT_VAL["inches"]).round(2)
      obj.P999= (obj.P999 * Health::UNIT_VAL["inches"]).round(2)
      obj.measure_unit = "inches"
      obj.save
      puts "saved #{obj.id}"
    end

     

    WhoWaist.not_in(measure_unit: "inches").each do |obj1|
      obj = WhoWaist.new(obj1.attributes)

      obj.P01 = (obj.P01 * Health::UNIT_VAL["inches"]).round(2)
      obj.P1 =  (obj.P1 *  Health::UNIT_VAL["inches"]).round(2)
      obj.P3 =  (obj.P3 *  Health::UNIT_VAL["inches"]).round(2)
      obj.P5 =  (obj.P5 *  Health::UNIT_VAL["inches"]).round(2)
      obj.P10 = (obj.P10 * Health::UNIT_VAL["inches"]).round(2)
      obj.P15 = (obj.P15 * Health::UNIT_VAL["inches"]).round(2)
      obj.P25 = (obj.P25 * Health::UNIT_VAL["inches"]).round(2)
      obj.P50 = (obj.P50 * Health::UNIT_VAL["inches"]).round(2)
      obj.P75 = (obj.P75 * Health::UNIT_VAL["inches"]).round(2)
      obj.P85 = (obj.P85 * Health::UNIT_VAL["inches"]).round(2)
      obj.P90 = (obj.P90 * Health::UNIT_VAL["inches"]).round(2)
      obj.P95 = (obj.P95 * Health::UNIT_VAL["inches"]).round(2)
      obj.P97 = (obj.P97 * Health::UNIT_VAL["inches"]).round(2)
      obj.P99 = (obj.P99 * Health::UNIT_VAL["inches"]).round(2)
      obj.P999= (obj.P999 * Health::UNIT_VAL["inches"]).round(2)
      obj.measure_unit = "inches"
      obj.save

    end

    WhoHeight.not_in(measure_unit: "inches").each do |obj1|
      obj = WhoHeight.new(obj1.attributes)

      obj.P01 = (obj.P01 * Health::UNIT_VAL["inches"]).round(2)
      obj.P1 =  (obj.P1 *  Health::UNIT_VAL["inches"]).round(2)
      obj.P3 =  (obj.P3 *  Health::UNIT_VAL["inches"]).round(2)
      obj.P5 =  (obj.P5 *  Health::UNIT_VAL["inches"]).round(2)
      obj.P10 = (obj.P10 * Health::UNIT_VAL["inches"]).round(2)
      obj.P15 = (obj.P15 * Health::UNIT_VAL["inches"]).round(2)
      obj.P25 = (obj.P25 * Health::UNIT_VAL["inches"]).round(2)
      obj.P50 = (obj.P50 * Health::UNIT_VAL["inches"]).round(2)
      obj.P75 = (obj.P75 * Health::UNIT_VAL["inches"]).round(2)
      obj.P85 = (obj.P85 * Health::UNIT_VAL["inches"]).round(2)
      obj.P90 = (obj.P90 * Health::UNIT_VAL["inches"]).round(2)
      obj.P95 = (obj.P95 * Health::UNIT_VAL["inches"]).round(2)
      obj.P97 = (obj.P97 * Health::UNIT_VAL["inches"]).round(2)
      obj.P99 = (obj.P99 * Health::UNIT_VAL["inches"]).round(2)
      obj.P999= (obj.P999 * Health::UNIT_VAL["inches"]).round(2)
      obj.measure_unit = "inches"
      obj.save
    end

    WhoWeight.not_in(measure_unit: "lbs").each do |obj1|
      obj = WhoWeight.new(obj1.attributes)
      obj.P01 = (obj.P01 * Health::UNIT_VAL["lbs"]).round(2)
      obj.P1 =  (obj.P1 *  Health::UNIT_VAL["lbs"]).round(2)
      obj.P3 =  (obj.P3 *  Health::UNIT_VAL["lbs"]).round(2)
      obj.P5 =  (obj.P5 *  Health::UNIT_VAL["lbs"]).round(2)
      obj.P10 = (obj.P10 * Health::UNIT_VAL["lbs"]).round(2)
      obj.P15 = (obj.P15 * Health::UNIT_VAL["lbs"]).round(2)
      obj.P25 = (obj.P25 * Health::UNIT_VAL["lbs"]).round(2)
      obj.P50 = (obj.P50 * Health::UNIT_VAL["lbs"]).round(2)
      obj.P75 = (obj.P75 * Health::UNIT_VAL["lbs"]).round(2)
      obj.P85 = (obj.P85 * Health::UNIT_VAL["lbs"]).round(2)
      obj.P90 = (obj.P90 * Health::UNIT_VAL["lbs"]).round(2)
      obj.P95 = (obj.P95 * Health::UNIT_VAL["lbs"]).round(2)
      obj.P97 = (obj.P97 * Health::UNIT_VAL["lbs"]).round(2)
      obj.P99 = (obj.P99 * Health::UNIT_VAL["lbs"]).round(2)
      obj.P999= (obj.P999 * Health::UNIT_VAL["lbs"]).round(2)
      obj.measure_unit = "lbs"
      obj.save
    end
  end 
end


# rake add_zscore_data_in_who
# rake add_zscore_data_in_who RAILS_ENV=production
task :add_zscore_data_in_who => :environment do
  WhoBmi.seed_bmi_value_for_zscores
  WhoHeight.seed_height_value_for_zscores
  WhoWeight.seed_weight_value_for_zscores
  
  WhoWeight.in(measure_unit: "lbs").each do |record|
    record["weight_for_zscore_minus_3"] =  record["weight_for_zscore_minus_3"] * Health::UNIT_VAL["lbs"]
    record["weight_for_zscore_minus_2"] =  record["weight_for_zscore_minus_2"] * Health::UNIT_VAL["lbs"]
    record["weight_for_zscore_minus_1"] =  record["weight_for_zscore_minus_1"] * Health::UNIT_VAL["lbs"]
    record["weight_for_zscore_0"]       =  record["weight_for_zscore_0"]       * Health::UNIT_VAL["lbs"]
    record["weight_for_zscore_1"]       =  record["weight_for_zscore_1"]       * Health::UNIT_VAL["lbs"]
    record["weight_for_zscore_2"]       =  record["weight_for_zscore_2"]       * Health::UNIT_VAL["lbs"]
    record["weight_for_zscore_3"]       =  record["weight_for_zscore_3"]       * Health::UNIT_VAL["lbs"]
    record.save
  end

  WhoHeight.in(measure_unit: "inches").each do |record|
    record["height_for_zscore_minus_3"] =  record["height_for_zscore_minus_3"] * Health::UNIT_VAL["inches"]
    record["height_for_zscore_minus_2"] =  record["height_for_zscore_minus_2"] * Health::UNIT_VAL["inches"]
    record["height_for_zscore_minus_1"] =  record["height_for_zscore_minus_1"] * Health::UNIT_VAL["inches"]
    record["height_for_zscore_0"]       =  record["height_for_zscore_0"]       * Health::UNIT_VAL["inches"]
    record["height_for_zscore_1"]       =  record["height_for_zscore_1"]       * Health::UNIT_VAL["inches"]
    record["height_for_zscore_2"]       =  record["height_for_zscore_2"]       * Health::UNIT_VAL["inches"]
    record["height_for_zscore_3"]       =  record["height_for_zscore_3"]       * Health::UNIT_VAL["inches"]
    record.save
  end

end


# rake add_who_percentile_data
task :add_who_percentile_data => :environment do 
  WhoWeight.update_file("./whoData/tab_wfa_boys_p_5_10.txt","Boy")
  WhoWeight.update_file("./whoData/tab_wfa_girls_p_5_10.txt","Girl")
  
  WhoHeight.update_file_for_p_5_19("./whoData/tab_ihfa_boys_p_5_19.txt","Boy")
  WhoHeight.update_file_for_p_5_19("./whoData/tab_ihfa_girls_p_5_19.txt","Girl")
  
  WhoBmi.update_file("./whoData/tab_bmi_boys_p_5_19.txt","Boy")
  WhoBmi.update_file("./whoData/tab_bmi_girls_p_5_19.txt","Girl")

  WhoHeight.not_in(measure_unit: "inches").where(:month.gt=>60).each do |obj1|
      obj = WhoHeight.new(obj1.attributes)

      obj.P01 = (obj.P01 * Health::UNIT_VAL["inches"]).round(2)
      obj.P1 =  (obj.P1 *  Health::UNIT_VAL["inches"]).round(2)
      obj.P3 =  (obj.P3 *  Health::UNIT_VAL["inches"]).round(2)
      obj.P5 =  (obj.P5 *  Health::UNIT_VAL["inches"]).round(2)
      obj.P10 = (obj.P10 * Health::UNIT_VAL["inches"]).round(2)
      obj.P15 = (obj.P15 * Health::UNIT_VAL["inches"]).round(2)
      obj.P25 = (obj.P25 * Health::UNIT_VAL["inches"]).round(2)
      obj.P50 = (obj.P50 * Health::UNIT_VAL["inches"]).round(2)
      obj.P75 = (obj.P75 * Health::UNIT_VAL["inches"]).round(2)
      obj.P85 = (obj.P85 * Health::UNIT_VAL["inches"]).round(2)
      obj.P90 = (obj.P90 * Health::UNIT_VAL["inches"]).round(2)
      obj.P95 = (obj.P95 * Health::UNIT_VAL["inches"]).round(2)
      obj.P97 = (obj.P97 * Health::UNIT_VAL["inches"]).round(2)
      obj.P99 = (obj.P99 * Health::UNIT_VAL["inches"]).round(2)
      obj.P999= (obj.P999 * Health::UNIT_VAL["inches"]).round(2)
      obj.measure_unit = "inches"
      obj.save
    end

    WhoWeight.not_in(measure_unit: "lbs").where(:month.gt=>60).each do |obj1|
      obj = WhoWeight.new(obj1.attributes)
      obj.P01 = (obj.P01 * Health::UNIT_VAL["lbs"]).round(2)
      obj.P1 =  (obj.P1 *  Health::UNIT_VAL["lbs"]).round(2)
      obj.P3 =  (obj.P3 *  Health::UNIT_VAL["lbs"]).round(2)
      obj.P5 =  (obj.P5 *  Health::UNIT_VAL["lbs"]).round(2)
      obj.P10 = (obj.P10 * Health::UNIT_VAL["lbs"]).round(2)
      obj.P15 = (obj.P15 * Health::UNIT_VAL["lbs"]).round(2)
      obj.P25 = (obj.P25 * Health::UNIT_VAL["lbs"]).round(2)
      obj.P50 = (obj.P50 * Health::UNIT_VAL["lbs"]).round(2)
      obj.P75 = (obj.P75 * Health::UNIT_VAL["lbs"]).round(2)
      obj.P85 = (obj.P85 * Health::UNIT_VAL["lbs"]).round(2)
      obj.P90 = (obj.P90 * Health::UNIT_VAL["lbs"]).round(2)
      obj.P95 = (obj.P95 * Health::UNIT_VAL["lbs"]).round(2)
      obj.P97 = (obj.P97 * Health::UNIT_VAL["lbs"]).round(2)
      obj.P99 = (obj.P99 * Health::UNIT_VAL["lbs"]).round(2)
      obj.P999= (obj.P999 * Health::UNIT_VAL["lbs"]).round(2)
      obj.measure_unit = "lbs"
      obj.save
    end
   
  end


  # rake update_zcore_for_measurement_records RAILS_ENV=production
  task :update_zcore_for_measurement_records => :environment do  
    ChildMember.all.each do |child|
      measurement_record = child.health_records.order_by("updated_at asc").first
      if measurement_record.present?
        measurement_updated_at = measurement_record.updated_at 
        current_user = nil
        Health.update_zscore_for_impacted_measurement(measurement_updated_at,current_user,child)
      end
    end
  end


