  #rake create_checkup_test_list_data 
  #rake create_checkup_test_list_data RAILS_ENV=production 
  task :create_checkup_test_list_data => :environment do
    q1 = Roo::Excelx.new("./lib/data/development_Checkup_Planner.xlsx")
    sheets = q1.sheets
    System::Child::CheckupPlanner::Test.where(member_id:nil).delete_all
    (0..0).each do |index| 
        entries = q1.sheet(index).entries[0..-1]
        
        entries.each do |entry|

          System::Child::CheckupPlanner::Test.create(
            title:entry[0].strip,
            description: entry[1],
            added_by: "system"
          )
          puts "#{entry[0].strip.humanize} added by system"
      end
    end
  end


  #rake create_checkup_list_data 
  #rake create_checkup_list_data RAILS_ENV=production 
  task :create_checkup_list_data => :environment do
    q1 = Roo::Excelx.new("./lib/data/development_Checkup_Planner.xlsx")
    sheets = q1.sheets
    System::Child::CheckupPlanner::Checkup.delete_all
    System::Child::CheckupPlanner::CheckupTest.delete_all
    ["GB", "IN", "US", "AE", "NZ", "CA", "ZA" "SG", "Other"].each do |country_code|
      (1..1).each do |index| 
        entries = q1.sheet(index).entries[1..-1]
        
        entries.each do |entry|
          title = entry[0].strip
          system_checkup = System::Child::CheckupPlanner::Checkup.where(:country_code=>country_code, :title=>/^#{title}$/i).last
          if system_checkup.blank?
            system_checkup = System::Child::CheckupPlanner::Checkup.create(
              title: title,
              country_code: country_code,
              time_frame: entry[1].gsub(" ",".").downcase,
              time_frame_text: (entry[3] rescue entry[3])
          )
            puts "#{title} added into system schedule"
        end
          component_name = entry[2].strip rescue nil
          puts "#{component_name} is the name"
          component_object =  System::Child::CheckupPlanner::Test.where(:title=>/^#{component_name}$/i, added_by:"system").last
          scheduled_test = System::Child::CheckupPlanner::CheckupTest.create(test_id:component_object.id, checkup_id:system_checkup.id)
          puts "#{scheduled_test.attributes} added by job"
        end
      end
    end
  end

  #rake add_checkups_to_existing_member 
  #rake add_checkups_to_existing_member RAILS_ENV=production 
  task :add_checkups_to_existing_member => :environment do
    ChildMember.where(:birth_date.lte=>Date.today).each do |member|
      if !member.is_expected?
        current_user = User.where(member_id:member.family_mother_father.first.to_s).first.member rescue nil
        api_version = 1
        if current_user.present? &&  ::Child::CheckupPlanner::Checkup.where(member_id:member.id.to_s).blank?
          System::Child::CheckupPlanner::Checkup.add_system_checkup_to_member(current_user, member,api_version) rescue nil
        end
      end
    end
  end

  

