  # update_count_for_basic_tool_in_subscription
  # rake update_count_for_basic_tool_in_subscription RAILS_ENV=production
  task :update_count_for_basic_tool_in_subscription => :environment do
   subscription_manager = SubscriptionManager.free_subscription
   Subscription.where(title: "Free").update_all(max_basic_tool_count: subscription_manager.max_basic_tool_count)
   Subscription.where(title: "Free").update_all(max_premium_tool_count:subscription_manager.max_premium_tool_count)
  end


  task :create_report_for_send_email_for_pending_identifier_18 => :environment do
    UserMailer.send_identifier_18_report("pending").deliver
  end
  
  task :create_report_for_send_email_for_sent_identifier_18 => :environment do
    UserMailer.send_identifier_18_report("sent").deliver
  end

  # send subscription taken email and push date before 2018-06-28
  # rake send_email_for_identifier_17_subscription_before_28_06_2018 RAILS_ENV=qa
  task :send_email_for_identifier_17_subscription_before_28_06_2018 => :environment do
    subscription_manager_ids = SubscriptionManager.premium_subscriptions.map(&:id)
    family_ids = {}
    batch_manager = BatchNotification::BatchNotificationManager.where(identifier:"18").last
    Subscription.where(:start_date.lt=>"2018-06-28".to_date,:subscription_manager_id.in=>subscription_manager_ids,:family_id.nin=>[nil]).each do |subscription|
      begin
        # InstantNotification::Subscription.notification_for_identifier_17("18",current_user,family_id,options)
        current_user =  Member.find(subscription.member_id)
        family_id = subscription.family_id
        family =  Family.find(family_id)
        feed_id = subscription.id
        user_ids = Member.in(id:family.family_elder_member_ids).map(&:user_id).map(&:to_s)
        push_message = "Awesome, #{current_user.first_name} has taken the premium subscription for #{family.name}. You will now be a pro parent with our ever growing list of cutting edge premium tools and features :). Click to see details"
        
        options = {:subscription=>subscription,:family_id=>family_id}
        UserNotification.assign_push_to_all_elder_member(child=nil,push_message,batch_manager,feed_id,(user_ids),content_url=nil,options)
      
      rescue Exception=>e
        Rails.logger.info e.message
      end
    end
     
  end
  

  # send subscription push date after 2018-06-28 and before 2018-07-10 (last deployment)
  # rake send_email_for_identifier_17_subscription_after_28_06_2018 RAILS_ENV=qa
  task :send_email_for_identifier_17_subscription_after_28_06_2018 => :environment do
    subscription_manager_ids = SubscriptionManager.premium_subscriptions.map(&:id)
    family_ids = {}
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification("18")
    
    Subscription.where(:start_date =>{"$gt"=> "2018-06-28".to_date, "$lte"=>"2018-07-10".to_date },:subscription_manager_id.in=>subscription_manager_ids,:family_id.nin=>[nil]).each do |subscription|
      begin
        current_user =  Member.find(subscription.member_id)
        family_id = subscription.family_id
        family =  Family.find(family_id)
        feed_id = subscription.id
        user_ids = Member.in(id:family.family_elder_member_ids).map(&:user_id).map(&:to_s)
        push_message = "Awesome, #{current_user.first_name} has taken the premium subscription for #{family.name}. You will now be a pro parent with our ever growing list of cutting edge premium tools and features :). Click to see details"
        
        options = {:subscription=>subscription,:family_id=>family_id}
        UserNotification.assign_push_to_all_elder_member(child=nil,push_message,batch_notification,feed_id,(user_ids - [current_user.user_id.to_s]),content_url=nil,options)
        # UserNotification.assign_push_to_all_elder_member(child=nil,push_message,batch_notification,feed_id,[current_user.user_id],content_url=nil,options.merge({:trigger_only=>"push"}))
      rescue Execption=>e
        Rails.logger.info e.message
      end
    end
     
  end

