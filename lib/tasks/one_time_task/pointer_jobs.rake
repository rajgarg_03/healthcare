  # Merge all system pointer linking with system scenarion link tool eg faq/nhs
  #rake add_pointers_to_system_scanario_linked_tool RAILS_ENV=production 
  task :add_pointers_to_system_scanario_linked_tool => :environment do
    SysArticleRef.all.each do |system_pointer|
      if system_pointer["system_scenario_detail_custome_id"].present?
        system_pointer.update_scenario(system_pointer["system_scenario_detail_custome_id"])
      end
    end 
  end