  # remove system milestone dynamic data dependnacy from yml file
  #rake add_dynamic_title_into_system_milestone
  #rake add_dynamic_title_into_system_milestone RAILS_ENV=production 
  task :add_dynamic_title_into_system_milestone => :environment do
    data = YAML.load_file("#{Rails.root}/config/system_milestones.yml")
     SystemMilestone.seed_dynamic_title(data)
  end 

