  #rake set_token_for_user
  #rake set_token_for_user RAILS_ENV=production 
  task :set_token_for_user => :environment do
    User.where(:confirmation_token=>nil).each do |user|
        user.confirmation_token || user.send(:generate_confirmation_token)
      user.save
    end
  end 
  #rake set_token_for_user
  #rake set_token_for_user RAILS_ENV=production 
  task :set_fb_user_name => :environment do
    email_ids = User.where(first_name:nil).pluck(:email)
    Member.in(email:email_ids).each do |member|
      begin 
        user = User.where(email:member.email).last
        user.first_name = member.first_name if user.first_name.blank?
        user.last_name = member.last_name if user.last_name.blank?
        user.member_id = member.id if user.member_id.blank?
        user.save
      rescue Exception=>e 
        puts "User with #{member.email} not updated"
      end
    end
  end
    
  #rake set_token_for_user
  #rake set_user_type_of_existing_user_to_General RAILS_ENV=production 
  task :set_user_type_of_existing_user_to_General do 
    User.where(user_type:nil).each do |user|
      user[:user_type]= "General"
      user.save
    end
  end

  #rake update_user_segment
  #rake update_user_segment RAILS_ENV=production 
  task :update_user_segment => :environment do 
    options = {:event_source=>"batch_job"}
    user_tobe_updated_in_trial_segment  = User.where(:segment.ne=>"trial", :sign_in_count.lte=>GlobalSettings::FamilySegment[:trial]).pluck(:id)
    User.where(:sign_in_count.lte=>GlobalSettings::FamilySegment[:trial]).update_all(segment:"trial")
    report_event = Report::Event.where(name:"mark user segment trial").last
    user_tobe_updated_in_trial_segment.each do |user_id|
      ::Report::ApiLog.log_event(user_id,family_id=nil,report_event.custom_id,options) rescue nil
    end
    
    user_tobe_updated_in_starter_segment  = User.where(:segment.ne=>"starter", :sign_in_count.gt=> GlobalSettings::FamilySegment[:trial], :sign_in_count.lte=>GlobalSettings::FamilySegment[:starter]).pluck(:id)
    User.where(:sign_in_count.gt=> GlobalSettings::FamilySegment[:trial], :sign_in_count.lte=>GlobalSettings::FamilySegment[:starter]).update_all(segment:"starter")
    report_event = Report::Event.where(name:"mark user segment starter").last
    user_tobe_updated_in_starter_segment.each do |user_id|
      ::Report::ApiLog.log_event(user_id,family_id=nil,report_event.custom_id,options) rescue nil
    end


    user_tobe_updated_in_adoptor_segment  = User.where(:segment.ne=>"adoptor",:sign_in_count.gt=> GlobalSettings::FamilySegment[:starter], :sign_in_count.lte=>GlobalSettings::FamilySegment[:adoptor]).pluck(:id)
    User.where(:sign_in_count.gt=> GlobalSettings::FamilySegment[:starter], :sign_in_count.lte=>GlobalSettings::FamilySegment[:adoptor]).update_all(segment:"adoptor")
    report_event = Report::Event.where(name:"mark user segment adoptor").last
    user_tobe_updated_in_adoptor_segment.each do |user_id|
      ::Report::ApiLog.log_event(user_id,family_id=nil,report_event.custom_id,options) rescue nil
    end


    user_tobe_updated_in_ambassador_segment  = User.where(:segment.ne=>"ambassador", :sign_in_count.gt=> GlobalSettings::FamilySegment[:adoptor]).pluck(:id)
    User.where(:sign_in_count.gt=> GlobalSettings::FamilySegment[:adoptor]).update_all(segment:"ambassador")
    report_event = Report::Event.where(name:"mark user segment ambassador").last
    user_tobe_updated_in_ambassador_segment.each do |user_id|
      ::Report::ApiLog.log_event(user_id,family_id=nil,report_event.custom_id,options) rescue nil
    end
 
 
  end

  #rake update_user_stage_value
  #rake update_user_stage_value RAILS_ENV=production 
  task :update_user_stage_value => :environment do 
    User.all.batch_size(20).each do |user|
      user.update_user_stage
    end
  end

  #rake update_user_trial_days
  #rake update_user_trial_days RAILS_ENV=production 
  task :update_user_trial_days => :environment do 
    User.update_all(:trial_days=>14)
  end

  #rake update_user_pricing_exemption
  #rake update_user_pricing_exemption RAILS_ENV=production 
  task :update_user_pricing_exemption => :environment do 
    User.update_all(:pricing_exemption=>"basic")
  end

  #rake update_adaptor_to_adoptor_in_family_and user
  #rake update_adaptor_to_adoptor_in_family_and_user RAILS_ENV=production 
  task :update_adaptor_to_adoptor_in_family_and_user => :environment do 
    User.where(:segment=>"adaptor").update_all(:segment=>"adoptor")
    Family.where(:segment=>"adaptor").update_all(:segment=>"adoptor")
  end

  #rake update_allowed_signin_provider_for_user
  #rake update_allowed_signin_provider_for_user RAILS_ENV=production 
  task :update_allowed_signin_provider_for_user => :environment do 
    User.where(:provider.in=>["",nil]).update_all(:allowed_signin_provider=>["Nurturey"])
    User.where(:provider=>/facebook/i).update_all(:allowed_signin_provider=>["Facebook"])
    User.where(:provider=>/nhs/i).update_all(:allowed_signin_provider=>["Nhs"])
    User.where(:provider=>/apple/i).update_all(:allowed_signin_provider=>["Apple"])
  end




  # rake update_member_last_activity_done_on
  # rake update_member_last_activity_done_on RAILS_ENV=production 
  task :update_member_last_activity_done_on => :environment do 
    ChildMember.where(:last_activity_done_on=>nil).order_by("id asc").batch_size(10).no_timeout.each do |member|
      puts member.id
      event = Report::ApiLog.get_member_log_event(member.id,nil,{:limit=>1}).last
      puts "event found" if event.present?
      member.update_attribute(:last_activity_done_on, event.created_at) if event.present?
    end
  end
  
  # rake update_member_pregnancy_status
  # rake update_member_pregnancy_status RAILS_ENV=production 
  task :update_member_pregnancy_status => :environment do 
    ChildMember.where(:id.in=>Pregnancy.all.only(:expected_member_id).pluck(:expected_member_id)).update_all(pregnancy_status:true)
    ChildMember.where(:pregnancy_status.ne=>true).update_all(:pregnancy_status=>false)
  end

