  #rake update_tool_position_type 
  #rake update_tool_position_type RAILS_ENV=production 
  task :update_tool_position_type => :environment do
    #update nutrient position in db
    Nutrient.all.each do |a|
      a["supported_api_version"] = 1
      a["launch_status"] = "launched"
      a["position"] = a["position"].to_i
      a.save
    end

    #Update membernutrient position Db
    MemberNutrient.all.each do |tool|
      tool["poistion"] = tool["position"].to_i
      tool.save
    end

    #Update subscription maintance data
      SubscriptionManager.all.each do |subscription|
        subscription["subscription_status"] = "Draft"
        subscription["supported_api_version"] = 4
        subscription.save
      end
  end

  # rake update_child_pregnancy_tool_description[Timeline,tool description]
  # rake update_child_pregnancy_tool_description[Timeline,tool description] RAILS_ENV=production
  task :update_child_pregnancy_tool_description,[:tool_title,:description] => :environment do |t,arg|
    tool = Nutrient.where(title:arg[:tool_title]).not_in(tool_for:"adult").last
    tool.description = arg[:description]
    tool.save
  end

  #rake update_tool_supported_app_version 
  #rake update_tool_supported_app_version RAILS_ENV=production 
  task :update_tool_supported_app_version => :environment do
    Nutrient.all.each do |a|
      a["ios_supported_app_version"] = 0
      a["android_supported_app_version"] = 0
      a.save
    end
  end

  #rake update_menu_tool_status_soft_launch
  #rake update_menu_tool_status_soft_launch RAILS_ENV=production 
  task :update_menu_tool_status_soft_launch => :environment do
    tool = Nutrient.where(title:"Menu").last
    tool.launch_status = "soft_launch"
    tool.save
  end


  # Rake add_health_card_tool
  # Rake add_health_card_tool RAILS_ENV=production 
  task :add_health_card_tool => :environment do
    Nutrient.new(description: "", title: "Pregnancy Health Card", categories: ["Health", "Mind", "Education"], recommended: "false", position: 4, pregnancy_description: "Pregnancy Health Card", identifier: "Pregnancy Health Card", tool_category: "basic", tool_for: "pregnancy", launch_status: "soft_launch", supported_api_version: 1, is_mandatory: false, android_supported_app_version: 0, ios_supported_app_version: 0).save
    Nutrient.new(description: "Checkup Planner", title: "Checkup Planner", categories: ["Health", "Mind", "Education", "Other", "General", "Psychological", "Education"], recommended: "false", position: 10, pregnancy_description: "", identifier: "Checkup Planner", tool_category: "basic", tool_for: "child", launch_status: "soft_launch", supported_api_version: 1, is_mandatory: false, android_supported_app_version: 0, ios_supported_app_version: 0).save
    Nutrient.new(description: "Health Card for Child", title: "Health Card", categories: ["Health"], recommended: "false", position: 8, pregnancy_description: "", identifier: "Child Health Card", tool_category: "basic", tool_for: "child", launch_status: "soft_launch", supported_api_version: 1, is_mandatory: false, android_supported_app_version: 0, ios_supported_app_version: 0).save 
  end

  # rake add_birthday_wish_push_notification_text
  # rake add_birthday_wish_push_notification_text RAILS_ENV=production 
  task :add_birthday_wish_push_notification_text => :environment do 
    SystemTimeline.seed_notification_text_from_sheet
  end


  # rake add_age_calucation_for_progress_score_in_system_milestone RAILS_ENV=production 
  task :add_age_calucation_for_progress_score_in_system_milestone => :environment do 
    SystemMilestone.save_data_to_calculate_progress_scale_value
  end


  # update_accomplished_status_and_status_by_for_old_milestone RAILS_ENV=production
  task :update_accomplished_status_and_status_by_for_old_milestone => :environment do 
     Milestone.where(status_by:nil).update_all({status_by:"user",milestone_status:"accomplished"})
  end

  # mark_milestone_important RAILS_ENV=production
  task :mark_milestone_important => :environment do
    important_mileston_title_list = ["Begins to smile", "Eyes follow objects", "Responds to sound", "Smoother movements", "Holds head up", "Develops social smile", "Follow moving things side to side", "Hold head up steady", "Can roll over", "Begins to babble", "Pushes down on legs when feet are on a hard surface", "Brings hands to mouth", "Watches faces closely", "Tries to drag objects towards himself/herself", "Responds to other people's emotions", "Responds to own name", "Mouths objects", "Strings vowels together 'ah','eh','oh'", "Rolls in both directions", "Sounds to show joy and displeasure", "Stranger anxiety", "Likes to look at self in a mirror", "Looks curiously at things around", "Pass things one hand to other", "Sits without support", "When standing, \n    supports weight on legs", "Tries to crawl", "Pulls to stand", "Stands while holding on", "Says 'Mama' and 'dada'", "Plays peek-a-boo and pat-a-cake", "Point at objects", "Pick things with thumb and index finger", "Starts cruising", "Try imitate what other's say", "Copies gestures", "Can take few steps without holding", "Jabbers word-like sounds", "Separation anxiety", "Puts out arm or leg to help with dressing", "Has favorite things & people", "Understands simple instructions", "Bangs, drops, and throws objects", "Drinks from a cup", "Put things in container", "Stand alone for few seconds", "Points finger at a thing they want", "Says several single words", "Walks alone", "Can walk up steps", "Likes to hand things to others as play", "Follows simple 2-step Instructions", "Searches for hidden objects", "Begins to run", "Says short sentences", "Imitates mannerisms and activities", "Follows instructions with 2 or 3 steps", "Says words like 'I', 'me', 'we', and 'you'", "Builds towers of more than 6 blocks", "Walks on stairs, one foot on each step", "Uses prepositions (e.g. on, in, over)", "Understands 'taking turns'", "Carries on a conversation using 2-3 sentences", "Can jump", "Shows concern for crying friend", "Tells stories", "Cooperates with other children", "Understands the idea of 'same' and 'different'", "Enjoys doing new things", "Knows some basic rules of grammar", "Sings a song or says a poem", "Play board or card games", "Can say first and last name", "Likes to sing, dance, and act", "Can use the toilet on his/her own", "Speaks very clearly", "Uses future tense", "Understand things used on daily basis", "Tells a simple story using full sentences"] 
    SystemMilestone.in(title:important_mileston_title_list).update_all(:important_status=> true)
    SystemMilestone.not_in(title:important_mileston_title_list).update_all(:important_status=> false)
  end

  # seed_progress_score_formula_value RAILS_ENV=production
   task :seed_progress_score_formula_value => :environment do
    SystemMilestone.save_data_to_calculate_progress_scale_value
  end


  # add new field category in usernotification with destination field value And Update split data
  task :add_category_in_usernotification => :environment do
    all_type_destination = UserNotification.distinct(:destination)
    all_type_destination.each do | destination_name|
      UserNotification.where(destination:destination_name).update_all(category:destination_name)
    end
    # update splitted data
    all_type_destination = PushNotificationSplitedData.distinct(:destination)
    all_type_destination.each do | destination_name|
      PushNotificationSplitedData.where(destination:destination_name).update_all(category:destination_name)
    end
    UserNotification.where(type:"pointer").update_all(type:"pointers")
    PushNotificationSplitedData.where(type:"pointer").update_all(type:"pointers")
  end


  # add manage tools into child member account 
  # rake add_manage_tools_into_member_tool_list RAILS_ENV=production
  task :add_manage_tools_into_member_tool_list => :environment do 
    manage_tool = Nutrient.where(title:"Manage Tools").last
    if manage_tool.present? && manage_tool.launch_status == "launched"
      ChildMember.all.each do |child_member|
        begin
          MemberNutrient.create(status: "active", position:manage_tool.position , nutrient_id:manage_tool.id,member_id:child_member.id)
        rescue Exception => e
          puts e.message
          Rails.logger.info "Error in add_manage_tools_into_member_tool_list"
          Rails.logger.info e.message
        end
      end
    end
  end
