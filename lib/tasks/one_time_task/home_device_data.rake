  # Add user signup with alexa/google home device data into linked device
  #rake add_home_device_signup_data_into_lniked_devices
  #rake add_home_device_signup_data_into_lniked_devices RAILS_ENV=production 
  task :add_home_device_signup_data_into_lniked_devices => :environment do
    User.where(:signup_source.in=>["alexa", "googleassistance"]).each do |user|
      Devices::HomeDevice.add_device(user.signup_source, user.member_id)
    end
    UserLoginDetail.where(:platform.in=>["alexa", "googleassistance"]).each do |login_detail|
      user = User.find login_detail.user_id
      Devices::HomeDevice.add_device(login_detail.platform, user.member_id)
    end
    
  end 

