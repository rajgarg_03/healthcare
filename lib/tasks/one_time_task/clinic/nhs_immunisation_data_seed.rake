  #rake seed_nhs_immunisation_jab_data 
  #rake seed_nhs_immunisation_jab_data RAILS_ENV=production 
  task :seed_nhs_immunisation_jab_data => :environment do
    q1 = Roo::Excelx.new("./lib/data/Vaccinationcodes.xlsx")
    q1.default_sheet = q1.sheets[0]
    ::System::GpsocImmunisationJab.delete_all
    q1.entries[1..-1].each do |entry|
      if entry[1].present?
        read_v2_code = entry[0]
        term = entry[1]
        snomed_code = entry[2]
        snomed_ct_description_id = entry[3]
        snomed_ct_term = entry[4]
        status = entry[5]
        data = {"system_gpsoc_immunisation_jab"=>{snomed_ct_term:snomed_ct_term, snomed_ct_description_id:snomed_ct_description_id, term:term,snomed_code: snomed_code,read_v2_code:read_v2_code,status:status}}
        obj,status = ::System::GpsocImmunisationJab.create_record(data)
      end
        puts "#{obj.inspect}"
    end
  end