  #rake seed_uae_clinic_data 
  #rake seed_uae_clinic_data RAILS_ENV=production 
  task :seed_uae_clinic_data => :environment do
    q1 = Roo::Excelx.new("./lib/data/clinic/uae_SEHA_INFO.xlsx")
    q1.default_sheet = q1.sheets[0]
    service_map = {1=>'Immunisation', 2=>'Health Check ups',3=> 'Well Child',9=>"Drive Through"}
    service_key_index = service_map.keys
    q1.entries[1..-1].each do |entry|
        name = entry[0] rescue ""
        if entry[7].present? && entry[8].present?
          ::Clinic::Seha::Organization.where(longitude:entry[7] ,latitude: entry[8]).delete
        end
        services = []
        service_key_index.each do |i|
          if entry[i].to_s.downcase.strip == "available"
            services << service_map[i]
          end
        end
        obj = ::Clinic::Seha::Organization.new(
          name:name,
          latitude: entry[8],
          longitude:entry[7],
          address: entry[6].to_s.strip,
          email: entry[5].to_s.strip,
          phone_no: entry[4].to_s.strip,
          services: services
        )
        obj.save
        puts "#{obj.inspect}"
    end
  end