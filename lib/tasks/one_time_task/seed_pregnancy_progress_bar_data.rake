  #rake create_pregnancy_progress_bar_data 
  #rake create_pregnancy_progress_bar_data RAILS_ENV=production 
  task :create_pregnancy_progress_bar_data => :environment do
    PregnancyProgressBarManager.populate_sheet_data
  end