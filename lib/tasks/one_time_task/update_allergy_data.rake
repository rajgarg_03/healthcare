# Update allergy Manager data 
#rake add_country_gb_to_allergy_manager RAILS_ENV=production
task :add_country_gb_to_allergy_manager => :environment do
  AllergyManager.each do |allergy_manager|
    allergy_manager["country_code"] = "GB"
    allergy_manager["status"] = "active"
    allergy_manager.save
  end
  AllergySymptomManager.each do |allergy_symptom_manager|
    allergy_symptom_manager["country_code"] = "GB"
    allergy_symptom_manager["status"] = "active"
    allergy_symptom_manager.save
  end
end

# Update user allergy data 
#rake update_user_allergy_data RAILS_ENV=production
task :update_user_allergy_data => :environment do
  Child::HealthCard::Allergy.distinct(:member_id).each do |member_id|
    begin
      member = Member.find(member_id)
      member_country_code = User.where(member_id:member.family_mother_father.first.to_s).first.country_code.upcase rescue "GB"
      member_country_code = AllergyManager.distinct(:country_code).map(&:downcase).include?(member_country_code.downcase) ? member_country_code.upcase : "GB"
      # member_country_code = Member.sanitize_country_code(member_country_code || "other")
      Child::HealthCard::Allergy.where(member_id:member_id.to_s).all.each do |allergy|
        begin
          allergy_manager = AllergyManager.in_country(nil,member_country_code).where(name:allergy.name).last
          allergy.allergy_manager_id = allergy_manager.id
          # update allergy symptom
          symptom_names = []
          custom_symptom_ids = []
          system_added_symptom_names = []
          allergy_symptoms = allergy.allergy_symptoms 
          if allergy_symptoms.present?
            begin
              allergy_symptoms.first.class.to_s == "Hash"
            rescue Exception=> e 
              allergy_symptoms = []
              symptom_names = AllergySymptomManager.in(:id=>allergy.allergy_symptoms).pluck(:name)
            end
          else
            allergy_symptoms = []
          end
          allergy_symptoms.each do |obj|
            # begin
              symptoms_attr = {}
              obj = AllergySymptomManager.find(obj["_id"])
              symptoms_attr = obj.attributes
              if obj["member_id"].present?
                existing_obj = AllergySymptomManager.where(member_id:member_id,:name=>obj.name).last
                if existing_obj.present?
                  custom_symptom_ids << existing_obj.id
                else
                  x = AllergySymptomManager.new(symptoms_attr)
                  x.save
                  symptom_names << x.name

                end
              else
                existing_obj = AllergySymptomManager.in_country(nil,member_country_code).where(:name=>obj.name).last
                if existing_obj.present?
                  custom_symptom_ids << existing_obj.id
                  symptom_names << existing_obj.name
                else
                  symptoms_attr["country_code"] = member_country_code 
                  x = AllergySymptomManager.new(symptoms_attr)
                  # x.save
                  symptom_names << x.name
                end
              end
          end
          symptoms = AllergySymptomManager.in(name:symptom_names).in_country(nil,member_country_code).pluck(:id)
          user_added_symptoms = AllergySymptomManager.in(:id=>custom_symptom_ids).pluck(:id)
          allergy.allergy_symptoms = (symptoms + user_added_symptoms).uniq
          allergy.save
        rescue Exception=> e
          Rails.logger.info "update_user_allergy_data"
          Rails.logger.info e.message 
        end
      end
    rescue Exception=>e
      Rails.logger.info "Error in update_user_allergy_data rake job"
      Rails.logger.info e.message
    end
  end
end
