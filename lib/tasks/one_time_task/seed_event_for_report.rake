# seed event for clinic/gp soc setup and Appoitment Booking
# rake events_for_clinicsetup_and_book_appointment RAILS_ENV=production
task :events_for_clinicsetup_and_book_appointment => :environment do
  Report::Event.create(status:"active",custom_id:222, name:'setup clinic for member',category:'gpsoc', event_actions:["setup_clinic"], event_controllers:["clinic"])
  Report::Event.create(status:"active",custom_id:223, name:'list available clinic for member',category:'gpsoc', event_actions:["get_clinic_list"], event_controllers:["clinic"])
  Report::Event.create(status:"active",custom_id:224, name:'authenticate and link to clinic',category:'gpsoc', event_actions:["authenticate_by_clinic"], event_controllers:["clinic"])
  Report::Event.create(status:"active",custom_id:225, name:'get clinic linakble status',category:'gpsoc', event_actions:["get_clinic_linkable_status"], event_controllers:["clinic"])
  Report::Event.create(status:"active",custom_id:226, name:'list services available by clinic',category:'gpsoc', event_actions:["get_clinic_services"], event_controllers:["clinic"])
  Report::Event.create(status:"active",custom_id:227, name:'delink with clinic account ',category:'gpsoc', event_actions:["delink_with_clinic"], event_controllers:["clinic"])
 
  Report::Event.create(status:"active",custom_id:228, name:'list user appointments',category:'gpsoc', event_actions:["list_user_appointment"], event_controllers:["clinic/book_appointment"])
  Report::Event.create(status:"active",custom_id:229, name:'list available appointments for booking',category:'gpsoc', event_actions:["available_appointments"], event_controllers:["clinic/book_appointment"])
  Report::Event.create(status:"active",custom_id:230, name:'list available doctor from a clinic for appointment booking',category:'gpsoc', event_actions:["get_doctor_list"], event_controllers:["clinic/book_appointment"])
  Report::Event.create(status:"active",custom_id:231, name:'list purpose for appointment',category:'gpsoc', event_actions:["get_appointment_purpose_list"], event_controllers:["clinic/book_appointment"])
  Report::Event.create(status:"active",custom_id:232, name:'book an appointment with a doctor',category:'gpsoc', event_actions:["create_appointment"], event_controllers:["clinic/book_appointment"])
  Report::Event.create(status:"active",custom_id:233, name:'cancel booked appointment',category:'gpsoc', event_actions:["cancel_appointment"], event_controllers:["clinic/book_appointment"])
  Report::Event.create(status:"active",custom_id:234, name:'reschedule booked appointment',category:'gpsoc', event_actions:["update_appointment"], event_controllers:["clinic/book_appointment"])
  Report::Event.create(status:"active",custom_id:235, name:'reschedule booked appointment',category:'gpsoc', event_actions:["update_appointment"], event_controllers:["clinic/book_appointment"])
  
  Report::Event.create(status:"active",custom_id:236, name:'get pregnancy week detail',category:'pregnancy', event_actions:["get_pregnancy_week_detail"], event_controllers:["pregnancy"])
  Report::Event.create(status:"active",custom_id:237, name:'search gp practices in emis',category:'gpsoc', event_actions:["search_gp_practices"], event_controllers:["gp_soc"])
end

# seed event for clinic/gp soc setup and Appoitment Booking
# rake events_for_clinicsetup_and_book_appointment RAILS_ENV=production
task :events_for_cvtme => :environment do
  Report::Event.create(status:"active",custom_id:238,title:'update cvtme purchase for family', name:'update cvtme subscription for family',category:'family', event_actions:["update_family_purchase_for_cvtme"], event_controllers:["child/cvtme/color_vision_test_result"])
  Report::Event.create(status:"active",custom_id:239,title:'update cvtme color vision test result', name:'update cvtme color vision test result',category:'family', event_actions:["update_family_purchase_for_cvtme"], event_controllers:["child/cvtme/result"])
  Report::Event.create(status:"active",custom_id:244,title:'update cvtme purchase attempt count on expiry', name:'update cvtme purchase attempt count on expiry',category:'family', event_actions:["NA"], event_controllers:["NA"])
end


# seed event for clinic/otp verification
# rake events_for_clinicsetup_and_book_appointment RAILS_ENV=production
task :events_for_otp_clinic_verification => :environment do
  Report::Event.create(status:"active",custom_id:245,title:'Otp generated', name:'Otp generated',category:'gpsoc', event_actions:["NA"], event_controllers:["NA"])
  Report::Event.create(status:"active",custom_id:246,title:'Otp verified', name:'Otp verified',category:'gpsoc', event_actions:["NA"], event_controllers:["NA"])
end
# seed event for alexa math free popup
# rake events_for_alexa_math_free_popup RAILS_ENV=production
task :events_for_alexa_math_free_popup => :environment do
  custom_id =  Report::Event.max(:custom_id) + 1 
  Report::Event.create(status:"active",custom_id:custom_id,title:'Show mental math free popup', name:'Show mental math free popup',category:'mental math', event_actions:["NA"], event_controllers:["NA"])
end



# seed event for login/password changedevent
# rake events_for_login_password_changed RAILS_ENV=production
task :events_for_login_password_changed => :environment do
  Report::Event.create(status:"active",custom_id:240, name:'user logged in successfully',category:'user', event_actions:["NA"], event_controllers:["NA"])
  Report::Event.create(status:"active",custom_id:241, name:'user password changed  successfully',category:'user', event_actions:["NA"], event_controllers:["NA"])
  Report::Event.create(status:"active",custom_id:242, name:'user signout successfully',category:'user', event_actions:["NA"], event_controllers:["NA"])
  Report::Event.create(status:"active",custom_id:243, name:'user signup successfully',category:'user', event_actions:["NA"], event_controllers:["NA"])
  Report::Event.where(:custom_id.in=>[180,201,205]).update_all(:status=>"inactive")

end


# seed event for Nhs syndicated Content/ Nhs Librery
# rake events_for_syndicated_content RAILS_ENV=production
task :events_for_syndicated_content => :environment do
  custom_id =  Report::Event.max(:custom_id) + 1 
  Report::Event.create(status:"active",custom_id:custom_id, name:'Get filtered content from syndicated content', title:'Get filtered content from syndicated content',category:'nhs', event_actions:["get_filter_content"], event_controllers:["nhs/syndicated_content"])
  custom_id =  Report::Event.max(:custom_id) + 1 
  Report::Event.create(status:"active",custom_id:custom_id, name:'Get category list syndicated content',title:'Get category list syndicated content' ,category:'nhs', event_actions:["get_category_list"], event_controllers:["nhs/syndicated_content"])
  custom_id =  Report::Event.max(:custom_id) + 1 
  Report::Event.create(status:"active",custom_id:custom_id, name:'Get topic list for category list from syndicated content',title:'Get topic list for category list from syndicated content',category:'nhs', event_actions:["get_topic_list_for_category"], event_controllers:["nhs/syndicated_content"])
  custom_id =  Report::Event.max(:custom_id) + 1 
  Report::Event.create(status:"active",custom_id:custom_id, name:'Show syndicated content', title:'Show syndicated content',category:'nhs', event_actions:["get_content"], event_controllers:["nhs/syndicated_content"])
end

