# User notification report data
#rake save_user_notification_report_data RAILS_ENV=production
task :save_user_notification_report_data => :environment do
  (1..365).entries.reverse.each do |day|
    Report::PushNotificationReportData.create_data(day)
  end
end