#rake update_kick_count_in_pregnancy 
#rake update_kick_count_in_pregnancy RAILS_ENV=production 
task :update_kick_count_in_pregnancy => :environment do
   Pregnancy.where(:expected_on.gt => (Date.today - 3.weeks),:status=> "expected").each do |pregnancy|
    current_user, api_version = nil, 4
    pregnancy.add_kick_counts(current_user, api_version)
    puts "Kick count for #{pregnancy.id}"
  end
end
