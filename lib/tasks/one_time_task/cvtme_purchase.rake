  # seed premium puchase into purchase history
  #rake seed_cvtme_purchase_history RAILS_ENV=production 
  task :seed_cvtme_purchase_history => :environment do
    Child::VisionHealth::Purchase.where(:subscription_level=>1).each do |purchase|
      begin
        history_data = purchase.attributes
        history_data.delete("_id")
        history_data["purchase_date"] = purchase.purchase_date || purchase.created_at
        Child::VisionHealth::PurchaseHistory.new(history_data).save
      rescue Exception=>e 
        Rails.logger.info "Error inside seed_cvtme_purchase_history........ "
        Rails.logger.info purchase.inspect
        Rails.logger.info e.message
      end
    end

  end 


  # Add vision Health tool for existing member
  #rake seed_cvtme_purchase_history RAILS_ENV=production 
  task :seed_cvtme_purchase_history => :environment do
    tool = Nutrient.where(identifier:'Vision Health').last
    ChildMember.all.batch_size(100).each do |member|
      begin
        tool_included =  MemberNutrient.where(member_id:member.id,nutrient_id:tool.id).last
        if tool_included.blank?
           MemberNutrient.new(member_id:member.id,nutrient_id:tool.id,status:"active",position:tool.position).save
        end
      rescue Exception =>e 
        puts e.message
      end
    end
  end
