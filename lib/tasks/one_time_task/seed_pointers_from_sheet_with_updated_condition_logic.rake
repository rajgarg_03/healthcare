  #rake create_pointer_with_updated_conditions 
  #rake create_pointer_with_updated_conditions RAILS_ENV=production 
  task :create_pointer_with_updated_conditions => :environment do
    q1 = Roo::Excelx.new("./lib/system_references_with_updated_condition.xlsx")
    q1.default_sheet = q1.sheets[0]
    # SysArticleRef.delete_all
    q1.entries[1..-1].each do |entry|
      name = entry[5].gsub('\\\\','\\') rescue ""
        SysArticleRef.where(custome_id:entry[0].to_i).delete
        SysArticleRef.create(
          name:name,
          custome_id: entry[0].to_i,
          status: entry[1],
          description:entry[8],
          category:entry[3],
          criteria: entry[7],
          score:entry[9], 
          ref_start_date:entry[11],
          ref_peak_rel_date:entry[13],
          ref_expire_date:entry[15],
          N_factor: entry[16], 
          valid_for_role: entry[18],
          stage: entry[2] ,
          scenarios: entry[4] ,
          condition: entry[6],
          topic: entry[8],
          ds: entry[10],
          dp: entry[12],
          dexp: entry[14]
        )
        puts "#{entry[3]} and name is #{name}"
    end
  end