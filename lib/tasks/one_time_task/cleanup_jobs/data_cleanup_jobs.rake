  #rake clean_notification_from_db_for_deleted_member
  #rake clean_notification_from_db_for_deleted_member RAILS_ENV=production 
  task :clean_notification_from_db_for_deleted_member => :environment do
    Notification.not_in(member_id:nil).batch_size(20).each do |notification|
      Member.find(notification.member_id.to_s) rescue notification.delete
    end
    Notification.not_in(child_member_id:nil).batch_size(20).each do |notification|
      Member.find(notification.child_member_id.to_s) rescue notification.delete
    end

    Notification.not_in(sender_id:nil).batch_size(20).each do |notification|
      Member.find(notification.sender_id.to_s) rescue notification.delete
    end

   end

   #rake clean_junked_child_member_record
   #rake clean_junked_child_member_record RAILS_ENV=production 
   task :clean_junked_child_member_record => :environment do
    ChildMember.all.batch_size(20).each do |member|
      begin
        FamilyMember.where(member_id:member.id.to_s).last.family_id 
      rescue Exception=>e
        member.delete_child_member
        member.delete rescue nil
      end
    end
   end

   #rake duplicated_family_event_in_api_log
   #rake duplicated_family_event_in_api_log RAILS_ENV=production 
   task :duplicated_family_event_in_api_log => :environment do
    country = Member.sanitize_country_code("uk")
    mark_family_active = 13
    mark_family_inactive = 14
    #inactive_marked_family_ids  = ::Report::ApiLog.get_log_event(custom_id=14,nil,nil,{:family_country_code=>"uk", :start_date=>Date.today - 1.month,:end_date=>Date.today,:selected_field=>"family_id"}).map(&:family_id).uniq

    Family.where(:country_code.in=>country).only(:family_id).batch_size(20).no_timeout.each do |family|
      begin
        Report::ApiLog.delete_family_duplicate_event_v2(event_ids:[mark_family_active,mark_family_inactive], family_id:family.id,delete_record:true)
      rescue Exception=>e
        Rails.logger.info "Error for family id #{family.id} ........   ....#{e.message}" rescue nil
      end
    end
   end
