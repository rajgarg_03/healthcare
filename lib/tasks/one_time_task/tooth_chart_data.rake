# Tooth chart data creation
#rake create_tooth_chart_data RAILS_ENV=production
task :create_tooth_chart_data => :environment do
  Child::ToothChart::ChartManager.seed_data
end
