  #rake create_allergy_manager_data 
  #rake create_allergy_manager_data RAILS_ENV=production 
  task :create_allergy_manager_data => :environment do
    q1 = Roo::Excelx.new("./lib/data/ChildHealthCardAllergyManager.xlsx")
    # q1 = Roo::Excelx.new("./lib/system_references.xlsx")
    sheets = q1.sheets
     AllergyManager.delete_all
    (0..2).each do |index| 
        entries = q1.sheet(index).entries[1..-1]
        category = sheets[index]
        entries.each do |entry|
          AllergyManager.create(
            name:entry[0],
            category:category 
          )
          puts "#{entry[0]} and category is #{category}"
          puts sheets[index].downcase
      end
    end
  end


  #rake create_allergy_symptom_manager_data 
  #rake create_allergy_symptom_manager_data RAILS_ENV=production 
  task :create_allergy_symptom_manager_data => :environment do
    q1 = Roo::Excelx.new("./lib/data/ChildHealthCardSymptom.xlsx")
    # q1 = Roo::Excelx.new("./lib/system_references.xlsx")
    
     q1.default_sheet = q1.sheets[0]
    AllergySymptomManager.delete_all
    entries = q1.entries[1..-1]
    
    entries.each do |entry|
      AllergySymptomManager.create(
        name:entry[0]
      )
      puts "#{entry[0]}"
    end
  end

