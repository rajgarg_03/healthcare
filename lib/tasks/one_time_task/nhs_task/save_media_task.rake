task :add_media_content=> :environment do 
  begin
    start_date = (Date.current - 3.year).to_date6
    end_date = Date.current.to_date6
    Nhs::NhsContentManager.save_media_data(start_date,end_date)
  rescue Exception => e 
    UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,{},"Error inside nhs- add_media_content rake job")
  end
end


# Migrate added syndicated content into Pregnancy week detail into syndicatedcontentlink tool
task :add_nhs_syndicated_content_linked_with_pregnancy_week_detail => :environment do
  System::Pregnancy::PregnancyWeekDetail.where(:nhs_syndicate_content_id.nin=>[nil,""]).each do |pregnancy_week_detail|
    begin
      nhs_syndicated_content = Nhs::SyndicatedContent.find(pregnancy_week_detail.nhs_syndicate_content_id)
      tool_object = pregnancy_week_detail
      nhs_syndicated_content_custom_id = nhs_syndicated_content.custom_id
      ::Nhs::SyndicatedContentLinkTool.add_to_list(tool_object,nhs_syndicated_content_custom_id)
    rescue Exception => e 
      Rails.logger.info "Error inside rake job add_nhs_syndicated_content_linked_with_pregnancy_week_detail ...... #{e.message}"
      puts "Error inside rake job add_nhs_syndicated_content_linked_with_pregnancy_week_detail ...... #{e.message}"
    end
  end
end


# Migrate added syndicated content into system vaccination into syndicatedcontentlink tool
task :add_nhs_syndicated_content_linked_with_vaccination => :environment do
  SystemVaccin.where(:nhs_syndicate_content_id.nin=>[nil,""]).each do |system_vaccine|
    begin
      nhs_syndicated_content = Nhs::SyndicatedContent.find(system_vaccine.nhs_syndicate_content_id)
      tool_object = system_vaccine
      nhs_syndicated_content_custom_id = nhs_syndicated_content.custom_id
      ::Nhs::SyndicatedContentLinkTool.add_to_list(tool_object,nhs_syndicated_content_custom_id)
    rescue Exception => e 
      Rails.logger.info "Error inside rake job add_nhs_syndicated_content_linked_with_vaccination ...... #{e.message}"
      puts "Error inside rake job add_nhs_syndicated_content_linked_with_vaccination ...... #{e.message}"
    end
  end
end
    