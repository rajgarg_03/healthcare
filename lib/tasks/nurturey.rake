#encoding: utf-8
namespace :nurturey do
  

  def self.open_spreadsheet(file, filename)
	case File.extname(filename)
	  when ".ods" then
	  	Roo::OpenOffice.new(file.path)
	  when ".csv" then
	    Roo::Csv.new(file.path, nil)
	  when ".xls" then
	    Roo::Excel.new(file.path, nil)
	  when ".xlsx" then
	    Roo::Excelx.new(file.path)
	  else
	    raise "Unknown file type: #{filename}"
	end
  end

  desc "Create dummy milestones"
  task :create_system_milestones => :environment do
  	
  	filename = "Milestones.ods"
    file= File.open("lib/#{filename}", "r")
    spreadsheet = open_spreadsheet(file, filename)    
  	sheet = spreadsheet.sheet("Milestones")
  	puts "hi"
  	header = sheet.row(1)
   	(2..sheet.last_row).each do |i|
	  row = Hash[[header, sheet.row(i)].transpose]
	  row["Age"] = row["Age"].split(" ").first if row["Age"]
	  row["Age"] ||= SystemMilestone.last.category
	  system_milestone = SystemMilestone.find_or_initialize_by(category: row["Age"], milestone_type: row["Type"], title: row["Title"], description: row["Description"])
	  system_milestone.save
	  puts "System milestone #{system_milestone.title} with category #{system_milestone.category} saved"
	end
 
  end

  task :add_nutrients_data do
    Rake::Task["nurturey:create_nutrients"].invoke
    Rake::Task["nurturey:add_nutrients"].invoke
  end  

  desc "Create Nutrients"
  task :create_nutrients => :environment do  	
  	Nutrient.where(title: "Timeline",identifier:"Timeline",  description: "Timeline Description", categories: ["Health"], recommended: true).first_or_create
  	Nutrient.where(title: "Calendar",identifier:"Calendar", description: "Calendar Description", categories: ["Health", "Mind"], recommended: true).first_or_create
        Nutrient.where(title: "Measurements", identifier:"Measurements", description: "Measurement Description", categories: ["Health", "Mind","Education"], recommended: true).first_or_create
        Nutrient.where(title: "Milestones",identifier:"Milestones", description: "Milestone Description", categories: ["Health", "Mind","Education"], recommended: true).first_or_create  	
  	Nutrient.where(title: "Immunisations",identifier:"Immunisations", description: "Immunisations Description", categories: ["Health", "Mind"], recommended: true).first_or_create
  	Nutrient.where(title: "Documents",identifier:"Documents", description: "Documents Description", recommended: true).first_or_create
	  puts "Nutrients #{Nutrient.all.map(&:title)} added"  	
  end

  desc "Updated pregnancy tool description for nutrients, Add Prenatal Test Nutrient"
  task :update_desc_and_add_prenatal_nutrient => :environment do   
    nutrients_data = {Timeline: 'Culmination of information from all tools - the whole story',
     Calendar: 'Easily manage your appointments and activity schedule now',
     Documents: 'Keep your most important documents safe and easily accessible'}
    nutrients_data.each do |title,preg_desc|
      nutrient = Nutrient.where(title: title).first
      nutrient.update_attribute(:pregnancy_description, preg_desc)
      puts "Nutrient pregnancy description updated for #{nutrient.title}"
    end
    prenatal_nutrient = Nutrient.where(title: "Prenatal Tests",identifier:"Prenatal Tests").first_or_initialize
    prenatal_nutrient.recommended = true
    prenatal_nutrient.pregnancy_description = "Manage your Prenatal tests schedule and details easily"
    puts "Nutrients #{prenatal_nutrient.title} added" if prenatal_nutrient.save!
  end

  task :add_nutrients => :environment do
    Member.all.each do |member|
      if member.is_child?
        ["Timeline","Calendar", "Milestone", "Measurements", "Immunisations", "Documents"].each do |nut_name|
          nutrient = Nutrient.where(title: nut_name).first          
          member_nutrient = MemberNutrient.where(nutrient_id: nutrient.id, member: member.id).first_or_create
        end
      end  
    end
    puts "nutrients added"
  end  

  task :delete_nutrients_data => :environment do
    MemberNutrient.destroy_all
    Nutrient.destroy_all
  end

  task :update_nutrients_order => :environment do
    Nutrient.where(title: "Timeline").first.update_attribute(:position,"1")
    Nutrient.where(title: "Calendar").first.update_attribute(:position,"2")
    Nutrient.where(title: "Measurements").first.update_attribute(:position,"3")
    Nutrient.where(title: "Milestones").first.update_attribute(:position,"4")
    Nutrient.where(title: "Immunisations").first.update_attribute(:position,"5")
    Nutrient.where(title: "Documents").first.update_attribute(:position,"6")
    puts "updated"
  end  

  task :update_member_nutrients_order => :environment do
    ChildMember.all.each do |child|
      MemberNutrient.where(member_id:child.id.to_s).each do |member_nut|
        member_nut.update_attribute(:position,member_nut.nutrient.position)
      end  
    end  
    puts "updated"
  end  

  task :send_welcome_email => :environment do
    #emails = ["vatsal@nurturey.com","rajgarg_03@yahoo.co.in","aman199002@gmail.com"]
    #users = User.where(:email.in => emails)
    users = User.or({:status => 'confirmed'},{:status => 'approved'}).where(:created_at.lte => 30.minutes.ago, :welcome_email_sent.ne => true)
    users.each do |user|
      begin
        UserMailer.welcome_email(user).deliver!
        user.update_attribute(:welcome_email_sent,true)
        puts("Sent welcome_email to #{user.email}")
        #Rails.logger.warn("Sent welcome_email to #{user.email}")
      rescue
        puts("Email could not be sent to #{user.email}")
        Rails.logger.warn("Email could not be sent to #{user.email}")
      end  
    end
  end


#rake nurturey:create_system_medical_event
  task :create_system_medical_event => :environment do
    
    filename = "test_details.xlsx"
    file= File.open("lib/#{filename}", "r")
    spreadsheet = open_spreadsheet(file, filename)    
    sheet = spreadsheet.sheet("Sheet1")
    puts "hi"
    header = sheet.row(1)
    # For all countries
    # ["in", "gb", "us", "au", "sg", "nz", "other", "za", "ca", "ae"].each do |country_code|

    (4..6).each do |i|
      sheet.row(i)[2].split(",").each do |a|
        SysMedicalEvent.new(category:sheet.row(i)[0],expected_on:sheet.row(i)[1],  name:a.strip, recommended:true,category_title:sheet.row(i)[3]).save
      end
    end
    (14..17).each do |i|
      sheet.row(i)[1].split(",").each do |a|
        SysMedicalEvent.new(category:sheet.row(i)[0], name:a.strip, recommended:false,category_title:sheet.row(i)[3]).save
      end
    end
    # end
    SysMedicalEvent.where(:recommended=>"false").each do |data|
      if SysMedicalEvent.where(:recommended=>"true",name: data.name).present?
        data.delete
      end
    end

  end
  
  #rake nurturey:create_medical_test_time RAILS_ENV=production
  task :create_medical_test_time => :environment do
    filename = "test_details.xlsx"
    file= File.open("lib/#{filename}", "r")
    spreadsheet = open_spreadsheet(file, filename)    
    sheet = spreadsheet.sheet("Sheet1")
    header = sheet.row(1)
    (4..7).each do |k|
      sheet.row(3)[k].split(",").each do |m|
        (4..6).each do |i|
          sheet.row(i)[4]
            puts sheet.row(i)[k]
            SysMedicalEventTime.new(country_code:m.downcase.strip,test_expected_on:sheet.row(i)[k], category_title:sheet.row(i)[3]).save
        end
      end
    end
  end

  

  #rake nurturey:clean_notification
  #rake nurturey:clean_notification RAILS_ENV=production 
  task :clean_notification => :environment do
    Notification.all.each do |notification|
      if notification.child_member_id.present?
        if ChildMember.where(id:notification.child_member_id.to_s).first.blank?
          puts "child member is not present ----- #{notification.child_member_id}"
          Rails.logger.info "child member is not present ----- #{notification.child_member_id}"
          notification.delete
        end
      end
    end
  end

  #rake nurturey:add_system_settings
  #rake nurturey:add_system_settings RAILS_ENV=production 
  task :add_system_settings => :environment do
    System::Settings.create(setting_values:{user_stage:'ambassador',signin_frequency:9,per_day_limit:1000}, name:"appReviewPopupLunch",status:"Active",signin_frequency:9,description:'ambassador user ,every 10th signin 100 popup per day')
    
  end

  #rake nurturey:create_sys_vaccination[7,NZ]
  #rake nurturey:create_sys_vaccination[7,NZ] RAILS_ENV=production 
  task :create_sys_vaccination,[:sheet_no,:country_code] => :environment do |t,arg|
    q1 = Roo::Excelx.new("./public/Vaccination.xlsx")
    q1.default_sheet = q1.sheets[arg[:sheet_no].to_i]
    # SysArticleRef.delete_all
    name = ""
    count = 0
    sysvaccin = nil
    q1.entries[1..-1].each do |entry|
      if sysvaccin.present? && entry[0].present?
        sysvaccin.jabs_count = count
        sysvaccin.save
      end
      if entry[0].present?
        name = entry[1]
        sysvaccin = SystemVaccin.create(name:entry[1],category:entry[4],vaccin_type:entry[5],country:arg[:country_code])
        count = 0
      else
        SystemJab.create(jab_type:sysvaccin.category,due_on:entry[2],system_vaccin_id:sysvaccin.id)
        if (entry[3].strip == "Girls only" rescue false)
          sysvaccin.only_for= FamilyMember::ROLES[:daughter]
          sysvaccin.save
        end
        puts name.to_s + entry[1].to_s
        count = count+1
      end
    end
    sysvaccin = SystemVaccin.last
    jabs = SystemJab.where(system_vaccin_id:sysvaccin.id).count
    sysvaccin.jabs_count = jabs
    sysvaccin.save
    SystemJab.save_due_on_day
  end

     
    
end
