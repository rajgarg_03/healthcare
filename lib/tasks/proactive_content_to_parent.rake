# rake nurturey:send_proactive_email RAILS_ENV=production
namespace :nurturey do
  task :send_proactive_email => :environment do
    find_parent_for_milestone("May have temper tantrums",["http://www.mumsnet.com/toddlers/tantrums"])
    find_parent_for_milestone("Begins to crawl",["http://www.parents.com/baby/safety/babyproofing/","http://www.babycenter.com/0_childproofing-checklist-before-your-baby-crawls_9446.bc"])
    find_parent_for_milestone("Separation anxiety",["http://www.mumsnet.com/jobs/childcare-separation-anxiety"])
    find_parent_for_health("bmi",85,"http://www.obesityaction.org/understanding-obesity-in-children/what-is-childhood-obesity")
  end

  def find_parent_for_milestone(mileston_name, links)
    system_milstone = SystemMilestone.where(title:mileston_name).first
    sent_to_parent_emails = []
    child_with_miestone_selected = Milestone.where(system_milestone_id: system_milstone.id).map(&:member_id)
    childerns = ChildMember.in(id:child_with_miestone_selected)
    childerns.each do |child|
      child.family_elder_members.each do |parent_id|
        parent = User.where(member_id:parent_id).or({:status => 'confirmed'},{:status => 'approved'}).first
        begin
          if parent.present?
            UserMailer.proactive_content(parent,links,mileston_name).deliver unless sent_to_parent_emails.include?(parent.email) 
            sent_to_parent_emails.push(parent.email)
            puts("Sent proactive_content1 email to #{parent.inspect}")
            Rails.logger.warn("Sent proactive_content email to #{parent.inspect}")
          end
        rescue
          Rails.logger.warn("Not Sent proactive_content email to #{parent.inspect}")
        end
      end
    end
  end

  def find_parent_for_health(measurement,threshold, links)
    health = Health.not_in(measurement =>["",nil,"0.0"])
    member_bmi = {}
    sent_to_parent_emails = []
    health.each do |health|
      member_bmi[health.member_id] = health.bmi.to_f if health.bmi.to_i > threshold.to_i rescue nil 
    end

    child_members = member_bmi.keys.uniq
    childerns = ChildMember.in(id:child_members)
    childerns.each do |child|
      child.family_elder_members.each do |parent_id|
        puts parent_id
        parent = User.where(member_id:parent_id).or({:status => 'confirmed'},{:status => 'approved'}).first
        begin
          if parent.present?
            UserMailer.proactive_content(parent,links,measurement).deliver unless sent_to_parent_emails.include?(parent.email) 
            sent_to_parent_emails.push(parent.email)
            Rails.logger.warn("Sent proactive_content email to #{parent.inspect}")
          end
        rescue
          Rails.logger.warn("Not Sent proactive_content email to #{parent.inspect}")
        end
      end
    end
  end

# rake nurturey:send_device_version_notification["ios","Please update to latest Nurturey app. Else you may see issues on iOS 10"] RAILS_ENV=production
  task :send_device_version_notification,[:device_type,:msg] => :environment do |t,args|
    notification = UserNotification.new(message:args[:msg],status:"sent",type:"customize")
    ids = Device.where(platform:args[:device_type]).pluck(:member_id).uniq
    user_member_ids = User.in(:member_id=>ids.map(&:to_s)).in(status:["confirmed","approved"]).pluck(:member_id).uniq
    pusher = UserNotification.notification_pusher
    Member.in(:id=>user_member_ids.map(&:to_s)).batch_size(50).each do |current_user|
      begin
        UserNotification.push_customize_notification(notification,current_user,pusher)
       rescue Exception=>e
      end
    end
  end

# rake nurturey:send_device_version_email["ios"] RAILS_ENV=production
  task :send_device_version_email,[:device_type] => :environment do |t,args|
    ids = Device.where(platform:args[:device_type]).pluck(:member_id).uniq
    user_member_ids = User.in(:member_id=>ids.map(&:to_s)).in(status:["confirmed","approved"]).pluck(:member_id).uniq
    Member.in(:id=>user_member_ids.map(&:to_s)).batch_size(50).each do |current_user|
      begin
        UserMailer.send_device_version_email(current_user).deliver 
      rescue Exception=>e
      end
    end
  end
end