namespace :reports do
require "spreadsheet"
  def weekly_analytics_data(country_code,start_date=nil,end_date=nil)
  	country = (country_code.downcase == 'others' ? [ 'in','us','gb','IN','US','GB','In','Us','Gb'] : [ country_code, country_code.downcase, country_code.upcase])
    users = get_users_by_date(country_code,country,start_date,end_date)
    user_member_ids = users.map{|u| u.member && u.member.id}
    user_members = Member.in(id:user_member_ids)
  	app_signups = Device.in(:member_id => user_member_ids)
  	no_of_signups_ios = Device.where(platform: 'ios').in(:member_id => user_member_ids).count
  	no_of_signups_android = Device.where(platform: 'android').in(member_id:user_member_ids).count
  	
  	# ios_signups_count = app_signups.where(platform: 'ios').count
  	# android_signups_count = app_signups.where(platform: 'android').count  	
  	no_of_signups_total = users.count
  	no_of_signups_web = no_of_signups_total - no_of_signups_ios - no_of_signups_android 
  	# no_of_signins_total = users.where(:last_sign_in_at.gte => 7.days.ago).count
    login_time = start_date ? start_date : 7.days.ago
    no_of_signins_total = start_date ? UserLoginDetail.where(:user_id.in => users.map(&:id), :login_time.gte => start_date, :login_time.lte => end_date).count : UserLoginDetail.where(:user_id.in => users.map(&:id), :login_time.gte => login_time).count
    no_of_weekly_active_users = no_of_signins_total
    month_start_date = start_date ? start_date.beginning_of_month : Time.now.beginning_of_month
    month_end_date = end_date ? start_date.end_of_month : Time.now.end_of_month
  	no_of_monthly_active_users = UserLoginDetail.where(:user_id.in => users.map(&:id), :login_time.gte => month_start_date, :login_time.lte => month_start_date).count
  	average_login_per_user = no_of_signins_total/users.count rescue 0
    no_of_inactive_users = (if country_code == 'others'
                            User.nin(:country_code => country).where(:last_sign_in_at.lte => 6.months.ago).count
                          else
                            User.in(:country_code => country).where(:last_sign_in_at.lte => 6.months.ago).count
                          end)
    general_families = user_members.map(&:user_families).flatten.compact #users_ind.map(&:member).map(&:families).flatten
  	no_of_families = general_families.count
    child_family_members = general_families.map(&:family_members).flatten.select{|fm| ['son','daughter','Son','Daughter'].include?(fm.role)}
    child_member_ids = child_family_members.map(&:member_id)
  	no_of_kids = child_family_members.count
    no_of_immunisations = get_tool_data('Vaccination',child_member_ids,start_date,end_date)
    no_of_calendar_events = get_tool_data('Event',child_member_ids,start_date,end_date)
    no_of_measurements = get_tool_data('Health',child_member_ids,start_date,end_date)
    no_of_milestones = get_tool_data('Milestone',child_member_ids,start_date,end_date)
    no_of_documents = get_tool_data('Document',child_member_ids,start_date,end_date)
    no_of_timelines = get_tool_data('Timeline',child_member_ids,start_date,end_date)
    no_of_prenatal_tests = get_tool_data('MedicalEvent',child_member_ids,start_date,end_date)
    # General	No. of basic tools active/ user
  	# General	No. of premium tools active/ user
  	all_notifications = Notification.in(member_id:user_member_ids)
  	no_of_notifications_sent = all_notifications.count
  	#TODO: Need to confirm with Tushar
  	# no_notifications_disabled = Notification.where('member_id' => {"$in" => users_ind.map(&:member).map(&:id).flatten})
  	no_of_notifications_clicked = all_notifications.where(checked: true).count
    data = {"Country"=> (Country[country_code] || country_code).to_s, 
            "No of iphone signups"=> no_of_signups_ios,
            "No of android signups"=> no_of_signups_android,
            "No of web signups"=> no_of_signups_web,
            "No of total signups" => no_of_signups_total,
            "No of signins"=> no_of_signins_total,
            "No of weekly active users" => no_of_weekly_active_users,
            "No of monthly active users" => no_of_monthly_active_users,
            "Average login per active user per week" => average_login_per_user,
            "No. of inactive users (no login in 6 months)" => no_of_inactive_users,
            "No. of Immunisations" => no_of_immunisations,
            "No. of Calendar Events" => no_of_calendar_events,
            "No. of Measurements" => no_of_measurements,
            "No. of Milestones" => no_of_milestones,
            "No. of Documents" => no_of_documents,
            "No. of Timelines" => no_of_timelines,
            "No. of Prenatal tests" => no_of_prenatal_tests,
            "No. of Families" => no_of_families,
            "No. of Kids" => no_of_kids,
            # "No. of basic tools active/ user" => 0,
            "No. of notifications sent" => no_of_notifications_sent,
            # "No. of notifications disabled" => 0,
            "No. of notifications clicked" => no_of_notifications_clicked
          }
  end

  def get_users_by_date(country_code,country,start_date=nil,end_date=nil)
    if start_date && end_date
      users = country_code == 'others' ? User.where(:country_code.nin => country, :created_at.gte => start_date, :created_at.lte => end_date) : User.where(:country_code.in => country, :created_at.gte => start_date, :created_at.lte => end_date)
    else
      users = country_code == 'others' ? User.where(:country_code.nin => country, :created_at.gte => 7.days.ago) : User.where(:country_code.in => country, :created_at.gte => 7.days.ago)
    end
    users  
  end

  def get_tool_data(class_name,child_member_ids,start_date=nil,end_date=nil)
    if start_date && end_date
      class_name.constantize.where(:member_id.in => child_member_ids, :created_at.gte => start_date, :created_at.lte => end_date).count
    else
      class_name.constantize.where(:member_id.in => child_member_ids, :created_at.gte => 7.days.ago).count
    end
  end

  def generate_report_and_mail(data,recipient,start_date=nil,end_date=nil)
    Spreadsheet.client_encoding = 'UTF-8'
    # create an xls workbook template for data importing based on models in activerecord
    @format = Spreadsheet::Format.new(:weight => :bold)
    @template_folder = File.join("./tmp/")
    @template_file = File.join(@template_folder, "data_import.xls")
    @book = Spreadsheet::Workbook.new
    data.each do |k,data_values|
      write_sheet1 = @book.create_worksheet :name => k.upcase
      write_sheet1.column(0).width = 50
      write_sheet1.column(1).width = 50
      data_values.keys.each_with_index do |key,i|
        row_id = i + 1
        write_sheet1.row(row_id).set_format(i, @format)
        write_sheet1.row(row_id).push key.to_s
      end
      data_values.values.each_with_index do |value,i|
        row_id = i + 1
        write_sheet1.row(row_id).push(value)
      end
    end
    @book.write @template_file
    UserMailer.weekly_report_to_admin(start_date,end_date,recipient).deliver
  end  

  task :send_report, [:recipient] => :environment do |t,arg|
    ## TODO: To be run only once
    User.in(country_code:["India","india"]).update_all({country_code:"IN"})
    User.in(country_code:["uk","UK"]).update_all({country_code:"GB"})
    User.where(country_code:nil).update_all({country_code:"GB"})
    recipient = arg[:recipient].present? ? arg[:recipient] : 'tushar@nurturey.com'
    data = {}
    country_codes = ['in','us','gb','others']
    country_codes.each do |country_code|
      data[country_code] = weekly_analytics_data(country_code)
    end
    generate_report_and_mail(data,recipient)
  end

  task :send_report_for_previous_weeks, [:start_date,:end_date,:recipient] => :environment do |t,arg|
    start_date = DateTime.parse(arg[:start_date])
    end_date = DateTime.parse(arg[:end_date])
    data = {}
    country_codes = ['in','us','gb','others']
    country_codes.each do |country_code|
      data[country_code] = weekly_analytics_data(country_code,start_date,end_date)
    end
    generate_report_and_mail(data,arg[:recipient],start_date,end_date)
  end

end