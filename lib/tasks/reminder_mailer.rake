 # send reminder to all user for their childerns vaccination according to their setting saved in settings (jab_reminder_email).
 # send reminder 15 days before if user selected no setting 
 # usage: rake nurturey:send_remind_email RAILS_ENV=production
 # usage: rake nurturey:within_30day_jab_reminder RAILS_ENV=production

namespace :nurturey do
  task :send_remind_email => :environment do
    parents = ParentMember.all
    parents.batch_size(100).each do |parent|
      begin
       parent_user = parent.user_id rescue nil
        user = User.where(id:parent_user).or({:status => 'confirmed'},{:status => 'approved'}).first
        if user.present? && user.subscribed_to_mailer?
          reminder_setting = MemberSetting.where(member_id:parent.id,name:"jab_reminder_email").first.status rescue 15
          childern_ids = parent.childern_ids
          jab_reminder(parent,reminder_setting,childern_ids,user)
          # event_reminder(parent,reminder_setting,childern_ids,user)
        end
      rescue Exception=> e 
        puts e.message
        puts "parent #{parent.inspect}"
      end
    end
  end
# 1 time rake job
  task :within_30day_jab_reminder => :environment do 
    parents = ParentMember.all
    parents.batch_size(100).each do |parent|
     begin
        parent_user = parent.user.id rescue nil
        user = User.where(id:parent_user).or({:status => 'confirmed'},{:status => 'approved'}).first
        if user.present? && user.subscribed_to_mailer?
          childern_ids = parent.childern_ids
          within_30_day_jabs(parent,30,childern_ids,user)
        end
     rescue
        puts "mail not sent to parent #{parent.inspect}"
     end
    end
  end

  def jab_reminder(parent,reminder_setting,childern_ids,user)
    #separate email for each child
    childern_ids.each do |child_id|
      vaccinations = Vaccination.where({'member_id' => { "$in" => [child_id] } }).where(opted:"true").includes(:jabs)
      total_jabs = []
      child_member =  ChildMember.find(child_id)
      vaccinations.each do |vaccine|
        # due date for 30 days
        due_jabs = vaccine.jabs.where(status: "Planned").not_in(:due_on => [nil])
        due_jabs.each do |jab|
          reminder_day =  30.days
          schedule_date =  jab.due_on.in_time_zone(parent.time_zone) rescue jab.due_on
          if ((schedule_date - reminder_day).to_date == Date.today rescue false )
            jab_index = vaccine.jabs.pluck(:id).sort.index(jab.id)+1 rescue 1
            total_jabs.push({:jab=>jab,:jab_index=>jab_index, :vaccine=>vaccine, :schedule_date=>schedule_date}) rescue nil
          end
        end

        #estimated dated jab
        vaccine.jabs.where(status: "Planned",:due_on => nil).not_in(:est_due_time => [nil]).each do |jab|
          reminder_day =  30.days
          schedule_date =  jab.est_due_time.in_time_zone(parent.time_zone) rescue jab.est_due_time
          if ((schedule_date - reminder_day).to_date == Date.today rescue false )
            jab_index = vaccine.jabs.pluck(:id).sort.index(jab.id)+1 rescue 1
            
            total_jabs.push({:jab=>jab,:jab_index=>jab_index, :vaccine=>vaccine, :schedule_date=>schedule_date}) rescue nil
          end
        end

        #due dated jab for 24 hours before
        vaccine.jabs.where(status: "Planned").not_in(:due_on => [nil]).each do |jab|
          reminder_day =  1.days
          schedule_date = jab.due_on.in_time_zone(parent.time_zone) rescue jab.due_on 
          if ((schedule_date - reminder_day).to_date == Date.today rescue false )
            jab_index = vaccine.jabs.pluck(:id).sort.index(jab.id)+1 rescue 1
            total_jabs.push({:jab=>jab,:jab_index=>jab_index, :vaccine=>vaccine, :schedule_date=>schedule_date}) rescue nil
          end
        end
      end
      total_jabs = total_jabs.sort_by{|jab| jab["schedule_date"]} rescue nil
      
      if total_jabs.present?
        BatchMailer.immunisation_jab_reminder(nil,user.member,child_id,total_jabs).deliver #rescue nil
      end
    end
  end

  def within_30_day_jabs(parent,reminder_setting,childern_ids,user)
    childern_ids.each do |child_id|
      total_jabs = []
      vaccinations = Vaccination.all.where({'member_id' => { "$in" => [child_id] } }).where(opted:"true").includes(:jabs)
      vaccinations.each do |vaccine|
        #due dated jab
        vaccine.jabs.where(status: "Planned").each do |jab|
          time_zone = parent.time_zone rescue nil
          schedule_date = (jab.due_on || jab.est_due_time).in_time_zone(time_zone) rescue nil 
          if (( schedule_date.present? && schedule_date >= Date.today  && schedule_date <= (Date.today + 30.days)) rescue false )
            total_jabs.push({:jab=>jab, :vaccine=>vaccine, :schedule_date=>schedule_date})
          end
        end 
      end
      BatchMailer.immunisation_jab_reminder(nil,user.member,child_id,total_jabs).deliver    
    end
  end


  # def event_reminder(parent,reminder_setting,childern_ids,user)
  #   Event.where(:member_id =>parent.id).each do |event|
  #      if ((event.start_date - reminder_setting).to_date == Date.today rescue false)
  #       UserMailer.remind_event(user,event).deliver
  #     end
  #   end
  #   Event.all.where({'member_id' => { "$in" => childern_ids} }).each do |event|
  #     if ((event.start_date - reminder_setting).to_date == Date.today rescue false)
  #       UserMailer.remind_event(user,event).deliver
  #     end
  #   end
  # end

end
 
