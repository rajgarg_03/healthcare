
# rake nurturey:send_scheduled_birthday_wish_push_notification["IN"] RAILS_ENV=production
namespace :nurturey do
  task :send_scheduled_birthday_wish_push_notification,[:country_code] => :environment do |t,arg|
    pusher = UserNotification.notification_pusher
    country_code = Member.sanitize_country_code(arg[:country_code])
    UserCommunication::BirthdayWish.all_schedule_user_notification.batch_size(50).each do |notification|
      if arg[:country_code].downcase == "other"
        user = User.where(id:notification.user_id).not_in(country_code: UserCommunication::BirthdayWish::CountryCodeListForBirthdayPush).first
      else
        user = User.where(id:notification.user_id).in(country_code:country_code).first
      end
      begin
        if user.present? && user.is_valid_user_to_send_push?
          member = user.member
          devices = member.devices
          UserNotification.push_notification(notification,member,pusher,devices)
          birthday_wish_record = UserCommunication::BirthdayWish.find(notification.identifier)
          birthday_wish_record.update_record_for_schedule_push
        end
      rescue Exception => e 
          Rails.logger.info e.message
      end
    end     
  end
# rake nurturey:add_birthday_wish_push_notification RAILS_ENV=production
  task :add_birthday_wish_push_notification => :environment do
    identifier_19_is_active = (BatchNotification::BatchNotificationManager.where(identifier:19).last.status == "Active" ? true : false) rescue false
    if identifier_19_is_active
      childerns = ChildMember.where(:birth_date=>{"$lte"=>Date.today,"$gte"=>Date.today - 1.month})
    else
      childerns = ChildMember.where(:birth_date=>{"$lte"=>Date.today})
    end
    childerns.batch_size(100).each do |child|
      if Pregnancy.where(expected_member_id:child.id.to_s).pluck("id").blank?
         # send monthly and yearly birthday wish:() sending by identifier 19 )
        if !identifier_19_is_active && (child.birth_date.present? &&  (child.birth_date.day == Date.today.day || child.birth_date.day == (Date.today - 1.days).day) )
          age = child.age_in_months
          elder_member_ids = child.family_elder_members.map(&:to_s)
          users = User.in(member_id:elder_member_ids)
          push_msg_for_birthday_next_day = child.birth_date.day == (Date.today - 1.days).day
          users.each do |user|
            if user.is_valid_user_to_send_push?
              begin
                UserCommunication::BirthdayWish.add_to_push_notification(user,child,age,push_msg_for_birthday_next_day)
              rescue
                Rails.logger.warn("Push not sent to #{user.email}")
              end
            end
          end
	      else
	       # weekly birthday_wish
         add_weekly_birthday_wish(child)
        end
      end
     
    end
  end
  


  def add_weekly_birthday_wish(child)
    day_difference =  (Date.today - child.birth_date)
    age_in_week = day_difference.to_i/7 
    
    if age_in_week <= 4 &&  (day_difference%7 == 0) && (child.birth_date.day != Date.today.day)
      elder_member_ids = child.family_elder_members.map(&:to_s)
      users = User.in(member_id:elder_member_ids)
      users.each do |user|
        if user.is_valid_user_to_send_push?
          begin
            UserCommunication::BirthdayWish.add_weekly_birthday_wish_to_push_notification(user,child,age_in_week)
          rescue
            Rails.logger.warn("Push not sent to #{user.email}")
          end
        end
      end
    end   
  end
end   