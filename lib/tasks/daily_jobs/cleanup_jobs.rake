#Delete UserNotification::EmailDataToSend data for sent push and in draft since 1 month
task :clean_email_data_to_send => :environment do 
  push_notification_ids = UserNotification.or({notify_by:"both"},{notify_by:"email"}).where(email_status:"sent").pluck(:id)
  UserNotification::EmailDataToSend.in(:user_notification_id.in=> push_notification_ids).delete_all 
  UserNotification::EmailDataToSend.where(:updated_at.lte=> (Date.today - 1.month) ).delete_all
end