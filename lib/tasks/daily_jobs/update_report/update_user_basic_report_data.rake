# rake update_user_basic_summary_report_data RAILS_ENV=production
  task :update_user_basic_summary_report_data => :environment do  
    Report::UserBasicSummaryReportData.new.fetch_data
  end

# rake update_kpi_report_data RAILS_ENV=production
  task :update_kpi_report_data => :environment do  
    ::Report::KpiData.save_report_data(Date.today)
  end

# rake update_country_code_for_kpi RAILS_ENV=production
  task :update_country_code_for_kpi => :environment do  
    ::Report::ApiLog.update_country_code_for_event
  end

  