# rake mark_milestone_to_unknown_unaccomplished["unknown"] RAILS_ENV=production
# rake mark_milestone_to_unknown_unaccomplished["unaccomplished"] RAILS_ENV=production
  task :mark_milestone_to_unknown_unaccomplished,[:status] => :environment do |t,arg|
    childerns = ChildMember.where(:birth_date.lte=>Date.today,:pregnancy_status=>false)
    childerns.batch_size(100).each do |child|
      begin
        Milestone.mark_milestone_to_unknown_unaccomplished(child,arg[:status])  
      rescue Exception=>e 
      end
    end
  end
  