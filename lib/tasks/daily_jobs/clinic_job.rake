#rake update_nhs_gp_search_active_status
#rake update_nhs_gp_search_active_status RAILS_ENV=production 
task :update_nhs_gp_search_active_status=> :environment do 
  begin
    postcode = "br5 1qb"
    # data,message,status =  GpSoc::RegisteredUser.find_practice_by_nhs_search_api(postcode)
    data1,message1,status1 =  GpSoc::RegisteredUser.find_practice_by_place_or_postcode(postcode)

    system_setting = System::Settings.where(name:'nhsGPSearchApi').last
    if system_setting.present? &&  system_setting.status == "Active"
      # API depreciated
      # nhs_search_by_place_or_postcode_status = system_setting.setting_values["nhs_search_by_place_or_postcode_status"] || false
      # if status == 401
      #   system_setting.update_attribute(:setting_values,{nhs_search_by_postcode_api_status:false,nhs_search_by_place_or_postcode_status:nhs_search_by_place_or_postcode_status})
      # elsif status == 200
      #   system_setting.update_attribute(:setting_values,{nhs_search_by_postcode_api_status:true,nhs_search_by_place_or_postcode_status:nhs_search_by_place_or_postcode_status})
      # end

      nhs_search_by_postcode_api_status = system_setting.setting_values["nhs_search_by_postcode_api_status"] || false
      if status1 == 401
        system_setting.update_attribute(:setting_values,{nhs_search_by_postcode_api_status:nhs_search_by_postcode_api_status, nhs_search_by_place_or_postcode_status:false})
      elsif status1 == 200
        system_setting.update_attribute(:setting_values,{nhs_search_by_postcode_api_status:nhs_search_by_postcode_api_status, nhs_search_by_place_or_postcode_status:true})
      end

    end
  rescue Exception => e
    Rails.logger.info "Error inside.....update_nhs_gp_search_active_status .....#{e.message}"
  end  
end