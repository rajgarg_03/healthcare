# rake split_user_notification_data RAILS_ENV=production
  task :split_user_notification_data => :environment do 
    PushNotificationSplitedData.copy_data_from_user_notification rescue nil
    UserNotification::ArchivedEmailDateSent.copy_data_from_email_data_to_send rescue nil
  end