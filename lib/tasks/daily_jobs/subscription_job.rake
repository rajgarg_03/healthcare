# deactivate all premium tool on subscription expire
# rake deactivate_premium_tools_for_expire_subscription RAILS_ENV=production
  task :deactivate_premium_tools_for_expire_subscription => :environment do 
    free_subscriptionmanager = SubscriptionManager.where(:subscription_listing_order=>0).last
    expired_subscriptions = Subscription.expired_subscription.entries
    premium_tool_ids = Nutrient.premium_tools.pluck(:id)
    expired_subscriptions.each do |subscription|
      begin
        subscription.deactivate_premium_tools_for_expire_subscription(premium_tool_ids)
        subscription.subscription_manager_id = free_subscriptionmanager.id.to_s
        subscription.subscription_valid_certificate = nil
        subscription.save
      rescue Exception=>e 
        Rails.logger.info "From deactivate_premium_tools_for_expire_subscription"
        Rails.logger.info "subscription id - #{subscription.id}"
        Rails.logger.info e.message
      end
    end
  end

# Deactivate all premium tool on free subscription
# rake deactivate_premium_tools_for_free_subscription RAILS_ENV=production
  task :deactivate_premium_tools_for_free_subscription => :environment do 
    free_subscriptionmanager = SubscriptionManager.where(:subscription_listing_order=>0).last
    free_subscriptions = Subscription.where(:subscription_manager_id=>free_subscriptionmanager.id.to_s).entries
    premium_tool_ids = Nutrient.premium_tools.pluck(:id)
    free_subscriptions.each do |subscription|
      begin
        subscription.deactivate_premium_tools_for_expire_subscription(premium_tool_ids)
      rescue Exception=>e 
        Rails.logger.info "From deactivate_premium_tools_for_free_subscription"
        Rails.logger.info "subscription id - #{subscription.id}"
        Rails.logger.info e.message
      end
    end
  end


  # update all android receipt date
  # rake verify_update_android_data RAILS_ENV=production
  task :verify_update_android_data => :environment do 
    subscription_receipts = SubscriptionReceipt::Android.all
    subscription_receipts.each do |subscription_receipt|
      begin
        receipt_data = {}
        receipt_data[:purchase_token] = subscription_receipt.purchase_token
        receipt_data[:product_id] = subscription_receipt.product_id
        SubscriptionReceipt::Android.update_receipt(receipt_data)
      rescue Exception=>e 
        Rails.logger.info e.message
      end
    end
  end
  
  # Not in use. 
  # update subscription manager id(subscription plan) based in receipt
  # rake update_subscription_plan_for_ios RAILS_ENV=production
  task :update_subscription_plan_for_ios => :environment do 
    subscriptions = Subscription.where(platform:"ios",:subscription_listing_order.gt=>0)
    subscriptions.each do |subscription|
      begin
        family_id = subscription.family_id.to_s 
        subscription_receipt = SubscriptionReceipt::Ios.where(family_id:family_id).last
        receipt = subscription_receipt.receipt
        status, response , status_code = SubscriptionReceipt::Ios.validate_receipt(receipt,nil,{:family_id=>family_id}) 
        # subscription_receipt.product_id
        if status_code != StatusCode::Status401
          params = {}
          params[:receipt_end_date] = Time.at(response["expires_date_ms"].to_f/1000).to_date  
          params[:receipt_response] = response
          subscription_manager =  SubscriptionManager.where(ios_product_id:response[:product_id]).last
          current_user = Member.find(subscription.member_id)
          if !(subscription.assigned_by == "admin" && subscription.expiry_date > params[:receipt_end_date])
            subscription.update_subscription(subscription_manager,api_version=4,current_user,family_id,"ios",params)
          end
        end
      rescue Exception=>e 
        Rails.logger.info e.message
      end
    end
  end

  # update subscription_valid_certificate
  # rake update_subscription_valid_certificate RAILS_ENV=production
  task :update_subscription_valid_certificate => :environment do 
    Subscription.where(:expiry_date.lt=>Date.today ,:subscription_valid_certificate=>"true").update_all(:subscription_valid_certificate=>"grace")
    Subscription.expired_subscription.update_all(:subscription_valid_certificate=>"false")
  end
 
 