  #rake update_family_having_trial_pricing_stage
  #rake update_family_having_trial_pricing_stage RAILS_ENV=production 
  task :update_family_having_trial_pricing_stage=> :environment do 
    Family.where(:pricing_stage=>"trial").batch_size(20).each do |family|
      begin
        family.update_family_pricing_stage({:event_source=>"batch_job"})
      rescue Exception => e 
        UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,{},"Error inside update_family_trial_days rake job")
      end
    end
  end