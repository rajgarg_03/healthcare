# rake mark_teeth_status_unknown_or_unaccomplished["unknown/acoomplished","eurpted/shedded/all/nil"]
# rake mark_teeth_status_unknown_or_unaccomplished["unaccomplished/unknown","eurpted/shedded/all/nil"] RAILS_ENV=production
  task :mark_teeth_status_unknown_or_unaccomplished,[:status,:type] => :environment do |t,arg|
    childerns = ChildMember.where(:birth_date.lte=>Date.today)
    childerns.batch_size(100).each do |child|
      Child::ToothChart::ToothDetails.mark_teeth_status_unknown_or_unaccomplished(child,arg[:type],arg[:status])  
    end
  end
  