  # user not signin since 4 week and no notification sent since 2 week
  # rake assign_batch_notification_for_identifier_1[1] RAILS_ENV=production
  task :assign_batch_notification_for_identifier_1,[:identifier] => :environment do |t,arg| 
    BatchNotification::PushNotification.assign_batch_notification_for_identifier_1(arg[:identifier])
  end


  #All kids. Every Wednesday. Milestone summary report
  # rake assign_batch_notification_for_identifier_5[5] RAILS_ENV=production
  task :assign_batch_notification_for_identifier_5,[:identifier] => :environment do |t,arg| 
    BatchNotification::PushNotification.assign_batch_notification_for_identifier_5(arg[:identifier])
  end

  #Prepare list of milestones achieved in the last 30 days. Order by achievement date; recent first.
  # No pictures uploaded. This notification not sent before.
  # rake assign_batch_notification_for_identifier_3[3] RAILS_ENV=production
  task :assign_batch_notification_for_identifier_3,[:identifier] => :environment do |t,arg| 
    BatchNotification::Tools::Milestones.assign_batch_notification_for_identifier_3(arg[:identifier])
  end


  #For a given user: given child: milestone tool active: prepare list of unaccomplished milestones that have no information from user + current day more than 'UL+N' days.
  # Sort by important first
  # rake assign_batch_notification_for_identifier_2[2] RAILS_ENV=production
  task :assign_batch_notification_for_identifier_2,[:identifier] => :environment do |t,arg| 
    BatchNotification::Tools::Milestones.assign_batch_notification_for_identifier_2(arg[:identifier])
  end
 
 # All adults of a given child: 
 # Case 1: If notification not sent before + Status is planned + (2 weeks =< Due date - current date =< 4 weeks) 
 # Case 2: If notification not sent before + Status is planned + (1.days =< Due date - current date =< 1 weeks) 
 # Case 3: If notification not sent before + Status is estimated + (2 weeks =< Due date - current date =< 4 weeks) 
 # Case 4: If notification not sent before + Status is estimated + (1.days =< Due date - current date =< 1.weeks)
 # rake assign_batch_notification_for_identifier_7[7] RAILS_ENV=production
  task :assign_batch_notification_for_identifier_7,[:identifier] => :environment do |t,arg| 
    BatchNotification::Tools::Immunisations.assign_batch_notification_for_identifier_7(arg[:identifier])
  end

  # (N:overdue immunisations > 0) AND (not sent in last 14 days) AND (sign up date > 10 days ago )
  task :assign_batch_notification_for_identifier_8,[:identifier] => :environment do |t,arg| 
    BatchNotification::Tools::Immunisations.assign_batch_notification_for_identifier_8(arg[:identifier])
  end

  #Not sent in last 60 days + sign up date > 4 days
  task :assign_batch_notification_for_identifier_9,[:identifier] => :environment do |t,arg| 
    BatchNotification::Tools::Milestones.assign_batch_notification_for_identifier_9(arg[:identifier])
  end


  #Not sent in last 60 days + sign up date > 2 days
  task :assign_batch_notification_for_identifier_10,[:identifier] => :environment do |t,arg| 
    BatchNotification::Tools::Measurement.assign_batch_notification_for_identifier_10(arg[:identifier])
  end

# Case: If Child age =< 8weeks AND N: no of days since last measurement record > 8 days
#   8weeks < Child age =< 24months AND N: no of days since last measurement record > 35 days 
#   24months < Child age =< 6 years AND N: no of days since last measurement record > 190 days
#   Child age > 6years  AND N: no of days since last measurement record > 380 days 
  task :assign_batch_notification_for_identifier_12,[:identifier] => :environment do |t,arg| 
    BatchNotification::Tools::Measurement.assign_batch_notification_for_identifier_12(arg[:identifier])
  end
  
  #Check for each child: If at least one measurement record exist + subscription package is 'Free' for the family + same notification has not been sent in the last 2 weeks.
  #message:Z score is WHO's tool to assess normal growth in height, weight and BMI. Check <child name>'s Z score based growth assessment
  task :assign_batch_notification_for_identifier_14,[:identifier] => :environment do |t,arg| 
    BatchNotification::Tools::Measurement.assign_batch_notification_for_identifier_14(arg[:identifier])
  end

  #All children + (tooth chart not active) + Age less than 10 years
  #message:I think <Child name> may have grown M teeth by now. Let's try my new 'Dental health' tool to track eruption and shedding of <Child name>'s teeth
  # rake assign_batch_notification_for_identifier_15[15] RAILS_ENV=production
  task :assign_batch_notification_for_identifier_15,[:identifier] => :environment do |t,arg| 
    BatchNotification::Tools::ToothChart.assign_batch_notification_for_identifier_15(arg[:identifier])
  end


  # All children 
  # message: Check out our new tools/features that will support <child name>'s growth journey. Dental Health, Z score, Check up planner, Health report to name a few.
  # rake assign_batch_notification_for_identifier_16[16] RAILS_ENV=production
  task :assign_batch_notification_for_identifier_16,[:identifier] => :environment do |t,arg| 
    BatchNotification::GeneralNotification.assign_batch_notification_for_identifier_16(arg[:identifier])
  end



  # All children. monthly or yearly birthday on the current date
  # message: 1) Nurturey wishes ' + member.first_name + ' a very happy N month birthday. Have a great one! N= one, two, three, four, five, six, seven, eight, nine, ten, eleven 
  #          2) Nurturey wishes ' + member.first_name + ' a very happy N year birthday. Have a great one! N= first, second, third, fourth, fifth, sixth, seventh, eighth, ninth, tenth
  # rake assign_batch_notification_for_identifier_19[19] RAILS_ENV=production
  task :assign_batch_notification_for_identifier_19,[:identifier] => :environment do |t,arg| 
    BatchNotification::Tools::Timeline.assign_batch_notification_for_identifier_19(arg[:identifier])
  end

  # FAq Question message
  task :assign_batch_notification_for_identifier_23,[:identifier] => :environment do |t,arg| 
    BatchNotification::Tools::Faq.assign_batch_notification_for_identifier_23(arg[:identifier])
  end


 # No subscription
  task :assign_batch_notification_for_identifier_24,[:identifier] => :environment do |t,arg| 
    BatchNotification::Tools::Subscription.assign_batch_notification_for_identifier_24(arg[:identifier])
  end

# No subscription = identifier 24 notified + 10 days and  last shown > 5 launches ago OR > 10 days ago
  task :assign_batch_notification_for_identifier_25,[:identifier] => :environment do |t,arg| 
    BatchNotification::Tools::Subscription.assign_batch_notification_for_identifier_25(arg[:identifier])
  end

  # Only for Alexa user
  #  Track <name>’s mental maths experience with Nurturey App dashboard
  # rake assign_batch_notification_for_identifier_26[26] RAILS_ENV=production
  task :assign_batch_notification_for_identifier_26,[:identifier] => :environment do |t,arg| 
    BatchNotification::Tools::MentalMaths.assign_batch_notification_for_identifier_26(arg[:identifier])
  end

  # Only for Non Alexa user
  #  Track <name>’s mental maths experience with Nurturey App dashboard
  # rake assign_batch_notification_for_identifier_28[28] RAILS_ENV=production
  task :assign_batch_notification_for_identifier_28,[:identifier] => :environment do |t,arg| 
    BatchNotification::Tools::MentalMaths.assign_batch_notification_for_identifier_28(arg[:identifier])
  end
  
  # Start of pregnancy week
  # Pregnancy weekly summary
  # rake assign_batch_notification_for_identifier_30[30] RAILS_ENV=production
  task :assign_batch_notification_for_identifier_30,[:identifier] => :environment do |t,arg| 
    BatchNotification::GeneralNotification.assign_batch_notification_for_identifier_30(arg[:identifier])
  end


  # Only for  Alexa user
  #  Triggered on the last day of every week(on monday)
  #  Alexa monthly summary report
  # rake assign_batch_notification_for_identifier_28[28] RAILS_ENV=production
  task :assign_batch_notification_for_identifier_32,[:identifier] => :environment do |t,arg| 
    if Date.today.strftime("%A").downcase == "monday"
      BatchNotification::Tools::MentalMaths.assign_batch_notification_for_identifier_32(arg[:identifier])
    end
  end

   # Pregnancy Due
  # Show how to use tools post pregnancy
  # rake assign_batch_notification_for_identifier_33[33] RAILS_ENV=production
  task :assign_batch_notification_for_identifier_33,[:identifier] => :environment do |t,arg| 
    BatchNotification::GeneralNotification.assign_batch_notification_for_identifier_33(arg[:identifier])
  end

  # cvtme event schedule
  # member_name is eligible to take the Color Vision test!
  # rake assign_batch_notification_for_identifier_34[34] RAILS_ENV=production
  task :assign_batch_notification_for_identifier_34,[:identifier] => :environment do |t,arg| 
    BatchNotification::Tools::VisionHealth.assign_batch_notification_for_identifier_34(arg[:identifier])
  end

  # Design Contest email
  # "Enter Nurturey's Design A Smile and Win Our Prize!"
  # rake assign_batch_notification_for_identifier_35[35] RAILS_ENV=production
  task :assign_batch_notification_for_identifier_35,[:identifier] => :environment do |t,arg| 
    BatchNotification::GeneralNotification.assign_batch_notification_for_identifier_35(arg[:identifier])
  end
  

  # NHS Content message
  # rake assign_batch_notification_for_identifier_36[36] RAILS_ENV=production
  task :assign_batch_notification_for_identifier_36,[:identifier] => :environment do |t,arg| 
    BatchNotification::Nhs::SyndicatedContent.assign_batch_notification_for_identifier_36(arg[:identifier])
  end


  # Pregnancy weekly summary 
  # rake assign_batch_notification_for_identifier_38[38] RAILS_ENV=production
  task :assign_batch_notification_for_identifier_38,[:identifier] => :environment do |t,arg| 
    BatchNotification::GeneralNotification.assign_batch_notification_for_identifier_38(arg[:identifier])
  end


  # Assign pointer user notification
  # rake assign_batch_notification_for_identifier_pointer["pointers"] RAILS_ENV=production
  task :assign_batch_notification_for_identifier_pointer,[:identifier] => :environment do |t,arg| 
    BatchNotification::Tools::Pointers.assign_batch_notification_for_identifier_pointer(arg[:identifier])
  end
  