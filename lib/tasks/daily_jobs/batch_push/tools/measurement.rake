# rake assign_measurement_push_not_updated_since RAILS_ENV=production
  task :assign_measurement_push_not_updated_since,[:identifier] => :environment do |t,arg|
    if identifier.to_i > 0
      BatchNotification::Tools::Measurement.assign_measurement_push(arg[:identifier])
    else
      BatchNotification::Tools::Measurement.assign_measurement_push
    end
  end


# rake send_batch_measurement_push["UK"] RAILS_ENV=production
  task :send_batch_measurement_push,[:country] => :environment do |t,arg| 
    BatchNotification::Tools::Measurement.send_batch_measurement_push(arg[:country])
  end