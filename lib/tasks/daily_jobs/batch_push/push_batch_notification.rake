# rake send_push_for_batch_no[19,"UK"] RAILS_ENV=production
  task :send_push_for_batch_no,[:batch_no,:country_code] => :environment do |t,arg| 
    UserNotification.send_push_notification_by_batch(arg[:batch_no].to_i, arg[:country_code])
  end