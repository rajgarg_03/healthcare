  #rake assign_scenario_for_faq_question_to_member
  #rake assign_scenario_for_faq_question_to_member RAILS_ENV=production 
  #TODO: rename job name :assign_scenario_to_member
  task :assign_scenario_for_faq_question_to_member => :environment do
    system_scenario_list = ::System::Scenario::ScenarioDetails.where(status:"Active").entries
    # Fetch all scenaro linked group by custom id
    system_scenario_group_by_custom_id =  System::Scenario::ScenarioLinkedToTool.group_by_system_scenario_detail_custom_id
    
    ChildMember.where(:last_activity_done_on.gte=>Date.today - 12.month).batch_size(20).no_timeout.each do |member|
      ::Child::Scenario.assign_scenario(member.id,{:member_obj=>member,:system_scenario_list=>system_scenario_list,:system_scenario_group_by_custom_id=>system_scenario_group_by_custom_id}) 
      ::Child::Scenario.where(member_id:member.id, :updated_at.lt=>Date.today, :shortlisted=> false).delete_all
      # Delete pointer if scenario is not relevent
      ArticleRef.where(member_id:member.id, :updated_at.lt=>Date.today, :shortlisted=> false).delete_all
    end
    
    ::Child::Scenario.where(:status=>"deleted", :ref_expire_date.lt=> Date.today).delete_all
  end
