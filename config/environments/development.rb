
# require 'ruby-debug'
#require 'new_relic/rack/developer_mode'
# Debugger.start
Nurturey::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false 

#config.middleware.use NewRelic::Rack::DeveloperMode
   #Rails.application.routes.default_url_options[:host] = 'http://localhost:3000'
  # Log error messages when you accidentally call methods on nil.
  config.whiny_nils = true

  # Show full error reports and disable caching
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Print deprecation notices to the Rails logger
  config.active_support.deprecation = :log

  # Only use best-standards-support built into browsers
  config.action_dispatch.best_standards_support = :builtin
  
  # Do not compress assets
  #config.assets.compress = false

  # Expands the lines which load the assets
  config.assets.debug = true
  #config.log_level = :error

# ActionMailer Config
  #config.action_mailer.default_url_options = { :host => 'http://localhost:3000' }
  # config.action_mailer.delivery_method = :ses
  config.action_mailer.delivery_method = :letter_opener
  config.action_mailer.raise_delivery_errors = false
  config.action_mailer.default :charset => "utf-8"

  Paperclip.options[:command_path] = "/usr/local/bin/"

  ActionMailer::Base.default :from => 'Nurturey Support <support@nurturey.com>'
  
  # To include current timestamp in log before each log message
  config.log_tags = [ lambda {|r| DateTime.now } ]
  # Delayed job 
  Delayed::Worker.logger = Logger.new("log/delayed_job.log", 10, 1040857600)

  if caller.last =~ /script\/delayed_job/ or (File.basename($0) == "rake" and ARGV[0] =~ /jobs\:work/)
    Moped.logger = Delayed::Worker.logger
  end
end
