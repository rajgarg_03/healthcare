TimeDifference.class_eval do

  def humanize
    diff_parts = []
    in_general.each do |part,quantity|
      next if quantity <= 0
      part = part.to_s

      if quantity <= 1
        part = part.singularize
      end

      diff_parts << "#{quantity} #{part}"
    end

    last_part = diff_parts[0]

    return last_part
    # if diff_parts.empty?
    #   return last_part
    # else
    #   return [diff_parts.join(', '), last_part].join(' and ')
    # end
  end

end