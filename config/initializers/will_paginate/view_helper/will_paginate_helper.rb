require 'will_paginate/array'
module WillPaginate
  module ViewHelpers
    # This class does the heavy lifting of actually building the pagination
    # links. It is used by +will_paginate+ helper internally.
    class LinkRenderer < LinkRendererBase

      private

      def rel_value(page)
        case page
          when @collection.current_page - 1; 'prev' + (page == 1 ? ' start facebox' : ' facebox')
          when @collection.current_page + 1; 'next facebox'
          when 1; 'start facebox'
        end
      end

    end
  end
end
