# Geocoder.configure(
#   :timeout      => 3.5,           # geocoding service timeout (secs)
# )


Geocoder.configure(ip_lookup: :maxmind_local, maxmind_local: {file: File.join("#{Rails.root}/lib", 'GeoLiteCity.dat')})