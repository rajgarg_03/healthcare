ActionMailer::Base.add_delivery_method :ses, AWS::SES::Base,
  :server  => APP_CONFIG["ses_server"],
  :access_key_id     => APP_CONFIG["ses_access_key_id"],
  :secret_access_key => APP_CONFIG["ses_secret_access_key"]