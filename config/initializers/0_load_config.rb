APP_CONFIG_str = YAML.load_file("#{Rails.root}/../settings.yml")[Rails.env]
APP_CONFIG = APP_CONFIG_str.with_indifferent_access
APP_CONFIG[:handhold_basic_setup_run_after_count] = 3
Koala.config.api_version = APP_CONFIG["FB_api_version"]
require 'ext/date'
require 'ext/candy_check_changes'
require 'rack_snoop'
require 'fuzzystringmatch'
