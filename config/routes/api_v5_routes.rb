# Nurturey::Application.routes.draw do
module ApiV5Routes
  def self.extended(router)
    router.instance_exec do
      namespace :api do
        namespace :v5 do
          get 'unread_objects_count' => 'notifications#unread_count'

          resources :gp_soc do
            collection do
              get :search_gp_practices
            end  
          end
          namespace :clinic do 
            resources :book_appointment do 
              collection do 
                post :create_appointment
                delete :cancel_appointment
                put :update_appointment
                get :available_appointments
                get :list_user_appointment
                get :get_doctor_list
                get :get_appointment_purpose_list
              end
            end 

            resources :message do 
              collection do 
                get :list_messages
                get :get_message_detail
                delete :delete_message
                post :send_message
                get :get_recipients
                put :update_message_read_status
              end
            end 

            resources :prescribing do 
              collection do 
                get :list_medication_courses
                post :request_prescription
                get :get_prescription_requests
                delete :cancel_prescription
              end
            end

            resources :pharmacy do 
              collection do 
                get :get_user_nominated_pharmacy
                get :list_pharmacies
                put :update_pharmacy_nomination
                delete :delete_pharmacy_nomination
              end
            end 

          end

          
          resources :clinic do
            collection do
              post :setup_clinic
              get :get_clinic_list
              get :get_uae_clinic_list
              post :authenticate_by_clinic
              post :link_proxy_user
              get :get_clinic_linkable_status
              put :delink_with_clinic
              get :get_clinic_services
              post :sent_otp_for_clinic_access
              post :verify_otp_for_clinic_access
              get :get_clinical_data
            end

          end

          resources :nhs do
            collection do
              get :search
              get :news
              get :media
              get :organisations_search
              get :authorize_redirect
              get :user_info
              get :token
              get :authenticate_user
              get :initiate_user_authentication
              get :get_syndicated_content
            end  
          end

          resources :notifications do
            collection do
              put  :mark_read
              get  :log_event
              post :log_event
              put  :mark_user_notification_responded
            end  
          end
          resources :measurements do
            collection do
              get :get_details
            end
          end
          resources :subscription do
            collection do
              get :get_available_plans
              get :subscriptions_tool_list
              post :update_subscription_from_apple_server
              post :update_subscription_from_play_store
              put :update_subscription_from_play_store
              get :get_tool_list
              get :verify_subscription
              put :create_or_update_subscription
              delete :unsubscribe_from_subscription
              put :update_subscription_receipt
              post :update_subscription_receipt
            end  
          end
          namespace :pregnancy do
            
            namespace :health_card do
              resources :hemoglobin 
              resources :blood_pressure 
              resources :measurement do
                collection do
                  get :get_detail
                end
              end


            end

            namespace :kick_counter do
              resources :kick_count do
                collection do
                  get :get_detail
                  post :add_kick_count
                  put :update_kick_count
                  post :add_kick_counter_schedule
                  put :update_kick_counter_schedule
                  put :update_upcoming_kick_counter_schedule
                  delete :delete_kick_count
                end
              end 
            end

          end 
          namespace :nhs do
            resources :syndicated_content do
              collection do 
                get :get_filter_content
                get :get_category_list
                get :get_topic_list_for_category
                get :get_content
              end
            end
          end
          namespace :child do
            namespace :tooth_chart do
              resources :chart_manager do
                collection do
                  get 'get_tooth_manager_data'
                  post 'add_child_tooth_detail'
                  get 'toothList'
                end
                member do
                  put 'update_child_tooth_detail'
                end
              end
            end
            resources :health_card do 
              collection do 
                put :update_blood_group
                get :list_allergies
              end

            end
            namespace :checkup_planner do
              resources :checkup_schedule do

                collection do 
                  get  :get_checkup_plans
                  get :get_system_tests
                  post :add_test_to_system
                  post :add_test_to_checkup_plan
                  post :add_schedule_for_member
                  post :add_checkup_plan
                  
                  put  :update_checkup_plan
                  delete :delete_checkup
                  post :delete_checkup_tests
                end
              end
            end

            namespace :vision_health do
              resources :color_vision_test_result do
                collection do 
                  get  :result
                  get :results
                  put :update_family_purchase_for_cvtme
                end
              end

              resources :event do
                collection do 
                  post :add_event
                end
              end

            end
            
            namespace :activity_planner do
              resources :activity do
                collection do 
                  get :list_all_activity
                  get :list_participant
                  post :add_activity
                  post :add_participant
                  delete :delete_participant
                  delete :delete_activity
                  put :update_activity
                end
              end
            end
            namespace :alexa_quiz do
              resources :quiz_details do
                collection do 
                  post :update_quiz_level
                end
              end
            end

            namespace :mental_math, only: [:none] do
              resources :quiz_details do
                collection do 
                  get  :complete_summary
                  get  :daily_summary
                  post :add_quiz_details
                  put  :mental_math_banner_skipped
                end
              end
            end

            namespace :health_card do
              resources :allergy do 
                collection do 
                  get :list_all_allergies
                  get :list_member_allergies
                  post :add_allergy_to_allergy_manager
                  post :add_allergy_to_member_list
                  put :update_allergy
                  delete :delete_allergy

                end
              end
              resources :allergy_symptom do
                collection do
                  get :list_all_allergy_symptom
                  post :add_symptom_to_allergy_symptom_manager
                  get :delete_symptom
                  post :add_symptom_to_allergy_symptom_manager
                end
              end

              resources :eye_chart, only: [] do
                collection do
                  get :list
                  post :create_eye_data
                  put :update_eye_data
                  delete :destroy_eye_data
                end
              end
              resources :skin_assessment do
                collection do 
                  delete :delete_skin_assessment
                  post :upload_image_for_skin_assessment
                  get :skin_assessment_details
                  get :get_list
                end
              end  
            end
          end

          resources :users , :except=>["create","show","index"] do 
            collection do 
              get :sign_in
              get :push_msg
              get :user_sign_out
              get :secure_sign_in
              post :secure_sign_in
              post :reauthenticate_user
              get :reauthenticate_user
              get :secure_alexa_sign_in
              post :secure_alexa_sign_in
              post :sign_in
              get :basic_set_up_evaluator_for_user
              get :child_members
              post :sign_in_via_facebook
              get :user_dashboard
              get :userDashboard
              get :dashboard
              get :recent_items
              get :user_events
              post :upload_image_multipart          
              post :upload_image
              post :upload_cover_image
              get :get_cover_image
              get :user_info
              put :dismiss_nutrient
              get :delete_account
              get :get_handhold_actions
              put :skipped_details
              post :skipped_details
              get :get_summary_screen
              get :resend_confirmation_link
              get :test_push_notification
              get :test_rake_task
              get :test_action_notification
              get :test_birthday_mail_deeplink
              get :set_default_cover_image
              get :set_version_controller
              post :enable_push_notification
              post :skip_push_screen
              get :send_scheduled_birthday_wish_push
              get :test_birthday_instant_push
              get :test_deeplink
              post :test_deeplink
              get :get_face_info
              post :get_face_info
              get :other_purchases
            end
          end
          
          devise_for :users ,:controllers => {:passwords => "passwords", :sessions => "sessions",:registrations => 'api/v5/registrations',:confirmations=>'confirmations',:omniauth_callbacks => "omniauth_callbacks"} do
            resources :passwords do
              post :update
              collection do  
                put :secure_update
              end
            end
            resources :registrations do 
              collection do 
                post :secure_signup
              end
            end
            resources :sessions do
              get :sign_in
            end
          end
          resources :system_milestones do
            member do
              get :hh_milestones
            end  
          end  
          resources :milestones do
            member do
              get :milestones_for_graph
              post :upload_image
            end
            collection do
              put :add_multiple
              put :add_milestone_from_handhold
              get :member_milestone_list
              post :member_milestone_list
              get :memberMilestoneList
              post :memberMilestoneList
              
              get :milestone_detail
              get :get_milestone_detail
            end  
          end  
          resources :nutrients do
            member do
              get :acitvate
              delete :deactivate
              get :nutrients_status
              put :update_nutrient_status
            end
            collection do
              get :categories
              get :change_category          
            end
          end

          resources :families do
            resources :members, only: [] do
              resources :nutrients, only: :index
              resources :timeline, only: :index
              resources :milestones, only: :index
              resources :calendar, only: :index
              resources :health, only: :index
              resources :documents, only: :index
              resources :immunisation, only: :index
            end  
            collection do
              get :search_family
              get :select_role
              post :save_role
              post :dont_want_add_spouse
              get :find_member
            end
            member do
              post :join_this_family
              post :dont_want_add_spouse
              get :family_members  
              #get :join_this_family  # Temporary because form is not working with ajax      
            end   
            resources :member do
              collection do
                get :new_child
                post :create_child        
                get :new_elder
                post :create_elder 
              end
            end  
          end
          resources :documents do
            collection do
              post :save_documents
              get :search
              get :types
              get :get_clinic_document_list
              get :clinic_document_content          
            end  
            member do
              get :get_doc_detail    
              delete :delete_attachment
              post :upload_file
              # get :download
            end
          end  
          resources :timeline do
            collection do
              post :create_timeline_for_multi_member
              post :user_timeline
              post :index
              get :get_pre_defined_timeline
              post :create_timeline
              put :update_timeline
              delete :delete_timeline
              get :get_timeline_detail
            end  
            member do
              post :upload_image
              get :get_hh_system_timelines
              post :add_hh_system_timelines
            end  
          end
          resources :invitations do
            collection do
              put 'update_status/:status', :to => "invitations#update_status"
              get 'update_status/:status', :to => "invitations#update_status"
              get 'invite_friend_to_nurturey'
            end  
          end    
          # extara for test
          resources :health do
            collection do 
              get :get_detail
              get :edit
              get :new_health_record
              get :get_health_detail
              get :get_zscore_detail
              post :create_record
              post :create_record_from_alexa
              post :update_record
              get :delete_parameter
              get :health_info
              get :close_info_pop
            end
          end
          resources :immunisation do 
            collection do
              # get :new_vaccination
              get :new_jab
              post :save_jab
              get :edit_jab
              put :update_jab
              get :delete_jab
              get :immunisation_info
              get :close_info_pop
              get :upcoming_jabs
              get :jab_detail
              get :jabs_month_wise
            end
          end
          resources :score  
          resources :vaccination do
            collection do
              get :add_vaccin
              get :delete_vaccin
              get :edit_vaccin
              put :update_vaccination
            end
          end
          resources :timeline do
            collection do
              get :pics
              get :media
              get :automated_timeline
              post :save_auto_time
              get :skip_automated_timeline
              get :edit
              post :create_timeline
              put :update_timeline
              get :delete_timeline
              get :remind_later
              get :delete_timeline_pic
              get :timeline_info
            end
            member do
              get :facebook_preview
              post :send_to_facebook
            end  
          end
          resources :manage_family
        
          match "/calendar/dashboard" => "users#dashboard"
          match '/families/get_family_data'        => 'families#get_family_data', :as => :get_family_data 
          match '/families/join_family'        => 'families#join_family', :as => :join_family 
          match '/families/save_joined_family'        => 'families#save_joined_family', :as => :save_joined_family
       
          resources :calendar do
            collection do
              get :get_data
              get :get_parent_data
              post :create_event
              put :event_update
              get :edit_event
              get :event_delete
              get :get_event_datail
              # get  :timeline
              get :calendar_info
            end
          end
          post 'profile/delete_child_interest'
          resources :milestones do
            collection do
              put :add_multiple
              get :add_description
              get :change_category
              get :display_later
              get :milestones_info
            end  
            member do
              put :save_and_next
              get :skip
              delete :remove_picture
              get :display_picture
            end  
          end  

          resources :nutrients do
            collection do
              get :change_category
            end  
            member do
              get :acitvate
              delete :deactivate
            end
          end

          resources :documents do
            collection do
              post :save_documents
              get :search
              get :documents_info
            end  
            member do
              delete :delete_attachment
              get :download
            end  
          end  
          resources :notifications do
            member do
              get :view
            end  
            collection do
              get :check_all
            end  
          end
          resources :invitations do
            collection do
              get :accept
              get :decline
              get :add_invitee
              get :remove
              get :edit_email
              get :change_email
              post :send_invites
              get :view_invite
              get 'invite_friend_to_nurturey'
            end  
          end  
          resources :invitations
          resources :pregnancy do
            collection do
              post :create_preg
              get :get_mother_list
              get :get_preg_info
              delete :delete_preg
              put :update_preg_child
              post :create_system_timeline
              get :get_default_timelines
              put :get_default_timelines
              put :mark_preg_complete
              put :update_preg
              get :health_card
              get :get_pregnancy_week_detail
            end
          end
        # resources 'parental_test', :path => :medical_event  do
          resources :medical_event, :path => 'prenatal_test'  do
            collection do
              get :get_medical_event_list
              post :add_recommended_test
              get :get_recommended_medical_event
              delete :delete_component_from_medical_event
              delete :delete_component_from_sys
              post :create_new_component
              post :add_component_to_medical_event
              get :get_sys_components
              get :get_medical_event_detail
              delete :delete_parental_test
            end
            member do
              post :upload_image
            end  
          end
          resources :member do
            member do
              get :edit_child
              put :update_child
              get :edit_elder
              put :update_elder
              put :update_timezone
              delete :delete_child
              delete :delete_elder_invitation
              delete :remove_elder
              get :verify_elder_for_family
              get :verify_elder      
              get :resend_invitation
              get :display_picture
              get :edit_picture
              get :tool_activation
              get :hide_invited_user_block
            end
            collection do
              # get :invite_friend
              post :set_metric_system
              get :edit_current_member
              get :trigger_tour_step
              post :update_current_member
              put :update_quiz_level
            end
          end
          resources :article_ref do
            collection do
              delete :delete_article_ref
              put :add_to_favorite
              put :remind_me_later
              put :update_art_ref
              get :art_ref_detail
              get :recommended_art_ref
              post :save_pointer_reference_link_clicked_detail
            end
          end

          namespace :user do
            resources :faq, only: [:index] do
              collection do
                put :user_feedback
                get :get_question_detail
                get :get_topic_list
                get :get_subtopic_list
              end
            end
          end
        end
      end
    end
  end
end