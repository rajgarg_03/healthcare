# Nurturey::Application.routes.draw do
module ApiV3Routes
  def self.extended(router)
    router.instance_exec do
      namespace :api do
        namespace :v3 do
          get 'unread_objects_count' => 'notifications#unread_count'
          resources :notifications do
            collection do
              put :mark_read
            end  
          end
          # resources :subscription do
          #   collection do
          #     get :get_available_subscriptions
          #     get :subscriptions_tool_list
          #     get :verify_subscription
          #     put :create_or_update_subscription
          #     delete :unsubscribe_from_subscription
          #   end  
          # end    
          # resources :pregbp 
          # resources :preg_hemo_glob 
          # resources :preg_health do
          #   collection do
          #     get :get_detail
          #   end
          # end 
          resources :users , :except=>["create","show","index"] do 
            collection do 
              get :sign_in
              get :push_msg
              get :user_sign_out
              get :secure_sign_in
              post :secure_sign_in
              post :sign_in
              get :basic_set_up_evaluator_for_user
              get :child_members
              post :sign_in_via_facebook
              get :user_dashboard
              get :recent_items
              get :user_events
              post :upload_image_multipart          
              post :upload_image
              post :upload_cover_image
              get :get_cover_image
              get :user_info
              put :dismiss_nutrient
              get :delete_account
              get :get_handhold_actions
              put :skipped_details
              post :skipped_details
              get :get_summary_screen
              get :resend_confirmation_link
              get :test_action_notification
              get :set_default_cover_image
              get :set_version_controller
              post :enable_push_notification
              post :skip_push_screen
            end
          end
          
          devise_for :users ,:controllers => {:passwords => "passwords", :sessions => "sessions",:registrations => 'api/v3/registrations',:confirmations=>'confirmations',:omniauth_callbacks => "omniauth_callbacks"} do
            resources :passwords do
              post :update
              collection do  
                put :secure_update
              end
            end
            resources :registrations do 
              collection do 
                post :secure_signup
              end
            end
            resources :sessions do
              get :sign_in
            end
          end
          resources :system_milestones do
            member do
              get :hh_milestones
            end  
          end  
          resources :milestones do
            member do
              get :milestones_for_graph
              post :upload_image
            end
            collection do
              put :add_multiple
              get :get_milestone_detail
            end  
          end  
          resources :nutrients do
            member do
              get :acitvate
              delete :deactivate
              get :nutrients_status
              put :update_nutrient_status
            end
            collection do
              get :categories
              get :change_category          
            end
          end

          resources :families do
            resources :members, only: [] do
              resources :nutrients, only: :index
              resources :timeline, only: :index
              resources :milestones, only: :index
              resources :calendar, only: :index
              resources :health, only: :index
              resources :documents, only: :index
              resources :immunisation, only: :index
            end  
            collection do
              get :search_family
              get :select_role
              post :save_role
              post :dont_want_add_spouse
            end  
            member do
              post :join_this_family
              post :dont_want_add_spouse
              get :family_members  
              #get :join_this_family  # Temporary because form is not working with ajax      
            end   
            resources :member do
              collection do
                post :create_child        
                post :create_elder 
              end
            end  
          end
          resources :documents do
            collection do
              post :save_documents
              get :search
              get :types          
            end  
            member do
              get :get_doc_detail    
              delete :delete_attachment
              post :upload_file
              # get :download
            end
          end  
          resources :timeline do
            collection do
              post :create_timeline_for_multi_member
              post :user_timeline
              post :index
              get :get_pre_defined_timeline
              post :create_timeline
              put :update_timeline
              delete :delete_timeline
              get :get_timeline_detail
            end  
            member do
              post :upload_image
              get :get_hh_system_timelines
              post :add_hh_system_timelines
            end  
          end
          resources :invitations do
            collection do
              put 'update_status/:status', :to => "invitations#update_status"
              get 'update_status/:status', :to => "invitations#update_status"
            end  
          end    
          # extara for test
          resources :health do
            collection do 
              get :get_detail
              get :edit
              get :get_health_detail
              post :create_record
              post :update_record
              get :delete_parameter
              get :health_info
              get :close_info_pop
            end
          end
          resources :immunisation do 
            collection do
              # get :new_vaccination
              get :new_jab
              post :save_jab
              get :edit_jab
              put :update_jab
              get :delete_jab
              get :immunisation_info
              get :close_info_pop
              get :upcoming_jabs
              get :jab_detail
            end
          end
          resources :score  
          resources :vaccination do
            collection do
              get :add_vaccin
              get :delete_vaccin
              get :edit_vaccin
              put :update_vaccination
            end
          end
          resources :timeline do
            collection do
              get :pics
              get :media
              get :automated_timeline
              post :save_auto_time
              get :skip_automated_timeline
              get :edit
              post :create_timeline
              put :update_timeline
              get :delete_timeline
              get :remind_later
              get :delete_timeline_pic
              get :timeline_info
            end
            member do
              get :facebook_preview
              post :send_to_facebook
            end  
          end
          resources :manage_family
        
          match "/calendar/dashboard" => "users#dashboard"
          match '/families/get_family_data'        => 'families#get_family_data', :as => :get_family_data 
          match '/families/join_family'        => 'families#join_family', :as => :join_family 
          match '/families/save_joined_family'        => 'families#save_joined_family', :as => :save_joined_family
       
          resources :calendar do
            collection do
              get :get_data
              get :get_parent_data
              post :create_event
              put :event_update
              get :edit_event
              get :event_delete
              get :get_event_datail
              # get  :timeline
              get :calendar_info
            end
          end
          post 'profile/delete_child_interest'
          resources :milestones do
            collection do
              put :add_multiple
              get :add_description
              get :change_category
              get :display_later
              get :milestones_info
            end  
            member do
              put :save_and_next
              get :skip
              delete :remove_picture
              get :display_picture
            end  
          end  

          resources :nutrients do
            collection do
              get :change_category
            end  
            member do
              get :acitvate
              delete :deactivate
            end
          end

          resources :documents do
            collection do
              post :save_documents
              get :search
              get :documents_info
            end  
            member do
              delete :delete_attachment
              get :download
            end  
          end  
          resources :notifications do
            member do
              get :view
            end  
            collection do
              get :check_all
            end  
          end
          resources :invitations do
            collection do
              get :accept
              get :decline
              get :add_invitee
              get :remove
              get :edit_email
              get :change_email
              post :send_invites
              get :view_invite
            end  
          end  
          resources :invitations
          resources :pregnancy do
            collection do
              post :create_preg
              get :get_mother_list
              get :get_preg_info
              delete :delete_preg
              put :update_preg_child
              post :create_system_timeline
              get :get_default_timelines
              put :get_default_timelines
              put :mark_preg_complete
              put :update_preg
              get :health_card
            end
          end
        # resources 'parental_test', :path => :medical_event  do
          resources :medical_event, :path => 'prenatal_test'  do
            collection do
              get :get_medical_event_list
              post :add_recommended_test
              get :get_recommended_medical_event
              delete :delete_component_from_medical_event
              delete :delete_component_from_sys
              post :create_new_component
              post :add_component_to_medical_event
              get :get_sys_components
              get :get_medical_event_detail
              delete :delete_parental_test
            end
            member do
              post :upload_image
            end  
          end
          resources :member do
            member do
              get :edit_child
              put :update_child
              get :edit_elder
              put :update_elder
              put :update_timezone
              delete :delete_child
              delete :delete_elder_invitation
              delete :remove_elder
              get :verify_elder_for_family
              get :verify_elder      
              get :resend_invitation
              get :display_picture
              get :edit_picture
              get :tool_activation
              get :hide_invited_user_block
            end
            collection do
              # get :invite_friend
              post :set_metric_system
              get :edit_current_member
              get :trigger_tour_step
              post :update_current_member
            end
          end
          resources :article_ref do
            collection do
              delete :delete_article_ref
              put :add_to_favorite
              put :remind_me_later
              put :update_art_ref
              get :art_ref_detail
              get :recommended_art_ref
            end
          end
        end
      end
    end
  end
end