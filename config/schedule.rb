# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

# every 12.hours do
# every '0 3,15 * * *' do	
#   # runner "MyModel.task_to_run_at_four_thirty_in_the_morning"
#   # rake "nurturey:send_welcome_email"
# end

# # assign references to all member
# every 1.day, :at => '6:30 am' do
# #  rake "assign_references_to_member"
# end

# every 1.day, :at => '6:30 am' do
#   # rake "nurturey:send_birthday_email"
# end

# every 1.day, :at => '6:30 am' do
#   # rake "nurturey:send_remind_email"
# end

# every 1.day, :at => '6:30 am' do
#   # rake "nurturey:within_30day_jab_reminder"
# end

# every :thursday, :at => '6:00 am' do
#   # rake "reports:send_report"
# end	
