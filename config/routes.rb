
# require Rails.root.join('config/routes/api_v3_routes.rb')
# require Rails.root.join('config/routes/api_v4_routes.rb')
# require Rails.root.join('config/routes/api_v5_routes.rb')
# require Rails.root.join('config/routes/api_v6_routes.rb')

Nurturey::Application.routes.draw do
  extend ApiV6Routes
  extend ApiV5Routes
  extend ApiV4Routes
  extend ApiV3Routes
  namespace :api do
    
    namespace :v1 do
      get 'unread_objects_count' => 'notifications#unread_count'
      resources :notifications do
        collection do
          put :mark_read
        end  
      end    
      resources :users do 
        collection do 
          get :sign_in
          post :sign_in
          get :basic_set_up_evaluator_for_user
          get :child_members
          post :sign_in_via_facebook
          get :user_dashboard
          get :recent_items
          get :user_events
          post :upload_image
          post :upload_cover_image
          get :get_cover_image
          get :user_info
          put :dismiss_nutrient
          get :delete_account
          get :get_handhold_actions
          put :skipped_details
          post :skipped_details
          get :get_summary_screen
          get :nhs_authentication_completed
        end
      end
      resources :system_milestones do
        member do
          get :hh_milestones
        end  
      end  
      resources :milestones do
        member do
          get :milestones_for_graph
          post :upload_image
        end
        collection do
          put :add_multiple
          get :get_milestone_detail
        end  
      end  
      resources :nutrients do
        member do
          get :acitvate
          delete :deactivate
          get :nutrients_status
          put :update_nutrient_status
        end
        collection do
          get :categories
          get :change_category          
        end
      end  
      
      resources :documents do
        collection do
          post :save_documents
          get :search
          get :types          
        end  
        member do
          get :get_doc_detail    
          delete :delete_attachment
          post :upload_file
          # get :download
        end
      end  
      resources :timeline do
        collection do
          post :create_timeline
          put :update_timeline
          delete :delete_timeline
          get :get_timeline_detail
        end  
        member do
          post :upload_image
          get :get_hh_system_timelines
          post :add_hh_system_timelines
        end  
      end
      resources :invitations do
        collection do
          put 'update_status/:status', :to => "invitations#update_status"
          get 'update_status/:status', :to => "invitations#update_status"
          get 'invite_friend_to_nurturey'
        end  
      end    
    end

    namespace :v2 do
      get 'unread_objects_count' => 'notifications#unread_count'
      resources :notifications do
        collection do
          put :mark_read
        end  
      end
      
      resources :users , :except=>["create","show","index"] do 
        collection do 
          get :sign_in
          get :push_msg
          get :user_sign_out
          post :sign_in
          get :secure_sign_in
          post :secure_sign_in
          get :basic_set_up_evaluator_for_user
          get :child_members
          post :sign_in_via_facebook
          get :user_dashboard
          get :recent_items
          get :user_events
          post :upload_image_multipart          
          post :upload_image
          post :upload_cover_image
          get :get_cover_image
          get :user_info
          put :dismiss_nutrient
          get :delete_account
          get :get_handhold_actions
          put :skipped_details
          post :skipped_details
          get :get_summary_screen
          get :resend_confirmation_link
          get :test_action_notification
          get :set_default_cover_image
          get :set_version_controller
          post :enable_push_notification
        end
      end
      
      devise_for :users ,:controllers => {:passwords => "passwords", :sessions => "sessions",:registrations => 'api/v2/registrations',:confirmations=>'confirmations',:omniauth_callbacks => "omniauth_callbacks"} do
        resources :passwords do
          post :update
          collection do  
            put :secure_update
          end    
        end
        resources :registrations do 
          collection do 
            post :secure_signup
          end
        end
        resources :sessions do
          get :sign_in
        end
      end
      resources :system_milestones do
        member do
          get :hh_milestones
        end  
      end  
      resources :milestones do
        member do
          get :milestones_for_graph
          post :upload_image
        end
        collection do
          put :add_multiple
          get :get_milestone_detail
        end  
      end  
      resources :nutrients do
        member do
          get :acitvate
          delete :deactivate
          get :nutrients_status
          put :update_nutrient_status
        end
        collection do
          get :categories
          get :change_category          
        end
      end

      resources :families do
        resources :members, only: [] do
          resources :nutrients, only: :index
          resources :timeline, only: :index
          resources :milestones, only: :index
          resources :calendar, only: :index
          resources :health, only: :index
          resources :documents, only: :index
          resources :immunisation, only: :index
        end  
        collection do
          get :search_family
          get :select_role
          post :save_role
        end  
        member do
          post :join_this_family
          get :family_members  
          #get :join_this_family  # Temporary because form is not working with ajax      
        end   
        resources :member do
          collection do
            get :new_child
            post :create_child        
            get :new_elder
            post :create_elder 
          end
        end  
      end

      resources :documents do
        collection do
          post :save_documents
          get :search
          get :types          
        end  
        member do
          get :get_doc_detail    
          delete :delete_attachment
          post :upload_file
          # get :download
        end
      end  
      resources :timeline do
        collection do
          post :create_timeline_for_multi_member
          post :user_timeline
          post :index
          get :get_pre_defined_timeline
          post :create_timeline
          put :update_timeline
          delete :delete_timeline
          get :get_timeline_detail
        end  
        member do
          post :upload_image
          get :get_hh_system_timelines
          post :add_hh_system_timelines
        end  
      end
      resources :invitations do
        collection do
          put 'update_status/:status', :to => "invitations#update_status"
          get 'update_status/:status', :to => "invitations#update_status"
          get 'invite_friend_to_nurturey'
        end  
      end    
     
      resources :health do
        collection do 
          get :get_detail
          get :edit
          get :new_health_record
          get :get_health_detail
          post :create_record
          post :update_record
          get :delete_parameter
          get :health_info
          get :close_info_pop
        end
      end

      resources :immunisation do 
        collection do
          # get :new_vaccination
          get :new_jab
          post :save_jab
          get :edit_jab
          put :update_jab
          get :delete_jab
          get :immunisation_info
          get :close_info_pop
          get :upcoming_jabs
          get :jab_detail
        end
      end

      resources :score  

      resources :vaccination do
        collection do
          get :add_vaccin
          get :delete_vaccin
          get :edit_vaccin
          put :update_vaccination
        end
      end

      resources :timeline do
        collection do
          get :pics
          get :media
          get :automated_timeline
          post :save_auto_time
          get :skip_automated_timeline
          get :edit
          post :create_timeline
          put :update_timeline
          get :delete_timeline
          get :remind_later
          get :delete_timeline_pic
          get :timeline_info
        end
        member do
          get :facebook_preview
          post :send_to_facebook
        end  
      end

      resources :manage_family

      match "/calendar/dashboard" => "users#dashboard"
      match '/families/get_family_data'        => 'families#get_family_data', :as => :get_family_data 
      match '/families/join_family'        => 'families#join_family', :as => :join_family 
      match '/families/save_joined_family'        => 'families#save_joined_family', :as => :save_joined_family
      resources :calendar do
        collection do
          get :get_data
          get :get_parent_data
          post :create_event
          put :event_update
          get :edit_event
          get :event_delete
          get :get_event_datail
          # get  :timeline
          get :calendar_info
        end
      end


      post 'profile/delete_child_interest'
      
      resources :milestones do
        collection do
          put :add_multiple
          get :add_description
          get :change_category
          get :display_later
          get :milestones_info
        end  
        member do
          put :save_and_next
          get :skip
          delete :remove_picture
          get :display_picture
        end  
      end  

      resources :nutrients do
        collection do
          get :change_category
        end  
        member do
          get :acitvate
          delete :deactivate
        end
      end

      resources :documents do
        collection do
          post :save_documents
          get :search
          get :documents_info
        end  
        member do
          delete :delete_attachment
          get :download
        end  
      end  

      resources :notifications do
        member do
          get :view
        end  
        collection do
          get :check_all
        end  
      end

      resources :invitations do
        collection do
          get :accept
          get :decline
          get :add_invitee
          get :remove
          get :edit_email
          get :change_email
          post :send_invites
          get :view_invite
          get :invite_friend_to_nurturey
        end  
      end  
      

      resources :invitations
      resources :pregnancy do
        collection do
          post :create_preg
          get :get_mother_list
          get :get_preg_info
          delete :delete_preg
          put :update_preg_child
          post :create_system_timeline
          get :get_default_timelines
          put :get_default_timelines
          put :mark_preg_complete
          put :update_preg
        end
      end
      # resources 'parental_test', :path => :medical_event  do
      resources :medical_event, :path => 'prenatal_test'  do
        collection do
          get :get_medical_event_list
          post :add_recommended_test
          get :get_recommended_medical_event
          delete :delete_component_from_medical_event
          delete :delete_component_from_sys
          post :create_new_component
          post :add_component_to_medical_event
          get :get_sys_components
          get :get_medical_event_detail
          delete :delete_parental_test
        end
        member do
          post :upload_image
        end  
      end

      resources :member do
        member do
          get :edit_child
          put :update_child
          get :edit_elder
          put :update_elder
          put :update_timezone
          delete :delete_child
          delete :delete_elder_invitation
          delete :remove_elder
          get :verify_elder_for_family
          get :verify_elder      
          get :resend_invitation
          get :display_picture
          get :edit_picture
          get :tool_activation
          get :hide_invited_user_block
        end
        collection do
          # get :invite_friend
          post :set_metric_system
          get :edit_current_member
          get :trigger_tour_step
          post :update_current_member
          
        end
      end
      resources :article_ref do
        collection do
          delete :delete_article_ref
          put :add_to_favorite
          put :remind_me_later
          put :update_art_ref
          get :art_ref_detail
          get :recommended_art_ref
        end
      end
    end
    # End of Namespace v2
  
    #start of V3
    # Moved in api_v3_routes files
    #End of V3
  end

namespace :alexa do

  resources :sign_up do 
    collection do
      get :sign_in
      get :user_details
      get :create_family
      get :add_child
      get :finish_setup
      get :get_email_verification_screen
      get :get_user
      get :get_family
    end

  end

end 
match "/alexa/sign_in"=>"alexa::sign_up#sign_in"

namespace :admin do
  resources :amazon_affiliate_links do 
    collection do
      post :search_product
    end
  end

  resources :smile_a_design_contest_result

  resources :system_settings do 
    collection do
      get :new_setting
      delete :delete_system_setting
      get :delete_system_setting
    end
  end

  resources :kpi do 
    collection do
      get :kpi_data_on_date
    end
  end

  resources :manage_nhs_account do 
    collection do
      get :update_clinic_account_status
      post :get_filter_data
      
    end
  end
  resources :unregister_from_email do 
    collection do
      post :unregister_email_from_list
    end
  end
  
  resources :clinic do 
    collection do
      get  :new_organization
      get  :delete_organization
      put  :update_organization
      post :create_organization
      get  :list_organization_user
      get  :edit_user
      get  :get_organization_detail
      get  :new_organization_user
      get  :delete_organization_user
      put  :update_organization_user
      post :create_organization_user
      get :import_user_from_file
      post :save_imported_organization_user

    end
  end
  resources :nhs_request_manager do 
    collection do
      get :nhs_news_request
      get :search_data
      get :nhs_search_request
      get :nhs_search_request_output
      get :nhs_news_request_output
      get :get_data
      post :nhs_search_data
      get :nhs_search_data
      get :show_saved_articles
      get :show_saved_media
      get :get_organisation_data
      get :show_organisation_data
    end
  end

  
  resources :test_page

  resources :nhs_data do
    collection do 
      post :list_data
    end 
  end
  
  resources :role_for_admin_panel do 
    collection do
      get :new_role
      get :delete_role
    end
  end
  resources :module_access_for_role do 
    collection do
      get :new_module_access
      get :delete_module_access
    end
  end
  resources :batch_notification_manager do 
    collection do
      get :new_batch_notification
      get :delete_batch_notification
      get :test_notification_on
      get :clear_notification_count
    end
  end

  resources :alexa do 
    collection do
    end
  end

  resources :face_plus do 
    collection do
      get :get_response_from_face_plus_plus
       post :get_response_from_face_plus_plus
    end
  end

  resources :system_template do 
    collection do
      get :get_gpsoc_immunisation_jab_detail
      get :get_system_jab_detail
       get :autocomplete_system_immunisation_jab_title
       get :autocomplete_gpsoc_immunisation_jab_title
       get :autocomplete_nhs_syndicate_content_title
       get :new_template
       get :delete_template
       get :add_syndicated_content_action_card_category
       post :save_syndicated_content_action_card_category
       get :get_system_data_for_selected_country
       get :new_system_record
       get :show_system_record
       get :clone_form_for_country
       post :clone_complete_data_for_country
       post :create_system_record
       put :update_system_record
       get :delete_system_record
       get :edit_system_record
       get :next_system_record
       get :previous_system_record
       get :clone_system_record
       get :duplicate_system_record
       post :add_existing_checkup_test
       get :get_linked_syndicated_content
    end
  end

  

  resources :tool_list_maintenance do 
    collection do
      get :new_tool
      get :delete_tool
    end
  end

  resources :faq do 
    collection do
      get :new_topic
      get :new_subtopic
      get :new_question
      get :new_answer
      
      post :create_topic
      post :create_subtopic
      post :create_question
      post :create_answer

      put :update_topic
      put :update_subtopic
      put :update_question
      put :update_answer

      get :edit_topic
      get :edit_subtopic
      get :edit_question
      get :edit_answer

      get :show_topic
      get :show_subtopic
      get :show_question
      get :show_answer
      get :autocomplete_system_question_custom_id
      get :delete_record
      get :test_question_scenario_validation_to_assign
    end
  end

  resources :subscription_maintenance do 
    collection do
      get :new_subscription
      get :delete_subscription
      get :subscription_report
      get :subscription_history_paginated_data
      get :get_users_list_checking_subcription_list
      get :paginated_data
    end
  end

  resources :action_card_list_maintenance do 
    collection do
      get :new_action_card
      get :delete_action_card
    end
  end

  resources :api_log do 
    collection do
      get :get_filter_data
      post :get_filter_data
      get :new_action_card
      get :delete_action_card
      get :admin_panel_access_log
      get :paginated_data
      get :event_log_data
      get :event_list
      get :get_event_description
      get :tpp_logs
      get :tpp_log_paginated_data
      post :get_emis_filter_data
      get :emis_logs
      get :emis_log_paginated_data
      post :get_nhs_api_filter_data
      get :nhs_api_log_paginated_data
      get :nhs_api_logs
      get :cvtm_purchase_event_log_data
      get :emis_logs
      get :emis_log_paginated_data
      get :edit_event
      put :update_event
    end
  end

  resources :user_role_for_admin_panel do 
    collection do
      get :new_role
      get :delete_role
    end
  end

  resources :scenario_details do 
    collection do
      get  :new_scenario
      post :add_scenario 
      get  :delete_scenario
      get  :edit_scenario
      put  :update_scenario
      get  :get_linked_items
      get  :paginated_data
      get  :clone_scenario
      get  :go_to_next_scenario
      get  :go_to_previous_scenario
      get  :edit_next_scenario
      get  :edit_previous_scenario 
      get  :test_notification_validation_to_assign
    end
  end

  resources :nhs_syndicated_content do 
    collection do
      get  :new_syndicated_content
      post :add_syndicated_content
      get  :delete_syndicated_content
      get  :edit_syndicated_content
      put  :update_syndicated_content
      get  :get_linked_items
      get  :paginated_data
      get  :clone_syncdicated_content
      get  :go_to_next_syndicated_content
      get  :go_to_previous_syndicated_content
      get  :edit_next_syndicated_content
      get  :edit_previous_syndicated_content
      get  :show_content
      get :fetch_content
      get :link_with_tool
      get :get_country_list_for_tool_class
      get :autocomplete_link_tool_title
      post :save_link_tool_linking
      get :get_linkd_tool_with_sydicated_content
      get :delink_tool_with_syndicated_content
       
    end
  end

  resources :pointers do
    collection do 
      get :autocomplete_system_scenario_scenario_details_title
      get :autocomplete_system_pointer_custom_id
      post :search_pointer
      get :search_pointer
      get :edit_pointer_link
      put :update_pointer_link
      get :edit_pointer
      get :test_notification_validation_to_asign
      put :update_pointer
      get :validate_all_pointer_links
      put :update_pointer_status
      get :delete_pointer
      get :clone_pointer
      get :get_pointer_history
      get :delete_pointer_link
      get :clone_pointer_link
      get :new_pointer
      get :new_pointer_link
      get :validate_pointer_link
      post :create_pointer
      get :paginated_link_data
      post :create_pointer_link
      get :test_notification_on
      get :test_notification_off
      get :go_to_next_pointer
      get :go_to_previous_pointer
      get :edit_next_pointer_link
      get :edit_previous_pointer_link
      get :edit_next_pointer
      get :edit_previous_pointer 
      get :push_pointer_count_report
      get :get_pointer_history
      get :get_link_details
      get :all_pointer_links
    end 
  end
  resources :users do
     collection do
       get :approve
       put :aprove_user  
       get :update_user_status 
       get :delete_user 
       get :report
       get :user_not_signed_in_since_form
       get :user_not_signed_in_since
       get :user_type
       post :search    
       get :search
       post :search_user    
       get :search_user    
       get :update_user_for_mail_registration 
       get :get_user_device_details
       get :edit_user_detail   
       get :get_user_detail   
       post :update_user_datail   
       get :push_notification_report
       get :push_notification_report_identifier_wise
       get :push_notification_response_report
       get :push_notification_identifier_response_summary_report
       post :push_notification_identifier_response_summary_report
       get :user_subscription_change_form
       get :edit_credit_in_family_cvtme_purchase
       get :update_credit_in_family_cvtme_purchase
       get :update_user_subscription
       get :user_login_atleast_once_at_interval_in_duration_form 
       get :user_login_atleast_once_at_interval_in_duration
       get :get_push_notification_identifier_detail
       get :user_subscription_history
       get :user_families_detail
       get :home
       get :deleted_user_details
       get :kpi_data
     end
  end
  resources :questionnaires do

    resources :questions do
      get :take_quiz
      post :take_quiz
      post :save_answer
    end
    resources :relevances
  end
end
resources :health do
  collection do 
    get :get_detail
    get :edit
    get :new_health_record
    get :get_health_detail
    post :create_record
    post :update_record
    get :delete_parameter
    get :health_info
    get :close_info_pop
  end
end

resources :immunisation do 
  collection do
    # get :new_vaccination
    get :new_jab
    post :save_jab
    get :edit_jab
    put :update_jab
    get :delete_jab
    get :immunisation_info
    get :close_info_pop
    get :upcoming_jabs
    get :jab_detail
  end
end

resources :score  

resources :vaccination do
  collection do
    get :add_vaccin
    get :delete_vaccin
    get :edit_vaccin
    put :update_vaccination
  end
end

resources :timeline do
  collection do
    get :pics
    get :media
    get :automated_timeline
    post :save_auto_time
    get :skip_automated_timeline
    get :edit
    post :create_timeline
    put :update_timeline
    get :delete_timeline
    get :remind_later
    get :delete_timeline_pic
    get :timeline_info
  end
  member do
    get :facebook_preview
    post :send_to_facebook
  end  
end

resources :manage_family

match '/uk', to: 'home#uk'
match '/pinkbook', to: 'home#pinkbook'
match '/uae', to: 'home#uae'
match '/designasmile', to: 'home#designasmile'
match "/save_designasmile" , to: 'home#save_designasmile'

match "/calendar/dashboard"          => "users#dashboard"
match '/families/get_family_data'    => 'families#get_family_data', :as => :get_family_data 
match '/families/join_family'        => 'families#join_family', :as => :join_family 
match '/families/save_joined_family' => 'families#save_joined_family', :as => :save_joined_family
match "/features"                     => 'home#features', :as => :features
match '/alexa_account_link' => 'home#alexa_account_link',:as => 'alexa_account_link'
# view mails in browse
get 'mails_view/:params', to: 'mails_view#show'

resources :calendar do
  collection do
    get :get_data
    get :get_parent_data
    post :create_event
    put :event_update
    get :edit_event
    get :event_delete
    get :get_event_datail
    # get  :timeline
    get :calendar_info
  end
end


  post 'profile/delete_child_interest'
  
  resources :milestones do
    collection do
      put :add_multiple
      get :add_description
      get :change_category
      get :display_later
      get :milestones_info
    end  
    member do
      put :save_and_next
      get :skip
      delete :remove_picture
      get :display_picture
    end  
  end  

  resources :nutrients do
    collection do
      get :change_category
    end  
    member do
      get :acitvate
      delete :deactivate
    end
  end

  resources :documents do
    collection do
      post :save_documents
      get :search
      get :documents_info
    end  
    member do
      delete :delete_attachment
      get :download
    end  
  end  

  resources :notifications do
    member do
      get :view
    end  
    collection do
      get :check_all
    end  
  end

  resources :invitations do
    collection do
      get :accept
      get :decline
      get :add_invitee
      get :remove
      get :edit_email
      get :change_email
      post :send_invites
      get :view_invite
      get 'invite_friend_to_nurturey'
    end  
  end  
  resources :families do
    resources :members, only: [] do
      resources :nutrients, only: :index
      resources :timeline, only: :index
      resources :milestones, only: :index
      resources :calendar, only: :index
      resources :health, only: :index
      resources :documents, only: :index
      resources :immunisation, only: :index
    end  
    collection do
      get :search_family
      get :select_role
      post :save_role
    end  
    member do
      post :join_this_family  
      #get :join_this_family  # Temporary because form is not working with ajax      
    end  
    resources :member do
      collection do
        get :new_child
        post :create_child        
                
        get :new_elder
        post :create_elder 
      end
    end  
  end

  resources :invitations
  resources :pregnancy do
    collection do
      post :create_preg
      get :get_mother_list
      get :get_preg_info
      delete :delete_preg
      put :update_preg_child
      post :create_system_timeline
      get :get_default_timelines
      put :get_default_timelines
      put :mark_preg_complete
      put :update_preg
    end
  end
  # resources 'parental_test', :path => :medical_event  do
  resources :medical_event, :path => 'prenatal_test'  do
    collection do
      get :get_medical_event_list
      post :add_recommended_test
      get :get_recommended_medical_event
      delete :delete_component_from_medical_event
      delete :delete_component_from_sys
      post :create_new_component
      post :add_component_to_medical_event
      get :get_sys_components
      get :get_medical_event_detail
      delete :delete_parental_test
    end
    member do
      post :upload_image
    end  
  end

  resources :member do
    member do
      get :edit_child
      put :update_child
      get :edit_elder
      put :update_elder
      put :update_timezone
      delete :delete_child
      delete :delete_elder_invitation
      delete :remove_elder
      get :verify_elder_for_family
      get :verify_elder      
      get :resend_invitation
      get :display_picture
      get :edit_picture
      get :tool_activation
      get :hide_invited_user_block
    end
    collection do
      # get :invite_friend
      post :set_metric_system
      get :edit_current_member
      get :trigger_tour_step
      post :update_current_member
      
    end
  end

  resources :nhs do
    collection do
      get :authorize_redirect
      get :user_info
      get :token
      get :authenticate_user
      get :initiate_user_authentication
    end  
  end

  resources :article_ref do
    collection do
      delete :delete_article_ref
      put :add_to_favorite
      put :remind_me_later
      put :update_art_ref
      get :art_ref_detail
      get :recommended_art_ref
    end
  end

 resources :album do
  collection do
    get :add_album
    get :get_album_detail
  end
end
  namespace :campaign_manager do
    resources :email_campaign do
      collection do
        get :uninstall_callback_campaign
        get :uninstall_callback_campaign_browser_view
      end
    end
  end
  resources :orion do
    collection do 
      get :initiate_user_authentication
      # get :orion_authentication_completed, 

    end
  end

  get :faq, to: 'home#faq'
  get :uae_faq, to: 'home#uae_faq'
  get :message, to: 'home#message'
  get :prescriptions, to: 'home#prescriptions'
  get :measurements, to: 'home#measurements'
  get :appointments, to: 'home#appointments'
  get :vaccinations, to: 'home#vaccinations'
  get :nhs_guidance, to: 'home#nhs_guidance'
  get :nhs_pregnancy_guides, to: 'home#nhs_pregnancy_guides'
  get :baby_on_board, to: 'home#baby_on_board'
  get :nhs_services, to: 'home#nhs_services'
  get :download, to: 'home#download'
  get :pinkbook_tools, to: 'home#pinkbook_tools'
  get :six_in_one, to: 'home#six_in_one'
  get :rotavirus_vaccine, to: 'home#rotavirus_vaccine'
  get :meningitis_b_vaccine, to: 'home#meningitis_b_vaccine'
  get :hib_men_c_booster_vaccine, to: 'home#hib_men_c_booster_vaccine'
  get :pneumococcal_vaccination, to: 'home#pneumococcal_vaccination'
  get :mmr_vaccine, to: 'home#mmr_vaccine'
  get :pre_school_dtap_ipv_booster, to: 'home#pre_school_dtap_ipv_booster'
  get :teenage_booster, to: 'home#teenage_booster'
  get :child_flu_vaccine, to: 'home#child_flu_vaccine'
  get :bcg_tuberculosis_tb_vaccine, to: 'home#bcg_tuberculosis_tb_vaccine'
  get :chickenpox_vaccine, to: 'home#chickenpox_vaccine'
  get :travel_vaccinations, to: 'home#travel_vaccinations'
  get :hpv_human_papillomavirus_vaccine, to: 'home#hpv_human_papillomavirus_vaccine'
  get :nhs_vaccinations_and_when_to_have_them, to: 'home#nhs_vaccinations_and_when_to_have_them'

  resources :home, :only => [:index] do
    collection do
      get :team
      get :privacy
      get :copyright
      get :cookies
      get :login
      get :faq
      get :terms
      get :popup_url
      get :youtube_video
      get :vimeo_video
      get :mobile_experience_popup
      get :get_geoname
      get :accept_join_family_request
      get :decline_join_family_request
    end
    member do
      get :notification
    end
  end

  authenticated :user do
    root :to => 'home#index'
  end

  get 'oauth2callback' => 'documents#set_google_drive_token' # user return to this after login
  get 'list_google_doc'  => 'documents#list_google_docs', :as => :list_google_doc #for listing the 
  get 'download_google_doc'  => 'documents#download_google_docs', :as => :download_google_doc #download

  get '/user/registration/status' => "users#after_registration", :as => "after_registration"
  
  root :to => "home#index"
  get "/messages" => "home#messages", as: :home_messages
  get "/about" => "home#about"
  get "/contact" => "home#contact"
  get "/unsubscribe" => "home#unsubscribe"
  get "/tools" => "api/v1/nutrients#index" 
  get "/tools" => "api/v2/nutrients#index" 
  # devise_scope :user do
  #   match '/passwords/update' => "passwords#update"
  #   get '/users/sign_out'     => 'sessions#destroy',     :as => :logout
  # end
  
  # # devise_for :users, :controllers => { :omniauth_callbacks => "omniauth_callbacks" }
  # devise_for :users ,:controllers => {:passwords => "passwords", :sessions => "sessions",:registrations => 'registrations',:confirmations=>'confirmations',:omniauth_callbacks => "omniauth_callbacks"} do
  #   resources :passwords do
  #     post :update    
  #   end
    
  #   resources :sessions do
  #     get :sign_in
  #   end
  # end




  devise_scope :user  do
    match '/passwords/update' => "passwords#update"
    match "/users/auth/:provider" => "omniauth_callbacks#orion"

    get '/users/sign_out'     => 'sessions#destroy',     :as => :logout
     resources :passwords do
      post :update
      collection do  
            put :secure_update
          end    
    end
    resources :registrations do 
      collection do 
        post :secure_signup
      end
    end
    resources :sessions do
      get :sign_in
    end
  end
  
  # devise_for :users, :controllers => { :omniauth_callbacks => "omniauth_callbacks" }
  devise_for :users ,:controllers => {:passwords => "passwords", :sessions => "sessions",:registrations => 'registrations',:confirmations=>'confirmations',:omniauth_callbacks => "omniauth_callbacks"}
 


  match 'families/delete_member' => 'families#delete_member'
  match '/passwords/update' => 'passwords#update', :as => :update  
  
  # match '/school/dashboard' => 'users#dashboard', :as => :dashboard
  match '/users/home_page' => 'users#home_page'
  match '/users/settings' => 'users#settings'
  match 'dashboard'             => 'users#dashboard', :as => :dashboard
  match 'nhs_authentication_completed'             => 'users#nhs_authentication_completed', :as => :nhs_authentication_completed
  match "orion_authentication_completed"           => 'orion#orion_authentication_completed', :as => :orion_authentication_completed
  match 'alexa_login'=> 'users#alexa_login'
  match "resend_confirmation_link"   => 'users#resend_confirmation_link'
  match 'add_new_family'        => 'users#add_new_family', :as => :add_new_family
  match '/families/family_name_update'        => 'families#family_name_update', :as => :family_name_update
  match 'associate_to_family'   => 'users#associate_to_family', :as => :associate_to_family
  match '/users/change_email' => 'users#change_email', :as => :change_email
  match '/users/verify_secondary_email' => 'users#verify_secondary_email'
  match '/users/sec_email_add' => 'users#sec_email_add', :as => :sec_email_add
  match '/users/sec_email_del' => 'users#sec_email_del', :as => :sec_email_del
  match '/users/change_primary_email' => 'users#change_primary_email'
  match '/users/edit_cover_image'   => 'users#edit_cover_image' 
  match '/users/update_cover_image'   => 'users#update_cover_image' 
  match '/users/set_default_cover_image' =>  'users#set_default_cover_image'
  match '/users/delete_cover_image'   => 'users#delete_cover_image' 
   
  match 'show_list'                => 'users#show_list',                :as => :show_list
  
  match "/album/add_album"      => 'album#add_album'
  match "/album/add_photo"      => 'album#add_photo'
  match 'photo_list'            => 'album#photo_list', :as => :photo_list
  match 'album_list'            => 'album#album_list', :as => :album_list
  match 'destroy_album_photo'   => 'album#destroy_album_photo', :as => :destroy_album_photo
  match 'delete_photo'          => 'album#delete_photo', :as=> :delete_photo
  match 'destroy_album'         => 'album#destroy_album', :as => :destroy_album

  match 'app_invite_fb'   => "home#app_invite_fb", :as => :app_invite_fb
  match 'app_download'    => "home#app_download", :as=> :app_download
  match 'nhs_login_failed' => "home#nhs_login_failed", :as=>:nhs_login_failed
  resources :users, :except => [:show] do
    collection do
      get :fetch_location
    end  
  end  

  #match 'auth/:provider/callback' => 'users#dashboard'


  match 'accept_invitee_to_group' => 'users#accept_invitee_to_group'

  resources :event  

  match 'event_comment_delete' => 'event#event_comment_delete'  
  match '/nhs/authenticate_user' => 'api/v6/nhs#authenticate_user'  
  match 'create_event' => 'event#create_event'
  match 'event_response' => 'event#event_response'
  match 'event_delete' => 'event#event_delete'
 

  match '*path', :to => 'application#routing_error'

end
