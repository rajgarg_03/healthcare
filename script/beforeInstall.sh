#!/bin/bash

# I want to make sure that the directory is clean and has nothing left over from
# previous deployments.
# Copying just tmp folder and Gemfile.lock from the previous backup as they are not part of the build
DATE=`/bin/date "+%Y%m%d%H%M"`
if [ -d /opt/web/nurtureyreloaded ]; then
    mkdir /opt/web/backups/nurtureyreloaded.${DATE}
    mv /opt/web/nurtureyreloaded/* /opt/web/backups/nurtureyreloaded.${DATE}
    mv /opt/web/nurtureyreloaded/.ruby* /opt/web/backups/nurtureyreloaded.${DATE}/.
    mv /opt/web/nurtureyreloaded/.git* /opt/web/backups/nurtureyreloaded.${DATE}/.
    cp -rp /opt/web/backups/nurtureyreloaded.${DATE}/Gemfile.lock /opt/web/nurtureyreloaded/.
    cp -rp /opt/web/backups/nurtureyreloaded.${DATE}/tmp /opt/web/nurtureyreloaded/.
    mkdir -p /opt/web/nurtureyreloaded/log
fi


