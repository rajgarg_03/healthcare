#!/bin/bash

 # This is to restart the application
if [ -d /opt/web/nurtureyreloaded ]; then
    touch /opt/web/nurtureyreloaded/tmp/restart.txt
    cd "/opt/web/nurtureyreloaded"
    RAILS_ENV=$application_env;
    source ~/.rvm/scripts/rvm
    rvm use ruby-2.2.2@gatividhi
    bundle install
    rake assets:clean RAILS_ENV=production --trace
    rake assets:precompile RAILS_ENV=production --trace
    chmod a+x '/opt/web/nurtureyreloaded/script/delayed_job'
    chown -R nginx:nginx '/opt/web/nurtureyreloaded/'
    touch /opt/web/nurtureyreloaded/tmp/restart.txt
     
     
    exit
fi
