module ApplicationHelper	
  def paginate(collection, params= {})
    will_paginate collection, params.merge(:renderer => RemoteLinkPaginationHelper::LinkRenderer)
  end

  def alexa_reference_params
    @reference_params = "state=#{session[:alexa_state]}&redirect_uri=#{session[:alexa_redirect_uri]}&app_name=#{session[:alexa_source_reference]}&platform=#{session[:alexa_platform]}"
  end
  
  def get_alexa_platform
    platform = (params[:platform].blank? ? session[:platform] : params[:platform]).to_s.downcase
    if platform == "alexa" 
       "Alexa"
    elsif platform == "googleassistance"
      "Google Assistance"
    end  
  end

  def alexa_reference_params_for_home
    if params[:app_name].present? && params[:redirect_uri].present?
      @reference_params = "?state=#{params[:state]}&redirect_uri=#{params[:redirect_uri]}&app_name=#{params[:app_name]}"
    else
      ""
    end
  end

  def available_system_table
    System::Template.available_template
  end

  def available_systme_table_for_scenario
   # [["System::Faq","System::Faq"],["System::Faq::Topic","System::Faq::Topic"],["System::Faq::SubTopic","System::Faq::SubTopic"], ["System::Faq::Question","System::Faq::Question"]]
    System::Template.available_system_table_for_scenario
  end
	
  def check_album_dashboard
    @dashboard_ablums = []
    current_user.families.each do |family|
      family.albums.each do |album|
        @dashboard_ablums << album
      end
    end    
  end
  
  def value_convert(value,name,unit)
      (value.to_f * Health::UNIT_VAL[unit]).round(2) rescue value
  end


  def user_status(user)
    if user.status=="confirmed" || user.status=="approved" 
      "unapproved"
    else
      "approved"
    end
  end
  def app_version_supported_options
    data = []
    (-1..10).each do |a|
      if a == -1
        data <<  ["NA",a]  
      else
        data <<  [a,a]  
       end  
    end
    data
  end
  def user_action(user)
    if user.status=="confirmed" || user.status=="approved" 
      "Unapprove"
    else
      "Approve"
    end
  end

   def user_clinic_access(member)
    if member.clinic_account_status=="Active" 
      "Block"
    else
      "Active"
    end
  end

  def user_mail_status(user)
    if user.unregister_from_mail==true 
      "Register for mail"
    else
      "Unregister from mail"
    end
  end
  
  def home_cover_image(member, size="large",api_version=1)
    begin
      member.user.cover_image.photo_url_str(size,api_version)
    rescue
      '/assets/home-img.jpg'
    end
  end

  def user_badge
    user_family_member = current_user.family_members(FamilyMember::STATUS[:accepted])
    families = current_user.user_families
    score = 4#Score.where(:family_id.in=>families.map(&:id)).sum(:point) + current_user.scores.where(:activity_type.in => ['User','Invitation']).sum(:point)
    @score = Score::badge(score)
  end
  
  def profile_image(member, size="small",api_version=1)
    begin
      member.profile_image.photo_url_str(size,api_version)
    rescue
      member.image || "/images/original/missing.png"
    end
  end

  def profile_image_tag(member, size="small",api_version=1)
     # begin
     #  ('<img src=' + member.profile_image.photo_url_str(size) + ' class="img-icon-type1" alt=""/>').html_safe
     # rescue
     #    family_member = member.family_members.first
     #    html = ''
     #    html << "<div class='user-avatar #{FamilyMember::IMAGE_CLASS[family_member.role.underscore.to_sym]}'>"  
     #    html << "</div>"
     #    html.html_safe  
     #    # ('<div class="user-avatar"></div>').html_safe
     # end
    begin 
      if member.profile_image.present?
        ('<img src=' + member.profile_image.photo_url_str(size,api_version) + ' class="img-icon-type1" alt=""/>').html_safe
      elsif member.image.present? #Fb image hardly needed as this method is for child
        ('<img src=' + member.image + ' class="img-icon-type1" alt=""/>').html_safe
      else
        family_member = member.family_members.first
        html = ''
        html << "<div class='user-avatar #{FamilyMember::IMAGE_CLASS[family_member.role.underscore.to_sym]}'>"  
        html << "</div>"
        html.html_safe  
      end
    rescue
      family_member = member.family_members.first
      role = FamilyMember::IMAGE_CLASS[family_member.role.underscore.to_sym] rescue :administrator
      html = ''
      html << "<div class='user-avatar #{role}'>"  
      html << "</div>"
      html.html_safe  
    end
  end

  def welcome_image_tag(member, size="small")
      family_member = member.family_members.first
      html = ''
      html << "<div class='user-avatar #{FamilyMember::IMAGE_CLASS[family_member.role.underscore.to_sym]}'>" 
     begin
      html << ('<img src=' + member.profile_image.photo_url_str(size,api_version) + ' class="img-icon-type1" alt=""/>').html_safe  rescue   " "   
      html << "</div>"
      html.html_safe  
      # ('<div class="user-avatar"></div>').html_safe
     end
  end


  def header_profile_image_tag(member, size="small",api_version=1)
     begin
     #  ('<img src=' + member.profile_image.photo_url_str(size) + ' class="avatar-img" alt="" height="35" width="35"/>').html_safe
     # rescue
     #  member.image || "<div class='user-avatar'></div>".html_safe  
      # ('<div class="user-avatar"></div>').html_safe

      if member.profile_image.present?
        ('<img src=' + member.profile_image.photo_url_str(size,api_version) + ' class="avatar-img" alt="" height="35" width="35"/>').html_safe
      elsif member.image.present?
        ('<img src=' + member.image + ' class="avatar-img" alt="" height="35" width="35"/>').html_safe
      else
        ('<div class="user-avatar"></div>').html_safe 
      end
    rescue      
      ('<div class="user-avatar"></div>').html_safe 
    end
  end

def recent_profile_tag(member, size="small",api_version=1)
  begin 
    family_member = member.family_members.first
    ("<div class='user-avatar'><img src='" +  member.profile_image.photo_url_str(size,api_version) + "' /></div>").html_safe
  rescue
    "<div class='user-avatar #{FamilyMember::IMAGE_CLASS[family_member.role.underscore.to_sym]}'></div>".html_safe
  end
end
 
    def album_cover(album,size="large",api_version=1)
      begin
        album.pictures.first.photo_url_str(size,api_version)
      rescue
         "/assets/post-dummy-img-gallery-2.jpg"
      end
    end

  def milestone_photo(milestone,size="medium",api_version=1)
    begin
      milestone.pictures.first.photo_url_str(size,api_version)
    rescue
         "/assets/post-dummy-img-gallery-2.jpg"
      end
  end  

  def calculate_age(birthday,totime=Time.now)
    return 0 if birthday.blank?
    time_diff = distance_of_time_in_words_hash(birthday, totime)
    time_diff = Hash[time_diff.select{|k,v| v>0}.first(2)]
    time_diff.reject!{|k,v| ['hours','minutes','seconds'].include?(k.to_s)}
    distance = []
    time_diff.map do |k,v| 
      if v > 0
        k = k.to_s.singularize if v == 1
        distance << "#{v} #{k}"
      end
    end  
    distance.blank? ? "1 day" : distance.join(' and ')
  end
  
  #eg. 3 months 2 days
  def calculate_age2(birthday,totime=Time.now)
    return 0 if birthday > totime.to_date
    calculate_age(birthday,totime)
  end
  
  #eg. 3m 2d
  def calculate_age3(birthday,totime=Time.now)
    age = calculate_age(birthday,totime)
    if age != 0
      age.gsub!("and", "")
      age.gsub!(" month", "m")
      age.gsub!(" year", "y")
      age.gsub!(" week", "w")
      age.gsub!(" day", "d")
      age.gsub!("s", "")
    end 
    age
  end

  #2 months and 1 week
  def calculate_age4(birthday,totime=Time.now,level=nil)
    return "today" if birthday.to_date == totime.to_date
    date_string = calculate_age(birthday,totime)
    if level.present? && level == 1
      date_string = date_string.split("and")[0].strip
    end
    date_string
  end

  #2 yrs 1 mth
  def calculate_age5(birthday,totime=Time.now)
    age = calculate_age(birthday,totime)
    if age != 0
      age.gsub!("and", "")
      age.gsub!(" month", " mth")
      age.gsub!(" year", " yr")
      age.gsub!(" week", " wk")
      age.gsub!(" day", " day")
      age.gsub!("  ", " ")
    end 
    age
  end

  #eg. 3 months 2 days
  def calculate_age6(birthday,totime=Time.now)
    calculate_age2(birthday,totime).gsub("and ", "")
  end
  
  def title(page_title, show_title = true)
    @content_for_title = page_title.to_s
    @show_title = show_title
  end

  def milestone_desc(milestone)
    truncate( (milestone.description.present? ? milestone.description : milestone.system_milestone.description),:length =>140) rescue nil
  end
  
  def user_secondary_email
    current_user.secondary_email? ? current_user.secondary_email.email : ""
  end

  def show_title?
    @show_title
  end
  
  def stylesheet(*args)
    content_for(:head) { stylesheet_link_tag(*args) }
  end
  
  def javascript(*args)
    content_for(:head) { javascript_include_tag(*args) }
  end

  def toDate(date=nil)
    begin
      date.to_date.strftime("%d-%m-%Y")
    rescue
      Date.today.strftime("%d-%m-%Y")
    end
  end

  def toDate1(date)
    begin
      date.to_date.strftime("%d %b %Y")
    rescue
      Date.today.strftime("%d %b %Y")
    end
  end
 
  def toDate2(date)
    time_ago_in_words(date).split(",")[0].split("and")[0] + " ago"   rescue Date.today.strftime("%d %b %Y")
  end

  def toDate3(date)
    date.to_date.strftime("%d %b %Y")    rescue Date.today.strftime("%d %b") 
  end
  
  def title_length(length=nil)
    length ||50
  end

  def desc_length(length=nil)
    length || 250
  end

  def child_member?(family_member)
     ["Son","Daughter"].include?(family_member.role)
  end

  def family_roles_select(id, options=nil)
    select_tag id, options_for_select(Family::ROLES.values.collect {|role| [role, role]}), options
  end

  def country_fields
    pop_country_codes = ['IN', 'GB', 'US']
    countries = Country.all
    popular_countries = countries.collect{|con| con if pop_country_codes.include?(con[1]) }.delete_if{|con| con.blank?}
    [["Popular Countries", popular_countries.sort], ["Other Countries", countries.sort.delete_if {|con| pop_country_codes.include?(con[1])}]]
  end  
  

  def get_count(obj)
    if obj.count > 1
      "js-acc-main"
    else
       "js-non-acc"
    end 
  end

  def added_milestones(member)
    arr = member.milestones.group_by{|m| m.system_milestone.category}.map{|k,grp| {k => grp.count} }
    SystemMilestone::CATEGORIES_RANGE.values.map{|v| v==arr}
    arr2 = []
    SystemMilestone::CATEGORIES_RANGE.values.map do |v|
      arr.each do |h1|
        @value = v==h1.keys.first ? h1.values.first : 0
        break if @value != 0
      end      
      arr2 << @value.to_i
    end      
    arr2
    # arr = member.milestones.group_by{|m| m.system_milestone.category}.map{|grp| grp.last.count }
    # arr + ([0]*(SystemMilestone::CATEGORIES_RANGE.keys.count - arr.count))
  end  

  def non_added_milestones(member)
    country_code = member.get_country_code
    options = {:country_code=>country_code}
    added_milestones(member).zip(SystemMilestone.milestone_count(options) ).map{|x,y| y-x}
  end

  def milestone_add_date_path(member,system_milestone)    
    if member.milestone_for(system_milestone)
      edit_milestone_path(member.milestone_for(system_milestone).try(:id))
    else  
      new_milestone_path(member_id: @member.id,system_milestone_id: system_milestone.id)
    end  
  end  

  def family_member_role_class(family_member)
    role_class = family_member.role == FamilyMember::ROLES[:admin] ? "fa-admin fontIcon" : ""
    role_class = role_class.split(' ').concat(['you-icon','fontIcon']).uniq.join(' ') if family_member.member == current_user
  end  

  def family_children(families=nil)
    families = families || @families
    ids = families.map(&:id)
    family_ids = ids - (FamilyMember.where({'family_id' => { "$in" => ids} }).where({'role' => { "$in" => ["Daughter","Son"]} }).group_by(&:family_id).keys)
    @family_children = families.entries.select{|x| family_ids.include?(x.id)} || []
  end

  def family_album(families=nil)
    families = families || @families
    ids = families.map(&:id)
    family_ids = ids - (Album.where({'family_id' => { "$in" => ids} }).where({'role' => { "$in" => ["Daughter","Son"]} }).group_by(&:family_id).keys)
    @family_album = families.entries.select{|x| family_ids.include?(x.id)} || []
  end
 
  def set_priority
    {"0"=>["family_children"],  "1"=>["childern_timelines","childern_nutrients"], "2"=>["childerns_immunsation","childerns_milestones","childerns_health"], "3"=>["childerns_document"] }
    # {"0"=>["family_children","childerns_document"],  "1"=>["childern_timelines","childern_nutrients"], "2"=>["childerns_immunsation","childerns_milestones","childerns_health"] }
  end
  def priority(ids,childern,families)
    @ids = ids || []
    @childern = childern || []
    @families = families || []
    out = true
    set_priority.each do |k,v|
      if out
        v.each do |va|
          out = false if !eval(va).blank?
        end
      end
    end
  end

  def childerns_milestones(ids=nil, childern=nil)
    ids = ids || @ids
    childern = childern || @childern
    child_ids = (ids) - (Milestone.where({'member_id' => { "$in" => ids} }).where("'created_at' > " + "'#{(Time.now - 2.month)}'").group_by(&:member_id).keys)
    @childerns_milestones = (childern).entries.select{|x| child_ids.include?(x.id)}
  end
  
  def childern_nutrients(ids=nil, childern=nil)
    ids = ids || @ids
    childern = childern || @childern
    child_ids = (ids || @ids)  - MemberNutrient.where({'member_id' => { "$in" => ids || @ids} }).where(status: "active").group_by(&:member_id).keys
    @childern_nutrients = (childern||@childern).entries.select{|x| child_ids.include?(x.id)}
  end
  
  def childern_timelines(ids=nil, childern=nil)
    ids = ids || @ids || []
    childern = childern || @childern
    child_ids = ids - Timeline.where({'member_id' => { "$in" => ids} }).where("'timeline_at' > " + "'#{(Time.now - 1.month)}'").group_by(&:member_id).keys
     
    @childern_timelines = childern.entries.select{|x| child_ids.include?(x.id)}

  end


  def childerns_immunsation(ids=nil, childern=nil)
    ids = ids || @ids
    childern = childern || @childern
    child_ids = ids - Vaccination.where({'member_id' => { "$in" => ids} }).where(opted:"true").group_by(&:member_id).keys
    
     @childerns_immunsation = childern.entries.select{|x| child_ids.include?(x.id)}
    
  end
  
  def childerns_health(ids=nil, childern=nil)
    ids = ids || @ids
    childern = childern || @childern
    child_ids = ids - Health.where({'member_id' => { "$in" => ids} }).where("'updated_at' > " + "'#{(Time.now - 1.month)}'").group_by(&:member_id).keys
    @childerns_health = childern.entries.select{|x| child_ids.include?(x.id)}
  end
  def childerns_document(ids=nil, childern=nil)
    ids = ids || @ids
    childern = childern || @childern
    child_ids = ids - Document.where({'member_id' => { "$in" => ids} }).group_by(&:member_id).keys
    @childerns_document = childern.entries.select{|x| child_ids.include?(x.id)}
  end

  def truncated_name(child_records)
    child_records.map(&:first_name).to_sentence.truncate(20)
  end

  def nested_family_members_fields
    @member.new_record? ? :family_members : [:family_members,@family_member]
  end    

  def get_email_value
    # debugger
    if @invitation.present?
      email = @invitation.member.email
    elsif session[:email].present?
      # email = params[:user] && params[:user][:email] ? params[:user][:email] : nil
      email = session[:email] ? session[:email] : nil
      session[:email] = nil
    else
      email = nil    
    end  
    email
  end

  def get_password_value
    if session[:password].present?
      password = session[:password] ? session[:password] : nil
      session[:password] = nil
    else
      password = nil    
    end  
    password
  end

  def underscore(str = nil)
    if str
      str.underscore.to_sym
    else
      :other  
    end  
  end  

  def existing_health_keys(health,health_keys)
    health_keys.select{|health_key| eval("health.#{health_key}").present?}
  end  

  def timeline_fb_name(timeline)
    if timeline.timelined_type == "Milestone"
      "#{timeline.member.first_name}'s Milestone achieved - #{timeline.name}"
    else
      # "#{timeline.member.first_name} - #{timeline.name}"
      timeline.name
    end
  end

  def timeline_fb_desc(timeline)
    if timeline.timelined_type == "Milestone" && timeline.timelined
      timeline.timelined.description.present? ? timeline.timelined.description : timeline.timelined.system_milestone.description
    else
      timeline.desc
    end
  end  

  def timeline_image_url(timeline)
    if timeline.timelined_type == "Milestone"      
      image_url = timeline.timelined.pictures.first.fbshare_image_url if timeline.timelined.pictures.present?
      image_url ||= default_image_url(timeline.timelined).gsub(' ','%20') if milestone_default_image_exists?(timeline.timelined)
      image_url ||= "#{APP_CONFIG['default_host']}/images/milestone_small.jpg"
    else
      image_url = timeline.pictures.first.fbshare_image_url if timeline.pictures.present?
      image_url ||= default_image_url(timeline).gsub(' ','%20') if nutrient_default_image_exists?(timeline)
      image_url ||= "#{APP_CONFIG['default_host']}/images/timeline_small_fb_share.jpg"
    end    
    image_url   
  rescue
    "#{APP_CONFIG['default_host']}/images/nurturey_fb.jpg"
  end  

  def get_health_label(data_type)
    case data_type
    when 'height'
      "Height record"
    when 'weight'
      "Weight record"
    when 'bmi'
      "BMI record"
    when 'head_circum'
      "Head Circumference record"
    when 'waist_circum'
      "Waist record"
    else
      "Measurement record"    
    end  
  end  


  def default_image_url(nutrient,api_version=1)
    case (nutrient.class.to_s)
    when 'Milestone'
      url = "#{APP_CONFIG["s3_base_url"]}/default/milestones/#{nutrient.system_milestone.title.gsub('/','_').gsub(',','').gsub(/'/,'')}.jpg" #rescue ""
    when 'Timeline'
      if (nutrient.system_entry == "true" rescue false)
        system_entry = SystemTimeline.where(id: nutrient.timelined_id).first.name
        if ["Fifth birthday","Sixth birthday", "Seventh birthday", "Eighth birthday", "Ninth birthday", "Tenth birthday"].include?(system_entry)
          system_entry = "Fifth birthday" # default image is fifth birthday's image for 6th to 10th birthday 
        end
        url = "#{APP_CONFIG["s3_base_url"]}/default/timelines/small/#{system_entry.gsub(' ','_').downcase}.jpg" #rescue ""
      end
    else
    end
    url = ShaAlgo.encryption(url) if api_version >= 6
    url 
  rescue
    nil
  end


  def default_small_image_url(nutrient,api_version=1)
    case (nutrient.class.to_s)
    when 'Milestone'
      url = "#{APP_CONFIG["s3_base_url"]}/default/milestones/small/#{nutrient.system_milestone.title.gsub('/','_').gsub(',','').gsub(/'/,'')}.jpg" #rescue ""
    when 'Timeline'
      if (nutrient.system_entry == "true" rescue false)
        system_entry = SystemTimeline.where(id: nutrient.timelined_id).first.name rescue ""
        if ["Fifth month birthday","Sixth month birthday", "Seventh month birthday", "Eighth month birthday", "Ninth month birthday", "Tenth month birthday"].include?(system_entry)
          system_entry = "Fifth month birthday" # default image is fifth birthday's image for 6th to 10th birthday 
        end
        url = "#{APP_CONFIG["s3_base_url"]}/default/timelines/small/#{system_entry.gsub(' ','_').downcase}.jpg" #rescue ""
      end
    else
    end
    url = ShaAlgo.encryption(url) if api_version >= 6
    url 
  rescue
    nil
  end

  def nutrient_default_image_exists?(nutrient)
    case (nutrient.class.to_s)
    when 'Milestone'
        nutrient.system_milestone.default_image_exists?
        # uri = URI("#{APP_CONFIG["s3_base_url"]}/default/milestones/#{nutrient.system_milestone.title.gsub('/','_').gsub(',','').gsub(' ','%20').gsub(/'/,'')}.jpg")
        # Rails.logger.info("############## #{APP_CONFIG['s3_base_url']} ###############")
        # request = Net::HTTP.new uri.host
        # response= request.request_head uri.path
        # return response.code.to_i == 200
    when 'Timeline'
      if (nutrient.system_entry.to_s == "true" rescue false)
        system_timeline = SystemTimeline.where(id: nutrient.timelined_id).first
        system_timeline && system_timeline.default_image_exists?
        # nutrient.system_timeline && nutrient.system_timeline.default_image_exists?
        # image_name = SystemTimeline.where(id:nutrient.timelined_id).first.name rescue "no_image"
        # image_name = image_name.gsub(' ','_').downcase.gsub(' ','%20')
        # uri = URI("#{APP_CONFIG["s3_base_url"]}/default/timelines/small/#{image_name}.jpg")
        # request = Net::HTTP.new uri.host
        # response= request.request_head uri.path
        # return response.code.to_i == 200
      else
        false
      end
    else
      false
    end
  end

  def milestone_default_image_exists?(nutrient)
    case (nutrient.class.to_s)
      when 'Milestone'        
        nutrient.system_milestone.default_image_exists?
        # uri = URI("#{APP_CONFIG["s3_base_url"]}/default/milestones/#{nutrient.system_milestone.title.gsub('/','_').gsub(',','').gsub(' ','%20').gsub(/'/,'')}.jpg")
        # Rails.logger.info("############## #{APP_CONFIG['s3_base_url']} ###############")
        # request = Net::HTTP.new uri.host
        # response= request.request_head uri.path
        # return response.code.to_i == 200
      when 'Timeline'
        if (nutrient.system_entry == "true" rescue false)
          system_entry = SystemTimeline.where(id: nutrient.timelined_id).first.name rescue ""
          if ["Fifth month birthday","Sixth month birthday", "Seventh month birthday", "Eighth month birthday", "Ninth month birthday", "Tenth month birthday"].include?(system_entry)
            system_entry = "Fifth month birthday" # default image is fifth birthday's image for 6th to 10th birthday 
          end
          File.exists?("app/assets/images/timelines/#{system_entry.gsub('/','_').gsub(',',' ')}.jpg") rescue nil
        else
          false
        end
      else
        false
      end
  rescue
    nil    
  end
  
  def get_image_if_not_exist(tool,alternative_url)
    check_url = "#{Rails.root}/app/assets/images/mailer/#{tool.identifier.downcase.gsub(" ",'')}.png"
    if FileTest.exist?(check_url)
      "#{APP_CONFIG['default_host']}/assets/mailer/#{tool.identifier.downcase.gsub(" ",'')}.png"
    else
      alternative_url
    end
  end

  def mobile_device_main_launch_param(user_id,options={:user=>nil,:api_token=>nil,:email=>nil, :authentication_status=>nil,:invitation_id=>nil,:invitation_status=>nil})
    if options[:user].present?
      user = options[:user]
      user_id = user.id
      options[:api_token] = user.confirmation_token
      options[:email] = user.email
      options[:authentication_status] =  User.user_authentication_status(user,current_user)
    end
    # {
    #   :user_id=>(user_id rescue ""),
    #   :invitation_id=>options[:invitation_id],
    #   :invitation_status=>options[:api_invitation_status],
    #   :authentication_status=>options[:authentication_status],
    #   :authentication_token=> (options[:api_token] rescue ""), 
    #   :email=> (options[:email] rescue "")
    # }
    "email=#{options[:email]}&authentication_token=#{options[:api_token]}&user_id=#{user_id}&invitation_id=#{options[:invitation_id]}&invitation_status=#{options[:invitation_status]}&authentication_status=#{options[:authentication_status]}"
  end

end
