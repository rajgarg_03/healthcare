module ImmunisationHelper

  def completed_vaccin(vaccin)
    total_jabs = vaccin.jabs.count
    completed_jabs = vaccin.jabs.where(status:"Administered").count
    "#{completed_jabs}"
  end
end
