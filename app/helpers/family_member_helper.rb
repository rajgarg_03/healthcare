module FamilyMemberHelper

	def family_member_type(family_member)
		class_name = if family_member.parent?
			"gray"
		else
			(family_member.role_name == FamilyMember::ROLES[:daughter]) ? "purple" : "blue"
		end
		class_name
	end

end
