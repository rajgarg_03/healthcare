module TimelineHelper

  def born_link_text(member)
    text = "Born, "
     text + member.birth_date.strftime("%d %b %Y") if @timeline_member.birth_date 
  end

  def timeline_year(member)
    current_year = Date.today.year
    birth_year  = member.birth_date.year
    years = []
    if current_year > birth_year
      years =  (birth_year..(current_year-1)).to_a.reverse
    end
    years
  rescue
    nil
  end

   def changed_attribute(attrib)
    Hash[*eval(attrib)] rescue []
  end

  def timeline_month(member)
    begin
      time_months = Date::MONTHNAMES.compact().in_groups_of(Date.today.month).first.reverse
      if member.birth_date.year == Date.today.year
        member_birth_month = member.birth_date.strftime("%B")
        time_months[0..time_months.index(member_birth_month)]
      else
        months
      end
    rescue
      []
    end
  end
  
  def completed_timeline(member,timeline)
    # begin
    timeline_setting = member.member_settings.where(name:"timeline_setting").first
    skipped_timelines = timeline_setting.note.split(",") rescue []
     
    index = skipped_timelines.index(timeline.id.to_s)
    total_entries = SystemTimeline.count
    "#{index} of #{skipped_timelines.length}"
  # rescue
  #   "0 of #{total_entries}"
  # end
  end
  
  def nutrient_link(nutrient,family_member)
    family = family_member.family
    member = family_member.member
    # if family_member.member.id == "547bea2e0a9a99b27e000014"
    #   debugger
    # end
    # begin
      # eval(Nutrient::NUTRIENT_PATH[nutrient.title.downcase.tr(" ","_").to_sym])
      eval(Nutrient::NUTRIENT_PATH[nutrient.title.downcase.tr(" ","_").to_sym]) rescue ""
    # rescue
    #   "#"
    # end
  end

end
