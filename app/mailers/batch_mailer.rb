class BatchMailer < ActionMailer::Base
  default from: "Nurturey Team <support@nurturey.com>"
  helper :application
  layout "mailer"
    
  def trigger_batch_mail(notification_mail_obj,current_user)
    subject = notification_mail_obj.subject
    @message = notification_mail_obj.message
    to = notification_mail_obj.recipient_email
    mail(:to => to, :subject => subject) do |format|
      format.html { render layout: false }
    end
  end
  # Html not ready yet
  # def notification_email_for_identifier_20(user_notification,current_user,member_id,options={})
  #   subject = user_notification.message
  #   @user_notification = user_notification
  #   @birthday_email_for = options[:birthday_email_for]
  #   mail(:to =>current_user.email, :subject => subject) do |format|
  #     format.html { render layout: false }
  #   end  
  # end
  
  def month_birthday_notification_email_for_identifier_19(user,user_notification,options)
    subject = user_notification.message
    @user_notification = user_notification
    @user = user
    @email = user.email
    @child = user_notification.member
    @feed_id =  user_notification.feed_id
    @age_in_month = options[:age_in_month]
    @deeplink_data = @child.get_email_deeplink_data(user,@child,"child","Timeline","child",@feed_id,@user_notification)
    @deeplink = "#{APP_CONFIG["default_host"]}/home?#{@deeplink_data}&source=email_deeplink&token=#{@user.deeplink_api_token}"
    
    @birthday_email_for = options[:birthday_email_for]
    subject = "We wish #{@child.first_name} a very happy #{@age_in_month} month Birthday"
    mail(:to => user.email, :subject => subject) do |format|
      format.html { render layout: false }
    end  
  
  end

  def notification_email_for_identifier_14(user_notification,current_user,member_id,options={})
    @child_member = user_notification.member
    family_id = FamilyMember.where(member_id:@child_member.id).last.family_id
    family_subscription = Subscription.find_family_subscription(family_id).last
    
    @premium_subscription_status = Subscription.is_free?(family_subscription.subscription_listing_order.to_i)
    
    @user = current_user.user
    @email = @user.email
    zscore = ::Child::Zscore.get_calculated_zscore(@child_member.id)
    @zscore_analysis = ::Child::Zscore.zscore_analysis(zscore,@child_member)
    @zscore_deeplink = user_notification.get_deeplink_data(current_user,api_version=4)
    subject = "#{@child_member.first_name}'s Z Score Classification"
 
    mail(:to =>current_user.email, :subject => subject) do |format|
      format.html { render layout: false }
    end
    
  end
 
  def notification_email_for_identifier_15(user_notification,current_user,member_id,options={})
    subject = user_notification.message
    @user_notification = user_notification
    user = current_user.user
    destination_name  = user_notification.destination
    destination_type = user_notification.destination_type
    role = user_notification.role
    @user = user
    @email = user.email

    @child = user_notification.member
    @feed_id =  user_notification.feed_id
    options = {:age_in_month=>@child.age_in_months}
    @upcoming_tooth_hash = Child::ToothChart::ToothDetails.upcoming_tooth_data_hash(@child.id,options)
    @child_name = @child.name

    @gender_pronoun = @child.pronoun

    @tooth_stats_hash = Child::ToothChart::ToothDetails.tooth_stats_data_hash(member_id)
    @recommended_pointers = nil #ArticleRef.get_recommended_pointer(current_user, params[:member_id], "health")

    @erupted_tooth_count = Child::ToothChart::ToothDetails.where(child_id:member_id).erupt_data.count

    updated_at =  Child::ToothChart::ToothDetails.where(child_id:member_id).not_in_unaccomplished_status.order_by("updated_at desc").first.updated_at.convert_to_ago2 rescue nil
    @recently_updated_teeth =  Child::ToothChart::ToothDetails.child_data_hash({:member_id=>member_id,:type=>"all"},"all")[0..2]
    @updated_at_text_default = "Not updated yet"
    @updated_at_text = updated_at.present? ? "Last update #{updated_at}" : @updated_at_text_default

    @deeplink_data = @child.get_email_deeplink_data(user,@child,destination_type,destination_name,role,@feed_id,@user_notification)
    @dental_health_deeplink = "#{APP_CONFIG["default_host"]}/home?#{@deeplink_data}&source=email_deeplink&token=#{@user.deeplink_api_token}"
    

    mail(:to => user.email, :subject => subject) do |format|
      format.html { render layout: false }
    end
  end


  def notification_email_for_identifier_5(user_notification,current_user,member_id,options={})
    subject = user_notification.message
    @user_notification = user_notification
    user = current_user.user
    @child = user_notification.member
    options[:age_category] =   @child.find_category
    options[:child_male] = options[:child_male] || @child.male?
    @child_male = options[:child_male] 
    api_version = 3
     
    @pronoun = Member::PRONOUN1[(@child_male ? "Son": "Daughter")]
    @age_category = options[:age_category]
    destination_name  = user_notification.destination
    destination_type = user_notification.destination_type
    role = user_notification.role
    @user = user
    @email = user.email
    @feed_id =  user_notification.feed_id
    begin
      @last_updated = (Date.today - Milestone.where(member_id:@child.id,:status_by=>"user").order_by("updated_at desc").first.updated_at.to_date).to_i
    rescue Exception=>e
      @last_updated = (Date.today - (Milestone.where(member_id:@child.id).order_by("updated_at desc").first.updated_at || @child.birth_date).to_date).to_i rescue 0
    end
    @last_updated = @last_updated < 0 ? 0 : @last_updated
    @achieved_milestones_in_last_6_month = Milestone.where(:member_id=>@child.id,:milestone_status=>"accomplished",:status_by=>"user",:date.gte=>(Date.today - 6.months) ).order_by("date desc").limit(3)
    @member_milestone_stats = Milestone.milestone_stats(current_user,@child,api_version,options)
    
    milestone_data = Milestone.member_milestone_graph_data(@child,current_user,options)
    @achieved_percentage = milestone_data[:milestones][:achieved_percentage]  rescue 0
    @achieved_percentage = @achieved_percentage.to_i == @achieved_percentage ? @achieved_percentage.to_i : (@achieved_percentage.round(2) rescue @achieved_percentage)
       
    # Milestone.get_achieved_percentage_for_age_range(current_user,@child,api_version,options).to_d.truncate(2).to_f.to_i    
    
    @deeplink_data = @child.get_email_deeplink_data(user,@child,destination_type,destination_name,role,@feed_id,@user_notification)
    @milestone_deeplink = "#{APP_CONFIG["default_host"]}/home?#{@deeplink_data}&source=email_deeplink&token=#{@user.deeplink_api_token}"
    
    mail(:to => user.email, :subject => subject) do |format|
      format.html { render layout: false }
    end
  end

  def pointer_email(current_user,user_notification,options={})
    pointer_s_id = user_notification.identifier
    @pointer =  ArticleRef.where(:s_id=> pointer_s_id).last
    @child_member = @pointer.member
    @current_user = current_user
    @user = current_user.user
    @pointer_deeplink = user_notification.get_deeplink_data(current_user,api_version=4)
    @email = @user.email

    subject = @pointer.name
    mail(:to =>current_user.email, :subject => subject) do |format|
      format.html { render layout: false }
    end
  end
  
  def immunisation_jab_reminder(user_notification,current_user,member_id,jabs,options={})
    member = Member.find(member_id)
    @jabs = jabs
     
    # @vaccine = vaccine
    # @index = vaccin.jabs.pluck(:id).sort.index(jab.id)+1 rescue 1
    user_notification = user_notification || UserNotification.new(destination_type:"child",feed_id:member_id,destination:"Immunisations",message:"Immunisation reminder for #{member.first_name}")
    subject = user_notification.message
    @user_notification = user_notification
    user = current_user.user
    destination_name  = user_notification.destination
    destination_type = user_notification.destination_type
    role =  FamilyMember.where(member_id:member_id).last.role rescue "User"
    @user = user
    @email = user.email
    @child = member
    @feed_id =  user_notification.feed_id
    @deeplink_data = @child.get_email_deeplink_data(user,@child,destination_type,destination_name,role,@feed_id,@user_notification)
    @immunisation_deeplink = "#{APP_CONFIG["default_host"]}/home?#{@deeplink_data}&source=email_deeplink&token=#{@user.deeplink_api_token}"
    
    mail(:to => user.email, :subject => subject) do |format|
      format.html { render layout: false }
    end
  end  

  
  def notification_email_for_identifier_24(user_notification,current_user,member_id,options={})
    user = current_user.user
    @susbcription_price_list = SubscriptionManager.get_price_list(current_user,options)
    api_version = 5
    destination_type =  "user"
    role = "User"
    destination_name = "premium_subscription"
    @email = user.email
    @deeplink_data = current_user.get_email_deeplink_data(user,nil,destination_type,destination_name,role,@feed_id,user_notification)
    @tool_list = Subscription.tool_list(current_user,api_version,true) 
    @deeplink = "#{APP_CONFIG["default_host"]}/home?#{@deeplink_data}&source=email_deeplink&token=#{current_user.user.deeplink_api_token}"
    subject = user_notification.message
     mail(:to => user.email, :subject => subject) do |format|
      format.html { render layout: false }  
    end
  end 

  def notification_email_for_identifier_25(user_notification,current_user,member_id,options={})
   
    user = current_user.user
    @susbcription_price_list = SubscriptionManager.get_price_list(current_user,options)
    api_version = 5
    destination_type =  "user"
    role = "User"
    @email = user.email
    destination_name = "premium_subscription"
    @deeplink_data = current_user.get_email_deeplink_data(user,nil,destination_type,destination_name,role,@feed_id,user_notification)
    @tool_list = Subscription.tool_list(current_user,api_version,true) 
    @deeplink = "#{APP_CONFIG["default_host"]}/home?#{@deeplink_data}&source=email_deeplink&token=#{current_user.user.deeplink_api_token}"
    subject = user_notification.message
     mail(:to => user.email, :subject => subject) do |format|
      format.html { render layout: false }  
    end
  end 


 def notification_email_for_identifier_26(user_notification,current_user,member_id,options={})
  user = current_user.user
  child = Member.find(member_id)
  destination_type = user_notification.destination_type
  role = "User"
  @email = user.email
  @child_name = child.first_name
  destination_name = user_notification.destination
  @deeplink_data = current_user.get_email_deeplink_data(user,child,destination_type,destination_name,role,@feed_id,user_notification)
  @deeplink = "#{APP_CONFIG["default_host"]}/home?#{@deeplink_data}&source=email_deeplink&token=#{current_user.user.deeplink_api_token}"
  subject = user_notification.message
   mail(:to => user.email, :subject => subject) do |format|
    format.html { render layout: false }  
  end
end 


def notification_email_for_identifier_28(user_notification,current_user,member_id,options={})
  user = current_user.user
  child = Member.find(member_id)
  destination_type = user_notification.destination_type
  role = "User"
  @email = user.email
  @child_name = child.first_name
  destination_name = "alexa_skill"#user_notification.destination
  @deeplink_data = current_user.get_email_deeplink_data(user,child,destination_type,destination_name,role,@feed_id,user_notification)
  @deeplink = "#{APP_CONFIG["default_host"]}/home?#{@deeplink_data}&source=email_deeplink&token=#{current_user.user.deeplink_api_token}"
  # @deeplink = "alexa://alexa?fragment=skills/dp/B078TMBZJV"
  subject = user_notification.message
   mail(:to => user.email, :subject => subject) do |format|
    format.html { render layout: false }  
  end
end


  def notification_email_for_identifier_27(user_notification,current_user,member_id,options={})
    user = current_user.user
    member = Member.find(member_id)
    api_version = 5
    destination_type = user_notification.destination_type
    role = "User"
    @email = user.email
    pregnancy = Pregnancy.where(expected_member_id:member_id).last
    destination_name = user_notification.destination
     
    @deeplink_data = current_user.get_email_deeplink_data(user,nil,destination_type,destination_name,role,@feed_id,user_notification)
    @tool_list = Nutrient.get_member_tools(member,current_user,api_version).where(:identifier.nin=>["Manage Tools"])
    @deeplink = "#{APP_CONFIG["default_host"]}/home?#{@deeplink_data}&source=email_deeplink&token=#{current_user.user.deeplink_api_token}"
    @user_name = user.first_name
    pregnancy_week,pregnancy_status =  Pregnancy.calculate_pregnancy_week(pregnancy.expected_on)
    @pregnancy_week = pregnancy_week.to_s + "th week"
    expected_date = pregnancy.expected_on.to_date 
    @estimated_due_date = expected_date.strftime("%d %b %Y")
   

    prenatal_test =  member.medical_events.not_in(:opted => "false").where(:est_due_time.gt=> Date.today).limit(1).first
    prenatal_test_date = prenatal_test.est_due_time.to_date  rescue nil
    @first_parental_test_date = prenatal_test_date.strftime("%d %b %Y") rescue nil
    
    subject = user_notification.message
     mail(:to => user.email, :subject => subject) do |format|
      format.html { render layout: false }  
    end
  end 


  def notification_email_for_identifier_29(user_notification,current_user,member_id,options={})
  
  user = current_user.user
  @child = Member.find(member_id)
  destination_type = user_notification.destination_type
  role = "User"
  api_version = 5
  @user_name = user.first_name
  @family_name = Family.find(user_notification.family_id).name
  @child_role = @child.is_girl? ? "girl" : "son"
  @child_name = @child.first_name
  @child_dob = @child.birth_date.strftime("%d %b %Y") rescue nil
  @child_age = @child.member_age

  @feed_id =  user_notification.feed_id rescue nil
  member_vaccination_ids = Vaccination.where(:member_id=>@child.id.to_s).pluck(:id).map(&:to_s)
  @jab_count = Jab.where(:vaccination_id.in=>member_vaccination_ids).uniq.count
  
  # @jab_count = Vaccination.where(member_id:@child.id).total_jabs_count rescue nil
  tool_list_first_section = Nutrient.in(identifier:["Measurements","Tooth Chart", "Milestones"])
  @ordered_tool_list_first_section = Array.new(3)
  tool_list_first_section.each_with_index do |tool, index| 
    if tool[:identifier] == "Measurements"
      @ordered_tool_list_first_section[0] = tool 
    elsif tool[:identifier] == "Tooth Chart"
      @ordered_tool_list_first_section[1] = tool
    else
      @ordered_tool_list_first_section[2] = tool
    end
      
  end

  @tool_list_second_section = Nutrient.in(identifier:["Z score","Child Health Card", "Checkup Planner", "Immunisations", "Mental Maths"])
  
  @manage_tool_deeplink_data = current_user.get_email_deeplink_data(user,nil,destination_type,"Manage Tools",role,@feed_id,user_notification)
  @manage_tool_deeplink = "#{APP_CONFIG["default_host"]}/home?#{@manage_tool_deeplink_data}&source=email_deeplink&token=#{current_user.user.deeplink_api_token}"

  @milestones_deeplink_data = @child.get_email_deeplink_data(user,@child,destination_type,"Milestones",role,@feed_id,user_notification)
  @milestones_deeplink = "#{APP_CONFIG["default_host"]}/home?#{@milestones_deeplink_data}&source=email_deeplink&token=#{user.deeplink_api_token}"

  @tooth_chart_deeplink_data = @child.get_email_deeplink_data(user,@child,destination_type,"Tooth Chart",role,@feed_id,user_notification)
  @tooth_chart_deeplink = "#{APP_CONFIG["default_host"]}/home?#{@tooth_chart_deeplink_data}&source=email_deeplink&token=#{user.deeplink_api_token}"

  @measurements_deeplink_data = @child.get_email_deeplink_data(user,@child,destination_type,"Measurements",role,@feed_id,user_notification)
  @measurements_deeplink = "#{APP_CONFIG["default_host"]}/home?#{@measurements_deeplink_data}&source=email_deeplink&token=#{user.deeplink_api_token}"
  
  @email = user.email
  subject = user_notification.message
   mail(:to => user.email, :subject => subject) do |format|
    format.html { render layout: false }  
  end
end

  def notification_email_for_identifier_30(user_notification,current_user,member_id,options={})
    user = current_user.user
    @user_notification = user_notification
    @current_user = current_user
    @child = Member.find(member_id)
    @not_yet_updated = "Not yet updated"
    @pregnancy = Pregnancy.where(expected_member_id:member_id).last
    destination_type = user_notification.destination_type
    role = "Child"
    blank_values = ["",nil, 0]
    @metric_system = current_user.metric_system
    api_version = 5
    @pregnancy_week = @pregnancy.pregnancy_week
    @pregnancy_week_text = PregnancyProgressBarManager.where(:pregnancy_week=>@pregnancy_week).last.description  rescue nil #{}"At this stage of your pregnancy, Inhale, exhale: your baby can now breathe."
    # prenatal test detail
    @upcoming_prenatal_test =  MedicalEvent.where(:status.ne=>"Completed",:member_id=>member_id,:est_due_time.gte=> Date.today).order_by("est_due_time asc" ).first
    destination_name = "Prenatal Tests"
    @prenatal_test_deeplink  =  "#{APP_CONFIG["default_host"]}/home?" + @child.get_email_deeplink_data(user,@child,destination_type,destination_name,role,@feed_id,@user_notification)
    # pregnancy health card detail
    pregnancy_last_weight = @pregnancy.pregnancy_measurements.not_in(:weight => blank_values).order_by("test_date desc").first #|| Health.new
    @pregnancy_last_weight = pregnancy_last_weight.api_convert_to_unit(@metric_system) rescue {}
    @measurment_updated_at_text = @pregnancy_last_weight["last_updated_at"].to_time.convert_to_ago2 rescue nil
    destination_name = "Pregnancy Health Card"
    action_name = user_notification.action_name
    user_notification.action_name = "view"
    @pregnancy_health_card_deeplink  = "#{APP_CONFIG["default_host"]}/home?" + @child.get_email_deeplink_data(user,@child,destination_type,destination_name,role,@feed_id,@user_notification)
   
    @pregnancy_last_hemoglobin = Pregnancy::HealthCard::Hemoglobin.where(pregnancy_id:@pregnancy.id).order_by("test_date desc").first
    @hemoglobin_updated_at_text = @pregnancy_last_hemoglobin.created_at.convert_to_ago2 rescue nil
    @pregnancy_last_bp = Pregnancy::HealthCard::BloodPressure.where(pregnancy_id:@pregnancy.id.to_s).order_by("test_date desc").first
    @bp_updated_at_text = @pregnancy_last_bp.created_at.convert_to_ago2 rescue nil
    # Kick counter
    @kick_counter_records_count = Pregnancy::KickCounter::KickCountDetail.where(:pregnancy_id => @pregnancy.id.to_s).where(:start_time=>{"$gte"=>(Date.today - 1.week),"$lte"=>Date.today}).count
    @total_kick_count = Pregnancy::KickCounter::KickCountDetail.where(:pregnancy_id => @pregnancy.id.to_s).count
    @avg_kick_count = Pregnancy::KickCounter::KickCountDetail.get_avg_kick_count(@pregnancy, 2,{:start_time=>(Date.today - 1.week),:end_time=>Date.today}).gsub("kicks","baby kicks").gsub("Avg ","").gsub("hours per day","hours/day")
    destination_name = "Kick Counter"
    @kick_counter_deeplink  =  "#{APP_CONFIG["default_host"]}/home?" + @child.get_email_deeplink_data(user,@child,destination_type,destination_name,role,@feed_id,@user_notification)
   
    #recommneded pointer
    @recommneded_pointers = ArticleRef.get_recommended_pointer(current_user,member_id,"pregnancy",{:list_all=>true}).limit(3)
    destination_name = "Pointers"
    destination_type = "User"
    role = "User"
    @pointer_deeplink  =  "#{APP_CONFIG["default_host"]}/home?" + @child.get_email_deeplink_data(user,@child,destination_type,destination_name,role,@feed_id,@user_notification)
   
    @feed_id =  user_notification.feed_id rescue nil
    @email = user.email
    subject = user_notification.message
     mail(:to => user.email, :subject => subject) do |format|
      format.html { render layout: false }  
    end
  end

  

  def notification_email_for_identifier_31(user_notification,current_user,member_id,options={})
    user = current_user.user
    @email = user.email
    subject = user_notification.message
     mail(:to => user.email, :subject => subject) do |format|
      format.html { render layout: false }  
    end
  end


def notification_email_for_identifier_32(user_notification,current_user,member_id,options={})
    user = current_user.user
    @member = Member.find(member_id)
    api_version = 5
    destination_type = user_notification.destination_type
    role = "User"
    @email = user.email
    destination_name = user_notification.destination
    @feed_id =  user_notification.feed_id rescue nil

   
    @deeplink_data = current_user.get_email_deeplink_data(user,@member,destination_type,destination_name,role,@feed_id,user_notification)
    @deeplink = "#{APP_CONFIG["default_host"]}/home?#{@deeplink_data}&source=email_deeplink&token=#{current_user.user.deeplink_api_token}"
    @user_name = user.first_name
    
    @child_name = @member.first_name

    yesterday_date = Date.today - 1.days # to get preivous month
    previous_week = Date.today.prev_week
    start_date = previous_week
    end_date = previous_week + 6.day
    @date_interval_string = start_date.to_date3() + " - " + end_date.to_date3()

    quiz_summary = Child::AlexaQuiz::QuizDetail.get_complete_summary({:member_id => @member.id},current_user,4,{:start_date=>start_date, :end_date=>end_date})
    
    @total_questions = quiz_summary["mental_math_summary"]["total_questions"].to_i
    @last_session_date =  quiz_summary["mental_math_summary"]["last_session_held"]
    @total_practice_days = quiz_summary["mental_math_summary"]["total_practiced_days"].to_i 
    @avg_questions_per_day = quiz_summary["mental_math_summary"]["avg_questions_per_day"].to_i
    

    total_correct_answers = 0.0

    daily_records = quiz_summary["mental_math_summary"]["daily_records"]
    @total_session_held = daily_records.count
    topic_data = {}
    daily_records.each do |daily_record|
      total_correct_answers = daily_record["total_correct_answer"].to_i + total_correct_answers.to_i
        daily_record["topics_summary"].each do |topic_summary|
        topic_data[topic_summary["topic_name"]] = topic_data[topic_summary["topic_name"]].to_i + topic_summary["total_question"].to_i
      end
    end

    @accuracy_score_percentage = @total_questions == 0 ? 0 : (total_correct_answers.to_f/@total_questions) * 100
    @most_practiced_question = Hash[topic_data.sort_by {|k,v| v}.reverse].keys.first
    @practice_status = quiz_summary["mental_math_summary"]["practice_status"]
    
    subject = user_notification.message
     mail(:to => user.email, :subject => subject) do |format|
      format.html { render layout: false }  
    end
  end 


def notification_email_for_identifier_33(user_notification,current_user,member_id,options={})
    user = current_user.user
    @email = user.email
    @pregnancy = Pregnancy.where(expected_member_id:member_id).last
    api_version = 5
    destination_type = user_notification.destination_type
    role = "User"
    @email = user.email
    destination_name = user_notification.destination
    @feed_id =  user_notification.feed_id rescue nil

   
    @deeplink_data = current_user.get_email_deeplink_data(user,nil,destination_type,destination_name,role,@feed_id,user_notification)
    @deeplink = "#{APP_CONFIG["default_host"]}/home?#{@deeplink_data}&source=email_deeplink&token=#{current_user.user.deeplink_api_token}"
    tool_list = Nutrient.in(identifier:["Measurements","Immunisations", "Milestones", "Z score", "Child Health Card", "Mental Maths"])
    @tools_hash = {}
    tool_list.each do |tool|
      @tools_hash[tool[:identifier]] = tool
    end
    subject = user_notification.message
     mail(:to => user.email, :subject => subject) do |format|
      format.html { render layout: false }  
    end
  end


  def notification_email_for_identifier_35(user_notification,current_user,member_id,options={})
    user = current_user.user
    api_version = 6
    
    @email = user.email
    @link = "#{APP_CONFIG['default_host']}/designasmile"
    
    subject = user_notification.message
     mail(:to => user.email, :subject => subject) do |format|
      format.html { render layout: false }  
    end
  end


  def notification_email_for_identifier_37(user_notification,current_user,member_id,options={})
    user = current_user.user
    @email = user.email
    api_version = 6
    destination_type = user_notification.destination_type
    role = "User"
    @email = user.email
    destination_name = user_notification.destination
    @feed_id =  user_notification.feed_id rescue nil

   
    @deeplink_data = current_user.get_email_deeplink_data(user,nil,destination_type,destination_name,role,@feed_id,user_notification)
    @deeplink = "#{APP_CONFIG["default_host"]}/home?#{@deeplink_data}&source=email_deeplink&token=#{current_user.user.deeplink_api_token}"
    
    subject = user_notification.message
     mail(:to => user.email, :subject => subject) do |format|
      format.html { render layout: false }  
    end
  end

  def notification_email_for_identifier_39(user_notification,current_user,member_id,options={})
    user = current_user.user
    api_version = 6
    destination_type =  "user"
    role = "User"
    destination_name = "Manage GP"
    @feed_id =  user_notification.feed_id rescue nil
    @deeplink_data = current_user.get_email_deeplink_data(user,nil,destination_type,destination_name,role,@feed_id,user_notification)
    @deeplink = "#{APP_CONFIG["default_host"]}/home?#{@deeplink_data}&source=email_deeplink&token=#{current_user.user.deeplink_api_token}"

    subject = user_notification.message
     mail(:to => user.email, :subject => subject) do |format|
      format.html { render layout: false }  
    end
  end

end
