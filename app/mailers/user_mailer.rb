class UserMailer < ActionMailer::Base
  helper :application
  default from: "Nurturey Team <support@nurturey.com>"
  layout "mailer"

  # Invite a member to join family
  def elder_member_request_email(invitation,pregnancy=nil,options={})
    @invitation = invitation
    @invitee = @invitation.member rescue nil
    @user = @invitee.user
    @mother_for_pregnancy = pregnancy.present?
    @pregnancy = nil
    @role = @invitation.role.downcase
    @email =  @invitee.email #rescue nil
    @family = @invitation.family
    @family_owner = @family.owner
    @url = verify_elder_for_family_member_url(@invitation)
    if(UserMailer.user_registered_for_email?(@invitation.member.email))
      mail(:to => @invitation.member.email, :subject => "You have received invitation to join family from #{@invitation.family.owner.first_name}") do |format|
        format.html { render layout: "/updated_mailer_layout/updated_mailer" }
      end
    end
  end
 
  
  #Email sent to Family(eg A) owner to notify request from member to join Family(A)
  def join_family_request_email(invitation)
    @invitation = invitation
    @receiver = @invitation.family.owner
    # Request from <name> to join <family name>
    if(UserMailer.user_registered_for_email?(@receiver.email))
      mail(:to => @receiver.email, :subject => "Request from #{@invitation.member.name} to join #{@invitation.family.name}") do |format|
        format.html { render layout: false }
      end
    end
  end  

#invite a friend to nurturey
  def invite_friend_email(invitation,mail_content)
    @invitation = invitation
    @mail_content = mail_content
    @url = verify_elder_member_url(@invitation)
    if(UserMailer.user_registered_for_email?(@invitation.member.email))
      mail(:to => @invitation.member.email, :subject => "You have received invitation to join Nurturey from #{@invitation.sender.first_name}") do |format|
        format.html { render layout: 'common_email' }
      end
    end
  end
  #  UserMailer.welcome_email(User.first).deliver!
  def welcome_email(user)
    @user = user
    @email = user.email
    # @host  = default_url_options[:host] #+"/verify_account?id=" + @user.email_verification_hash
    if(UserMailer.user_registered_for_email?(user.email))
      mail(:to => user.email, :subject => "Welcome to Nurturey, start your journey in just a few clicks!") do |format|
        format.html { render layout: false }
      end
    end
  end

   
   
  #join to a family request from family owner to Invitee
  def family_associate_request_email(family_member,to_user)
    @family_member = family_member
    @family = family_member.family
    @family_owner = @family.owner
    @to_user_name = to_user
    @url  = "#{default_url_options[:host]}#{verify_family_member_path(:id => @family_member.email_verification_hash, :verify_request => true)}"
    if(UserMailer.user_registered_for_email?(@family_member.family.user.email))
    
      mail(:to => @family_member.family.user.email, :subject => "Request To Verify Family Member") do |format|
        format.html { render layout: 'common_email' }
      end
    end
  end

   
   
 #if user accept/decline join invitation
  def notify_sender_for_invitation(invitation)
    @invitation = invitation
    @status = invitation.status
    @sender = invitation.sender_id.present? ? Member.where(id: invitation.sender_id).first : invitation.member
    # @receiver = invitation.member #who accept the request
    @receiver = invitation.sender_id.present? ? invitation.member : invitation.family.owner
    @owner_requested_user = invitation.sender_id.present?
    if(UserMailer.user_registered_for_email?(@sender.email))
      mail(:to => @sender.email, :subject => "Request to join Family #{@status}") do |format|
        format.html { render layout: 'common_email' }
      end
    end
  end  

  # Used from devise
  # def forgot_password_email(user,password)
  #   @user = user
  #   @password = password
  #   @url  = default_url_options[:host]+"/"
  #   mail(:to => user.email, :subject => "Password Reset Information.")
  # end

  def enquiry_email(email, name)
    @url  = default_url_options[:host]+"/"
    mail(:to => user.email, :subject => "Forgot Password.")
  end

  def secondary_email(recipient, subject, url, sent_at = Time.now)
    @subject = subject
    @recipients = recipient.email
    #@from = 'reachrohitroy@gmail.com'
    @sent_on = sent_at
    @url = url
    @body = {}
    @body["email"] = 'sender@yourdomain.com'
    @headers = {}
      mail(:to => @recipients, :subject => @subject)
  end

 def first_birthday_email(user,child,options={})
  @user = user
  @child = child
  @user_notification = options[:user_notification]
  @deeplink_data = child.get_email_deeplink_data(user,child,"child","Timeline","child",@feed_id,@user_notification)
  @deeplink = "#{APP_CONFIG["default_host"]}/home?#{@deeplink_data}"
  if(UserMailer.user_registered_for_email?(user.email))
    mail(:to => user.email, :subject => "We wish #{@child.first_name} a very Happy 1st Birthday")
  end
 end
 
 def second_birthday_email(user,child,options={})
  @user = user
  @child = child
  @user_notification = options[:user_notification]

  @deeplink_data = child.get_email_deeplink_data(@user,@child,"child","Timeline","child",@feed_id,@user_notification)
  @deeplink = "#{APP_CONFIG["default_host"]}/home?#{@deeplink_data}"

  mail(:to => user.email, :subject => "We wish #{@child.first_name} a very Happy 2nd Birthday")
 end

 def third_birthday_email(user,child,options={})
  @user = user
  @child = child
  @user_notification = options[:user_notification]
  @deeplink_data = child.get_email_deeplink_data(@user,@child,"child","Timeline","child",@feed_id,@user_notification)
  @deeplink = "#{APP_CONFIG["default_host"]}/home?#{@deeplink_data}"
  if(UserMailer.user_registered_for_email?(user.email))
    mail(:to => user.email, :subject => "We wish #{@child.first_name} a very Happy 3rd Birthday")
  end
 end

 def fourth_birthday_email(user,child,options={})
  @user = user
  @child = child
  @user_notification = options[:user_notification]
  @deeplink_data = child.get_email_deeplink_data(user,child,"child","Timeline","child",@feed_id,@user_notification)
  @deeplink = "#{APP_CONFIG["default_host"]}/home?#{@deeplink_data}"
  
  if(UserMailer.user_registered_for_email?(user.email))
    mail(:to => user.email, :subject => "We wish #{@child.first_name} a very Happy 4th Birthday")
  end
 end
     
 def generic_birthday_email(user,child,options={})
  @user = user
  @child = child
  @user_notification = options[:user_notification]
  @deeplink_data = child.get_email_deeplink_data(user,child,"child","Timeline","child",@feed_id,@user_notification)
  @deeplink = "#{APP_CONFIG["default_host"]}/home?#{@deeplink_data}"
  if(UserMailer.user_registered_for_email?(user.email))
    mail(:to => user.email, :subject => "We wish #{@child.first_name} a very Happy Birthday")
  end
 end

 def remind_event(user,jab,vaccin,member_id)
  @user = user
  @jab = jab
  @vaccin = vaccin
  @index = vaccin.jabs.map(&:id).sort.index(jab.id)+1 rescue 1
  member = ChildMember.where(id:member_id).first
  if(UserMailer.user_registered_for_email?(user.email))
    mail(:to => user.email, :subject => "Immunisation reminder for #{member.first_name}")
  end
 end

 # def remind_child_event(user,event)
 #  mail(:to => user.email, :subject => "Childern event reminder")
 # end

 def remind_vaccin(user,jab,vaccin,member_id)
  @user = user
  member = ChildMember.where(id:member_id).first
  if(UserMailer.user_registered_for_email?(user.email))
    mail(:to => @user.email, :subject => "Upcoming immunisation for #{member.first_name}")
  end
 end


  
 
 def weekly_report_to_admin(start_date,end_date,recipient)
    attachments['report.xls'] = File.read('./tmp/data_import.xls')
    from_date = start_date.present? ? start_date.strftime("%d-%m-%Y") : 7.days.ago.strftime("%d-%m-%Y")
    curr_date = end_date.present? ? end_date.strftime("%d-%m-%Y") : Date.today.strftime("%d-%m-%Y")
    mail(to: recipient, subject: "Weekly report from #{from_date} to #{curr_date}")
  end
  
 def proactive_content(user,links,type=nil)
  @user  = user
  @links = links
  @type = type
  if(UserMailer.user_registered_for_email?(user.email))
    mail(:to => user.email, :subject => "proactive content - #{type}")
  end
 end

def send_device_version_email(member)
    @user = member
    mail(:to => member.email, :subject => "Please update latest Nurturey app to be ready for iOS 10")
 end


  def notify_dev_for_error(exception_error,exception_backtrace,params,subject)
    Rails.logger.info "#{subject}.........#{exception_error}"
    # self.message.perform_deliveries = false
    
    # @exception = exception_error
    # @exception_backtrace = exception_backtrace
    # params.delete(:token)
    # params.delete(:authentication_token)
    # @params = params
    # to = "ronit.garg@gmail.com"
    # cc = "dharmendra@nurturey.com"
    # mail(:to => to,:cc=>cc, :subject => "Alert:#{subject}- #{Rails.env}")
  end


  def notify_dev_for_verification_email(verification_token,options={})
    @verification_token = verification_token
    to = "ronit.garg@gmail.com"
    cc = "dharmendra@nurturey.com"
    mail(:to => to,:cc=>cc, :subject => "Alert: Email verification failed- #{Rails.env}")
  end


  def notify_dev_for_account_access(subject,message,options={})
    to = "ronit.garg@gmail.com"
    cc = "dharmendra@nurturey.com"
    @message = message
    mail(:to => to,:cc=>cc, :subject => "Account Access Alert:#{subject}- #{Rails.env}")
  end

   def notify_admin_for_system_security_alert(system_security,options={})
    if Rails.env != "production"
      to = "ronit.garg@gmail.com"
      cc = "dharmendra@nurturey.com"
    else
      to = 'support@nurturey.com'
      cc = "dharmendra@nurturey.com"
    end
    @system_security = system_security
    mail(:to => to,:cc=>cc, :subject => "Security Alert: #{system_security.mail_msg}- #{Rails.env}")
  end
  
  def send_identifier_18_report(status="pending")
      to = APP_CONFIG["admin_email"]
      @families = UserNotification.where(type:"18",email_status:status).group_by{|notification| notification.family_id}.entries
      mail(:to => to, :subject => "Subscription mail assigned report") do |format|
      format.html { render layout: false }
    end
  end

def subscription_taken(current_user,family_id,subscription=nil)
  @current_user = current_user
  @user = current_user.user
  if subscription.blank?
    @subscription_user = Subscription.where(family_id:family_id).last.member rescue Member.new
  else
    @subscription_user = subscription.member rescue Member.new
  end
  @family =  Family.find(family_id)
  member_id = @family.family_youngest_child.id rescue nil
  @manage_family_deeplink = {tool_id:nil,tool_feature_type:nil, title:nil, email:@user.email,member_id: member_id, destination_name:"Manage Family", authentication_token:@user.deeplink_api_token,user_id:@user.id.to_s,role:"User",feed_id:family_id,family_id:family_id,action_name:"add",destination_type:"User".downcase}
  manage_tool = Nutrient.where(identifier:"Manage Tools").last
  # tool_id = MemberNutrient.where(member_id:member_id,nutrient_id:tool_id).id rescue nil
  @manage_tools_deeplink = {tool_id:manage_tool.id,:tool_feature_type=>manage_tool.feature_type, title:nil, email:@user.email,member_id: member_id, destination_name:"Manage Tools", authentication_token:@user.deeplink_api_token,user_id:@user.id.to_s,role:"child",feed_id:family_id,family_id:family_id,action_name:"view",destination_type:"child".downcase}
  api_version = Nutrient.all.order_by("supported_api_version desc").first.osupported_api_version rescue 4
  @premium_tools = Nutrient.premium_tools.where(:launch_status=>"launched").not_in(:feature_type=>["button","box"])


  user_notification_for_manage_tool = UserNotification.new(destination:"Manage Family")
  if(UserMailer.user_registered_for_email?(current_user.email))
    mail(:to => current_user.email, :subject => "Thank you, let's get started with Nurturey's premium subscription") do |format|
      format.html { render layout: false }
    end
  end
end

 private

  def self.user_registered_for_email?(email)
    user = User.where(email:email).first
    if user && user.unregister_from_mail== true
      false
    else
      true
    end
  end
  
end
