class CampaignMailer < ActionMailer::Base
   default from: "Nurturey Team <support@nurturey.com>"
   layout "mailer"  
  
    
  #Email sent to user who are not login in last 45 days
  def uninstall_callback_campaign(user,uninstall_callback_campaign)
    @user = user
    @uninstall_callback_campaign =  uninstall_callback_campaign
    mail(:to => user.email, :subject => "Do you know what happened to your PA?") do |format|
      format.html { render layout: false }
    end
  end  

  
end
