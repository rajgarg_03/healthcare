class ClinicMailer < ActionMailer::Base
  helper :application
  default from: "Nurturey Team <support@nurturey.com>"
  layout "mailer"

  # Send otp for access clinic
  def otp_for_clinic_access_authentication(email,member_id,user_id,otp,options={})
    @user = User.find(user_id)
    @otp = otp
    @email= email
    mail(:to => email, :subject => "Nurturey: Account verification code") do |format|
      format.html { render layout: false }
    end
  end
end