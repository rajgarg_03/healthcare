class SysMedicalEventTime
  include Mongoid::Document
  include Mongoid::Timestamps
  field :category_title,    :type => String
  field :test_expected_on,  :type => String
  field :country_code,      :type => String
  def self.get_component_test_data(record)
  	# data = SysMedicalEvent.where(category_title:record.category_title)
  	data = SysMedicalEvent.where(category_title:record.category_title,:country=>/^#{record.country_code}$/i)
  	fields = SysMedicalEvent.attribute_names
    fields.delete("_id")
    fields.delete("created_at")
    fields.delete("updated_at")
    [data,fields]
  end  
end