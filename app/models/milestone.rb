class Milestone
  include Mongoid::Document
  include Mongoid::Timestamps  
  require 'bigdecimal/util'
  field  :member_id,          :type => String
  field  :system_milestone_id,:type => String
  field  :description,        :type => String
  field	 :date,               :type => Date
  field  :content_type,       :type => String     
  field  :display_default_image,       :type => Boolean, :default => true
  field  :milestone_status, :type=>String, :default=>"accomplished" #eg [accomplished, unknown/, unaccomplished, removed]   
  field  :status_by, :type=>String, :default=>"system" #eg system/nil/user    
  CATEGORIES_RANGE_VALUE = {"0-2"=>1,"2-4"=>2,"4-6"=>3,"7-9"=>4, "9-12"=>5,"13-18"=>6,"19-24"=>7,"24-36"=>8,"37-48"=>9,"49-60"=>10}

  SYSTEMMILESTONE_VALUE =
    {
      "Social & Emotional" => "social and emotional",
      "Language/Communication" => "language and communication",
      "Cognitive Development" => "cognitive development",
      "Movement/Physical Development" => "physical development"
    }
 
  attr_accessor :child_male_status  
  validates :system_milestone_id, :member_id, :presence => true

  validates :system_milestone_id, uniqueness: {scope: :member_id}

  validate :accomplished_date_cannot_be_greater_than_today  

  has_many :pictures, :as => :imageable, :dependent => :destroy

  validates_associated :pictures
  
  belongs_to :member  
  belongs_to :system_milestone

  after_create :enable_milestone_alert, :add_point
  after_save :add_to_timeline
  after_destroy :delete_timeline
  after_destroy :remove_point
  index ({member_id:1})
  index ({system_milestone_id:1})

  #used in article refs
  def achieve_date
    if self.date 
      self.date.to_date
    else
     system_milestone.get_expected_date(member).to_date rescue Date.today
   end
  end
  # To be remove after scenario detail fixed
  def date_achieve
    achieve_date
  end

  def progress_score(system_milestone,child)
     scale =  GlobalSettings::MilestoneProgressCalculationFactors[:scale]
     milestone = self
     return (1/(2.to_f) * scale).round(1) if self.milestone_status == "accomplished" && (self.date.blank? || milestone.status_by == "system")

     child_age_in_day = (milestone.milestone_status == "accomplished" && milestone.status_by == "user") ? milestone.milestone_added_at_child_age_in_days(child)  : (Date.today - child.birth_date).to_i
     x2 = (child_age_in_day) ** GlobalSettings::MilestoneProgressCalculationFactors[:power]
     cf2 = GlobalSettings::MilestoneProgressCalculationFactors[:cf2] 
     cf1 = GlobalSettings::MilestoneProgressCalculationFactors[:cf1]  
     
     c = 2 * (system_milestone.n2_m2)
     d = 3 * (system_milestone.n2_2m2 + x2).to_f
     
     begin
      calculated_value =  (c/d) * scale
     rescue Exception=>e 
       calculated_value = cf2.to_f
     end
     calculated_value = 10 if calculated_value < 0
    [calculated_value,cf2].min.to_d.truncate(2).to_f
    
    
  end

  def is_progress_bar_enabled?
    milestone = self
    milestone.status_by == "user" && milestone.date.present? && milestone.milestone_status == "accomplished"
  end


  def milestone_added_at_child_age(child)
    milestone = self
    milestone_added_at = (milestone.date || milestone.created_at).to_time
    ApplicationController.helpers.calculate_age(child.birth_date,milestone_added_at) rescue ""
  end

  def milestone_added_at_child_age_in_days(child)
    milestone = self
    milestone_added_at = (milestone.date || milestone.created_at).to_date
    (milestone_added_at - child.birth_date ).to_i rescue 0
  end

  def self.milestones_by_order(member, filter_by=nil,options={})
    if filter_by.present?
      filter_milestones(member, filter_by,options)
    else
      order_system_milestones_ids = SystemMilestone.all.order_by("max_range_in_day desc").pluck(:id)
      system_milestone_id_with_index = Hash[order_system_milestones_ids.map.with_index { |id,idx| [id,idx] }] 
      
      accomplished_milestones_without_date =  member.milestones.in(milestone_status:["accomplished","",nil]).or({status_by:"user",date:nil},{status_by:"system"}).sort_by { |milestone| system_milestone_id_with_index[milestone.system_milestone_id] }
      accomplished_milestones_with_date =  member.milestones.in(milestone_status:["accomplished","",nil]).where(status_by:"user").not_in(:date=>[nil]).where(:system_milestone_id.in=>order_system_milestones_ids).order_by("date desc").to_a
      unaccomplished_milestone = member.milestones.where(milestone_status:"unaccomplished",status_by:"system").order_by("date desc")#.sort_by { |milestone| system_milestone_id_with_index[milestone.system_milestone_id] }
      milestone_not_due_yet = member.milestones.in(milestone_status:"unaccomplished").where(status_by:"user").sort_by { |milestone| system_milestone_id_with_index[milestone.system_milestone_id] }
      accomplished_milestones_with_date + accomplished_milestones_without_date   + unaccomplished_milestone + milestone_not_due_yet
    end
  end

  def self.filter_milestones(member, filter_by,options={})
    milestones = member.milestones 
    current_user = options[:current_user]
    country_code = member.get_country_code
    options[:country_code] = country_code
    if filter_by[:start_date] and filter_by[:end_date]
      milestones = milestones.filter_by_date(filter_by[:start_date].to_i, filter_by[:end_date].to_i)
    end

    if filter_by[:type].present?
      milestones = milestones.filter_by_type(filter_by[:type].downcase,options)
    end
    if filter_by[:status].present?
      milestones = filter_by_status(filter_by[:status].downcase, milestones) 
    else
      accomplished_milestones_without_date =  milestones.in(milestone_status:["accomplished","",nil]).or({status_by:"user",date:nil},{status_by:"system"}).entries
      accomplished_milestones_with_date = milestones.in(milestone_status:["accomplished","",nil]).where(status_by:"user").not_in(:date=>[nil]).order_by("date desc").entries
      unaccomplished_milestone = milestones.where(milestone_status:"unaccomplished",status_by:"system").order_by("date desc").entries
      milestone_not_due_yet = milestones.in(milestone_status:"unaccomplished").where(status_by:"user").entries
      milestones = accomplished_milestones_with_date + accomplished_milestones_without_date   + unaccomplished_milestone + milestone_not_due_yet
    end
     data = milestones
    if options[:group_by] == "child_age"
      system_milestone_ids = milestones.map(&:system_milestone_id) 
      system_milestones_group_by_id = SystemMilestone.where(:id.in=>system_milestone_ids).group_by{|system_milestone| system_milestone.id}
      data = []
      milestones.each do |milestone|
        system_milestone = system_milestones_group_by_id[milestone.system_milestone_id].first
        milestone_data = Milestone.milestone_data(current_user,member,milestone,system_milestone,options)
        data << milestone_data
      end
        data = data.group_by{|milestone| milestone["updated_at_age_text"]}
    end 
    data
  end

  def self.filter_by_date(start_date, end_date)
    where({"$or"=>[{:date => {:gte=>Date.ordinal(start_date), :lt => Date.ordinal(end_date + 1)}},{:date => nil}]})
  end

  def self.filter_by_status(status, milestones)
    @all_status = true if status.downcase == "all"
    status = status.split(',').map{|m| m.strip}
    accomplished_milestones_with_date = [] 
    accomplished_milestones_without_date = []  
    unaccomplished_milestone = []  
    milestone_not_due_yet = []
    # byebug
    if  @all_status || status.include?("accomplished milestones")
      accomplished_milestones_with_date = milestones.in(milestone_status:["accomplished","",nil]).where(status_by:"user").not_in(:date=>[nil]).order_by("date desc").entries
    end
    if @all_status ||  status.include?("not accomplished yet")
      unaccomplished_milestone = milestones.where(milestone_status:"unaccomplished",status_by:"system").order_by("date desc").entries
      milestone_not_due_yet = milestones.in(milestone_status:"unaccomplished").where(status_by:"user").entries
    end

    if @all_status ||  status.include?("accomplished but date unknown")
      accomplished_milestones_without_date =  milestones.in(milestone_status:["accomplished","",nil]).or({status_by:"user",date:nil},{status_by:"system"}).entries
    end
    accomplished_milestones_with_date + accomplished_milestones_without_date   + unaccomplished_milestone + milestone_not_due_yet
  end

  def self.filter_by_type(type,options={})
    type = type.split(',').map{|m| m.strip}
    milestone_type = Milestone::SYSTEMMILESTONE_VALUE.invert.values_at(*type)
    system_milestone_ids = SystemMilestone.in_country_code(options).in(:milestone_type=>milestone_type).pluck(:id)
    where(:system_milestone_id.in=>system_milestone_ids)
  end

  def milestone_accomplished_status(system_milestone,child)
    milestone = self

    x = GlobalSettings::MilestoneProgressDay[system_milestone["category"]]
    calculated_expected_date_with_upper_limit_range = (system_milestone.calculate_expected_date_with_upper_limit_range(child) || Time.now).to_date
    calculated_expected_date_with_lower_limit_range = (system_milestone.calculate_expected_date_with_lower_limit_range(child) || Time.now).to_date
    if milestone.status_by == "user"
      if milestone.milestone_status == "accomplished"
        if milestone.date.blank?
          "can't tell" 
        elsif milestone.date <= (calculated_expected_date_with_lower_limit_range - x.days)
          "Ahead"
        elsif milestone.date < (calculated_expected_date_with_upper_limit_range + x.days) && milestone.date > (calculated_expected_date_with_lower_limit_range - x.days)
          "On Track"
        elsif milestone.date >= (calculated_expected_date_with_upper_limit_range + x.days)
          "Behind"
        end
      elsif  milestone.milestone_status == "unaccomplished"
        if Date.today < (calculated_expected_date_with_upper_limit_range + x.days)
          "yet to achieve"
        else
          "Behind"  
        end
      end
    elsif milestone.status_by == "system"
      if milestone.milestone_status == "accomplished"
        "can't tell"
      elsif milestone.milestone_status == "unaccomplished"
        "Behind"
      end
    end
  end


  def progress_text(score)
    begin
      milestone = self
      text_value = ""
      scale = GlobalSettings::MilestoneProgressCalculationFactors[:scale] 
      two_third_of_score = (2/(3.to_f) * scale).to_d.truncate(2).to_f
      one_third_of_score = (1/(3.to_f) * scale).to_d.truncate(2).to_f
      if milestone.milestone_status == "accomplished"
        return  "Provide accomplished date"  if milestone.date.blank? || milestone.status_by == "system"
        text_value =  case   
          when score >  two_third_of_score  
            "Achieved earlier than usual age"
          when score >= one_third_of_score && score <= two_third_of_score
            "Achieved within usual age range"
          when score <  one_third_of_score 
            "Milestone has been caught up"
         end
      else milestone.milestone_status == "unaccomplished"
        text_value = case  
          when score > (2/(3.to_f) * scale).round(2)   
            "Milestone not due yet"
          when score >= one_third_of_score && score <= two_third_of_score
            "Milestone due soon"
          when score <  one_third_of_score 
             "Milestone due"
         end
      end
    rescue Exception=>e 
      Rails.logger.info e.message
      text_value = ""
    end
    text_value
  end




  def milestone_accomplished_at_text(child)
    if self.milestone_status == "accomplished"
      if self.date.present? && status_by == "user"
        age = ApplicationController.helpers.calculate_age(child.birth_date,self.date) rescue ""
        "Accomplished at #{age}"
      else
         "Accomplished date unknown"
      end
    else
      "Not accomplished yet"
    end
  end
  
  def self.mark_milestone_to_unknown_unaccomplished(member,status="unknown")
    if Nutrient.is_tool_activated?(member.id, "Milestones")
      category =   member.find_category
      country_code = member.get_country_code
      options = {:country_code=>country_code}

      current_category_value = SystemMilestone::CATEGORIES_ORDER[category] 
      add_milestone_of_categroy = SystemMilestone::CATEGORIES_ORDER.select  do |k,v| 
        if v <= current_category_value
          upper_limit_month = k.split('-').last.to_i
          if status == "unknown"
            true 
          else
            estimated_date = member.birth_date + eval("upper_limit_month.month") + GlobalSettings::MilestoneProgressDay[k].days
            estimated_date < Date.today
          end
        end
      end
      add_milestone_of_categroy = add_milestone_of_categroy.keys
      required_system_milestone_ids = SystemMilestone.in_country_code(options).in(category:add_milestone_of_categroy).pluck(:id).map(&:to_s)
      if status == "unaccomplished"
        unknown_milestone_ids = Milestone.where(member_id:member.id,status:"unknown").pluck(:id).map(&:to_s)
        Milestone.where(member_id:member.id).in(id:unknown_milestone_ids).update_all(milestone_status:status)
      end
      added_system_milestone_ids = Milestone.where(member_id:member.id).pluck(:system_milestone_id).map(&:to_s)
      SystemMilestone.in(id:(required_system_milestone_ids - added_system_milestone_ids)).each do |system_milestone|
        Milestone.new( member_id: member.id, display_default_image: system_milestone.default_image_exists, system_milestone_id:system_milestone.id, status_by:"system", milestone_status:status).save
      end
    end
  end



  def self.unaccomplished_milestone_having_no_info_from_user(member)
    category =   member.find_category
    country_code = member.get_country_code
    options = {:country_code=>country_code}
    current_category_value = SystemMilestone::CATEGORIES_ORDER[category] 
    add_milestone_of_categroy = SystemMilestone::CATEGORIES_ORDER.select  do |k,v| 
      if v <= current_category_value
        upper_limit_month = k.split('-').last.to_i
        estimated_date = member.birth_date + eval("upper_limit_month.month") + GlobalSettings::MilestoneProgressDay[k].days
        estimated_date < Date.today
      end 
    end
    add_milestone_of_categroy = add_milestone_of_categroy.keys
    required_system_milestone_ids = SystemMilestone.in_country_code(options).in(category:add_milestone_of_categroy).pluck(:id).map(&:to_s)
    added_system_milestone_ids = Milestone.where(member_id:member.id,status_by:"user").pluck(:system_milestone_id).map(&:to_s)
    SystemMilestone.in(id:(required_system_milestone_ids - added_system_milestone_ids)).order_by_important_status
  end  

  def delete_timeline
    if timeline = Timeline.where(timelined_type: "Milestone", timelined_id: self.id).first
      timeline.delete
    end  
  end 
  private

  def accomplished_date_cannot_be_greater_than_today
    errors.add(:date, "can't be greater than today") if date.present? && date > Time.now.in_time_zone.to_date
  end

  def add_to_timeline
    if self.milestone_status == "accomplished"
      timeline = Timeline.where(timelined_type: 'Milestone', timelined_id: self.id).first
      if timeline
        timeline.update_attributes(desc: description, timeline_at: timeline.get_timeline_at(date || created_at))
      else
        Timeline.new(entry_from: "milestone", name: system_milestone.get_dynamic_title(member,self.child_male_status), desc: system_milestone.description, member_id: member_id, timeline_at: date || created_at, timelined_type: 'Milestone', timelined_id: self.id).save
      end
    end  
  end

  def self.milestone_stats(current_user,child,api_version,options={})
    member = child
    member["age"] = ApplicationController.helpers.calculate_age(member.birth_date) rescue ""
    category = options[:age_category] || member.find_category
    options[:age_category] =  options[:age_category] || member.male?
    system_milestones_count = {}
    system_milestones_type_count = {}
    country_code = member.get_country_code
    options = {:country_code => country_code}
    added_system_milestone_ids = Milestone.where(member_id:member.id).pluck(:system_milestone_id)
    milestone_sequence = SystemMilestone.in_country_code(options).where(category: category).first.sequence

    system_milestones_count = SystemMilestone.get_milestone_count_group_by_category(nil,options)
    system_milestones_type_count = SystemMilestone.get_milestone_count_group_by_type(milestone_sequence, nil,options )
    
    # added_system_milestone_ids = Milestone.where(member_id:member.id).pluck(:system_milestone_id)
    milestones_accomplished_by_user = Milestone.achieved_milestones(member).pluck(:system_milestone_id)
    
    added_milestones_count = added_milestones(member,added_system_milestone_ids)
    added_milestones_group_by_type_count = added_milestones_group_by_type(member,milestones_accomplished_by_user)
   
    estimated_system_milestone_ids = Milestone.estimated_milestones(member).pluck(:system_milestone_id)
    member_milestones_having_estamted_date_count = added_milestones_group_by_type(member,estimated_system_milestone_ids)
    
    milestone_category_stats  = []
    milestone_progress_report = get_progress_report_by_type(member,added_system_milestone_ids)
    added_milestones_group_by_type_count.each do |k,v|
      milestone_title_description = SystemMilestone::SystemMilestoneDescription[k.to_s]
      line2 = (member_milestones_having_estamted_date_count[k] rescue 0).to_s + " Estimated" 
      line3 = (milestone_progress_report[k.to_s]["delayed"] rescue 0).to_s + " Due"
      line1 = v.to_s + "/" + (system_milestones_type_count[k]||0) .to_s
      milestone_category_stats << {line1: line1, line2:line2, line3:line3, title:milestone_title_description[:title], description:milestone_title_description[:description] }
    end
    milestone_category_stats
  end


  def self.added_milestones(member,milestone_ids=nil)
    # arr = member.milestones.group_by{|m| m.system_milestone.category}.map{|k,grp| {k => grp.count} }
    member_milestone_list = SystemMilestone.get_member_milestone_count_group_by_category(member.id,milestone_ids)
    data = Hash.new
    SystemMilestone::CATEGORIES_RANGE.values.map do |v|
      data[v] = member_milestone_list[v].to_i
    end
    data
  end
  def self.added_milestones_group_by_type(member,milestone_ids=[])
    if milestone_ids.present?
      member_milestone_list = SystemMilestone.get_member_milestone_count_group_by_type(member.id,milestone_ids)
    else
     member_milestone_list = {}
    end
    data = Hash.new
    country_code = member.get_country_code
    options = {:country_code=>country_code}
    SystemMilestone.in_country_code(options).distinct(:milestone_type).map do |v|
      data[v] = member_milestone_list[v].to_i
    end
    data
  end

  def self.get_progress_report_by_type(member,system_milestone_id=[])
    data = {}
    score = ((1/3.to_f) * GlobalSettings::MilestoneProgressCalculationFactors[:scale]).round(2)
    scale = GlobalSettings::MilestoneProgressCalculationFactors[:scale]
    data = SystemMilestone.get_member_milestone_group_by_type(member,system_milestone_id)
    data.each do |type,system_milestone_ids|
      stats = {"cannot_tell"=>0,"delayed"=>0}
      milestones = Milestone.where(member_id:member.id).in(system_milestone_id:system_milestone_ids)
      milestones.each do |milestone|
        system_milestone = milestone.system_milestone
        if milestone.milestone_status == "unaccomplished" && milestone.progress_score(system_milestone,member) < score
          stats["delayed"] = stats["delayed"] + 1
        end
      end
      data[type] = stats
    end
    data
  end
  
  def self.get_achieved_percentage_for_age_range(current_user,member,api_version,options={})
    begin
      age_category =  member.find_category
      country_code =  member.get_country_code
      options = options.merge(:country_code=>country_code)
      system_milestone_ids_in_age_category = SystemMilestone.in_country_code(options).where(category: age_category).pluck(:id)
      achieved_milestones_in_age_category = Milestone.where(:member_id=>member.id,:system_milestone_id.in=>system_milestone_ids_in_age_category,:status=>"accomplished").pluck(:id)
      percentage =  ( (achieved_milestones_in_age_category.count.to_f/system_milestone_ids_in_age_category.count) *100  )
    rescue Exception => e 
      percentage = 0
    end
    percentage
  end
  def self.order_by_date(milestones)    
    date_blank = milestones.where(date: nil).order_by("created_at DESC")
    sorted_by_date = milestones.where(:date.ne => nil).order_by("date DESC")
    (date_blank+sorted_by_date)
  end 

  def enable_milestone_alert
    if date.blank?
      display_setting = member.member_settings.where(name:"milestone_display").first
      display_setting.update_attributes({:status => "true", note: 1.minutes.ago}) if display_setting
    end  
    # member.update_attribute(:milestone_alert, true) unless member.milestone_alert?
  end  

  def add_point
    begin   
      family_id = FamilyMember.where(member_id: self.member_id).first.family_id
      Score.create(family_id: family_id, activity_type: "Milestone", activity_id: self.id, activity: "Added milestone #{system_milestone.title.truncate(15)}", point: Score::Points["Health"], member_id: self.member_id)
    rescue Exception=>e 
      Rails.logger.info e.messsage
    end
  end  

  def remove_point
    family_id = FamilyMember.where(member_id: self.member_id).first.family_id
    Score.where(family_id: family_id, activity_type: "Milestone", activity_id: self.id, member_id: self.member_id).first.destroy
  rescue
  end  
  

  

  def self.convert_to_months(age,round_off=0)
    begin
      age.gsub!("months", " * 1")
      age.gsub!("days", " * 1/30.round(round_off)")
      age.gsub!("weeks", " * 1/4.round(round_off)")
      age.gsub!("week", " * 1/4.round(round_off)")
      age.gsub!("years", " * 12")
      age.gsub!("year", " * 12")
      age.gsub!("month", " * 1")
      age.gsub!("day", " * 1/30.round(round_off)")
      age.gsub!("and", " + ")
      age.gsub!(",", " + ")
      eval(age).round(round_off)
    rescue 
      0
    end
  end

   

  def self.estimated_milestones(member)
    Milestone.where(member_id:member.id).in(status_by:"system",:milestone_status=>"accomplished")
    #Milestone.where(member_id:member.id).in(milestone_status:["uncomplished"])
  end


  def self.achieved_milestones(member)
    Milestone.where(member_id:member.id).in(status_by:["user",nil],milestone_status:["accomplished",nil])
  end

  def self.milestone_group_by_id(member_id,system_milestone_ids=[])

    member_id = Moped::BSON::ObjectId(member_id.to_s)
    system_milestones_ids = system_milestone_ids.blank? ? Milestone.where(member_id:member_id).pluck(:system_milestone_id) : system_milestone_ids
    hash_data =  Milestone.collection.aggregate([
      { "$match" => { :member_id=>member_id, :system_milestone_id => {"$in"=> system_milestones_ids  } } },
    {"$group" => {
        "_id" =>   "$_id",
        "data" => {"$push"=> "$$ROOT"}
        
      }}  
     ]) 
    data = {}
    hash_data.each{|result| data[result["_id"] ] = result["data"] }
    data
  end  

  def self.member_milestone_graph_data(member,current_user,options={})
    begin  
      system_milestones_group_by_category_count = {}
      country_code = member.get_country_code
      options = {:country_code => country_code}
      all_data = SystemMilestone.in_country_code(options)
      all_data.group_by{|system_milestone| system_milestone.category}.each{|sm_group| system_milestones_group_by_category_count[sm_group.first] = sm_group.last.count}
      
      achieved_milestones_ids = Milestone.achieved_milestones(member).pluck(:system_milestone_id)
      achieved_milestones_count = Milestone.added_milestones(member,achieved_milestones_ids)
      
      
      current_age_range = member.find_category
      age_range = current_age_range.split("-")[1]
      achieved_milestones_count_for_age_range = 0 
      system_milestones_count_for_age_range = 0 
      
      age_range_for_percentage_calculation = achieved_milestones_count.keys.select{|a| a.split("-")[1].to_i < age_range.to_i}
      
      age_range_for_percentage_calculation.each do |milestone_age_range|
        achieved_milestones_count_for_age_range = achieved_milestones_count_for_age_range + achieved_milestones_count[milestone_age_range]
        system_milestones_count_for_age_range = system_milestones_count_for_age_range + system_milestones_group_by_category_count[milestone_age_range]
      end
      percentage = Milestone.calculate_percentage(system_milestones_group_by_category_count,achieved_milestones_count)
      # check divide by zero
      system_milestones_count_for_age_range = 1 if system_milestones_count_for_age_range == 0
      
      achieved_percentage = (achieved_milestones_count_for_age_range.to_f/system_milestones_count_for_age_range.to_f)* 100
      data = {:current_age_range=>current_age_range, :achieved_milestones_percentage=>percentage,:achieved_percentage=>achieved_percentage}
    rescue Exception=>e 
      
    end
    {:milestones=>data}
  end

  def self.calculate_percentage(system_milestones_count,added_milestones_count)
    percentage = {}
    system_milestones_count.each do |k,v|
      begin
        percentage[k] =  ( (added_milestones_count[k].to_f/system_milestones_count[k]) *100  )
      rescue Exception=>e 
         
        percentage[k] = 0
      end
    end  
    percentage
  end
  
  def self.milestone_due(current_user,member,options={})
    options["child_mail"]  = options["child_mail"] || member.male?
    category = member.find_category.split("-").last.to_i
    country_code = member.get_country_code
    options[:country_code]= country_code
   
    due_lower_range =  0 #category >= 6 ? (category - 6) : 0 # in months
    due_upper_range = category + 1 # in month
    system_milestone_category_range_for_due_milestone = SystemMilestone::CATEGORIES_RANGE.values.select{|a| a.split("-").last.to_i >= due_lower_range && a.split("-").first.to_i <= due_upper_range }
    
    system_milestone_ids = SystemMilestone.in_country_code(options).where(:category.in=>system_milestone_category_range_for_due_milestone).pluck(:id).map(&:to_s)
    
    # Remove accomplished/unaccomplished milestone    
    member_system_milestone_ids = member.milestones.where(:milestone_status.in=>["accomplished","unaccomplished"]).map{|milestone| milestone.system_milestone_id.to_s}
    due_system_milestone_ids = system_milestone_ids - member_system_milestone_ids

    due_milestones_group_by_system_milestone_id = Milestone.where(member_id:member.id,:system_milestone_id.in=>due_system_milestone_ids).group_by{|milestone| milestone.system_milestone_id}

    system_milestones = SystemMilestone.in_country_code(options).where(:id.in=>due_system_milestone_ids)
    data = []
    
    system_milestones.each do |system_milestone|
      milestone =  (due_milestones_group_by_system_milestone_id[system_milestone.id] || [Milestone.new(milestone_status:"unknown")]).first
      data << Milestone.milestone_data(current_user,member,milestone,system_milestone,options)
    end
    
    # Remove overdue milestones
    data = data.reject{|milestone| milestone["milestone_score_category"] == "overdue"}
   
    data = data.sort_by{|milestone| milestone["milestone_date"]}#.reverse
    data_by_category = data.group_by{|milestone| milestone["milestone_category"]}
    # sort by important status
      data_by_category.each do |k,v|
        data_by_category[k] = data_by_category[k].sort_by{|milestone| [milestone["list_order"],milestone["important_status_value"]]}
      end
      data = data_by_category.values.flatten
      data[0..11]
  end

  def self.accomplished_milestone_list(current_user,member,options={})
    options["child_mail"]  = options["child_mail"] || member.male?
    country_code = member.get_country_code
    options[:country_code]= country_code
   
    milestones_with_status = ["accomplished","unknown"]
    
    milestones = Milestone.where(member_id:member.id)#.or({:milestone_status.in=>milestones_with_status},{:milestone_status=>"unaccomplished",:status_by=>"system"}).order_by("date desc")
    # support for filter by important
    if options[:important_milestones].present? && options[:important_milestones].to_s == "true"
      added_system_milestones_ids = milestones.map(&:system_milestone_id)
      important_system_milestone_ids = SystemMilestone.where(:id.in=>added_system_milestones_ids).milestones_with_important_status
      milestones = Milestone.where(member_id:member.id,:system_milestone_id.in=>important_system_milestone_ids,:milestone_status.in=>milestones_with_status).order_by("date desc")
    end

    data = []
    
    # system_milestone_ids = milestones.map(&:system_milestone_id) 
    # system_milestones_group_by_id = SystemMilestone.where(:id.in=>system_milestone_ids).group_by{|system_milestone| system_milestone.id}
    # milestone_unknown_to_system = (all_system_milestone_ids - system_milestone_ids)
    
    milestones_group_by_system_milestone_id = milestones.group_by{|milestone| milestone.system_milestone_id}
    milestone_category = member.find_category
    max_day_range_of_category = SystemMilestone.where(category:milestone_category).last.max_range_in_day
    system_milestones = (SystemMilestone.in_country_code(options).where(:max_range_in_day.lte=>max_day_range_of_category).entries + SystemMilestone.where(:id.in=>(milestones_group_by_system_milestone_id.keys)).entries).uniq
    
    system_milestones.each do |system_milestone|
      begin
        milestone = (milestones_group_by_system_milestone_id[system_milestone.id] || [Milestone.new(milestone_status:"unknown")]).first
        milestone_data = Milestone.milestone_data(current_user,member,milestone,system_milestone,options)
        data << milestone_data
      rescue Exception=>e 
        UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside accomplished_milestone_list")
      end
    end


    # remove overdue milestone from list
    overdue_milestones = data.select{|milestone| (milestone["milestone_score_category"] == "overdue" && milestone["milestone_status"] != "accomplished")  }
    data = data.reject{|milestone| (milestone["milestone_score_category"] == "overdue" && milestone["milestone_status"] != "accomplished") }
    

    # select accomplished milestones 
    data = data.select{|milestone| (milestone["milestone_status"] == "accomplished") }

    # filter data for Important,  Overdue, Accomplished, Estimated
    if options[:filter_list].present?
      filter_list = options[:filter_list].split(",").map(&:downcase)
      filter_data = []
      if filter_list.include?("important")
        filter_data << data.select{|milestone| milestone["is_important"].to_s == "true"}
      end
      # if filter_list.include?("overdue")
      #   overdue_milestones = data.select{|milestone| (milestone["milestone_status"] == "unknown" || (milestone["milestone_status"] == "unaccomplished" && milestone["status_by"] == "system") ) }
      #   filter_data << overdue_milestones
      # end
      if filter_list.include?("accomplished")
        filter_data << data.select{|milestone| milestone["milestone_status"] == "accomplished" && milestone["status_by"]== "user"}
      end
      if filter_list.include?("estimated")
        filter_data << data.select{|milestone| milestone["milestone_status"] == "accomplished" && milestone["status_by"]== "system"}
      end
      data = filter_data.flatten.uniq
    end
    
    data = data.sort_by{|milestone| milestone["milestone_date"].to_date}.reverse
    overdue_milestones = overdue_milestones.sort_by{|milestone| milestone["milestone_date"].to_date}.reverse
    
    if options[:group_by] == "child_age"
      data = data.group_by{|milestone| milestone["updated_at_age_text"]}
      data.each do |k,v|
        data[k] = data[k].sort_by{|milestone| milestone["list_order"]}
      end
    end
    data["Overdue"] = overdue_milestones if overdue_milestones.present?
    accomplished_milestones = data
    [accomplished_milestones,nil]
  end
  
  def self.get_system_milestone_data(current_user,member,system_milestone,options={})
    data  = {
      "id"=> system_milestone.id,
      "category"=> system_milestone.category,
      "milestone_type"=> system_milestone.milestone_type,
      "title"=> system_milestone.get_dynamic_title(member),
      "description"=> system_milestone.description
    }
    data
  end


  def self.get_milestone_pictures(member,milestone,system_milestone,options={})
    data = []
    milestone_picutres = milestone.pictures
    if milestone_picutres.present?
      milestone_picutres.each  do |picture|
        data << picture.attributes.merge(url_small: picture.complete_image_url(:small),url_large: picture.complete_image_url(:medium),url_original: picture.complete_image_url(:original), aws_small_url: picture.aws_url(:small), aws_original_url: picture.aws_url, type: 'non_default')
      end

    elsif system_milestone.default_image_exists?
      milestone_picutres = [ApplicationController.helpers.default_image_url(milestone)]
      milestone_picutres.each do |picture|
      url_small = ApplicationController.helpers.default_small_image_url(milestone)
      url_large = ApplicationController.helpers.default_image_url(milestone)
      url_original = ApplicationController.helpers.default_image_url(milestone)
        data << {"type" => "default","_id"=>'default_1',"url_small"=>url_small,"url_large"=>url_large,"url_original"=>url_original}
      end
    end
    data
  end

  def self.milestone_data(current_user,member,milestone,system_milestone,options={})
    child_male  = options["child_mail"] || member.male?
    milestone_attributes = {}
    milestone_attributes = milestone.attributes
    milestone_attributes["milestone_accomplished_at"] = milestone.milestone_accomplished_at_text(member)
    milestone_attributes["description"] = milestone_attributes["description"].present? ? milestone_attributes["description"] : system_milestone.description
    milestone_attributes["title"] = milestone.milestone_status == "accomplished" ? system_milestone.get_dynamic_title(member,child_male) : system_milestone.get_dynamic_title(member,child_male).gsub(member.first_name, "Child") 
    milestone_attributes["milestone_type"] = system_milestone.milestone_type
    milestone_attributes["milestone_category"] = system_milestone.category 
    milestone_attributes["age"] = milestone.milestone_added_at_child_age(member) rescue nil
    milestone_accomplished_status = milestone.milestone_accomplished_status(system_milestone,member)
    milestone_attributes["milestone_progress_status"] = milestone_accomplished_status
    milestone_attributes["is_progress_bar_enabled"] = milestone.is_progress_bar_enabled?
    milestone_progress_score = milestone.progress_score(system_milestone,member)
    milestone_attributes["progress_score"] = milestone_progress_score
    milestone_attributes["progress_text"] = milestone.progress_text(milestone_attributes["progress_score"])
    milestone_attributes["is_important"] = system_milestone.is_important?
    milestone_attributes["date"] = nil if (milestone.status_by == "system" && milestone.milestone_status == "accomplished")
    milestone_attributes["milestone_date"] = milestone.date || (member.birth_date + system_milestone.max_range_in_day.day) rescue nil
    milestone_attributes["expected_date_text"] = "Usually achieved in #{system_milestone.category} months"
    milestone_attributes["added"] = milestone.system_milestone_id.present?
    milestone_attributes["_id"] = nil if milestone_attributes["added"] == false
    milestone_attributes["system_milestone_id"] = system_milestone.id rescue nil
    
    age_in_year,list_order = Milestone.accomplished_at_age(member,system_milestone,milestone)
    milestone_attributes["updated_at_age_text"] = "In the #{age_in_year.ordinalize} year"
    filter_mapping = {1=>"accomplished",2=>"accomplished",3=>"estimated",6=>"overdue",5=>"unaccomplished"}
    milestone_attributes["is_estimated"] = list_order == 3 
    milestone_attributes["is_overdue"] =  list_order == 6
    milestone_attributes["is_accomplished"] = (list_order == 1 || list_order == 2) 
    pictures = Milestone.get_milestone_pictures(member,milestone,system_milestone,options)  
    milestone_attributes["pictures"] = pictures if pictures.present?
    milestone_attributes["list_order"] = list_order
    milestone_attributes["milestone_score_category"] = Milestone.milestone_score_category(milestone_progress_score)
    milestone_attributes["important_status_value"] = system_milestone.important_status
    
    milestone_attributes["calculated_milestone_status"] = filter_mapping[list_order]
    milestone_attributes
  end
  
  def self.milestone_score_category(milestone_progress_score)
    return nil if milestone_progress_score.nil?
    milestone_progress_score = milestone_progress_score.to_f
    on_track_score_min = ((1/3.to_f) * GlobalSettings::MilestoneProgressCalculationFactors[:scale]).round(2)
    on_track_score_max = ((1/2.to_f) * GlobalSettings::MilestoneProgressCalculationFactors[:scale]).round(2)
    if milestone_progress_score < on_track_score_min
      "overdue"
    elsif milestone_progress_score >= on_track_score_min && milestone_progress_score <= on_track_score_max
      "due"
    elsif milestone_progress_score > on_track_score_max
      "upcoming"           
    end
  end

  def self.accomplished_at_age(member,system_milestone,milestone)
    # not accomplished milestone    
    if milestone.milestone_status != "accomplished" 
      age_in_year = (system_milestone.max_range_in_day.to_i/365) + 1 rescue 1
      if milestone.milestone_status == "unaccomplished"
        if milestone.status_by == "user"
          list_order =  5
        else
          list_order =  6
        end
      end
      if milestone.milestone_status == "unknown"
        list_order = 6
      end
      # if milestone.milestone_status.blank?
      #   list_order = system_milestone.important_status
      # end
    else
      #  accomplished by system milestone    
      if milestone.status_by == "system"
        list_order =  3
        age_in_year = (system_milestone.max_range_in_day.to_i/365) + 1 rescue 1
        
      elsif milestone.date.blank?
        #  accomplished by user without date milestone    
        age_in_year = (system_milestone.max_range_in_day.to_i/365) + 1 rescue 1
        list_order =  2
      else
      #  accomplished by user with date milestone    
        age_in_year = ((member.birth_date - milestone.date).abs.to_i/365) + 1 rescue 1
        list_order =  1
      end
    end
    [age_in_year,list_order]
  end


end

 
 