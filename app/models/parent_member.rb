class ParentMember < Member
  # attr_accessible :families_attributes 
has_many :families , :foreign_key => :member_id, inverse_of: :member
accepts_nested_attributes_for :families
has_many :notifications, class_name: "Invitation"
has_one :secondary_email, :foreign_key => :member_id, inverse_of: :user
has_many :notifications, class_name: 'Notification', foreign_key: :member_id, inverse_of: :member, :dependent => :destroy
# has_one :profile_image, :class_name => "Picture",:foreign_key => :member_id # inverse_of: :user
# has_one :profile_image,  :as=>:imageable, :class_name=>"Picture", foreign_key: :member_id
# accepts_nested_attributes_for :family_members, :autosave => true
 #, :allow_destroy => true, :foreign_key => :member_id

validates :first_name, presence: true, if: (Proc.new do |p|	
	p.validate_first_name
end)
validates :email, presence: true, uniqueness: true 
validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i }
#validates_inclusion_of :time_zone, :in => ActiveSupport::TimeZone.zones_map { |m| m.name }, :message => "is not a valid Time Zone"

attr_accessor :validate_first_name

def child?
	family_members.where(role:["Daughter","Son"]).entries
end

def is_expected?
  false
end

def has_mother_role?
  family_members.map(&:role).include?(FamilyMember::ROLES[:mother])
end
#update refs if user updated.
def update_childern_refs
  self.childerns.each do |member|
    member.update_member_ref("update",{:name_updated=>true})
  end
end

  def family_elder_member_list
    begin
      member_ids = FamilyMember.includes(:member).where({'family_id' => { "$in" => self.user_families.map(&:id)} }).not_in(:role =>["Son","Daughter"]).pluck(:member_id).uniq
    rescue
      []
    end
  end
 

def add_refs_to_childern
  self.childerns.each do |member|
    member.update_member_ref("add")
  end
end

# handle_asynchronously :update_childern_refs
# handle_asynchronously :add_refs_to_childern

#Api

def save_user_report_for_admin_panel
  report_data  = Report::UserBasicSummaryReportData.new
  report_data.fetch_data
end

def mothers_in_user_family(family_id=nil)
    begin
      families_associated_with_member = family_id.present?  ? [family_id] : FamilyMember.where(member_id:self.id).map(&:family_id).uniq
      family_members =  FamilyMember.where(:family_id.in=>families_associated_with_member, role:Family::ROLES[:mother]).map(&:member_id)
      data = []
      Member.where(:id.in=>family_members).order_by("email desc").each do |member|
        data <<{ email: member.email, member_id:member.id, user_id:member.user_id}
      end
      data
    rescue
      []
    end
  end

end