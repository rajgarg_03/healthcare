class InstantNotification::Member
  def self.notification_for_identifier_29(identifier,current_user,child_id,options={})
    # if member is added
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    if batch_notification.present? && batch_notification.is_valid_notification?
      if options[:family_id].present?
        family = Family.find options[:family_id] 
      else
        family =  FamilyMember.where(member_id:child_id).last.family
      end
      family_members = Member.in(id:family.family_elder_member_ids)
      child =  ChildMember.find(child_id)
      push_message = "Be on top of #{child.first_name}'s health and development with Nurturey"
      feed_id = nil
      if options[:user_ids].present?
        family_members =  Member.where(:user_id.in=>options[:user_ids])
      end
      push_type = batch_notification.identifier #push identifier
      priority = batch_notification.priority.to_i
      child_id = child_id.to_s rescue nil
      identifier  = batch_notification.identifier
      destination_name = batch_notification.destination
      action_type = batch_notification.action.downcase rescue ""
      batch_no = batch_notification.batch_no.to_i
      family_id = options[:family_id] || family.id
      notification_expiry_date = Date.today + batch_notification.expire_in_no_of_day.to_i.days
      status = false
      family_members.each do |member|
      begin
        notification_obj = UserNotification.add_push(batch_notification,member,push_type,batch_no,priority,push_message,destination_name,child_id,action_type,content_url=nil,feed_id,notification_expiry_date,score=priority,identifier,options)
        if notification_obj.present? &&  batch_notification.push_type == "Instant"
        notify_by = (batch_notification.notify_by || "push") rescue "push"
        # send realtime notification 
        if UserNotification.notify_by_push?(notify_by) || UserNotification.notify_by_email_and_push?(notify_by)
          UserNotification.push_notification(notification_obj,current_user,nil,nil,nil,{notification_type:"instant"}.merge(options)) rescue Rails.logger.info "Push not sent for id #{notification_obj.id}"
        end
        
        if UserNotification.notify_by_email?(notify_by) || UserNotification.notify_by_email_and_push?(notify_by)
          UserNotification.delay.send_email_notification(notification_obj,current_user, {notification_type:"instant"}.merge(options)) rescue Rails.logger.info "notification id #{notification_obj.id} not sent"
        end
        end     
      rescue Exception=>e 
      end
      end
    end
  end
end