class InstantNotification::Subscription
  def self.notification_for_identifier_17(identifier,current_user,family_id,options={})
    # notify family member for subscription taken
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    if batch_notification.present? && batch_notification.is_valid_notification?
      tool_name = batch_notification.destination  
      family =  Family.find(family_id)
      family_members = Member.in(id:family.family_elder_member_ids)
      child_id = (family.family_youngest_child).id rescue nil
      message = "Awesome, #{current_user.first_name} has taken the premium subscription for #{family.name}. You will now be a pro parent with our ever growing list of cutting edge premium tools and features :). Click to see details"
      action = batch_notification.action
      tool_id = Nutrient.where(identifier:tool_name).last.id
      feed_id = MemberNutrient.where(member_id:member_id,nutrient_id:tool_id).id rescue tool_id
      if options[:user_ids].present?
        family_members =  Member.where(:user_id.in=>options[:user_ids])
      end
      family_members.each do |elder_member|
        # if tool is not active
        begin
          unique_id = "#{feed_id}_identifier#{identifier}_#{current_user.user_id}_#{family_id}"
          user_action =  UserCommunication::UserActions.create(:action_for_tool=>batch_notification.destination,
          :message => message,
          :actor_id => current_user.id,
          :action_type => action,
          :unique_id => unique_id, 
          :object_id => elder_member.id,
          :family_id => family.id,
          :child_id => child_id
           )
          tool_id = nil
          UserNotification.add_user_action_in_notification(elder_member,user_action,nil,batch_notification.identifier,options)
          
        rescue Exception=>e 
          Rails.logger.info "Error in identifier 17"
          Rails.logger.info e.message
        end
      end
    end
  end


     
end