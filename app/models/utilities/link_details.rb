class Utilities::LinkDetails

  def self.fetch_link_content(link)
    begin
      page_link = link
      page = MetaInspector.new(page_link)
      # page = MetaInspector.new(link)
      return link_details(page)    
    rescue MetaInspector::TimeoutError
      Rails.logger.info MetaInspector::TimeoutError
    end
  end

  #private 
  def self.link_details(page)
    if page == nil
      return nil
    end
    page_details = {}
    page_details["title"] = page.title
    page_details["is_tracked"] = page.tracked?
    page_details["root_url"] = page.root_url
    page_details["page_title"] = page.title 
    page_details["best_title"] = page.best_title
    page_details["description"] = page.description
    page_details["best_description"] = page.best_description
    page_details["keywords"] = page.meta['keywords']
    page_details["images"] = page.images
    page_details
  end
end