class BatchNotification::PushNotification
  include Mongoid::Document  
  field :child_id, type:String
  field :message, type: String # custom message for notification
  field :action_type, type: String  # eg: updated/created/deleted
  field :unique_id, type:String # eg: (id of action_for  + action_for) (measurment_id+ Measurement)
  field :family_id , type:String # Family id for which action is taken 
  field :action_for_tool, type: String # eg:Family/Measurement/Timeline
  field :object_id, type:String # member id to whom action to be  add
  field :schedule_push_date, type:Date # member id to whom action to be  add
  
  
  # user not signin since 4 week and no notification sent since 2 week
  def self.assign_batch_notification_for_identifier_1(identifier,options={})
    begin
      @notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options) 
      batch_notification_manager = @notification
      if @notification.present? && @notification.is_valid_notification?
        users_not_login_since_4_week = User.where(:app_login_status=>true, :current_sign_in_at.lt=> Date.today - 4.weeks).in(status:["confirmed","approved"]).pluck(:id).map(&:to_s)
        if batch_notification_manager.check_sent_in_x_day.present? && batch_notification_manager.check_sent_in_x_day != "NA"
          user_received_notification_since_x_days = UserNotification.where(type:identifier, status:"sent",:notification_expiry_date.gte=>(Date.today - batch_notification_manager.check_sent_in_x_day.to_i.days)).in(user_id:users_not_login_since_4_week).pluck(:user_id).map(&:to_s)
        else
          user_received_notification_since_x_days = []
        end
        user_not_received_notification_since_x_days = users_not_login_since_4_week - user_received_notification_since_x_days
        push_type = @notification.identifier #push identifier
        priority = @notification.priority.to_i
        push_message = @notification.message
        child_id = nil
        destination_name = @notification.destination
        action_type = @notification.action.downcase rescue ""
        batch_no = @notification.batch_no.to_i
        notification_expiry_date = Date.today + @notification.expire_in_no_of_day.to_i.days
        if options[:user_ids].present?  
          user_not_received_notification_since_x_days = options[:user_ids]
        end
        Member.in(user_id:user_not_received_notification_since_x_days).no_timeout.batch_size(100).each do |member|
          record_object =  BatchNotification::PushNotification.create(:action_for_tool=>destination_name,
            :message=> push_message,
            :action_type => action_type,
            :unique_id=> nil, 
            :object_id=>member.id,
            :family_id => nil,
            :child_id => nil,
            :schedule_push_date=> Date.today
             )
          UserNotification.add_push(batch_notification_manager,member,push_type,batch_no,priority,push_message,destination_name,child_id,action_type,content_url=nil,tool_id=nil,notification_expiry_date,score=1,idenitifer=nil,options)
          break if !batch_notification_manager.valid_total_allowed_count?(member,options)
        end
      end
    rescue Exception=>e 
      puts e.message
      Rails.logger.info e.message
    end
  end

  # Milestone summary report
  def self.assign_batch_notification_for_identifier_5(identifier,options={})
    begin
      score = ((1/3.to_f) * GlobalSettings::MilestoneProgressCalculationFactors[:scale]).round(2)
      scale = GlobalSettings::MilestoneProgressCalculationFactors[:scale]
      week_day = Time.now.strftime("%A").downcase
      @notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
      batch_notification_manager = @notification
      if @notification.present? && @notification.is_valid_notification?
        if options[:child_member_ids].present?
          child_members = ChildMember.in(:id=>options[:child_member_ids])
        else
          child_members = ChildMember.all
        end
        child_members.no_timeout.batch_size(100).each do |child|
          break if !batch_notification_manager.valid_total_allowed_count?(current_user = nil,options)
          if !child.is_expected?
            begin
              estimated_milestone_count = Milestone.estimated_milestones(child).count
              accomplished_milestone_count = Milestone.achieved_milestones(child).count
              due_milestone_count = 0
              Milestone.where(milestone_status:"unaccomplished",member_id:child.id.to_s).no_timeout.each do |milestone|
                system_milestone = milestone.system_milestone
                due_milestone_count = due_milestone_count + 1 if milestone.progress_score(system_milestone,child) < score
              end
              msg = "As per my records, #{child.first_name} has #{accomplished_milestone_count} accomplished milestones, #{estimated_milestone_count} estimated milestones and #{due_milestone_count} milestones that should be due. Let's update now, if any discrepancy"
              family_elder_member_ids =  child.family_elder_members.map(&:to_s)
              user_ids  = User.where(:app_login_status=>true).in(member_id:family_elder_member_ids,status:["confirmed","approved"]).pluck(:id).map(&:to_s)
              push_type = @notification.identifier #push identifier
              priority = @notification.priority.to_i
              push_message = msg
              child_id = child.id.to_s
              destination_name = @notification.destination
              action_type = @notification.action.downcase rescue ""
              batch_no = @notification.batch_no.to_i
              family_id = FamilyMember.where(member_id:child_id).first.family_id
              notification_expiry_date = Date.today + @notification.expire_in_no_of_day.to_i.days
              Member.in(user_id:user_ids).no_timeout.each do |member|
                record_object =  BatchNotification::PushNotification.create(:action_for_tool=>destination_name,
                  :message=> push_message,
                  :action_type => action_type,
                  :unique_id=> nil, 
                  :object_id=>member.id,
                  :family_id => family_id,
                  :child_id => child.id,
                  :schedule_push_date=> Date.today
                   )
                UserNotification.add_push(batch_notification_manager,member,push_type,batch_no,priority,push_message,destination_name,child_id,action_type,content_url=nil,tool_id=nil,notification_expiry_date,score=1,identifier=nil,options)
              end
            rescue Exception=>e 
              puts e.message
              Rails.logger.info e.message
            end
          end
        end
      end
    rescue Exception=>e 
      Rails.logger.info e.message
    end
  end


end


