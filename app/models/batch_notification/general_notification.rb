class BatchNotification::GeneralNotification

  # All children 
  # message: Check out our new tools/features that will support <child name>'s growth journey. Dental Health, Z score, Check up planner, Health report to name a few.
  # BatchNotification::GeneralNotification.assign_batch_notification_for_identifier_16(16)
  def self.assign_batch_notification_for_identifier_16(identifier,options={})
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    if batch_notification.present? && batch_notification.is_valid_notification?
      tool_name = batch_notification.destination  
      if options[:child_member_ids].present?
        children = ChildMember.where(:id.in=>options[:child_member_ids])
      else
        children = ChildMember.where(:birth_date.lte =>  Date.today)
      end
      children.no_timeout.batch_size(100).each do |child|
        # if tool is not active
        if !child.is_expected?
          begin
            feed_id = nil
            msg = "Check out our new tools/features that will support #{child.first_name}'s growth journey. Dental Health, Z score, Check up planner, Health report to name a few."
            content_url = nil
            UserNotification.assign_push_to_all_elder_member(child,msg,batch_notification,feed_id,user_ids=nil,content_url,options)
          rescue Exception=>e 
            Rails.logger.info "Error in identifier 16"
            Rails.logger.info e.message
          end
        end
      end
    end
  end
  # Pregnancy Weekly Summary Email
  # start of each pregnancy week
  def self.assign_batch_notification_for_identifier_30(identifier,options={})
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    if batch_notification.present? && batch_notification.is_valid_notification?
      tool_name = batch_notification.destination  
      if options[:pregnancy_ids].present? && options[:test_without_validation]
        children = ChildMember.where(:id.in=>options[:pregnancy_ids])
      else
        data = []
        or_condition = []
        (0..39).each do |pregnancy_week|
          or_condition << {:expected_on=>Date.today + pregnancy_week.week}
        end
        pregnancy_in_start_of_week =  Pregnancy.or(or_condition).where(status: "expected")
        pregnancy_in_start_of_week = pregnancy_in_start_of_week.where(:expected_member_id.in=>options[:pregnancy_ids]) if options[:pregnancy_ids].present?
        children = Member.in(:id=>pregnancy_in_start_of_week.map(&:expected_member_id))
      end
      children.no_timeout.batch_size(100).each do |child|
        begin
          pregnancy = Pregnancy.where(expected_member_id:child.id).last
          feed_id = pregnancy.id rescue nil
          msg = "Your pregnancy in #{pregnancy.pregnancy_week.to_i.ordinalize} week"
          content_url = nil
          UserNotification.assign_push_to_all_elder_member(child,msg,batch_notification,feed_id,options[:user_ids],content_url,options)
        rescue Exception=>e 
          Rails.logger.info "Error in identifier 30"
          Rails.logger.info e.message
        end
      end
    end
  end

    # Pregnancy Weekly Summary Email
  # start of each pregnancy week
  def self.assign_batch_notification_for_identifier_38(identifier,options={})
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    if batch_notification.present? && batch_notification.is_valid_notification?
      tool_name = batch_notification.destination  
      sanitize_country_code = Member.sanitize_country_code("uk")
      if options[:pregnancy_ids].present? && options[:test_without_validation]
        children = ChildMember.where(:id.in=>options[:pregnancy_ids])
      else
        data = []
        or_condition = []
        (0..39).each do |pregnancy_week|
          or_condition << {:expected_on=>Date.today + pregnancy_week.week}
        end
        pregnancy_in_start_of_week =  Pregnancy.or(or_condition).where(status: "expected")
        pregnancy_in_start_of_week = pregnancy_in_start_of_week.where(:expected_member_id.in=>options[:pregnancy_ids]) if options[:pregnancy_ids].present?
        children = Member.where(:country_code.in=>sanitize_country_code).where(:id.in=>pregnancy_in_start_of_week.map(&:expected_member_id))
      end
      children.no_timeout.batch_size(100).each do |child|
        begin
          pregnancy = Pregnancy.where(expected_member_id:child.id).last
          feed_id = pregnancy.id rescue nil
          msg = "Your pregnancy in #{pregnancy.pregnancy_week.to_i.ordinalize} week"
          content_url = nil
          options[:pregnancy_id] = feed_id
          options[:pregnancy_week] = pregnancy.pregnancy_week.to_i
          UserNotification.assign_push_to_all_elder_member(child,msg,batch_notification,feed_id,options[:user_ids],content_url,options)
        rescue Exception=>e 
          Rails.logger.info "Error in assign_batch_notification_for_identifier_38"
          Rails.logger.info e.message
        end
      end
    end
  end

  # All pregnancy before 3-4 week of EDD, while setup pregnancy with pregnnacy age more than 6 week/1 month  
  # message: "Let's get ready for the next stage of your journey!"
  # BatchNotification::GeneralNotification.assign_batch_notification_for_identifier_33(33)
  def self.assign_batch_notification_for_identifier_33(identifier,options={})
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    if batch_notification.present? && batch_notification.is_valid_notification?
      push_type = batch_notification.identifier
      batch_no = batch_notification.batch_no
      priority = batch_notification.priority
      push_message = "Let's get ready for the next stage of your journey!"
      destination_name = batch_notification.destination
      notification_expiry_date = Date.today + batch_notification.expire_in_no_of_day.to_i.days
      action_type = batch_notification.action
      if options[:test_without_validation] == true
        pregnancies = Pregnancy.where(:id.in=>options[:pregnancy_ids])
      elsif options[:test_without_validation] == false
        pregnancies = Pregnancy.where(:status=> "expected",:expected_on=>{"$lte"=>(Date.today + 4.week),"$gte"=>(Date.today + 3.week)}).where(:id.in=>options[:pregnancy_ids])
      elsif options[:pregnancy_setup_flow] == true
        pregnancies = Pregnancy.where(:status=> "expected",:id.in=>options[:pregnancy_ids],:expected_on=>{"$gte"=>(Date.today + 6.week)})
      else
        pregnancies = Pregnancy.where(:status=> "expected",:expected_on=>{"$lte"=>(Date.today + 4.week),"$gte"=>(Date.today + 3.week)})
      end
      pregnancies.no_timeout.batch_size(100).each do |pregnancy|
        begin
          child_id = pregnancy.expected_member_id
          family = Family.where(:id=>pregnancy.family_id).first
          family_admin_member_id = family.member_id
          associated_mother_member_id = pregnancy.member_id
          feed_id = nil
          member_ids = [family_admin_member_id,associated_mother_member_id].compact
          Member.where(:id.in=>member_ids).each do |current_user|
            UserNotification.add_push(batch_notification,current_user,push_type,batch_no,priority,push_message,destination_name,child_id,action_type,content_url=nil,feed_id=nil,notification_expiry_date,score=1,identifier=nil,options)
          end
        rescue Exception=>e 
          Rails.logger.info "Error in identifier 33"
          Rails.logger.info e.message
        end
      end
    end

  end
  
  # Design a smile email
  def self.assign_batch_notification_for_identifier_35(identifier,options={})

    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    if batch_notification.present? && batch_notification.is_valid_notification?
      tool_name = batch_notification.destination  
      push_type = batch_notification.identifier #push identifier
      idenitifer = batch_notification.identifier
      priority = batch_notification.priority.to_i
      push_message = "Enter Nurturey's Design A Smile and Win Our Prize!"
      child_id = nil
      score = options[:score] || 1
      destination_name = batch_notification.destination
      action_type = batch_notification.action.downcase rescue ""
      batch_no = batch_notification.batch_no.to_i
      notification_expiry_date = Date.today + batch_notification.expire_in_no_of_day.to_i.days
      if options[:user_ids].present?  
        user_list = Member.where(:user_id.in=>options[:user_ids])
      else
        user_list = ParentMember.all
      end
      user_list.no_timeout.batch_size(100).each do |member|
        begin
          UserNotification.add_push(batch_notification,member,push_type,batch_no,priority,push_message,destination_name,child_id,action_type,content_url=nil,tool_id=nil,notification_expiry_date,score,idenitifer,options)
          break if !batch_notification_manager.valid_total_allowed_count?(member,options)  
        rescue Exception=>e 
          Rails.logger.info "Error in identifier 35..........................."
          Rails.logger.info e.message
        end
      end
    end
  end
 

end