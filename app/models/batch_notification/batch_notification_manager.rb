class BatchNotification::BatchNotificationManager
  include Mongoid::Document
  field :identifier, type:String # id
  field :message, type:String
  field :title_prefix, type:String # append prefix in message for notification eg, nhs, faq
  field :priority, type:Integer # 0,1,2,3
  field :batch_no, type:Integer #1,2,3,4 ..24
  field :status, type:String # active/draft
  field :destination, type:String #home,measurment,timleine,pointer,manage family..
  field :push_type, type:String # Instant/Batch
  field :case, type:String # critirea to assign and push notification 
  field :action, type:String # view/add/edit/bmi/upcoming/past..
  field :frequency, type:String # Not implemented yet
  field :category, type:String # #home,measurment,timleine,pointer,manage family..
  field :expire_in_no_of_day, type:Integer ,default:0# 
  field :tool_status, type: String, default:"active" #eg. active/deactivated/NA  
  field :subscription_status, type: Array, default:["free","paid"] #eg free/paid/both
  #field to manage sent notification again
  field :check_sent_in_x_day ,:type=>String, default:"NA" # check notification not sent in 3 month
  field :check_sent_before ,:type=>Boolean   # check not sent ever , also check in splited/archeived data
  field :check_signup_tenure, :type=>String
  field :notify_by, :type=>Array,:default=> ["email","push"] # Eg "email/push"
  #to limit total assign push for identifier
  field :total_allowed_push_count, :type=>Integer, default: -1
  
  field :added_child_member_tenure, :type=> String, default: "NA"
  field :user_activity_status, :type=> Array, default: ["Active","Inactive","Defunct"] # Eg. "Active/Inctive/Defunct"
  field :user_segment, :type=> Array, default: ["trial","starter","adoptor","ambassador"] # Eg. "trial/starter/adoptor/ambassador"
  
  #Enable on  Week day
  field :week_days_for_push, :type=>Array, default:["sunday","monday","tuesday","wednesday", "thursday","friday","saturday"] 
  

  index({identifier:1})
  index({batch_no:1})
  index({status:1})
  index({check_sent_in_x_day:1})
  index({check_sent_before:1})
  validates :identifier, presence: true

  validates :identifier, uniqueness: true

  def self.find_active_notification(identifier,options={})
    if options.blank?
      @notification = BatchNotification::BatchNotificationManager.where(identifier:identifier,status:"Active").last
    elsif options[:test_without_validation] == false
      @notification = BatchNotification::BatchNotificationManager.where(identifier:identifier).last
    elsif options[:test_without_validation]
      @notification = BatchNotification::BatchNotificationManager.where(identifier:identifier).last
    else
      @notification = BatchNotification::BatchNotificationManager.where(identifier:identifier,status:"Active").last
    end
    
    
  end
  
  def self.destination_values
      ["NA"] + (["Home", "Pointers", "Manage Family", "Manage Tools",'premium_subscription', 'pregnancy_week', 'Manage GP'] +  Nutrient.pluck(:identifier) - ["Menu"]).sort.uniq
  end

  def self.action_values
    ["add","edit","view","upcoming","past","height","weight","headcf","bmi"]
  end


  def is_valid_notification?
    batch_notification = self
    week_day = Time.now.strftime("%A").downcase
    batch_notification.week_days_for_push.include?(week_day)
  end


  def valid_total_allowed_count?(current_user,options={})
    batch_notification = self
    return true if options[:test_without_validation] == true
    #  Skip check if assign
    if options[:check_before_sent] == true
      max_count = batch_notification.total_allowed_push_count
      return true if max_count.blank? || max_count == -1
      if options[:user_notification_type] == "email"
        sent_push_count =   UserNotification.where(email_status:"sent", type:batch_notification.identifier,:updated_at.gte=> Date.today).count
      else
        sent_push_count =   UserNotification.where(status:"sent", type:batch_notification.identifier,:updated_at.gte=> Date.today).count
      end
      sent_push_count < max_count
    else
      return true
    end
  end

  def self.update_for_multi_select
    BatchNotification::BatchNotificationManager.each do |batch_notification|
      ["user_activity_status", "subscription_status","user_segment","notify_by","week_days_for_push"].each do |k|
        if batch_notification[k].blank? || batch_notification[k].to_s.downcase == "na" || batch_notification[k].to_s.downcase == "both"
          batch_notification[k] = BatchNotification::BatchNotificationManager.new[k]
        else
          batch_notification[k] = [batch_notification[k]].flatten
        end
        
      end
      batch_notification.save
    end
  end  
end