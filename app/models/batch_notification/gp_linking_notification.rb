class BatchNotification::GpLinkingNotification

  def self.assign_batch_notification_for_identifier_39(identifier,options={})
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    if batch_notification.present? && batch_notification.is_valid_notification?
      tool_name = batch_notification.destination  
      push_type = batch_notification.identifier #push identifier
      idenitifer = batch_notification.identifier
      priority = batch_notification.priority.to_i
      push_message = "Link to your GP to use NHS online services!"
      child_id = nil
      score = options[:score] || 1
      destination_name = batch_notification.destination
      action_type = batch_notification.action.downcase rescue ""
      batch_no = batch_notification.batch_no.to_i
      notification_expiry_date = Date.today + batch_notification.expire_in_no_of_day.to_i.days
      country_code = Member.sanitize_country_code("uk")
      if options[:user_ids].present?  
        if options[:test_without_validation] == true
          user_list = User.where(:id.in=>options[:user_ids])
        else
          user_list = User.where(:country_code.in=>country_code, :id.in=>options[:user_ids])
        end
      else
        user_list = User.where(:clinic_link_state.lt=>3, :country_code.in=>country_code,:activity_status=>"Active").any_of({provider: nil},{ provider: ""}, { provider: "nhs" }, { provider: "nurturey" } )
      end
      user_list.no_timeout.batch_size(100).each do |user|

        begin
          member = user.member
          UserNotification.add_push(batch_notification,member,push_type,batch_no,priority,push_message,destination_name,child_id,action_type,content_url=nil,tool_id=nil,notification_expiry_date,score,idenitifer,options)
          break if !batch_notification_manager.valid_total_allowed_count?(member,options)  
        rescue Exception=>e 
          Rails.logger.info "Error in identifier 39..........................."
          Rails.logger.info e.message
        end
      end
    end
  end
end
