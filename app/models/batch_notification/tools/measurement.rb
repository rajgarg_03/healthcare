class BatchNotification::Tools::Measurement

  #BatchNotification::Tools::Measurement.measurement_not_updated_since(child,3,21)
  def self.measurement_not_updated_since(child,check_for_age,not_updated_since_no_of_days,batch_notification_manager=nil,options={})
    begin
      measurement_last_updated_date = Health.where(member_id:child.id).order_by("updated_at Desc").first.updated_at rescue nil

       calculated_age = child.birth_date + eval(check_for_age) rescue child.birth_date
      if (calculated_age == Date.today) && (measurement_last_updated_date.blank? || measurement_last_updated_date < (Date.today - not_updated_since_no_of_days.to_i.days) )
       family = FamilyMember.where(member_id:child.id).last.family
       child_age = child.age_in_months

       if child_age == 0
         child_age = child.age_in_weeks.to_s + " week"
      else
         child_age = child_age.to_s + " month"
       end
       elder_member_ids = child.family_elder_members
       batch_notification_manager = batch_notification_manager || BatchNotification::BatchNotificationManager.new(category:"Measurements", check_sent_in_x_day:"NA",check_sent_before:false)
       message = batch_notification_manager.message || "It's time again! Click to update #{child.first_name}'s age #{child_age}'s measurements. It's easy to forget these, let's update as soon as we can :)"
       action_for_tool = batch_notification_manager.destination || "Measurements"
       action = batch_notification_manager.action || "Add"
       members = Member.in(id:elder_member_ids)
       members.no_timeout.each do |member|
         record_object =  BatchNotification::PushNotification.create(:action_for_tool=>action_for_tool,
            :message=> message,
            :action_type => action,
            :unique_id=> nil, 
            :object_id=>member.id,
            :family_id => family.id,
            :child_id => child.id,
            :schedule_push_date=> Date.today
             )
          # add user action in user notification 
          push_type = batch_notification_manager.push_type || "batch_notification"
          priority = batch_notification_manager.priority ||  0
          push_message = record_object.message
          child_id = record_object.child_id
          tool_name = record_object.action_for_tool
          action_type = action
          batch_no = batch_notification_manager.batch_no || 19
          UserNotification.add_push(batch_notification_manager,member,push_type,batch_no,priority,push_message,tool_name,child_id,action_type="add",content_url=nil,feed_id=nil,notification_expiry_date=nil,score=1,identifier=nil,options)
        end
      end
    rescue Exception=>e 
      Rails.logger.info e.message
    end
  end

  # TODO: Add to Admin panel
  def self.assign_measurement_push(identifier=nil,options={})
    if identifier.present?
      @batch_notification_manager = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
      return  true if @batch_notification_manager.blank?
    end
    condition_list = []
    # child age  filter condtition
    (1..8).each do |week_value|
      condition_list << {:birth_date=> (Date.today - (week_value.week + 1.day)) }
    end
    (3..24).each do |month_value|
      condition_list << {:birth_date=> (Date.today - (month_value.months + 1.day)) }
    end
    (1..12).each do |l|
      condition_list << {:birth_date=> (Date.today - (24.months + (l*6).months + 1.day)) }
    end
    
    if options[:child_member_ids].present?
      child_members = ChildMember.in(:id=>options[:child_member_ids])
    else
      child_members = ChildMember.where({"$or"=>condition_list})
    end
    
    child_members.no_timeout.batch_size(100).each do |child|
      begin
        batch_notification_manager = @batch_notification_manager
        break if !batch_notification_manager.valid_total_allowed_count?(current_user = nil,options)
        
        child_age = child.age_in_months
        check_for_age = ""
        no_of_days_since_not_updated = 0
        if child_age < 3
          (1..8).each do |week_value|
            if child.birth_date + (week_value.week + 1.day) == Date.today
              check_for_age = week_value.to_s + ".weeks + 1.days"   
              no_of_days_since_not_updated = 2
            end
            break true if check_for_age.present?
          end
        elsif child_age >= 3 && child_age < 25
          (3..24).each do |month_value|
            if child.birth_date + (month_value.months + 1.day) == Date.today
              check_for_age = month_value.to_s + ".months + 1.days"    
              no_of_days_since_not_updated = 4
            end
            break true if check_for_age.present?
          end
        elsif child_age >= 24 && child_age < 97
         (1..12).each do |l|
            if child.birth_date + (24.months + (l*6).months + 1.days) == Date.today
              check_for_age = (l*6).to_s + ".months + 24.months + 1.days"    
              no_of_days_since_not_updated = 14
            end
            break if check_for_age.present?
         end
        end
        if check_for_age.present? &&   !child.is_expected?
           BatchNotification::Tools::Measurement.measurement_not_updated_since(child,check_for_age,no_of_days_since_not_updated,@batch_notification_manager,options)
        end
      rescue Exception=>e 
        Rails.logger.info e.message
      end
    end
  end


  def self.send_batch_measurement_push(country=nil)
    country_code = Member.sanitize_country_code(country)
    notifications = UserNotification.where(type:"batch_notification",schedule_push_date:Date.today,status:"pending")
    notifications.no_timeout.batch_size(100).each do |notification|
      begin
        if country == "other"
          user = User.where(id:notification.user_id).not_in(country_code:["uk","UK","gb","GB",'in',"india","INDIA","IN","US","us","UK","AE","ae","nz","NZ","CA","ca","ZA","za","SG","sg"]).last
        else
          user = User.where(id:notification.user_id).in(country_code:country_code).last
        end

        if user.is_valid_user_to_send_push?
          current_user = user.member
          UserNotification.push_notification(notification,current_user)
        end
      rescue Exception=>e 
        Rails.logger.info e.message
      end
    end
  end

  #Not sent in last 60 days + sign up date > 2 days
  def self.assign_batch_notification_for_identifier_10(identifier,options={})
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    if batch_notification.present? && batch_notification.is_valid_notification?
      tool_name = batch_notification.destination  
      if options[:child_member_ids].present?
        child_members = ChildMember.where(:id.in=>options[:child_member_ids])
      else
        child_members = ChildMember.all
      end
      child_members.no_timeout.batch_size(100).each do |child|
        batch_notification_manager = batch_notification
        break if !batch_notification_manager.valid_total_allowed_count?(current_user = nil,options)
        if !child.is_expected?
          pointer_477 = ArticleRef.where(custome_id:477,member_id:child.id.to_s).last
          if options[:test_without_validation]
            pointer_477 = pointer_477  || ArticleRef.where(member_id:child.id.to_s).first
          end
          if pointer_477.present?
            begin
              tool_id = pointer_477.id
              msg = "We should regularly record #{child.first_name}'s measurements to monitor #{child.pronoun} growth. Learn why measurements tracking is so important"
              UserNotification.assign_push_to_all_elder_member(child,msg,batch_notification,tool_id,user_ids=nil,content_url=nil,options)
            rescue Exception=>e 
              Rails.logger.info e.message
            end
          end
        end
      end
    end
  end
  

  def self.assign_batch_notification_for_identifier_12(identifier,options={})
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    if batch_notification.present? && batch_notification.is_valid_notification?
   
      #Child age =< 8weeks AND N: no of days since last measurement record > 8 days
      BatchNotification::Tools::Measurement.assign_notification_12(nil,8.week, 8,"week",batch_notification,options)
      
      #8weeks < Child age =< 24months 
      BatchNotification::Tools::Measurement.assign_notification_12(8.week,24.month, 35,"month",batch_notification,options)
   
      #24months < Child age =< 6 years 
      BatchNotification::Tools::Measurement.assign_notification_12(24.month,6.years, 190,"6 months",batch_notification,options)
      
      #Child age > 6years
      BatchNotification::Tools::Measurement.assign_notification_12(6.years,nil, 380,"year",batch_notification,options)
   
    end
  end

  #Check for each child: If at least one measurement record exist + subscription package is 'Free' for the family + same notification has not been sent in the last 2 weeks.
  #Message: Z score is WHO's tool to assess normal growth in height, weight and BMI. Check <child name>'s Z score based growth assessment
  def self.assign_batch_notification_for_identifier_14(identifier,options={})
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    if batch_notification.present? && batch_notification.is_valid_notification?
      tool_name = batch_notification.destination  
      
      if options[:child_member_ids].present?
        child_members = ChildMember.where(:id.in=>options[:child_member_ids])
      else
        child_members = ChildMember.where(:birth_date.lte=> Date.today)
      end

      child_members.no_timeout.batch_size(100).each do |child|
        batch_notification_manager = batch_notification
        break if !batch_notification_manager.valid_total_allowed_count?(current_user = nil,options)

        measurement_record = Health.where(member_id:child.id.to_s).order_by("updated_at desc").limit(1).first
        if measurement_record.present?

          # check family subscription
          begin
            family_id = FamilyMember.where(member_id:child.id.to_s).last.family_id
            family_subscribed_plan = Subscription.family_free_or_premium_subscription(family_id) 
            free_subscription = Subscription.is_free?(family_subscribed_plan.subscription_listing_order.to_i)
            if !child.is_expected?
              begin
                tool_id = measurement_record.id
                msg = "Is #{child.first_name}'s measurement Z score within normal range? Let's check now. Z score is WHO's tool to analyse normal growth in height, weight and BMI."
                UserNotification.assign_push_to_all_elder_member(child,msg,batch_notification,tool_id,user_ids=nil,content_url=nil,options)
              rescue Exception=>e
                Rails.logger.info "Identifier 14" 
                Rails.logger.info e.message
              end
            end
          rescue Exception=> e
            Rails.logger.info e.message
            Rails.logger.info "inside identifier 14"
  
          end
        end
      end
    end
  end

private
  # For Identifier 12
  def self.assign_notification_12(min_age_val,max_age_val,not_updated_since_days,m,batch_notification,options={})
    begin 
      if max_age_val.present? && min_age_val.blank?
        members = ChildMember.where(:birth_date.gte=> (Date.today - max_age_val) )
      elsif  min_age_val.present? && max_age_val.present?
        members = ChildMember.where(:birth_date.lt=> (Date.today - min_age_val),:birth_date.gte=> (Date.today - max_age_val) )
      else
        members = ChildMember.where(:birth_date.lt=> (Date.today - min_age_val) )
      end
      tool_name = batch_notification.destination  
      if options[:child_member_ids].present?
        members = ChildMember.where(:id.in=>options[:child_member_ids])
      end
      members.no_timeout.batch_size(100).each do |child|
        begin
          
          batch_notification_manager = batch_notification
          break if !batch_notification_manager.valid_total_allowed_count?(current_user = nil,options)
          
          n = (Date.today - Health.where(member_id:child.id).order_by("updated_at desc").limit(1).first.updated_at.to_date).ceil rescue 0
          if !child.is_expected? && n > not_updated_since_days
            msg = "We haven't updated #{child.first_name}'s measurements for the last #{n} days. At this stage, we should record the measurements at least every #{m} to identify any potential developmental concerns. Click, let's do it now"
            tool_id = nil
            UserNotification.assign_push_to_all_elder_member(child,msg,batch_notification,tool_id,user_ids=nil,content_url=nil,options)
          end
        rescue Exception=> e 
          Rails.logger.info e.message
        end
      end
    rescue Exception=>e 
      Rails.logger.info e.message
    end
  end

end