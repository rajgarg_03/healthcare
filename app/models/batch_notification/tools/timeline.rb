class BatchNotification::Tools::Timeline

  # All Children. Current date = birthday + 1.day.
  # message: 1) Upload a memory of <child name>'s N month birthday on Nurturey Timeline. I will preserve to create <his/her> journey N=one, two, three, four, five, six, seven, eight, nine, ten, eleven
  #          2) Upload a memory of <child name>'s N birthday on Nurturey Timeline.       I will preserve to create <his/her> journey N= first, second, third, fourth, fifth, sixth, seventh, eighth, ninth, tenth
  # BatchNotification::Tools::Timeline.assign_batch_notification_for_identifier_20
  def self.assign_batch_notification_for_identifier_20(identifier,options={})
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    if batch_notification.present? && batch_notification.is_valid_notification?
      tool_name = batch_notification.destination  
      if options[:child_member_ids].present?
        if options[:test_without_validation] == true
          child_data_for_monthly_birthday = Member.in(:id=>options[:child_member_ids])
          child_data_for_annual_birthday = child_data_for_monthly_birthday
        elsif options[:test_without_validation] == false
          child_data_for_monthly_birthday = BatchNotification::Tools::Timeline.child_had_month_birthday_1_day_before.in(:id=>options[:child_member_ids])
          child_data_for_annual_birthday = BatchNotification::Tools::Timeline.child_had_birthday_1_day_before.in(:id=>options[:child_member_ids])
         
        end
      else
         child_data_for_monthly_birthday = BatchNotification::Tools::Timeline.child_had_month_birthday_1_day_before
         child_data_for_annual_birthday = BatchNotification::Tools::Timeline.child_had_birthday_1_day_before
      end
      child_data_for_monthly_birthday.each do |child|
        if !child.is_expected?
          begin
            user_ids = options[:user_ids]
            age_in_month = child.age_in_months
            system_timeline = SystemTimeline.find_by_age(age_in_month)
            tool_id = Timeline.where(member_id:child.id,system_timeline_id:system_timeline.id).last rescue nil
            msg = "Upload a memory of #{child.first_name}'s #{age_in_month} month birthday on Nurturey Timeline. I will preserve to create #{child.pronoun} journey"
            content_url = system_timeline.get_birthday_image rescue nil
            options = options.merge({:system_timeline =>system_timeline,:age_in_month=>age_in_month,:birthday_email_for=>"monthly"})
            UserNotification.assign_push_to_all_elder_member(child,msg,batch_notification,tool_id,user_ids ,content_url,options)
          rescue Exception=>e 
            Rails.logger.info "Error in identifier 20 for Month"
            Rails.logger.info e.message
          end
        end
      end
      child_data_for_annual_birthday.no_timeout.batch_size(100).each do |child|
        if !child.is_expected?
          begin
            user_ids = options[:user_ids]
            age_in_month = child.age_in_months
            system_timeline = SystemTimeline.find_by_age(age_in_month)
            tool_id = Timeline.where(member_id:child.id,system_timeline_id:system_timeline.id).last rescue nil
            msg = "Upload a memory of #{child.first_name}'s #{(age_in_month/12).ordinalize} birthday on Nurturey Timeline. I will preserve to create #{child.pronoun} journey"
            content_url = system_timeline.get_birthday_image rescue nil
            options = options.merge({:system_timeline =>system_timeline,:age_in_month=>age_in_month,:birthday_email_for=>"annually"})
            UserNotification.assign_push_to_all_elder_member(child,msg,batch_notification,tool_id,user_ids ,content_url,options)
          rescue Exception=>e 
            Rails.logger.info "Error in identifier 20"
            Rails.logger.info e.message
          end
        end
      end
    end
  end

  # All children. monthly or yearly birthday on the current date
  # message: 1) Nurturey wishes ' + member.first_name + ' a very happy N month birthday. Have a great one! N= one, two, three, four, five, six, seven, eight, nine, ten, eleven 
  #          2) Nurturey wishes ' + member.first_name + ' a very happy N year birthday. Have a great one! N= first, second, third, fourth, fifth, sixth, seventh, eighth, ninth, tenth
  # BatchNotification::Tools::Timeline.assign_batch_notification_for_identifier_20
  def self.assign_batch_notification_for_identifier_19(identifier,options={})
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    if batch_notification.present? && batch_notification.is_valid_notification?
      tool_name = batch_notification.destination  
      if options[:child_member_ids].present?
        if options[:test_without_validation] == true
          child_data_for_monthly_birthday = Member.in(:id=>options[:child_member_ids])
          child_data_for_annual_birthday = child_data_for_monthly_birthday
        elsif options[:test_without_validation] == false
          child_data_for_monthly_birthday = BatchNotification::Tools::Timeline.child_having_month_birthday.in(:id=>options[:child_member_ids])
          child_data_for_annual_birthday = BatchNotification::Tools::Timeline.child_having_birthday.in(:id=>options[:child_member_ids])
         
        end
      else
        child_data_for_monthly_birthday = BatchNotification::Tools::Timeline.child_having_month_birthday
        child_data_for_annual_birthday = BatchNotification::Tools::Timeline.child_having_birthday
      end
      child_data_for_monthly_birthday.no_timeout.batch_size(100).each do |child|
        if !child.is_expected?
          begin
            user_ids = options[:user_ids]
            age_in_month = child.age_in_months
            system_timeline = SystemTimeline.find_by_age(age_in_month)
            tool_id = Timeline.where(member_id:child.id,system_timeline_id:system_timeline.id).last rescue nil
            msg = "Nurturey wishes " + child.first_name + " a very happy #{age_in_month} month birthday. Have a great one!"
            content_url = system_timeline.get_birthday_image rescue nil
            options = options.merge({:system_timeline =>system_timeline,:age_in_month=>age_in_month,:birthday_email_for=>"monthly"})
            UserNotification.assign_push_to_all_elder_member(child,msg,batch_notification,tool_id,user_ids ,content_url,options)
          rescue Exception=>e 
            Rails.logger.info "Error in identifier 19 for Month"
            Rails.logger.info e.message
          end
        end
      end
      child_data_for_annual_birthday.each do |child|
        if !child.is_expected?
          begin
            user_ids = options[:user_ids]
            age_in_month = child.age_in_months
            system_timeline = SystemTimeline.find_by_age(age_in_month)
            tool_id = Timeline.where(member_id:child.id,system_timeline_id:system_timeline.id).last rescue nil
            msg = "Nurturey wishes " + child.first_name + " a very happy #{(age_in_month/12).ordinalize} year birthday. Have a great one!"
            content_url = system_timeline.get_birthday_image rescue nil
            options = options.merge({:system_timeline =>system_timeline,:age_in_month=>age_in_month,:birthday_email_for=>"annually"})
            UserNotification.assign_push_to_all_elder_member(child,msg,batch_notification,tool_id,user_ids ,content_url,options)
          rescue Exception=>e 
            Rails.logger.info "Error in identifier 19"
            Rails.logger.info e.message
          end
        end
      end
    end
  end

  def self.child_had_month_birthday_1_day_before
     day = Date.today.day
      current_date =  Date.today - 1.day
     condition_data = []
     (1..11).each do |month|
        birthday_value = current_date - month.month
       condition_data << {:birth_date=>birthday_value}
     end
    ChildMember.or(condition_data).where(:birth_date=>{"$lte"=>current_date, "$gte"=>(current_date - 1.year)})
  end 

  def self.child_had_birthday_1_day_before
    day = Date.today.day
     current_date =  Date.today - 1.day
     condition_data = []
     (1..20).each do |year|
        birthday_value = current_date - year.year
       condition_data << {:birth_date=>birthday_value}
     end
    ChildMember.or(condition_data).where(:birth_date=>{"$lte"=>current_date, "$gte"=>(current_date - 21.year)})
  end 

  def self.child_having_month_birthday
     day = Date.today.day
      current_date =  Date.today
     condition_data = []
     (1..11).each do |month|
        birthday_value = current_date - month.month
       condition_data << {:birth_date=>birthday_value}
     end
    ChildMember.or(condition_data).where(:birth_date=>{"$lte"=>current_date, "$gte"=>(current_date - 1.year)})
  end 

  def self.child_having_birthday
    day = Date.today.day
     current_date =  Date.today
     condition_data = []
     (1..20).each do |year|
        birthday_value = current_date - year.year
       condition_data << {:birth_date=>birthday_value}
     end
    ChildMember.or(condition_data).where(:birth_date=>{"$lte"=>current_date, "$gte"=>(current_date - 21.year)})
  end 

end