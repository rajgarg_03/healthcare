class BatchNotification::Tools::Faq

  def self.assign_batch_notification_for_identifier_23(identifier,options={})
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    # check  scenario added in batch notification with active status
    if batch_notification.present? && batch_notification.is_valid_notification?
      max_limit_for_faq_question = GlobalSettings::MaxAllowedPushCountTypeWise["faq question"] + 0
      notify_by_push  = UserNotification.notify_by_push?(batch_notification.notify_by)
      # assign faq questions only mobile users if only push is to be sent
      if notify_by_push
        member_ids = Device.not_in(token:nil).pluck(:member_id)
        members = ParentMember.in(:id=>member_ids)
      else
        members = ParentMember.all
      end
      if options[:user_ids].present?
        members = Member.in(:user_id=>options[:user_ids])
      end
      options[:scenario_tool_class] = "System::Faq::Question"
      members.batch_size(100).no_timeout.each do |current_user|
        batch_notification_manager = batch_notification
        break if !batch_notification_manager.valid_total_allowed_count?(current_user)
        faq_question_limit = GlobalSettings::MaxAllowedPushCountTypeWise["faq question"]
        UserNotification.add_scenario_notification(current_user,batch_notification_manager,faq_question_limit,options)
      end
    end
  end
end 

