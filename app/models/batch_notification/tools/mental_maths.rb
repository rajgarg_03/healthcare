class BatchNotification::Tools::MentalMaths

  # All children. (Minimum age = 3.5 years and Country UK and US only)
  # message: Stimulate brain growth of <Child name>. <He/She> can practise mental maths questions with Alexa or Google Home.
  # BatchNotification::Tools::MentalMaths.assign_batch_notification_for_identifier_21
  def self.assign_batch_notification_for_identifier_21(identifier,options={})
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    if batch_notification.present? && batch_notification.is_valid_notification?
      tool_name = batch_notification.destination  
      children_with_age_greater_than_3_5_years = ChildMember.where(:birth_date => {"$lte"=> (Date.today - (3.years + 5.month) ) })
      if options[:child_member_ids].present?
        if options[:test_without_validation] == true
          children_with_age_greater_than_3_5_years = ChildMember.where(:id.in=>options[:child_member_ids])
        else
          children_with_age_greater_than_3_5_years = children_with_age_greater_than_3_5_years.where(:id.in => options[:child_member_ids])
        end
      end
      children_with_age_greater_than_3_5_years.no_timeout.batch_size(100).each do |child|
        if !child.is_expected?
          begin
            tool_id = nil
            msg = "Stimulate brain growth of #{child.first_name}. #{child.pronoun1} can practise mental maths questions with Alexa or Google Home."
            child_country_code = child.get_country_code
            sanitize_country_code  = Member.sanitize_country_code(child_country_code)
            if sanitize_country_code.include?("uk") || sanitize_country_code.include?("us") || options[:test_without_validation] == true
              UserNotification.assign_push_to_all_elder_member(child,msg,batch_notification,tool_id,user_ids=nil,content_url=nil,options)
            end
          rescue Exception=>e 
            UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside assign_batch_notification_for_identifier_21")
            Rails.logger.info "Error in identifier 15"
            Rails.logger.info e.message
          end
        end
      end
    end
  end

  # Mental math email for Alexa signup :
  # Mental math email for Alexa signup : — signup platform Alexa — email verified — atleast one child — batch notifications — send only once — send email to the old users who has signup using Alexa
  # message: Track <name>’s mental maths experience with Nurturey App dashboard
  # BatchNotification::Tools::MentalMaths.assign_batch_notification_for_identifier_26
  def self.assign_batch_notification_for_identifier_26(identifier,options={})
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    if batch_notification.present? && batch_notification.is_valid_notification?
      tool_name = batch_notification.destination  
      if options[:user_ids].present?
        if options[:test_without_validation] == true
          users = User.where(:id.in=>options[:user_ids])
        else
          users = User.where(:id.in=>options[:user_ids],:signup_source.in=>["alexa","googleassistance"])
        end
      else
        users = User.where(:signup_source.in=>["alexa","googleassistance"])
      end
      users.batch_size(100).no_timeout.each do |user|
        batch_notification_manager = batch_notification
        current_user =  user.member
        break if !batch_notification_manager.valid_total_allowed_count?(current_user)
        child_id = current_user.ordered_child_list_excluding_pregnancy.first
        family_id = FamilyMember.where(member_id:child_id).last rescue nil 
        if batch_notification_manager.check_sent_before == true
          sent_in_x_day_status =  UserNotification.or({email_status:"sent"},{status:"sent"}).where(type:batch_notification.identifier,user_id:user.id.to_s ).last.present?
          if sent_in_x_day_status == false
            check_sent_before_status = PushNotificationSplitedData.or({email_status:"sent"},{status:"sent"}).where(type:batch_notification.identifier,user_id:user.id.to_s).last.present? 
          else
            check_sent_before_status = true
          end
        else
          check_sent_before_status = false
        end
        if child_id.present? && family_id.present? && !check_sent_before_status
          child = ChildMember.find(child_id)
          msg = "Track #{child.first_name}'s mental maths experience with Nurturey App dashboard"
          push_type = batch_notification_manager.identifier #push identifier
          priority = batch_notification_manager.priority.to_i
          push_message = msg
          destination_name = batch_notification_manager.destination #'premium_subscription'
          action_type = batch_notification_manager.action.downcase rescue ""
          batch_no = batch_notification_manager.batch_no.to_i
          options[:family_id] = options[:family_id] || family_id
          notification_expiry_date = Date.today + batch_notification_manager.expire_in_no_of_day.to_i.days
          UserNotification.add_push(batch_notification_manager,current_user,push_type,batch_no,priority,push_message,destination_name,child_id,action_type,content_url=nil,tool_id=nil,notification_expiry_date,score=1,identifier=nil,options)
        end
      end
    end
  end 

  # Mental math promo email  :
  # message: Let your child practise mental maths with Nurturey + Alexa
  # BatchNotification::Tools::MentalMaths.assign_batch_notification_for_identifier_28
  def self.assign_batch_notification_for_identifier_28(identifier,options={})
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    if batch_notification.present? && batch_notification.is_valid_notification?
      tool_name = batch_notification.destination  
      if options[:user_ids].present?
        if options[:test_without_validation] == true
          users = User.where(:id.in=>options[:user_ids])
        else
          users = User.where(:id.in=>options[:user_ids],:signup_source.nin=>["alexa","googleassistance"])
        end
      else
        users = User.where(:signup_source.nin=>["alexa","googleassistance"])
      end
      users.batch_size(100).no_timeout.each do |user|
        batch_notification_manager = batch_notification
        current_user =  user.member
        break if !batch_notification_manager.valid_total_allowed_count?(current_user)
        child_ids = current_user.ordered_child_list_excluding_pregnancy
        child_with_age_more_than_3_5_years = ChildMember.where(:id.in=>child_ids,:birth_date.lte=>Date.today- (12*3+5).months)
        child_id = child_with_age_more_than_3_5_years.first.id rescue nil
        family_id = FamilyMember.where(member_id:child_id).last rescue nil 
        if batch_notification_manager.check_sent_before == true
          sent_in_x_day_status =  UserNotification.or({email_status:"sent"},{status:"sent"}).where(type:batch_notification.identifier,user_id:user.id.to_s ).last.present?
          if sent_in_x_day_status == false
            check_sent_before_status = PushNotificationSplitedData.or({email_status:"sent"},{status:"sent"}).where(type:batch_notification.identifier,user_id:user.id.to_s).last.present? 
          else
            check_sent_before_status = true
          end
        else
          check_sent_before_status = false
        end
        if child_id.present? && family_id.present? && !check_sent_before_status
          child = ChildMember.find(child_id)
          msg = "Let your child practise mental maths with Nurturey + Alexa"
          push_type = batch_notification_manager.identifier #push identifier
          priority = batch_notification_manager.priority.to_i
          push_message = msg
          destination_name = batch_notification_manager.destination #'premium_subscription'
          action_type = batch_notification_manager.action.downcase rescue ""
          batch_no = batch_notification_manager.batch_no.to_i
          options[:family_id] = options[:family_id] || family_id
          notification_expiry_date = Date.today + batch_notification_manager.expire_in_no_of_day.to_i.days
          UserNotification.add_push(batch_notification_manager,current_user,push_type,batch_no,priority,push_message,destination_name,child_id,action_type,content_url=nil,tool_id=nil,notification_expiry_date,score=1,identifier=nil,options)
        end
      end
    end
  end 
  


  # Alexa cmd email - user adds/registers an amazon alexa device in mental maths
  # Message: Tips to use Nurturey mental maths with Alexa
  # BatchNotification::Tools::MentalMaths.assign_batch_notification_for_identifier_31
  def self.assign_batch_notification_for_identifier_31(identifier,options={})
    # if member is added
     batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    if batch_notification.present? && batch_notification.is_valid_notification?
      tool_name = batch_notification.destination  
      if options[:user_ids].present?
        if options[:test_without_validation] == true
          users = User.where(:id.in=>options[:user_ids])
        else
          users = User.where(:id.in=>options[:user_ids],:signup_source.nin=>["alexa","googleassistance"])
        end
      else
        users = []
      end
      users.batch_size(100).no_timeout.each do |user|
        batch_notification_manager = batch_notification
        current_user =  user.member
        break if !batch_notification_manager.valid_total_allowed_count?(current_user)
        msg = "Tips to use Nurturey mental maths with Alexa"
        push_type = batch_notification_manager.identifier #push identifier
        priority = batch_notification_manager.priority.to_i
        push_message = msg
        destination_name = batch_notification_manager.destination #'premium_subscription'
        action_type = batch_notification_manager.action.downcase rescue ""
        batch_no = batch_notification_manager.batch_no.to_i
        options[:family_id] = options[:family_id]
        notification_expiry_date = Date.today + batch_notification_manager.expire_in_no_of_day.to_i.days
        notification_obj = UserNotification.add_push(batch_notification_manager,current_user,push_type,batch_no,priority,push_message,destination_name,child_id=nil,action_type,content_url=nil,tool_id=nil,notification_expiry_date,score=1,identifier=nil,options)
        if notification_obj.present? &&  batch_notification.push_type == "Instant"
          notify_by = (batch_notification.notify_by || "push") rescue "push"
          # send realtime notification 
          if UserNotification.notify_by_push?(notify_by) || UserNotification.notify_by_email_and_push?(notify_by)
            UserNotification.push_notification(notification_obj,current_user,nil,nil,nil,{notification_type:"instant"}.merge(options)) rescue Rails.logger.info "Push not sent for id #{notification_obj.id}"
          end
          if UserNotification.notify_by_email?(notify_by) || UserNotification.notify_by_email_and_push?(notify_by)
            UserNotification.delay.send_email_notification(notification_obj,current_user, {notification_type:"instant"}.merge(options)) rescue Rails.logger.info "notification id #{notification_obj.id} not sent"
          end
        end     
      end
    end
  end

  def self.assign_batch_notification_for_identifier_32(identifier,options={})
     batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    if batch_notification.present? && batch_notification.is_valid_notification?
      tool_name = batch_notification.destination  
      if options[:user_ids].present?
        users = User.where(:id.in=>options[:user_ids])
        if options[:test_without_validation] == false
          member_ids = Devices::HomeDevice.where(:member_id.in=>users.map(&:member_id)).map(&:member_id)
          users =  User.where(:member_id.in=>member_ids)
        end
      else
        member_ids = Devices::HomeDevice.all.pluck(:member_id)
        users =  User.where(:member_id.in=>member_ids)
      end
      users.batch_size(100).no_timeout.each do |user|
        current_user = user.member   
        batch_notification_manager = batch_notification
        break if !batch_notification_manager.valid_total_allowed_count?(current_user)
        current_user.ordered_child_list_excluding_pregnancy.each do |child_id|
          child = Member.find(child_id)
          if options[:test_without_validation] == true
            children_with_age_greater_than_3_5_years = true
          else
            quiz_record_present = Child::AlexaQuiz::QuizDetail.where(member_id:child_id).present?
            children_with_age_greater_than_3_5_years = quiz_record_present || child.birth_date <= (Date.today - (3.years + 5.month))
          end
          msg = "Mental Maths summary of the last week for #{child.first_name}"
          push_type = batch_notification_manager.identifier #push identifier
          priority = batch_notification_manager.priority.to_i
          push_message = msg
          destination_name = batch_notification_manager.destination #'premium_subscription'
          action_type = batch_notification_manager.action.downcase rescue ""
          batch_no = batch_notification_manager.batch_no.to_i
          options[:family_id] = options[:family_id]
          notification_expiry_date = Date.today + batch_notification_manager.expire_in_no_of_day.to_i.days
          if children_with_age_greater_than_3_5_years
            notification_obj = UserNotification.add_push(batch_notification_manager,current_user,push_type,batch_no,priority,push_message,destination_name,child_id,action_type,content_url=nil,tool_id=nil,notification_expiry_date,score=1,identifier=nil,options)
          end
          
          if children_with_age_greater_than_3_5_years && notification_obj.present? &&  batch_notification.push_type == "Instant"
            notify_by = (batch_notification.notify_by || "push") rescue "push"
            # send realtime notification 
            if UserNotification.notify_by_push?(notify_by) || UserNotification.notify_by_email_and_push?(notify_by)
              UserNotification.push_notification(notification_obj,current_user,nil,nil,nil,{notification_type:"instant"}.merge(options)) rescue Rails.logger.info "Push not sent for id #{notification_obj.id}"
            end
            if UserNotification.notify_by_email?(notify_by) || UserNotification.notify_by_email_and_push?(notify_by)
              UserNotification.delay.send_email_notification(notification_obj,current_user, {notification_type:"instant"}.merge(options)) rescue Rails.logger.info "notification id #{notification_obj.id} not sent"
            end
          end     
        end
      end
    end
  end


end