class BatchNotification::Tools::ToothChart

  # All children + (tooth chart not active) + Age less than 10 years
  # message: I think <Child name> may have grown M teeth by now. Let's try my new 'Dental health' tool to track eruption and shedding of <Child name>'s teeth
  # BatchNotification::Tools::ToothChart.assign_batch_notification_for_identifier_15
  def self.assign_batch_notification_for_identifier_15(identifier,options={})
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    if batch_notification.present? && batch_notification.is_valid_notification?
      tool_name = batch_notification.destination  
      children_with_age_less_than_10_years = ChildMember.where(:birth_date => {"$gte"=> (Date.today - 10.years), "$lte"=>(Date.today - 5.months) })
      if options[:child_member_ids].present?
        if options[:test_without_validation] == true
          children_with_age_less_than_10_years = ChildMember.where(:id.in=>options[:child_member_ids])
        else
          children_with_age_less_than_10_years = children_with_age_less_than_10_years.where(:id.in=>options[:child_member_ids])
        end
      end
      children_with_age_less_than_10_years.no_timeout.each do |child|
        # if tool is not active
        if !child.is_expected?
          begin
            tool_id = nil
            type = ::Child::ToothChart::ToothDetails::Erupt
            status = "unknown"
            grown_tooth = Child::ToothChart::ToothDetails.child_data(child.id.to_s).erupt_data.count
            grown_teeth_count = ::Child::ToothChart::ToothDetails.eligible_tooth_chart_manager_for_child_age(child,type,status).count rescue 0
            msg = "I think #{child.first_name} may have grown #{grown_teeth_count} teeth by now. Let's try my new 'Dental health' tool to track eruption and shedding of #{child.first_name}'s teeth"
            if grown_tooth < 20 
              UserNotification.assign_push_to_all_elder_member(child,msg,batch_notification,tool_id,user_ids=nil,content_url=nil,options)
            end
          rescue Exception=>e 
            Rails.logger.info "Error in identifier 15"
            Rails.logger.info e.message
          end
        end
      end
    end
  end    
end