class BatchNotification::Tools::Immunisations

  def self.assign_batch_notification_for_identifier_7(identifier,options={})
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    if batch_notification.present? && batch_notification.is_valid_notification?
      max_limit_for_immunisaton = GlobalSettings::MaxAllowedPushCountTypeWise["immunisations"] + 0
      condition_list = []

      condition_list << {:due_on=> (Date.today + 2.week)..(Date.today+4.week) }
      condition_list << {:due_on=> (Date.today + 1.day)..(Date.today+1.week)}
      condition_list << {:due_on=>nil ,:est_due_time=>(Date.today + 2.week)..(Date.today+4.week)}
      condition_list << {:due_on=>nil,:est_due_time=>(Date.today + 1.day)..(Date.today+1.week)}
      
      vaccine_ids = Jab.where({"$or"=>condition_list}).pluck(:vaccination_id).uniq
      vaccines_group_by_child = Vaccination.vaccines_group_by_member(vaccine_ids)
      if options[:child_member_ids].present?
        child_members = ChildMember.where(:id.in=>options[:child_member_ids])
      else
        child_members = ChildMember.in(id:vaccines_group_by_child.keys)
      end
      child_members.batch_size(100).no_timeout.each do |child|
        if !child.is_expected?
          begin
            assinged_push_count = 0
            child_vaccines = vaccines_group_by_child[child.id]
            child_vaccines.each do |vaccine_id|
              break if assinged_push_count >= max_limit_for_immunisaton
              jabs_index_list = Jab.where(vaccination_id:vaccine_id.to_s).order_by("id asc").pluck(:id)
              vaccine = Vaccination.find vaccine_id
              vaccine_name = vaccine.get_name
              jabs = Jab.where({"$or"=>condition_list}).where(vaccination_id:vaccine_id)
              msg = ""
              jabs.no_timeout.each do |jab|
                feed_id = jab.id
                break if assinged_push_count >= max_limit_for_immunisaton
                if jab.due_on.present?
                  jab_date = jab.due_on.strftime("%d %b %Y")
                  jab_start_time = jab.start_time.utc.strftime("%I:%M %p")
                  msg = "PA alert: #{child.first_name}'s #{vaccine_name} #{(jabs_index_list.index(jab.id)+1).ordinalize} Jab is due on #{jab_date} at #{jab_start_time}. Keep your diary clear. :)"
                else
                  if jab.est_due_time >=(Date.today + 2.week)  && jab.est_due_time <= (Date.today+4.week)
                    msg = "#{child.first_name}'s #{vaccine_name} #{(jabs_index_list.index(jab.id)+1).ordinalize} Jab is due within a month. Let's not forget to take an appointment with the doctor." 
                  else
                    msg = "#{child.first_name}'s #{vaccine_name} #{(jabs_index_list.index(jab.id)+1).ordinalize} Jab is due within a week. Let's not forget to take an appointment with the doctor." 
                  end
                end
                if UserNotification.assign_push_to_all_elder_member(child,msg,batch_notification,feed_id,user_ids=nil,content_url=nil,options)
                   assinged_push_count = assinged_push_count + 1 
                end
                 
              end
            end
          rescue Exception=>e 
            Rails.logger.info e.message
          end
        end
      end
    end
  end


  def self.assign_batch_notification_for_identifier_8(identifier,options={})
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    if batch_notification.present? && batch_notification.is_valid_notification?
      max_limit_for_immunisaton = GlobalSettings::MaxAllowedPushCountTypeWise["immunisations"] + 0
      condition_list = []

      condition_list << {:due_on.lt => Date.today  }
      condition_list << {:due_on=>nil ,:est_due_time.lt=>Date.today}
      
      vaccine_ids = Jab.where({"$or"=>condition_list}).pluck(:vaccination_id).uniq
      vaccines_group_by_child = Vaccination.vaccines_group_by_member(vaccine_ids)
      tool_name = batch_notification.destination 
      if options[:child_member_ids].present?
        child_members = ChildMember.where(:id.in=>options[:child_member_ids])
      else
        child_members = ChildMember.in(id:vaccines_group_by_child.keys)
      end

      child_members.no_timeout.each do |child|
        if !child.is_expected?
          begin
            child_vaccines = vaccines_group_by_child[child.id]
            no_of_vaccine = child_vaccines.count
            msg = "It appears that #{child.first_name} has #{no_of_vaccine} overdue Immunisations. It is very important to preserve immunisation records till adulthood. Let's update the dates now in case of discrepancy"
            UserNotification.assign_push_to_all_elder_member(child,msg,batch_notification,feed_id=nil,user_ids=nil,content_url=nil,options)
          rescue Exception=>e 
            Rails.logger.info e.message
          end
        end
      end
    end
  end
 
    
end