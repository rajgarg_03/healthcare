class BatchNotification::Tools::Subscription

  def self.assign_batch_notification_for_identifier_24(identifier,options={})
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    # check  scenario added in batch notification with active status
    if batch_notification.present? && batch_notification.is_valid_notification?
      notify_by_push  = UserNotification.notify_by_push?(batch_notification.notify_by)
      # assign only mobile users if only push is to be sent
      if notify_by_push
        member_ids = Device.not_in(token:nil).pluck(:member_id)
        members = ParentMember.in(:id=>member_ids)
      else
        members = ParentMember.all
      end
      if options[:user_ids].present?
        members = Member.in(:user_id=>options[:user_ids])
      end
      members.batch_size(100).no_timeout.each do |current_user|
        batch_notification_manager = batch_notification
        user =  current_user.user
        if options[:test_without_validation] == true 
          valid_to_notify =  true
        else
         valid_to_notify =  !user.has_premium_subscription_for_family?
        end
        break if !batch_notification_manager.valid_total_allowed_count?(current_user)
        family_id = user.valid_family_to_subscribe(current_user.user_families).id rescue nil
        if valid_to_notify && family_id.present?
          msg = "Try Premium Subscription & help us build the smartest assistant"
          push_type = batch_notification_manager.identifier #push identifier
          priority = batch_notification_manager.priority.to_i
          push_message = msg
          child_id = nil
          destination_name = batch_notification_manager.destination #'premium_subscription'
          action_type = batch_notification_manager.action.downcase rescue ""
          batch_no = batch_notification_manager.batch_no.to_i
          options[:family_id] = options[:family_id] || family_id
          notification_expiry_date = Date.today + batch_notification_manager.expire_in_no_of_day.to_i.days
          UserNotification.add_push(batch_notification_manager,current_user,push_type,batch_no,priority,push_message,destination_name,child_id,action_type,content_url=nil,tool_id=nil,notification_expiry_date,score=1,identifier=nil,options)
        end
      end
    end
  end

  def self.assign_batch_notification_for_identifier_25(identifier,options={})
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    # check  scenario added in batch notification with active status
    if batch_notification.present? && batch_notification.is_valid_notification?
      notify_by_push  = UserNotification.notify_by_push?(batch_notification.notify_by)
      # assign only mobile users if only push is to be sent
      if notify_by_push
        member_ids = Device.not_in(token:nil).pluck(:member_id)
        members = ParentMember.in(:id=>member_ids)
      else
        members = ParentMember.all
      end
      if options[:user_ids].present?
        members = Member.in(:user_id=>options[:user_ids])
      end
      members.batch_size(100).no_timeout.each do |current_user|
        batch_notification_manager = batch_notification
        user =  current_user.user
        if options[:test_without_validation] == true
          valid_to_notify =  true
        else
          break if !batch_notification_manager.valid_total_allowed_count?(current_user)
    
          # check if  1st Email + 10 days
          first_notification_for_subscription = UserNotification.where(user_id:user.id,type:24).or({:status=>"sent"},{:email_status=>"sent"}).last
          break if first_notification_for_subscription.blank? || (first_notification_for_subscription.updated_at > Date.today - 10.days)
          valid_to_notify = user.subscription_popup_status(nil,options)
        end
        family_id = user.valid_family_to_subscribe(current_user.user_families).id rescue nil
        
        if valid_to_notify && family_id.present?
          msg = "Try Premium Subscription & help us build the smartest assistant"
          push_type = batch_notification_manager.identifier #push identifier
          priority = batch_notification_manager.priority.to_i
          push_message = msg
          child_id = nil
          destination_name = batch_notification_manager.destination #'premium_subscription'
          action_type = batch_notification_manager.action.downcase rescue ""
          batch_no = batch_notification_manager.batch_no.to_i
          options[:family_id] = options[:family_id] || family_id
          notification_expiry_date = Date.today + batch_notification_manager.expire_in_no_of_day.to_i.days
          UserNotification.add_push(batch_notification_manager,current_user,push_type,batch_no,priority,push_message,destination_name,child_id,action_type,content_url=nil,tool_id=nil,notification_expiry_date,score=1,identifier=nil,options)
        end
      end
    end
  end


  # def self.assign_batch_notification_for_identifier_27(identifier,options={})
  #   batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
  #   # check  scenario added in batch notification with active status
  #   if batch_notification.present? && batch_notification.is_valid_notification?
  #     notify_by_push  = UserNotification.notify_by_push?(batch_notification.notify_by)
  #     # assign only mobile users if only push is to be sent
  #     if notify_by_push
  #       member_ids = Device.not_in(token:nil).pluck(:member_id)
  #       members = ParentMember.in(:id=>member_ids)
  #     else
  #       members = ParentMember.all
  #     end
  #     if options[:user_ids].present?
  #       members = Member.in(:user_id=>options[:user_ids])
  #     end
  #     members.batch_size(100).no_timeout.each do |current_user|
  #       batch_notification_manager = batch_notification
  #       user =  current_user.user
  #       if options[:test_without_validation] == true
  #         valid_to_notify =  true
  #       else
  #         break if !batch_notification_manager.valid_total_allowed_count?(current_user)
    
  #         # check if  1st Email + 10 days
  #         first_notification_for_subscription = UserNotification.where(user_id:user.id,type:24).or({:status=>"sent"},{:email_status=>"sent"}).last
  #         break if first_notification_for_subscription.blank? || (first_notification_for_subscription.updated_at > Date.today - 10.days)
  #         valid_to_notify = user.subscription_popup_status(nil,options)
  #       end
  #       family_id = user.valid_family_to_subscribe(current_user.user_families).id rescue nil
        
  #       if valid_to_notify && family_id.present?
  #         msg = "Try Premium Subscription & help us build the smartest assistant"
  #         push_type = batch_notification_manager.identifier #push identifier
  #         priority = batch_notification_manager.priority.to_i
  #         push_message = msg
  #         child_id = nil
  #         destination_name = batch_notification_manager.destination #'premium_subscription'
  #         action_type = batch_notification_manager.action.downcase rescue ""
  #         batch_no = batch_notification_manager.batch_no.to_i
  #         options[:family_id] = options[:family_id] || family_id
  #         notification_expiry_date = Date.today + batch_notification_manager.expire_in_no_of_day.to_i.days
  #         UserNotification.add_push(batch_notification_manager,current_user,push_type,batch_no,priority,push_message,destination_name,child_id,action_type,content_url=nil,tool_id=nil,notification_expiry_date,score=1,identifier=nil,options)
  #       end
  #     end
  #   end
  # end



end