class BatchNotification::Tools::VisionHealth

  def self.assign_batch_notification_for_identifier_34(identifier,options={})
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    if batch_notification.present? && batch_notification.is_valid_notification?
      tool_name = batch_notification.destination
      # member with age 3 years 
      if options[:child_member_ids].present?
        if options[:test_without_validation] == true
          child_members = ChildMember.where(:id.in=>options[:child_member_ids])
        else
          child_members = ChildMember.where(:birth_date=>{"$lte"=> (Date.today - 3.years), "$gte"=> (Date.today - 5.years) }).where(:id.in=>options[:child_member_ids])
        end
      else
        child_members = ChildMember.where(:birth_date=> {"$lte"=> (Date.today - 3.years), "$gte"=> (Date.today - 5.years) })
      end
      options[:check_platform] = true
      options[:valid_device_list_for_notification] = ["ios"]
      child_members.no_timeout.each do |child|
        if !child.is_expected?
          event_on_vision = Child::VisionHealth::VisionTestEvent.get_event(child.id) 
          if event_on_vision.blank?
            begin
              # msg = "Try today! Gold standard online color vision test for #{child.first_name}, fun for kids, informative for parents"
              msg = "Test #{child.first_name} for colour blindness. Gold standard online color vision test can be done in minutes!"
              UserNotification.assign_push_to_all_elder_member(child,msg,batch_notification,feed_id=nil,user_ids=nil,content_url=nil,options)
            rescue Exception=>e 
              Rails.logger.info "Error in identifier 34"
              Rails.logger.info e.message
            end
          end
        end
      end
    end
  end
  
end