class BatchNotification::Tools::Pointers

  def self.assign_batch_notification_for_identifier_pointer(identifier,options={})
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    # check  scenario added in batch notification with active status
    if batch_notification.present? && batch_notification.is_valid_notification?
      notify_by_push  = UserNotification.notify_by_push?(batch_notification.notify_by)
      # assign pointers only mobile users if only push is to be sent
      if notify_by_push
        member_ids = Device.not_in(token:nil).pluck(:member_id)
        members = ParentMember.in(:id=>member_ids)
      else
        members = ParentMember.where(:user_id.in=>User.where(activity_status: "Active").only("id").pluck(:id))
      end
      if options[:user_ids].present?
        members = Member.in(:user_id=>options[:user_ids])
      end
      options[:scenario_tool_class] = "SysArticleRef"
      system_pointer_custome_ids = {}
      SysArticleRef.only("id","custome_id").map{|a| system_pointer_custome_ids[a["id"].to_s] = a["custome_id"] }
      options[:system_pointer_custome_ids] =  system_pointer_custome_ids
      members.batch_size(500).no_timeout.each do |current_user|

        batch_notification_manager = batch_notification
        break if !batch_notification_manager.valid_total_allowed_count?(current_user)
        pointer_limit = GlobalSettings::MaxAllowedPushCountTypeWise["pointers"]
        UserNotification.add_scenario_notification(current_user,batch_notification_manager,pointer_limit,options)
      end
    end
  end
end