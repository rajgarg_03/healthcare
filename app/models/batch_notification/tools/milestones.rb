class BatchNotification::Tools::Milestones



def self.assign_batch_notification_for_identifier_3(identifier,options={})
  begin
    max_limit_for_milestone = GlobalSettings::MaxAllowedPushCountTypeWise["measurements"] + 5
    @notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    batch_notification_manager = @notification
    if @notification.present? && @notification.is_valid_notification?
      if options[:child_member_ids].present?
        child_members = ChildMember.in(:id=>options[:child_member_ids])
      else
        child_members = ChildMember.where(:birth_date.lte=>Date.today)
      end
      
      child_members.no_timeout.batch_size(100).each do |child|
        batch_notification_manager = @notification
        break if !batch_notification_manager.valid_total_allowed_count?(current_user = nil,options)
        
        if !child.is_expected?
          begin
            acheived_milestones_within_month = Milestone.achieved_milestones(child).where(:date.gte=> Date.today - 30.days).order_by("date desc")
            achived_system_milstone_without_picuture = acheived_milestones_within_month.select {|milestone| !milestone.pictures.present? }.map{|a| a.id.to_s}
            # Filter out milestone already sent
            sent_milestone_ids = UserNotification.in(feed_id:achived_system_milstone_without_picuture).where(status:"sent",type:@notification.identifier,member_id:child.id.to_s).pluck(:feed_id)
            pending_to_sent_milestone_ids = achived_system_milstone_without_picuture - sent_milestone_ids

            assinged_push_count = 0
            if options[:test_without_validation]
              pending_to_sent_milestone_ids = Milestone.where(member_id:child.id).map(&:id)
            end
            pending_to_sent_milestone_ids.each do |milestone_id|
              milestone = Milestone.find(milestone_id)
              system_milestone = SystemMilestone.find(milestone.system_milestone_id)
              push_message = "Congratulations on #{child.first_name} achieving the milestone " + '"' + system_milestone.title + '". Click to upload pictures of the milestone so that I can preserve an ever lasting memory'
              family_elder_member_ids =  child.family_elder_members.map(&:to_s)
              user_ids  = User.where(:app_login_status=>true).in(member_id:family_elder_member_ids,status:["confirmed","approved"]).pluck(:id).map(&:to_s)
              push_type = @notification.identifier #push identifier
              priority = @notification.priority.to_i
              child_id = child.id.to_s
              tool_id = milestone.id.to_s
              destination_name = @notification.destination
              action_type = @notification.action.downcase rescue ""
              batch_no = @notification.batch_no.to_i
              family_id = FamilyMember.where(member_id:child_id).first.family_id
              notification_expiry_date = Date.today + @notification.expire_in_no_of_day.to_i.days
              push_assigned_status = false
              Member.in(user_id:user_ids).each do |member|
                begin

                  notification_obj = UserNotification.add_push(batch_notification_manager,member,push_type,batch_no,priority,push_message,destination_name,child_id,action_type,content_url=nil,tool_id,notification_expiry_date,score=1,identifier=nil,options)
                  if notification_obj.present?
                    push_assigned_status = true
                    record_object =  BatchNotification::PushNotification.create(:action_for_tool=>destination_name,
                      :message=> push_message,
                      :action_type => action_type,
                      :unique_id=> nil, 
                      :object_id=>member.id,
                      :family_id => family_id,
                      :child_id => child.id,
                      :schedule_push_date=> Date.today
                    )
                  end
                rescue Exception=>e 
                  Rails.logger.info e.message
                end
              end
              assinged_push_count =  assinged_push_count + 1 if push_assigned_status == true
              break if assinged_push_count >= max_limit_for_milestone
            end
          rescue Exception=>e 
            puts e.message
            Rails.logger.info e.message
          end
        end
      end
    end
    rescue Exception=>e 
      puts e.message
      Rails.logger.info e.message
    end
  end



  def self.assign_batch_notification_for_identifier_2(identifier,options={})
    begin
      max_limit_for_milestone = GlobalSettings::MaxAllowedPushCountTypeWise["milestones"] + 5
      @notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
      batch_notification_manager = @notification
      if @notification.present? && @notification.is_valid_notification?
        if options[:child_member_ids].present?
          child_members = ChildMember.where(:id.in=>options[:child_member_ids])
        else
          child_members = ChildMember.where(:birth_date.lte=>Date.today)
        end
        child_members.no_timeout.batch_size(100).each do |child|
          
          batch_notification_manager = @notification
          break if !batch_notification_manager.valid_total_allowed_count?(current_user = nil,options)
          
          if !child.is_expected?
             begin
              system_milestone_ids = Milestone.unaccomplished_milestone_having_no_info_from_user(child).pluck(:id).map(&:to_s)
              if options[:test_without_validation]
                system_milestone_ids =  Milestone.where(:member_id=>child.id.to_s).pluck(:system_milestone_id).map(&:to_s) #if system_milestone_ids.blank?
              end
              data = {}
              Milestone.in(system_milestone_id:system_milestone_ids).where(member_id:child.id.to_s).only(:system_milestone_id,:id).no_timeout.each{|a| data[a.system_milestone_id.to_s]= a.id.to_s}
              assinged_push_count = 0
              SystemMilestone.in(id:data.keys).order_by_important_status.no_timeout.each do |system_milestone|
                if system_milestone.notification_message.present?
                  milestone_msg = system_milestone.notification_message
                  milestone_msg = milestone_msg %{:child_name=>child.first_name}
                  push_message = milestone_msg.strip + " Mention Yes or No so that I can track this developmental milestone"
                else
                  push_message =  "Has #{child.first_name} accomplished the milestone " + '"' + system_milestone.title + '" yet? Mention Yes or No so that I can track this developmental milestone'
                end
                family_elder_member_ids =  child.family_elder_members.map(&:to_s)
                user_ids  = User.where(:app_login_status=>true).in(member_id:family_elder_member_ids,status:["confirmed","approved"]).pluck(:id).map(&:to_s)
                push_type = @notification.identifier #push identifier
                priority = @notification.priority.to_i
                child_id = child.id.to_s
                tool_id = data[system_milestone.id.to_s] 
                destination_name = @notification.destination
                action_type = @notification.action.downcase rescue ""
                batch_no = @notification.batch_no.to_i
                family_id = FamilyMember.where(member_id:child_id).first.family_id
                notification_expiry_date = Date.today + @notification.expire_in_no_of_day.to_i.days
                push_assigned_status = false
                Member.in(user_id:user_ids).no_timeout.each do |member|
                  notification_obj = UserNotification.add_push(batch_notification_manager,member,push_type,batch_no,priority,push_message,destination_name,child_id,action_type,content_url=nil,tool_id,notification_expiry_date,score=1,identifier=nil,options)
                  if notification_obj.present?
                    push_assigned_status = true
                    record_object =  BatchNotification::PushNotification.create(:action_for_tool=>destination_name,
                      :message=> push_message,
                      :action_type => action_type,
                      :unique_id=> nil, 
                      :object_id=>member.id,
                      :family_id => family_id,
                      :child_id => child.id,
                      :schedule_push_date=> Date.today
                    )
                  end
                end
                assinged_push_count =  assinged_push_count + 1 if push_assigned_status == true
                break if assinged_push_count >= max_limit_for_milestone
              end
            rescue Exception=>e 
              puts e.message
              Rails.logger.info e.message
            end
        end
        end
      end
    rescue Exception=>e 
      puts e.message
      Rails.logger.info e.message
    end
  end


  def self.assign_batch_notification_for_identifier_9(identifier,options={})
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    if batch_notification.present? && batch_notification.is_valid_notification?
      tool_name = batch_notification.destination
      if options[:child_member_ids].present?
        child_members = ChildMember.where(:id.in=>options[:child_member_ids])
      else
        child_members = ChildMember.all
      end
      child_members.no_timeout.batch_size(100).each do |child|
       
        batch_notification_manager = batch_notification
        break if !batch_notification_manager.valid_total_allowed_count?(current_user = nil,options)
          
        if !child.is_expected?

          pointer_476 = ArticleRef.where(custome_id:476,member_id:child.id.to_s).last
          if options[:test_without_validation]
            pointer_476 = pointer_476 || ArticleRef.where(member_id:child.id.to_s).first
          end
          if pointer_476.present?
            begin
              msg = "By properly tracking 'milestones' of #{child.first_name}, we can proactively keep an eye on any emerging social, physical and cognitive developmental issues. Click to learn why it is so effective"
              tool_id = pointer_476.id
              UserNotification.assign_push_to_all_elder_member(child,msg,batch_notification,tool_id,user_ids=nil,nil,options)
            rescue Exception=>e 
              Rails.logger.info e.message
            end
          end
        end
      end
    end
  end

end