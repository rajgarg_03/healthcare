class BatchNotification::Nhs::SyndicatedContent

  # Assign push for NHSContent
  # BatchNotification::Nhs::SyndicatedContent.assign_batch_notification_for_identifier_36(identifier)
  def self.assign_batch_notification_for_identifier_36(identifier,options={})
    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    # check  scenario added in batch notification with active status
    if batch_notification.present? && batch_notification.is_valid_notification?
      notify_by_push  = UserNotification.notify_by_push?(batch_notification.notify_by)
      # assign faq questions only mobile users if only push is to be sent
      if notify_by_push
        member_ids = Device.not_in(token:nil).pluck(:member_id)
        members = ParentMember.in(:id=>member_ids)
      else
        members = ParentMember.all
      end
      if options[:user_ids].present?
        members = Member.in(:user_id=>options[:user_ids])
      end
      valid_country_codes = Member.sanitize_country_code("uk")
      options[:scenario_tool_class] = "Nhs::SyndicatedContent"
      members.batch_size(100).no_timeout.each do |current_user|
        user = current_user.user
        # Push only for UK users
        if (valid_country_codes.include?(user.country_code) rescue false)
          batch_notification_manager = batch_notification
          break if !batch_notification_manager.valid_total_allowed_count?(current_user)
          nhs_content_limit = GlobalSettings::MaxAllowedPushCountTypeWise["nhs content"]
          UserNotification.add_scenario_notification(current_user,batch_notification_manager,nhs_content_limit,options)
        end
      end
    end
  end




  # Assign push for NHSPromo email to all active UK  users
  # BatchNotification::Nhs::SyndicatedContent.assign_batch_notification_for_identifier_37(identifier)
  def self.assign_batch_notification_for_identifier_37(identifier,options={})

    batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(identifier,options)
    if batch_notification.present? && batch_notification.is_valid_notification?
      tool_name = batch_notification.destination  
      push_type = batch_notification.identifier #push identifier
      idenitifer = batch_notification.identifier
      priority = batch_notification.priority.to_i
      push_message = "Introducing NHS Library!"
      child_id = nil
      score = options[:score] || 1
      destination_name = batch_notification.destination
      action_type = batch_notification.action.downcase rescue ""
      batch_no = batch_notification.batch_no.to_i
      notification_expiry_date = Date.today + batch_notification.expire_in_no_of_day.to_i.days
      country_code = Member.sanitize_country_code("uk")
      
      if options[:user_ids].present?  
        if options[:test_without_validation] == true
          user_list = User.where(:id.in=>options[:user_ids])
        else
          user_list = User.where(:country_code.in=>country_code, :id.in=>options[:user_ids])
        end
      else
        user_list = User.where(:country_code.in=>country_code,:activity_status=>"Active")
      end
      user_list.no_timeout.batch_size(100).each do |user|

        begin
          member = user.member
          UserNotification.add_push(batch_notification,member,push_type,batch_no,priority,push_message,destination_name,child_id,action_type,content_url=nil,tool_id=nil,notification_expiry_date,score,idenitifer,options)
          break if !batch_notification_manager.valid_total_allowed_count?(member,options)  
        rescue Exception=>e 
          Rails.logger.info "Error in identifier 37..........................."
          Rails.logger.info e.message
        end
      end
    end
  end 
end
