class SystemJab
  include Mongoid::Document
  include Mongoid::Timestamps

  
  # field :desc, type: String
  field :status, type: String , :default => "Planned"
  field :jab_type , type: String, :default => "Injection" # Injection/Oral/Nasal Spray
  field :due_on , type: String 
  # field :doc_name , type: String
  field :route, type: String
  field :jab_trade_name, type: String # depricated
  field :facility, type: String
  field :jab_location, type: String
  field :note, type: String
  field :name, type: String
  field :gpsoc_immunisation_jab_custom_id, type:String
  field :given_after_birth_date, type: String

  # field :due_on_in_day, type: String 
  field :due_on_in_day_int, type: Integer
  belongs_to :system_vaccin
  index ({system_vaccin_id:1})
  
  def gpsoc_immunisation_jab
    jab = self
    ::System::GpsocImmunisationJab.where(custom_id:jab.gpsoc_immunisation_jab_custom_id).last rescue nil
  end

  def get_jab_detail
    system_jab = self
    begin
    system_vaccine = SystemVaccin.find(system_jab.system_vaccin_id)
    if system_vaccine.present?
      system_jab.name = "#{system_vaccine.name} - #{system_vaccine.country}" 
      system_jab
    else
      system_jab = nil
    end
  rescue Exception =>e 
    nil
  end
     
  end

def get_system_jab_route(options={})
    jab  = self
    if options[:immunisation_version].to_i > 0
      SystemVaccin::CategoryMapping[jab.jab_type]
    else
      jab.route
    end       

end   

  def trade_name(system_vaccine=nil)
    self["jab_trade_name"] || (system_vaccine || self.system_vaccin).trade_name
  end

   

  def self.convert_to_days(age)
    age.gsub!(".", " ")
    age.gsub!("old"," ")
    age.gsub!("months", " * 30")
    age.gsub!("days", " * 1")
    age.gsub!("weeks", " * 7")
    age.gsub!("week", " * 7")
    age.gsub!("years", " * 365")
    age.gsub!("year", " * 365")
    age.gsub!("month", " * 30")
    age.gsub!("day", " * 1")
    age.gsub!("and", " + ")
    age.gsub!(",", " + ")
    eval(age) rescue 0
  end


  def self.save_due_on_day
      SystemJab.all.each do |jab|
      puts "#{jab.id} => #{jab.due_on}"
      jab.due_on_in_day_int = SystemJab.convert_to_days(jab.due_on)
      jab.save
    end
  end

# Used for admin panel
   def update_record(params)
    begin
      system_jab = self
      jab_params = params["SystemJab".underscore]
      jab_params[:due_on] = params[:duration] + "." + params[:frequency]
       
      if system_jab.update_attributes(jab_params)
        status = true
      else
        status = false
      end
    rescue Exception => e
      Rails.logger.info e.message
      status = false 
    end
    [system_jab.system_vaccin,status]
  end


  def self.create_record(params)
    begin
      jab_params = params["SystemJab".underscore]
      jab_params[:due_on] = params[:duration] + "." + params[:frequency]
      system_jab = self.new(jab_params)
      system_jab.save!
      system_vaccine = system_jab.system_vaccin
      system_vaccine.jabs_count = SystemJab.where(system_vaccin_id:system_vaccine.id).count
      system_vaccine.save
      status = true
    rescue Exception => e
      Rails.logger.info e.message
      status = false 
    end
    [system_vaccine,status]
  end

  def self.duplicate_record(params)
    record_to_be_duplicated = SystemJab.find(params[:record_id]).attributes
    record_to_be_duplicated.delete("_id")
    record_to_be_duplicated.delete("created_at")
    record = SystemJab.new(record_to_be_duplicated)
    record.save
      system_vaccine = record.system_vaccin
      system_vaccine.jabs_count = SystemJab.where(system_vaccin_id:system_vaccine.id).count
      system_vaccine.save
      system_vaccine.reload
      [record,system_vaccine]
  end

  def self.delete_record(record_id)
    begin
      system_jab = SystemJab.find(record_id)
      system_jab.delete
      system_vaccine = system_jab.system_vaccin
      system_vaccine.jabs_count = SystemJab.where(system_vaccin_id:system_vaccine.id).count
      system_vaccine.save
      
    rescue Exception => e
      Rails.logger.info e.message
    end
  end


end
