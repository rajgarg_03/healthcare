class ChildMember < Member

  attr_accessor :skip_birth_date_validation

  has_many :timelines , :foreign_key => :member_id, inverse_of: :member
  has_one :pregnancy, foreign_key: :expected_member_id

  validates :first_name, presence: true

  validate :birth_date_cannot_be_greater_than_today, unless: lambda{|child_member| child_member.skip_birth_date_validation}


  def birth_date_cannot_be_greater_than_today
    errors.add(:birth_date,"add birth date") if birth_date.blank? 
    errors.add(:birth_date, "can't be greater than today") if birth_date.present? && birth_date > Time.zone.now.to_date
  end

  def get_family(options={})
    child = self
    FamilyMember.where(member_id:child.id).last.family rescue nil
  end
  
  def delete_child_member(member_id=nil,options={})
  begin
      member =  self
      member_type = member.is_expected? ? "pregnancy" : "member"
      member_id = member.id.to_s
      options[:member_id] = member_id
      options[:case] = "member_removed"
      user_id = options[:current_user].user_id rescue nil
      family_ids = Array(options[:family_id]) ||  FamilyMember.where(member_id:member_id).pluck(:family_id).uniq
      FamilyMember.where(member_id:member_id).delete_all
      if options[:update_user_stage] != false 
        member.delay.update_familyuser_stage_on_add_or_delete_member(family_ids,options)
      end
      if member_type == "pregnancy"
        report_event = Report::Event.where(name:"pregnancy deleted from to family").last
      else
        report_event = Report::Event.where(name:"child deleted from family").last
      end
      begin
        ::Report::ApiLog.log_event(user_id,family_ids.last,report_event.custom_id,options) 
      rescue Exception=>e 
        Rails.logger.info ".............Error inside delete_child_member in log event.........."
      end
      member.delay.delete_member_records(member_id,options)
    rescue Exception =>e 
      Rails.logger.info ".............Error inside delete_child_member  #{e.message}.........."
      Rails.logger.info "member not delete: member id is - #{member.id}"
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,{member_id:member_id},"Error inside delete_child_member")
    end
  end

  def delete_member_records(member_id,options={})
    member = self
    vaccins = Vaccination.where(member_id:member_id)#.delete_all
    jabs = Jab.in(vaccination_id:vaccins.pluck(:id)).delete_all
    
    family_ids = Array(options[:family_id]) ||  FamilyMember.where(member_id:member_id).pluck(:family_id).uniq
    Vaccination.where(member_id:member_id).delete_all
    Picture.where(imageable_type: "Timeline").in(imageable_id:member.timelines.pluck(:id)).delete_all
    Timeline.where(member_id:member_id).delete_all
    Event.where(member_id:member_id).delete_all
    Notification.delete_notification(member_id)
    UserNotification.in(member_id:member_id).delete_all
    Pregnancy.where(expected_member_id:member_id).delete_all
    MemberNutrient.where(member_id:member_id).delete_all

    Milestone.where(member_id:member_id).delete_all
    Health.where(member_id:member_id).delete_all
    Document.where(member_id:member_id).delete_all
    Picture.where(member_id:member_id).delete_all rescue nil
    ArticleRef.where(member_id:member_id).delete_all
    ::Child::Scenario.where(member_id:member_id).delete_all
    MedicalEvent.where(member_id:member_id).delete_all
    Score.where(member_id:member_id).delete_all rescue nil
    checkup_ids = Child::CheckupPlanner::Checkup.where(member_id:member_id).pluck(:id)#.delete_all
    Child::CheckupPlanner::CheckupTest.where(:checkup_id.in=>checkup_ids).delete_all
    Child::CheckupPlanner::Checkup.where(:id.in=>checkup_ids).delete_all
    Child::HealthCard::Allergy.where(member_id:member_id).delete_all
    ::Clinic::LinkDetail.where(member_id:member_id).delete_all
    ::Clinic::User.where(member_id:member_id).delete_all
    member.destroy

  end


  def update_familyuser_stage_on_add_or_delete_member(family_ids, options={})
    begin
      if options[:family_elder_member_ids].present?
        family_elder_member_ids = options[:family_elder_member_ids]
      else
        family_elder_member_ids = FamilyMember.where(:family_id.in=>family_ids,:role.nin=>["Son","Daughter"]).pluck(:member_id).uniq
      end
      User.where(:member_id.in=>family_elder_member_ids).each do |user|
        stage_value = nil
        user.delay.update_user_stage(stage_value,options)
      end
    rescue Exception=>e 
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,{member_id:member_id},"Error inside update_user_stage_for_family_user_after_deleted_member")
    end  
  end
  #Api for save preg child 
  def preg_to_born(preg,current_user, member_data,api_version=1,options={})
    begin
      first_member = self #Member.find(first_member_id)
      family_member = first_member.family_members.first
      family = family_member.family
      family_creator = family.member
      activated_tools_controller = []
      activated_tools = []
      activated_tools_controller_list = {}
      activated_tools_list = {}
      Score.where(family_id:preg.family_id,activity_type: "Member",activity_id:self.id.to_s).first.delete rescue nil
      no_of_children = member_data.count
      if options[:activated_tools_controller] == true
        is_multiple_child_added = false
      else
        is_multiple_child_added = no_of_children > 1
        added_children_name_list = member_data.map{|a| (a[:first_name] rescue nil)}.compact
        options = options.merge(:added_children_name_list=>added_children_name_list.compact,:multiple_child_added=>is_multiple_child_added)
      end
      (member_data||[]).each do |data|
        if options[:activated_tools_controller] == true
          options = options.merge(:added_children_name_list=>data[:first_name],:multiple_child_added=>is_multiple_child_added)
        end
  
        role = data.delete("family_members")["role"] rescue "Son"
        child_member = ChildMember.new(data)
        child_member.last_name = family_creator.last_name
        if child_member.save
          child_member_id = child_member.id.to_s
          child_member.family_members.create(family_id:family.id, role:role)
          activated_tools,activated_tools_controller_data = child_member.run_add_child_setup_contoller(api_version,current_user,options)
          activated_tools_list[child_member_id] = activated_tools
          activated_tools_controller_list[child_member_id] = activated_tools_controller_data
            
          if options[:activated_tools_controller] != true
            if activated_tools_controller.blank?
              activated_tools_controller = activated_tools_controller_data
            else
              activated_tools_controller[:activated_tools_screens] = activated_tools_controller[:activated_tools_screens] +  activated_tools_controller_data[:activated_tools_screens] if activated_tools_controller_data.present?
            end
          end

          preg.added_child.present? ? (preg.added_child << child_member.id) : (preg.added_child = [child_member.id])
          Health.new.health_setting(child_member.id)
          #Timeline.save_system_timeline_to_member(child_member.id,current_user.id)
          Timeline.save_future_system_timeline_to_member(child_member.id,current_user.id)
          Member.add_system_entries(child_member,current_user,api_version)
          Score.create(family_id: family.id, activity_type: "Member", activity_id: child_member.id, activity: "Added #{child_member.first_name} to #{family.name}", point: Score::Points["Child"], member_id: child_member.id)
          child_member.update_member_ref("add") rescue nil
        end
      end
    rescue Exception=>e
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside preg_to_born")
      raise e.message
    end
     
    if options[:activated_tools_controller] == true
      [activated_tools_list,activated_tools_controller_list]
    else
      [activated_tools,activated_tools_controller]
    end
  end 

  def update_system_entries(family_id,child_member)
    family = Family.find(family_id)
    FamilyMember.create(member_id:child_member.id, family_id:family_id)
    Timeline.save_system_timeline_to_member(child_member.id,current_user.id)
    Health.new.health_setting(child_member.id)
    Score.create(family_id: family_id, activity_type: "Member", activity_id: child_member.id, activity: "Added #{child_member.first_name} to #{family.name}", point: Score::Points["Child"], member_id: child_member.id)
  end

  def get_nutrients(api_version,current_user,options={})
    api_version = api_version.present? ? api_version : 1
    critriea =   is_expected? ? Nutrient.for_pregnancy(api_version,current_user,options) : Nutrient.for_born_child(api_version,current_user,options)
    critriea.options.delete(:sort)
    critriea.order_by("tool_category DESC, position ASC")
  end

  def is_expected?
    return self.pregnancy_status unless self.pregnancy_status.nil?
    status = pregnancy.present? ? true : false
    self.update_attribute(:pregnancy_status,status)
    status
  end


  def male?
    family_member = FamilyMember.where(member_id: id).first
    family_member.role.gsub(/\"/, '').downcase == 'son' if family_member
  end  

  def get_quiz_level
    is_expected? ? nil : (self.quiz_level || 1)
  end

  def save_quiz_level(current_user,options={})
    member = self
    birth_date = member.birth_date
    if birth_date <= Date.today - 10.years
      quiz_level = 5
    elsif birth_date >= (Date.today - 9.years) && birth_date < (Date.today- 8.years)
      quiz_level = 4
    elsif birth_date >= (Date.today - 8.years) && birth_date < (Date.today- 6.years)
      quiz_level = 3
    elsif birth_date >= (Date.today - 6.years) && birth_date < (Date.today- 4.years)
      quiz_level = 2
    elsif birth_date >= (Date.today - 4.years) && birth_date < (Date.today- 0.years)
      quiz_level = 1
    else
      quiz_level = 1
    end
    member.update_attribute(:quiz_level,quiz_level)
  end
  
  def update_child_member_country_code(current_user,options={})
    begin
      member = self
      country_code = User.where(member_id:member.family_mother_father.first.to_s).first.country_code rescue "other"
      member.update_attribute(:country_code,country_code)   
    rescue Exception => e
      Rails.logger.info "Error inside update_child_member_country_code ..... #{e.message}"
    end
  end

  def run_add_child_setup_contoller(api_version,current_user,options={})
    data = []
    member = self
    member.update_child_member_country_code(current_user,options)
    # Update activity status and last activity done
    member.update_attribute(:activity_status_v2, "Active") 
    member.update_attribute(:last_activity_done_on, member.created_at) 
    
    activate_tools(api_version,current_user,options)
    family_id =  FamilyMember.where(member_id:member.id).last.family_id
    if api_version > 4
      data = member_tool_setup(current_user,api_version,options)
    end
  
    if member.is_expected?
      member.update_attribute(:pregnancy_status,true)

      report_event = Report::Event.where(name:"pregnancy added to family").last
      # Send an email after adding pregnancy
      pregnancy = Pregnancy.where(expected_member_id:member.id).last
      InstantNotification::Pregnancy.notification_for_identifier_27(27,current_user,member.id,options)
      #identifier 33 message: "Let's get ready for the next stage of your journey!"
      BatchNotification::GeneralNotification.assign_batch_notification_for_identifier_33(33,{:pregnancy_setup_flow=>true,:pregnancy_ids=>[pregnancy.id]})
    else
      member.update_attribute(:pregnancy_status,false)

      report_event = Report::Event.where(name:"member added to family").last
      # Send an email after adding child member
      InstantNotification::Member.notification_for_identifier_29(29,current_user,member.id,options)
    end
    ::Report::ApiLog.log_event(current_user.user_id,family_id,report_event.custom_id,options.merge({:member_id=>member.id})) rescue nil
    data
  end
  # child.member_tool_setup(current_user,api_version,options)
  def member_tool_setup(current_user,api_version,options={})

    member = self
    child = member
    member_type =  child.is_expected? ? "pregnancy" : "child"
    options = options.merge({:api_version=>api_version})
    tool_data_to_save = {}
    activated_tools_controller = {}

    tool_summary_screen = {}
    child_name = options[:added_children_name_list].to_sentence rescue member.first_name
    activated_tools = {"title"=> "I have activated a few basic tools for #{child_name}",
       "title_texts_to_bold"=> ["#{child_name}"," activated"], "subtitle"=>"We can activate other tools from Manage Tools, with changing growth needs of #{member_type}"}
    tools = []
    user_data = {}
    multiple_child_added =  options[:multiple_child_added] || false
    begin
      # tool_ids = MemberNutrient.where(member_id:child.id).pluck(:nutrient_id)
      tools = Nutrient.get_member_tools(member,current_user,api_version).where(:identifier.ne=>"Manage Tools")
      other_tools = []
      # tools = Nutrient.where(:id.in=>tool_ids).where(:identifier.ne=>"Manage Tools")
      # if member_type == "pregnancy"
      #   other_tools = Nutrient.for_pregnancy(api_version,current_user,options).where(:identifier.nin=>(tools.map(&:identifier)+ ["Manage Tools"] ) )
      # else
      #    other_tools = Nutrient.for_born_child(api_version,current_user,options).where(:identifier.nin=>tools.map(&:identifier) + ["Manage Tools"] )
      # end
      child_country = options[:child_country] || child.get_country_code
      child_country = child_country == "other" ? "UK" : child_country.upcase
      child_country = child_country == "AE" ? "UAE" : child_country.upcase
      activated_tools_screens = []
      tools_data = []
      tools.each do |tool|
        begin
          tools_data << tool.get_json(options)
          tool_identifier = tool.identifier.downcase
          puts tool_identifier
          
          handhold_screen = "hh_#{tool_identifier}"
          
          if  member_type == 'pregnancy'
            pregnancy = Pregnancy.where(:expected_member_id=>child.id).last if member_type == 'pregnancy'
            tool_identifier = 'add_timeline' if tool_identifier == "timeline" 
            handhold_screen = tool_identifier
            user_data[handhold_screen] = {:data=>HandHold.get_handhold_data_for_action(current_user,pregnancy,tool_identifier,options)}
          else
            user_data[handhold_screen] = {:data=>HandHold.get_handhold_data_for_action(current_user,child,tool_identifier,options)}
          end
          summary_screen_text_for_tool = HandHold.save_data(current_user,child,handhold_screen,user_data,member_type,options)
          if tool_identifier == "Documents" && member_type != 'pregnancy'
            summary_screen_text_for_tool = nil
          end
          
          if summary_screen_text_for_tool.present?
            tool_summary_screen[handhold_screen] = summary_screen_text_for_tool
            options = options.merge({:multiple_child_added=>multiple_child_added,:child_country=>child_country}) 
            # if handhold_screen == "hh_child health card"
            #   other_tools  << child.activate_controller_response_formatted(current_user,child,tool,user_data[handhold_screen],tool_summary_screen[handhold_screen],options)
            # else
              activated_tools_screens  << child.activate_controller_response_formatted(current_user,child,tool,user_data[handhold_screen],tool_summary_screen[handhold_screen],options)
            # end
          else
            other_tools << child.activate_controller_response_formatted(current_user,child,tool,{},{},options)
          end
        rescue Exception => e
           Rails.logger.info "Error inside member_tool_setup for handhold_screen : #{handhold_screen} ----  #{e.message}"
           Rails.logger.info "..........member id #{child.id}.............."
        end
      end
      if options[:activated_tools_controller] == true
        if member_type == 'pregnancy'
          sortedlist =  ["Pregnancy Health Card","Prenatal Tests","Kick Counter","Documents"]
          other_tools_sort_order = Nutrient.for_pregnancy(api_version,current_user).order_by("position asc").pluck("identifier") - sortedlist - ["Manage Tools"]
        else
          sortedlist =  ["Immunisations", "Measurements", "Milestones", "Tooth Chart", "Vision Health"]
          other_tools_sort_order = Nutrient.for_born_child(api_version,current_user).order_by("position asc").pluck("identifier") - sortedlist - ["Manage Tools"]
        end
        activated_tools_screens_data = activated_tools_screens
        activated_tools_screens = activated_tools_screens.select{|a| sortedlist.include?(a[:tools_identifier])}
        activated_tools_screens = activated_tools_screens.sort_by{|a| sortedlist.index(a[:tools_identifier])}
        activated_tools_screens_data.each do |a|
          if !sortedlist.include?(a[:tools_identifier])
            other_tools << a 
          end 
        end
        other_tools = other_tools.sort_by{|a| other_tools_sort_order.index(a[:tools_identifier])}
      else
        activated_tools_screens = activated_tools_screens.select{|a| ["Timeline","Milestones","Immunisations","Prenatal Tests","Checkup Planner","Tooth Chart"].include?(a[:tools_identifier]) }
      end

      activated_tools_controller = {
        :summary_title=> "I have updated information relevant in #{child_country} to get you started quickly",
        :summary_title_texts_to_bold=> [" #{child_country}"],
        :summary_subtitle=> "Don't worry, you can review and make specific changes later easily",
        :activated_tools_screens => activated_tools_screens,
        :other_tools => other_tools
      }
      activated_tools[:tools] = tools_data
      data = [activated_tools,activated_tools_controller]
       
      
    rescue Exception=> e 
      Rails.logger.info "Error inside member_tool_setup........#{e.message}"
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside member_tool_setup")
    end
    data
  end

  def activate_controller_response_formatted(current_user,child,tool,user_data,summary_data,options={})
    tool_name =  tool.title
    child_name = child.first_name
    if options[:multiple_child_added].to_s == "true"
      # child_name = options[:added_children_name_list].to_sentence rescue child.first_name
      summary_title = "#{tool_name} for #{child_name}"
    else
      summary_title = tool_name
    end
    
    child_country = options[:child_country] || child.get_country_code
    child_country = child_country == "other" ? "UK" : child_country.upcase
    
    data = []
    if summary_data[:data_key].present?
      data_list = user_data[:data][summary_data[:data_key].to_sym]
    else
      data_list = user_data[:data]
    end
     
    activated_tools_screens_data = {
      :tools=> tool.title,
      :tools_identifier=> tool.identifier,
      :title=> "#{tool.title} for #{child.first_name}",
      :titleTextsToBold=> [tool_name,child_name],
      :subtitle1 => (summary_data[:subtitle1] %{:child_country=>child_country} rescue nil),
      :subtitle1TextsToBold => ( (summary_data[:subtitle1TextsToBold]) rescue []),
      :subtitle2=> summary_data[:subtitle2],
      :data_list=>  summary_data[:data_list],
      :summary_text=> summary_data[:summary_text],
      :total_added_text=>  summary_data[:total_added_text],
      :status_text=> summary_data[:status_text],
      :summary_title=>  summary_title
    }
    data = activated_tools_screens_data
    data
  end
  
  def activate_tools(api_version,current_user,options={})
    add_default_nutrients(api_version,current_user)
  end

  def add_default_nutrients(api_version,current_user,options={})
    api_version = api_version.present? ? api_version : 1
    active_status =  "active"
    member = self
    # Update family elder stage status
    options = {:case=>"member_added",:add_default_nutrient=> true}.merge(options)
  
    family_id = FamilyMember.where(member_id:member.id).pluck(:family_id).last
    family_ids = [family_id]
    member.delay.update_familyuser_stage_on_add_or_delete_member(family_ids, options)
    platform = current_user.device_platform
    if api_version >=4
      subscription = Subscription.add_default_subscription(current_user,family_id,platform,api_version)  
    end
    # Add basic/premium type for api_version >= 5, and only basic for api version less than 5 
    unless is_expected?
      member.save_quiz_level(current_user,options)
      # member.update_attribute(:quiz_level, 1 ) #if member.quiz_level.blank?
      tools = Nutrient.for_born_child(api_version,current_user,options)
      if api_version <=4
        tools =  tools.where(tool_category:Nutrient::ToolType[:basic])
      end
      tools.each do |nutrient|
        MemberNutrient.new(status:active_status, member_id: member.id, nutrient_id: nutrient.id, position: nutrient.position).save
        subscription.update_subscription_tool_count(nutrient,family_id,"activated") rescue nil
      end
    else

      tools = Nutrient.for_pregnancy(api_version,current_user,options)
      if api_version <=4
        tools =  tools.where(tool_category:Nutrient::ToolType[:basic])
      end
      tools.each do |nutrient|
        MemberNutrient.new(status:active_status, member_id: member.id, nutrient_id: nutrient.id, position: nutrient.position).save
        subscription.update_subscription_tool_count(nutrient,family_id,"activated") rescue nil
      end
    end
    begin
      manage_tool = Nutrient.where(:identifier=>"Manage Tools").last 
      manage_tool_added = MemberNutrient.where(nutrient_id:manage_tool.id,member_id:member.id).last
      if !manage_tool_added
        MemberNutrient.new(status:active_status, member_id: member.id, nutrient_id: manage_tool.id, position: manage_tool.position).save
      end
    rescue Exception => e 
      Rails.logger.info "Error in add default tool"
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside add_default_nutrients")
      Rails.logger.info e.message
    end
  end
  
  def age_in_months
    child = self
    age = ApplicationController.helpers.calculate_age(child.birth_date)
    Milestone.convert_to_months(age) rescue 0
  end

  def age_format_for_alexa(is_child=false)
    if is_child == true
      child = self
      age = ApplicationController.helpers.calculate_age(child.birth_date)
      age_data = age.split("and").map(&:strip)
      return age_data[0]
    end
  end

  def validate_to_send_email
    self.birth_date.month == Date.today.month && !is_expected?
  end

end