  class Emis::Client
    
    def self.get_link_details_for_session(current_user,member_id,options={})
      clinic_link_detail = ::Clinic::LinkDetail.get_clinic_link_detail(member_id,current_user,options)
      user_details = clinic_link_detail.link_info.with_indifferent_access rescue nil
    end

    def self.get_status(api_response_status,options={})
      api_response_status == StatusCode::Status403 ? StatusCode::Status403: 100
    end

    def self.get_session_detail(member_id,current_user,options={})
      ::Clinic::UserClinicSession.where(member_id:member_id, parent_member_id:current_user.id).last
    end

    def self.clear_member_session(current_user,member_id,options={})
      ::Clinic::UserClinicSession.where(member_id:member_id,parent_member_id:current_user.id).delete_all
    end

    def self.clear_session_account_detail(member_id,current_user,options={})
      ::Emis::Client.get_session_detail(member_id,current_user,options).delete rescue nil
      ::Clinic::LinkDetail.get_clinic_link_detail(member_id,current_user,options).delete rescue nil
    end

    def self.get_clinic_detail_by_postcode(current_user,postcode,ods_code,options={})
      begin
        member_id = current_user.id rescue nil
        request_options = {}
        request_data = options[:request_data] || {}
        request_body = {} 
        request_header = {}
        request_options[:end_point] = "https://pim.emishealth.com/pfs"
        if Rails.env != "production"
          ::Emis::Client.clear_member_session(current_user,member_id,options)
        end
        request_options.merge!({:request_type=>"Get", :request_uri=> "/practices?Postcode=#{postcode}", :request_body=>request_body,:request_header=>request_header})
        response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)
        data = {}
        if status == 200
          if ods_code.present?
            practice = response["Practices"].select{|a| a["NationalPracticeCode"] == ods_code}
          else
             practice = response["Practices"]
          end
          emis_practice_status = practice.present?
          data =  {practice_list:practice, practice_status:emis_practice_status, status:200}
        else
          data =  {message: ::Message::Clinic[:gp_not_found_nearby],practice_status:false, status:100}
        end
        @retry_count = 0
      rescue Exception=>e 
        ::Emis::Client.clear_member_session(current_user,member_id,options)
        @retry_count = @retry_count.to_i + 1 
        retry if @retry_count.to_i <= 2
        data = {message: ::Message::Clinic[:gp_not_found_nearby],practice_status:false, status:100}
      end
      data

    end

    def self.get_session_id_and_link_token(current_user,member_id,options={})
      begin
        user_details = ::Emis::Client.get_link_details_for_session(current_user,member_id,options)
        user_clinic_session = ::Emis::Client.get_session_detail(member_id,current_user,options)
        if user_clinic_session.blank? || user_clinic_session.link_token.blank? || user_clinic_session.session_id.blank?
          # create new session
          session_response = ::Emis::Login.get_session_id(current_user,member_id,user_details, options)
          user_clinic_session = ::Emis::Client.get_session_detail(member_id,current_user,options)
        end
        link_token = user_clinic_session.link_token
        session_id = user_clinic_session.session_id
        [session_id,link_token]
      rescue Exception=>e
        GpsocLogger.info  '.....Error inside get_session_id_and_link_token .....'
	      GpsocLogger.info e.message
        raise 'Unable to get session details' 
      end
    end

    def self.make_request(current_user,member_id,request_options,options={})
      begin
        data = {}
        if current_user.clinic_account_status != "Active"
         data = { "Message"=> ::Message::Clinic[:clinic_access_blocked], status:403}
         return [data,403]
        end
        if current_user.is_gpsoc_api_request_limit_exceed?(options)
          data = { "Message"=> ::Message::Clinic[:clinic_access_limit_exceeded], status:403}
          return [data,403]
        end
        status = 200
        pinDocument = ::Emis::Client.get_link_details_for_session(current_user,member_id,options)
        # Set application session before request
        begin
          user_session = ::Emis::Client.get_session_detail(member_id,current_user,options)
          if user_session.blank? || user_session.session_key.blank?
            end_user_session_id = ::Emis::Login.end_user_session(current_user,member_id,request_options)
          else
            end_user_session_id = user_session.session_key
            raise 'No Appliation session id' if end_user_session_id.blank?
          end
        rescue Exception => e 
          ::Emis::Client.clear_member_session(current_user,member_id,options)
          retry
        end
        message_id = SecureRandom.uuid
        header = {'MessageId'=>message_id, 'X-API-EndUserSessionId'=> end_user_session_id, 'Content-Type'=> 'application/json','X-API-Version'=>'2.0.0.0','X-API-ApplicationId'=>APP_CONFIG["emis_application_id"]}
        header.merge!(request_options[:request_header]) if request_options[:request_header].present?
        GpsocLogger.info  '...................Request..Action ..................'
        GpsocLogger.info  "..................#{request_options[:request_uri].to_s}.................."
        GpsocLogger.info  '...................Request...Headers values...................'
        GpsocLogger.info  header.inspect
        puts '.....................Request.Headers values...................'
        puts header.inspect
        end_point_url =  request_options[:end_point] || APP_CONFIG['emis_v2_end_point']
        uri = URI.parse("#{end_point_url}" + request_options[:request_uri].to_s)

        http = Net::HTTP.new(uri.host, uri.port)
        if uri.scheme == 'https'
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_PEER
        end
        
        http.cert = OpenSSL::X509::Certificate.new(File.read("#{Rails.root}/../emis/certificate/client.cer"))
        http.key = OpenSSL::PKey::RSA.new(File.read("#{Rails.root}/../emis/certificate/client.key"))
        
        request_body = request_options[:request_body]
        GpsocLogger.info '......................Request Body...................'
        if options[:log_request] != false
          GpsocLogger.info  request_body.inspect
          puts '......................Request Body...................'
        end

        if request_options[:request_type] == "Get"
          uri.query = URI.encode_www_form(request_body) if request_body.present?
          request = Net::HTTP::Get.new(uri.request_uri, header)
        elsif request_options[:request_type] == "Delete"
          uri.query = URI.encode_www_form(request_body) if request_body.present?
          request = Net::HTTP::Delete.new(uri.request_uri, header)
          request.body = request_body.to_json if request_body.present?
        elsif request_options[:request_type] == "Put"
          # uri.query = URI.encode_www_form(request_body) if request_body.present?
          request = Net::HTTP::Put.new(uri.request_uri, header)
          request.body = request_body.to_json if request_body.present?
        else 
          request = Net::HTTP::Post.new(uri.request_uri, header)
          request.body = request_body.to_json if request_body.present?
        end
        response = http.request(request)
        GpsocLogger.info  '......................Response...................'
        if !(response.code.to_i >= 200 && response.code.to_i < 300) 
          GpsocLogger.info  response.body.inspect rescue nil
        end
        GpsocLogger.info '......................Response Code...................'
        GpsocLogger.info  response.code rescue nil
        if response.code.to_i >= 200 && response.code.to_i < 300 
          data =  JSON.parse(response.body) rescue response.body
          status = 200
          @makeRequest_retry_count = 0
        elsif (response.body == "Missing or invalid EndUserSessionId" || response.body == '"Missing or invalid EndUserSessionId"' rescue false)
          @makeRequest_retry_count = 0
          raise 'Invalid EndUserSessionId'
        else
          data =  JSON.parse(response.body) rescue response.body
          status = response.code.to_i
          if (data["Message"].include?("User Session not found. It may have expired") rescue false)
            @makeRequest_retry_count =  2
            ::Emis::Client.clear_member_session(current_user,member_id,options)
            @user_session_required = true
            raise "User Session not found. It may have expired."
          elsif (data["Message"].include?("Unable to find organisation for NationalPracticeCode") rescue false)
            status = 100
          elsif status == 500
            status = 100
          elsif status == 403
            # status = 100
            if request_options[:request_uri].to_s.include?("message")
              data["Message"] = "Sorry, your GP hasn't given access to - view messages. Kindly contact your GP to get access"
            elsif  request_options[:request_uri].to_s.include?("appointment") 
              data["Message"] = "Sorry, your GP hasn't given access to - GP appointments. Kindly contact your GP to get access"
            elsif  request_options[:request_uri].to_s.include?("courses") || request_options[:request_uri].to_s.include?("prescription")
              data["Message"] = "Sorry, your GP hasn't given access to - view prescriptions. Kindly contact your GP to get access"
            else
              data["Message"] = data["Message"].blank? ? Message::Emis[:permission_denied] : data["Message"]
            end
          else
            raise 'retry request'  if @makeRequest_retry_count.to_i < 2
          end 
        end
        
      rescue Exception=>e 
          GpsocLogger.info "................ Error description......................."
          GpsocLogger.info "................ #{e.message}......................."
        if @makeRequest_retry_count.to_i < 2
          ::Emis::Client.clear_member_session(current_user,member_id,options)
          @makeRequest_retry_count = @makeRequest_retry_count.to_i  + 1
          GpsocLogger.info "................Retrying Emis session........."
          puts "............................Retrying emis session........."
          retry 
        end  
        status = 100
        UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,request_options,"Error inside emis make_request : #{options[:request_uri]} ")
        raise e.message if @user_session_required == true
      end
      # log event 
      action =  request_options[:request_uri].to_s
      request_status = status == 200 ? "true" : "false"
      message = data["Message"]
      request_data = {message:message, :request_type=>request_options[:request_type],:request_status=> request_status, :action=> action, :patient_id=>(pinDocument["account_id"] rescue nil),:message_id=>message_id}    
    
        current_user.log_emis_request(member_id,request_data,options)
      
      [data,status]
    end

  end