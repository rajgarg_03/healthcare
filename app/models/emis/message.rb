class Emis::Message
  
  def self.formatted_massage(message,options={})
    data = message
    data["MessageId"] = message["MessageId"].to_s
    data["details_available"] = false

    data
  end
  # Emis::Message.get_messages(organization_uid,member_id,options)
  def self.get_messages(organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      request_options = {}
      request_data = options[:request_data] || {}
      

      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken': link_token}.merge(request_data) 
      request_header = {'X-API-SessionId'=>session_id}
      request_options.merge!({:request_type=>"Get", :request_uri=> "/messages", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)
       
      
      if status == 200
        # message list
        messages = response["Messages"]
        messages = messages.sort_by{|message| message[:MessageId]}.reverse #rescue messages
        read_message = messages.select{|message| message["HasUnreadReplies"] == false}.map{|message| ::Emis::Message.formatted_massage(message)}
        unread_message = messages.select{|message| message["HasUnreadReplies"] == true}.map{|message| ::Emis::Message.formatted_massage(message)}
        read_message  = nil if  read_message.blank?
        unread_message  = nil if  unread_message.blank?
        return {status:200,  messages:{:read_message=>read_message,:unread_message=>unread_message }}
      else
        status = ::Emis::Client.get_status(status,options)
        error_msg = response["Message"]
        return {status:status,message:response["Message"], error:error_msg}
      end
    rescue Exception=>e 
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @getMessage_retry_count = @getMessage_retry_count.to_i + 1 
      retry if @getMessage_retry_count.to_i <= 2
      return {status:100, error:e.message, message:Message::Emis[:Messages][:unable_to_get_message]}
    end
  end
  # Emis::Message.get_message_detail(message_id, organization_uid,member_id,options)
  def self.get_message_detail(message_id,organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      request_options = {}
      request_data = options[:request_data] || {}
      

      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken': link_token}.merge(request_data) 
      request_header = {'X-API-SessionId'=>session_id}
      request_options.merge!({:request_type=>"Get", :request_uri=> "/messages/#{message_id}", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)
      if  status == 200
        message = ::Emis::Message.formatted_massage(response["Message"])
        @GetMessageDetail_retry_count = 0
        return {message_detail:message, status:200}
      else
        status = ::Emis::Client.get_status(status,options)
        return {message:response["Message"],error:response["Message"], status:status}
      end
    rescue Exception=>e 
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @GetMessageDetail_retry_count = @GetMessageDetail_retry_count.to_i + 1 
      retry if @GetMessageDetail_retry_count.to_i <= 2
      return {message:Message::Emis[:Messages][:unable_to_get_message_detail],error:e.message, status:100}
    end
  end

   # Emis::Message.delete_message(message_id, organization_uid,member_id,options)
  def self.delete_message(message_id,organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      request_options = {}
      request_data = options[:request_data] || {}
      

      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken': link_token}.merge(request_data) 
      request_header = {'X-API-SessionId'=>session_id}
      request_options.merge!({:request_type=>"Delete", :request_uri=> "/messages/#{message_id}", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)
     
      if (status != 200)
        status = ::Emis::Client.get_status(status,options)
        return {error:response["Message"],message:response["Message"], status:status}
      else
        @retry_count = 0
        return {:message=>Message::Emis[:Messages][:message_deleted] , status:200}
      end
    rescue Exception=>e 
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      return {error:e.message, :message=>Message::Emis[:Messages][:unable_to_delete_message], status:100}
    end
  end

  def self.get_message_receiptent_list(current_user,member_id,options={})
    begin
      current_user = options[:current_user]
      request_options = {}
      request_data = options[:request_data] || {}
      

      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken': link_token}.merge(request_data) 
      request_header = {'X-API-SessionId'=>session_id}
      request_options.merge!({:request_type=>"Get", :request_uri=> "/messagerecipients", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)
      if (status != 200)
        status = ::Emis::Client.get_status(status,options)
        return {message:response["Message"] ,error:response["Message"], status:status}
      else
        recipients = response["MessageRecipients"].group_by{|a| a["RecipientGuid"]}
        usual_gp_recipients = response["MessageRecipients"].select{|c| c["RecipientType"] == "UsualGP"}.group_by{|a| a["RecipientGuid"]}
        recipients = recipients.merge(usual_gp_recipients).values.flatten
        @retry_count = 0
        return {:recipients=>recipients, status:200}
      end
    rescue Exception=>e 
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      return {error: e.message, message:Message::Emis[:Messages][:unbale_to_get_message_receiptent_list], status:100}
    end
  end
  
  def self.update_message_read_status(message_id,message_read_state,member_id,options={})
    begin
      current_user = options[:current_user]
      request_options = {}
      request_data = options[:request_data] || {}
      
      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken': link_token,
                       "MessageId"=>message_id,
                      "MessageReadState"=>message_read_state}.merge(request_data) 
      request_header = {'X-API-SessionId'=>session_id}
      request_options.merge!({:request_type=>"Put", :request_uri=> "/messages", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)
     
     if (status != 200)
        status = ::Emis::Client.get_status(status,options)
        return {message:response["Message"], error:response["Message"], status:status}
      else
        @retry_count = 0
        return {:message=>Message::Emis[:Messages][:message_read_status_updated], status:200}
      end
    rescue Exception=>e 
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      return {:message=>response["Message"], error:e.message, status:100}
    end
  end

  # options ={:current_user=>current_user,:recipients=>["10xn342cuywekdsu09"]}
  def self.send_message(subject,msg,organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      request_options = {}
      request_data = options[:request_data] || {}
      
      recipients = options[:recipients]
      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken': link_token,
                      "Subject"=>subject,
                      "MessageBody"=>msg,
                      "Recipients"=> recipients}.merge(request_data) 
      request_header = {'X-API-SessionId'=>session_id}
      request_options.merge!({:request_type=>"Post", :request_uri=> "/messages", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)
     
     if (status != 200)
        status = ::Emis::Client.get_status(status,options)
        return {:message=>response["Message"] , error:response["Message"], status:status}
      else
        @retry_count = 0
        return {:message=>Message::Emis[:Messages][:message_sent], status:200}
      end
    rescue Exception=>e 
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      return {:message=>Message::Emis[:Messages][:message_not_sent],error:e.message, status:100}
    end
  end

end