class Emis::Immunisations
  # Get Patient Immunisations
  # None, Allergies, Consultations, Documents, Immunisations, Medication, Problems, TestResults

  def self.get_patient_immunisations(member_id,options={})
    begin
      current_user = options[:current_user]
      request_options = {}
      request_data = options[:request_data] || {}
      immunisation_records = []
      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken': link_token,"ItemType"=>"Immunisations"}.merge(request_data) 
      request_header = {'X-API-SessionId'=>session_id}
      request_options.merge!({:request_type=>"Get", :request_uri=> "/record", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)
      if status == 200
        # response =  [{"MedicalRecord"=>{"PatientGuid"=>"5a09eb8f-7e0b-4630-ad48-87e8e05d124f", "Title"=>"Master", "Forenames"=>"Mike", "Surname"=>"Singh", "Sex"=>"Male", "DateOfBirth"=>"2016-09-22T00:00:00", "Allergies"=>nil, "Consultations"=>nil, "Documents"=>nil, "Immunisations"=>[{"ObservationType"=>"Immunisation", "Episodicity"=>"None", "NumericValue"=>nil, "NumericOperator"=>nil, "NumericUnits"=>nil, "DisplayValue"=>nil, "TextValue"=>nil, "Range"=>nil, "Abnormal"=>false, "AbnormalReason"=>nil, "AssociatedText"=>nil, "EventGuid"=>"9fe87ee5-4e7e-46e3-8b88-86b4e9ca84bd", "Term"=>"Influenza vaccination", "AvailabilityDateTime"=>"2020-01-30T06:39:56.043", "EffectiveDate"=>{"DatePart"=>"YearMonthDay", "Value"=>"2020-01-30T00:00:00"}, "CodeId"=>142934010, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}, {"ObservationType"=>"Immunisation", "Episodicity"=>"None", "NumericValue"=>nil, "NumericOperator"=>nil, "NumericUnits"=>nil, "DisplayValue"=>nil, "TextValue"=>nil, "Range"=>nil, "Abnormal"=>false, "AbnormalReason"=>nil, "AssociatedText"=>nil, "EventGuid"=>"d9e4d77b-d8c3-4fc9-a0dd-db5a76d3f372", "Term"=>"1st hepatitis A vaccination", "AvailabilityDateTime"=>"2020-01-30T06:41:06.473", "EffectiveDate"=>{"DatePart"=>"YearMonthDay", "Value"=>"2020-01-30T00:00:00"}, "CodeId"=>264177017, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}, {"ObservationType"=>"Immunisation", "Episodicity"=>"None", "NumericValue"=>nil, "NumericOperator"=>nil, "NumericUnits"=>nil, "DisplayValue"=>nil, "TextValue"=>nil, "Range"=>nil, "Abnormal"=>false, "AbnormalReason"=>nil, "AssociatedText"=>nil, "EventGuid"=>"32e9cbf1-a55f-4e61-91fc-ee1446bb53e6", "Term"=>"Immunisation given", "AvailabilityDateTime"=>"2020-01-30T06:18:40.763", "EffectiveDate"=>{"DatePart"=>"YearMonthDay", "Value"=>"2020-01-30T00:00:00"}, "CodeId"=>62701000000116, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}], "Medication"=>nil, "Problems"=>nil, "TestResults"=>nil, "Users"=>[{"UserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "Title"=>"Dr", "Forenames"=>"Nurturey", "Surname"=>"Test", "Role"=>"General Medical Practitioner", "Organisation"=>"Nurt services"}]}, "FilterDetails"=>{"ItemFilter"=>"Immunisations", "ItemFilterFromDate"=>nil, "ItemFilterToDate"=>nil, "FreeTextFilterFromDate"=>nil}}, 200]
        observation_data = []
        observation_data = response["MedicalRecord"]["Immunisations"].flatten.compact.select{|observation| observation["Term"].present? }
        immunisation_records = Emis::Immunisations.get_formatted_immunisation_data(member_id, observation_data,options)
        #immunisation_records = {"2020-01-30T00:00:00"=>[{:code_id=>846639, member_id=>nil, :name=>"Influenza vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 30 Jan 20"}, {:code_id=>8466339, :member_id=>nil, :name=>"1st hepatitis A vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 30 Jan 20"}], "2020-02-02T00:00:00"=>[{:member_id=>nil, :name=>"First typhoid vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 02 Feb 20"}, {:member_id=>nil, :name=>"1st hepatitis B vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 02 Feb 20"}], "2020-02-02T08:09:00"=>[{:member_id=>nil, :name=>"First diphtheria vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 02 Feb 20"}]}
        message = nil
      else
        # status = 100
        status = ::Emis::Client.get_status(status,options)
        message = response["Message"]
        raise (response["Message"] || 'Error inside get_patient_immunisations') if (@retry_count.to_i < 2 && status != StatusCode::Status403)
      end    
    rescue Exception=>e 
      puts '@retry_count.to_i is'
      puts @retry_count.to_i
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      status = 100
      @retry_count = 0
      message = ::Message::Emis[:HealthCard][:unable_to_list] %{:item_type=>"Immunisations"}
      @error = e.message
    end
    {data:immunisation_records,:status=>status,:message=>message,:error=>@error}
  end

   def self.get_formatted_immunisation_data(member_id,records,options={})
    data = {}
    group_by_date = records.group_by{|a| a["EffectiveDate"]["Value"].to_s}
    group_by_date.each do |date,records|
      record_data = []
      records.each do |record|
        record_date = date.to_date.to_s

        record_data << ({:record_uid=>record["EventGuid"], :record_date=>date.to_date, :code_id=>record["CodeId"] ,:member_id=>member_id,:name=>record["Term"], :value=> record["NumericValue"],:data_source=>"emis",:added_by=>"system",:updated_at=>"Administered on #{date.to_date.to_date7}" })
      end
      data[date] = record_data
    end
    data
  end
 
  
end
 