class Emis::Allergy
  # Get Patient Allergy
  # None, Allergies, Consultations, Documents, Immunisations, Medication, Problems, TestResults
  def self.get_patient_allergies(member_id,options={})
    begin
      current_user = options[:current_user]
      request_options = {}
      allergy_records = []
      request_data = options[:request_data] || {}

      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken': link_token,"ItemType"=>"Allergies"}.merge(request_data) 
      request_header = {'X-API-SessionId'=>session_id}
      request_options.merge!({:request_type=>"Get", :request_uri=> "/record", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)
      # [{"MedicalRecord"=>{"PatientGuid"=>"5a09eb8f-7e0b-4630-ad48-87e8e05d124f", "Title"=>"Master", "Forenames"=>"Mike", "Surname"=>"Singh", "Sex"=>"Male", "DateOfBirth"=>"2016-09-22T00:00:00", "Allergies"=>[{"ObservationType"=>"Allergy", "Episodicity"=>"First", "NumericValue"=>nil, "NumericOperator"=>nil, "NumericUnits"=>nil, "DisplayValue"=>nil, "TextValue"=>nil, "Range"=>nil, "Abnormal"=>false, "AbnormalReason"=>nil, "AssociatedText"=>nil, "EventGuid"=>"0d41cfe3-fca8-461d-9332-773d36a192e7", "Term"=>"Egg allergy", "AvailabilityDateTime"=>"2020-01-30T06:16:29.357", "EffectiveDate"=>{"DatePart"=>"YearMonthDay", "Value"=>"2020-01-30T00:00:00"}, "CodeId"=>637141000006110, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}], "Consultations"=>nil, "Documents"=>nil, "Immunisations"=>nil, "Medication"=>nil, "Problems"=>nil, "TestResults"=>nil, "Users"=>[{"UserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "Title"=>"Dr", "Forenames"=>"Nurturey", "Surname"=>"Test", "Role"=>"General Medical Practitioner", "Organisation"=>"Nurt services"}]}, "FilterDetails"=>{"ItemFilter"=>"Allergies", "ItemFilterFromDate"=>nil, "ItemFilterToDate"=>nil, "FreeTextFilterFromDate"=>nil}}, 200]
      if status == 200
        observation_data = []
        observation_data = response["MedicalRecord"]["Allergies"].flatten.compact.select{|observation| observation["Term"].present? }
        allergy_records = Emis::Allergy.get_formatted_allergies_data(member_id, observation_data,options)
        message = nil
      else
        status = ::Emis::Client.get_status(status,options)
        if status == 403
          message = "Sorry, your GP hasn't given access to - Allergies. Kindly contact your GP to get access"
        else
          raise 'Error inside get_patient_allergies' 
        end
      end    
    rescue Exception=>e 
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      status = 100
      message  = ::Message::Emis[:HealthCard][:unable_to_list] %{:item_type=>"Allergy"}
      @retry_count = 0
      @error = e.message
    end
    {data:allergy_records,:status=>status,:message=>message,:error=>@error}
  end

   def self.get_formatted_allergies_data(member_id,records,options={})
    data = {}
    group_by_date = records.group_by{|a| a["EffectiveDate"]["Value"].to_s}
    group_by_date.each do |date,records|
      record_data = []
      records.each do |record|
        record_date = date.to_date.to_s
        record_data << ({:record_uid=>record["EventGuid"], record_date:date.to_date ,:member_id=>member_id,:name=>record["Term"], :value=> record["NumericValue"],:data_source=>"emis",:added_by=>"system",:updated_at=>date.to_date.to_date7 })
      end
      data[date] = record_data
    end
    data
  end

  
end
 