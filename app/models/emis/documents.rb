class Emis::Documents
  
  # Emis::Document.get_document_content(member_id,document_id,options)
  def self.get_document_content(member_id,document_id,options={})
    begin
      current_user = options[:current_user]
      
      request_options = {}
      request_data = options[:request_data] || {}
      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken': link_token}.merge(request_data) 
      request_header = {'X-API-SessionId'=>session_id}
      request_options.merge!({:request_type=>"Get", :request_uri=> "/documents/#{document_id}", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)
      data = {}
      if status == 200
        doc_data =  response["CompressedEncodedDocumentContent"]
        document_data = ActiveSupport::Gzip.decompress(Base64.decode64(doc_data))
        data =  {documents:document_data, status:200}
      else
        api_status = ::Emis::Client.get_status(status,options)
        data =  {message:response["Message"], status:api_status}
      end
      @retry_count = 0
    rescue Exception=>e 
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      data = {message:response["Message"], status:100}
    end
    data
  end

  # Emis::Document.list_document(member_id,options)

  def self.list_document(member_id,options={})
    begin
      current_user = options[:current_user]
      request_options = {}
      request_data = options[:request_data] || {}

      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken': link_token,"ItemType"=>"Documents"}.merge(request_data) 
      request_header = {'X-API-SessionId'=>session_id}
      request_options.merge!({:request_type=>"Get", :request_uri=> "/record", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)
      if status == 200
        observation_data = []
        Document.where(member_id:member_id,data_source:"emis").delete_all

        document_data = response["MedicalRecord"]["Documents"]
        formated_data = []
        document_data.each do | doc|
          if doc["Available"] == true
            document_guid = doc["DocumentGuid"]
            name = "Document-" + document_guid.split("-").last
            last_modified = doc["Observation"]["AvailabilityDateTime"]
            file_name = name + "." + doc["Extension"]
            Document.new(created_at:last_modified, last_modified:last_modified, file_updated_at:last_modified, file_file_name: file_name,file_content_type:  doc["Extension"],file_file_size:doc["Size"],  uid:document_guid,member_id:member_id,
                         data_source:"emis", name:name,document_type:doc["Observation"]["Term"]).save
            formated_data << {:record_uid=>document_guid,:record_date=>(last_modified.to_date rescue nil), :member_id=>member_id,:name=>name, :value=> name,:data_source=>"emis",:added_by=>"system",:updated_at=>(last_modified.to_date.to_date7 rescue nil) }
          end
        end
        @retry_count = 0
        {data:formated_data, status:200}
      else
        @retry_count = 0
        api_status = ::Emis::Client.get_status(status,options)
        {message:response["Message"], status:api_status}
      end
    rescue Exception=>e 
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      return {:error=>e.message, message:response["Message"], status:100}
    end
  end


end