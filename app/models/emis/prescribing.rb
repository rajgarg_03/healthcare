class Emis::Prescribing
  # Emis::Prescribing.get_medication_courses(organization_uid,member_id,options)
  def self.get_medication_courses(organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      request_options = {}
      request_data = options[:request_data] || {}
      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken': link_token}.merge(request_data) 
      request_header = {'X-API-SessionId'=>session_id}
      request_options.merge!({:request_type=>"Get", :request_uri=> "/courses", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)

      if (status != 200 )
        status = ::Emis::Client.get_status(status,options)
        return {message:response["Message"],error:response["Message"], status:status}
      else
        courses = response["Courses"].sort_by{|a| a["MostRecentIssueDate"]}.reverse rescue response["Courses"] 
        return {courses: courses, status:200}
        @retry_count = 0
      end
    rescue Exception=>e 
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
  
      return {message:Message::Emis[:Prescribing][:unable_to_list_prescription], error:e.message, status:100}
    end
  end
  
  # medication_course_ids = [{id=>"9ce0766e-b51f-4f68-8426-841e178af08c",type=>""},{:id=>"9ce0766e-b51f-4f68-8426-841e178af08c",:type=>""}]
  #Emis::Prescribing.request_prescription(medication_course_ids,organization_uid,member_id,options)
  def self.request_prescription(medication_course_ids,organization_uid,member_id,options={})
    begin
      # Fetch ids only
      medication_course_ids = medication_course_ids.map{|a| a["id"]}
      current_user = options[:current_user]
      request_options = {}
      request_data = options[:request_data] || {}
      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken': link_token,
        "MedicationCourseGuids"=>medication_course_ids}.merge(request_data) 
      request_header = {'X-API-SessionId'=>session_id}
      request_options.merge!({:request_type=>"Post", :request_uri=> "/prescriptionrequests", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)

     if (status != 200 )
        status = ::Emis::Client.get_status(status,options)
        return {message:response["Message"], clinic_type:"emis", error:response["Message"], status:status}
      else
        if response["Message"] == "Prescription request comment is not allowed by the practice"
          @retry_count = 0
          options[:request_data].delete(:RequestComment)
          raise response["Message"]
        end

        request_guid = response["RequestedGuid"] rescue nil
        return {request_guid:request_guid, message:Message::Emis[:Prescribing][:prescription_requested], status:200, clinic_type:"emis"}
        @retry_count = 0
      end
    rescue Exception=>e 
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      return {message:Message::Emis[:Prescribing][:prescription_not_requested], clinic_type:"emis", error:e.message, status:100}
    end
  end

  # Emis::Prescribing.get_prescription_requests((Date.today - 1.month),organization_uid,member_id,options)
  def self.get_prescription_requests(from_date, organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      
      request_options = {}
      request_data = options[:request_data] || {}

      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken': link_token}.merge(request_data) 
      request_header = {'X-API-SessionId'=>session_id}
      request_options.merge!({:request_type=>"Get", :request_uri=> "/prescriptionrequests", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)
      if (status != 200)
        status = ::Emis::Client.get_status(status,options)
        return {requested_prescription_list:[],:message=> response["Message"], status:status}
      else
        medication_course_list = response["MedicationCourses"].group_by{|a| a["MedicationCourseGuid"] }
        prescription_list = response["PrescriptionRequests"]
        # 
        data = []
        prescription_list.each do |prescription|
          requested_medication_courses = {}
          requested_medication_courses = prescription["RequestedMedicationCourses"]
         
          requested_medication_courses_data = []
          [requested_medication_courses].flatten.each do |req_medi_course|
            medication_course_details =  medication_course_list[req_medi_course["RequestedMedicationCourseGuid"]][0] || {}
            requested_medication_courses_data << req_medi_course.merge(medication_course_details)
          end
          # requested_medication_courses.merge!(medication_course_details)
          prescription_data = prescription
          prescription_data[:date_requested] = prescription["DateRequested"].to_date
          prescription_data[:medication_course_ids] = requested_medication_courses_data.map{|a| a["RequestedMedicationCourseGuid"]}
          prescription_data[:requested_medication_courses] =  requested_medication_courses_data
          data << prescription_data 
        end
        @retry_count = 0
        return {requested_prescription_list:data,:clinic_type=>"emis", status:200}
      
      end
    rescue Exception=>e 
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      return {message:Message::Emis[:Prescribing][:unable_to_list_requested_prescription] ,:error=>e.message, requested_prescription_list:data, status:100}
    end
  end


  # medication_course_ids = ["b2ab7f85-b1ef-4ec3-9ecc-33eb690482e1","9ce0766e-b51f-4f68-8426-841e178af08c"]
  #Emis::Prescribing.cancel_prescription(medication_course_ids,organization_uid,member_id,options)
  def self.cancel_prescription(request_id,organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      request_options = {}
      request_data = options[:request_data] || {}
      cancellation_reason = options[:cancellation_reason]
      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken': link_token,
                      "CancellationReason"=>cancellation_reason,
                     "PrescriptionRequestGuid"=>request_id}.merge(request_data) 
      request_header = {'X-API-SessionId'=>session_id}
      request_options.merge!({:request_type=>"Delete", :request_uri=> "/prescriptionrequests", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)

      if (status != 200 )
        status = ::Emis::Client.get_status(status,options)
        return {clinic_type:"emis",message:response["Message"], error:response["Message"], status:status}
      else
        return {clinic_type:"emis",message:Message::Emis[:Prescribing][:prescription_request_cancelled], status:200}
        @retry_count = 0
      end
    rescue Exception=>e 
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      return {clinic_type:"emis",message:Message::Emis[:Prescribing][:prescription_request_not_cancelled],:error=>e.message, status:100}
    end
  end

  
   

end