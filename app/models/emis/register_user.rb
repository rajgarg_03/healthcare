class Emis::RegisterUser
  def self.format_emis_proxy_user(user,clinic_name,options={})
    data = nil
    current_user = options[:current_user]
    user_link_detail = Clinic::LinkDetail.where(member_id:current_user.id).last.link_info

    active_status =  user["Age"].to_i <= GlobalSettings::EmisRegistrationAllowedAgeLimit && user["AssociationType"].downcase != "self" # 13 years less then 3 years anot self assocciated 
    if user["AssociationType"].downcase == "proxy"
      link_detail = ::Clinic::LinkDetail.where(parent_member_id:current_user.id,patient_uid: user["PatientActivityContextGuid"]).last
      
      if link_detail.present? && FamilyMember.where(member_id:link_detail.member_id).last.present?
        linked = true
        linked_member_id = link_detail.member_id
        dob = link_detail.user_info["date_of_birth"].to_date.to_date6.to_s rescue nil
        gender =  link_detail.user_info["gender"].to_s
        nhs_number = link_detail.user_info["nhs_number"]
      elsif options[:fetch_date_of_birth] == false
        dob = nil
        gender = nil
      else
        user_details = {"access_identity_guid"=>user_link_detail["access_identity_guid"],:practice_code=>user_link_detail["practice_ods_code"],:patient_context_guid=>user["PatientActivityContextGuid"]}
        # user_details = {"access_identity_guid"=>user_link_detail["access_identity_guid"],:practice_code=>user_link_detail["practice_ods_code"],:patient_context_guid=>user_link_detail["patient_context_guid"]}
        user_detail_data,status =  Emis::Login.getPatientInfo(current_user,nil,user_details,options)
        linked = false
        linked_member_id = nil
        dob = user_detail_data[:date_of_birth].to_date.to_date6.to_s rescue nil
        gender =  user_detail_data[:gender].to_s
        nhs_number = user_details[:nhs_number]
      end
      age = ApplicationController.helpers.calculate_age4(dob) rescue ( user["Age"].to_s + " years")
      data  = {:full_name=> "#{user['Forenames']} #{user['Surname']}",
       :first_name =>user['Forenames'],
       :nhs_number => nhs_number,
       :last_name =>user['Surname'],
       :dob=> (dob),
       :age=> age,
       :age_value=> user["Age"].to_i,
       :gender => gender,
       :proxy_active_status=>active_status,
       :clinic_name=>clinic_name,
       :ods_code=>user["NationalPracticeCode"],
       :patient_uid=> user["PatientActivityContextGuid"],
       :association_type=>user["AssociationType"],
       :proxy_linked=>linked,
       :proxy_linked_member_id=>linked_member_id
      }
    end
    data
  end

  def self.get_proxyUser(organization_uid,member_id,options={})
    begin
      data = []
      current_user = options[:current_user]
      # Registered with pin / without pin
      clinic_user_access_level = ::Clinic::User.get_clinic_user(member_id,current_user,options).access_level rescue 3
      return nil if clinic_user_access_level > 1
      
      clinic_name = current_user.get_clinic_detail(current_user,options).name rescue ""
      user_details = ::Emis::Client.get_link_details_for_session(current_user,member_id,{})
      @get_proxyUser_retry_count = @get_proxyUser_retry_count.to_i  
       
      
      request_body = {
        "AccessIdentityGuid": user_details[:access_identity_guid],
        "NationalPracticeCode": (user_details[:practice_code] || user_details[:practice_ods_code])
      }
      request_options = ({:request_type=>"Post", :request_uri=> "/sessions", :request_body=>request_body})
      
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)
      # puts response.inspect
      if status == 200
        if response["ApplicationLinkLevel"] == "Linked"
          response["UserPatientLinks"].each do |user|
            data << ::Emis::RegisterUser.format_emis_proxy_user(user,clinic_name,options)
          end
          data.compact!
          if data.blank?
            options[:ip_address] = current_user.gpsoc_api_requested_ip
            report_event_custom_id = 254 #Report::Event.where(name:"No proxy list found for linked account").last
            ::Report::ApiLog.log_event(current_user.user_id,options[:family_id],report_event_custom_id,options) rescue nil
          end
        else
          Rails.logger.info "Account is not Linked with GP. ........................."
        end
      end
     
    rescue Exception=>e 
      #puts e.message
      Rails.logger.info "Error insdie ....#{e.message}"
    end
    # puts "Data is #{data.inspect}"
    data
  end

 
  
  # registeration_detail = {:title=>'Mr',:first_name=>"HHHH",:surname=>'Garg',:date_of_birth=> "1991-01-01", :gender=>"Male", :house_name_no=>"10 Rillington Place",:street=>"Smart Street",:town=>"ontario", :postcode=>"BR5 1QB", :practice_ods_code=>"A28826", :telephone_number=>"",:mobile_number=>"", :email=>"hh@ff.com", :village=>"",:country=>"" }.with_indifferent_access
  def self.register_with_patient_detail(registeration_detail, options={})
    begin
      pinDocument = registeration_detail.with_indifferent_access
      current_user = options[:current_user]
      options[:ip_address] = current_user.gpsoc_api_requested_ip
      member_id = options[:member_id]
      pinDocument[:date_of_birth] = pinDocument[:date_of_birth].to_date.strftime("%Y-%m-%d") rescue pinDocument[:date_of_birth]

      # registeration_detail = {:sex=>"NotKnown", :postcode=>"", :title=>"Mr", :surname=>"Garg", :first_name=>"Raj",  :date_of_birth=> "1991-01-01", :village=>"", :practice_ods_code=>"A28826", :telephone_number=>"",:mobile_number=>"918826936913", :email=>"ronit.garg@gmail.com",:house_name_no=>"10 Rillington Place",:street=>"Smart Street", :village=>"" }.with_indifferent_access
      request_body = 
      {
        "NationalPracticeCode": pinDocument["practice_ods_code"],
        "RegistrationDetails": {
          "ContactDetails": {
            "TelephoneNumber": pinDocument["telephone_number"],
            "MobileNumber": pinDocument["mobile_number"],
            "EmailAddress": pinDocument["email"]
          },
          "Address": {
            "HouseNameFlatNumber": pinDocument["house_name_no"],
            "NumberStreet": pinDocument["street"],
            "Village": pinDocument["village"],
            "Town": pinDocument["town"],
            "County": pinDocument["country"],
            "Postcode": pinDocument["postcode"]
          }
        },
        "SearchCriteria": {
          "AssociationType": "None",
          "DateOfBirth": pinDocument["date_of_birth"].to_time,
          "FirstName": pinDocument["first_name"],
          "Postcode": pinDocument["postcode"],
          "Sex": pinDocument["gender"],
          "Surname": pinDocument["surname"],
          "Title": pinDocument["title"]
        }
      }

      request_options = {:request_type=>"Post", :request_uri=> "/users", :request_body=>request_body}
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options, options.merge({:log_request=>false}))
      if status != 200
       report_event_custom_id = 266 #Clinic link with user detail (GP online user linking without linkage key)
       ::Report::ApiLog.log_event(current_user.user_id,options[:family_id],report_event_custom_id,options.merge({:status=>"failed"})) rescue nil
      end

      if status == 200
         report_event_custom_id = 266 #Clinic link with user detail
        ::Report::ApiLog.log_event(current_user.user_id,options[:family_id],report_event_custom_id,options.merge({:status=>"success"})) rescue nil
    
        response_data = {:access_identity_guid=>response["AccessIdentityGuid"]}
        {:data=>response_data,:message=>nil,:status=>200}.with_indifferent_access
      elsif status == 409
        link_detail = ::Clinic::DelinkedLinkDetail.where(email:current_user.email).last
        if link_detail.present? && (current_user.user.is_internal? || current_user.uer.is_beta_user?)
          response_data = {:access_identity_guid=>link_detail.link_info["access_identity_guid"]}
          {:data=>response_data,:message=>nil,:status=>200}.with_indifferent_access
        else
          report_event_custom_id = 260 #Report::Event.where(name:"GP Account already exist for register with detail").last
          ::Report::ApiLog.log_event(current_user.user_id,options[:family_id],report_event_custom_id,options.merge({:status=>"failed"})) rescue nil
          data = {:message=>Message::Emis[:RegisterUser][:exisitng_account], :status=>100}
        end
      elsif status== 404
        report_event_custom_id = 261 #Report::Event.where(name:'GP Account not found for register with detail').last
        ::Report::ApiLog.log_event(current_user.user_id,options[:family_id],report_event_custom_id,options.merge({:status=>"failed"})) rescue nil
        data = {:message=>Message::Emis[:RegisterUser][:no_record_found], :status=>100}
      elsif status == 400
        data = {:message=>Message::Emis[:RegisterUser][:missing_or_invalid_data], :status=>100}
      else
        report_event_custom_id = 262 #Report::Event.where(name:'GP Account register with detail failed').last
        ::Report::ApiLog.log_event(current_user.user_id,options[:family_id],report_event_custom_id,options.merge({:status=>"failed"})) rescue nil
    
        message = response["Message"] || Message::Emis[:RegisterUser][:register_with_pin_document_failed]
       
        data = {:message=>message,:status=>100}.with_indifferent_access
      end
    rescue Exception=>e 
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside get_session_id")
    end
  end
  
  # pinDocument = {"account_id"=>"2689028826","account_linkage_key"=>"tJ32RCgdUAAm1","practice_ods_code"=>"A28826","surname"=>"Garg","date_of_birth"=>"1995-11-30"}
  def self.register_with_pin_document(pinDocument,options={})
    begin
      return {:error=>'Invalid pin document',:status=>100} if pinDocument.blank?
      member_id = options[:member_id]
      current_user = options[:current_user]
      pinDocument = pinDocument.with_indifferent_access
      pinDocument[:date_of_birth] = pinDocument[:date_of_birth].to_date.strftime("%Y-%m-%d") rescue pinDocument[:date_of_birth]
      options[:ip_address] = current_user.gpsoc_api_requested_ip
      request_body = {
        "Surname": pinDocument[:surname],
        "DateOfBirth": pinDocument[:date_of_birth].to_time,
        "LinkageDetails": {
          "AccountId":pinDocument[:account_id],
          "LinkageKey": pinDocument[:account_linkage_key],
          "NationalPracticeCode": pinDocument[:practice_ods_code]
        }
      }
      request_options = {}
      request_options.merge!({:request_type=>"Post", :request_uri=> "/me/applications", :request_body=>request_body})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options, options.merge({:log_request=>false}) )
      # Mock for testing
      #response,status = [{"AccessIdentityGuid"=>"5358df2b-4659-48ea-9630-99303dc54ebe"}, 200] 
      if status == 200
        report_event_custom_id = 265 #GP online user linking with linkage key
        ::Report::ApiLog.log_event(current_user.user_id,options[:family_id],report_event_custom_id,options.merge({:status=>"success"})) rescue nil

        data = {:data=> {:access_identity_guid=>response["AccessIdentityGuid"]},:status=>200}
        # data = {:data=>data,:message=>nil,:status=>200}.with_indifferent_access
        # {:data=>data,:error=>nil,:status=>200}.with_indifferent_access
      else
        link_detail = ::Clinic::DelinkedLinkDetail.where(email:current_user.email).last
        if link_detail.present? && (current_user.user.is_internal? || current_user.user.is_beta_user?)
          report_event_custom_id = 265 #Clinic link with pin document
          ::Report::ApiLog.log_event(current_user.user_id,options[:family_id],report_event_custom_id,options.merge({:status=>"success"})) rescue nil

          response_data = {:access_identity_guid=>link_detail.link_info["access_identity_guid"]}
          data = {:data=> response_data,:status=>200}
        else
          if response["LinkageDetails.AccountId"].present?
            #  report_event_custom_id = 256 #Report::Event.where(name:"GP Account already exist for a pin dcoument").last
            # ::Report::ApiLog.log_event(current_user.user_id,options[:family_id],report_event_custom_id,options.merge({:status=>"failed"})) rescue nil
       
            message = [response["LinkageDetails.AccountId"]].flatten.first
          elsif response["LinkageDetails.LinkageKey"].present?
            #  report_event_custom_id = 256 #Report::Event.where(name:"GP Account already exist for a pin dcoument").last
            # ::Report::ApiLog.log_event(current_user.user_id,options[:family_id],report_event_custom_id,options.merge({:status=>"failed"})) rescue nil
       
            message = [response["LinkageDetails.LinkageKey"]].flatten.first
          else
            message  = response["Message"] || ::Message::Emis[:RegisterUser][:missing_or_invalid_data]
          end
           report_event_custom_id = 265 #Clinic link with pin document
           ::Report::ApiLog.log_event(current_user.user_id,options[:family_id],report_event_custom_id,options.merge({:status=>"failed"})) rescue nil

          data = {:message=>message,:status=>100}.with_indifferent_access
        end
      end
    rescue Exception =>e 
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside EMIS::register_with_pin_document : #{options[:request_uri]} ")
      data  = {:error=>e.message,:message=>Message::Emis[:RegisterUser][:register_with_pin_document_failed] , :status=>100}.with_indifferent_access
    end
    data
  end


  # Emis::Regiteruser.reset_linkage_key(member_id,options)
  def self.reset_linkage_key(member_id,options={})
    begin
      current_user = options[:current_user]
      options[:ip_address] = current_user.gpsoc_api_requested_ip
      member_id = current_user.id
      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {}
      request_header = {'X-API-SessionId'=>session_id}
      request_options = ({:request_type=>"Delete", :request_uri=> "/me/linkagedocument", :request_body=>request_body,:request_header=>request_header})
      response,status = [{},200]#::Emis::Client.make_request(current_user,member_id,request_options,options)
      data = {}
      if status == 200
        report_event_custom_id = 259 #Report::Event.where(name:'GP Account delink').last
        ::Report::ApiLog.log_event(current_user.user_id,options[:family_id],report_event_custom_id,options.merge({:status=>"success"})) rescue nil
        ::Clinic::DelinkedLinkDetail.where(email:current_user.email).delete_all rescue nil
        data =  {message:'Account delinked', status:200}
      else
        report_event_custom_id = 259 #Report::Event.where(name:'GP Account delink').last
        ::Report::ApiLog.log_event(current_user.user_id,options[:family_id],report_event_custom_id,options.merge({:status=>"failed"})) rescue nil
        data =  {message:response["Message"], status:100}
      end
      @reset_linkage_key_retry_count = 0
    rescue Exception=>e 
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @reset_linkage_key_retry_count = @reset_linkage_key_retry_count.to_i + 1 
      retry if @reset_linkage_key_retry_count.to_i <= 2
      Rails.logger.info "Error inside .....Emis::Login.reset_linkage_key ....#{e.message}"

      data = {message:response["Message"], status:100}
    end
    data
  end
    
end