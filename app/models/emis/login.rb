class Emis::Login

   
  def get_clinic_linkable_status(user_details,options={})
    true
  end

  # v2

  # Api 2.0.0.0
   def self.authenticateUser(member_id,user_details,options={})
    GpsocLogger.info '........authenticating user...........'
    options[:member_id] = member_id
    member = Member.find(member_id)
    current_user = options[:current_user]
    if options[:proxy_user_link_flow] != true
      ::Emis::Client.clear_member_session(current_user,member_id,options)
    end
    pin_document = user_details.with_indifferent_access
    # No need to register user in Proxy User Linking
    if options[:proxy_user_link_flow] == true
      GpsocLogger.info '........Proxy flow activated...........'

      # user_clink_link_detail = ::Clinic::LinkDetail.get_clinic_link_detail(member_id,current_user,options)
      response = {:data=> {:access_identity_guid=>user_details["access_identity_guid"]},:status=>200}
      GpsocLogger.info "........Proxy flow activated response #{response.inspect}..........."
    
    else
      if options[:register_with_pindocument] == true
        # pinDocument = {"account_id"=>"2325028826","account_linkage_key"=>"TcdbmCgdUANr7","practice_ods_code"=>"A28826","surname"=>"","date_of_birth"=>"1993-04-08"}
        response = Emis::RegisterUser.register_with_pin_document(pin_document,options)
      else
        # user_details = {:title=>'Mr',:first_name=>"HHHH",:surname=>'Garg',:date_of_birth=> "1991-01-01", :gender=>"Male", :house_name_no=>"10 Rillington Place",:street=>"Smart Street",:town=>"ontario", :postcode=>"BR5 1QB", :practice_ods_code=>"A28826", :telephone_number=>"",:mobile_number=>"", :email=>"hh@ff.com", :village=>"",:country=>"" }.with_indifferent_access
       response = Emis::RegisterUser.register_with_patient_detail(user_details,options)
      end
    end
    response = response.with_indifferent_access
    return response if response[:status] == 100
    # Clean user exiting details
    ::Emis::Client.clear_session_account_detail(member_id,current_user,options)
    GpsocLogger.info "........Cleaned up existing detail for member..........."
     
    is_child_member = member.user_id.blank?

    user_detail_data = {}
    user_details = response[:data]
    user_details[:practice_code] =   pin_document[:practice_ods_code]
    user_details[:patient_context_guid] = pin_document[:patient_context_guid]
    member.update_attribute(:ods_code,user_details[:practice_code])
    if member.class.to_s == 'ParentMember'
      GpsocLogger.info "........Updating User personal details..........."
      member.birth_date = pin_document[:date_of_birth]
      user_last_name = pin_document[:surname]
      user_first_name = pin_document[:first_name]
      # Don't update member first name
      # member.first_name = user_first_name
      # member.last_name = user_last_name
      # user = member.user
      # user.update_attributes({last_name:user_last_name, first_name:user_first_name})
      # member.save
    end
    if options[:is_proxy_selected] == true
     # data = {"access_identity_guid"=>"5358df2b-4659-48ea-9630-99303dc54ebe",:practice_code=>'',:patient_context_guid=>''}
      GpsocLogger.info "........Skiping validation. for proxy user........."
      user_detail_data,status =  Emis::Login.getPatientInfo(current_user,member_id,user_details,options)
     
      if status == 200 && user_detail_data[:date_of_birth].to_s != member.birth_date.to_s 
        GpsocLogger.info "........Birthdate check failed..........."
        status = 100 
        response =  {:message=>Message::Clinic[:birth_date_does_not_matched],:status=>100}.with_indifferent_access
      end
    elsif member.is_child? && member.age_in_months <= (13*12) && user_detail_data[:date_of_birth].to_s == member.birth_date.to_s # 13 years
      GpsocLogger.info "........Checking Age for child..........."
      # user_details = response[:data]
      user_detail_data,status =  Emis::Login.getPatientInfo(current_user,member_id,user_details,options)
    elsif !member.is_child? && member.age_in_months > (16*12)
      # user_details = response[:data]
      user_detail_data,status = Emis::Login.getPatientInfo(current_user,member_id,user_details,options)
    else
        GpsocLogger.info "........age validation failed..........."
      response =  {:message=>Message::Clinic[:age_criteria_failed_to_registration],:status=>100}.with_indifferent_access
    end    

    if status == 200  && options[:is_proxy_selected] != true
      member_name = (pin_document[:first_name] || member.first_name).to_s.downcase
      # No first name check for now
      # if((user_detail_data[:first_name].downcase != member_name) rescue true)
      #   GpsocLogger.info "........Name does not match..........."
      #   response =  {:message=>::Message::Clinic[:first_name_not_matched],:status=>100}.with_indifferent_access
      # end
    end
    # Log proxy user added event
    if options[:is_proxy_selected] == true
      report_event_custom_id = 267 ##Link child via proxy access
      status_text = status == 200 ? "success" : "failed"
      ::Report::ApiLog.log_event(current_user.user_id,options[:family_id],report_event_custom_id,options.merge({:status=>status_text})) rescue nil
    end

    user_detail_data[:proxy_user_status] = options[:proxy_user_link_flow]
    response[:data].merge!(user_detail_data) rescue nil
    response
  end

  # user_details = {:access_identity_guid=>'',:practice_code=>'A28826'}
  def self.getPatientInfo(current_user,member_id,user_details,options={})
    begin
      GpsocLogger.info "........Getting member info from EMIS..........."
      GpsocLogger.info "........details for session ..........."

      data = {}
      patient_context_guid = nil
      user_clinic_session = ::Emis::Client.get_session_detail(member_id,current_user,options)
      # if ((current_user.id == member_id && (user_clinic_session.blank? || user_clinic_session.session_id.blank?) ) || current_user.id != member_id
      if user_clinic_session.present? && member_id.nil?
        user_clinic_session.delete rescue nil
        user_clinic_session = nil
      end
      if user_clinic_session.blank? || user_clinic_session.session_id.blank?
        GpsocLogger.info "......No Existing session..Creating new session..........."
        response = ::Emis::Login.get_session_id(current_user,member_id,user_details,options)
        if response[:status] != 200
           GpsocLogger.info  "......... #{response.inspect}. ............."
          if (response[:message].include?('Unable to find organisation for NationalPracticeCode') rescue false)
            data = {:message=>response[:message]}
            return [data,100]
          else
            raise 'Invalid session'
          end
        end
        patient_context_guid = response[:patient_context_guid]
        user_clinic_session = ::Emis::Client.get_session_detail(member_id,current_user,options)
      end
      user_session_id = user_clinic_session.session_id
      link_token = user_clinic_session.link_token
      current_user = options[:current_user]
      user_details = user_details.with_indifferent_access
      request_options = {} 
      request_body = {'UserPatientLinkToken': link_token} 
      request_options.merge!({:request_type=>"Get", :request_uri=> "/demographics",:request_body=>request_body,  :request_header=>{"X-API-SessionId"=>user_session_id} })
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options, options.merge({:log_request=>false}))
   
      # {:title=>"Mr", :first_names=>"Ronit", :surname=>"Garg", :calling_name=>"Emis", :patient_identifiers=>{:value=>nil, :type=>"NHS Number"}, :date_of_birth=>Wed, 22 Nov 2000, :contact_details=>{:email_address=>nil, :mobile_number=>nil, :telephone_number=>nil, :address=>{:house_name_flat_number=>"123", :number_street=>"Dcf", :village=>nil, :town=>"Brq 4Hw", :county=>nil, :postcode=>nil}, :status=>"Current"}, :gender=>"M"}
      address_detail = response["Address"]   
      contact_details = response["ContactDetails"] ||{}
      data = {
        :first_name=>response["FirstName"],
        :last_name=> response["Surname"],
        :nhs_number => (response["PatientIdentifiers"][0]["IdentifierValue"] rescue nil),
        :patient_identifier => ( nil),
        :date_of_birth=> (response["DateOfBirth"].to_date rescue nil),
        :mobile_number => contact_details["MobileNumber"],
        :email => (contact_details["EmailAddress"] rescue nil),
        :address=> address_detail ,
        :gender=> (response["Sex"])
      }
      data[:patient_context_guid] = patient_context_guid if patient_context_guid.present?
      GpsocLogger.info  "..................Formated response for Patient info............................................."
      #GpsocLogger.info  "..................#{data.inspect}............................................."
      status = 200
    rescue Exception=> e 
      @getPatientInfo_retry_count = @getPatientInfo_retry_count.to_i + 1
      GpsocLogger.info  "..............................................................."
      GpsocLogger.info  "getPatientInfo: error msg is :#{e.message}"
      puts "...........getPatientInfo..Retry count --  #{@getPatientInfo_retry_count.to_i}"
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      retry if @getPatientInfo_retry_count.to_i <= 2
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside getPatienDetail")
      data = {:error=>e.message}
      status = 100
    end
    @getPatientInfo_retry_count = 0
    [data,status]
  end



  def self.delink_with_clinic(member_id,options={})
    current_user = options[:current_user]
    im1_delinking_enable_status = ::System::Settings.im1DelinkingEnabled?(current_user,options) rescue  true
    report_event_custom_id = 259 #Report::Event.where(name:"GP Account delink").last
    options[:ip_address] = current_user.gpsoc_api_requested_ip
    if im1_delinking_enable_status
      ::Report::ApiLog.log_event(current_user.user_id,options[:family_id],report_event_custom_id,options.merge({:status=>"success"})) rescue nil
      ::Emis::RegisterUser.reset_linkage_key(member_id,options)
    else
      ::Report::ApiLog.log_event(current_user.user_id,options[:family_id],report_event_custom_id,options.merge({:status=>"failed"})) rescue nil
      data = {message:'Account delinked', status:200}
      data
    end
  end

  # Emis::Login.available_services(member_id,options)
  def self.available_services(member_id,options={})
    begin
      current_user = options[:current_user]
      request_data = options[:request_data] || {}
      options[:ip_address] = current_user.gpsoc_api_requested_ip

      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken': link_token}.merge(request_data) 
     
      request_header = {'X-API-SessionId'=>session_id}
      request_options = ({:request_type=>"Get", :request_uri=> "/me/settings", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)
      data = {}
      if status == 200
        if response["ApplicationLinkLevel"] == "Restricted"
          begin
            if current_user.id.to_s == member_id.to_s
              user_clink_link_detail = ::Clinic::LinkDetail.get_clinic_link_detail(member_id,current_user,options)
              user_clink_link_detail.link_info["register_with_pindocument"] = "false"
              user_clink_link_detail.save
              user_clinic = ::Clinic::User.get_clinic_user(member_id,current_user,options={})
              user_clinic.access_level = 2
              user_clinic.save  
            end
          rescue Exception=>e 
            GpsocLogger.info 'Unable to update access level for Restricted account'
            GpsocLogger.info e.message
          end
        end
        services = response["EffectiveServices"]
        if services["MedicalRecord"].blank?
          report_event_custom_id = 263 #Report::Event.where(name:'HealthReport not enabled').last
          ::Report::ApiLog.log_event(current_user.user_id,options[:family_id],report_event_custom_id,options.merge({:status=>"success"})) rescue nil
          services["MedicalRecord"] = {"ImmunisationsEnabled"=>false,"AllergiesEnabled"=>false,"ConsultationsEnabled"=>false,"ProblemsEnabled"=>false,"MedicationEnabled"=>false,"TestResultsEnabled"=>false,"DocumentsEnabled"=>false}
        end
        # {"AppointmentsEnabled"=>true, "DemographicsUpdateEnabled"=>true, "PracticePatientCommunicationEnabled"=>true, "PrescribingEnabled"=>true, "EpsEnabled"=>true, "OnlineTriageEnabled"=>true, "MedicalRecordEnabled"=>true, "RecordViewAuditEnabled"=>true, "RecordSharingEnabled"=>true, "MedicalRecord"=>{"RecordAccessScheme"=>"CoreSummaryCareRecord", "AllergiesEnabled"=>true, "ConsultationsEnabled"=>false, "ImmunisationsEnabled"=>false, "DocumentsEnabled"=>false, "MedicationEnabled"=>true, "ProblemsEnabled"=>false, "TestResultsEnabled"=>false}}
        service_data =  {appointment_booking:services["AppointmentsEnabled"],
         repeat_prescription:services["PrescribingEnabled"],
         view_health_records:services["MedicalRecordEnabled"],
         manage_messages: services["PracticePatientCommunicationEnabled"],
         immunisations:(services["MedicalRecord"]["ImmunisationsEnabled"] rescue false),
         health_card:{
          Immunisations:  services["MedicalRecord"]["ImmunisationsEnabled"],
          Allergy:        services["MedicalRecord"]["AllergiesEnabled"],
          Consultations:  services["MedicalRecord"]["ConsultationsEnabled"] ,
          Problems:       services["MedicalRecord"]["ProblemsEnabled"],
          Medication:     services["MedicalRecord"]["MedicationEnabled"],
          Investigations: services["MedicalRecord"]["TestResultsEnabled"],
          Documents:      services["MedicalRecord"]["DocumentsEnabled"]
         }
       }
        if !service_data.values.include?(true)
          user_clink_link_detail = ::Clinic::LinkDetail.get_clinic_link_detail(member_id,current_user,options)
          if (user_clink_link_detail.link_info["register_with_pindocument"].to_s == "true" rescue false)
            report_event_custom_id = 255 #Report::Event.where(name:'No GP service enabled for fully linked with pin document').last
            ::Report::ApiLog.log_event(current_user.user_id,options[:family_id],report_event_custom_id,options.merge({:status=>"success"})) rescue nil
          end
        end

        data =  {services:service_data, status:200}
      else
        data =  {message:response["Message"], status:100}
      end

      @available_services_retry_count = 0
    rescue Exception=>e 
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @available_services_retry_count = @available_services_retry_count.to_i + 1 
      retry if @available_services_retry_count.to_i <= 2
      Rails.logger.info "Error inside .....Emis::Login.available_services ....#{e.message}"
      data = {message:response["Message"], status:100}
    end
    
    data
  end

  #  Not in use for now
  # Emis::Login.clinic_services(member_id,options)
  def self.clinic_services(member_id,options={})
    begin
      current_user = options[:current_user]
      request_data = options[:request_data] || {}
      user_clink_link_detail = ::Clinic::LinkDetail.get_clinic_link_detail(current_user.id,current_user,options)
      ods_code = user_clink_link_detail.user_info["practice_code"] || user_clink_link_detail.link_info["practice_ods_code"]
      request_body = {}
      request_header = {}
      request_options = ({:request_type=>"Get", :request_uri=> "/practices/#{ods_code}/settings", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)
      data = {}
      if status == 200
        services = response["AssignedServices"]
        # {"AppointmentsEnabled"=>true, "DemographicsUpdateEnabled"=>true, "PracticePatientCommunicationEnabled"=>true, "PrescribingEnabled"=>true, "EpsEnabled"=>true, "OnlineTriageEnabled"=>true, "MedicalRecordEnabled"=>true, "RecordViewAuditEnabled"=>true, "RecordSharingEnabled"=>true, "MedicalRecord"=>{"RecordAccessScheme"=>"CoreSummaryCareRecord", "AllergiesEnabled"=>true, "ConsultationsEnabled"=>false, "ImmunisationsEnabled"=>false, "DocumentsEnabled"=>false, "MedicationEnabled"=>true, "ProblemsEnabled"=>false, "TestResultsEnabled"=>false}}
        service_data =  {appointment_booking:services["AppointmentsEnabled"],
         repeat_prescription:services["PrescribingEnabled"],
         view_medical_records:services["MedicalRecordEnabled"],
         manage_messages: services["PracticePatientCommunicationEnabled"]}
        data =  {services:service_data, status:200}
      else
        data =  {message:response["Message"], status:100}
      end
      @available_services_retry_count = 0
    rescue Exception=>e 
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @available_services_retry_count = @available_services_retry_count.to_i + 1 
      retry if @available_services_retry_count.to_i <= 2
      data = {message:response["Message"], status:100}
    end
    data
  end
  


  def self.get_session_id(current_user,member_id,user_details,options={})
    begin
      @get_session_id_retry_count = @get_session_id_retry_count.to_i  
      begin
        user_details = user_details.with_indifferent_access
      rescue Exception=>e 
        raise "No account detail found to authenticate"
      end
      current_user = options[:current_user]
      request_options = {}
      request_body = {
        "AccessIdentityGuid": user_details[:access_identity_guid],
        "NationalPracticeCode": (user_details[:practice_code] || user_details[:practice_ods_code])
      }
      if user_details[:access_identity_guid].blank?
        data = {:message=>"Invalid session detail",:status=>100}.with_indifferent_access
        return
      end
      request_options.merge!({:request_type=>"Post", :request_uri=> "/sessions", :request_body=>request_body})
      
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options.merge({:log_request=>false}))
       
      if status == 200
        session_id = response["SessionId"]
        # if response["ApplicationLinkLevel"] == "Linked"
          # To map patient Token
          user_clink_link_detail = ::Clinic::LinkDetail.get_clinic_link_detail(current_user.id,current_user,options)
          if user_details[:patient_context_guid].blank? && user_clink_link_detail.blank?
            response_data = response["UserPatientLinks"].select{|a| a["AssociationType"].downcase == "self"}.first || {}
          else
            response_data = response["UserPatientLinks"].select{|a| a["PatientActivityContextGuid"] == user_details[:patient_context_guid] }.first || {}
          end

          token = response_data["UserPatientLinkToken"] rescue nil
          patient_context_guid = response_data["PatientActivityContextGuid"] rescue nil
          
          user_clinic_session =  ::Emis::Client.get_session_detail(member_id,current_user,options)
          if user_clinic_session.present?
            user_clinic_session.session_id = session_id
            user_clinic_session.link_token = token
            user_clinic_session.save
          end  
          data = {:session_id=>session_id,:token=>token,patient_context_guid:patient_context_guid,  :user_info=>response, :status=>200}.with_indifferent_access
        # else
        #   # Issue
        #   data = {:message=>"Account not linked.Contact ot GP",:status=>100}.with_indifferent_access
        # end
      else
        data = {:message=>response["Message"],:status=>100}.with_indifferent_access
      end
    rescue Exception => e 
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside emis get_session_id : #{options[:request_uri]} ")
      @get_session_id_retry_count = @get_session_id_retry_count.to_i + 1
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      retry if @get_session_id_retry_count <= 2 
      data  = {:error=>e.message,:message=>Message::Emis[:Login][:unable_to_get_session], :status=>100}.with_indifferent_access
    end
    @get_session_id_retry_count = 0
    data
  end


  # Get application session ID
  def self.end_user_session(current_user,member_id,options={})
    begin
      app_session_id = nil
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      header = {'Content-Type'=> 'application/json','X-API-Version'=>'2.0.0.0','X-API-ApplicationId'=>APP_CONFIG["emis_application_id"]}
      end_point_url = options[:end_point] || APP_CONFIG['emis_v2_end_point']
      uri = URI.parse("#{end_point_url}/sessions/endusersession")
      http = Net::HTTP.new(uri.host, uri.port)
      if uri.scheme == 'https'
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_PEER
      end
      http.cert = OpenSSL::X509::Certificate.new(File.read("#{Rails.root}/../emis/certificate/client.cer"))
      http.key = OpenSSL::PKey::RSA.new(File.read("#{Rails.root}/../emis/certificate/client.key"))
      request = Net::HTTP::Post.new(uri.request_uri, header)
      response = http.request(request)
      if response.code == "201"
        app_session_id = JSON.parse(response.body)["EndUserSessionId"]
        user_clinic_session = ::Clinic::UserClinicSession.new(parent_member_id:current_user.id, session_key:app_session_id,member_id:member_id)
        user_clinic_session.save
      end
    rescue Exception=>e 
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside emis login end_user_session")
    end
    app_session_id
  end

end

