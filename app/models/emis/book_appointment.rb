class Emis::BookAppointment
  
  def self.get_doctor_list(organization_uid,member_id,options)
    begin
      current_user = options[:current_user]
      request_options = {}
      doctor_list = []
      request_data = options[:request_data] || {}

      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken': link_token}.merge(request_data) 
      request_header = {'X-API-SessionId'=>session_id}
      request_options.merge!({:request_type=>"Get", :request_uri=> "/appointmentslots/meta", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)
      if status == 200
        location_mapping_by_id = response["Locations"].group_by{|location| location["LocationId"]}
        clinician_mapping_by_id = response["SessionHolders"].group_by{|session_holder| session_holder["ClinicianId"] }
        slots_mapping_with_clinician_id = response["Sessions"]
        slot_data  = {}
        # response["Sessions"].each do |slot|
        #    #slot = {"SessionName"=>"Daily DRIQ Session", "SessionId"=>3753, "LocationId"=>4884, "DefaultDuration"=>15, "SessionType"=>"Timed", "NumberOfSlots"=>32, "ClinicianIds"=>[3386], "StartDate"=>"2020-01-15T08:00:00", "EndDate"=>"2020-01-15T16:00:00"}
        #   slot["ClinicianIds"].each do |clinician_id|
        #     slot_data[clinician_id] = slot_data[clinician_id] || []   
        #     slot = slot.merge({:location=>(location_mapping_by_id[slot["LocationId"]].first|| {}) })
        #     slot_data[clinician_id] << slot
        #   end
        # end
        clinician_mapping_by_id.each do |clinician_id,clinician_detail|
          clinician_data = ::Emis::BookAppointment.formatted_clinician_data(clinician_detail.first)
          #slots = (slot_data[clinician_id] || []).sort_by{|a| a["StartDate"]}
          #clinician_data[:slots_data] = slots.group_by{|a| a["StartDate"]}
          doctor_list <<  clinician_data
        end
      else
        raise response["Message"]
      end
    rescue Exception => e 
      GpsocLogger.info  ".....Error inside ........./appointmentslots/meta................................................."
      GpsocLogger.info  ".....#{e.message}................................................"
    end
    doctor_list
  end
   
  def self.formatted_clinician_data(clinician_datail,options={})
    #clinician_datail =  "ClinicianId"=>3386, "DisplayName"=>"TEST, ATTechnology (Mr)", "Forenames"=>"ATTechnology", "Surname"=>"Test", "Title"=>"Mr", "Sex"=>"Male", "JobRole"=>"General Medical Practitioner", "Languages"=>[]}
    return {} if clinician_datail.blank?
    data  = { :clinician_id => clinician_datail["ClinicianId"].to_s, 
      :name=>clinician_datail["DisplayName"],
      :title => clinician_datail["Title"],
      :gender => clinician_datail["Sex"],
      :type => clinician_datail["JobRole"],
      :specialization=> clinician_datail["JobRole"],
      :languages => clinician_datail["Languages"]
    }
    data
  end
  
  def self.formatted_slot_data(slot_detail,options={})
    begin
      data = []
       data << {
          :start_time=>slot_detail["StartTime"].to_time.strftime("%H:%M"),
          :end_time=>slot_detail["EndTime"].to_time.strftime("%H:%M"), 
          :id=>slot_detail["SlotId"].to_s,
          :appointment_id=>slot_detail["SlotId"].to_s,
          :slot_id=>slot_detail["SlotId"].to_s,
          :slot_type=>slot_detail["SlotTypeName"],
          :slot_type_status=>slot_detail["SlotTypeStatus"]
        }
        
    rescue Exception =>e 
      data = nil
      Rails.logger.info "formatted_slot_data error : .................... #{e.message}............"
    end
    data
  end      
 
  # options = {:request_data => {:LocationId=> 4884,:ClinicianId=>3386, :FromDateTime=>(Time.now + 7.day), :ToDateTime=>(Time.now + 8.day)}}
  def self.get_available_appointments(organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      request_data = {}
      appointment_data = {}
      request_data = options[:request_data] || {}
      request_data = request_data.reject{|k,v| v.blank? }
      # testing code
      # request_data = {:LocationId=> 4884,:ClinicianId=>3386, :FromDateTime=>(Time.now + 7.day), :ToDateTime=>(Time.now + 8.day)}
     
      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken': link_token}.merge(request_data) 
      request_header = {'X-API-SessionId'=>session_id}
      request_options = {}
      request_options.merge!({:request_type=>"Get", :request_uri=> "/appointmentslots", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)
      if (status != 200)
        raise (response["Message"] || 'Invalid session') if @retry_count.to_i <= 2
        status = ::Emis::Client.get_status(status,options)
      elsif status == 200
        session_group_by_date = response["Sessions"].group_by{|session| session["SessionDate"].to_date}
        session_group_by_date.each do |session_date, session_detail|
          # appointment_data[session_date.to_s] = session_detail.first["Slots"].map{|slot| ::Emis::BookAppointment.formatted_slot_data(slot)}.flatten.compact
          appointment_data[session_date.to_s] = session_detail.first["Slots"].sort_by{|a| a["StartTime"]}.map{|slot| ::Emis::BookAppointment.formatted_slot_data(slot)}.flatten.compact

        end
      end
    rescue Exception => e
      GpsocLogger.info  "..............................................................."
      GpsocLogger.info  "get_available_appointments error msg is :#{e.message}"
      @error_message = response["Message"]
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      GpsocLogger.info "Inside exception retry count:  #{@retry_count} .................." 
      @retry_count = @retry_count.to_i + 1
      retry if @retry_count.to_i <= 2
       @retry_count = 0
        status = 100

    end
    doctor_list = nil
    appointments = {:message=>@error_message, :slots_data=>appointment_data,:doctor_list=>doctor_list}
    [status,appointments]
  end


    # options = {current_user=>current_user,}
    # appointment_details = {:appointment_id=>113036,:appointment_purpose=>'Consultation',:telephone_no=>'8826936913', :telephone_contact_type=> Unknown/ Home/ Work/ Mobile/ Other}
    # appointment_details = {:appointment_id=>113036,:appointment_purpose=>'Consultation',:telephone_no=>'8826936913', :telephone_contact_type=> "Unknown"}
  def self.book_appointment(organization_uid,member_id,appointment_details,options={})
    begin
      current_user = options[:current_user]
      appointment_details = appointment_details.with_indifferent_access
      slot_id =  appointment_details[:appointment_id].to_i
      booking_reason = appointment_details[:appointment_purpose].to_s
      telephone_number = appointment_details[:telephone_no]
      telephone_contact_type = ['Unknown', 'Home', 'Work', 'Mobile', 'Other'].map(&:downcase).include?(appointment_details[:telephone_contact_type].to_s.downcase) ? appointment_details[:telephone_contact_type].titleize : 'Unknown'
      request_data = options[:request_data] || {}
      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken' => link_token,
          "SlotId"=> slot_id, 
          "BookingReason"=> booking_reason,
          "TelephoneNumber"=> telephone_number,"TelephoneContactType"=>telephone_contact_type}.merge(request_data) 
      request_header = {'X-API-SessionId'=>session_id}
      request_options = {}
      request_options.merge!({:request_type=>"Post", :request_uri=> "/appointments", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)
     
    if (status == 409)
      {message:Message::Emis[:BookAppointment][:error][:slot_unavailable],status:100}
    elsif status == 200 || status == 201 || response["BookingCreated"].to_s == "true" 
      {id:slot_id.to_s,:source=>"emis",status:200,message:Message::Emis[:BookAppointment][:success][:booking_created]}
    else
      if response["Message"] == "Booking reason is not allowed by the practice"
        @retry_count = 0
         appointment_details[:appointment_purpose] = nil
        raise response["Message"]
      end
      api_status = ::Emis::Client.get_status(status,options)
      {id:slot_id.to_s,:source=>"emis",status:api_status,message:response["Message"]}
    end
    rescue Exception => e
      if @retry_count.to_i <= 2
        ::Emis::Client.clear_member_session(current_user,member_id,options)
        @retry_count = @retry_count.to_i + 1 
        retry 
      end
       @retry_count = 0
      {message: Message::Emis[:BookAppointment][:error][:unable_to_book_appointment],status:100}
    end
  end

  # options = {:appointment_details=>appointment_details,:current_user=>current_user}
  # appointment_details = {:appointment_id=>'113037',:cancellation_reason=>'no resson'}
  def self.cancel_appointment(ods_code,member_id,options={})
    begin
      current_user = options[:current_user]
      
      appointment_details = options[:appointment_details]
      appointment_id =  appointment_details[:appointment_id].to_i
      cancellation_reason = appointment_details[:cancellation_reason]
      request_data = options[:request_data] || {}
      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken' => link_token,
          "SlotId"=> appointment_id, 
          "CancellationReason"=> cancellation_reason}.merge(request_data)
      request_header = {'X-API-SessionId'=>session_id}
      request_options = {}
      request_options.merge!({:request_type=>"Delete", :request_uri=> "/appointments", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)
     
      if (status == 200) 
        {id:appointment_id.to_s,status:200,:message=>Message::Emis[:BookAppointment][:success][:appointment_canceled]}
      else
        api_status = ::Emis::Client.get_status(status,options)
        {message:response["Message"], error:response["Message"], status:api_status}
      end
    rescue Exception=>e 
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      {error:e.message, id:appointment_id,status:100,message:Message::Emis[:BookAppointment][:error][:unable_to_cancel_appointment]}
    end
  end
  

  def self.update_appointment(organization_uid,member_id,appointment_details,options={})
    {id:Random.rand(99999999999),status:200}
  end
  
  

  def self.getPatienAppointment(member_id,options = {})
    
    begin
      current_user = options[:current_user]
      request_data = options[:request_data] || {}
      member = Member.find(member_id)
      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken' => link_token}.merge(request_data)
      request_header = {'X-API-SessionId'=>session_id}
      request_options = {}
      request_options.merge!({:request_type=>"Get", :request_uri=> "/appointments", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)
        
        
      if status == 200
        data = []
        doctor_list = {}
        clinician_group_by_id = response["SessionHolders"].group_by{|clinician_detail| clinician_detail["ClinicianId"] }
        sessions_data = {}
        response["Sessions"].each do |session|
          session_data = session
          session_data["Clinician"] =  clinician_group_by_id[session["ClinicianIds"].first].first
          sessions_data[session["SessionId"]] = session_data
        end
        # session_data = [session_data].group_by{|a| a["SessionId"]}
        response["Appointments"].each do |appointment_slot|
          session_detail = sessions_data[appointment_slot["SessionId"]] || {}
          clinician_detail = session_detail["Clinician"] || {}
          data << {
          :id=>appointment_slot["SlotId"].to_s,
          :appointment_id=>appointment_slot["SlotId"].to_s,
          :purpose=>appointment_slot["BookingReason"], 
          :appointment_purpose=>appointment_slot["BookingReason"],
          :start_time=>Time.find_zone("London").parse(appointment_slot["StartTime"]).utc,
          :end_time=>Time.find_zone("London").parse(appointment_slot["EndTime"]).utc, 
          :doctor_name=>clinician_detail["DisplayName"],
          :doctor_clinic_id =>clinician_detail["ClinicianId"].to_s,
          :can_cancel=>true,
          :can_reschedule=> false,
          :doctor_specialization=> clinician_detail["JobRole"],
          :organization_uid=>member.organization_uid,
          :member_id=>member_id,
          :appointment_status=>"confirmed",
          :data_source=>"emis"
          }
        end
        data = data.sort_by{|a| a[:start_time]}.reverse rescue data
        data = {:appointments=>data,:status=>200}
     
      else
        api_status = ::Emis::Client.get_status(status,options)
        data  = {:appointments=>[],:error=> response["Message"],message:response["Message"],status:api_status}
      end
     
    rescue Exception=> e
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      @retry_count = 0
      data = {:appointments=>[],:error=> e.message,message:Message::Emis[:BookAppointment][:error][:unable_to_list_appointment],status:100}
    end
    data
  end

end