class Emis::Pharmacy
  
  # Emis::Prescribing.user_nominated_pharmacy(member_id,options)
  def self.user_nominated_pharmacy(member_id,options={})
    begin
      current_user = options[:current_user]
      request_options = {}
      request_data = options[:request_data] || {}
      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken': link_token}.merge(request_data) 
      request_header = {'X-API-SessionId'=>session_id}
      request_options.merge!({:request_type=>"Get", :request_uri=> "/pharmacies/nomination", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)

      if (status != 200 )
        status = ::Emis::Client.get_status(status,options)
        return {message:response["Message"], status:status}
      else
        return {pharmacies: response, status:200}
        @retry_count = 0
      end
    rescue Exception=>e 
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      return {error:e.message, message:Message::Emis[:Pharmacy][:unable_to_get_nominated_pharmacy], status:100}
    end
  end


  # Emis::Prescribing.list_pharmacies(member_id,options)
  # request_data = {:Postcode=>"RS1 Y6T",:SearchDistance=>""}
  def self.list_pharmacies(member_id,options={})
    begin
      current_user = options[:current_user]
      request_options = {}
      request_data = options[:request_data] || {}
      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken': link_token}.merge(request_data) 
      request_header = {'X-API-SessionId'=>session_id}
      request_options.merge!({:request_type=>"Get", :request_uri=> "/pharmacies", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)

      if (status != 200 )
        status = ::Emis::Client.get_status(status,options)
        return {message:response["Message"], status:status}
      else
        pharmacies = response["Pharmacies"] 
        return {pharmacies: pharmacies, status:200}
        @retry_count = 0
      end
    rescue Exception=>e 
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      return {message:Message::Emis[:Pharmacy][:unable_to_list_pharmacy], :error=>e.message ,status:100}
    end
  end



  # Emis::Prescribing.update_pharmacy_nomination(member_id,options)
  # request_data = {:NacsCode=>, :Postcode=>}
  def self.update_pharmacy_nomination(member_id,options={})
    begin
      current_user = options[:current_user]
      request_options = {}
      request_data = options[:request_data] || {}
      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken': link_token}.merge(request_data) 
      request_header = {'X-API-SessionId'=>session_id}
      request_options.merge!({:request_type=>"Put", :request_uri=> "/pharmacies/nomination", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)

      if (status != 200 )
        status = ::Emis::Client.get_status(status,options)
        return {message:response["Message"], status:status}
      else
        return {message:Message::Emis[:Pharmacy][:pharmacy_nomination_updated], status:200}
        @retry_count = 0
      end
    rescue Exception=>e 
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      return {error:e.message, message:Message::Emis[:Pharmacy][:pharmacy_nomination_not_updated], status:100}
    end
  end


  # Emis::Prescribing.delete_pharmacy_nomination(member_id,options)
  # request_data = {:NacsCode=>}
  def self.delete_pharmacy_nomination(member_id,options={})
    begin
      current_user = options[:current_user]
      request_options = {}
      request_data = options[:request_data] || {}
      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken': link_token}.merge(request_data) 
      request_header = {'X-API-SessionId'=>session_id}
      request_options.merge!({:request_type=>"Delete", :request_uri=> "/pharmacies/nomination", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)
      if (status != 200 )
        status = ::Emis::Client.get_status(status,options)
        return {message:response["Message"], status:status}
      else
        return {message:Message::Emis[:Pharmacy][:pharmacy_nomination_deleted] , status:200}
        @retry_count = 0
      end
    rescue Exception=>e 
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      return {error=>e.message, message:Message::Emis[:Pharmacy][:pharmacy_nomination_not_deleted], status:100}
    end
  end
  
   

end