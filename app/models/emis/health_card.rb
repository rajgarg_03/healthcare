class Emis::HealthCard
  # Get Patient Allergy
  # None, Allergies, Consultations, Documents, Immunisations, Medication, Problems, TestResults
  # options = {:item_type=>"Consultations",:current_user=>current_user}
  # Emis::HealthCard.get_patient_health_record(member_id,options)
  def self.get_patient_health_record(member_id,options={})
    begin
      current_user = options[:current_user]
      request_options = {}
      request_data = options[:request_data] || {}
      item_type = options[:item_type]
      if item_type == "Immunisations"
        response = ::Emis::Immunisations.get_patient_immunisations(member_id,options)
        return response
      elsif item_type == "Allergy"
        response = ::Emis::Allergy.get_patient_allergies(member_id,options)
        # response[:data] = response[:data].values.flatten.sort_by{|a| ["updated_at"]}.reverse rescue []
        return response        
      end
      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken': link_token,"ItemType"=>item_type}.merge(request_data) 
      request_header = {'X-API-SessionId'=>session_id}
      request_options.merge!({:request_type=>"Get", :request_uri=> "/record", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)
      # [{"MedicalRecord"=>{"PatientGuid"=>"5a09eb8f-7e0b-4630-ad48-87e8e05d124f", "Title"=>"Master", "Forenames"=>"Mike", "Surname"=>"Singh", "Sex"=>"Male", "DateOfBirth"=>"2016-09-22T00:00:00", "Allergies"=>[{"ObservationType"=>"Allergy", "Episodicity"=>"First", "NumericValue"=>nil, "NumericOperator"=>nil, "NumericUnits"=>nil, "DisplayValue"=>nil, "TextValue"=>nil, "Range"=>nil, "Abnormal"=>false, "AbnormalReason"=>nil, "AssociatedText"=>nil, "EventGuid"=>"0d41cfe3-fca8-461d-9332-773d36a192e7", "Term"=>"Egg allergy", "AvailabilityDateTime"=>"2020-01-30T06:16:29.357", "EffectiveDate"=>{"DatePart"=>"YearMonthDay", "Value"=>"2020-01-30T00:00:00"}, "CodeId"=>637141000006110, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}], "Consultations"=>nil, "Documents"=>nil, "Immunisations"=>nil, "Medication"=>nil, "Problems"=>nil, "TestResults"=>nil, "Users"=>[{"UserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "Title"=>"Dr", "Forenames"=>"Nurturey", "Surname"=>"Test", "Role"=>"General Medical Practitioner", "Organisation"=>"Nurt services"}]}, "FilterDetails"=>{"ItemFilter"=>"Allergies", "ItemFilterFromDate"=>nil, "ItemFilterToDate"=>nil, "FreeTextFilterFromDate"=>nil}}, 200]
      if status == 200
        observation_data = []
        if ["Problems"].include?(item_type)
          observation_data = (response["MedicalRecord"][item_type]||[]).map{|problem| problem["Observation"] }
        elsif [ "Documents"].include?(item_type)
          observation_data = (response["MedicalRecord"][item_type]||[]).map do |problem| 
            doc = problem["Observation"] 
            doc["DocumentGuid"] = problem["DocumentGuid"]
            doc
          end
        elsif item_type == "TestResults"
          observation_data = response["MedicalRecord"][item_type].map{|problem| problem["Value"] } + response["MedicalRecord"][item_type].map{|problem| problem["ChildValues"] }
          observation_data = observation_data.flatten.compact
        else
          observation_data = response["MedicalRecord"][item_type].flatten.compact#.select{|observation| observation["Term"].present? }
        end
        allergy_records = Emis::HealthCard.get_formatted_health_data(member_id, observation_data.compact,item_type, options)
        message = nil
      else
        # status = 100
        status = ::Emis::Client.get_status(status,options)
        raise (response["Message"] || 'Error inside get_patient_allergies') if (@retry_count.to_i < 2 && status != StatusCode::Status403)
        if status == 403
          message = ::Message::Emis[:HealthCard][:service_disabled] %{:item_type=>item_type}
        else
          message = response["Message"]
        end
        allergy_records = []
      end    
    rescue Exception=>e 
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @retry_count = @retry_count.to_i + 1
      @error = nil 
      retry if @retry_count.to_i <= 2
      status = 100
      message  = ::Message::Emis[:HealthCard][:unable_to_list] %{:item_type=>item_type}
      @retry_count = 0
      @error = e.message
    end
    {data:allergy_records,:status=>status,:message=>message,:error=>@error}
  end

  def self.get_formatted_health_data(member_id,response_records,item_type,options={})
    data = {}
    # "2020-01-13T10:22:59"
    group_by_date = response_records.group_by{|a| a["EffectiveDate"]["Value"].to_s}
    group_by_date.each do |date,records|
      if item_type == "Consultations"
        record_data = ::Emis::HealthCard.consultations_data_format(date,records,member_id,options)
      elsif item_type == "Medication"
        record_data = ::Emis::HealthCard.medication_data_format(date,records,member_id,options)
      elsif item_type == "Problems"
        record_data = ::Emis::HealthCard.problem_data_format(date,records,member_id,options)
      elsif item_type == "TestResults"
        record_data = ::Emis::HealthCard.test_results_data_format(date,records,member_id,options)
      elsif item_type == "Documents"
        record_data = ::Emis::HealthCard.documents_data_format(date,records,member_id,options)
      end
      data[date] = record_data
    end
    data = data.select{|k,v| v.present? }
    data
  end
  
  def self.medication_data_format(date,records,member_id,options={})
    # [{"DrugName"=>"Co-beneldopa 25mg/100mg modified-release capsules", "Dosage"=>"1", "Quantity"=>1.0, "QuantityUnit"=>"capsule", "QuantityRepresentation"=>"1 capsule", "IsMixture"=>false, "Mixture"=>nil, "FirstIssueDate"=>"2020-02-02T12:26:50.957", "LastIssueDate"=>"2020-02-02T12:26:50.957", "PrescriptionType"=>"Repeat", "DrugStatus"=>"Active", "EventGuid"=>"4895ccfb-e02f-4594-8ca7-6046b5d28d13", "Term"=>"Co-beneldopa 25mg/100mg modified-release capsules", "AvailabilityDateTime"=>"2020-02-02T12:26:25.34", "EffectiveDate"=>{"DatePart"=>"YearMonthDay", "Value"=>"2020-02-02T00:00:00"}, "CodeId"=>359341000033113, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}] 
    data = []
    records.each do |record| 
      name = record["Term"].to_s + " \n" + record["QuantityRepresentation"] 
      data << ({:record_catgeory=>"Medication",dosage:record["QuantityRepresentation"],:quantity=>record["Quantity"],:record_term=> record['Term'],:record_uid=>record["EventGuid"], :record_date=>date.to_date, :member_id=>member_id,:name=>name, :value=> record["NumericValue"],:data_source=>"emis",:added_by=>"system",:updated_at=>date.to_date.to_date7 })
    end
    data 
  end

  def self.problem_data_format(date,records,member_id,options={})
    # [{"Status"=>"Active", "Significance"=>"Significant", "ProblemEndDate"=>nil, "Observation"=>{"ObservationType"=>"Allergy", "Episodicity"=>"First", "NumericValue"=>nil, "NumericOperator"=>nil, "NumericUnits"=>nil, "DisplayValue"=>nil, "TextValue"=>nil, "Range"=>nil, "Abnormal"=>false, "AbnormalReason"=>nil, "AssociatedText"=>nil, "EventGuid"=>"3f33081a-809f-45d5-bb11-56e740cfe776", "Term"=>"Cow's milk allergy", "AvailabilityDateTime"=>"2020-01-31T14:23:55.353", "EffectiveDate"=>{"DatePart"=>"YearMonthDay", "Value"=>"2019-01-10T00:00:00"}, "CodeId"=>26953014, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}}] 
    data = []
    records.each do |record| 
      name = record["ObservationType"].to_s + " \n" + record["Term"] 
      data << ({:record_catgeory=>record["ObservationType"].to_s,:record_term=> record['Term'],:record_uid=>record["EventGuid"], :record_date=>date.to_date, :member_id=>member_id,:name=>name, :value=> record["NumericValue"],:data_source=>"emis",:added_by=>"system",:updated_at=>date.to_date.to_date7 })
    end
    data 
  end

  def self.documents_data_format(date,records,member_id,options={})
    # data =[{"DocumentGuid"=>"386854ae-b452-460b-912d-4b67aebc953b", "Observation"=>{"ObservationType"=>"Document", "Episodicity"=>"Unknown", "NumericValue"=>nil, "NumericOperator"=>nil, "NumericUnits"=>nil, "DisplayValue"=>nil, "TextValue"=>nil, "Range"=>nil, "Abnormal"=>false, "AbnormalReason"=>nil, "AssociatedText"=>nil, "EventGuid"=>"883a758f-4599-4f3d-a3d7-90d75f4dafe6", "Term"=>"Administration", "AvailabilityDateTime"=>"2020-01-31T14:22:23.55", "EffectiveDate"=>{"DatePart"=>"YearMonthDayTime", "Value"=>"2020-01-31T14:22:08"}, "CodeId"=>2548615018, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}, "Size"=>27126, "PageCount"=>1, "Extension"=>"pdf", "Available"=>true}]  []
    data = []
    records.each do |record| 
      uid = record["DocumentGuid"]
      document_guid = record["EventGuid"]
      name = "#{record['Term']} \nDocument-" + document_guid.split("-").last
      last_modified = record["AvailabilityDateTime"]
      data << ({:record_catgeory=>"Document",:record_term=> record['Term'], :record_uid=>record["EventGuid"], :record_date=>date.to_date, uid:uid , document_type:record["Term"],:member_id=>member_id,:name=>name, :value=>document_guid ,:data_source=>"emis",:added_by=>"system",:updated_at=>date.to_date.to_date7 })
    end
    data 
  end

  def self.test_results_data_format(date,records,member_id,options={})
    # [{[{"ObservationType"=>"Value", "Episodicity"=>"None", "NumericValue"=>45.0, "NumericOperator"=>nil, "NumericUnits"=>"cm", "DisplayValue"=>"45 cm", "TextValue"=>"45", "Range"=>nil, "Abnormal"=>false, "AbnormalReason"=>nil, "AssociatedText"=>nil, "EventGuid"=>"7c8454ec-da98-4f80-89ac-3e88de112523", "Term"=>"O/E - height", "AvailabilityDateTime"=>"2020-01-31T14:17:17.493", "EffectiveDate"=>{"DatePart"=>"YearMonthDay", "Value"=>"2018-01-26T00:00:00"}, "CodeId"=>253669010, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}, {"ObservationType"=>"Value", "Episodicity"=>"None", "NumericValue"=>3.5, "NumericOperator"=>nil, "NumericUnits"=>"kg", "DisplayValue"=>"3.5 kg", "TextValue"=>"3.5", "Range"=>nil, "Abnormal"=>false, "AbnormalReason"=>nil, "AssociatedText"=>nil, "EventGuid"=>"d0867500-f72c-4845-b197-d18cb465fcde", "Term"=>"O/E - weight", "AvailabilityDateTime"=>"2020-01-31T14:18:39.543", "EffectiveDate"=>{"DatePart"=>"YearMonthDay", "Value"=>"2018-01-26T00:00:00"}, "CodeId"=>253677014, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}] "DrugName"=>"Co-beneldopa 25mg/100mg modified-release capsules", "Dosage"=>"1", "Quantity"=>1.0, "QuantityUnit"=>"capsule", "QuantityRepresentation"=>"1 capsule", "IsMixture"=>false, "Mixture"=>nil, "FirstIssueDate"=>"2020-02-02T12:26:50.957", "LastIssueDate"=>"2020-02-02T12:26:50.957", "PrescriptionType"=>"Repeat", "DrugStatus"=>"Active", "EventGuid"=>"4895ccfb-e02f-4594-8ca7-6046b5d28d13", "Term"=>"Co-beneldopa 25mg/100mg modified-release capsules", "AvailabilityDateTime"=>"2020-02-02T12:26:25.34", "EffectiveDate"=>{"DatePart"=>"YearMonthDay", "Value"=>"2020-02-02T00:00:00"}, "CodeId"=>359341000033113, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}] 
    data = []
    records.each do |record|
      term = record['Term']  
      record_catgeory = ""
      name =  [record['Term'], record['DisplayValue'] ].compact.join(" : ")# "#{record['Term']} : #{record['DisplayValue']}" 
      if term.include?("height")
        record_catgeory = 'height'
        title = "height : #{record['DisplayValue']}"
      elsif term.include?("weight")
        title = "weight : #{record['DisplayValue']}"
        record_catgeory = 'weight'
      else
        title = name
      end
      data << ({:record_catgeory=>record_catgeory,:record_term=>record['DisplayValue'], :record_uid=>record["EventGuid"], :record_date=>date.to_date, :member_id=>member_id,:title=>title, :name=>name, :value=> record["DisplayValue"],:data_source=>"emis",:added_by=>"system",:updated_at=>date.to_date.to_date7 })
    end
    data 
  end
  
  def self.consultations_data_format(date,records,member_id,options={})
    data = []
    records.each do |record| 
      (record["Sections"]||[]).each do |section|
        section_name = section["Header"]
        (section["Observations"]||[]).each do |observation|
          # if observation["Term"].to_s.present? || record["NumericValue"].present?
            name = section_name + " \n" + observation["Term"].to_s
            data << ({:record_catgeory=>section_name,:record_term=>observation["Term"].to_s, :record_uid=>record["EventGuid"], :record_date=>date.to_date, :member_id=>member_id,:name=>name, :value=> record["NumericValue"],:data_source=>"emis",:added_by=>"system",:updated_at=>date.to_date.to_date7 })
          # end
        end
      end
    end
    data.uniq
  end  
end
 