class Emis::Measurements
#   Get Patient Measurements
#   Description – This method can be used to get the patient details for the given user. Patient details
#                 will be given as json string. This will also have the measurements for each visit.
  
  def self.get_patient_measurements(organization_uid,member_id,options={})
    begin
      measurement_ids = Health.where(member_id:member_id,:data_source=>"emis").pluck(:id)
      Timeline.where(timelined_type: "Health", timelined_id: measurement_ids).delete_all
      Health.where(member_id:member_id,:data_source=>"emis").delete_all
      current_user = options[:current_user]
      
      request_options = {}
      request_data = options[:request_data] || {}

      session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      request_body = {'UserPatientLinkToken': link_token,"ItemType"=>"TestResults"}.merge(request_data) 
      request_header = {'X-API-SessionId'=>session_id}
      request_options.merge!({:request_type=>"Get", :request_uri=> "/record", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)
      if status == 200
        # ["MedicalRecord"]["TestResults"] = 
        # [{"Value"=>{"ObservationType"=>"Value", "Episodicity"=>"None", "NumericValue"=>165.0, "NumericOperator"=>nil, "NumericUnits"=>"cm", "DisplayValue"=>"165 cm", "TextValue"=>"165", "Range"=>nil, "Abnormal"=>false, "AbnormalReason"=>nil, "AssociatedText"=>nil, "EventGuid"=>"3d985c0e-f3e8-45fd-b488-6bfe779dec09", "Term"=>"O/E - height", "AvailabilityDateTime"=>"2020-01-13T13:38:40.877", "EffectiveDate"=>{"DatePart"=>"YearMonthDay", "Value"=>"2020-01-10T00:00:00"}, "CodeId"=>253669010, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}, "ChildValues"=>[{"ObservationType"=>"Observation", "Episodicity"=>"None", "NumericValue"=>nil, "NumericOperator"=>nil, "NumericUnits"=>nil, "DisplayValue"=>nil, "TextValue"=>nil, "Range"=>nil, "Abnormal"=>false, "AbnormalReason"=>nil, "AssociatedText"=>nil, "EventGuid"=>"fbc8ffe8-5d53-4d1d-844e-3775bf78fcd5", "Term"=>"Child height > 99.6th centile", "AvailabilityDateTime"=>"2020-01-13T13:38:40.933", "EffectiveDate"=>{"DatePart"=>"YearMonthDay", "Value"=>"2020-01-10T00:00:00"}, "CodeId"=>459092015, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}]} ]
        observation_data = []
        response["MedicalRecord"]["TestResults"].each do |consultation|
           observation_data << consultation["Value"]
          end
        observation_data = observation_data.flatten.compact.select{|observation| observation["Term"].present? }
        measurement_records = Emis::Measurements.get_formatted_measurement_data(member_id, observation_data,options)
        measurement_records.each do |updated_at,data|
          begin
            data[:health].delete(nil)
            data.delete(nil)
            data = data.with_indifferent_access
            data[:health] = data[:health].with_indifferent_access
            if data[:health].present?
              Health.create_measurement_record(current_user,data)
            end
          rescue Exception =>e 
            Rails.logger.info "......Inside emis::Measurement. get_patient_measurements  #{e.message}........"
          end
        end
        #status = 401 if measurement_records.blank?
        @retry_count = 0
      else
        status = ::Emis::Client.get_status(status,options)
      end
    rescue Exception=>e 
      ::Emis::Client.clear_member_session(current_user,member_id,options)
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      status = 100
    end
    status
  end

   def self.get_formatted_measurement_data(member_id,records,options={})
    measurement_unit = {"height"=>"length_unit","weight"=>"weight_unit"}
    health_data = {}
    # group_by_date = records.group_by{|a| a[:value][:availability_date_time].to_s}
    group_by_date = records.group_by{|a| a["EffectiveDate"]["Value"].to_s}

    group_by_date.each do |date,health_records|
      measurement_data = {:health=>{}}
      health_records.each do |record|
        data = record
        record_date = date.to_date.to_s
        measurement_parameter = ::Emis::Measurements.get_measurement_parameter(data["Term"])
        if measurement_parameter.present?
          metric_unit = measurement_unit[measurement_parameter]
          measurement_data[metric_unit]= (data["NumericUnits"].pluralize rescue nil )
          measurement_data[:health].merge!({:member_id=>member_id,measurement_parameter=> data["NumericValue"],:data_source=>"emis",:added_by=>"system",:updated_at=>record_date })
        end
      end
      health_data[date] = measurement_data
    end
    health_data
  end



   

 
  def self.get_measurement_parameter(term)
    if term.include?("height")
      "height"
    elsif term.include?("weight")
        "weight"
    end
  end

  def self.get_patient_measurements1(organization_uid,member_id,options={})
    retry_count = 0
    begin
      data = []
      current_user = options[:current_user]
      api_version = options[:api_version]
    rescue Exception=>e
      Rails.logger.info "Error inside get_patient_measurements-- #{e.message}"
      puts "Error inside get_patient_measurements-- #{e.message}"
    end
    data
  end
  
end
 