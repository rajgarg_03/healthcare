class SysArticleRefLink
  include Mongoid::Document
  include Mongoid::Timestamps
  field :custome_id,             :type => Integer # system pointer custom id
  field :link_order, :type=>Integer
  field :name,      :type => String
  field :source, :type=> String
  field :title,      :type => String
  field :type,       :type => String 
  field :status, :type=> String, :default=>"Active"
  field :image_url,       :type => String
  field :country, :type=>String
  field :link_image_url_status,       :type => String # valid/invalid
  field :link_url_status,             :type => String # valid/invalid
  field :keywords,      :type => String
  field :description,   :type => String
  index ({country: 1})
  index ({custome_id: 1})
  index ({status: 1})
  
  validates :custome_id, presence: true
  CountryList = ["india","uk","us","ae","other"]
  def self.group_by_system_pointer_custom_id(country_code,custom_ids=nil)
   # custom_ids = [235, 234, 230, 229, 228, 227, 226, 225, 224, 223, 222, 221, 220, 219, 218, 217, 216, 215, 214, 213, 212, 211, 210, 209, 208, 207, 206, 205, 204, 203, 202, 201, 200, 199, 198, 197, 196, 195, 194, 193, 192, 191, 190, 189, 188, 187, 186, 185, 184, 183, 182, 181, 180, 179, 178, 177, 176, 175, 174, 173, 172, 171, 170, 169, 168, 167, 166, 165, 164, 163, 162, 161, 160, 159, 158, 157, 156, 155, 154, 153, 152, 151, 150, 149, 148, 147, 146, 145, 144, 143, 142, 141, 140, 139, 138, 137, 136, 135, 134, 133, 132, 131, 130, 129, 128, 127, 126, 125, 124, 123, 122, 121, 120, 119, 118, 117, 116, 115, 114, 113, 112, 111, 110, 109, 108, 107, 106, 105, 104, 103, 102, 101, 100, 99, 98, 97, 96, 95, 94, 93, 92, 91, 90, 89, 88, 87, 86, 85, 84, 83, 82, 81, 80, 79, 78, 77, 76, 75, 74, 73, 72, 71, 70, 69, 68, 67, 66, 65, 64, 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 322, 321, 320, 319, 318, 317, 316, 315, 314, 313, 312, 311, 310, 309, 308, 307, 306, 305, 304, 303, 302, 301, 300, 299, 298, 297, 296, 295, 294, 293, 292, 291, 290, 289, 288, 287, 286, 285, 284, 283, 282, 281, 280, 279, 278, 277, 276, 275, 274, 273, 272, 271, 270, 269, 268, 267, 266, 265, 264, 263, 262, 261, 260, 259, 258, 257, 256, 255, 254, 253, 252, 251, 250, 249, 248, 247, 246, 244, 243, 242, 241, 240, 239, 238, 237, 236, 235, 234, 233, 232, 231]
    country_code =  country_code.downcase rescue country_code
    country_code = "india" if country_code == "in"
    country_code = "uk" if ((country_code.downcase == "gb") rescue false)
    code = ["india","uk","us","other"].include?(country_code) ? country_code : "other"
    if custom_ids.blank?
        match_values = {"country"=>country_code,"status"=>"Active" } 
    else
        match_values = {"country"=>country_code,"status"=>"Active", "custome_id"=>{"$in"=>custom_ids} } 
    end
    hash_data =  SysArticleRefLink.collection.aggregate([
      { "$match" => match_values },
      {"$group" => {
        "_id" => "$custome_id",
        "data" => {"$push"=> "$$ROOT"}
      }},
      {"$sort"=>{"link_order"=>1}}
    ])

    data = {}
    hash_data.each{|result| data[result["_id"] ] = result["data"].map{|link| SysArticleRefLink.filter_data(link)} }
    data
  end
  
  def validate_link_url(url)
    response = Faraday.get(url)
    status_code  = response.status
    valid_url = url
    if status_code.to_s == "200"
      status = "valid"
    elsif status_code == 301
      valid_url = response.headers["location"]
      if valid_url.split("://").last == url.split("://").last
        status = "valid"
      else
        status = "invalid"
      end
    else
      status = "invalid"
    end
    [status,valid_url]
  end

  def validate_and_update_links_status
    begin
      status,valid_url = [nil,nil]
      pointer_link = self
      begin
        status,valid_url  = validate_link_url(pointer_link.name)
        pointer_link.link_url_status = status
        pointer_link.name = valid_url
      rescue Exception => e 
        Rails.logger.info "Error inside validate_and_update_links_status.."
        Rails.logger.info e.message
      end
      begin
        status,valid_url = validate_link_url(pointer_link.image_url)
        pointer_link.link_image_url_status = status
        pointer_link.image_url = valid_url  
      rescue Exception => e
        Rails.logger.info "Error inside validate_and_update_links_status.."
        Rails.logger.info e.message
      end
      pointer_link.save
    rescue Exception => e 
      Rails.logger.info "Error inside validate_and_update_links_status.."
      Rails.logger.info e.message
    end
  end

  def self.filter_data(link,system_pointer=nil)
    if system_pointer.present?
      if (system_pointer.pointer_type.downcase == "affiliate" rescue false)
        article_provide = link['source']
      else
        article_provide = "#{link['type']} | #{link['source']}"
      end
    else
      article_provide = "#{link['type']} | #{link['source']}"
    end
    {
      "url" => link["name"],
      "pointer_reference_link_id" => link["_id"],
      "article_provide" => article_provide,
      "linktype"=> link['type'],
      "linkimageurl"=>link['image_url'],
      "linktitle"=> link['title']
    }
  end
  
  # Copy all pointer link form 1 country to other country
  # SysArticleRefLink.clone_pointer_links_for_country("other","ae")
  def self.clone_pointer_links_for_country(source_country,destination_country,options={})
    system_pointer_reference_links = SysArticleRefLink.where(:country=>source_country.downcase)
    system_pointer_reference_links.each do |system_pointer_link|
      begin  
        pointer_link = system_pointer_link.attributes
        pointer_link.delete("_id")
        pointer_link["country"] = destination_country.downcase
        cloned_sys_pointer_link = SysArticleRefLink.new(pointer_link) 
        cloned_sys_pointer_link.save
      rescue Exception=>e
        Rails.logger.info ".....Error inside clone_pointer_links_for_country ..........."
        Rails.logger.info e.message
      end
    end
  end
  
  # Used in data table
  def self.datatable_filter(search_value, search_columns)
    return all if search_value.blank?
    result = self
    filter = []
    search_columns.each do |key, value|
      if value['data'] == "custome_id"
       filter << {"#{value['data']}"=>search_value} if value['searchable']
      else
       filter << {"#{value['data']}"=>/#{search_value}/i} if value['searchable']
      end
    end
    result = result.where("$or"=>filter) 
    result
  end  

  DATATABLE_COLUMNS = [:custome_id,:link_order,:name,:title,:source,:type,:image_url,:status,:country]
  
  def self.datatable_order(order_column_index, order_dir)
    order_by("#{SysArticleRefLink::DATATABLE_COLUMNS[order_column_index]} #{order_dir}")
  end
    

end