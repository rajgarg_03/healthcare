require 'securerandom'
require 'openssl'
require 'base64'

class Pbk
  def self.encrypt(password,client_token,server_token,options={})
    iteration = 1
    dk_length = 32 # in byte
    password = Utility.base64_to_hex(password)
    # password =  Utility.base64_to_hex("qvNqJ/Nu+91KUAZoQ6ww9GH+p7UOJRC2QO8wIbsT4jY=")
    client_token = client_token #||  "eKyvJlpyhjPbCITX4jl7aUdWCbIzRTQ3j8o8hZHkYNo="
    server_token = server_token #"yWr7gS180L/O3P/NuAE1RDdQmxvlV5k7xtXTL3pXwNo="
    client_token_byte = Utility.base64_to_hex(client_token)
    server_token_byte = Utility.base64_to_hex(server_token)
    salt = (client_token_byte + server_token_byte)
    Base64.strict_encode64(OpenSSL::PKCS5.pbkdf2_hmac_sha1(password, salt, 1, 32))
  end

  def self.session_hash(session_key=nil,sequence_no=nil)
    puts "number and session key"
    puts sequence_no
    puts session_key
    base64_session = session_key || "vBNiZ6wnS4QC4V4Lqrl+TSD6dw6vNMVjhfFLPL3ekHk="
    sequenceNumber = sequence_no
    hex_sequence_no = sequenceNumber.to_i.to_s(16).split("").each_slice(2).map{|a| a.join("")}.reverse.join
    base64_sequence_no = Utility.hex_to_base64(hex_sequence_no)
    session_byte = Utility.base64_to_hex(base64_session)
    sequence_no_byte = Utility.base64_to_hex(base64_sequence_no)
    session_hash_byte = session_byte + sequence_no_byte
    Digest::SHA256.base64digest(session_hash_byte)
    
  end
end

  
