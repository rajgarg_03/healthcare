class GpSoc::RegisteredUser
  include Mongoid::Document
  include Mongoid::Timestamps

  field :member_id,                  type: String
  field :practice_type,              type: String  # "emis."
  field :registration_detail,        type: Hash   #{:practice_ods=>,:linkage_key}
  
  validates_presence_of :member_id
  validates_presence_of :practice_type
  validates_presence_of :registration_detail

  def save_detail(current_user,data,options={})
  end
  

  # GpSoc::RegisteredUser.find_gp_practices_by_post_code("br8")
  def self.find_gp_practices_by_post_code(postcode,distance=50,options={})
    current_user = options[:current_user]
    nhs_gp_search_active = ::System::Settings.nhsGPSearchApiActive?(current_user,options)
    if nhs_gp_search_active
      system_setting = System::Settings.where(name:'nhsGPSearchApi').last
      if system_setting.blank?
        nhs_gp_search_by_place_or_postcode_active = true
        nhs_postcode_search_api_active = false
      else
        nhs_gp_search_by_place_or_postcode_active =  (system_setting.setting_values["nhs_search_by_place_or_postcode_status"].to_s == "true") rescue false
        nhs_postcode_search_api_active = (system_setting.setting_values["nhs_search_by_postcode_api_status"].to_s == "true") rescue false
      end
    else
      nhs_postcode_search_api_active = false
      nhs_gp_search_by_place_or_postcode_active = false
    end
    
    if nhs_gp_search_by_place_or_postcode_active
      GpSoc::RegisteredUser.find_practice_by_place_or_postcode(postcode,distance,options)
    elsif nhs_postcode_search_api_active
      GpSoc::RegisteredUser.find_practice_by_nhs_search_api(postcode,distance,options)
    else
      data = []
      emis_response = ::Emis::Client.get_clinic_detail_by_postcode(current_user,postcode,nil,options)
      if emis_response[:status] == 200
        status = emis_response[:status]
        data = GpSoc::RegisteredUser.format_data_for_emis_practices_response(emis_response,options)
        message = emis_response[:message]
        if data.blank? 
          message = ::Message::Clinic[:gp_not_found_nearby] 
          status = 100
        end
        [data,message,status]
      else
        status = emis_response[:status]
        message = emis_response[:message]
        [data,message,status]
      end
    end
  end


  def self.find_practice_by_place_or_postcode(postcode,distance=50,options={})
    begin
      filter =  "(OrganisationTypeID eq 'GPB') or (OrganisationTypeID eq 'GPP')"
      distance =  distance || 50
      url = "https://api.nhs.uk/service-search/search-postcode-or-place"
      # ?api-version=1&search=BR5%201QB"
      uri = URI(url)
      http = Net::HTTP.new(uri.host, uri.port)
      if uri.scheme == 'https'
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_PEER
    end
      request_query = {"api-version"=>1,"search"=>postcode}
      request_body = { "filter": filter, "top": 25, "skip": 0,"count": true }
      header = {'subscription-key'=>'74cc9b1db5c844d8ab8193bb8bf9295a', 'Content-Type'=>'application/json'}
      uri.query = URI.encode_www_form(request_query)
      
      request = Net::HTTP::Post.new(uri.request_uri, header)
      request.body = request_body.to_json 
      response = http.request(request)
      data = []
      message = nil
      status = 100
      Rails.logger.info "Gp search response............................ #{response.body.inspect} .................."
      if response.code.to_s == "200" 
        json_response = JSON.parse(response.body)
        data = ::GpSoc::RegisteredUser.format_data_for_search_by_place_postcode(json_response)
        Rails.logger.info "Gp search result............................ #{data.inspect} .................."
        status = 200
        if data.blank?
          message = ::Message::Clinic[:gp_not_found_nearby]
          status = 100
        end
      elsif response.code.to_s == "401" 
        json_response = JSON.parse(response.body)
        UserMailer.notify_dev_for_account_access("Find GP api account",json_response["message"]).deliver rescue nil
        Rails.logger.info "Error inside ....find_practice_by_place_or_postcode  Access denied"
        message = ::Message::Clinic[:gp_search_error]
        status = 401
      else
        message = ::Message::Clinic[:gp_search_error]
      end
    rescue Exception => e 
      Rails.logger.info "Error inside ....find_practice_by_place_or_postcode  #{e.message}"
      message = ::Message::Clinic[:gp_search_error]
    end
    [data,message,status]
  end


  # GpSoc::RegisteredUser.find_practice_by_nhs_search_api("br5 1qb")
  def self.find_practice_by_nhs_search_api(postcode,distance=50,options={})
    begin
      
      distance =  distance || 50
      url = "https://api.nhs.uk/data/gppractices/postcode/#{URI.encode(postcode)}/?distance=#{distance}"
      uri = URI(url)
      request = Net::HTTP::Get.new(uri.request_uri)
      # Request headers
      request['subscription-key'] =  APP_CONFIG['nhs_subscription_key']
      response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
        http.request(request)
      end
      data = []
      message = nil
      status = 100
      Rails.logger.info "Gp search response............................ #{response.body.inspect} .................."
      if response.code.to_s == "200" 
        json_response = JSON.parse(Hash.from_xml(response.body).to_json)
        data = format_data_for_gp_practices_response(json_response)
        Rails.logger.info "Gp search result............................ #{data.inspect} .................."
        status = 200
        if data.blank?
          message = ::Message::Clinic[:gp_not_found_nearby]
          status = 100
        end
      elsif response.code.to_s == "401" 
        json_response = JSON.parse(response.body)
        UserMailer.notify_dev_for_account_access("Find GP api account",json_response["message"]).deliver rescue nil
        Rails.logger.info "Error inside ....find_gp_practices_by_post_code  Access denied"
        message = ::Message::Clinic[:gp_search_error]
        status = 401
      else
        message = ::Message::Clinic[:gp_search_error]
      end
    rescue Exception => e 
      Rails.logger.info "Error inside ....find_gp_practices_by_post_code  #{e.message}"
      message = ::Message::Clinic[:gp_search_error]
    end
    [data,message,status]
  end

  
  def self.format_data_for_search_by_place_postcode(response,options={})
    data = []
    (response["value"]||[]).each do |gp_detail|
      begin
        address_line =  [ gp_detail["Address1"],gp_detail["Address2"],  gp_detail["Address3"],gp_detail["City"], gp_detail["County"]].compact.join(", ")
        postcode = gp_detail["Postcode"]
        title = gp_detail["OrganisationName"].strip 
        data << {
          :ods_code=>gp_detail["NACSCode"],
          :id=>gp_detail["OrganisationID"],
          :organization_uid=>gp_detail["OrganisationID"],
          :organization_type=>"unknown",
          :delinkable=>true,
          :title=>title,
          :postcode=>postcode,
          :phone_no=>( nil),
          :email=> nil,
          :longitude => gp_detail["Longitude"],
          :latitude => gp_detail["Latitude"],
          :address=>( address_line)
        }
      rescue Exception=>e 
        Rails.logger.info ".......Error inside .format_data_for_search_by_place_postcode  #{e.message}.................................................."
        Rails.logger.info gp_detail.inspect
      end
    end
    data
  end
  def self.format_data_for_emis_practices_response(response,options={})
    data = []
    response[:practice_list].each do |gp_detail|
      begin
        address = gp_detail["Address"] || {}
        address_line = [address['NumberStreet'], address['Village'] , address['Town'], address["County"] ].compact.join(", ") rescue nil
        title = gp_detail["Name"].gsub("- #{gp_detail["PracticeId"]}","").strip rescue gp_detail["Name"]
        data << {
          :ods_code=>gp_detail["NationalPracticeCode"],
          :id=>gp_detail["PracticeId"],
          :organization_uid=>gp_detail["NationalPracticeCode"],
          :organization_type=>"emis",
          :delinkable=>true,
          :title=>title,
          :postcode=>address["Postcode"],
          :phone_no=>(gp_detail["TelephoneNumber"] rescue nil),
          :email=> nil,
          :longitude => nil,
          :latitude => nil,
          :address=>( address_line)
        }
      rescue Exception=>e 
        Rails.logger.info ".......Error inside .format_data_for_emis_practices_response  #{e.message}.................................................."
        Rails.logger.info gp_detail.inspect
      end
    end
    data
  end

  def self.format_data_for_gp_practices_response(response)
    data = []
    response["feed"]["entry"].each do |gp_detail|
      begin
      data << {
        :ods_code=>gp_detail["content"]["organisationSummary"]["odsCode"],
        :id=>gp_detail["id"].split("/").last,
        :organization_uid=>gp_detail["id"].split("/").last,
        :organization_type=>"unknown",
        :delinkable=>false,
        :title=>gp_detail["title"],
        :postcode=>gp_detail["content"]["organisationSummary"]["address"]["postcode"],
        :phone_no=>(gp_detail["content"]["organisationSummary"]["contact"]["telephone"] rescue nil),
        :email=>(gp_detail["content"]["organisationSummary"]["contact"]["email"] rescue nil),
        :longitude =>(gp_detail["content"]["organisationSummary"]["geographicCoordinates"]["longitude"] rescue nil),
        :latitude =>(gp_detail["content"]["organisationSummary"]["geographicCoordinates"]["latitude"] rescue nil),
        :address=>(gp_detail["content"]["organisationSummary"]["address"]["addressLine"].join(", ") rescue nil)
      }
      rescue Exception=>e 
        Rails.logger.info ".........................................................."
        Rails.logger.info gp_detail.inspect
      end
    end
    data
  end

  def self.get_clinic_linkable_status(clinic_type,options={})
    status = false
    if ["emis","gpsoc"].include?(clinic_type)# == "emis"
      status = Emis::Login.new.get_clinic_linkable_status(user_details={})
    else
      status = false
    end
    status
  end

end