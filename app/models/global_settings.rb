class GlobalSettings
    EmailVerificationAfterSignup = {:allowed_time => 2.days,:allowed_time_toString => "2 days",:enable=>true }
    EmailVerificationAfterSignupForAlexa = {:allowed_time => 0.days,:allowed_time_toString => "0 days",:enable=>true }
	SupportEMail = "support@nurturey.com"
    MeasurementMin = {:cms=>{"height"=> 30, "head_circum"=>20, "waist_circum"=>25}, :inches=>{"height"=> 11, "head_circum"=>7,"waist_circum"=>9},   :kgs=>{"weight"=>0.50},:lbs=>{"weight"=>1.10}}.with_indifferent_access
    MeasurementMax = {:cms=>{"height"=> 200,"head_circum"=>75, "waist_circum"=>130}, :inches=>{"height"=> 80, "head_circum"=>30,"waist_circum"=>52},:kgs=>{"weight"=>100},:lbs=>{"weight"=>166}}.with_indifferent_access
    
    HealthCardMin = {:cms=>{"height"=> 80},  :inches=>{"height"=> 60},:kgs=>{"weight"=>30},:lbs=>{"weight"=>50}}.with_indifferent_access
    HealthCardMax = {:cms=>{"height"=> 205}, :inches=>{"height"=> 180},:kgs=>{"weight"=>250},:lbs=>{"weight"=>400}}.with_indifferent_access
    
    UserSegment   = {:trial=>2,:starter=>5,:adoptor=>15,:ambassador=>16}
    FamilySegment = {:trial=>2,:starter=>5,:adoptor=>15,:ambassador=>16}
    
    CountryListForPushNotification = ["uk","UK","gb","GB",'in',"india","INDIA","India" "IN","US","us","UK","AE","ae","nz","NZ","CA","ca","ZA","za","SG","sg"]
    # Need to update - 8 total , 2 each
    TotalMaxAllowedPush = 8
    TotalMaxAllowedEmail = 4
    DefaultMaxAllowedPushCountTypeWise = 2
    DefaultMaxAllowedEmailCountTypeWise = 2
  # MaxAllowedPushCountTypeWise = {"pointers" => 1, "action_for_home"=> 1, "prenatal tests" => 2, "timeline" =>2 , "calendar" => 2 , "measurements" =>2, "milestones"=>2 , "immunisations" =>2 , "documents"=>2, "tooth chart"=> 2, "manage tools"=>2}.with_indifferent_access
    MaxAllowedPushCountTypeWise = {"book appointment"=>2 ,"calendar"=>2, "milestones"=>2, "measurements"=>2, "immunisations"=>2, "documents"=>2, "prenatal tests"=>2, "child health card"=>2,  "pointers"=>1,"action_for_home"=> 1, "timeline"=>2, "pregnancy health card"=>2, "checkup planner"=>2, "activity planner"=>2, "tooth chart"=>2, "kick counter"=>2, "z score"=>2, "skin assessment"=>2, "mental maths"=>2, "manage tools"=>2, "manage family"=>2, "home"=> 2,"faq question"=>1, "vision health"=>2, "nhs content"=>1,"nhs library"=>1 }.with_indifferent_access
    MaxAllowedEmailCountTypeWise = {"book appointment"=>2 ,"calendar"=>2, "milestones"=>2, "measurements"=>2, "immunisations"=>2, "documents"=>2, "prenatal tests"=>2, "child health card"=>2,  "pointers"=>1,"action_for_home"=> 1, "timeline"=>2, "pregnancy health card"=>2, "checkup planner"=>2, "activity planner"=>2, "tooth chart"=>2, "kick counter"=>2, "z score"=>2, "skin assessment"=>2, "mental maths"=>2, "manage tools"=>2, "manage family"=>2, "home"=> 2,"faq question"=>1, "vision health"=>2, "nhs content"=>1,"nhs library"=>1 }.with_indifferent_access
    
    PregnacyBloodPressure = {:systolic_max=> 250.0,:systolic_min=> 60.0 ,:diastolic_max=>150.0 ,:diastolic_min=>50.0, :pulses_max=> 250.0,  :pulses_min => 30.0}.with_indifferent_access
    Hemoglobin = {:hemoglobin_min=>5.0, :hemoglobin_max=>30.0}.with_indifferent_access 
    MilestoneProgressDay = {"0-2"=>30,"2-4"=>45,"4-6"=>60,"7-9"=>75, "9-12"=>75,"13-18"=>90,"19-24"=>150,"24-36"=>210,"37-48"=>240,"49-60"=>360}.with_indifferent_access # in days
    MilestoneProgressCalculationFactors = {:scale=>10,:cf1=>0, :cf2=>10, :power=>2 }.with_indifferent_access
    ToothProgressDay = {"0-2"=>30,"2-4"=>45,"4-6"=>60,"7-9"=>75, "9-12"=>75,"13-18"=>90,"19-24"=>150,"24-36"=>210,"37-48"=>240,"49-60"=>360}.with_indifferent_access # in days
    ToothChartProgressCalculationFactors = {:scale=>10,:cf1=>0, :cf2=>10, :power=>2 }.with_indifferent_access
    
    MatchStringThresholdForAlexa = 80
    AndroidSubscriptionAppName = 'com.nurturey.app'
    AndroidSubscriptionPakageName = 'com.nurturey.app'

    EyeChartMinMax = { "spherical_min" => -10, "cylindrical_min" => -10, "axis_min" => -180, "vision_min" => 6, "spherical_max" => 10, "cylindrical_max" => 10, "axis_max" => 180, "vision_max" => 36 }
    PricingExemptionOrder= {"none"=>1, "basic"=> 2, "all"=>3}
    DefaultSubscriptionBoxData = {"title"=>"Upgrade to our Premium subscription for:", "values"=>["Unlimited practise questions for all your kids.","Access to detailed summary report of the practises"]}
    SubscriptionPopupLunchFrequency = 5
    AppReviewPopupLunchFrequency = 9 # 10th 
    PasswordValidationType = "type2"
    EmisRegistrationAllowedAgeLimit = 13
    ShowCovid19VerusUpdates = true
    MaxLoginTryCount = 3
    RetryLoginTime = 10.minutes
    AuthTokenExpireIn = 1.year
    StsSessionExpireDuration = 900 # seconds
    PointerAccessToAdminPanel = ["gcapoor@gmail.com",'anisha.sodhi1606@gmail.com']
    def self.timeout_clinic_authentication
      data = System::Settings.where(name:"timeoutClinicAuthentication").last.setting_values.with_indifferent_access rescue  {}
      data["gpsoc"]
    end

    def self.timeout_authentication
      data = System::Settings.where(name:"timeoutAuthentication").last.setting_values.with_indifferent_access rescue  {}
      data["api"]
    end


end

