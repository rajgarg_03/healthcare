class Jab
  include Mongoid::Document  
  field :name, type: String
  field :desc, type: String
  field :status, type: String , :default => "Planned"
  field :jab_type , type: String, :default => "Injection"
  field :due_on , type: Time 
  field :doc_name , type: String
  field :route, type: String
  field :manufacturer, type: String #trade name
  field :facility, type: String
  field :jab_location, type: String # site of admiistration
  field :note, type: String
  field :est_due_time, type: Time

  field :start_time, type: Time
  field :end_time, type: Time
  field :fullday, type: Boolean, :default => true
  field :system_jab_id, type: String
  field :data_source, type:String, :default =>"nurturey"  
  field :due_on_from_data_source, type:String # due on date from emis/tpp etc

  belongs_to :vaccination
  after_create :update_jab_count_to_sys_vaccin
  after_destroy :update_jab_count_to_sys_vaccin
  has_many :timelines, :as=> :timelined
  
  index ({vaccination_id:1})
   
  def data_source_due_on
    jab = self 
    jab.due_on_from_data_source.to_time rescue nil
  end

  def jab_due_on
    jab = self 
    datasource_due_on = jab.due_on_from_data_source.to_time rescue nil
    datasource_due_on || jab.due_on
  end

  def self.administered
    self.or({status:"Administered"},{:due_on_from_data_source.ne=>nil})
  end
  
  def self.not_administered_jab
    where(:status.ne=>"Administered",:due_on_from_data_source=>nil)
  end
  
   def update_jab_count_to_sys_vaccin
    begin
      sys_vaccin = SystemVaccin.find(vaccination.sys_vaccin_id)
      sys_vaccin.update_attribute(:jabs_count, vaccination.jabs.count)
    rescue Exception => e
      Rails.logger.info "Error inside update_jab_count_to_sys_vaccin ........#{e.message}"
    end
   end

   def get_jab_route(options={})
    jab  = self
    if options[:immunisation_version].to_i > 0
      SystemVaccin::CategoryMapping[jab.jab_type]
    else
      jab.route
    end
   end

   def get_jab_status
    return "Administered" if  (status == "Administered" || due_on_from_data_source.present?) 
    if due_on.present? || ( due_on.blank? && est_due_time.blank?)
      "Planned"
    else
      "Estimated"
    end
  end
  
  def get_manufacturer(vaccine,options={})
    jab = self
    jab.manufacturer || (SystemVaccin.find(vaccine.sys_vaccin_id).trade_name rescue nil)
  end
  
  # map jab with system jab
  def self.identify_and_save_system_jab_id(current_user,member,jab,vaccin,options={})
    return jab.system_jab_id if jab.system_jab_id.present?
    return nil if jab.est_due_time.blank?
    system_vaccine = SystemVaccin.find(vaccin.sys_vaccin_id)
    return nil if system_vaccine.blank?
    system_jab_id = nil
    system_vaccine.system_jabs.each do |a|
      jab_due_on = member.birth_date + eval(a.due_on) rescue nil
      if (jab.start_time.to_date == jab_due_on rescue false)
        system_jab_id = a.id 
        break
      end
    end
    if jab.system_jab_id.blank? && system_jab_id.present?
      jab.system_jab_id = system_jab_id 
      jab.save
    end
    system_jab_id
  end


   def as_json(options={})
    json = super(options)
    json["route"] = jab_type
    json["start_time"] = start_time.utc.to_formatted_s(:time) rescue nil
    json["end_time"] = end_time.utc.to_formatted_s(:time) rescue nil
   json
   end
end
