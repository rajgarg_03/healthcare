class Health
  include Mongoid::Document  
  # include Mongoid::Timestamps
  include Mongoid::Timestamps::Created 
  field :height, type: String
  field :weight, type: String
  field :head_circum, type: String 
  field :waist_circum , type: String 
  field :weight_unit, type: String, default: "kgs"
  field :height_unit, type: String, default: "cms"
  field :head_circum_unit, type: String, default: "cms"
  field :waist_circum_unit, type: String, default: "cms"
  field :bmi , type: String
  field :height_zscore, type:Float
  field :weight_zscore, type:Float
  field :bmi_zscore, type:Float
  field :last_updated_at, type:Time
  field :updated_at, type:Time
  field :data_source, type:String,:default=>"nurturey" # eg smrthi,emis,nurturey
  field :added_by, type:String,:default=>"user" # eg user/system

  HEALTH_PARAM = {"height"=>"height","weight"=>"weight","head_circum"=>"headcirc","waist_circum"=>"waistcirc","bmi"=>"bmi"}
  OverView_Param = {"height"=>"height","weight"=>"weight","bmi"=>"bmi","head_circum"=>"headcirc","waist_circum"=>"waistcirc"}
#  UNIT_VAL = {"kgs"=>1, "lbs"=>2.204, "cms"=>1 , "inc"=>0.394, "inches"=>0.394}
  UNIT_VAL = {"kgs"=>1, "lbs"=>2.20462, "cms"=>1 , "inc"=>0.39370, "inches"=>0.39370}

  HEALTH_ENTRY_PARAM = {"height"=>"height","weight"=>"weight","head_circum"=>"headcircum","waist_circum"=>"waistcircum","bmi"=>"bmi"}
  HEALTH_ENTRY_TITLE = {"height"=>"Height","weight"=>"Weight","head_circum"=> "Head CF", "headcircum"=> "Head CF","waist_circum"=>"Waist","waistcircum"=>"Waist","bmi"=>"BMI"}
  HEALTH_CHART_MIN_VAL = {"height"=>40,"weight"=>0,"head_circum"=> 30, "headcircum"=> 30,"waist_circum"=>0,"waistcircum"=>0,"bmi"=>10}
  API_HEALTH_ENTRY_TITLE = {"height"=>"Height","weight"=>"Weight","head_circum"=> "Head circumference", "headcircum"=> "Head circumference","waist_circum"=>"Waist circumference","waistcircum"=>"Waist circumference","bmi"=>"BMI"}

  Health_norecord = {"height"=>"No record","weight"=>"No record","head_circum"=>"No record","waist_circum"=>"No record","bmi"=>"No record"}

  after_create :add_point

  MetricSystem = "uk"
  

  belongs_to :member
  has_many :timelines, :as=> :timelined, :dependent => :destroy
  index ({member_id:1})
  
  def self.health_unit(option={"length_unit"=>"cms","weight_unit"=>"kgs"})
    length_unit = option["length_unit"]
    weight_unit = option["weight_unit"]
    {"height"=> length_unit, "weight"=> weight_unit, "head_circum"=> length_unit,"waist_circum"=>length_unit}
  end

  def self.health_min_max_value(option={"length_unit"=>"cms","weight_unit"=>"kgs"},&block)
    if option["length_unit"] == "cms" || option["length_unit"].blank?
     minimum_range = GlobalSettings::MeasurementMin[:cms]
     maxmimum_range = GlobalSettings::MeasurementMax[:cms]
     length_param =  {"height_min"=> minimum_range["height"], "height_max"=> maxmimum_range["height"],"head_circum_min"=>minimum_range["head_circum"], "head_circum_max"=>maxmimum_range["head_circum"],"waist_circum_min"=>minimum_range["waist_circum"],"waist_circum_max"=>maxmimum_range["waist_circum"]}
    else
      minimum_range = GlobalSettings::MeasurementMin[:inches]
      maxmimum_range = GlobalSettings::MeasurementMax[:inches]
     length_param =  {"height_min"=> minimum_range["height"], "height_max"=> maxmimum_range["height"],"head_circum_min"=>minimum_range["head_circum"], "head_circum_max"=>maxmimum_range["head_circum"],"waist_circum_min"=>minimum_range["waist_circum"],"waist_circum_max"=>maxmimum_range["waist_circum"]}
   end
    if option["weight_unit"]=="kgs" || option["weight_unit"].blank?
      minimum_range = GlobalSettings::MeasurementMin[:kgs]
      maxmimum_range = GlobalSettings::MeasurementMax[:kgs]
      weight_param = {"weight_min"=> minimum_range["weight"],"weight_max"=>maxmimum_range["weight"]}
    else
      minimum_range = GlobalSettings::MeasurementMin[:lbs]
      maxmimum_range = GlobalSettings::MeasurementMax[:lbs]
      weight_param = {"weight_min"=> minimum_range["weight"],"weight_max"=>maxmimum_range["weight"]}
    
     # weight_param = { "weight_min"=>1.10,"weight_max"=>166 }
    end
    return length_param.merge!(weight_param)

  end

  def health_setting(member_id)
    MemberSetting.create(name:"health_setting",member_id:member_id,status: true, note: "height,weight" )
  end


  def convert_to_unit(option={"length_unit"=>"to_inches","weight_unit"=>"to_lbs","data"=>nil},round_off_val= nil)
    round_off_val_status = round_off_val.present?
    round_off_val = round_off_val || 2
    params = option["data"] || self.attributes
    length_unit = option["length_unit"]
    weight_unit = option["weight_unit"]
    if length_unit == "inches" 
      params["height"] = (params["height"].to_f * UNIT_VAL["inches"]).round(round_off_val) if params["height"].present?
      params["head_circum"] = (params["head_circum"].to_f * UNIT_VAL["inches"]).round(round_off_val) if params["head_circum"].present?
      params["waist_circum"] = (params["waist_circum"].to_f * UNIT_VAL["inches"]).round(round_off_val) if params["waist_circum"].present?
    elsif length_unit == "cms" 
      params["height"] = (params["height"].to_f / UNIT_VAL["inches"]).round(round_off_val) if params["height"].present?
      params["head_circum"] = (params["head_circum"].to_f / UNIT_VAL["inches"]).round(round_off_val) if params["head_circum"].present?
      params["waist_circum"] = (params["waist_circum"].to_f / UNIT_VAL["inches"]).round(round_off_val) if params["waist_circum"].present?
    end

    if weight_unit == "lbs" 
      params["weight"] = (params["weight"].to_f * UNIT_VAL["lbs"]).round(round_off_val) if  params["weight"].present?
    elsif weight_unit == "kgs"
      params["weight"] = (params["weight"].to_f / UNIT_VAL["lbs"]).round(round_off_val) if params["weight"].present?
    end
    params["height_unit"] = length_unit
    params["head_circum_unit"] = length_unit 
    params["waist_circum_unit"] = length_unit
    params["weight_unit"] = weight_unit
    blank_val = round_off_val_status ? 0.0.round_to(round_off_val) : nil
    params["height"] = params["height"].present? ? params["height"].to_f.round_to(round_off_val) : blank_val 
    params["weight"] = params["weight"].present? ?  params["weight"].to_f.round_to(round_off_val) : blank_val  
    params["weight_zscore"] = params["weight_zscore"].present? ?  params["weight_zscore"].to_f.round_to(round_off_val) : blank_val  
    params["height_zscore"] = params["height_zscore"].present? ?  params["height_zscore"].to_f.round_to(round_off_val) : blank_val  
    params["bmi_zscore"] = params["bmi_zscore"].present? ?  params["bmi_zscore"].to_f.round_to(round_off_val) : blank_val  
    params["head_circum"] = params["head_circum"].present? ? params["head_circum"].to_f.round_to(round_off_val) : blank_val
    params["waist_circum"] = params["waist_circum"].present? ? params["waist_circum"].to_f.round_to(round_off_val) : blank_val
    params["bmi"] = params["bmi"].present? ? params["bmi"].to_f.round_to(round_off_val) : blank_val
    params["updated_at"] = params["updated_at"] if params["updated_at"].present?
    return params
  end


  def api_convert_to_unit(option={"length_unit"=>"to_inches","weight_unit"=>"to_lbs","data"=>nil},round_off_val=1)
   convert_to_unit(option,round_off_val)
  end
 
  # for detail health record
  def api_converted_data(option={"length_unit"=>"to_inches","weight_unit"=>"to_lbs","data"=>nil},round_off_val=1)
    convert_to_unit(option,round_off_val)
  end


    def self.member_health_records(member,measurement_type=nil)
    if measurement_type.present?
     where(member_id:member.id.to_s).not_in(measurement_type => ["", nil,"0.0","0"])
    else
      where(member_id:member.id.to_s)
    end
  end
  
  def self.chart_min_max_axis(data=[],who_data97=[],who_data3=[], who_month=[])
     begin
      hashed_value = Hash[data]
      sorted_who_data3= who_data3.sort rescue []
      sorted_who_data97 = who_data97.sort rescue []

      x_axis = hashed_value.keys.sort
      y_axis = (data.flatten - x_axis).sort
      min_x_axis = x_axis[0].present? && x_axis[0].to_i < who_month[0].to_i ?  x_axis[0].to_i : who_month[0].to_i
      min_y_axis = y_axis[0].present? && y_axis[0].to_f < sorted_who_data3[0].to_f ?  y_axis[0].to_f : sorted_who_data3[0].to_f

      max_x_axis = x_axis[-1].to_i > who_month[-1].to_i ?  x_axis[-1].to_i : who_month[-1].to_i
      max_y_axis = y_axis[-1].to_f > sorted_who_data97[-1].to_f ?  y_axis[-1].to_f : sorted_who_data97[-1].to_f
      {min_x:min_x_axis, max_x: max_x_axis, min_y:min_y_axis, max_y: max_y_axis}
     rescue
       {min_x:0, max_x: 0, min_y:0, max_y: 0}
     end
  end


  def calulate_bmi(weight, height)
    begin
    wgt = (weight || 0).to_f 
    hgt = (height || 0).to_f 
    hgt = (hgt/100 ) * (hgt/100)
    tbmi = ((wgt) / (hgt)) 
    tbmi.to_s == "NaN" || tbmi.to_s =="Infinity" ? 0 : tbmi.round(2) 
    rescue 
      0
    end
  end

  def self.is_update_due?(child,current_user)
    child_birth_date = child.birth_date
    status = false
    n = (Date.today - Health.where(member_id:child.id).order_by("updated_at desc").first.updated_at.to_date).ceil rescue 0
    if child_birth_date > (Date.today - 8.weeks) && n > 8
      status = true
    elsif child_birth_date < (Date.today - 8.weeks) && child_birth_date >= (Date.today - 24.month) && n > 35
      status = true
    elsif child_birth_date < (Date.today - 24.weeks) && child_birth_date >= (Date.today - 6.years) && n > 190
      status = true
    elsif child_birth_date < (Date.today - 6.years) && n > 380
      status = true
    else
      status = false
    end
    status
  end
  
  def self.convert_to_cms_kgs(params,current_user,options={})
    metric_system = options[:metric_system].present? ? options[:metric_system] :  current_user.metric_system
    health_unit_to_convert = {}
    health_unit_to_convert["length_unit"] = "cms" if metric_system["length_unit"] == "inches"
    health_unit_to_convert["weight_unit"] = "kgs" if metric_system["weight_unit"] == "lbs"
    health_unit_to_convert["data"] = params
    Health.new.convert_to_unit(health_unit_to_convert)
  end

   #member: ChildMember object, params={:health=>{}}
  def self.create_measurement_record(current_user,params,member=nil)
    member_id =  params[:health][:member_id]
    member = member || Member.find(member_id)
    changed_measurement_unit = (params[:health])
    
    user_metric_system = current_user.metric_system
    options = {:metric_system=>{}}
    options[:metric_system]["length_unit"] = params["length_unit"].present? ? params["length_unit"] : user_metric_system["length_unit"]
    options[:metric_system]["weight_unit"] = params["weight_unit"].present? ? params["weight_unit"] : user_metric_system["weight_unit"]

    last_measurement_record = member.health_records.last || Health.new
    params[:health][:updated_at] = params[:health][:updated_at] || Time.now.in_time_zone.to_date.to_s
    if ["gpsoc","emis","tpp"].include?(params[:health][:data_source])
      updated_time_in_date = ActiveSupport::TimeZone["UTC"].parse("#{params[:health][:updated_at]}").utc rescue Date.today.to_time.utc
    else
      updated_time_in_date = ActiveSupport::TimeZone[current_user.time_zone].parse("#{params[:health][:updated_at]}").utc rescue Date.today.to_time.utc
    end
    params[:health][:updated_at] = updated_time_in_date
    if params[:health][:data_source].present? && params[:health][:data_source] != "nurturey"
      record_exist =  Health.where(data_source:params[:health][:data_source], member_id:member_id,:updated_at.gte=>updated_time_in_date.to_date, :updated_at.lte=>(updated_time_in_date + 1.day).to_date).last
    else
      record_exist =  Health.where(:data_source.in=>["nurturey",""], member_id:member_id,:updated_at.gte=>updated_time_in_date.to_date, :updated_at.lte=>(updated_time_in_date + 1.day).to_date).last
    end
    blank_values = ["0","0.0",nil,""]
    params[:health] = Health.convert_to_cms_kgs(params[:health],current_user,options)
    params[:health][:last_updated_at] = DateTime.now.in_time_zone
    # Set height and weight and BMI from last record 

    height = params[:health]["height"] || member.health_records.where(:updated_at.lte=> updated_time_in_date).not_in(:height => blank_values).order_by("updated_at desc").first.height rescue 0
    weight = params[:health]["weight"] || member.health_records.where(:updated_at.lte=> updated_time_in_date).not_in(:weight =>  blank_values).order_by("updated_at desc").first.weight rescue 0
    bmi = last_measurement_record.calulate_bmi(weight ,height)
    params[:health].merge!({:bmi=> bmi})   
    
    if  record_exist
      params[:health][:height] = params[:health][:height] || record_exist.height
      params[:health][:weight] = params[:health][:weight] || record_exist.weight
      params[:health][:head_circum] = params[:health][:head_circum] || record_exist.head_circum
      params[:health][:waist_circum] = params[:health][:waist_circum] || record_exist.waist_circum
  
       record_exist.update_attributes(params[:health])
       Health.not_in(id:record_exist.id).where(member_id:member_id, :updated_at.gte=>updated_time_in_date, :updated_at.lte=>(updated_time_in_date + 1.day)).delete_all
       @health_record = record_exist.reload
    else
    #Insert record in to DB
      @health_record = Health.create(params[:health])
    end
    
    @health_record.update_attribute(:updated_at, updated_time_in_date)
    # update health settings .used for Website
    member_health_setting =  member.member_settings.where(name:"health_setting").first
    changed_measurement_unit.delete("updated_at")
    changed_measurement_unit.delete("_id")
    changed_measurement_unit.delete("member_id")
    changed_measurement_unit.delete("last_updated_at")
    member_health_setting.update_attributes({note:changed_measurement_unit.keys.join(",")}) rescue nil
    # action = changed_measurement_unit.length > 1 ?  "Add" : changed_measurement_unit.keys.last 
    #Add to Timeline
    to_timeline = {"height"=>params[:health][:height],"weight"=>params[:health][:weight]}
    timeline_at = Timeline.new.get_timeline_at(@health_record.updated_at)
    @health_record.timelines << Timeline.new({data_source:@health_record.data_source, entry_from: "health", name:bmi, desc: to_timeline.to_a.flatten.to_s, member_id:member_id,added_by: current_user.first_name +  "  " + current_user.last_name, timeline_at: timeline_at})
    # Score added by callback
    
    # Send push notification to other elder member
    if @health_record.data_source.blank? || @health_record.data_source == "nurturey"
      @health_record.send_push_to_elder_members(current_user,member)
    end
     @health_record.update_zscore(current_user,member)
    @health_record
  end


  def self.delete_measurement_unit(params,current_user)
    @health = Health.find(params[:id])
    updated_at = @health.updated_at
    if params[:type]  # check if single unit eg wiast, weight is to be deleted
      @health.update_attributes({params[:type]=> nil,last_updated_at:Time.now.in_time_zone})
      if params[:type].downcase == "height" || params[:type].downcase == "weight"
        member = @health.member
        blank_values = ["0","0.0",nil,""]
        height =   member.health_records.where(:updated_at.lte=>updated_at).not_in(:height => blank_values).order_by("updated_at desc").first.height rescue 0
        weight =   member.health_records.where(:updated_at.lte=>updated_at).not_in(:weight =>  blank_values).order_by("updated_at desc").first.weight rescue 0
        bmi = @health.calulate_bmi(weight,height)
        @health.bmi =  bmi
        @health.save
        @health.update_zscore(current_user,@health.member)
        @health.send_push_to_elder_members(current_user,member)
      end
      @health.update_attributes({:updated_at=>updated_at})
      if(@health.height.blank? && @health.weight.blank? && @health.head_circum.blank? && @health.waist_circum.blank?)
        Score.where(activity_type: "Health", activity_id:@health.id).delete
        #UserCommunication::UserActions.delete_user_action(@health,"Measurement",current_user)
        @health.update_zscore(current_user,@health.member)
        @health.delete
      end
    else # delete complete measurement db record
      Score.where(activity_type: "Health", activity_id:@health.id).delete
      Timeline.where(timelined_type: "Health", timelined_id: @health.id).delete
      #UserCommunication::UserActions.delete_user_action(@health,"Measurement",current_user)

      @health.delete
    end
    @health

  end
   
  # params = {:health=>{}}
   def self.update_measurement_record(measurement_id, params,current_user)
     health = Health.find(measurement_id)
     params[:health][:updated_at] = DateTime.now if params[:health][:updated_at].to_date == Date.today
     params[:health][:last_updated_at] = Time.now
     member = health.member
     blank_values = ["",nil,"0","0.0"]
     user_metric_system = current_user.metric_system
     
     options = {:metric_system=>{}}
     options[:metric_system]["length_unit"] = params["length_unit"].present? ? params["length_unit"] : user_metric_system["length_unit"]
     options[:metric_system]["weight_unit"] = params["weight_unit"].present? ? params["weight_unit"] : user_metric_system["weight_unit"]


     params[:health] = convert_to_cms_kgs(params[:health],current_user,options)
     params[:health]["height"] = params[:health]["height"] || health.height
     params[:health]["weight"] = params[:health]["weight"] || health.weight
     params[:health]["head_circum"] = params[:health]["head_circum"] || health.head_circum

     health_setting =  member.member_settings.where(name:"health_setting").first
        height = params[:health]["height"] || member.health_records.where(:updated_at.lte=> health.updated_at).not_in(:height => blank_values ).order_by("updated_at desc").first.height rescue 0
        weight = params[:health]["weight"] || member.health_records.where(:updated_at.lte=> health.updated_at).not_in(:weight =>  blank_values).order_by("updated_at desc").first.weight rescue 0
        bmi = health.calulate_bmi(weight,height)
        params[:health].merge!({:bmi=> bmi})
     note = params[:health].keys.join(",")
     health_setting.update_attributes({note:note})
     health.update_attributes(params[:health])

     health.update_attribute("updated_at",params[:health][:updated_at])
     timeline = Timeline.where(timelined_type: "Health", timelined_id: health.id).first || Timeline.create(entry_from:"health", timelined_type: "Health", timelined_id: health.id, timeline_at: Timeline.new.get_timeline_at(health.updated_at))
     to_timeline = {"height"=>params[:health][:height]|| height,"weight"=>params[:health][:weight] || weight}
     timeline.update_attributes({name:bmi, desc: to_timeline.to_a.flatten.to_s, timeline_at: timeline.get_timeline_at(health.updated_at)})
     health.reload
     health.update_zscore(current_user,member)
     # Send push notification to other elder member
     health.send_push_to_elder_members(current_user,member)
     
     health

  end
  
  def update_zscore(current_user,child)
    member = child
    record_updated_at = self.updated_at
    latest_measurement_record = member.health_records.order_by("updated_at desc").limit(1).first
    zscore_values  = Child::Zscore.calculate_zscore(child.id.to_s,false)  
    latest_measurement_record["height_zscore"] = zscore_values["height"]
    latest_measurement_record["weight_zscore"] = zscore_values["weight"]
    latest_measurement_record["bmi_zscore"] = zscore_values["bmi"]
    latest_measurement_record.save
    Health.delay.update_zscore_for_impacted_measurement(record_updated_at,current_user,child)
  end

  def self.update_zscore_for_impacted_measurement(measurement_updated_at,current_user,child)
    impacted_records = Health.where(member_id:child.id,:updated_at.gte=>measurement_updated_at)
    impacted_records.each do |measurement|
      # puts measurement.inspect
      zscore_values  = Child::Zscore.calculate_zscore(child.id.to_s,false,{:measurement_id=>measurement.id})  
      measurement["height_zscore"] = zscore_values["height"]
      measurement["weight_zscore"] = zscore_values["weight"]
      measurement["bmi_zscore"] = zscore_values["bmi"]
      measurement.save
    end
  end


  def send_push_to_elder_members(current_user,child)
    push_identifier = 11
    measurement_record = self
    msg =   "#{current_user.first_name} has updated measurement records for #{child.first_name}. Click to check"
    current_user.push_user_action_notification(push_identifier,measurement_record,msg,child)
  end
  
   def get_unit_converted_record(current_user,round_off_val=1)
      user_measurement_matric = current_user.metric_system
      measurement_units = {}
      measurement_units["length_unit"] =  "inches" if user_measurement_matric["length_unit"] == "inches"        
      measurement_units["weight_unit"] =  "lbs" if user_measurement_matric["weight_unit"] == "lbs"   
     convert_to_unit(measurement_units,round_off_val)
   end

   def self.get_latest_height_weight(current_user,child)
     values = ["", nil,"0","0.0"]
     last_height = child.health_records.not_in(:height => values).order_by("updated_at desc").first["height"] rescue nil
     last_weight = child.health_records.not_in(:weight => values).order_by("updated_at desc").first["weight"] rescue nil
     last_head_circum = child.health_records.not_in(:head_circum => values).order_by("updated_at desc").first["head_circum"] rescue nil
     last_bmi =    child.health_records.last.bmi rescue nil   
     measurement = Health.new(:height=>last_height, :weight=>last_weight, :bmi=>last_bmi,:head_circum=>last_head_circum)
     return measurement
   end 
  
  def self.member_measurement_graph_data(member,current_user,round_off_val=1,options={})
    chart_data = {}
    measurement_units = get_measurement_units(current_user)
    units = {"height"=>(measurement_units["length_unit"] || "cms"),
        "weight"=> (measurement_units["weight_unit"] || "kgs"),
        "bmi"=>""}
    if options[:graph_vesion].to_i > 1      
      data_identifier = ["bmi"]
    else
      data_identifier =  ["height","weight","bmi"] 
    end
    data_identifier.each do |health_parameter|
      health_record =   Health.member_health_records(member,health_parameter).order_by("updated_at desc").first
      data = Health.get_chart_data(member,current_user,health_parameter,round_off_val)
      health_record.api_convert_to_unit(measurement_units,round_off_val) rescue nil
      data["title"] = "#{health_record[health_parameter]} #{units[health_parameter]}" rescue nil
      updated_at_value = ApplicationController.helpers.calculate_age4(Date.today,health_record.updated_at,level=1) rescue nil
      if updated_at_value.blank?
        data["updated_at_text"] = nil
      else
        if updated_at_value == "today"
          data["updated_at_text"] = "Updated #{updated_at_value}" rescue nil
        else
          data["updated_at_text"] = "Updated #{updated_at_value} ago" rescue nil
        end
      end
      chart_data[health_parameter] =  data #if data[:chart_data].present?
    end
    chart_data
  end
  
  def self.get_measurement_units(current_user)
    user_measurement_metric = current_user.metric_system
    measurement_units = {}
    measurement_units["length_unit"] =  "inches" if user_measurement_metric["length_unit"] == "inches" #? "inches" : "cms"      
    measurement_units["weight_unit"] =  "lbs" if user_measurement_metric["weight_unit"] == "lbs"  #? "lbs" : "kgs"
    measurement_units
  end

  def self.get_chart_data(member,current_user,health_parameter,round_off_val=1)
    health_unit = Health.health_unit(current_user.metric_system)
    chart_unit = health_unit[health_parameter]
    chart_data = Health.member_health_records(member,health_parameter).order_by("updated_at desc").collect{|record| [get_month_in_float(member,record.updated_at.to_date),convert_to_unit(record[health_parameter],chart_unit,round_off_val)]}
      who_data = measurementWHOData(current_user,health_parameter,member)
      data = {
        :chart_data=>chart_data,
        :chart_min_max_axis=> Health.chart_min_max_axis(chart_data,who_data[:p97],who_data[:p3],who_data[:month]) ,
        :who_data =>{:p3=>who_data[:p3], :p15=>who_data[:p15],:p85=>who_data[:p85], :p97=>who_data[:p97],:month=>who_data[:month] 
          } }
   end
  
  def self.measurementWHOData(current_user,health_parameter,member)
    gender = member.member_gender
    data = System::MeasurementWho.get_who_data(current_user,health_parameter, gender)
    who_data = {}
    age = get_month_in_float(member,(Date.today) ) + 12
    data = data.entries
    who_data[:p3] = data.map(&:P3)[0..age]
    who_data[:p15] = data.map(&:P15)[0..age]
    who_data[:p85] = data.map(&:P85)[0..age]
    who_data[:p97] = data.map(&:P97)[0..age]
    who_data[:month] = data.map(&:month)[0..age]
    who_data
  end
  def self.get_month_in_float(member,date_obj,round_off_val=nil)
    begin
      d1 = member.birth_date.to_date
      d2 = date_obj
      months = (d2 - d1).fdiv(30)
      if round_off_val.blank?
        months = months.to_i
      else
        months = months.round(round_off_val)
      end
      months
    rescue
      0
    end
  end

  def self.convert_to_unit(rec_val,health_unit,round_off_val=1)
    (rec_val.to_f * Health::UNIT_VAL[health_unit]).to_f.round(round_off_val) rescue rec_val.to_f.round(round_off_val)
  end
  # def convert_to_cms_kgs(params)
  #    Health.convert_to_cms_kgs(params,current_user)
  #  end


  private
  def add_point
    updated_attributes = {"height" => height, "weight"=> weight, "waist_circum"=> waist_circum, "head_circum"=> head_circum}.delete_if { |k, v| v.nil? }
    family_id = FamilyMember.where(member_id: self.member_id).first.family_id
    name = Health::HEALTH_ENTRY_TITLE.values_at(*updated_attributes.keys).to_sentence rescue ""
    Score.create(family_id: family_id, activity_type: "Health", activity_id: self.id, activity: "Added  " + name, point: Score::Points["Health"], member_id: self.member_id)
  end
   
end
