class Nhs::NhsContentManager

  def self.format_and_save_nhs_article_data(start_date,end_date,order,page,keywords_to_match,search_type, save_to_db=false) #order can be newest or oldest
    begin
      # keywords_to_match = ["health","exercise","pregnency","women", "higher risk","keto diet"]
      data = Nhs::NhsApi.get_news_data(start_date,end_date,page,order)
      formatted_content = []

      significant_links = data['significantLink']

      significant_links.each do |significant_link|

        json_string = significant_link.to_json.downcase
        matched_kewords = Utility.get_keywords_exists_in_string(keywords_to_match,json_string)
        
        #check if any keyword matched.
        if matched_kewords.length > 0  
          formatted_content.push(significant_link)
          if save_to_db 
            Nhs::NhsData.add_article_content(significant_link,search_type,matched_kewords)
          end
        end

      end

      return {:complete_data=>data, :filtered_data=>formatted_content}

    rescue Exception=>e
        Rails.logger.info e.message
    end

  end



def self.format_and_save_media_data(start_date,end_date,page,keywords_to_match,search_type,save_to_db=false) #order can be newest or oldest

  begin
      data = Nhs::NhsApi.get_media_data(start_date,end_date,page)
      formatted_content = [] 

      medias = data['video']

      medias.each do |media|

        json_string = media.to_json.downcase
        matched_kewords = Utility.get_keywords_exists_in_string(keywords_to_match,json_string)
        
        #check if any keyword matched.
        if matched_kewords.length > 0 
          formatted_content.push(media)
          if save_to_db
            Nhs::NhsData.add_media_content(media,search_type,matched_kewords)
          end
        end

      end

      return {:complete_data=>data, :filtered_data=>formatted_content}

    rescue Exception=>e
        Rails.logger.info e.message
    end
  end

  # #for testing on console run Nhs::NhsContentManager.save_news_between_dates("2017-01-01","2018-12-31")
  # def self.save_news_between_dates(start_date,end_date)
  #   page = 1
  #   order = "newest"
  #   keywords_to_match = System::Scenario::ScenarioDetails.get_all_keywords
  #   total_pages = 1
  #   data = nil
  #   search_type = "article"
  #   begin
  #     Rails.logger.info( "start_date" => start_date, "end_date" => end_date, "page" => page) 
  #     data = Nhs::NhsApi.get_news_data(start_date,end_date,page,order)
  #   rescue Exception=>e
  #       Rails.logger.info e.message
  #   end
    
  #   #fetch page count
  #   related_links = data['relatedLink']
  #   related_links.each do |related_link|
  #     if related_link["name"].downcase.eql? "last page"
  #       url_params  = Rack::Utils.parse_query URI(related_link["url"]).query
  #       total_pages = url_params["page"].to_i
  #       # total_pages = related_link["url"].split('=').last.to_i
  #       break
  #     end
  #   end

  #   #iterate all pages and save content to db
  #   loop do 
  #       if page > 1
  #         #this is done because server not responding more than 10 queries at a time. 5 is taken to be on the safer side.
  #         if page % 5 == 0
  #           sleep 10
  #         end
          
  #         begin 
  #           Rails.logger.info( "start_date" => start_date, "end_date" => end_date, "page" => page) 
  #           data = Nhs::NhsApi.get_news_data(start_date,end_date,page,order)
  #         rescue  Exception=>e
  #           Rails.logger.info e.message
  #         end
  #       end

  #       significant_links = data['significantLink']
        
  #       unless significant_links.blank?
  #         significant_links.each do |significant_link|
  #           json_string = significant_link.to_json.downcase
  #           matched_kewords = Utility.get_keywords_exists_in_string(keywords_to_match,json_string)

  #           if matched_kewords.length > 0  
  #             Nhs::NhsData.add_article_content(significant_link,search_type,matched_kewords)
  #           end
  #         end
  #       end
  #     page = page + 1   
  #     break if page > total_pages
  #   end 
  # end

  #for testing on console run Nhs::NhsContentManager.save_news_between_dates("2017-01-01","2018-12-31")
  def self.save_articles_data(start_date,end_date)
    page = 1
    order = "newest"
    keywords_to_match = System::Scenario::ScenarioDetails.get_all_keywords#System::Scenario::ScenarioDetails.pluck(:keyword).flatten.compact.map(&:downcase).uniq
    total_pages = 1
    data = nil
    # search_type = "article"
    begin
      Rails.logger.info( "start_date" => start_date, "end_date" => end_date, "page" => page) 
      data = Nhs::NhsApi.get_news_data(start_date,end_date,page,order)
    rescue Exception=>e
        Rails.logger.info e.message
    end
    Nhs::NhsContentManager.save_articles_in_db(data,keywords_to_match)
    #fetch page count
    related_links = data['relatedLink']
    related_links.each do |related_link|
      if related_link["name"].downcase.eql? "last page"
        url_params  = Rack::Utils.parse_query URI(related_link["url"]).query
        total_pages = url_params["page"].to_i
        # total_pages = related_link["url"].split('=').last.to_i
        break
      end
    end

    (2..total_pages).each do |page|

        if page % 5 == 0
          sleep 30
        end
        data = Nhs::NhsApi.get_news_data(start_date,end_date,page,order)
        Nhs::NhsContentManager.save_articles_in_db(data,keywords_to_match)

    end
  end

  def self.save_articles_in_db(data,keywords_to_match)
    significant_links = data['significantLink']
    search_type = "article"
        
      unless significant_links.blank?
        significant_links.each do |significant_link|
          json_string = significant_link.to_json.downcase
          matched_kewords = Utility.get_keywords_exists_in_string(keywords_to_match,json_string)

          if matched_kewords.length > 0  
            Nhs::NhsData.add_article_content(significant_link,search_type,matched_kewords)
          end
        end
      end
  end

  #for testing on console run Nhs::NhsContentManager.save_media_between_dates("2017-01-01","2018-12-31")
  # def self.save_media_between_dates(start_date,end_date)
  #   page = 1
  #   keywords_to_match = System::Scenario::ScenarioDetails.pluck(:keyword).flatten.compact.map(&:downcase).uniq
  #   total_pages = 1
  #   data = nil
  #   search_type = "media"
  #   begin
  #     Rails.logger.info( "start_date" => start_date, "end_date" => end_date, "page" => page) 
  #     data = Nhs::NhsApi.get_media_data(start_date,end_date,page)
  #   rescue Exception=>e
  #       Rails.logger.info e.message
  #   end
    
  #   #fetch page count
  #   related_links = data['relatedLink']
  #   related_links.each do |related_link|
  #     if related_link["name"].downcase.eql? "last page"
  #       url_params  = Rack::Utils.parse_query URI(related_link["url"]).query
  #       total_pages = url_params["page"].to_i
  #       # total_pages = related_link["url"].split('=').last.to_i
  #       break
  #     end
  #   end

  #   #iterate all pages and save content to db
  #   loop do 
  #       if page > 1
  #         #this is done because server not responding more than 10 queries at a time. 5 is taken to be on the safer side.
  #         if page % 5 == 0
  #           sleep 30
  #         end

  #         begin 
  #           Rails.logger.info( "start_date" => start_date, "end_date" => end_date, "page" => page) 
  #           data = Nhs::NhsApi.get_media_data(start_date,end_date,page)
  #         rescue  Exception=>e
  #           Rails.logger.info e.message
  #         end
  #       end

  #       medias = data['video']
        
  #       unless medias.blank?
  #         medias.each do |media|
  #           json_string = media.to_json.downcase
  #           matched_kewords = Utility.get_keywords_exists_in_string(keywords_to_match,json_string)

  #           #check if any keyword matched.
  #           if matched_kewords.length > 0 
  #             Nhs::NhsData.add_media_content(media,search_type,matched_kewords)
  #           end
  #         end
  #       end
  #     page = page + 1   
  #     break if page > total_pages
  #   end 
  # end


  #for testing on console run Nhs::NhsContentManager.save_media_data("2017-01-01","2018-12-31")
  def self.save_media_data(start_date,end_date)
    page = 1
    # keywords_to_match = System::Scenario::ScenarioDetails.pluck(:keyword).flatten.compact.map(&:downcase).uniq
    keywords_to_match = []
    total_pages = 1
    data = nil
    # search_type = "media"
    begin
      Rails.logger.info( "start_date" => start_date, "end_date" => end_date, "page" => page) 
      data = Nhs::NhsApi.get_media_data(start_date,end_date,page)
    rescue Exception=>e
        Rails.logger.info e.message
    end
    #fetch page count
    related_links = data['relatedLink']
    related_links.each do |related_link|
      if related_link["name"].downcase.eql? "last page"
        url_params  = Rack::Utils.parse_query URI(related_link["url"]).query
        total_pages = url_params["page"].to_i
        # total_pages = related_link["url"].split('=').last.to_i
        break
      end
    end

    Nhs::NhsContentManager.save_media_in_db(data)
    interval = 30
    seconds_delay = 1
    content_manager_obj = Nhs::NhsContentManager.new
    #iterate all pages and save content to db
    (2..total_pages).each do |page|
      if page % 5 == 0
        seconds_delay = seconds_delay + interval
      end
      ParentMember.first.delay(run_at: seconds_delay.seconds.from_now).fetch_and_save_nhs_media(start_date,end_date,page)
    end
  end


  def fetch_and_save_media(start_date, end_date,page)
    begin
      data = Nhs::NhsApi.get_media_data(start_date,end_date,page)
      Nhs::NhsContentManager.save_media_in_db(data)
    rescue Exception=>e
      Rails.logger.info e.message
    end
  end


  def self.save_media_in_db(data)
    medias = data['video']
    search_type = "media"
    unless medias.blank?
      medias.each do |media|
        Nhs::NhsData.add_media_content(media,search_type,[])
      end
    end
  end

end