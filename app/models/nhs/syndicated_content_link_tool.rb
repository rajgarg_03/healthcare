class Nhs::SyndicatedContentLinkTool
  include Mongoid::Document  
  include Mongoid::Timestamps
  # field :status,                                  :type=>  String, :default=>"Active"
  field :tool_type ,                                :type=>  String # eg "question/pointer/faq/" 
  field :tool_subtype ,                             :type=>  String # eg "question/pointer/topic/" 
  field :linked_tool_object_id,                     :type=>  String # Objectid of linked class
  field :linked_tool_class,                         :type=>  String # Linked class name eg Member/ArticleRef
  field :nhs_syndicated_content_custom_id,          :type=>  Integer 
  field :notification_expiry,                       :type=>  String
  

  index({linked_tool_class:1})
  index({linked_tool_object_id:1})
  index({nhs_syndicated_content_custom_id:1})
  
  def self.tool_list_for_linking
    data = []
    ::System::Template.all.map(&:mapped_table).each do |class_name|
      data << [class_name,class_name]
    end
    data <<  ["System::Faq::Question","System::Faq::Question"]
    data <<  ["System::Faq::SubTopic","System::Faq::Topic","System::Faq::SubTopic","System::Faq::Topic"]
    System::Template::ClassForContentMapping.each do |k,v|
      data << [v,k]
    end
    data
  end

  def self.action_card_list_for_linking
    data = ["::Nhs::GuideTiles","::Nhs::GuideTiles"]
    data
  end

  def get_nhs_content
    return nil if self.nhs_syndicated_content_custom_id.blank?
    ::Nhs::SyndicatedContentLinkTool.where(:nhs_syndicated_content_custom_id=>self.nhs_syndicated_content_custom_id).last    
  end


  def get_tool
    eval(self.linked_tool_class).find(linked_tool_object_id) rescue nil
  end



  
  def self.add_to_list(tool_object,nhs_syndicated_content_custom_id)
    tool = tool_object
    tool_class = tool.class.to_s

    tool_type,tool_subtype = ::Nhs::SyndicatedContentLinkTool.get_tool_type_and_subtype(tool_class)
    if nhs_syndicated_content_custom_id.blank?
      ::Nhs::SyndicatedContentLinkTool.where(linked_tool_object_id:tool.id, linked_tool_class:tool_class).delete_all
    else
      linked_content = ::Nhs::SyndicatedContentLinkTool.get_nhs_content_by_linked_tool(tool,nhs_syndicated_content_custom_id)
       if !linked_content.present?
        linked_content = ::Nhs::SyndicatedContentLinkTool.create(:nhs_syndicated_content_custom_id=>nhs_syndicated_content_custom_id,tool_subtype:tool_subtype,tool_type:tool_type,linked_tool_object_id:tool.id,linked_tool_class:tool_class)
       end
    end
  end


  def self.add_tool_to_list(tool_class,nhs_syndicated_content_custom_id)
    tool_type,tool_subtype = ::Nhs::SyndicatedContentLinkTool.get_tool_type_and_subtype(tool_class)
    linked_content = ::Nhs::SyndicatedContentLinkTool.where(:nhs_syndicated_content_custom_id=>nhs_syndicated_content_custom_id,linked_tool_class:tool_class).last
    if !linked_content.present?
      linked_content = ::Nhs::SyndicatedContentLinkTool.create(:nhs_syndicated_content_custom_id=>nhs_syndicated_content_custom_id,tool_subtype:tool_subtype,tool_type:tool_type,linked_tool_class:tool_class)
    end
  end

  def self.get_nhs_content_by_linked_tool(tool,nhs_syndicated_content_custom_id,options={})
    tool_class = tool.class.to_s
    linked_content = ::Nhs::SyndicatedContentLinkTool.where(:nhs_syndicated_content_custom_id=>nhs_syndicated_content_custom_id,linked_tool_object_id:tool.id,linked_tool_class:tool_class).last
  end


  def self.nhs_content_list_linked_with_tool(tool,options={})
    # Only for "UK"country 
    return nil if options[:country_code].present? && !options[:country_code].include?("UK")
    if tool.present?
      tool_class = tool.class.to_s
      content_custom_ids = ::Nhs::SyndicatedContentLinkTool.where(linked_tool_object_id:tool.id,linked_tool_class:tool_class).only(:nhs_syndicated_content_custom_id).pluck(:nhs_syndicated_content_custom_id)
      ::Nhs::SyndicatedContent.where(:custom_id.in=>content_custom_ids).order_by("order_id asc")
    else options[:tool_class].present?
       tool_class =  options[:tool_class]
      content_custom_ids = ::Nhs::SyndicatedContentLinkTool.where(linked_tool_class:tool_class).only(:nhs_syndicated_content_custom_id).pluck(:nhs_syndicated_content_custom_id)
      ::Nhs::SyndicatedContent.where(:custom_id.in=>content_custom_ids).order_by("order_id asc")
    end
  end
  
  def get_title
    syndicated_linked_tool = self 
    if syndicated_linked_tool.linked_tool_object_id.present?
      tool = eval(syndicated_linked_tool.linked_tool_class).find(syndicated_linked_tool.linked_tool_object_id)
      searchable_column = ::System::Template.get_serach_column_for_mapped_table(syndicated_linked_tool.linked_tool_class)
     
      "#{searchable_column[0]}:#{tool[searchable_column[0]]},#{searchable_column[1]}:#{tool[searchable_column[1]]}"
    else
      System::Template::ClassForContentMapping[syndicated_linked_tool.linked_tool_class]
    end
  end
  
  def get_syndicated_content_title
    syndicated_linked_tool = self 
    content = Nhs::SyndicatedContent.where(custom_id:syndicated_linked_tool.nhs_syndicated_content_custom_id).last
    "Title:#{content.title}, Description:#{content.description}"
  end
  

 def get_country_code
    syndicated_linked_tool = self 
    if syndicated_linked_tool.linked_tool_object_id.present?
      tool = eval(syndicated_linked_tool.linked_tool_class).find(syndicated_linked_tool.linked_tool_object_id)
      country_column = ::System::Template.get_country_column_for_mapped_table(syndicated_linked_tool.linked_tool_class)
      tool[country_column]
    else
      ""
    end
  end


  def self.get_tool_type_and_subtype(tool_class)
    
    if tool_class== "System::Faq::Question"
      ["faq","question"]  
    else
      [tool_class,nil]  
    end
  end

  
  
end