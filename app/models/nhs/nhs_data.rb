class Nhs::NhsData

  include Mongoid::Document  
  include Mongoid::Timestamps 
  field :keywords, type: Array, :default=>[] #keywords passed by admin
  field :link, type: String 
  field :search_type, type: String  #article, media
  field :nhs_keywords, type: String #keyword found in nhs api response
  field :description, type:String
  field :link_relationship, type:String
  field :link_name, type:String
  field :date_added, type:Time
  field :date_modified, type:Time

  #fields specifically for videos
  field :thumbnail_url, type:String
  field :duration, type:String
  field :last_reviewed, type: Array, :default=>[]

  validates_uniqueness_of :link

  def self.add_article_content(significant_link,search_type,matched_kewords)
         #check if any keyword matched.
          nhs_data = Nhs::NhsData.new()
          nhs_data.keywords = matched_kewords
          nhs_data.link = significant_link["url"]
          nhs_data.search_type = search_type
          nhs_data.nhs_keywords = significant_link["mainEntityOfPage"]["keywords"]
          nhs_data.description =  significant_link["description"]
          nhs_data.link_relationship =  significant_link["linkRelationship"]
          nhs_data.link_name =  significant_link["name"]

          # need to confirm is it the right way to add the dates.
          nhs_data.date_added =   (significant_link["mainEntityOfPage"]["datePublished"]).to_time
          nhs_data.date_modified =    (significant_link["mainEntityOfPage"]["dateModified"]).to_time
          nhs_data.save
  end

  def self.add_media_content(media,search_type,matched_kewords)
         #check if any keyword matched.
          nhs_data = Nhs::NhsData.new()
          nhs_data.keywords = matched_kewords

          media_url = media["embedUrl"]
          nhs_data.link = media_url[0..1] == "//" ? media_url.slice(2,media_url.length-2) : media_url
          nhs_data.search_type = search_type
          nhs_data.nhs_keywords = media["keywords"]
          nhs_data.description =  media["description"]
          nhs_data.duration =  media["duration"]
          nhs_data.link_name =  media["name"]
          nhs_data.thumbnail_url =  media["thumbnailUrl"]

          # need to confirm is it the right way to add the dates. 
          nhs_data.date_added =   media["uploadDate"] == nil ? "" : (media["uploadDate"]).to_time
          nhs_data.date_modified = media["dateModified"] == nil ? "" :  (media["dateModified"]).to_time
          nhs_data.last_reviewed =    media["lastReviewed"]
          nhs_data.save
  end

end