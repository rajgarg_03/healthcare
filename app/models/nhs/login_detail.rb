class Nhs::LoginDetail
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :user_id,                       :type=> String
  field :refresh_token,                 :type=> String # Non expiry token use to generate new token

  def self.save_access_token(user,refresh_token,options={})
    begin
      if user.blank?
        Rails.logger.info "No user found to save token for NHS login"
        return nil 
      end
      # Update user nhs login count
      user["nhs_login_count"] = user["nhs_login_count"].to_i + 1
      user.save(validate:false)
      
      Rails.logger.info "Saving token for NHS login"
      nhs_login_detail = Nhs::LoginDetail.where(user_id:user.id).last
      if nhs_login_detail.present?
        nhs_login_detail.refresh_token = refresh_token
        nhs_login_detail.save!
      else
        Nhs::LoginDetail.new(user_id:user.id,refresh_token:refresh_token).save!
      end
    rescue Exception=>e 
      Rails.logger.info "Error inside Nhs LoginDetail save_access_token..#{user.id}.........#{refresh_token}......#{e.message}"
    end
  end
end