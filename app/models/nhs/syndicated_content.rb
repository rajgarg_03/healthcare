class Nhs::SyndicatedContent
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :custom_id,                     :type=> Integer #auto increamented value for  ScenarioDetails.eg 1,2,3 etc will used for backtracking
  field :description,                  :type=> String # name
  field :title,                         :type=> String # name
  field :category,                      :type=> Array # 
  field :status,                        :type=> String, :default=>"Active"
  field :api_link ,                     :type=> String
  field :web_url ,                      :type=> String
  field :notification_expiry,           :type=> String
  field :content_topic_name,            :type=> String # /condition/news/health 
  field :content,                       :type=> Hash
  field :added_by,                      :type=> String, default:"user"  # system/user
  field :genre,                         :type=> String # Condition/Common Health Questions
  field :order_id,                      :type=> Integer # Used to sort by order

  after_create :assign_custom_id
  
  index ({custom_id: 1})
  index ({status: 1})
  index ({category: 1})
  index ({title: 1})
  index ({status: 1})
  index ({order_id: 1})
  
  DATATABLE_COLUMNS = [:custom_id,:status,:title, :category, :genre, :api_link, :notification_expiry]
  
  def topic
    self.title
  end
  def self.set_order_id
    Nhs::SyndicatedContent.all.each do |a|
      a.order_id = a.custom_id
      a.save
    end
  end

  def self.search_title(keyword)
    where(title:/#{keyword}/i)
  end

  # Nhs::SyndicatedContent.where(title:/keyword/i)
  def self.valid(current_user,member_id,options={})
    where(status:"Active",:added_by.in=>["user",nil])
  end

  # Nhs::SyndicatedContent.get_category_list(current_user,member_id,options={})
  def self.get_category_list(current_user,member_id,options={})
    category_list = ::Nhs::SyndicatedContent.valid(current_user,member_id,options).order_by("order_id asc").pluck(:category).compact.flatten.map(&:capitalize).uniq
    # sorted_order = ["Pregnancy","Infant (0 - 12 months)","Toddler (1 - 3 years)","Early childhood (3 years and above)"]   
    # category_list = sorted_order & category_list
    # category_list = category_list.sort_by{|a| sorted_order.index(a)|| 999999}
    data = []
    category_list.each do |category|
      topic_list = ::Nhs::SyndicatedContent.get_category_topic_list(current_user,member_id,category,options)
      data << {category:category,topic_count:topic_list.count, topic_list:topic_list}
    end
    data
  end

  def self.get_category_topic_list(current_user,member_id,category,options={})
    topic_list = ::Nhs::SyndicatedContent.valid(current_user,member_id,options).where(:category=>/^#{Regexp.escape(category)}$/i).only(:genre,:custom_id,:order_id,:title,:category,:api_link,:content_topic_name).order_by("order_id asc")
    data = topic_list.all.map{|a| a.syndicated_format_data(current_user,member_id,options) }
   data
  end

  # Used for Data table 
  def self.datatable_filter(search_value, search_columns)
    return all if search_value.blank?
    result = ::Nhs::SyndicatedContent
    filter = []
     
    search_columns.each do |key, value|
      if value['data'] == "custom_id"
       filter << {"#{value['data']}"=>search_value} if value['searchable']
      else
       filter << {"#{value['data']}"=>/#{Regexp.escape(search_value)}/i} if value['searchable']
      end
    end
    result = result.where("$or"=>filter)

    result
  end  
  
  def self.datatable_order(order_column_index, order_dir)
    order_by("#{System::Scenario::ScenarioDetails::DATATABLE_COLUMNS[order_column_index]} #{order_dir}")
  end

  def scenario_detail
    syndicated_content = self
    System::Scenario::ScenarioLinkedToTool.where(tool_class:syndicated_content.class.to_s,linked_tool_id:syndicated_content.id.to_s).last.scenario_detail rescue nil

  end

  def update_scenario(system_scenario_custom_id)
    syndicated_content = self
    System::Scenario::ScenarioLinkedToTool.add_to_list(syndicated_content,system_scenario_custom_id)
  end
  
  def self.get_suggested_syndicated_content(current_user,member_id,options={})
    children_ids = member_id.blank? ?  current_user.children_ids : [member_id]
    scenario_linked_to_tool_ids = ::Child::Scenario.where(:category=>"pregnancy", :linked_tool_class=> "Nhs::SyndicatedContent", :member_id.in=>children_ids).order_by("effective_score desc")
    if options[:suggested_content_limit].present?
      scenario_linked_to_tool_ids = scenario_linked_to_tool_ids.limit((options[:suggested_content_limit]||10).to_i)
    end
    nhs_content_ids =  scenario_linked_to_tool_ids.pluck(:linked_tool_id).compact
    if nhs_content_ids.blank?
      scenario_linked_to_tool_ids =  scenario_linked_to_tool_ids.pluck(:scenario_linked_to_tool_id)
      nhs_content_ids = ::System::Scenario::ScenarioLinkedToTool.where(:id.in=>scenario_linked_to_tool_ids).pluck(:linked_tool_id)
    end
    options[:without_content] = true
    data = ::Nhs::SyndicatedContent.where(:id.in=>nhs_content_ids).map{|a| a.syndicated_format_data(current_user,member_id,options) }
    data
  end
 

  def assign_custom_id
    syndicated_content  = self
    syndicated_content[:custom_id] = ::Nhs::SyndicatedContent.max(:custom_id).to_i + 1
    syndicated_content.save
  end


  def syndicated_format_data(current_user,member_id,options={})
    syndicated_content = self
    if options[:without_content] == true
      data = syndicated_content.attributes.except("content")
    else
      data = syndicated_content.attributes
    end
    data
  end

  def fetch_content
    syndicated_content = self
    options={}
    url = syndicated_content.api_link.to_s.strip
    custom_id = syndicated_content.custom_id
    option[:genre] = syndicated_content.genre
    data,status = ::Nhs::NhsApi.get_nhs_syndicated_data(url,options)
    if data.present?
      syndicated_content.content = data
      syndicated_content.save
    end
  end

  def self.format_content(content,options={})
    title = content["name"]
    api_link = options[:api_link].present? ? options[:api_link] :  content["api_link"]
    if options[:web_url].present?
      web_url = options[:web_url]
    else
      web_url = api_link.gsub("api.nhs.uk", "www.nhs.uk") rescue "https://www.nhs.uk"
    end
    description = content["description"]
    genre =  content["genre"]
    html_text = []
    if genre == "Common Health Questions"
      content["mainEntityOfPage"].each do |page|
        html_text << page["text"]
        if page['acceptedAnswer'].present?
          page['acceptedAnswer'].each do |section|
            section["mainEntity"].each do |main_entity|
              main_entity["mainEntityOfPage"].each do |main_entity_page|
                html_text << main_entity_page["text"]
              end
            end
          end
        end
      end
    else
      content["mainEntityOfPage"].each do |page|
        # html_text << page["text"]
        if page['mainEntityOfPage'].present?
          page['mainEntityOfPage'].each do |section|
            html_text << section["text"]
          end
        elsif page["text"].present?
            html_text << page["text"]
        end   
      end
    end
    html_str = html_text.join("\n")
    {html_str:html_str,title:title,description:description,genre:genre,url:web_url}
  end

  def self.get_content(url,options={})
    syndicated_content = ::Nhs::SyndicatedContent.where(api_link:url).last
    
    if syndicated_content.present? && syndicated_content.content.present?
      data  = syndicated_content.content
      data["api_link"] = syndicated_content.web_url.blank? ? url : syndicated_content.web_url
      return data
    end
    options[:genre] = syndicated_content.genre rescue ""
    data, status = ::Nhs::NhsApi.get_nhs_syndicated_data(url,options)
    if status == 200
      if syndicated_content.present? 
        syndicated_content.content = data
        syndicated_content.save
      else
       # save new ecord
      end
    end
    data["api_link"] = (syndicated_content.present?  && syndicated_content.web_url.present?) ?  syndicated_content.web_url : url
    data
  end
  
  
end