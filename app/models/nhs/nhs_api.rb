class Nhs::NhsApi
  def self.get_search_data(query)
    # curl -X GET https://api.nhs.uk/search/?subscription_key=85a9991cd68f43c587fa52a7a929624c&query=eye
    sub_key = APP_CONFIG['nhs_subscription_key']
    begin
      query = CGI::escape(query) rescue ""
      @data = JSON.parse(`curl -G https://api.nhs.uk/search/ -d subscription-key=#{sub_key} -d query=#{query}`)
      return @data
    rescue Exception=>e
      Rails.logger.info e.message
     return e.message
    end
  end

  def self.get_news_data(start_date,end_date,page,order)
    sub_key = APP_CONFIG['nhs_subscription_key']
    begin
      @data = JSON.parse(`curl -G https://api.nhs.uk/news/ -d subscription-key=#{sub_key} -d startDate=#{start_date} -d endDate=#{end_date} -d order=#{order} -d page=#{page}`)
      return @data
    rescue Exception=>e
      Rails.logger.info e.message
    end
  end

  def self.get_media_data(start_date,end_date,page)
    sub_key = APP_CONFIG['nhs_subscription_key']
    begin
      @data = JSON.parse(`curl -G https://api.nhs.uk/video/ -d subscription-key=#{sub_key} -d startDate=#{start_date} -d endDate=#{end_date} -d page=#{page}`)
      return @data
    rescue Exception=>e
      Rails.logger.info e.message
    end
  end


  # Nhs::NhsApi.get_clinic_by_ods_code(current_user,current_user.id,"B86095",options={})    
  def self.get_clinic_by_ods_code(current_user,member_id,ods_code,options={})
    data = nil
    data = ::Clinic::ClinicDetail.where(ods_code:ods_code).last
    if data.present?
      organization_obj = Clinic::Organization.new(data.attributes)
      organization_obj.uid = data.organization_uid
      return organization_obj 
    end
    uri = URI('https://api.nhs.uk/service-search/search?api-version=1')
    http = Net::HTTP.new(uri.host, uri.port)
    if uri.scheme == 'https'
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_PEER
    end    
    header = {'subscription-key'=>APP_CONFIG['nhs_subscription_key'], 'Content-Type'=>'application/json'}
    if Rails.env != "production"
      ods_code1 = "G84035"
      request_body = {"filter": "NACSCode eq '#{ods_code1}'", "top": 25, "skip": 0,"count": true }
    else
      request_body = {"filter": "NACSCode eq '#{ods_code}'", "top": 25, "skip": 0,"count": true }
    end
    request = Net::HTTP::Post.new(uri.request_uri, header)
    request.body = request_body.to_json 
    response = http.request(request)
    if response.code.to_i == 200

      data = JSON.parse(response.body)["value"][0]

      if data.present?
        address =  [data["Address1"] ,data["Address2"],data["Address3"],data["City"],data["County"]  ].compact.join(",")
        contacts = JSON.parse(data["Contacts"]) rescue []
        phone_no = nil 
        email = nil
        begin
          contacts.each do |contact|
            if contact["OrganisationContactMethodType"] == "Telephone"
              phone_no = contact["OrganisationContactValue"]
            elsif contact["OrganisationContactMethodType"] == "Email"
              email = contact["OrganisationContactValue"]
            end
          end
        rescue Exception => e 
          Rails.logger.info "Error inside  get_clinic_by_ods_code fetch contact ....... #{e.message}"
        end
        organization_uid = data["OrganisationID"]
        organisation_data = {name: data["OrganisationName"],
                            uid: organization_uid,
                            :address=>(address rescue nil),
                            :postcode=>data["Postcode"],
                            :longitude=>(data["Longitude"] rescue nil),
                            :latitude=>(data["Latitude"] rescue nil),
                            :phone_no=>(phone_no),
                            :email=>email,
                            :clinic_id=>(  nil),
                            :ods_code=> (ods_code rescue nil),
                            :clinic_type=>options[:organization_type] 
                          }
        organization_obj = Clinic::Organization.new(organisation_data)
        begin
	  # Update clinic organization id if not present or incorrect
          clinic_detail = ::Clinic::ClinicDetail.find_by_code_or_organization_id(ods_code,nil,options)
          if clinic_detail.present? && (clinic_detail.organization_uid.blank? || clinic_detail.ods_code.to_s == clinic_detail.organization_uid.to_s)
            clinic_detail.update_attribute(:organization_uid, organization_uid)
          end
        rescue Exception => e
          Rails.logger.info "Error inside get_clinic_by_ods_code updating clinic detail...#{e.message}"
        end
          
        data = organization_obj
      end
    end
    data
  end

  # Nhs::NhsApi.get_nhs_syndicated_data(url,options={})
  def self.get_nhs_syndicated_data(url,options={})
    uri = URI(url)
    http = Net::HTTP.new(uri.host, uri.port)
    if uri.scheme == 'https'
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_PEER
    end    
    current_user = options[:current_user]
    member_id = options[:member_id]
    if options[:genre].to_s.downcase == "common health questions"
      header = {'subscription-key'=>APP_CONFIG['nhs_syndicated_subscription_key']['common_health_questions'], 'Content-Type'=>'application/json'}
    else
      header = {'subscription-key'=>APP_CONFIG['nhs_syndicated_subscription_key']['condition'], 'Content-Type'=>'application/json'}
    end
    request = Net::HTTP::Get.new(uri.request_uri, header)
    response = http.request(request)
    if response.code.to_i == 200
      status = 200
      request_status = "success"
      data = JSON.parse(response.body)
    elsif response.code.to_i == 301 && response["location"].present?
      request = Net::HTTP::Get.new(response["location"], header)
      response = http.request(request)
      request_status = "success"
      data = JSON.parse(response.body)
    else
      data = nil 
      request_status = "failed"
      status = 100
    end
    request_data = {:request_status=> request_status,:action=>url}
    ::Log::NhsApiLog.create_record(current_user,member_id,request_data,options)
    [data,status]
  end

  def self.get_organisations(organisation_type, limit, offset, service_code, ods_code, select_param, order_by, order,lat,long)
    
    # uri = URI('https://api.nhs.uk/service-search/organisations?api-version=1')
    uri = URI('https://api.nhs.uk/service-search/search?api-version=1')

    query = URI.encode_www_form({
        # Request parameters
        'OrganisationType' => organisation_type,
        'limit' => limit,
        'offset' => offset,
        'ServiceCode' => service_code,
        'ODScode' => ods_code,
        'select' => select_param,
        'orderBy' => order_by,
        'order' => order,
        'long' => lat,
        'lat' => long
    })



    if uri.query && uri.query.length > 0
        uri.query += '&' + query
    else
        uri.query = query
    end

    request = Net::HTTP::Get.new(uri.request_uri)
    # Request headers
    sub_key = APP_CONFIG['nhs_subscription_key']
    request['subscription-key'] = sub_key
    # Request body
    request.body = "{body}"

    
    response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
        http.request(request)
    end
    
    return response.body
    
  end


  def self.generate_login_token(authorization_code, options={})
    begin
      Rails.logger.info "..........................Generate login token...with code ......#{authorization_code}......................"
      client_id = APP_CONFIG['nhs_login_client_id']
      redirect_uri = APP_CONFIG['nhs_login_redirect']
      assertion_token = Nhs::NhsApi.generate_assertion_token()
      client_assertion_type = APP_CONFIG['nhs_login_client_assertion_type']
      token_url = APP_CONFIG['nhs_login_token_url']
      uri = URI(token_url)
      algo = APP_CONFIG['nhs_login_algo']

      req = Net::HTTP::Post.new(uri.path, {'Content-Type' =>'application/x-www-form-urlencoded', 'alg' => algo, 'typ' => 'JWT'})
      req.body =  URI.encode_www_form({
                  'grant_type'=> "authorization_code",
                  'code'  => authorization_code,
                  'redirect_uri' => redirect_uri,
                  'client_id' => client_id,
                  'client_assertion_type' => client_assertion_type,
                  'client_assertion' => assertion_token
                })

      response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
          http.request(req)
      end
      response_data = JSON.parse(response.body).with_indifferent_access
      error_code = response_data["error"] || response_data["Error"].present?
      if error_code.present?
        Rails.logger.info "Error code from NHS login token.............#{error_code}"
        msg = ::Message::NhsLogin[:error_message]# response_data['error_description'] || ::Message::NhsLogin[error_code.to_sym]
        data = {status:100,message:msg,error:response_data["error"]}
      else
        data = response_data.merge({status:200})
        
      end
      Rails.logger.info "..........................Login Token Generated........................."
    rescue Exception=>e
      Rails.logger.info "..........................Failed To Generate login token........................."
      data = {status:100,error:e.message,message: ::Message::NhsLogin[:error_message]}
      Rails.logger.info e.message
    end
    data.with_indifferent_access
  end
  # Nhs::NhsApi.fetch_user_info(token)
  def self.fetch_user_info(token)
    begin
    
      Rails.logger.info "...........Fetched user token:   #{token}..............."
      uri = URI(APP_CONFIG['nhs_login_user_info_url'])
      header_value = "Bearer " + token
      req = Net::HTTP::Get.new(uri.path, {'Authorization' =>header_value})
      response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
        http.request(req)
      end
      response_data = JSON.parse(response.body).with_indifferent_access
      error_code = response_data["Error"] || response_data["error"].present?
      if error_code.present?
        msg = response_data['error_description'] || ::Message::NhsLogin[error_code.to_sym]
        data = {status:100,message:msg,error:response_data["error"]}
      else
        data = response_data.merge({status:200})
      end
      Rails.logger.info "NHS login User info.................." #+ response.body
    rescue Exception => e
      Rails.logger.info e.message
      data = {status:100,error:e.message,message: ::Message::NhsLogin[:error_message]}
    end
    data.with_indifferent_access
  end

  # Nhs::NhsApi.format_nhs_login_user_info(current_user,user_info,options)
  def self.format_nhs_login_user_info(current_user,user_info,options={})
    sub_value = user_info["sub"]
    iss = user_info["iss"]
    aud = user_info["aud"]
    nhs_number = user_info["nhs_number"]
    first_name = user_info["given_name"].to_s
    birthdate = user_info["birthdate"]
    family_name = user_info["family_name"]
    email = user_info["email"]
    email_verified = user_info["email_verified"] ? user_info["email_verified"].to_s : ""
    phone_number_verified = user_info["phone_number_verified"] ? user_info["phone_number_verified"].to_s : ""
    phone_number = user_info["phone_number"].to_s
    gp_user_id = user_info["gp_integration_credentials"]["gp_user_id"] ?  user_info["gp_integration_credentials"]["gp_user_id"] : "" 
    gp_ods_code = user_info["gp_integration_credentials"]["gp_ods_code"] ? user_info["gp_integration_credentials"]["gp_ods_code"] : ""
    gp_linkage_key = user_info["gp_integration_credentials"]["gp_linkage_key"] ? user_info["gp_integration_credentials"]["gp_linkage_key"] : ""
    data = {
      :nhs_number=>nhs_number,
      :date_of_birth=>birthdate,
      :family_name=>family_name,
      :user_email=> email,
      :last_name=> family_name,
      :first_name=>first_name.to_s,
      :surname=> family_name,
      :account_id=>gp_user_id.to_s,
      :practice_ods_code=>gp_ods_code.to_s,
      :email_verified=>email_verified,
      :phone_number_verified=>phone_number_verified,
      :account_linkage_key=>gp_linkage_key.to_s
    }
    data
  end
  


      
      
  # Nhs::NhsApi.generate_token_via_refresh_token(refresh_token)
  def self.generate_token_via_refresh_token(refresh_token)
    begin
      Rails.logger.info "............Generate refresh token .............."

      client_id = APP_CONFIG['nhs_login_client_id']
      redirect_uri = APP_CONFIG['nhs_login_redirect']
      scope = (APP_CONFIG['nhs_login_scope']).gsub("+"," ")

      assertion_token = Nhs::NhsApi.generate_assertion_token()
      client_assertion_type = APP_CONFIG['nhs_login_client_assertion_type']
      token_url = APP_CONFIG['nhs_login_token_url']
      uri = URI(token_url)

      req = Net::HTTP::Post.new(uri.path, {'Content-Type' =>'application/x-www-form-urlencoded'})
      
      req.body =  URI.encode_www_form({
                  'grant_type'=> "refresh_token",
                  'client_id' => client_id,
                  'client_assertion_type' => client_assertion_type,
                  'client_assertion' => assertion_token,
                  'refresh_token' => refresh_token,
                  'scope' => scope
                })

      response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
          http.request(req)
      end
      return (JSON.parse(response.body) rescue response.body)
    rescue Exception=>e
      Rails.logger.info e.message
    end
  end

def self.create_deeplink_data(user_info,token_info,options={})
  begin
    Rails.logger.info "Creating deeplink for nhs"
    Rails.logger.info "Token info #{token_info.inspect}"
    Rails.logger.info "User info for deeplink ............."#{user_info.inspect}
    token_info = token_info || {}
    user_info = (user_info).with_indifferent_access
    deep_link = APP_CONFIG['default_host']+"/nhs_authentication_completed"
    email = user_info["email"]
    options[:user_email] = options[:user_email].present? ? options[:user_email] : nil
    user = User.where(email:(options[:user_email]||email)).last
    current_user = user.member rescue Member.new(email:(options[:user_email]||email))
    
    im_info = ::Nhs::NhsApi.format_nhs_login_user_info(current_user,user_info,options)
    
    Rails.logger.info "............User information parsed.............."

    access_token = token_info["access_token"].to_s
    refresh_token = token_info[:refresh_token] ? token_info[:refresh_token] : ""
     
    # Find Clinic by ods code
    clinic_info = Nhs::NhsApi.get_clinic_by_ods_code(nil,nil,im_info[:practice_ods_code],options)
    clinic_info[:clinic_email] = clinic_info.email rescue nil
    Rails.logger.info "............Clinic info for ods code #{im_info[:practice_ods_code]}. ....#{clinic_info.inspect}........."
    options[:current_user] = current_user 
    options[:im1_account_id] =    im_info[:account_id]
    # Check  if clinic linkable
    clinic_type = ::Clinic::ClinicDetail.identify_clinic_type(current_user,clinic_info,options)
    Rails.logger.info "............Clnic identified as #{clinic_type}................"
    linkable_status = Clinic::ClinicDetail.clinic_linkable_status(clinic_type,options) == "active"
     
    already_account_exist = user.present?   
    if already_account_exist
      ::Clinic::Organization.setup_clinic_with_nhs(current_user,clinic_type, clinic_info,options)
    end
    nhs_account = (user.account_type_allowed?("Nhs") rescue false)

    api_access_token = (user.api_access_token ||  user.refresh_api_token )rescue nil
    
    # Save nhs login detail  
    ::Nhs::LoginDetail.save_access_token(user,refresh_token,options)
    
    # Check for nhs welcome screen on first time nhs login 
    if user.present?
      #  user exists and the user is using NHS login for auth very first time
      show_nhs_welcome_screen = user.nhs_welcome_screen_status(options) && options[:login_type] == "auth"
    else
      show_nhs_welcome_screen = options[:login_type] == "auth" ? true : false
    end

    query_data = im_info.merge({
      :access_token=>access_token,
      :refresh_token=>refresh_token,
      :api_access_token=>api_access_token,
      :linkable_status=>linkable_status,
      :is_account_already_exist =>already_account_exist,
      :is_nhs_account => nhs_account,
      :show_nhs_login_welcome=>show_nhs_welcome_screen
    }).merge(clinic_info.attributes)
    
    
    final_deep_link = deep_link + "?#{query_data.to_query}"

    # Rails.logger.info "................Deeplink:  #{final_deep_link} ......................."
    return final_deep_link
  rescue Exception=>e
    Rails.logger.info e.message
  end
end

  #Not in use
  def self.signup_or_signin_via_nhs(auth,options={})
    user = User.where(email: auth.info.email).first
    if user.provider.blank? || user.provider != auth.provider
      raise 'User account already exist.'
    end
    unless user
      user = User.new(provider: auth.provider, uid: auth.uid)
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,8] + SecureRandom.urlsafe_base64(nil, true)
      user.status = User::STATUS[:confirmed]
      first_name = (auth.info.family_name || auth.info.name) rescue nil
      last_name = auth.info.last_name
      image = auth.info.image rescue nil
      user.first_name = first_name 
      user.last_name =  last_name
      country_code = auth.info.country_code rescue nil 
      time_zone = (auth.info.time_zone || auth.time_zone ) rescue nil
      user.country_code = country_code.present? ? country_code.downcase : 'uk'
      # don't send verification mail
      user.skip_confirmation!
      user.confirmation_token || user.send(:generate_confirmation_token)
      if user.save!
        member = ParentMember.where(:email => user.email).first || ParentMember.create(time_zone: time_zone,user_id: user.id, first_name: first_name, last_name: last_name, image: image, email: user.email)
        if member.user_id.blank? # For invitation
          member.update_attributes(time_zone: time_zone, user_id: user.id, first_name: first_name, last_name: last_name, image: image)
        end
        Score.new(activity_type: "User", activity_id: user.id, activity: "Sign up", point: Score::Points["Signup"], member_id: member.id).save
        user.update_attributes(member_id:member.id )
        user.save(validate:false)
        user.save_detail(options)
        user.reload
      end
    end
    if user
      user.skip_confirmation!
      user.status = User::STATUS[:confirmed]
      Nhs::LoginDetail.save_access_token(user,auth.info.refresh_token,options)
      user.save!
    end
    user
  end

private


def self.generate_assertion_token(code=nil,options={})
  begin
    client_id = APP_CONFIG['nhs_login_client_id']  
    audience = APP_CONFIG['nhs_login_token_url']
    algo = APP_CONFIG['nhs_login_algo']
    expiresIn = Time.now.to_i + 60
    jwtid = 1
    vtr = APP_CONFIG['nhs_login_vtr']
    vtm = APP_CONFIG['nhs_login_vtm']
    client_token_sign_options = { "alg": algo, "sub": client_id, "iss": client_id, "aud": audience, "jti": SecureRandom.uuid,"exp": expiresIn,"typ": "JWT", "vtr": vtr,"vtm": vtm}

    key_path = "#{Rails.root}/#{APP_CONFIG['nhs_private_key']}"
    rsa_private = OpenSSL::PKey::RSA.new File.read key_path
    assertion_token = JWT.encode client_token_sign_options, rsa_private, algo
    Rails.logger.info "Assertion Token................... " + assertion_token
  rescue Exception => e 
    Rails.logger.info "Error inside generate_assertion_token.......... #{e.message}"
    assertion_token = nil
  end  
  return assertion_token
end



end