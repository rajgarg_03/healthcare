class Nhs::GuideTiles
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :custom_id,                     :type=> Integer #auto increamented value for  ScenarioDetails.eg 1,2,3 etc will used for backtracking
  field :title,                         :type=> String # name
  field :description,                   :type=> String # meta info
  field :category,                      :type=> String #  
  field :status,                        :type=> String, :default=>"Active"
  field :country_code,                  :type=> String, :default=>"UK"
  field :identifier,                    :type=> String 
  field :order_id,                      :type=> Integer # Used to sort by order

  after_create :assign_custom_id
  
  index ({custom_id: 1})
  index ({status: 1})
  index ({category: 1})
  index ({title: 1})
  index ({status: 1})
  index ({order_id: 1})
  
  DATATABLE_COLUMNS = [:custom_id,:status,:title, :category]
  
  def self.search_title(keyword)
    where(title:/#{keyword}/i)
  end

  
  def self.valid(current_user,member_id,options={})
    where(status:"Active")
  end

  # Nhs::GuideTiles.get_category_list(current_user,member_id,options={})
  def self.get_category_list(current_user,member_id,options={})
    category_list = ::Nhs::GuideTiles.valid(current_user,member_id,options).order_by("order_id asc")#.pluck(:category).compact.flatten.map(&:capitalize).uniq
    if (options[:tile_limit].present?  )
      category_list = category_list.limit(options[:tile_limit])
    end
    data = []
    category_list.each do |tile|
       data << {
        id:tile.id,
        title: tile.title,
        category:tile.category,
        identifier: tile.identifier,
        image_url: "#{APP_CONFIG[:default_host]}/assets/nhs_guide_tiles/#{tile.identifier}.png"
      }

    end
    data
  end

  # Nhs::GuideTiles.guidetile_widget(current_user,options={})
  def self.guidetile_widget(current_user,options={})
    member_id = nil
    category_list = ::Nhs::GuideTiles.get_category_list(current_user,member_id,options)
    suggested_content = ::Nhs::SyndicatedContent.get_suggested_syndicated_content(current_user,member_id,options)
    data = {tiles:category_list,suggested_content:suggested_content }
  end

  def self.show_nhs_guid_tile_status(current_user,options={})
    user_country =  Member.member_country_code(current_user.user.country_code)
    return false if user_country != "gb"
    family_ids = current_user.user_families.map{|a| a.id.to_s}
    preganancy_list = Pregnancy.in(family_id:family_ids).where(:status=> "expected")
    preganancy_list.present? 
  end

  
  # Used for Data table 
  def self.datatable_filter(search_value, search_columns)
    return all if search_value.blank?
    result = ::Nhs::GuideTiles
    filter = []
     
    search_columns.each do |key, value|
      if value['data'] == "custom_id"
       filter << {"#{value['data']}"=>search_value} if value['searchable']
      else
       filter << {"#{value['data']}"=>/#{Regexp.escape(search_value)}/i} if value['searchable']
      end
    end
    result = result.where("$or"=>filter)

    result
  end  
  
  def self.datatable_order(order_column_index, order_dir)
    order_by("#{System::Scenario::ScenarioDetails::DATATABLE_COLUMNS[order_column_index]} #{order_dir}")
  end

  def scenario_detail
    guide_tile = self
    System::Scenario::ScenarioLinkedToTool.where(tool_class:guide_tile.class.to_s,linked_tool_id:guide_tile.id.to_s).last.scenario_detail rescue nil

  end

  def update_scenario(system_scenario_custom_id)
    guide_tile = self
    System::Scenario::ScenarioLinkedToTool.add_to_list(guide_tile,system_scenario_custom_id)
  end

  def assign_custom_id
    guide_tile  = self
    guide_tile[:custom_id] = ::Nhs::GuideTiles.max(:custom_id).to_i + 1
    guide_tile.save
  end


  # Used for admin panel
   def update_record(params)
    begin
      guide_tile = self
      action_card_params = params["nhs_guide_tiles"]
      if guide_tile.update_attributes(action_card_params)
        status = true
      else
        status = false
      end
    rescue Exception => e
      Rails.logger.info e.message
      status = false 
    end
    [guide_tile,status]
  end


  def self.create_record(params)
    begin
       
      action_card_params = params["nhs_guide_tiles"]
      guide_tile = self.new(action_card_params)
      guide_tile.save!
      status = true
    rescue Exception => e
      Rails.logger.info e.message
      status = false 
    end
    [guide_tile,status]
  end

  def self.duplicate_record(params)
    record_to_be_duplicated = ::Nhs::GuideTiles.find(params[:record_id]).attributes
    record_to_be_duplicated.delete("_id")
    record_to_be_duplicated.delete("created_at")
    record = ::Nhs::GuideTiles.new(record_to_be_duplicated)
    record.save
    [record,nil]
  end

  def self.delete_record(record_id)
    begin
      guide_tile = ::Nhs::GuideTiles.find(record_id)
      guide_tile.delete
    rescue Exception => e
      Rails.logger.info e.message
    end
  end

  
  
end