#This will store System define Subscription plan which user can subscribe
class SubscriptionManager
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :title, type: String
  field :description, type: String
  field :plan_type, type: String #basic, premium
  field :category, type:String   #Free,Gold,Platinum
  field :plan_duration, type:String # text for api eg. "Month", Year
  field :frequency, type:Integer # eg 1,2,.. (1 month, 2 month  => frequency+ plan_duration)
  field :free_trial, type:String #free trial text eg "1 day free trial"
  field :trial_duration, type:String, default:"0.days" # "1.day", "1.week"
  field :plan_info, type:Array   # Used for api. Array of title and text_to_highlight hash 
  field :subscription_status, type:String # Active/Draft
  field :supported_api_version, type:Integer
  field :subscription_listing_order, type:Integer
  field :android_product_id, type:String
  field :ios_product_id, type:String
  field :grace_period, type: String, default:"0.days" # grace period eg:"13.days"

  field :max_premium_tool_count, type:Integer #premium tool count can be addedd for subscription eg(0,-1, some value) { 0=> no tool,-1=> unlimited,somecount=> any integer value) 
  field :max_basic_tool_count, type:Integer #premium tool count can be addedd for subscription eg(0,-1, some value) { 0=> no tool,-1=> unlimited,somecount=> any integer value) 
  
  field :android_supported_app_version, :type=>Integer, :default=>0   
  field :ios_supported_app_version, :type=>Integer,:default=>0
  field :android_supported_app_version_str, :type=>String, :default=>0    
  field :ios_supported_app_version_str, :type=>String,:default=>0
  field :identifier, :type=>Integer

  index({plan_type:1})
  index({category:1})
  index({subscription_status:1})
  index({subscription_listing_order:1})
  index({identifier:1})

  validates :subscription_listing_order, uniqueness: true
  validates :identifier, uniqueness: true
  validates :android_product_id, uniqueness: true
  validates :ios_product_id, uniqueness: true
  validates :max_premium_tool_count, presence: true
  validates :max_basic_tool_count, presence: true
  validates :title, presence: true
  
  def self.free_subscription
    where(identifier:Subscription::Level["free"]).last
  end
  def get_max_basic_tool_count(subscription_manager=nil)
    subscription_manager = self
    subscription_manager.max_basic_tool_count
  end

  def get_max_premium_tool_count(subscription_manager=nil)
    subscription_manager = self
    subscription_manager.max_premium_tool_count
  end

  def calculate_end_date
    begin
    subscriptiomanager = self
    subscriptiomanager.frequency = subscriptiomanager.frequency || 1
    Date.today + eval("#{subscriptiomanager.frequency.to_i}.#{subscriptiomanager.plan_duration.downcase}")
    rescue Exception=>e 
      nil
    end
  end
  def self.available_price_lable(device_platform="ios")
    price_labels = []

    (1..86).each do |a|
      price_labels << "Tier #{a}"
    end
    (1..5).each do |a|
      price_labels << " Alternate Tier#{a}"
    end 
     price_labels
  end


  def self.verify_subscription(current_user,params,api_version=4)
    begin
      is_sufficient_subscription = false
      current_user_obj = current_user
      @subscription_screen = nil
      platform = params[:platform] || current_user.device_platform
      member = Member.find(params[:member_id]) if params[:member_id]
      family_id = params[:family_id] || (member.member_families.last.family_id rescue raise "Invalid family id")
      Family.find(family_id) rescue raise "Invalid family id"
      # get detail of subscription
      subscription = Subscription.find_create_subscription(current_user,family_id,platform,api_version)
      subscription_manager = subscription.subscription_manager
      tool_require_subscription_level =  subscription_manager.subscription_listing_order
      # verify if coming from tool manage
      tool_screen_status = true
      if params[:tool_id].present?
        tool = (Nutrient.find(params[:tool_id])   rescue raise "Invalid tool id")
        # check if tool is available for current subscription
        options = {:subscription=>subscription,:platform=>platform}.merge({:source=>params[:source]})
        tool_screen_status = tool.info_screen.present?
        tool_status,msg, tool_require_subscription_level = tool.get_subscription_status(current_user_obj,family_id,api_version,options)
        free_trial_available_status,trial_msg = Subscription.free_trial_end_date_message(current_user,family_id,subscription,subscription,options)
        if tool_status && !free_trial_available_status
          tool_require_subscription_level = tool_require_subscription_level + 1
        end
        tool_status = tool_status && free_trial_available_status
        tool_message = tool_status ? Message::Subscription[:tool_Subscribed] : Message::Subscription[:tool_not_subscribed]
        is_sufficient_subscription = tool_status

        @subscription_screen = Subscription.subscription_screen(current_user,params[:tool_id],tool_require_subscription_level,family_id,api_version,params[:source]) if (!is_sufficient_subscription && tool_screen_status)
      else
        tool_require_subscription_level = 0
      end
      # prepare output
      data,subscribed_member,subscribed_family,subscribed_platform,subscribed_level,is_subscription_expired = Subscription.get_subscriptions_list(family_id,current_user.id,api_version,tool_require_subscription_level,{:subscription=>subscription,:subscription_manager=>subscription_manager,:platform=>params[:platform]})
      subscription_message =  subscribed_platform.present? ? "" : Message::Subscription[:not_subscribed]
      subscription_message = Message::Subscription[:subscription_expired] if  is_subscription_expired
      message = tool_message || subscription_message
      status = StatusCode::Status200
      @error = nil
    rescue Exception=>e
      status = StatusCode::Status100
      if params[:family_id].blank? && params[:tool_id].blank? && params[:member_id].blank?
        message = "Unable to verify subscription for family."
      # else   
      #   message = Message::Subscription[:verify_subscription_error]
      end
      @error = e.message
    end

    grade_required = tool_require_subscription_level
    response = {:grade_required => grade_required, :subscription_level=>subscribed_level, :is_sufficient_subscription=> is_sufficient_subscription, :subscribed_platform=>subscribed_platform, :family_to_subscribe=>subscribed_family ,:subscribed_by_member=>subscribed_member, :message=>message,:status=>status,:error=>@error, :subscription_plans=>data}
    if api_version <= 4
      response.merge!(:subscription_screen=>@subscription_screen) if @subscription_screen.present? 
    end
    response
  end

  def self.valid_subscription_manager(current_user,api_version,options={})
    user = current_user.user rescue  User.new
    user_device = current_user.devices.last
    device_app_version = (user_device.app_version_in_number  rescue 0).to_i # if web user 
    platform = user_device.platform.downcase rescue "web"
    user_type = user.user_type.downcase rescue nil
    na_values_for_app_version = -1
    na_value_for_api_version = -1
    if user_type == "internal"
      data = order_by("subscription_listing_order desc")
    elsif platform == "android"
      data = where(:supported_api_version=>{"$lte"=>api_version,"$ne"=>na_value_for_api_version}, :android_supported_app_version=>{"$lte" => device_app_version,"$ne"=>na_values_for_app_version}).order_by("subscription_listing_order desc")
    elsif platform == "ios"
      data = where(:supported_api_version=>{"$lte"=>api_version,"$ne"=>na_value_for_api_version},:ios_supported_app_version=>{"$lte" => device_app_version,"$ne"=>na_values_for_app_version}).order_by("subscription_listing_order desc")
    else
      data = where(:supported_api_version=>{"$lte"=>api_version,"$ne"=>na_value_for_api_version}).order_by("subscription_listing_order desc")
    end
    data = data.where(:subscription_status=>"Active") if (user_type.blank? || user_type.downcase == "general" || user_type.downcase == "normal" )  
    data
  end  
   
  def self.get_price_list(current_user,options={})
    country_code = current_user.get_country_code
    county_codes = Member.sanitize_country_code(country_code)
    price_list = {}
    if county_codes.include?("IN")
      price_list = {:yearly=>"499",:monthly=>"79",:per_month=>"41.58",:currency=>"₹"}
    elsif county_codes.include?("uk")
      price_list = {:yearly=>"9.99",:monthly=>"1.49",:per_month=>"0.83",:currency=>"£"}
    elsif county_codes.include?("us")
      price_list = {:yearly=>"9.99",:monthly=>"1.49",:per_month=>"0.83",:currency=>"$"}
    else
      price_list = {:yearly=>"9.99",:monthly=>"1.49",:per_month=>"0.83",:currency=>"£"}
    end
      price_list  
  end

  def subscription_product_id(platform)
    return "" if platform.blank? 
    if platform.downcase == "android"
      self.android_product_id
    else
      self.ios_product_id
    end
  end

  def self.premium_subscriptions
    where(:identifier.gt => Subscription::Level["free"]).order_by("subscription_listing_order desc") 
  end

   
end


