class PasswordHistory
  include Mongoid::Document
  include Mongoid::Timestamps
  field :user_id, :type => String
  field :encrypt_password, :type => String

  def self.create_password_record(user,password,options={})
    encrypt_password = ShaAlgo.encryption(password)
    PasswordHistory.create(:encrypt_password=>encrypt_password,:user_id=>user.id)
  end
end