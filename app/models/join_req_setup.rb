# This is Join Setup controller
class JoinReqSetup
  
# run join setup controller 
  def self.run(member,invitaion_id=nil)
    #find loggedin user families and prepare Family Screen data for api
    user_families_ids = member.user_family_ids 
    #family_data  = BasicSetupEvaluator.family_screen_data(member,user_families_ids)
    member_basic_setup_evalu = member.basic_setup_evaluator
    #Get summery scrren count from DB .Set by default 0 for first time 
    member_basic_setup_evalu ||= member.create_basic_setup_evaluator(welcome_screen_count: 0,summary_screen_count:0,  user_families_count: 0)
    
    # Lookout all invitatoin to join family
    member_received_join_family_invitations = (member.received_join_family_invitations)
    js = false
    data = [] 
    if  member_received_join_family_invitations.present?
       invit_data = []
      js = true
      member.received_invitations_data(member_received_join_family_invitations).each do |invit|
       invit_data << {:invitation_data=>invit,:progress=>[] } 
       end
       data << {:accept_reject_screen=>invit_data}
    end
      #increase summery screen count by 1
      if member_basic_setup_evalu["summary_screen_count"].to_i == 0
        member_basic_setup_evalu.summary_screen_count = member_basic_setup_evalu.summary_screen_count + 1 rescue 1
        member_basic_setup_evalu.save
         
      end
      data,js = [nil,false] if data.blank?
    [data,js]
  end
end