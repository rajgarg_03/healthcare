class SystemVaccin
  include Mongoid::Document 
  include Mongoid::Timestamps 
  field :name, type: String
  field :jabs_count, type: String,:default => 1
  # field :ip, type: String
  field :route, type: String # oral/injection/nasal spray
  field :member_id, type: String
  field :category, type: String
  # field :route, type: String, default: "Injection" # Oral/Injection/nasal spray
  field :vaccin_type, type: String #Routine/selective/Optional 
  field :country, type: String
  field :only_for, type:String
  field :trade_name, type:String
  field :schedule_updated, type:Boolean, default: false # vaccine with new schedule eg Pneumococcal vaccine for UK
  # field :nhs_syndicate_content_id, type:String
  field :data_source, type:String, default:"nurturey"
  has_many :system_jabs
  
  index ({name:1})
  index ({country:1})
  index ({member_id:1})
  index ({vaccin_type:1})
  
  validates :name, presence: true

  Popular = {"Selective"=>'', "Routine"=>"star", "Recommended"=>"star", "Optional"=>""}
  RouteType =  ["Oral","Injection","Nasal spray"]
  
  CategoryMapping = {"Oral"=>"Oral","Drops"=>"Oral","Injection"=>"Injection","Nasal spray"=>"Nasal spray"}
  VaccineTypeMapping = {"Routine"=>"Routine","Recommended"=>"Routine","Optional"=>"Optional","Selective"=>"Selective","Custom"=>"Custom"}
  
  # To support backward compatibilty
  CategoryMapping1 = {"Oral"=>"Drops","Drops"=>"Drops","Injection"=>"Injection","Nasal spray"=>"Nasal spray"}
    

  #before_save :set_route

  # def set_route
     
  #   if self["category"].present?
  #     self.route = self["category"]
  #   end
  # end

  # def category
  #   self["route"] || self["category"]
  # end
  
  def nhs_syndicate_content(options={})
    vaccine = self 
    ::Nhs::SyndicatedContentLinkTool.nhs_content_list_linked_with_tool(vaccine).map{|a| a.syndicated_format_data(nil,nil,options)}
  end
  def get_name
    self.name
  end

  def seed_route_data_from_category
  #  SystemVaccin.all.rename(:category,:route) 
  end
  def get_json_data(member,current_user,opted_value=nil,options={})
    system_vaccine = self
    system_vaccine["opted"]= opted_value || (system_vaccine.vaccin_type == "Recommended") 
    system_jab = system_vaccine.system_jabs.first
    jab_expected_on = system_jab.due_on rescue "0.weeks"
    system_vaccine["route"] = system_jab.get_system_jab_route(options) rescue "Injection"
    if opted_value == true
      system_vaccine["category"] = system_vaccine["route"]
    else
      system_vaccine["category"] = system_vaccine.get_category(options)
    end
    system_vaccine["estimated_due_date"] = (member.birth_date + eval(jab_expected_on)).to_date2 rescue nil
    system_vaccine["vaccin_type"] = system_vaccine.get_category(options)
    system_vaccine["estimated_due_date_text"] =  Vaccination.get_estimate_age(jab_expected_on,system_vaccine["vaccin_type"])
    system_vaccine
  end

  def self.system_vaccine_group_by_id(system_vaccine_ids=nil,options={})
    if system_vaccine_ids.nil?
      selection_critriea = {}
    else
      system_vaccine_ids = system_vaccine_ids.map{|a|Moped::BSON::ObjectId(a) }
      selection_critriea =  { "_id" => {"$in"=>system_vaccine_ids }}
    end
    hash_data =  SystemVaccin.collection.aggregate([
      { "$match" => selection_critriea },
      {"$group" => {
        "_id" => "$_id",
         "jab_ids" => {"$push"=> "$$ROOT"}
      }} ,
      {"$sort" => { "_id" => 1}}
    ])
     
    data = {}
    hash_data.each do |info|
      data[info["_id"].to_s] = info["jab_ids"]
    end
    data
  end

  def self.system_vaccination_list_for_user(current_user,child=nil,user_added_vaccin=true,options={:order_by=>"name"})
    # nhs_immunisation_jabs = System::GpsocImmunisationJab.all[8..10].map(&:term)
    country_code = Vaccination::Country[(current_user.user.country_code || "UK" ).downcase] || "UK"
    sanitize_country_code = Member.sanitize_country_code(country_code)
    child_role = child.family_members.first.role rescue nil
    if child.present?  
      nhs_vaccine, system_mapped_nhs_data = SystemVaccin.get_nhs_immunisation_jabs(current_user,child)
      nhs_system_vaccin = system_mapped_nhs_data.keys
    else
      nhs_system_vaccin = []
    end
      
    if(child_role == FamilyMember::ROLES[:daughter])
      user_vaccinations = user_added_vaccin ?  SystemVaccin.where(member_id: current_user.id,:id.nin=>nhs_system_vaccin).sort_by{|i| i.name.upcase} : []
      recommended_vaccin = SystemVaccin.where(:vaccin_type =>"Recommended").where(member_id: nil,:id.nin=>nhs_system_vaccin).in(country:sanitize_country_code).sort_by{|i| i.name.upcase}
    else
      user_vaccinations = user_added_vaccin ?  SystemVaccin.not_in(:only_for=>[FamilyMember::ROLES[:daughter]]).where(member_id: current_user.id,:id.nin=>nhs_system_vaccin).sort_by{|i| i.name.upcase} : []
      recommended_vaccin = SystemVaccin.where(:vaccin_type =>"Recommended").not_in(:only_for=>[FamilyMember::ROLES[:daughter]]).where(member_id: nil,:id.nin=>nhs_system_vaccin).in(country:sanitize_country_code).sort_by{|i| i.name.upcase}
    end
    if !options[:non_recommended].nil? && options[:non_recommended] == false
      non_recommended_vaccin = []
    else
      if child_role == FamilyMember::ROLES[:daughter]
        non_recommended_vaccin = SystemVaccin.where(:id.nin=>nhs_system_vaccin, :vaccin_type.ne=> "Recommended" ).where(member_id: nil).in(country:sanitize_country_code).sort_by{|i| i.name.upcase}
      else
        non_recommended_vaccin = SystemVaccin.where(:id.nin=>nhs_system_vaccin, :vaccin_type.ne=> "Recommended" ).not_in(:only_for=>[FamilyMember::ROLES[:daughter]]).where(member_id: nil).in(country:sanitize_country_code).sort_by{|i| i.name.upcase}
      end
    end
    if child
      data = []
        (nhs_vaccine + user_vaccinations + recommended_vaccin + non_recommended_vaccin ).each do |vaccination|
          system_jab = vaccination.system_jabs.first
          jab_expected_on = system_jab.first.due_on rescue "0.weeks"
          nhs_record = system_mapped_nhs_data[vaccination.id.to_s].first rescue nil
          if nhs_record.present?
            vaccination["estimated_due_date"] = (nhs_record["due_on"]).to_date2
            vaccination["data_source"] = "emis"
          else
            vaccination["estimated_due_date"] = (child.birth_date + eval(jab_expected_on)).to_date2
            vaccination["data_source"] = vaccination.data_source || "nurturey"
          end
           vaccination[:date_for_sorting] = (child.birth_date + eval(jab_expected_on))
           vaccination["estimated_due_date_text"] = "Given at an estimated age of " + ApplicationController.helpers.calculate_age(Date.today - eval(jab_expected_on))  rescue nil
           vaccination["category"] = vaccination.get_category(options)
           vaccination["vaccin_type"] = vaccination.get_category(options)
           data << vaccination
           if options[:order_by] == "due_date"
              data = data.sort_by{|a|  a[:date_for_sorting]}
           end
        end
    else
     data = user_vaccinations + recommended_vaccin + non_recommended_vaccin
     data.map do |vaccination|
         system_jab = vaccination.system_jabs.first
         jab_expected_on = system_jab.first.due_on rescue "0.weeks"
         vaccination["estimated_due_date"] = nil
         vaccination[:date_for_sorting] = nil
         vaccination["estimated_due_date_text"] = "Given at an estimated age of " + ApplicationController.helpers.calculate_age(Date.today - eval(jab_expected_on))  rescue nil
         vaccination["category"] = vaccination.get_category(options)
         vaccination["vaccin_type"] = vaccination.get_category(options)
         
     end
    end
    data
  end
  
  # SystemVaccin.get_nhs_immunisation_jabs(current_user,member,options={})
  def self.get_nhs_immunisation_jabs(current_user,member,options={})
    clinic_type = nil
    # nhs_immunisation_jabs = ::System::GpsocImmunisationJab.all[8..10]
    clinic_detail = member.get_clinic_detail(current_user,options)
    clinic_type = clinic_detail.clinic_type.downcase rescue nil
    if !member.is_user_fully_linked_with_clinic?(current_user,options)
        member_vaccination_ids = member.vaccinations.map(&:id).map(&:to_s)
        Jab.where(:vaccination_id.in=>member_vaccination_ids, :data_source.ne=>"nurturey").update_all(data_source:"nurturey",due_on_from_data_source: nil)
      return [[],{},nil] 
    end
    nhs_immunisation_jabs = []
    options[:item_type] = "Immunisations"
    options[:current_user] = current_user
    options[:only_administered] = true
    clinic_class = Clinic::ClinicDetail.get_clinic_linkable_class(current_user,member,"Immunisations",options)
    response = clinic_class.get_patient_immunisations(member.id,options)#[:data]
    return [[],{},response[:status]] if response[:status] != 200
    nhs_immunisation = response[:data]
    # nhs_immunisation = {"2020-01-30T00:00:00"=>[{:code_id=>2533976011, :member_id=>nil, :name=>"Influenza vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 30 Jan 20"}, {:code_id=>2236311000000116, :member_id=>nil, :name=>"1st hepatitis A vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 30 Jan 20"}], "2020-02-02T00:00:00"=>[{:member_id=>nil, :name=>"First typhoid vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 02 Feb 20"}, {:member_id=>nil, :name=>"1st hepatitis B vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 02 Feb 20"}], "2020-02-02T08:09:00"=>[{:member_id=>nil, :name=>"First diphtheria vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 02 Feb 20"}]}
    nhs_immunisation = nhs_immunisation.with_indifferent_access
    data_source = clinic_type || "nurturey"
    # nhs_immunisation = {"2020-01-30T00:00:00"=>[{:member_id=>nil, :name=>"Influenza vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 30 Jan 20"}, {:member_id=>nil, :name=>"1st hepatitis A vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 30 Jan 20"}], "2020-02-02T00:00:00"=>[{:member_id=>nil, :name=>"First typhoid vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 02 Feb 20"}, {:member_id=>nil, :name=>"1st hepatitis B vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 02 Feb 20"}], "2020-02-02T08:09:00"=>[{:member_id=>nil, :name=>"First diphtheria vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 02 Feb 20"}]}
   
    nhs_immunisation.each do |date,data_list|
      data_list.each do |nhsdata|
        nhsdata[:due_on] = date.to_date
        if clinic_type == "tpp"
          nhsdata[:term] = nhsdata[:tpp_term]
        else
          nhsdata[:term] = nhsdata[:name]
        end
        nhsdata[:snomed_ct_description_id] = nhsdata[:code_id].to_s
        data_source = nhsdata[:data_source]
        nhs_immunisation_jabs << nhsdata
      end
    end
    #selected_field = :term
    if clinic_type == "tpp"
      selected_field = :tpp_term
    else
      selected_field = :snomed_ct_description_id
    end
    country_code = Vaccination::Country[(current_user.user.country_code || "UK" ).downcase] || "UK"
    sanitize_country_code = Member.sanitize_country_code(country_code)
 
    nhs_immunisation_jabs_group_by_selected_field = nhs_immunisation_jabs.group_by{|a| a[selected_field]}
    # nhs_immunisation_jabs_group_by_term = nhs_immunisation_jabs.group_by{|a| a["term"]}
    
    if clinic_type == "tpp"
      # check for combine vaccine eg 6-in-1, 3-in-1
      nhs_immunisation_jabs_group_by_selected_field = SystemVaccin.check_for_combined_vaccine(current_user,member,nhs_immunisation_jabs_group_by_selected_field,options)
      nhs_immunisation_jabs_ids = nhs_immunisation_jabs_group_by_selected_field.keys
    else  
      nhs_immunisation_jabs_ids = nhs_immunisation_jabs.map{|a| a[selected_field]}
    end
    
    
    # Check for 6 in 1 vaccination
    six_in_1_system_jab_id,snomed_description_id_for_6_in_1 = [nil,nil] #SystemVaccin.get_6_in_1_vaccination_mapped_system_jab(current_user,member,nhs_immunisation_jabs_ids,options)
    
    gpsoc_immunisation_jabs = ::System::GpsocImmunisationJab.where(:country_code.in=>sanitize_country_code, selected_field.in=>nhs_immunisation_jabs_ids).entries
    if six_in_1_system_jab_id.present?
      # Assign SystemJab id to 
      gpsoc_immunisation_jabs.each do |gpsoc_immunisation_jab| 
        if gpsoc_immunisation_jab[selected_field] == snomed_description_id_for_6_in_1
          gpsoc_immunisation_jab[:system_jab_id] = six_in_1_system_jab_id
        end
      end
    end

    gpsoc_immunisation_jab_mapped = gpsoc_immunisation_jabs.map(&selected_field)
    
    # gpsoc_immunisation_jabs_gorup_by_custom_id = gpsoc_immunisation_jabs.group_by{|a| a[:custom_id]}
    # gpsoc_immunisation_jab_custom_ids = gpsoc_immunisation_jabs_gorup_by_custom_id.keys
    # system_jabs = SystemJab.where(:gpsoc_immunisation_jab_custom_id.in=>gpsoc_immunisation_jab_custom_ids).entries

    gpsoc_immunisation_jabs_gorup_by_system_jab_id = gpsoc_immunisation_jabs.group_by{|a| a[:system_jab_id]}
    gpsoc_immunisation_system_jab_ids = gpsoc_immunisation_jabs_gorup_by_system_jab_id.keys
    system_jabs = SystemJab.where(:id.in=>gpsoc_immunisation_system_jab_ids).entries

    system_vaccine_ids = system_jabs.map(&:system_vaccin_id).uniq
    
    system_vaccine_list = SystemVaccin.where(:id.in=>system_vaccine_ids,:country.in=>sanitize_country_code).sort_by{|i| i.name.upcase}
   
    # system_jabs = SystemJab.where(:gpsoc_immunisation_jab_custom_id.in=> gpsoc_immunisation_jabs_gorup_by_custom_id.keys, :system_vaccin_id.in=> system_vaccine_list.map(&:id).map(&:to_s)).entries
    system_jabs = SystemJab.where(:id.in=> gpsoc_immunisation_system_jab_ids, :system_vaccin_id.in=> system_vaccine_list.map(&:id).map(&:to_s)).entries
   

    nhs_mapping_with_system = {}
    system_jabs.each do |system_jab|
      # gpsoc_jab = gpsoc_immunisation_jabs_gorup_by_custom_id[system_jab.gpsoc_immunisation_jab_custom_id.to_i].last
      gpsoc_jab = gpsoc_immunisation_jabs_gorup_by_system_jab_id[system_jab.id.to_s].last
      nhs_mapping_with_system[system_jab.system_vaccin_id.to_s] = nhs_mapping_with_system[system_jab.system_vaccin_id.to_s] || [] 
      jab_data = nhs_immunisation_jabs_group_by_selected_field[gpsoc_jab[selected_field]].last
      jab_data[:system_jab_id] = system_jab.id.to_s
      nhs_mapping_with_system[system_jab.system_vaccin_id.to_s] << jab_data
    end
    # #  Check missed nhs jab from system gpsoc immunisation 
    # Not in use for now
    # jabs_not_exist_in_system_gpsoc = nhs_immunisation_jabs_ids - gpsoc_immunisation_jab_mapped
    # jab_not_mapped_with_sytem_jab = gpsoc_immunisation_jab_custom_ids - system_jabs.map(&:gpsoc_immunisation_jab_custom_id) 
    # #  Save into system vaccine if not added in systemGPsocImmunisation
    # if jabs_not_exist_in_system_gpsoc.present?
    #   jabs_not_exist_in_system_gpsoc.each do |selected_field| 
    #     nhs_data = nhs_immunisation_jabs_group_by_selected_field[selected_field]
    #     system_vaccine =  SystemVaccin.where(name:nhs_data["term"]).last
    #     if system_vaccine.blank?
    #       data = {"system_vaccin"=>{:name=>nhs_data["term"], country:"UK",data_source:"emis", jabs_count:1}}
    #       system_vaccine,status = SystemVaccin.create_record(data)
    #     end
    #     system_vaccine_list << system_vaccine.id
    #   end
    # end
    
    #  Save into system vaccine systemGPsocImmunisation not mapped
    #  Enable if required: not in use for now
    # if jab_not_mapped_with_sytem_jab.present?
    #   jab_not_mapped_with_sytem_jab.each do |custom_id| 
    #     nhs_data = ::System::GpsocImmunisationJab.where(custom_id:custom_id).last
    #     system_vaccine =  SystemVaccin.where(name:nhs_data["term"]).last
    #     if system_vaccine.blank?
    #       data = {"system_vaccin"=>{:name=>nhs_data["term"], country:"UK",data_source:"emis", jabs_count:0}}
    #       system_vaccine, status = SystemVaccin.create_record(data)
    #     end
    #     system_vaccine_list << system_vaccine.id
    #   end
    # end

    [system_vaccine_list,nhs_mapping_with_system,response[:status]]
  end

  def self.get_6_in_1_vaccination_mapped_system_jab(current_user,member,nhs_snomed_description_ids,options={})
    begin
      sanitize_country = Member.sanitize_country_code("uk")
      # 6-in-1 vaccine (DTaP/IPV(polio)/Hib/Hep B) Jab 1
      vaccine_name = "6-in-1 vaccine (DTaP/IPV(polio)/Hib/Hep B)"
      system_jabs = SystemVaccin.where(:country.in=>sanitize_country,name:vaccine_name).last.system_jabs.order_by("due_on_in_day_int asc").entries
       
      system_jab_id = nil
      snomed_id_for_6_in_1 = nil
      if nhs_snomed_description_ids.include?("2533976011") && nhs_snomed_description_ids.include?("2236311000000116")
        system_jab_id  = system_jabs[0].id
        snomed_id_for_6_in_1 = "2533976011"
      elsif nhs_snomed_description_ids.include?("2533977019") && nhs_snomed_description_ids.include?("2236351000000117")
        system_jab_id  = system_jabs[1].id
        snomed_id_for_6_in_1 = "2533977019"
      elsif nhs_snomed_description_ids.include?("2534071010") && nhs_snomed_description_ids.include?("2236391000000113")
        system_jab_id  = system_jabs[2].id
        snomed_id_for_6_in_1 = "2534071010"
      end
    rescue Exception =>e 
      system_jab_id = nil 
      snomed_id_for_6_in_1 = nil
      Rails.logger.info "Error inside get_6_in_1_vaccination_mapped_system_jab ......#{e.message}"
    end
    [system_jab_id,snomed_id_for_6_in_1]
  end


   # SystemVaccin.check_for_combined_vaccine(current_user,member,gpsoc_immunisation_jabs,options)
  def self.check_for_combined_vaccine(current_user,member,nhs_immunisation_jabs_group_by_selected_field,options={})
    country_code = Vaccination::Country[(current_user.user.country_code || "UK" ).downcase] || "UK"
    sanitize_country_code = Member.sanitize_country_code(country_code)
 
    system_combined_vaccine = []
    system_combined_vaccine_mapping_ids = {}
    ::System::GpsocImmunisationJab.where(:country_code.in=>sanitize_country_code,combined_vaccine:true).map do |gpsoc_immunisation_jab|
      tpp_term = gpsoc_immunisation_jab.tpp_term.split("+").map(&:strip)
      system_combined_vaccine << tpp_term
       system_combined_vaccine_mapping_ids[tpp_term.join('+')]  = gpsoc_immunisation_jab.tpp_term
    end 
    #  To check 6-in-1 , 4-in-1 in order
    system_combined_vaccine = system_combined_vaccine.sort_by{|a| a.length}.reverse
    
    system_combined_vaccine.each do |combined_vaccine|
      matched_jabs_term = nhs_immunisation_jabs_group_by_selected_field.values_at(*combined_vaccine).flatten.compact
      # Check all jab matched
      if matched_jabs_term.present? && matched_jabs_term.count == combined_vaccine.count
        # Delete from hash data and add new combine jab data
        data = matched_jabs_term.first
        combined_vaccine.each do |match_jab_term|
          nhs_immunisation_jabs_group_by_selected_field.delete(match_jab_term)
        end
        
        tpp_term = system_combined_vaccine_mapping_ids[combined_vaccine.join('+')]
        data[:term] = tpp_term 
        data[:name] = tpp_term 
        data[:tpp_term] = tpp_term 
        nhs_immunisation_jabs_group_by_selected_field[tpp_term] = [data]
      end
    end
    nhs_immunisation_jabs_group_by_selected_field
   end

  def get_category(options={})
    system_vaccine = self
    return system_vaccine.vaccin_type if options[:immunisation_version].to_i < 1    
    return "Custom" if system_vaccine.member_id.present?
    system_vaccine_category = "Selective"
    system_vaccine_category = ::SystemVaccin::VaccineTypeMapping[system_vaccine.vaccin_type] || system_vaccine_category

    system_vaccine_category       
  end

  def self.add_system_vaccine_to_member(member,system_vaccine_ids,options={})
    system_vaccine_ids = system_vaccine_ids.map(&:to_s)
    current_user = options[:current_user]
    system_mapped_nhs_data = {}
    data_source = "nurturey"
    if options[:sync_with_gpsoc] == true
      if member.is_user_fully_linked_with_clinic?(current_user,options)
        clinic_detail = member.get_clinic_detail(current_user,options)
        data_source = clinic_detail.is_emis?(options) ? "emis" : clinic_detail.clinic_type.to_s.downcase rescue nil
        nhs_vaccine, system_mapped_nhs_data,clinic_req_status = SystemVaccin.get_nhs_immunisation_jabs(current_user,member,options)
      end 
    end
    system_vaccine_ids.each do |system_vaccine_id|
      vaccine = Vaccination.where(sys_vaccin_id:system_vaccine_id,member_id:member.id).first
      if vaccine.present?
        vaccine.update_attribute("opted","true")
      else
        system_vaccine = SystemVaccin.find(system_vaccine_id)
        vaccination = Vaccination.new(name:system_vaccine.name, member_id:member.id,sys_vaccin_id:system_vaccine.id, :opted=>"true",category:system_vaccine.category)
        if vaccination.save
          if (system_mapped_nhs_data[vaccination.sys_vaccin_id.to_s].present? rescue false)
            system_mapped_nhs_jabs = system_mapped_nhs_data[vaccination.sys_vaccin_id.to_s] || []
          end
          jab_list = system_vaccine.system_jabs 
          jab_list = jab_list.order_by("id asc")
          if jab_list.blank?
            jab_list = [Jab.new]
          
          end
          jab_list.each do |jab|
            if system_vaccine.schedule_updated != true 
              valid_to_add  =  true 
            else
              if(member.birth_date >= "01-Jan-2020".to_date && jab.given_after_birth_date.present? &&  jab.given_after_birth_date.to_date >=  "01-Jan-2020".to_date rescue false)
                valid_to_add  =  true 
              elsif (member.birth_date < "01-Jan-2020".to_date && (jab.given_after_birth_date.blank? || jab.given_after_birth_date.to_date <  "01-Jan-2020".to_date rescue false) )
                valid_to_add  =  true 
              else
                valid_to_add  =  false
              end
            end
            if valid_to_add
              system_jab_id = jab.id.to_s
              nhs_jab = (system_mapped_nhs_jabs||[]).select{|a| a[:system_jab_id] == system_jab_id.to_s }.first || {}
              if nhs_jab.present?
                due_on_from_data_source = nhs_jab[:due_on]
                data_source = data_source
              else
                due_on_from_data_source = nil
                data_source = "nurturey"
              end
              due_on = member.birth_date + eval(jab.due_on) rescue nil
              if due_on.present?
                st_date = due_on.to_date
                start_at  = st_date.to_time1("09:00")
                end_at =   st_date.to_time1("10:00")
              end
              fullday =  true
              Jab.new(due_on_from_data_source:due_on_from_data_source,data_source:data_source, system_jab_id:system_jab_id, start_time: start_at, end_time: end_at, fullday: fullday, vaccination_id:vaccination.id, name:jab.name,est_due_time:due_on , jab_type: (::SystemVaccin::CategoryMapping1[jab.jab_type]), route:jab.route, jab_location:jab.jab_location).save
            end
          end
        end 
      end
    end
    vaccinations = member.vaccinations.not_in(:opted => "false").only(:id,:name,:sys_vaccin_id).entries
    # Update flu vaccination jab due date
    SystemVaccin.flu_vaccine_due_date(current_user,member,vaccinations,options)
    family_id = FamilyMember.where(member_id: member.id).first.family_id
    Score.where(activity_type: "Vaccination", :activity_id.in => member.vaccinations.pluck(:id)).delete_all
    score_data = []
    vaccinations.each do |vaccine|
      score_data << {family_id: family_id, activity_type: "Vaccination", activity_id: vaccine.id, activity: "Added  " + vaccine.get_name, point: Score::Points["Vaccination"], member_id: member.id}
    end
    Score.create(score_data)
    vaccinations
  end
  
  def self.flu_vaccine_due_date(current_user,member,vaccinations,options={})
    begin
      systme_flu_vaccine_ids = SystemVaccin.where(name:"Flu").where(member_id:nil).pluck(:id).map(&:to_s)
      flu_vaccine = vaccinations.select{|a| systme_flu_vaccine_ids.include?(a.sys_vaccin_id.to_s) }.last
      jabs = flu_vaccine.jabs 
      if(member.birth_date.month <= 8)
        offset_year = 0
      else
        offset_year = 1
      end
      jabs.each_with_index do |jab,index|
        system_jab = SystemJab.find(jab.system_jab_id)
        due_on  = "15-10-#{member.birth_date.year + offset_year}"
        due_on = due_on.to_date + eval(system_jab.due_on)
        st_date = due_on.to_date
        start_at  = st_date.to_time1("09:00")
        end_at =   st_date.to_time1("10:00")
        jab.update_attributes({start_time: start_at, end_time: end_at, est_due_time:due_on })
      end
    rescue Exception => e
      Rails.logger.info "Error inside flu_vaccine_due_date ............#{e.message}"
    end
  end

# For admin Panel


  def self.parse_nhs_content_id(autocomplete_nhs_syndicate_content_string,options={})
    nhs_content_custom_ids = []
     
    if autocomplete_nhs_syndicate_content_string.present?
      nhs_content_values = autocomplete_nhs_syndicate_content_string.split("\;")

      nhs_content_values.each do |nhs_content_value|
        nhs_content_custom_ids << nhs_content_value.split(",").first.split(":").last.strip
      end
      nhs_content_custom_ids = nhs_content_custom_ids.compact
      Nhs::SyndicatedContent.where(:custom_id.in=>nhs_content_custom_ids).pluck(:id).join(",")
    else
      nil
    end
  end
  def update_record(params)
    begin
      vaccine_params = params["SystemVaccin".underscore]
       
      nhs_syndicate_content_id = SystemVaccin.parse_nhs_content_id(params[:nhs_syndicate_content_id])
      # vaccine_params[:nhs_syndicate_content_id] = nhs_syndicate_content_id
      system_vaccine = self
      if system_vaccine.update_attributes(vaccine_params)
        status = true
      else
        status = false
      end
    rescue Exception => e
      Rails.logger.info e.message
      status = false 
    end
    [system_vaccine,status]
  end


  def self.create_record(params)
    begin
      vaccine_params = params["SystemVaccin".underscore]
       
      vaccine_params[:name] = vaccine_params[:name].strip rescue vaccine_params[:name]
       nhs_syndicate_content_id = SystemVaccin.parse_nhs_content_id(params[:nhs_syndicate_content_id])
      # vaccine_params[:nhs_syndicate_content_id] = nhs_syndicate_content_id
   
      system_vaccine = self.new(vaccine_params)
      system_vaccine.save!
      (0...system_vaccine.jabs_count.to_i).each do
        SystemJab.new(due_on:"0.days",due_on_in_day_int:0, system_vaccin_id:system_vaccine.id).save
      end
       
      status = true
    rescue Exception => e
      Rails.logger.info e.message
      status = false 
    end
    [system_vaccine,status]
  end
  
  def self.duplicate_record(params)
    record_to_be_duplicated = SystemVaccin.find(params[:record_id]).attributes
    record_to_be_duplicated.delete("_id")
    record_to_be_duplicated.delete("created_at")
    system_vaccine = SystemVaccin.new(record_to_be_duplicated)
    if system_vaccine.save!
      (0...system_vaccine.jabs_count.to_i).each do
        SystemJab.new(due_on:"0.days",due_on_in_day_int:0, system_vaccin_id:system_vaccine.id).save
      end
    end
    system_vaccine
  end
  def self.delete_record(record_id)
    begin
      system_vaccine = SystemVaccin.find(record_id)
      SystemJab.where(system_vaccin_id:system_vaccine.id).delete_all
      system_vaccine.delete
    rescue Exception => e
      Rails.logger.info e.message
    end
  end
  

  def self.get_jab_data(system_vaccine)
    data = system_vaccine.system_jabs
    fields = SystemJab.attribute_names
    fields.delete("_id")
    fields.delete("created_at")
    fields.delete("facility")
    fields.delete("name")
    fields.delete("note")
    fields.delete("due_on_in_day_int")
    fields.delete("system_vaccin_id")
    fields.delete("updated_at")
    [data,fields]
  end

  def self.duplicate_all_records(params)
    begin
      raise "Already added data for selected country" if SystemVaccin.where(:country_code.in=> Member.sanitize_country_code(params[:destination_country_code])).present?
      sanitize_country_code = Member.sanitize_country_code(params[:source_country_code])
      system_vaccine_list = SystemVaccin.where(:country.in=>sanitize_country_code )
      system_vaccine_list.each do |system_vaccine|
        vaccine_params = {:record_id=>system_vaccine.id}
        # vaccine_params["country_code"] = params[:destination_country_code]
        system_vaccine_obj = SystemVaccin.duplicate_record(vaccine_params)
        system_vaccine_obj.country = params[:destination_country_code]
        system_vaccine_obj.save
      end
      status = true
      update_member_system_vaccine_for_added_country(params[:destination_country_code])
      message = ""
    rescue Exception=> e 

      Rails.logger.info "Error in SystemVaccin.duplicate_all_records"
      Rails.logger.info e.message
      status = false
      message = e.message
    end
    [status,message]
  end
  
  def self.in_country_code(options={})
    country_code = options[:country_code]
    country_code = (country_code || "GB") rescue "GB"
    country_code = SystemVaccin.distinct(:country).map(&:downcase).include?(country_code.downcase)? country_code : "GB"
    sanitize_country_code = Member.sanitize_country_code(country_code)
    where(:country.in=>sanitize_country_code)
  end

  def self.update_member_system_vaccine_for_added_country(country_code)
    country_code = country_code.upcase
    Vaccination.distinct(:member_id).each do |child_id|
      begin
        puts "child_id"
        child = ChildMember.find(child_id)
        puts child.first_name
        puts child_id
        member = child
        member_country_code = User.where(member_id:child.family_mother_father.first.to_s).first.country_code.upcase #rescue "GB"
        if member_country_code == country_code
          options = {:country_code=>country_code}
          Vaccination.where(member_id:child_id.to_s).each do |vaccine|
            system_vaccine = SystemVaccin.find(vaccine.sys_vaccin_id.to_s)
            existing_system_vaccine_for_cloned_country = SystemVaccin.where(name:system_vaccine.name).in_country_code(options).last
            if existing_system_vaccine_for_cloned_country.present?
              vaccine.sys_vaccin_id = existing_system_vaccine_for_cloned_country.id
              vaccine.save
            end
          #   allergy_symptom_manager = AllergySymptomManager.where(:allergy_symptoms.in=>allergy.allergy_symptoms)
          #   existing_symptom_manager = AllergySymptomManager.where(:name.in=>allergy_symptom_manager.map(&:tittle)).in_country(nil,member_country_code)
          #   if existing_symptom_manager.present?
          #     allergy.allergy_symptoms = existing_symptom_manager.map(&:id)
          #     allergy.save
          #   end
          end
        end
      rescue Exception => e 
        puts e.message
        puts "Inside update_member_system_vaccine_for_added_country"
        Rails.logger.info "Inside update_member_system_vaccine_for_added_country"
        Rails.logger.info e.message
      end
    end
  end


end
