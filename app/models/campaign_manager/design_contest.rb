class CampaignManager::DesignContest
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paperclip
  
  field :name,                   :type => String # User to whom campaign mail sent
  field :email,                  :type => String # date on which campaign mail sent
  field :address,                :type => String # Mail sent to user
  field :no_of_children,         :type => Integer #Eg. True/False. User open mail 
  field :age_of_childrens,       :type => String #Eg. Date. User opened email on date 
  field :file,                   :type => Integer #Eg. 1..,n . How many time mail sent to user 
  field :term_of_participation,  :type => Boolean #terms for participations
  field :age_above_18_consent,   :type => Boolean #consnet for age above 16 as per gdpr

  validates :name, presence: true
  validates :local_image, presence: true
  validates_presence_of :email
  # validates_uniqueness_of :email
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i }
  

  paperclip_opts =    {
    :storage => :s3,
      :s3_credentials => {
        :bucket => APP_CONFIG["s3_bucket"],
        :access_key_id => APP_CONFIG["s3_access_key_id"],
        :secret_access_key => APP_CONFIG["s3_secret_access_key"]
      },
      :url => ':s3_domain_url',
      :path => lambda{ |f| '/:class' + '/:id/:style/:basename.:extension'},
      :s3_protocol => 'https'
  }
  has_mongoid_attached_file :file,paperclip_opts
  validates_attachment_content_type :file, :content_type => ["image/jpg", "image/jpeg", "image/png","image/bmp","image/tif"]
  
  has_mongoid_attached_file :local_image,
  :path => ":rails_root/public/assets/design_contest/:id/:style/:basename.:extension",
  :url  => "/assets/design_contest/:id/:style/:basename.:extension"


  after_save :queue_upload_to_s3

  before_file_post_process do |image|
    if image.local_image? && image.local_image_updated_at_changed?
      self.processing = true
      false # halts processing
    end
  end

  def queue_upload_to_s3
    Delayed::Job.enqueue(ImageJob.new(id, "CampaignManager::DesignContest"), :priority => 3, :run_at => 1.seconds.from_now) if local_image? && local_image_updated_at_changed? 
  end

  def upload_to_s3
    Rails.logger.info  "Design contest image uploading to s3"
    self.file = File.open(self.local_image.path, "r")
    save!
  end

  def self.create_record(data,options={})
    begin
      content = data.delete(:file)
      designcontest = CampaignManager::DesignContest.new(data)
      # designcontest.file = content
      designcontest.local_image = content
      designcontest.term_of_participation = true
      designcontest.age_above_18_consent = true
      designcontest.save!
      status = 200
      message = "Your entry has been successfully submitted. Thank you!"
    rescue Exception => e 
      Rails.logger.info "Error inside CampaignManager::DesignContest #{e.message}..........."
      status = 100
      message = 'Oops! Some Error Occur. Please try again later.'
    end
    {status:status,message:message,designcontest:designcontest}
  end

end