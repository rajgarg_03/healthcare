class CampaignManager::UninstallCallbackCampaign
  include Mongoid::Document
  field :member_id, :type => String # User to whom campaign mail sent
  field :last_mail_sent_date, :type=> Date # date on which campaign mail sent
  field :mail_sent_status, :type => Boolean # Mail sent to user
  field :user_respond_to_mail_status, :type => Boolean #Eg. True/False. User open mail 
  field :user_last_respond_date, :type => Date #Eg. Date. User opened email on date 
  field :mail_sent_count, :type => Integer #Eg. 1..,n . How many time mail sent to user 
  field :user_country_code, :type => String #Eg. user country code "uk, "in" # to improve performance
end