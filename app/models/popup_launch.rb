class PopupLaunch
  include Mongoid::Document
  include Mongoid::Timestamps

  field :member_id, type: String
  field :app_review_launch_count, type: Integer
  field :app_referral_launch_count1, type: Integer, :default=>1
  field :nook_launch_count, type: Integer, :default=>1
  field :mental_math_launch_count, type: Integer # popup for Alexa math is free now
  field :popup_info, type: Hash # {mental_math_free:{start_date:""},invite_friend_to_nurturey:{}}
  belongs_to :member
  index({member_id:1}) 
  
  # PopupLaunch.member_popup_status(current_user,family_list=nil,options={})
  def self.member_popup_status(current_user,family_list=nil,options={})
    popup_status = {}
    begin
      member_popup_launch = PopupLaunch.where(member_id:current_user.id).last || PopupLaunch.new
      popup_name_list = PopupLaunch.new.attribute_names - ["created_at","updated_at","_id","member_id","popup_info"]
      popup_name_list.each do |popup_name|
        popupname = "show_"+ popup_name.gsub("launch_count","popup")
        if member_popup_launch[popup_name].blank?
          popup_status[popupname] = false
            
        elsif member_popup_launch[popup_name] == 0
          popup_status[popupname] = true
        else
          popup_status[popupname] = false
        end
      end
    rescue Exception => e
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside popup launch member_popup_status")
        
    end
    popup_status
  end


  # options = {app_version,plateform}
  def self.update_mental_math_free_popup_launch_count(member_id,platform,count=nil,options={})
    member = Member.find(member_id)
    current_user = member
    mental_math_free_popup_settings = ::System::Settings.where(name:"mentalMathFreePopupLunch").last
    popup_is_inactive =  ((mental_math_free_popup_settings.blank? || mental_math_free_popup_settings.status != "Active"))
    if !popup_is_inactive && ::Child::AlexaQuiz::QuizDetail.is_mental_math_banner_valid?(current_user,options)
      device = member.devices.last
      required_ios_version = '2.3.2'
      required_android_version = '2.2.8'
      device_is_ready = device.check_device_is_upto_date?(platform,options[:app_version],required_ios_version, required_android_version, options)
      if options[:skipped].blank? && !device_is_ready
        return nil
      end
      member_popup = PopupLaunch.where(member_id:member_id).last || PopupLaunch.new(member_id:member_id)
      member_popup_info = member_popup.popup_info  || {}
      mental_math_free_popup = member_popup_info["mental_math_free"] || {}
      if count.present? || options[:skipped].present?
        member_popup["mental_math_launch_count"] = count || 1
        member_popup_info["mental_math_free"] = member_popup_info["mental_math_free"].present? ? member_popup_info["mental_math_free"] : {start_date:Date.today} 
        if options[:skipped] == true
          member_popup_info["mental_math_free"]["skipped"] = true
        end
        member_popup.popup_info =  member_popup_info
        member_popup.save
      elsif member_popup["mental_math_launch_count"].nil? || mental_math_free_popup.blank?
        member_popup["mental_math_launch_count"] = 0
        member_popup_info["mental_math_free"] = {start_date:Date.today} 
        member_popup.popup_info =  member_popup_info
        member_popup.save
      else
        # popup_setting = mental_math_free_popup_settings.setting_values.with_indifferent_access
        # popup_expire_in_days = (popup_setting[:expire_in_days]).to_i
        # if ((mental_math_free_popup["start_date"] + popup_expire_in_days.to_i.days) <= Date.today rescue false)
        if member_popup["mental_math_launch_count"] == 0
          member_popup["mental_math_launch_count"] = 1
          member_popup.save
        end
      end
        
      if member_popup["mental_math_launch_count"] == 0 
        report_event = Report::Event.where(name:'Show mental math free popup').last
        ::Report::ApiLog.log_event(current_user.user_id,family_id=nil,report_event.custom_id,options) rescue nil
      end
    end
  end

  # PopupLaunch.update_app_review_popup_launch_count(member_id,count=nil,options={})
  def self.update_app_review_popup_launch_count(member_id,count=nil,options={})
    app_review_popup_settings = System::Settings.where(name:"appReviewPopupLunch").last
    current_user = Member.find(member_id)
    user = current_user.user
    member_app_review_popup = PopupLaunch.where(member_id:member_id).last
    popup_setting = app_review_popup_settings.setting_values.with_indifferent_access
    
    popup_signin_frequency = (popup_setting[:signin_frequency] || 10).to_i
    user_stage_setting = [popup_setting[:user_stage]].flatten.compact.map(&:downcase) rescue []
    per_day_limit = popup_setting[:per_day_limit]
    options = {:start_date=>DateTime.now.beginning_of_day,:end_date=>DateTime.now.end_of_day,:get_count=>true}
    popup_shown_count_for_current_date = ::Report::ApiLog.get_log_event(39,user_id=nil,family_id=nil,options)
    
    # return true if (app_review_popup_settings.blank? || app_review_popup_settings.status =! "Active") && user.user_type.downcase == "general"
    popup_is_inactive =  ((app_review_popup_settings.blank? || app_review_popup_settings.status != "Active") && user.user_type.downcase == "general")
    
    # return true if popup_shown_count_for_current_date >= popup_setting[:per_day_limit].to_i
    if popup_setting[:per_day_limit].present?
      popup_count_limit_exceeded = popup_shown_count_for_current_date >= popup_setting[:per_day_limit].to_i
    else
      popup_count_limit_exceeded = false
    end
    if popup_is_inactive || popup_count_limit_exceeded
      if member_app_review_popup.app_review_launch_count == 0
        member_app_review_popup.update_attribute(:app_review_launch_count,1)
      end
      return true
    end

    if member_app_review_popup.blank?
      member_app_review_popup = PopupLaunch.new(member_id:member_id)
      member_app_review_popup.save
    end
    
    if user_stage_setting.present?
      if !user_stage_setting.include?(current_user.user.segment.downcase)
        member_app_review_popup.update_attribute(:app_review_launch_count,1)
        return true
      end
    end
    
    launch_count = member_app_review_popup.app_review_launch_count || 0
    if launch_count.blank? || launch_count < (popup_signin_frequency - 1)
      launch_count = launch_count.to_i + 1
    else
      launch_count = 0
    end
    if launch_count == 0 
      report_event = Report::Event.where(name:"show app review popup").last
      ::Report::ApiLog.log_event(current_user.user_id,family_id=nil,report_event.custom_id,options) rescue nil
    end
    member_app_review_popup.update_attribute(:app_review_launch_count,launch_count)
  end

  # Invite frinet o nutruey popup count
  # PopupLaunch.update_app_referral_popup_launch_count(member_id,count=nil,options={})
  def self.update_app_referral_popup_launch_count(member_id,count=nil,options={})
    begin
      current_user = Member.find(member_id)
      user = current_user.user
      app_referral_popup_settings = System::Settings.where(name:"appReferralPopupLunch").last
      popup_is_inactive =  app_referral_popup_settings.blank? || app_referral_popup_settings.status != "Active"
      # popup_is_inactive =  ((app_referral_popup_settings.blank? || app_referral_popup_settings.status != "Active") && user.user_type.downcase == "general")
      
      popup_setting = app_referral_popup_settings.setting_values.with_indifferent_access rescue {}
      member_app_referral_popup = PopupLaunch.where(member_id:member_id).last
      options = options.merge({:start_date=>DateTime.now.beginning_of_day,:end_date=>DateTime.now.end_of_day})
      
      if member_app_referral_popup.blank?
        member_app_referral_popup = PopupLaunch.new(member_id:member_id)
        member_app_referral_popup.save
      end

      if popup_is_inactive  
        if member_app_referral_popup.app_referral_launch_count.to_i == 0
          member_app_referral_popup.update_attribute(:app_referral_launch_count,1)
        end
        return true
      end
      popup_info = (member_app_referral_popup.popup_info  || {}).with_indifferent_access

      # Update if skipped
      if options[:app_refferal] == true || options[:skipped].present?
        member_app_referral_popup["app_referral_launch_count"] =   1
        popup_info["app_referral"] = popup_info["app_referral"].present? ? popup_info["app_referral"] : {} 
        if options[:skipped] == true
          popup_info["app_referral"]["skipped_date"] = Date.today + popup_setting["skip_for_days"].to_i.days
          report_event = ::Report::Event.where(name:"skip app referral popup").last
          ::Report::ApiLog.log_event(current_user.user_id,family_id=nil,report_event.custom_id,options) rescue nil
        end
        if options[:app_refferal] == true
          popup_info["app_referral"]["refferal_date"] = Date.today + popup_setting["app_refferal_skip_for_days"].to_i.days
        end

        member_app_referral_popup.popup_info =  popup_info
        member_app_referral_popup.save
        popup_info = (member_app_referral_popup.popup_info  || {}).with_indifferent_access
        return true

      end


      current_user = Member.find(member_id)
      user = current_user.user
      signin_frequency = (popup_setting[:signin_frequency] || 2).to_i
      dismissed_for_days = (popup_setting[:skip_for_days] || 14).to_i
      app_referral_days =  (popup_setting[:app_refferal_skip_for_days] || 30).to_i
      popup_info = popup_info["app_referral"].with_indifferent_access rescue {}
      # Check family created
      family_created = current_user.user_family_ids.present?
      
      # Check signin count
      signin_frequency_status = user.sign_in_count >=  signin_frequency
      
      # Check action card/popup dismissed
      if popup_info[:skipped_date].present?
        skipped_date_expired = popup_info[:skipped_date].to_date + dismissed_for_days.day < Date.today
      else
        skipped_date_expired = true
      end

      # Check user reffered his/her friend
      if popup_info[:refferal_date].present?
        refferal_time_expired = popup_info[:refferal_date].to_date + app_referral_days.day < Date.today
      else
        refferal_time_expired = true
      end
      
      launch_status = signin_frequency_status && family_created && refferal_time_expired && skipped_date_expired
      
       
      if launch_status
        # member_app_referral_popup.update_attribute(:app_referral_launch_count,0)
        report_event = ::Report::Event.where(name:"show app referral popup").last
        ::Report::ApiLog.log_event(current_user.user_id,family_id=nil,report_event.custom_id,options) rescue nil
      elsif !launch_status#  && member_app_referral_popup.app_referral_launch_count.to_i == 0
        # member_app_referral_popup.update_attribute(:app_referral_launch_count,1)
      end
      
    rescue Exception => e
      Rails.logger.info "Error inside .......update_app_referral_popup_launch_count ........ #{e.message}"
    end

  end

  # Nook popup Launch
  # PopupLaunch.update_nook_popup_launch_count(member_id,count=nil,options={})
  def self.update_nook_popup_launch_count(member_id,count=nil,options={})
    begin
      api_version = options[:api_version] || 6
      options = options.merge({:start_date=>DateTime.now.beginning_of_day,:end_date=>DateTime.now.end_of_day})
      current_user = Member.find(member_id)
      user = current_user.user
      
      system_popup = ::System::Settings.where(name:"nookPopupLunch").last
      popup_is_inactive =  system_popup.blank? || (system_popup.status != "Active" && !user.is_internal?)
      
      popup_system_setting = system_popup.setting_values.with_indifferent_access rescue {}
      member_popup = PopupLaunch.where(member_id:member_id).last
      
      # save new record for member popup if not exist
      member_popup = PopupLaunch.create(member_id:member_id) if member_popup.blank?
      is_nook_enable = ::System::Settings.isNookEnabled?(current_user,api_version,options)
      
      if popup_is_inactive  || !is_nook_enable
        if member_popup.nook_launch_count.to_i == 0
          member_popup.update_attribute(:nook_launch_count,1)
        end
        return true
      end
      popup_info = (member_popup.popup_info  || {}).with_indifferent_access
      popup_info["nook"] = popup_info["nook"].present? ? popup_info["nook"] : {} 
      # Update if skipped || clicked
      if options[:nook_clicked] == true || options[:skipped].present?
        member_popup[:nook_launch_count] = 1
        if options[:skipped] == true
          popup_info["nook"]["skipped_date"] = Date.today + popup_system_setting["skip_for_days"].to_i.days
          report_event = ::Report::Event.where(name:"nook popup skipped").last

          ::Report::ApiLog.log_event(current_user.user_id,family_id=nil,report_event.custom_id,options) rescue nil
        end
        if options[:nook_clicked] == true
          report_event = ::Report::Event.where(name:"nook popup clicked").last
          ::Report::ApiLog.log_event(current_user.user_id,family_id=nil,report_event.custom_id,options) rescue nil
          popup_info["nook"]["click_on_date"] = Date.today
        end

        member_popup.popup_info =  popup_info
        member_popup.save
        return true
      end

      signin_frequency = (popup_system_setting[:signin_frequency] || 2).to_i
      dismissed_for_days = (popup_system_setting[:skip_for_days] || 14).to_i

      popup_info = (popup_info["nook"]||{}).with_indifferent_access rescue {}
      # Check signin count
      signin_frequency_status = user.sign_in_count >=  signin_frequency
      
       
      # Check popup dismissed
      if popup_info[:skipped_date].present?
        skipped_date_expired = popup_info[:skipped_date].to_date + dismissed_for_days.day < Date.today
      else
        skipped_date_expired = true
      end

      # Check user clicked on popup
      popup_clicked = popup_info[:click_on_date].present?
      launch_status = signin_frequency_status && !popup_clicked && skipped_date_expired

      if launch_status
        member_popup.update_attribute(:nook_launch_count,0)
        report_event = ::Report::Event.where(name:"show nook popup").last
        ::Report::ApiLog.log_event(current_user.user_id,family_id=nil,report_event.custom_id,options) rescue nil
      else
        member_popup.update_attribute(:nook_launch_count,1)
      end
      
    rescue Exception => e
      Rails.logger.info "Error inside .......update_nook_popup_launch_count ........ #{e.message}"
    end

  end


end