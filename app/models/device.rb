class Device
  include Mongoid::Document
  include Mongoid::Timestamps

  field :member_id, type: String
  field :token, type: String
  field :enabled, type: Boolean
  field :platform, type: String
  field :os_version, type: String
  field :device_model, type: String
  field :app_version, type: String
  field :build_number, type:String
  field :screen_skip_count, type: Integer
  field :screen_skip_up_to, type:Time
  field :app_version_in_number, :type=>Integer
  belongs_to :member
  validates_presence_of :member_id
  
  before_save :update_app_vesion_in_number
  
  index({member_id:1})
  index({platform:1})
  index({token:1})
  
  Screen_skip_day = 3
  validates_uniqueness_of :token, scope: :member_id
  

  def update_app_vesion_in_number
  begin
    device_app_version =  self.app_version    
    self.app_version_in_number =  app_version_conversion_from_str_to_int(device_app_version)
  rescue
  end
  end
  
  def app_version_conversion_from_str_to_int(app_version_in_string)
    digit_in_string = app_version_in_string.split(".")
    (digit_in_string[0].to_i * 1000000) + (digit_in_string[1].to_i * 1000) + (digit_in_string[2].to_i )
  end

  def switch_member(member)
  	update_attribute(:member_id, member.id)
  end

  def check_device_is_upto_date?(platform,device_app_version,req_ios_app_version,req_android_app_version,options={})
    device = self 
    screens = []
    status = true
    os_version = device.os_version.to_i
    platform = platform.to_s.downcase
    device_app_version_in_number = Device.new.app_version_conversion_from_str_to_int(device_app_version) rescue 0
    #IOS
    app_version_ios = Device.new.app_version_conversion_from_str_to_int(req_ios_app_version)
    if(device_app_version_in_number.to_i < app_version_ios && platform == "ios")
      status = false
    end

    # Android
    app_version_android = Device.new.app_version_conversion_from_str_to_int(req_android_app_version)
    if(device_app_version_in_number.to_i < app_version_android && platform == "android")
      status = false
    end
    status
  end

end
 