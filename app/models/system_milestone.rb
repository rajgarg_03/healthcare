class SystemMilestone
  include Mongoid::Document
  include Mongoid::Timestamps  
  # include Mongoid::Paperclip

  field	:title,                    :type => String
  field :description,              :type => String  
  field :dynamic_title,      :type => String  
  field :milestone_type,           :type => String
  field	:category,                 :type => String
  field :default_image_exists,     :type => Boolean
  field :sequence,                 :type => Integer
  field :country_code, :type=>String
  field :important_status,         :type => Integer,default: 3 # 1=>high,2=>medium,3=>low
  field :milestone_question,       :type => String
  field :encourage,                :type => String # pointer link/connection
  field :faq,                      :type => String # Link to a FAQ question etc
  field :notification_message,             :type => String # customise push/notification msg used for identifier 2
  
  # for formula calculation
  field :min_range_in_day, :type =>Integer #m
  field :max_range_in_day, :type =>Integer #n
  field :n2_m2, :type =>Float #C
  field :n2_2m2, :type =>Float #D
  field :data_source, :type=> String, :default=>"nurturey"
  validates :title, :presence => true
  
  has_many :milestones
  
  index({title:1})
  index({category:1})
  index({sequence:1})
  index({country_code:1})
  index({important_status:1})
  
  ImprotantStatus = {1=>"High",2=>"Medium",3=>"Low"}

  def seed_data_to_calculate_progress_scale_value(power=2) #,cf1=0,cf2=10,factor=1,scale=10)
    begin
      system_milestone = self
      system_milestone.max_range_in_day = system_milestone.category.split('-').last.to_i * 30
      system_milestone.min_range_in_day  = system_milestone.category.split('-').first.to_i * 30
      
      n2 = system_milestone.max_range_in_day ** power
      m2 = system_milestone.min_range_in_day ** power
      system_milestone.n2_m2 = (n2 - m2).to_f
      system_milestone.n2_2m2 = (n2 - (2 * m2)).to_f
      system_milestone.save
    rescue Exception=> e
      puts e.message
      Rails.logger.info e.message
    end
  end
  
  def is_important?
    system_milestone = self
    (system_milestone.important_status < 2) #|| system_milestone.important_status == true 
  end

  def self.milestones_with_important_status
    where(:important_status.lt=>2)
    # where("$or"=>[{:important_status=>true},{:important_status.lt=>3}]
  end

  def self.order_by_important_status
    order_by("important_status asc")
  end

  def self.save_data_to_calculate_progress_scale_value  #(,cf1=0,cf2=10,factor=1,scale=10)
    power = GlobalSettings::MilestoneProgressCalculationFactors[:power]
    SystemMilestone.each do |system_milestone|
      system_milestone.seed_data_to_calculate_progress_scale_value(power) 
    end
  end   
  #has_many :pictures, :as => :imageable, :dependent => :destroy
  # has_and_belongs_to_many :members

  # CATEGORIES =  ["0-2", "2-4", "4-6", "7-9", "9-12", "13-18", "19-24", "24-36", "37-48", "49-60"]
  CATEGORIES_RANGE =  {(0..2) => "0-2", (2..4) => "2-4", (4..6) => "4-6", (7..9) => "7-9", (9..12) => "9-12", (13..18) => "13-18", (19..24) => "19-24", (24..36) => "24-36", (37..48) => "37-48", (49..60) => "49-60"}
  CATEGORIES_ORDER =  {"0-2"=>1, "2-4"=> 2,  "4-6" => 3,"7-9"=>4 , "9-12"=>5, "13-18"=>6,  "19-24" =>7, "24-36" =>8 , "37-48" => 9,"49-60" =>10}
  #MILESTONES_COUNT = SystemMilestone.milestones_count_order_by_category  #[10, 15, 14, 12, 13, 15, 17, 19, 15, 15]
  # SYSTEM_MILESTONE_TITLES = YAML.load_file("#{Rails.root}/config/system_milestones.yml")
  SystemMilestoneDescription = {"Cognitive Development"=>{:title=>"Cognitive",:description=>"Cognitive development is child's development in terms of information processing, conceptual resources, perceptual skill, language learning and other aspects of brain development. Its the emergence of the ability to think and understand"}, "Language/Communication"=>{:title=>"Language/Communication",:description=>"Language development supports your child's ability to communicate, and express and understand feelings. It also supports thinking and problem-solving, and developing and maintaining relationships"}, "Social & Emotional"=>{:title=>"Social & Emotional",:description=>"Social-emotional development includes the child's experience, expression, and management of emotions and the ability to establish positive and rewarding relationships with others. It encompasses both intra and interpersonal processes."}, "Movement/Physical Development"=>{:title=>"Physical",:description=>"Physical Development involves developing control over the body, particularly muscles and physical coordination. It involves learning activities such as grasping, writing, crawling, and walking."}}  
  

  def in_country_code(options={})
    country_code = options[:country_code].present? ? options[:country_code] : "GB"
    sanitize_country_code = Member.sanitize_country_code(country_code)
    if SystemMilestone.in(country_code:sanitize_country_code).blank?
       sanitize_country_code = Member.sanitize_country_code("GB")
    end
    sanitize_country_code
  end
  
  def self.in_country_code(options={})
    country_code = SystemMilestone.new.in_country_code(options)
    where(:country_code.in=>country_code)
  end
  
  def self.milestone_count(options={})
    sequence = nil
    if options[:sequence].present?
      sequence = options[:sequence]
    end
    count_data = SystemMilestone.get_milestone_count_group_by_category(sequence,options)
    count = []
    SystemMilestone::CATEGORIES_ORDER.keys.each do |category|
      count << count_data[category]
    end
    count
  end

  def self.system_milestone_for_member(child_member,options={})
    begin
      category = child_member.find_category(options)
      country_code = child_member.get_country_code
      options = options.merge({:country_code=>country_code})
      system_milestone_added_ids = child_member.milestones.pluck(:system_milestone_id)
      system_milestones_sequences =  SystemMilestone.in_country_code(options).in(id:system_milestone_added_ids).pluck(:sequence)
      if category.blank?
        milestone_sequence = -1
      else
        milestone_sequence = SystemMilestone.in_country_code(options).where(category: category).first.sequence
      end

      if milestone_sequence == 1
        system_milestones = SystemMilestone.in_country_code(options).where({:sequence.lte => milestone_sequence, :sequence.nin => system_milestones_sequences}).order_by(:sequence => 'asc')
      else
        system_milestones = SystemMilestone.in_country_code(options).where({:sequence.lte => milestone_sequence, :sequence.nin => system_milestones_sequences}).order_by(:sequence => 'asc')
      end
    rescue Exception => e
      system_milestones = []
    end
    system_milestones
  end

  def milestone_accomplished_status(child)
    milestone = self
    expected_date = (system_milestone.get_expected_date(child) ).to_date
  
    #if milestone.date.blank? || milestone.milestone_status == "unknown"
    if expected_date.blank? 
      "can't tell"
    elsif expected_date == Date.today
      "On Track"
    elsif expected_date.to_date > Date.today
      "Behind"
    else
      "Ahead"
    end
  end 

  #NOTE: Override to_json method and provide extra data
  def as_json(options = {})
    json = super(options)
    if options[:member] && (milestone = options[:member].milestones.where(system_milestone_id: self.id).first)
      json['added'] = true
      json['milestone_date'] = milestone.date if milestone.date.present?
    end  
    json
  end
  
  def get_dynamic_title(child,child_male=nil)
    child_name = child.first_name
    child_male = child_male.present? ? child_male : child.male?
    subject_referer = child_male ? 'his' : 'her'
    object_referer = child_male ? 'him' : 'her'
    subject = child_male ? 'he' : 'she'
    system_milestone = self
    begin
      hash_data = {title=>system_milestone.dynamic_title}
      hash_data[title] %{:child_name => child_name, :subject => subject, :subject_referer => subject_referer, :object_referer => object_referer}
    rescue 
      ""   
    end
  end

  def self.save_handhold_data(member,system_milestones_data,options={})
    child_male_status = member.male?
    status = true
    if system_milestones_data.present?
      system_milestones_data.each do |milestone_data|        
        milestone = Milestone.find_or_initialize_by(member_id: member.id, system_milestone_id: milestone_data[:system_milestone_id])
        milestone.date = milestone_data[:date]
        milestone.milestone_status =   "accomplished" 
        milestone.child_male_status = child_male_status
        milestone.status_by = "system"
        milestone.child_male_status = child_male_status
        status = false unless milestone.save
      end
    end
    Milestones.where(:system_milestone_id.in => options[:delete_milestone_ids], :member_id=>member.id.to_s).destroy_all if options[:delete_milestone_ids].present?
    status
  end  

  def get_expected_date(child)
    upper_limit_month = category.split('-').last.to_i
    expected_date = child.birth_date + eval("upper_limit_month.month")
    expected_date = Date.today if expected_date > Date.today
    expected_date.to_s
  end


  def calculate_expected_date_with_upper_limit_range(child)
    upper_limit_month = category.split('-').last.to_i
    expected_date = child.birth_date + eval("upper_limit_month.month")
    expected_date.to_s
  end

  def calculate_expected_date_with_lower_limit_range(child)
    lower_limit_month = category.split('-').first.to_i
    expected_date = child.birth_date + eval("lower_limit_month.month")
    expected_date.to_s
  end
  
  def self.seed_dynamic_title(yml_hash_data)
    SystemMilestone.all.each do |system_milestone|
      system_milestone.dynamic_title = yml_hash_data[system_milestone.title]
      system_milestone.save
    end
  end

  def self.get_milestone_count_group_by_category(sequence=nil,options={})
    country_code = SystemMilestone.new.in_country_code(options)
    if sequence.present?
      hash_data =  SystemMilestone.collection.aggregate([
       { "$match" => { :sequence => {"$lte"=>sequence },:country_code=>{"$in"=>country_code }} },
      {"$group" => {
          "_id" => "$category",
           "count" => {"$sum" => 1} 
        }} ,
        {"$sort" => { "count" => 1}}
       ])
    else
      hash_data =  SystemMilestone.collection.aggregate([
       { "$match" => {:country_code=>{"$in"=>country_code }} },
      {"$group" => {
          "_id" => "$category",
           "count" => {"$sum" => 1} 
        }} ,
        {"$sort" => { "count" => 1}}
       ])
    end
    data = {}
    hash_data.each{|result| data[result["_id"] ] = result["count"] }
    data
  end 


  def self.get_milestone_count_group_by_type(sequence=nil,system_milestone_ids=[],options={})
    country_code = SystemMilestone.new.in_country_code(options)
    if sequence.present?
      hash_data =  SystemMilestone.collection.aggregate([
        { "$match" => { :sequence => {"$lte"=>sequence },:country_code=>{"$in"=>country_code }} },
        {"$group" => {
          "_id" => "$milestone_type",
           "count" => {"$sum" => 1}
        }} ,
        {"$sort" => { "count" => 1}}
       ])
    elsif system_milestone_ids.present?
      hash_data =  SystemMilestone.collection.aggregate([
      { "$match" => { "_id" => {"$in"=>system_milestone_ids },"country_code" =>{"$in"=>country_code }} },
      {"$group" => {
        "_id" => "$milestone_type",
         "count" => {"$sum" => 1}
      }} ,
      {"$sort" => { "count" => 1}}
     ])
  else
      hash_data =  SystemMilestone.collection.aggregate([
      { "$match" => {"country_code" =>{"$in"=>country_code }} },
      {"$group" => {
        "_id" => "$milestone_type",
         "count" => {"$sum" => 1}
      }} ,
      {"$sort" => { "count" => 1}}
     ])
  end
    data = {}
    hash_data.each{|result| data[result["_id"] ] = result["count"] }
    data
  end 

  def self.get_member_milestone_count_group_by_type(member_id,system_milestone_id=nil,options={})
    system_milestones_ids = system_milestone_id.nil? ? Milestone.where(member_id:member_id).pluck(:system_milestone_id) : system_milestone_id
    hash_data =  SystemMilestone.collection.aggregate([
      { "$match" => { "_id" =>{"$in"=> system_milestones_ids } }},
    {"$group" => {
        "_id" =>  "$milestone_type" ,
         "count" => {"$sum" => 1} 
      }} ,
      {"$sort" => { "count" => 1}}
     ]) 
    data = {}
    hash_data.each{|result| data[result["_id"] ] = result["count"] }
    data
  end  



  def self.get_member_milestone_count_group_by_category(member_id,system_milestone_id=nil)
    system_milestones_ids = system_milestone_id.nil? ? Milestone.where(member_id:member_id).pluck(:system_milestone_id) : system_milestone_id
    hash_data =  SystemMilestone.collection.aggregate([
      { "$match" => { "_id" =>{"$in"=> system_milestones_ids } }},
    {"$group" => {
        "_id" =>   "$category" ,
         "count" => {"$sum" => 1} 
      }} ,
      {"$sort" => { "count" => 1}}
     ]) 
    data = {}
    hash_data.each{|result| data[result["_id"] ] = result["count"] }
    data
  end  


  def self.get_achieved_member_milestone_count_group_by_category(member_id,system_milestone_id=[])

    system_milestones_ids = system_milestone_id.blank? ? Milestone.where(member_id:member_id).pluck(:system_milestone_id) : system_milestone_id
    hash_data =   Milestone.collection.aggregate([
      { "$match" => { "_id" =>{"$in"=> system_milestones_ids } }},
    {"$group" => {
        "_id" =>   "$category" ,
         "count" => {"$sum" => 1} 
      }} ,
      {"$sort" => { "count" => 1}}
     ]) 
    data = {}
    hash_data.each{|result| data[result["_id"] ] = result["count"] }
    data
  end  
  

  def self.get_member_milestone_group_by_type(member_id,system_milestone_id=[])
    system_milestones_ids = system_milestone_id.blank? ? Milestone.where(member_id:member_id).pluck(:system_milestone_id) : system_milestone_id
    hash_data =   SystemMilestone.collection.aggregate([
      { "$match" => { "_id" =>{"$in"=> system_milestones_ids } }},
    {"$group" => {
        "_id" =>   "$milestone_type" ,
        "data" => {"$push"=> "$_id"}
      }}  
     ]) 
    data = {}
    hash_data.each{|result| data[result["_id"] ] = result["data"] }
    data
  end  



  # Used for admin panel
   def update_record(params)
    begin
       system_milestone = self
      milestone_params = params["SystemMilestone".underscore]
       power = GlobalSettings::MilestoneProgressCalculationFactors[:power]
      if  system_milestone.update_attributes(milestone_params)
        # update notification message for existing milestone without notification message
        SystemMilestone.where(title:system_milestone.title,:notification_message.in=>[nil,""]).update_all(notification_message:system_milestone.notification_message)
        system_milestone.seed_data_to_calculate_progress_scale_value(power)
        status = true
      else
        status = false
      end
    rescue Exception => e
      Rails.logger.info e.message
      status = false 
    end
    [ system_milestone,status]
  end


  def self.create_record(params)
    begin
      milestone_params = params["SystemMilestone".underscore]
      system_milestone = SystemMilestone.new(milestone_params)
      system_milestone.save!
      power = GlobalSettings::MilestoneProgressCalculationFactors[:power]
      system_milestone.seed_data_to_calculate_progress_scale_value(power)
      status = true
    rescue Exception => e
      Rails.logger.info e.message
      status = false 
    end
    [ system_milestone,status]
  end

  def self.duplicate_record(params)
    record_to_be_duplicated = SystemMilestone.find(params[:record_id]).attributes
    record_to_be_duplicated.delete("_id")
    record_to_be_duplicated.delete("created_at")
    record = SystemMilestone.new(record_to_be_duplicated)
    record.save
    [record,nil]
  end
  
   

  def self.duplicate_all_records(params)
    begin
      raise "Already added data for selected country" if SystemMilestone.where(:country_code.in=> Member.sanitize_country_code(params[:destination_country_code])).present?
      sanitize_country_code = Member.sanitize_country_code(params[:source_country_code])
      system_milestone_list = SystemMilestone.where(:country_code.in=>sanitize_country_code)
      system_milestone_list.each do |system_milestone|
        milestone_params = {:record_id=>system_milestone.id}
        system_milestone_obj,msg = SystemMilestone.duplicate_record(milestone_params)
        system_milestone_obj.country_code = params[:destination_country_code]
        system_milestone_obj.save
      end
      status = true
      update_member_system_milestone_for_added_country(params[:destination_country_code])
      message = ""
    rescue Exception=> e 

      Rails.logger.info "Error in SystemMilestone.duplicate_all_records"
      Rails.logger.info e.message
      status = false
      message = e.message
    end
    [status,message]
  end

  def self.delete_record(record_id)
    begin
       system_milestone = SystemMilestone.find(record_id)
       system_milestone.delete
    rescue Exception => e
      Rails.logger.info e.message
    end
  end

   def self.update_member_system_milestone_for_added_country(country_code)
    country_code = country_code.upcase
    Milestone.distinct(:member_id).each do |child_id|
      begin
        child = ChildMember.find(child_id)
        member = child
        member_country_code = User.where(member_id:child.family_mother_father.first.to_s).first.country_code.upcase #rescue "GB"
        if member_country_code == country_code
          Milstone.where(member_id:child_id.to_s).each do |milestone|
            system_milestone = SystemMilestone.find(milestone.system_milestone_id.to_s)
            existing_system_milestone_for_cloned_country = SystemMilestone.where(title:system_milestone.title).in_country_code({:country_code=>member_country_code}).last
            if existing_system_milestone_for_cloned_country.present?
              milestone.system_milestone_id = existing_system_milestone_for_cloned_country.id
              milestone.save
            end
          end
        end
      rescue Exception => e 
        puts e.message
        puts "Inside update_member_system_milestone_for_added_country"
        Rails.logger.info "Inside update_member_system_milestone_for_added_country"
        Rails.logger.info e.message
      end
    end
  end

  

end