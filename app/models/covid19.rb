# Related to corona virus
class Covid19
  def self.get_covid_message_data(current_user,options={})
    user = current_user.user
    system_setting = System::Settings.where(name:'showCovid19VirusUpdates').last
    if Member.sanitize_country_code(user.country_code).include?("uk")
      msg = system_setting.setting_values["uk"][(user.sign_in_count.to_i%6).to_s]
    else
      msg = system_setting.setting_values["other"][(user.sign_in_count.to_i%6).to_s]
    end
    data = {version_checker_notification_screen:msg}
  end

  def self.is_valid_for_user?(current_user,options={})
    UserLoginDetail.where(:login_time.gte=> (Date.today),user_id:current_user.user_id).count < 1
  end

  def self.is_active?
    system_setting = System::Settings.where(name:'showCovid19VirusUpdates').last
    status = (system_setting.present? && system_setting.status == "Active")
    status
  end
end