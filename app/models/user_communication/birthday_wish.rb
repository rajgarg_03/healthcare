class UserCommunication::BirthdayWish
	include Mongoid::Document
  field :user_id ,type:String # user to whom notification/email to be sent
  field :member_id ,type:String # child member id 
  # field :email_sent_on_date, type: Date
  # field :mail_sent_status ,type:Boolean #eg true/false
  field :push_sent_status ,type:Boolean # eg true/false
  field :schedule_push_sent_status, type: Boolean
  field :country_code ,type:String #eg. "in", "uk". user country code 
  field :push_sent_on_date, type: Date #instant push sent date
  field :send_push_on_schedule_on_date, type: Date # send push on schedule date
  
  field :name, type:String # Birthday mail name eg" First month birthday
  
  field :notification_message_for_instantly_push, type:String #{line1=>"text" ,line2=>pic, line3=>"text"}
  field :notification_message_for_schedule_push, type:String #{line1=>"text" ,line2=>pic, line3=>"text"}
  belongs_to :system_timeline
  
  CountryCodeListForBirthdayPush =  ["uk","UK","gb","GB",'in',"india","INDIA","IN","US","us"]#,"AE","ae","nz","NZ","CA","ca","ZA","za","SG","sg"]  

#UserCommunication::BirthdayWish.add_to_push_notification(current_user,child,age_in_month)
  def self.add_to_push_notification(user,child,age_in_month,push_msg_for_birthday_next_day=false)
    begin
      current_user = user.member
      system_timeline = SystemTimeline.find_by_age(age_in_month)
      birthday_wish_obj = UserCommunication::BirthdayWish.create_record(user,child,system_timeline,push_msg_for_birthday_next_day) if system_timeline.present?
      if birthday_wish_obj.present?
        UserNotification.add_birthday_wish_in_notification(current_user,birthday_wish_obj,push_msg_for_birthday_next_day)
      end
    rescue Exception=>e 
      Rails.logger.info e.message
    end
  end

  def update_record_for_schedule_push
    birthday_wish_obj = self
    if birthday_wish_obj.send_push_on_schedule_on_date == Date.today
      birthday_wish_obj.schedule_push_sent_status = true
    else
      birthday_wish_obj.push_sent_status = true
      birthday_wish_obj.push_sent_on_date = Date.today
    end
    birthday_wish_obj.save
  end

   def self.all_schedule_user_notification
     UserNotification.where(:type =>"birthday_wish", :status=>"pending").not_expired
   end
  
  def self.create_record(user,child,system_timeline,push_msg_for_birthday_next_day,birthday_wish_obj=nil)
    birthday_wish = birthday_wish_obj || UserCommunication::BirthdayWish.where(user_id:user.id.to_s,member_id:child.id.to_s, system_timeline_id:system_timeline.id.to_s).last
    birthday_wish = birthday_wish || UserCommunication::BirthdayWish.new(system_timeline:system_timeline.id.to_s, member_id:child.id, country_code:user.country_code, user_id:user.id, name:system_timeline.name)
    member = child
    # evalute push message  
    if birthday_wish.notification_message_for_instantly_push.blank?
      begin
        instantly_push_message = eval(system_timeline.notification_message_for_instantly_push)
      rescue Exception => e
        instantly_push_message = system_timeline.notification_message_for_instantly_push
      end
      birthday_wish.notification_message_for_instantly_push = instantly_push_message
    end

    if birthday_wish.notification_message_for_schedule_push.blank? 
      begin
        notification_message_for_schedule_push = eval(system_timeline.notification_message_for_schedule_push)
      rescue Exception => e
        notification_message_for_schedule_push = system_timeline.notification_message_for_schedule_push
      end
      push_date = push_msg_for_birthday_next_day ? Date.today : Date.today + 1.day
      birthday_wish.send_push_on_schedule_on_date = push_date
      birthday_wish.notification_message_for_schedule_push = notification_message_for_schedule_push
    end
    if birthday_wish.save
      birthday_wish
    else
      nil
    end
  end


  def self.add_weekly_birthday_wish_to_push_notification(user,child,age_in_week=1)
    begin
      current_user = user.member
      title = "#{age_in_week.to_i.ordinalize} week birthday"
      instantly_push_message = "Nurturey wishes #{child.first_name} a very happy #{age_in_week.to_i.ordinalize} week birthday. Have a great one!!"
      birthday_wish_obj = UserCommunication::BirthdayWish.new(  member_id:child.id, country_code:user.country_code, user_id:user.id, name:title)
      birthday_wish_obj.notification_message_for_instantly_push = instantly_push_message
      birthday_wish_obj.save

      if birthday_wish_obj.present?
        UserNotification.add_birthday_wish_in_notification(current_user,birthday_wish_obj,false)
      end
    rescue Exception=>e 
      Rails.logger.info e.message
    end
  end

end