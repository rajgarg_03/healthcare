class UserCommunication::UserActions
  include Mongoid::Document
  include Mongoid::Timestamps
  include UserCommunication::FamilyJoinNotification
   
  field :action_for_tool, type: String # eg:Family/Measurement/Timeline
  field :message, type: String # custom message for notification
  field :actor_id, type: String  # member id who updated action
  field :action_type, type: String  # eg: updated/created/deleted
  field :unique_id, type:String # eg: (id of action_for  + action_for) (measurment_id+ Measurement)
  field :object_id, type:String # member id to whom action to be  add
  field :action_expiry_date , type:Date # action expiry date
  field :family_id , type:String # Family id for which action is taken 
  field :child_id , type:String # child id for which action is taken 
  field :user_action_category , type:String # accept_join_family/reject_join_family/tool_updated 

   
  
  def self.add_user_action(push_identifier,action_record,current_user,msg,member_ids=[])
    #list of member id to whom action to be added
    begin
      @notification = BatchNotification::BatchNotificationManager.find_active_notification(push_identifier) 
      batch_notification_manager = @notification
      if @notification.present? && @notification.is_valid_notification?
        action_for_tool = @notification.destination
        action = @notification.action
        member_ids = member_ids.present? ? member_ids : current_user.family_elder_member_list.map(&:to_s)
        member_ids = member_ids.map(&:to_s) - [current_user.id.to_s]
        unique_id = UserCommunication::UserActions.set_unique_id(action_record,action_for_tool,action)
        begin
          action_taken_for = Member.find(action_record.member_id.to_s)
          child_id = action_taken_for.id.to_s
        rescue 
          child_id = nil
        end
        actor_id = current_user.id
        family_id = FamilyMember.where(member_id:child_id).pluck(:family_id).last.to_s
        
        #create new record if action is Add
        member_ids.each do |member_id|
          begin
            current_user = Member.find(member_id)
            # create user action record
            user_action =  UserCommunication::UserActions.create(:action_for_tool=>action_for_tool,
                :message=> msg,
                :actor_id=> actor_id,
                :action_type => action,
                :unique_id=> unique_id, 
                :object_id=>member_id,
                :family_id => family_id,
                :child_id => child_id
                 )

            # add user action in user notification 
            member = Member.find(member_id)
            push_type =  @notification.identifier
            tool_id = user_action.unique_id.split("_")[0]
            destination_name = @notification.destination
            batch_no = @notification.batch_no
            priority =  @notification.priority
            push_message = msg
            child_id = child_id
            action_type = action
            content_url = nil
            notification_expiry_date = Date.today + @notification.expire_in_no_of_day.to_i.days
            notification_obj = UserNotification.add_push(batch_notification_manager,current_user,push_type,batch_no,priority,push_message,destination_name,child_id,action_type,content_url,tool_id,notification_expiry_date,score=1)
            if notification_obj.present?
              UserNotification.push_notification(notification_obj,current_user,nil,nil,nil,{notification_type:"instant"})
            end
             
          rescue Exception=>e 
            Rails.logger.info e.message
          end
        end
      end
    rescue Exception=>e 
      add_user_action_logger = Logger.new('log/add_user_action_tool.log')
      add_user_action_logger.info e.message
    end
  end
  
  def self.set_unique_id(action_record,action_for_tool,action)
    action_record.id.to_s + "_" + action_for_tool + "_" + action
  end
  
  # Delete UserAction and Notification for action record
  def self.delete_user_action(action_record,action_for_tool,current_user)
    unique_id_for_add_action = UserCommunication::UserActions.set_unique_id(action_record,action_for_tool,"Add")
    unique_id_for_update_action = UserCommunication::UserActions.set_unique_id(action_record,action_for_tool,"Update")
    unique_id_list =  [unique_id_for_add_action,unique_id_for_update_action ]
    UserCommunication::UserActions.in(unique_id:unique_id_list).delete_all
    UserNotification.in(identifier:unique_id_list).where(type:"user_action",status:"pending").delete_all
  end


  

end



