module UserCommunication::FamilyJoinNotification
  
  def admin_approves_family_join_request(member,invitation)
    #notify user who wants to join family
    begin
      # user_joined_family = Member.find(invitation.member_id)
      # family = Family.find(invitation.family_id)
      # #elder_member_in_family = user_joined_family.family_elder_members.map(&:to_s) - [user_joined_family.id.to_s]
      # #members = Member.in(id:elder_member_in_family)
      # action_for_tool = "Manage Family"
      # action_record = invitation
      # action= "add"
      action,action_for_tool,unique_id,family,user_joined_family = get_member_data(member,invitation)

      message ="Yay, your request has been approved, welcome to #{family.name}. I'm the parenting PA on job, and will help you and your family with raising your little ones :)"
      #unique_id = UserCommunication::UserActions.set_unique_id(action_record,action_for_tool,action)
        user_action =  UserCommunication::UserActions.create(:action_for_tool=>action_for_tool,
            :message=> message,
            :actor_id=>member.id,
            :action_type => action,
            :unique_id=> unique_id, 
            :object_id=>user_joined_family.id,
            :family_id => family.id,
            :child_id => user_joined_family.id
             )
        UserNotification.add_user_action_in_notification(user_joined_family,user_action)
      rescue Exception=> e 
        Rails.logger.info e.message
      end
  end


  def family_owner_received_join_request(current_user,invitation)
    # notify family owner for join request
      begin
        action,action_for_tool,unique_id,family,user_to_be_joine_family = get_member_data(current_user,invitation)
        family_owner =  family.owner
        message ="Hello #{family_owner.first_name}. #{user_to_be_joine_family.first_name} has requested to be a part of #{family.name} as the #{invitation.role}. Let's confirm and add them to our parenting journey :)"
        user_action =  UserCommunication::UserActions.create(:action_for_tool=>action_for_tool,
              :message=> message,
              :actor_id=>current_user.id,
              :action_type => action,
              :unique_id=> unique_id, 
              :object_id=>family_owner.id,
              :family_id => family.id,
              :child_id => family_owner.id
               )
        UserNotification.add_user_action_in_notification(family_owner,user_action)
      rescue Exception=>e 
              Rails.logger.info e.message
      end
  end

  def user_received_family_join_invite(current_user,invitation)
    # notify family owner for join request
      begin
        action,action_for_tool,unique_id,family,user_to_join_family = get_member_data(current_user,invitation)
        family_owner = family.owner
        message ="Hello #{user_to_join_family.first_name}. #{family_owner.first_name} has requested you to be a part of #{family.name} as the #{invitation.role}. Let's confirm and make you a part of the glorious parenting journey :)"
        user_action =  UserCommunication::UserActions.create(:action_for_tool=>action_for_tool,
              :message=> message,
              :actor_id=>current_user.id,
              :action_type => action,
              :unique_id=> unique_id, 
              :object_id=>user_to_join_family.id,
              :family_id => family.id,
              :child_id => user_to_join_family.id
               )
        UserNotification.add_user_action_in_notification(user_to_join_family,user_action)
      rescue Exception=>e 
              Rails.logger.info e.message
      end
  end

  
  def get_member_data(current_user,invitation,action="add")
    user_joined_family = Member.find(invitation.member_id)
    family = Family.find(invitation.family_id)
    elder_member_in_family = user_joined_family.family_elder_members.map(&:to_s) - [user_joined_family.id.to_s]
    members = Member.in(id:elder_member_in_family)
    action_for_tool = "Manage Family"
    action_record = invitation
    action= "add"
    unique_id = UserCommunication::UserActions.set_unique_id(action_record,action_for_tool,action)
    [action,action_for_tool,unique_id,family,user_joined_family]
  end

  def invited_user_joins_family(current_user,invitation)
    begin
      action,action_for_tool,unique_id,family,user_joined_family = get_member_data(current_user,invitation)
      elder_member_in_family = user_joined_family.family_elder_members.map(&:to_s) - [user_joined_family.id.to_s]
      members = Member.in(id:elder_member_in_family)
      message = "Super news! #{user_joined_family.first_name} has joined your family. You can now share milestones, calendars, responsibilites and much more :)"
      #send notification to all other family members
      #unique_id = UserCommunication::UserActions.set_unique_id(action_record,action_for_tool,action)
      members.each do |current_member|
        user_action =  UserCommunication::UserActions.create(:action_for_tool=>action_for_tool,
            :message=> message,
            :actor_id=>current_user.id,
            :action_type => action,
            :unique_id=> unique_id, 
            :object_id=>current_member.id,
            :family_id => family.id,
            :child_id => current_member.id
             )
        UserNotification.add_user_action_in_notification(current_member,user_action)
      end
    rescue Exception=>e 
      Rails.logger.info e.message
    end
    # send notification to user joined family
    begin
      message = "Hello #{user_joined_family.first_name}. Welcome to #{family.name}. I'm the parenting PA on job, and will help you and your family with raising your little ones :)"
      user_action =  UserCommunication::UserActions.create(:action_for_tool=>action_for_tool,
          :message=> message,
          :actor_id=>user_joined_family.id,
          :action_type => action,
          :unique_id=> unique_id, 
          :object_id=>user_joined_family.id,
          :family_id => family.id,
          :child_id => user_joined_family.id
           )
      UserNotification.add_user_action_in_notification(user_joined_family,user_action)
    rescue Exception => e 
      Rails.logger.info e.message
    end
  end

  


end