class Member
  include Mongoid::Document
  include Mongoid::Timestamps
  include ApiDashboardData
  field :first_name,                :type => String
  field :last_name,                 :type => String
  field :email,                     :type => String, :default => ""
  field :user_id,                   :type => String
  field :image,                     :type => String
  field :birth_date,                :type => Date
  field :milestone_alert,           :type => Boolean, :default => true 
  field :display_later_on,          :type => DateTime
  field :time_zone,                 :type => String
  field :created_at,                :type => Time, :default => DateTime.now
  field :updated_at,                :type => Time, :default => DateTime.now
  field :blood_group,               :type=> String
  
  field :quiz_level,                :type=> Integer, :default=> 1 # alexa quiz level value eg. 1,2 3..
  field :activity_status,           :type=> String#, :default=>"Active" # show members activey status
  field :clinic_link_state,         :type=> Integer, :default=>0 # 0=> no clinic setup ,1,2..
  field :organization_uid,          :type=> String # Clinic organization uid
  field :ods_code,                  :type=> String # Clinic ods code
  field :clinic_account_status,     :type=> String, :default=>"Active" # "Active/Blocked" amange by admin
  field :gpsoc_api_request_count,   :type=> Integer,:default=> 0 # Total no of request hit by user in a day.
  field :gpsoc_api_request_updated_on,   :type=> Date # Count updated on
  field :gpsoc_api_requested_ip,    :type=> String # IP used to request gpsoc API
  
  field :orion_api_request_count,        :type=> Integer,:default=> 0 # Total no of request hit by user in a day.
  field :orion_api_request_updated_on,   :type=> Date # Count updated on
  field :last_activity_done_on,          :type => Time # last record access/updated
  field :country_code,                   :type => String
  field :pregnancy_status,               :type => Boolean # to identifiy pregnancy record, used to reduce db query
  field :activity_status_v2,             :type=> String#, :default=>"Active" # show members activey status

  ClinicLinkState = {0=> 'no_clinic_setup', 1=>"system_setup_organization",2=>"user_setup_orgnization",3=>"linked_with_clinic"}
  
  attr_accessor :confirm_email ,:role_name ,:family_id , :email_verification_hash,:status
  ADMIN_ROLE = "administrator"
  belongs_to :user
  has_many :invitations
  has_many :sent_invitations, foreign_key: 'sender_id', class_name: "Invitation"
  has_many :pictures, :as=> :imageable
  has_many :member_settings
  has_many :family_members
  has_many :vaccinations
  has_many :families , :dependent => :destroy#, inverse_of: :family_members
  has_one :profile_image,  :as=>:imageable , :class_name=>"Picture" #, foreign_key: :member_id
  has_many :health_records, :class_name => "Health"
  accepts_nested_attributes_for :family_members, :autosave => true
  accepts_nested_attributes_for :profile_image,  :autosave => true
  has_many :milestones
  has_many :albums
  has_many :documents
  has_many :scores
  has_many :events
  has_many :medical_events
  has_many :article_refs
  has_many :devices
  has_one :basic_setup_evaluator
  validates_associated :profile_image

  GENDER = {"Son" => 1, "Daughter" => 2}
  PRONOUN1 = {"Son"=> "his", "Daughter"=>"her",:Son=> "his", :Daughter=>"her"}
  PRONOUN2 = {"Son"=> "himself", "Daughter"=>"herself",:Son=> "himself", :Daughter=>"herself"}
  PRONOUN3 = {"Son"=> "He", "Daughter"=>"She",:Son=> "He", :Daughter=>"She"}
  PRONOUN4 = {"Son"=> "him", "Daughter"=>"her",:Son=> "him", :Daughter=>"her"}

  PREG = "pregnancy"

  index ({id: 1})
  index ({email: 1})
  index ({user_id:1})
  index ({birth_date:1})
  

  def can_access_clinic_services?
    member = self   
    status = false
    if member.clinic_services_active? 
      if member.ods_code == "orion"
        status = !member.is_orion_api_request_limit_exceed?
      else
        status = !member.is_gpsoc_api_request_limit_exceed?
      end
    end
    status
  end
  def clinic_services_active?
    member = self   
    (member.clinic_account_status == "Active" || member.clinic_account_status.blank?)
  end
  
  def member_nutrients(api_version=1,current_user=nil,options={})
    if api_version < 5
      options = options.merge({:show_feature_type=>"tools"})
    end
    sys_tool_ids = options[:system_tool_ids] || Nutrient.valid_to_launch(current_user,api_version,options).pluck(:id)
    member_tools = MemberNutrient.where(:member_id=>self.id).in(nutrient_id:sys_tool_ids).order_by("position ASC")
    if options[:system_tool_ids].blank?
      member_tools = member_tools.order_by("position ASC")
    end
    member_tools
  end

  def sent_join_family_invitations 
    invitations.where(:sender_type => 'Member', :status => 'invited')
  end

  def received_join_family_invitations
    invitations.where(:sender_type => 'Family', :status => 'invited').order_by("updated_at DESC")
  end 
  
  # return "his/her"
  def pronoun
    Member::PRONOUN1[self.family_members.first.role]
  end

  #return "He/She"
  def pronoun1
    Member::PRONOUN3[self.family_members.first.role]
  end

  # return "himself/herself"
  def pronoun2
    Member::PRONOUN2[self.family_members.first.role]
  end
  # return "him/her"
  def pronoun4
    Member::PRONOUN4[self.family_members.first.role]
  end
  def has_child?
    childern_ids.present?
  end

  def member_gender
     GENDER[family_members.first.role] rescue "Son"
  end

  def profile_pic_for_api
    url = profile_image.complete_image_url rescue ""
   # url.include?("/assets/pictures") ? Rails.application.routes.default_url_options[:host]+ url : url
  end

  def already_having_milestone?(milestone)    
    self.milestones.map(&:system_milestone_id).include?(milestone.system_milestone_id)
  end
  
  def metric_system
    if (user.country_code.downcase == "us" || user.country_code.downcase == "ca" || user.country_code.downcase == "uk"  || user.country_code.downcase == "gb" rescue false)
    length_metric_system = member_settings.where(name:"length_metric_system").first.status || "inches" rescue "inches"
    weight_metric_system = member_settings.where(name:"weight_metric_system").first.status || "lbs" rescue "lbs"
  else
    length_metric_system = member_settings.where(name:"length_metric_system").first.status || "cms" rescue "cms"
    weight_metric_system = member_settings.where(name:"weight_metric_system").first.status || "kgs" rescue "kgs"
  
  end
    {"length_unit"=>length_metric_system,"weight_unit"=>weight_metric_system}
  end

  def self.member_country_code(code)
    code = code.downcase rescue nil
    case code
    when "india"
      code = "in"
    when "uk"
      code = "gb"
    else
      code
    end
  end
  
  def get_clinic_detail(current_user,options={})
    member = self 
    ::Clinic::ClinicDetail.find_by_code_or_organization_id(member.ods_code,member.organization_uid,options)
  end

  def push_user_action_notification(push_identifier,action_record,msg,child,member_ids=[])
    current_user = self
    if child.present?
      family_id = FamilyMember.where(member_id:child.id.to_s).last.family_id
      member_ids = FamilyMember.where(:family_id => family_id).not_in(:role =>["Son","Daughter"]).pluck(:member_id).uniq
    else
      member_ids = []
    end 
    action_record.id = action_record["new_id"] if action_record["new_id"].present?
    UserCommunication::UserActions.add_user_action(push_identifier,action_record,current_user,msg,member_ids)
  end
  
  def validate_all_pointer_links
    SysArticleRefLink.batch_size(100).each do |link|
      link.validate_and_update_links_status
    end
  end

  handle_asynchronously :push_user_action_notification

  def self.sanitize_country_code(countrycode)
    code = []
    if countrycode.present? && countrycode.downcase != "other"
      code = [countrycode, countrycode.downcase,countrycode.upcase]
      code << ["gb","GB"] if countrycode.downcase == "uk"
      code << ["uk","UK"] if countrycode.downcase == "gb"
      code << ["INDIA","india","India"] if countrycode.downcase == "in"
      code << ["IN","in","India"] if countrycode.downcase == "india"
    end
    code.flatten
  end

  def member_families
    self.family_members
  end  

  def active_families
    families
  end

  def update_activity_status_v2(user_id,activity_status,options={})
    begin
      member = self
      member_id = member.id
      return nil if member.class.to_s == "ParentMember" 
      member_type = member.is_expected? ? "pregnancy": "child"
      if member_type == "pregnancy"
        pregnancy = Pregnancy.where(:expected_member_id=>member.id).last
      end
      options[:member_id] = member.id
      options[:update_activity_status] = false
      if member.activity_status_v2 != activity_status
        report_event = Report::Event.where(name:"mark #{member_type} #{activity_status.downcase}").last
        ::Report::ApiLog.log_event(user_id,nil,report_event.custom_id,options) rescue nil
        member.activity_status_v2 = activity_status
        if member_type == "pregnancy"
          pregnancy["activity_status_v2"]= activity_status
          pregnancy.save(:validate=>false)
        end
      end
      if activity_status.downcase == "active"
        family = FamilyMember.where(:member_id=>member.id).last.family
        if family.activity_status_v2 != activity_status 
          report_event = Report::Event.where(name:"mark family #{activity_status.downcase}").last
          options[:report_event] = report_event
          options[:user_id] = user_id
          family.mark_family_activity_status_v2(activity_status,options) rescue nil
        end
        member.last_activity_done_on = Time.now
        if member_type == "pregnancy"
          pregnancy["last_activity_done_on"] = Time.now
          pregnancy.save(:validate=>false)
        end
      end
      member.save(:validate=>false)

    rescue Exception => e
      Rails.logger.info "Error inside member update_activity_status_v2 #{e.message}"
    end
  end
  
  # used in pointer sheet
  def birth
    self.birth_date
  end
  
  def family_elder_members
    begin
      if self.class == "ParentMember"
        member_ids = [self.id]
      else
        member_ids = FamilyMember.where({'family_id' => { "$in" => self.user_family_ids} }).not_in(:role =>["Son","Daughter"]).pluck(:member_id).uniq
      end
    rescue
      []
    end
  end
  
  def device_platform
    self.devices.last.platform.downcase rescue nil
  end
  
  def skip_add_spouse(family_id)
    current_user = self
    skipped_action =  SkipBasicSetupAction.where(member_id:current_user.id,screen_name:"add_spouse_screen",screen_id:family_id).first
    skipped_action  = skipped_action || SkipBasicSetupAction.create(:skipped_forever=> true, :skipped=>true,member_id:current_user.id,screen_name:"add_spouse_screen",screen_id:family_id,:skipped_up_to=> 17.years)
    skipped_action["skipped_forever"] = true
    dimissed_tool =   DismissNutrient.where(parent_id:current_user.id, child_id: family_id ,nutrient_name: "AddSpouse").last
    dimissed_tool = dimissed_tool || DismissNutrient.new(parent_id:current_user.id, child_id: family_id ,nutrient_name: "AddSpouse" ,dismiss_up_to: Date.today+17.years,skipped_forever: true)
    dimissed_tool["skipped_forever"] = true
    
    skipped_action.save
    dimissed_tool.save
  end

  def family_mother_father(include_owner=true)
    begin
      member  = self
      mother = []
      father = []
      owner  = []
      if self.class == "ParentMember"
        mother = [member.id]
      else
        family_id = FamilyMember.where(:member_id=>member.id).first.family_id
        mother_father_id = FamilyMember.where(:family_id=>family_id,:role.in=>[FamilyMember::ROLES[:mother],FamilyMember::ROLES[:father]]).order_by("role desc").pluck(:member_id)
      #   mother = FamilyMember.where({'family_id' => { "$in" => self.user_families.map(&:id)} }).where({'role' => { "$in" => [FamilyMember::ROLES[:mother]]} }).pluck("member_id")
      #   if mother.blank?
      #     father = FamilyMember.where({'family_id' => { "$in" => self.user_families.map(&:id)} }).where({'role' => { "$in" => [FamilyMember::ROLES[:father]]} }).pluck("member_id")
      #   end
      end
      if(mother_father_id.blank?) && include_owner
        owner = [Family.find(family_id).member_id] rescue []
      end
      (mother_father_id + owner)
    rescue
      []
    end
  end


 # depricated
  def childern_ids(family_list=nil)
    family_list = family_list.present? ? family_list : self.user_families
    family_ids  = family_list.map(&:id)
    begin
      FamilyMember.where({'family_id' => { "$in" => family_ids} }).where({'role' => { "$in" => ["Son","Daughter"]} }).pluck("member_id")
    rescue
      []
    end
  end

  def children_ids(family_list=nil)
    family_list = family_list.present? ? family_list : self.user_families
    family_ids  = family_list.map(&:id)
    begin
      FamilyMember.where({'family_id' => { "$in" => family_ids} }).where({'role' => { "$in" => ["Son","Daughter"]} }).pluck("member_id")
    rescue
      []
    end
  end
  
  def unordered_children_ids
    member = self
    family_ids = FamilyMember.where(member_id:member.id).pluck(:family_id)
    FamilyMember.where(:family_id.in => family_ids,:role.in => ["Son","Daughter"]).pluck(:member_id)
  end

  def ordered_chidren_ids(family_list=nil,include_adult=false)
    current_user = self
    family_list = family_list.present? ? family_list : self.user_families
    ids_data = []
    family_list.each do |family|
      data = family.ordered_family_members_api(current_user)[0]
      if include_adult == true
        ids_data << data.map(&:member_id)
      else
        ids_data << data.select{|a| ["Son","Daughter"].include?(a.role)}.map(&:member_id)
      end
    end
    ids_data.flatten.compact
  end
  
   
  def ordered_child_list_excluding_pregnancy
    member = self
    data = []
    member.user_families.each do |family|
      child_ids = FamilyMember.where(family_id:family.id,:role.in=>FamilyMember::CHILD_ROLES).pluck(:member_id).map(&:to_s)
      expected_child_ids = Pregnancy.where(:expected_member_id.in=>child_ids).map(&:expected_member_id).map(&:to_s)
      data << child_ids - expected_child_ids
    end
    data.flatten
  end

  def ordered_pregnancy_list
    member = self
    data = []
    member.user_families.each do |family|
      child_ids = FamilyMember.where(family_id:family.id,:role.in=>FamilyMember::CHILD_ROLES).pluck(:member_id).map(&:to_s)
      expected_child_ids = Pregnancy.where(:expected_member_id.in=>child_ids).map(&:expected_member_id).map(&:to_s)
      data << expected_child_ids
    end
    data.flatten
  end

  # used for alexa
  def ordered_children_list
    current_user = self
    family_members_list = []
    family_ids = FamilyMember.where(member_id:current_user.id).order_by("created_at asc").pluck(:family_id)
    family_ids.each do |family_id|
      family  = Family.find family_id
      family_members_list << family.ordered_family_members_api(current_user)[0]
    end
    family_members_list.flatten.select{|a| a.role == "Son" || a.role == "Daughter"}.map(&:member)
  end

  def famlies_without_child
     user_families_id = self.user_families.map(&:id)
     families_with_childern =  FamilyMember.where({'family_id' => { "$in" => user_families_id } }).where({'role' => { "$in" => ["Son","Daughter"]} }).map(&:family_id)
     dismiss_nutrient_ids =  DismissNutrient.where(nutrient_name: "family" , parent_id: self.id).where({'child_id' => { "$in" => user_families_id } }).gte(dismiss_up_to: Date.today)
     families_with_dismiss = dismiss_nutrient_ids.map(&:child_id) # child_id is reffering family id here. 
     ( (user_families_id.map(&:to_s) - families_with_childern.map(&:to_s)) - families_with_dismiss.map(&:to_s))
  end
  
  # Depricated
  def childerns(ids=nil)
     ids  = ids || childern_ids
     Member.where({'_id' => { "$in" => ids} }) rescue []
  end

  def childrens(ids=nil)
     ids  = ids || childern_ids
     Member.where({'_id' => { "$in" => ids} }) rescue []
  end

  def recent_timeline(member_ids)
    recent_entries =[]
    
    recent_list = Timeline.where({'member_id' => { "$in" => member_ids} }).where(entry_from:"timeline").order_by("created_at DESC").group_by{|p| p.member_id }.entries rescue []
    recent_list.each do |k,v|
      recent_entries << v[0]
    end
      recent_entries
  end

  def recent_milestones(member_ids)
    recent_entries =[]
    
    recent_list = Milestone.where({'member_id' => { "$in" => member_ids} }).order_by("created_at DESC").group_by{|p| p.member_id }.entries rescue []
    # recent_list = Milestone.order_by("created_at DESC").group_by{|p| p.member_id }.entries rescue []
    recent_list.each do |k,v|
      v[0][:entry_from] = "Milestone"
      recent_entries << v[0]
    end
      recent_entries
  end


  def recent_health(member_ids)
    recent_entries =[]
    recent_list = Health.where({'member_id' => { "$in" => member_ids} }).order_by("created_at DESC").group_by{|p| p.member_id }.entries rescue []
    recent_list.each do |k,v|
       v[0][:entry_from] = "Measurement"
      recent_entries << v[0]
    end
      recent_entries
  end

  def recent_picture(member_ids)
    recent_entries =[]

    recent_list = Picture.where({'member_id' => { "$in" => member_ids} }).order_by("created_at DESC").group_by{|p| p.member_id }.entries rescue []
    recent_list.each do |k,v|
      v[0][:entry_from] = "Picture"
      recent_entries << v[0]
    end
      recent_entries
  end

  def recent_immunisation(member_ids)
    recent_entries =[]

    recent_list = Vaccination.where({'member_id' => { "$in" => member_ids} }).where(opted: "true").order_by("created_at DESC").group_by{|p| p.member_id }.entries rescue []
    recent_list.each do |k,v|
      v[0][:entry_from] = "Immunisation"
      recent_entries << v[0]
    end
      recent_entries
  end

  def user_families
    begin
      member =  self
      mother_father_role_family_ids =  FamilyMember.where(member_id:member.id).in(:role=>[FamilyMember::ROLES[:mother],FamilyMember::ROLES[:father]]).order_by("role desc").pluck(:family_id)
      other_role_family_ids = FamilyMember.where(member_id:member.id).not_in(:role=>[FamilyMember::ROLES[:mother],FamilyMember::ROLES[:father]]).pluck(:family_id)
      family_ids =  mother_father_role_family_ids +  other_role_family_ids
      family_list = Family.in(:id=>family_ids).sort_by{|family| family_ids.index(family.id)} rescue []
      # mother_father_role_family = Family.in(:id=>mother_father_role_family_ids).sort_by{|family| mother_father_role_family_ids.index(family.id)} rescue []
      #  other_role_family = Family.in(:id=>other_role_family_ids).entries rescue []
      #  (mother_father_role_family + other_role_family)
       # Family.in(id:mother_father_role_family + other_role_family).entries
    
    rescue
      []
    end
  end

  def user_family_ids
    begin
       ids =  FamilyMember.where(member_id:self.id).pluck(:family_id).map(&:to_s)
    rescue
      []
    end
  end


  def family_name
    name
  end

  def name
    first_name
  end  

  def full_name
    last_name ? (first_name + " " + last_name) : first_name
  end  

  def is_child?
    self.class.name == "ChildMember"
  end  

  def age_in_months
    month = (Date.today.year * 12 + Date.today.month) - (birth_date.year * 12 + birth_date.month)
  end

  def age_in_weeks
    (Date.today - birth_date).to_i/7
  end

  def age_in_years
    Date.today.year - birth_date.year
  end

  def age_duration
    age_received = age_in_months
    age_duration = age_received > 11 ? (age_in_years.to_s + '.years') : age_received.to_s + '.months'    
  end  

  def member_age
    ApplicationController.helpers.calculate_age(self.birth_date) rescue ""
  end

  def find_category(options={})
    child = self
    age_range_value = nil
    if child.birth_date.present?
      if child.age_in_months >= 60
        age_range_value = SystemMilestone::CATEGORIES_RANGE.values.last
      else
        category_range_key = SystemMilestone::CATEGORIES_RANGE.keys.select{|cat_range| cat_range.include?(child.age_in_months)}.last
        category_range_key ||= SystemMilestone::CATEGORIES_RANGE.keys.last 
        if options[:source] == "handhold" && options[:api_version] >= 4
          # Don't include current age range milestones
	        age_range_keys = SystemMilestone::CATEGORIES_RANGE.keys
          age_range_index = age_range_keys.index(category_range_key)
          return nil if age_range_index == 0
          age_range_index = age_range_index - 1
          category_range_key = age_range_keys.values_at(age_range_index).first
          age_range_value = SystemMilestone::CATEGORIES_RANGE[category_range_key]
        else
          age_range_value  = SystemMilestone::CATEGORIES_RANGE[category_range_key]
        end
      end
    else
      age_range_value = SystemMilestone::CATEGORIES_RANGE.values.first
    end  
    age_range_value
  end  

  def milestone_for(system_milestone)
    milestones.select{|m| m.system_milestone_id==system_milestone.id}.first
  end

  def social_image(size = nil)
    if size
      image.split('type').first + "?width=#{size}&height=#{size}"
    else
      image
    end    
  end  
  
  def delete_event_from_parent_google_cal(event_id,type="event")
    Member.where({'id' => { "$in" => family_elder_members} }).each do |family_member|
    gcal = family_member.user.google_cal rescue nil
      if gcal.present? && gcal.refresh_token.present?
        gcal.delete_google_event(event_id,type) rescue nil
      end
    end
  end

  def send_event_to_parent_google_cal(event,type="event",action="create",option={})
    Member.where({'id' => { "$in" => family_elder_members} }).each do |family_member|
      gcal = family_member.user.google_cal rescue nil
      time_zone =  family_member.time_zone || "UTC"
      option["time_zone"] = time_zone
      if gcal.present? && gcal.refresh_token.present?
        gcal.create_google_event(event,type,action,option) rescue nil
      end
    end
  end
  
 #used for alexa
  def find_children(child_name="",member_type="all")
     
      msg = ""
      child_name = child_name || ""
      child = []
      current_user = self
      include_adult = member_type == "adult"
      # byebug
      children_ids = current_user.ordered_chidren_ids(nil,include_adult).map(&:to_s)
      if member_type == "child" || member_type == "adult"
        expected_member_ids = Pregnancy.in(expected_member_id:children_ids).pluck(:expected_member_id).map(&:to_s)
        children_ids = children_ids - expected_member_ids
      elsif member_type == "pregnancy"
        expected_member_ids = Pregnancy.in(expected_member_id:children_ids).pluck(:expected_member_id).map(&:to_s)
        children_ids = expected_member_ids
      end
      childrens = []
      children_ids.each do |member_id|  
        (childrens << Member.find(member_id)) #rescue nil
      end
      if childrens.blank?
        msg =  Message::Alexa[:add_child_msg]
      elsif childrens.length == 1
       child = childrens
      else
        match_string_threshold_value = GlobalSettings::MatchStringThresholdForAlexa
        fuzzy_match = FuzzyStringMatch::JaroWinkler.create( :pure )
        child_name = child_name.downcase.strip
        matched_threshold_limit_data = []
        matched_blow_threshold_limit_data = []
        childrens.each do |child|
          child_first_name = child.first_name.to_s.strip.downcase 
          child[:matched_string_percentage] = fuzzy_match.getDistance(child_first_name,child_name) * 100
          if child[:matched_string_percentage] >= match_string_threshold_value
            matched_threshold_limit_data << child  
          else
            matched_blow_threshold_limit_data << child
          end
        end
        if matched_threshold_limit_data.present?
          matched_children = matched_threshold_limit_data.sort_by{|a| a.matched_string_percentage}.reverse
          child = [matched_children.first]
        else
          child = matched_blow_threshold_limit_data.sort_by{|a| a.matched_string_percentage}.reverse
        end
      end
    [child,msg]
  end



  def delete_family_depenedent_records(family_child_members,family_id,options={})
    current_user = options[:current_user]
    child_members = ChildMember.where(:id.in=>family_child_members)
    options = options.merge({:family_id=>family_id,:update_user_stage=>true})
    child_members.each do |child_member|
      begin
        pregnancy = Pregnancy.where(expected_member_id:child_member.id).last
        if pregnancy.present?
          pregnancy.delete_pregnancy(current_user,child_member,options[:api_version]||6)
        else
          child_member.delete_child_member(child_member.id,options.merge({:log_child_deleted_event=>true}))
        end
      rescue Exception=>e 
        Rails.logger.info "Error inside delete_family_depenedent_records #{e.message}"
      end
    end
    # Subscription.where(family_id:family_id).delete_all
    Score.where(family_id:family_id).delete_all
    Invitation.where(family_id:family_id).delete_all
    member_ids = FamilyMember.where(family_id:family_id).pluck(:member_id)
    ::Clinic::LinkDetail.where(:member_id.in=>member_ids).delete_all
    ::Clinic::User.where(:member_id.in=>member_ids).delete_all
    ::Clinic::UserClinicSession.where(member_id:member_ids).delete_all
  end
  
  handle_asynchronously :delete_event_from_parent_google_cal
  handle_asynchronously :delete_family_depenedent_records
  handle_asynchronously :send_event_to_parent_google_cal
  
  #Used in references update and assign when member update/add
  def update_member_ref(action="add",option={:name_updated=> false,:birth_date_updated=> false})
    member = self
    if action == "add"
      # ArticleRef.assign_article_ref(member.id,{:member_obj=>member})
      ::Child::Scenario.assign_scenario(member.id,option) 

    elsif action == "update"
      if option[:name_updated]
        all_article_refs = ArticleRef.where(member_id:member.id)#.where(:ref_expire_date.lte=>Date.today).includes(:sys_article_ref)
        all_article_refs.each do | ref|
          case ref.category
            when "pregnancy"
              pregnancy = Pregnancy.where(id:ref.record_id).first
            when "prenatal_test"
             prenatal_test = MedicalEvent.where(id:ref.record_id).first
            when "milestone"
             milestone = Milestone.where(id:ref.record_id).first
          end
          system_article = SysArticleRef.where(custome_id:ref.custome_id).first
          begin
            scenario_detail = System::Scenario::ScenarioDetails.where(:custome_id=>system_article.system_scenario_detail_custome_id).last
            scenario_title = scenario_detail.title_in_tech.gsub("sleep","sileep")
            ref.name = eval(scenario_title).gsub("sileep","sleep")
          rescue Exception=> e
            ref.name = (scenario_title).gsub("sileep","sleep") if scenario_title.present?
          end

          ref.save
        end
      end
      if option[:birth_date_updated]
        all_article_refs = ArticleRef.where(member_id:member.id)#.where(:ref_expire_date.lte=>Date.today).includes(:sys_article_ref)
        all_article_refs.delete_all
        ArticleRef.assign_article_ref(member.id,{:member_obj=>member})
      end
    end
    all_article_refs = ArticleRef.where(member_id:self.id)
    all_article_refs.map(&:calculate_effective_score)
  end
  # used for Pointer condition
  def ref_condition1(custom_id)
    begin
      customed_id_ref = self.article_refs.where(custome_id:custom_id).first
      (self.birth_date <= Date.today) && customed_id_ref.blank?
    rescue Exception=> e
      puts e.message
      false
    end
  end

  def get_country_code
    member = self
    if member.class.to_s == "ChildMember"
      if member.country_code.blank?
        country_code = User.where(member_id:member.family_mother_father.first.to_s).first.country_code rescue "other"
        if country_code.blank?
          family_id =  FamilyMember.where(member_id:member.id).last.family_id
          country_code = Family.find(family_id).owner.user.country_code rescue "other"
          member.country_code = country_code 
          member.save(validate:false)
        end
      else
        country_code = member.country_code
      end
    else
      country_code = (member.user.country_code || "other") rescue "other"
    end
    country_code = "UK" if country_code.to_s.downcase == "gb"
    country_code = "IN" if ["india","in"].include?(country_code.to_s.downcase)
    country_code

  end

  def subscribe_mail(mail_ids,from_setting=true)
    subscribe_mail_id = (mail_ids).join(",") rescue ""
    mailer_setting = MemberSetting.where(member_id:self.id, name:"mail_subscribe").first
    if mailer_setting.blank?
      MemberSetting.create(member_id:self.id, name:"mail_subscribe",note:subscribe_mail_id,status: "subscribe")
    else
      # subscribe_mail_id = mailer_setting.note + (mailer_setting.note.present? ? "," : "") + subscribe_mail_id unless from_setting
      mailer_setting.update_attribute(:note,subscribe_mail_id)
    end
    status = 200
  end
  
  def is_clinic_api_request_excced?(options)
    current_user = self
    clinic_detail = current_user.get_clinic_detail(current_user,options)
    if clinic_detail.is_emis?(options)
      status = current_user.is_gpsoc_api_request_limit_exceed?(options)
    elsif clinic_detail.is_orion?(options)
      status = current_user.is_orion_api_request_limit_exceed?(options)
    else
      status = false
    end
    status
  end

  def is_gpsoc_api_request_limit_exceed?(options={})
    current_user = self
    status = false
    if current_user.gpsoc_api_request_updated_on == Date.today
      gpsoc_api_limit_count_per_day  = ::System::Settings.gpsoc_api_limit_count_per_day(current_user,6)
      if gpsoc_api_limit_count_per_day != -1 && current_user.gpsoc_api_request_count >= gpsoc_api_limit_count_per_day
        status = true
      end
    end
    status 
  end 

  def is_orion_api_request_limit_exceed?(options={})
    current_user = self
    status = false
    if current_user.orion_api_request_updated_on == Date.today
      orion_api_limit_count_per_day  = -1 #::System::Settings.gpsoc_api_limit_count_per_day(current_user,6)
      if orion_api_limit_count_per_day != -1 && current_user.orion_api_request_count >= orions_api_limit_count_per_day
        status = true
      end
    end
    status 
  end 

  def subscribe_notification(notification_val)
    notification_status = notification_val.downcase == "on" ? true : false
    self.devices.update_all(enabled: notification_status)
  end

  def notification_subscribed?
    self.devices.any?{|device| device.enabled?}
  end
  
  def update_login_detail(login_time,platform,options={})
    current_user = self 
    user_id = current_user.user_id
    platform = platform || "Device"
    UserLoginDetail.create(user_id:user_id,login_time:login_time,platform:platform ) rescue nil
  end

  def update_mental_math_free_popup_launch_count(platform,options={})
    member = self
    PopupLaunch.update_mental_math_free_popup_launch_count(member.id,platform,nil,options)  
  end

  def update_user_popup_launch_count(platform,options={})
    member = self
    member.update_mental_math_free_popup_launch_count(platform, params) rescue nil
    PopupLaunch.update_app_referral_popup_launch_count(member.id) rescue nil
    PopupLaunch.update_nook_popup_launch_count(member.id) rescue nil
  end

  def store_device_info(member, token, platform,options={})
    member.update_family_and_user_segment(member,options)
    options = options || {}
    status = (member.member_settings.where(name:"subscribe_notification").last || MemberSetting.create(name:"subscribe_notification",member_id:member.id.to_s,status:true)).status rescue false
    if token.present?
      device_detail = member.devices.last || {}
      member.devices.delete_all
      Device.where(token:token).delete_all
    end

    device = member.devices.last || Device.new(member_id:member.id)
    ["email","auth_token","password","action","controller","format","device_token","fb_data"].each do |i|
       options.delete(i)
    end
    options = options.reject{|k,v| v.blank?}
    if options.present?
      options.each do |k,v|
        device[k] = v || device_detail[k]
      end
    end
    begin
      device.screen_skip_count = device_detail["screen_skip_count"]
      device.screen_skip_up_to = (device_detail["screen_skip_up_to"] + Device::Screen_skip_day.days) rescue Date.today - 1.days
    rescue 
    end
    device.token = token if token.present?
    device.enabled = status
    device.app_version = device.app_version || device_detail["app_version"]
    device.os_version = device.os_version || device_detail["os_version"]
    device.build_number = device.build_number || device_detail["build_number"]
    
    device.platform = platform if platform.present?
    device.save
    user = member.user
    user["app_login_status"] = true
    user.save
  end
  handle_asynchronously :store_device_info
  
  def update_family_and_user_segment(member=nil,options={})
    begin
      member = member || self
      user = member.user
      user.delay.update_user_segment(nil,options)
      user_family_ids = FamilyMember.where(member_id:member.id).pluck(:family_id).uniq
      Family.in(:id=>user_family_ids).each do |family|
        family.delay.update_family_segment(nil,options)
      end
    rescue Exception => e 
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside update_family_and_user_segment")
    end
  end
  
  def update_family_and_user_trial_days(member=nil,options={})
    begin
      member = member || self
      user = member.user
      user_family_ids = FamilyMember.where(member_id:member.id).pluck(:family_id).uniq
      Family.in(:id=>user_family_ids).each do |family|
        family.delay.update_family_trial_days(trial_days_value=nil,options)
      end
    rescue Exception => e 
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside update_family_and_user_segment")
    end
  end

  def update_family_and_user_pricing_exemption(member=nil,options={})
    begin
      member = member || self
      user = member.user
      report_event = Report::Event.where(name:"mark user pricing_exemption #{user.pricing_exemption}").last
      ::Report::ApiLog.log_event(user.id,family_id=nil,report_event.custom_id,options) rescue nil
  
      user_family_ids = FamilyMember.where(member_id:member.id).pluck(:family_id).uniq
      Family.in(:id=>user_family_ids).each do |family|
        family.delay.update_family_pricing_exemption(options)
      end
    rescue Exception => e 
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside update_family_and_user_price_exemption")
    end
  end

  def get_email_deeplink_data(user,child,destination_type,destination_name,role,feed_id=nil, user_notification)
    begin
      if destination_type.present?
         user = user
         notification_type= user_notification.type rescue nil
         notification_id = user_notification.id rescue nil
         action_name = user_notification.action_name
         family_member = child.family_members.first rescue nil
         child_id = child.id.to_s rescue nil
         family_id = family_member.family_id rescue nil
         family_id = family_id || user_notification.family_id rescue nil
         get_tool_id = Nutrient.get_tool_id(destination_name)
         tool = Nutrient.find(get_tool_id) rescue Nutrient.new
         # "token=#{user.api_token}&source=email_deeplink&notification_id=#{notification_id}&notification_type=#{notification_type}&tool_id=#{get_tool_id}&tool_feature_type=#{tool.feature_type}&email=#{user.email}&member_id=#{child_id}&destination_name=#{destination_name}&authentication_token=#{user.api_token}&user_id=#{user.id}&role=#{role}&feed_id=#{feed_id}&family_id=#{family_id}&action_name=#{action_name}&destination_type=#{destination_type.to_s.downcase}"
         deeplink_data = {:token=>user.deeplink_api_token,:source=>"email_deeplink",:notification_id=>notification_id,:notification_type=>notification_type,:tool_id=>get_tool_id,:tool_feature_type=>tool.feature_type,:email=>user.email,:member_id=>child_id,:destination_name=>destination_name,:authentication_token=>user.deeplink_api_token,:user_id=>user.id,:role=>role,:feed_id=>feed_id,:family_id=>family_id,:action_name=>action_name,:destination_type=>destination_type.to_s.downcase}
         deeplink_data.reject{ |k,v| v.blank?}.to_query
         # {email:user.email,member_id: child.id.to_s, destination_name:destination_name, authentication_token:user.api_token,user_id:user.id,role:role,feed_id:feed_id,family_id:family_id,action_name:nil,destination_type:destination_type.to_s.downcase}
      else
        {}
      end
    rescue 
      nil
    end
  end

  # Used in Article ref(Pointers) 
  def is_boy?
    begin
      member_role = self.family_members.first.role
      member_role == "Son" || member_role == :Son || member_role == "son" || member_role == :son
    rescue
      false
    end
  end
  def is_girl?
    begin
      member_role = self.family_members.first.role
      member_role == "Daughter" || member_role == :Daughter || member_role == "daughter" || member_role == :daughter
    rescue
      false
    end
  end

def alexa_help_screen_data(platform,signup_source_refrence=nil)
  begin
    current_user = self
    user_signup_source_reference = (signup_source_refrence || current_user.user.signup_source_reference).downcase
    @child_member = current_user.childerns.last
    child_name = 'my child' #@child_member.first_name
    user_signup_source_reference = user_signup_source_reference.downcase rescue nil 
    data = []
    topic = "addition/subtraction/multiplication/division"
    level = "3"
    if  user_signup_source_reference == "nurturey maths" || user_signup_source_reference == "nurtureymaths" || user_signup_source_reference == "nurturey math"
      if platform == "alexa"
        data << {"title"=>"Ask to open Nurturey maths","description"=>["Alexa, open Nurturey maths"]}
      elsif platform == "googleassistance"
        data << {"title"=>"Ask to open Nurturey maths","description"=>["Hey google, talk to Nurturey maths"]}
      end
      #data << {"title"=>"Ask to start quiz" , "description"=> ["How good are my maths skills","How good am I in #{topic}","Let's practise some maths"]}
      data << {"title"=>"Ask to repeat question" , "description"=>["Repeat"] }
    elsif user_signup_source_reference == "nurturey"
      if platform == "alexa"
        data << {"title"=>"Ask to open Nurturey","description"=>["Alexa, open Nurturey"]}
      end
      data << {"title"=>"Ask Nurturey to update child’s measurements","description"=>["Alexa, update #{child_name}'s height to <H> and weight to <W>","Alexa, update #{child_name}'s measurements"]}
      data << {"title"=>"Ask Nurturey to get child’s measurements", "description"=>["Alexa, what's #{child_name}'s measurements","Alexa, what's #{child_name}'s height","Alexa, what's #{child_name}'s weight"]}
    else
      if platform == "alexa"
        data << {"title"=>"Ask to open Nurturey maths","description"=>["Alexa, open Nurturey maths"]}
      elsif platform == "googleassistance"
        data << {"title"=>"Ask to open Nurturey maths","description"=>["Hey google, talk to Nurturey maths"]}
      end
    end
  rescue Exception=> e
    data = [] 
  end
  data
end

  def self.add_system_entries(child_member,current_user,api_version)
    # Add system checkup planner checkup to member account 
    if api_version < 5
      System::Child::CheckupPlanner::Checkup.add_system_checkup_to_member(current_user,child_member,api_version,{:save_checkup_planner=> true}) rescue nil
    end
  end

  # This method will give list of all_article_refs for all born and expected children
  def all_article_refs
    all_member_ids = FamilyMember.where({'family_id' => { "$in" => self.families.map(&:id)} }).map(&:member_id)
    all_article_refs = ArticleRef.in(:member_id => all_member_ids, :ref_expire_date.gte => Date.today)
  end

  def current_profile_image_url(size='small',api_version=1)
    (profile_image && profile_image.complete_image_url(size,api_version)) || image
  end
  
  def children_and_pregnancy_id(children_ids=nil)
    begin
      current_user = self
      childrens = children_ids || current_user.ordered_chidren_ids.map(&:to_s)
      pregnacy_ids = Pregnancy.where(:expected_member_id.in=>childrens).pluck(:expected_member_id).map(&:to_s)
      {:children_ids=>(childrens - pregnacy_ids),:pregnacy_ids=>pregnacy_ids}
    rescue Exception=>e 
      Rails.logger.info "Error inside children_and_pregnancy_id"
      {}
    end
  end

  def profile_or_generic_image_url(size='small',api_version=1)

    member_image = current_profile_image_url(size,api_version)
    if member_image.blank?
      member = self
      if member.is_expected?
        member_image = APP_CONFIG['default_host'] + "/assets/pregnancy_m.jpg"
      else
        if member.is_boy?
          member_image = APP_CONFIG['default_host'] + "/assets/boy_m.jpg"
        else 
          member_image = APP_CONFIG['default_host'] + "/assets/girl_m.jpg"
        end
      end
    end
    member_image
  end
  
  # used in batch push notification
  def self.has_valid_tool_activation_status?(batch_notification_manager,child_id,current_user,destination_name)
    return true if batch_notification_manager.blank?

    tool_status_to_check = batch_notification_manager.tool_status.downcase rescue "na"
    status = false
    return true if (tool_status_to_check != "active" && tool_status_to_check != "deactivated")

    tool_name = (batch_notification_manager.destination.blank? || batch_notification_manager.destination == "NA") ? destination_name : batch_notification_manager.destination

    if tool_status_to_check == "active"
      status = Nutrient.is_tool_activated?(child_id, tool_name)
    else
      status = !Nutrient.is_tool_activated?(child_id, tool_name)
    end
    status
  end

  # used in batch push notification
  def self.has_valid_subscription_status?(batch_notification_manager,child_id,current_user,options={})
    return true if batch_notification_manager.blank?
    subscription_status_to_check = batch_notification_manager.subscription_status
    subscription_status_to_check = [subscription_status_to_check].flatten.map(&:downcase)
    status = false
    subscription_status_to_check_free_and_paid = subscription_status_to_check.include?("free") && subscription_status_to_check.include?("paid")
    return true if (subscription_status_to_check.blank? || subscription_status_to_check_free_and_paid)
    child_family_subscribed_plan = current_user.child_family_subscription(child_id)

    if subscription_status_to_check.include?("free")
      status  = Subscription.is_free?(child_family_subscribed_plan.subscription_listing_order.to_i)
    else 
      status  = !Subscription.is_free?(child_family_subscribed_plan.subscription_listing_order.to_i)
    end
    status
  end

  # used in batch push notification
  def child_family_subscription(child_id)
    begin
      @family_id = FamilyMember.where(member_id:child_id.to_s).last.family_id
      family_subscribed_plan = Subscription.family_free_or_premium_subscription(@family_id)
    rescue Exception => e 
      Rails.logger.info "error in child_family_subscription"
      Rails.logger.info e.message
      subscription_manager = SubscriptionManager.free_subscription
      family_subscribed_plan = Subscription.new(subscription_manager_id:subscription_manager.id,family_id:@family_id)
    end
  end

  def has_valid_tool_and_subscription(batch_notification_manager,child_id,destination_name,options={})
    return true if options[:test_without_validation]
    return true if (batch_notification_manager.blank? && options[:user_notification_obj].blank?)
    current_user = self
    user_notification = options[:user_notification_obj]
    child_id = child_id || user_notification.member_id rescue nil
    batch_notification_manager = batch_notification_manager || BatchNotificationManager.where(identifier:user_notification.type).last rescue nil

    destination_name = destination_name || user_notification.tool_name
    
    return false if !Member.has_valid_tool_activation_status?(batch_notification_manager,child_id,current_user,destination_name)
    return false if !Member.has_valid_subscription_status?(batch_notification_manager,child_id,current_user)
    return true        
  end
  
  def has_valid_added_child_member_tenure?(batch_notification_manager,options)
    current_user = self
    return true if options[:test_without_validation] == true
    if batch_notification_manager.added_child_member_tenure.present?  && batch_notification_manager.added_child_member_tenure != "NA"
      status = Member.where(:id.in=>current_user.childern_ids,:created_at.lte=> (Date.today - eval(batch_notification_manager.added_child_member_tenure) ) ).count > 0 rescue false
    else
      status  = true
    end
    status
  end

  def has_valid_activity_status?(batch_notification_manager,options)
    current_user = self
    return true if options[:test_without_validation] == true
    if batch_notification_manager.user_activity_status.present?  && batch_notification_manager.user_activity_status != "NA"
      status = [batch_notification_manager.user_activity_status].flatten.include?(current_user.activity_status||current_user.user.activity_status)
    else
      status  = true
    end
    status
  end
  
  def get_tool_sample_data(tool,current_user,api_version,options={})
    sample_data = System::SampleDataForTool.where(:tool_identifier=>/#{tool.identifier}/i)
    options[:get_data_for_from_list] = options[:get_data_for_from_list].present? ? options[:get_data_for_from_list] : "all"
    sample_data = sample_data.where(:data_for_tab=>/#{options[:get_data_for_from_list]}/i)
    data = sample_data.last.data rescue []
    data
  end

  def self.updated_at_text(updated_at_value,level=1,options={})
    return "Update now" if updated_at_value.blank?
    updated_at_value = ApplicationController.helpers.calculate_age4(Date.today,updated_at_value,level) rescue nil
    if updated_at_value.blank?
      updated_text = updated_text_value
    elsif updated_at_value == "today"
      updated_text = "Updated #{updated_at_value}" rescue updated_text_value
    else  
      updated_text = "Updated #{updated_at_value} ago" rescue updated_text_value
    end
    updated_text
  end
  
  def measurement_milestone_graph_data(current_user,api_version=1,options={})
    member = self
    milestone_data = {}
    measurement_data = {}
    tooth_chart_data = {}
    extra_info = {}
    updated_text_value = "Update now"
    #tool, show_milestone_subscription_box_status,info_data,member_data = member.get_subscription_show_box_data(current_user,"Milestones",api_version,options)
    if true #show_milestone_subscription_box_status == false
      milestone_data = Milestone.member_milestone_graph_data(member,current_user)
      age_range = member.find_category
      updated_at = Milestone.where(member_id:member.id).order_by("updated_at desc").first.updated_at rescue nil
      updated_at_text = Member.updated_at_text(updated_at,level=1,options)
      # updated_at_value = ApplicationController.helpers.calculate_age4(Date.today,updated_at,level=1) rescue nil
      
      # if updated_at_value.blank?
      #   updated_at_text = updated_text_value
      # elsif updated_at_value == "today"
      #   updated_at_text = "Updated #{updated_at_value}" rescue updated_text_value
        
      # else  
      #   updated_at_text = "Updated #{updated_at_value} ago" rescue updated_text_value
      # end
        percantage_achieved = milestone_data[:milestones][:achieved_percentage]  rescue 0
        percantage_achieved = percantage_achieved.to_i  rescue percantage_achieved
        # milestone_title = "#{member.first_name} has achieved #{percantage_achieved}% milestones in #{member.pronoun} current age group #{age_range}".humanize
        milestone_title = "#{member.first_name} has achieved #{percantage_achieved}% of the milestones expected by #{member.pronoun} current age".humanize
        current_age_range = milestone_data[:milestones][:current_age_range]

        extra_info["milestones"] =  {:current_age_range=>current_age_range,:updated_at_text=>updated_at_text , :title=>milestone_title}
    end
    #tool, show_measurement_subscription_box_status,info_data,member_data = member.get_subscription_show_box_data(current_user,"Measurements",api_version,options)
    if true #show_measurement_subscription_box_status == false
      title_value = "- -" #"No records"
      round_off_val = 1
      measurement_data = Health.member_measurement_graph_data(member,current_user,round_off_val,options) rescue {}
      if options[:graph_vesion].blank?
        extra_info["height"] = {title:(measurement_data["height"].delete("title")||title_value),:updated_at_text=>(measurement_data["height"].delete("updated_at_text")||updated_text_value )}
        extra_info["weight"] = {title:(measurement_data["weight"].delete("title")||title_value),:updated_at_text=>(measurement_data["weight"].delete("updated_at_text")||updated_text_value)}
      end
      extra_info["bmi"] =    {title:(measurement_data["bmi"].delete("title") || title_value),:updated_at_text=>(measurement_data["bmi"].delete("updated_at_text")||updated_text_value)}
    end

    #tool, show_tooth_chart_subscription_box_status,info_data,member_data = member.get_subscription_show_box_data(current_user,"Tooth Chart",api_version,options)
    if true #show_tooth_chart_subscription_box_status == false
      erupted_tooth_count = Child::ToothChart::ToothDetails.where(child_id:member.id).erupt_data.count
      updated_at =  Child::ToothChart::ToothDetails.where(child_id:member.id).not_in_unaccomplished_status.order_by("updated_at desc").first.updated_at.convert_to_ago2.strip rescue nil
      updated_at_text = updated_at.present? ? "Updated: #{updated_at}" : updated_text_value
      tooth_chart_title = "#{(member.first_name.downcase.titleize rescue '')} has grown #{erupted_tooth_count} teeth"
      extra_info["tooth_chart"] = {title:tooth_chart_title,:updated_at_text=>updated_at_text}
      tooth_chart_data["tooth_chart"] = Child::ToothChart::ToothDetails.child_data_hash({:member_id=>member.id},"all")
    end

    if options[:graph_vesion].to_i > 1
      immunisation_chart_data, extra_info["immunisations"] = Vaccination.immunisation_graph_data(current_user,member,options)
      # immunisation_chart_data = Vaccination.vaccination_chart_data(current_user,member,options)
      # updated_at = Vaccination.last_jab_date(member.id, format="to_date")
      # immunsation_updated_at_text = Member.updated_at_text(updated_at,level=1,options)
      # # current_age_range = member.find_category
      # # age_range = current_age_range.split("-")[1]
      # age_range = immunisation_chart_data.delete(:current_age_range)
      # extra_info["immunisations"] =  {:current_age_range=>age_range, :x_axis_title=>"Age (months)",:y_axis_title=>"Doses",:updated_at_text=>immunsation_updated_at_text }
    end
    if options[:graph_vesion].to_i > 1
      eye_chart = Child::HealthCard::EyeChart.get_list(member.id).first
      if eye_chart.present?
        vision_health_data = {"vision_health"=>eye_chart}
        vision_health_updated_at = eye_chart[:date].to_date
        vision_health_updated_at_text = Member.updated_at_text(vision_health_updated_at,level=1,options)
        extra_info["vision_health"] =  {:updated_at_text=>vision_health_updated_at_text }
      else
        vision_health_data = {"vision_health"=>nil}
        vision_health_updated_at_text = Member.updated_at_text(nil,level=1,options)
        extra_info["vision_health"] =  {:updated_at_text=>vision_health_updated_at_text }
      end

    end
    tool_identifiers = {"vision_health"=> "Vision Health","immunisations"=>"Immunisations", "milestones"=>"Milestones","tooth_chart"=>"Tooth Chart","height"=>"Measurements","weight"=>"Measurements","bmi"=>"Measurements"}.with_indifferent_access
    data = []
    extra_info = extra_info.with_indifferent_access
    if options[:graph_vesion].to_i > 1
      graph_data = immunisation_chart_data.merge(measurement_data).merge(milestone_data).merge(tooth_chart_data).merge(vision_health_data)
    else
      graph_data =  milestone_data.merge(measurement_data).merge(tooth_chart_data)
    end
    graph_data.each do |key,value|
      data << {:chart_type=>key,:tool_identifier=>tool_identifiers[key], :chart_data=>value}.merge( ((extra_info[key]|| {}) rescue {}) )
    end
    {:member_id=>member.id,:charts=>data}
  end




  def get_subscription_show_box_data(current_user,tool_identifier,api_version=4,options={})
    member = self
    family = member.get_family
    member_data = nil
    tool = Nutrient.get_tool(tool_identifier)
    info_data = {}
    subscription_box_data = GlobalSettings::DefaultSubscriptionBoxData 
    if api_version > 4 
      show_subscription_box_status,free_trial_end_date_status,free_trial_end_msg = tool.show_subscription_box_status(current_user,family.id,api_version) 
    else
      show_subscription_box_status = false
      free_trial_end_date_status = true
    end
    
    if tool_identifier == "Immunisations"
      show_subscription_box_status = false
      free_trial_end_date_status = true
      subscription_box_data = {}
    end
    if tool_identifier == "Mental Maths"
      show_subscription_box_status = false
      free_trial_end_date_status = true
      subscription_box_data = {}
    end
    if tool_identifier == "Vision Health"
      show_subscription_box_status = false
      free_trial_end_date_status = true
      subscription_box_data = {}
    end

    if show_subscription_box_status == true
      member_data = member.get_tool_sample_data(tool,current_user,api_version,options)
      subscription_box_data = (tool.subscription_box_data || subscription_box_data) rescue {}
    end
    info_data[:free_trial_end_date_message] = nil
    info_data[:subscription_box_data] = subscription_box_data 
    info_data[:free_trial_end_date_message] = free_trial_end_msg if (!free_trial_end_date_status && tool.tool_category == "basic")
    [tool,show_subscription_box_status,info_data,member_data]
  end

  def log_api(params,options={})
    member = self
    Report::ApiLog.create_record(member.email,params,options)
  end

  def log_event(family_id,custom_event_id,options={})
    current_user = self
    family_id = family_id || options[:family_id]
    Report::ApiLog.log_event(current_user.user_id,family_id,custom_event_id,options)
  end

  def log_emis_request(member_id,request_data,options={})
    current_user = self
    if current_user.gpsoc_api_request_updated_on != Date.today
      current_user.gpsoc_api_request_count = 0
      current_user.gpsoc_api_request_updated_on = Date.today
    else
      current_user.gpsoc_api_request_count = current_user.gpsoc_api_request_count.to_i + 1 
    end
    current_user.save(validate:false)
    current_user.reload
    ::Log::EmisLog.create_record(current_user,member_id,request_data,options)
  end

  def log_tpp_request(member_id,request_data,options={})
    current_user = self
    ::Log::TppLog.create_record(current_user,member_id,request_data,options)
  end
  
  def log_orion_request(member_id,request_data,options={})
    current_user = self
    if current_user.orion_api_request_updated_on != Date.today
      current_user.orion_api_request_count = 0
      current_user.orion_api_request_updated_on = Date.today
    else
      current_user.orion_api_request_count = current_user.orion_api_request_count.to_i + 1 
    end
    current_user.save(validate:false)
    current_user.reload
    ::Log::OrionLog.create_record(current_user,member_id,request_data,options)
  end

  def accessible_admin_panel_module_list_for_user
    current_user = self
    if current_user.email == APP_CONFIG["superadmin_email"]
      UserRoleForAdminPanel::ModuleNameMapping
    
    else
      user_role = UserRoleForAdminPanel.where(email:current_user.email).map(&:role)
      ModuleAccessForRole.where(:role.in=> user_role).pluck(:module_name).uniq
    end
  end

def get_user_affliate_product_list(childern_ids,options={})
    current_user = self
    pointer_links = ArticleRef.get_pointers_link_list(current_user,childern_ids,nil,{:only_affiliated_pointer=>true})
    data = []
    product_count = options[:product_count] || 10
    pointer_links.each do |link|
      link = link.with_indifferent_access
      if (link[:article_provide].split(" | ").length == 1 rescue false)
        data << {
        :product_link_url => link[:url],
        :product_image_url => link[:linkimageurl],
        :product_name =>link[:linktitle],
        :product_price => nil,
        :product_provider=>(link[:article_provide]),
        }
      end
      data = data.uniq
      break if data.count == product_count 
    end
    data
  end
  def fetch_and_save_nhs_media(start_date,end_date,page,options={})
    content_manager_obj = Nhs::NhsContentManager.new
    content_manager_obj.fetch_and_save_media(start_date,end_date,page)
  end

  def fetch_color_vision_related_info(date_to)
    # byebug
      age = ApplicationController.helpers.calculate_age2(self.birth_date,date_to) rescue ""
      date = date_to.to_date.to_date4
      time = date_to.strftime("%I:%M %p")
      formatted_date = date+ " • " + time
      return {:age=>age, :date=>date,:time=>time, :formatted_date=>formatted_date}

  end
  
  def get_clinic_link_state(current_user,options={})
    member = self 
    clinic_detail = member.get_clinic_detail(current_user,options)
    return 0 if clinic_detail.blank?
    return member.clinic_link_state.to_i if member.clinic_link_state.to_i < 3
    member_clinic_link_detail = ::Clinic::LinkDetail.get_clinic_link_detail(member.id,current_user,options)
    if member_clinic_link_detail.blank?
      return 2
    else
      return 3
    end
  end

  def is_user_fully_linked_with_clinic?(current_user,options={})
    return false if current_user.blank?
    member = self
    # member.get_clinic_link_state(current_user,options) == 3
    is_full_access, access_level, msg , action_identifier,access_reason = ::Clinic::User.get_access_level(current_user,member.id,options)
    is_full_access
  end

  def reauthentication_required?(current_user,member_id,options={})
    return true
    status = false
    if member_id.present?
      member = Member.find(member_id)
      if(member.get_clinic_link_state(current_user,options) > 2)
        clinic = member.get_clinic_detail(current_user,options)
        if clinic.is_gpsoc?(options)
          status = true
        end
      end
    else
      family_ids = current_user.user_family_ids
      family_member_ids = FamilyMember.where(:family_id.in=>family_ids).pluck(:member_id)
      family_member_ids = ::Clinic::LinkDetail.where(:member_id.in=>family_member_ids,:parent_member_id=>current_user.id).map(&:member_id)
      clinic_link_state_list = Member.where(:id.in=>family_member_ids).pluck(:clinic_link_state)
      ods_code_list = Member.where(:id.in=>family_member_ids,:clinic_link_state.gte=>2).pluck(:ods_code)
      clinic_list = Clinic::ClinicDetail.where(:ods_code.in=>ods_code_list)
      clinic_list.each do |clinic|
        if clinic.is_gpsoc?(options)
          status = true
          break
        end
      end
    end
    status
  end

  def clinical_safty_message_enabled_for_app_version?(current_user,options={})
    member = self 
    return false if (member.is_expected? rescue false )
    devices = current_user.devices.last
    status  = devices.platform == "ios" && devices.app_version_conversion_from_str_to_int("2.4.0") <= Device.new.app_version_conversion_from_str_to_int(devices.app_version)
    status
  end
  
  def sync_data_with_clinic(current_user,api_version,organization_uid,tool,options={})
    begin
      member = self
      member_id = member.id
      user_country = Member.member_country_code(current_user.user.country_code)
      return nil if user_country != "gb"
      if !member.is_user_fully_linked_with_clinic?(current_user,options) && member.clinical_safty_message_enabled_for_app_version?(current_user,options)
        return  ::Message::Clinic[:tool_data_sync]["not_linked"] %{:member_name=>member.first_name}
      end
      if member.valid_for_sync_clinic_data?(current_user,api_version,tool,options)
        clinic = tool.get_clinic_class_for_member_tool(current_user,api_version,organization_uid,member,options)
        clinic_detail = member.get_clinic_detail(current_user,options)
        data_source = clinic_detail.get_clinic_type(options) rescue nil
        if tool.identifier == "Measurements"
          Timeline.where(data_source:data_source,entry_from: "health").delete_all
          status = clinic.get_patient_measurements(organization_uid,member.id,options)
        elsif tool.identifier == "Documents"
          Timeline.where(data_source:data_source,entry_from: "document").delete_all
          response = clinic.list_document(member.id,options)
          status = response[:status]
        end
      else
        options[:data_source] = data_source
        tool.clean_gpsoc_records(member_id,options)
        status = nil
      end
    rescue Exception =>e 
      status = 100
      Rails.logger.info "...........Error inside sync_data_with_clinic ...."
      Rails.logger.info e.message
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside sync_data_with_clinic")
    end
    ::Message::Clinic[:tool_data_sync][status.to_s]
  end

  def valid_for_sync_clinic_data?(current_user,api_version,tool,options={})
    member  = self
    is_full_access,access_level,msg,action_identifier,access_reason = ::Clinic::User.get_access_level(current_user,member.id,options) 
    (member.organization_uid.present? || member.ods_code.present? ) && member.get_clinic_link_state(current_user,options) == 3 && is_full_access
  end

  def parent_fully_linked_with_gpsoc?
    member =  self
    parent_ids = member.family_mother_father(include_owner=false)  
    Member.where(:id.in=>parent_ids,:clinic_link_state.gt=>2).present?
  end  
  
  def check_access_level(member_id,api_version,current_user,options={})
    
  end

  def get_screen_name_for_cvtme(event_on_vision)
    screen_name = nil
    member_age = self.age_in_months 
    if event_on_vision == nil 
      if (0..35).cover? member_age 
        screen_name = "younger_than_recommended_age_screen"
      elsif (36..59).cover? member_age
        screen_name = nil
      else
          screen_name = "older_than_recommended_age_screen"
      end
    end
    return screen_name
  end

end
