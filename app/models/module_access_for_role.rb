# It store info about which user has what role to access a module like pointer, report, tools etc

class ModuleAccessForRole
  include Mongoid::Document  
  include Mongoid::Timestamps
  
  field :module_name, type: String
  field :role, type: String
   

  def self.user_access_level_for_module(current_user,module_name)
    begin
      access_level = 0
      user_roles = UserRoleForAdminPanel.where(email:current_user.email).pluck(:role)
      role_list = ModuleAccessForRole.where(module_name: module_name, :role.in=> user_roles).map(&:role)
      if role_list.present?
        max_access_level = RoleForAdminPanel.in(name:role_list).entries.map(&:access_level).map(&:to_i).max
        access_level = max_access_level
      end
    rescue Exception=> e
      access_level = 0
    end 
    access_level = 3 if current_user.email == AppConfig["superadmin_email"] 
    access_level
  end  
end

