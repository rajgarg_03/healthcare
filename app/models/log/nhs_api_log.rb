class Log::NhsApiLog
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :fetch_url        ,  type:String
  field :user_id          ,  type:String # requester(logged in user's member id)
  field :member_id        ,  type:String # requested
  field :ip_address       ,  type:String
  field :location         ,  type:String
  field :request_status   ,  type:String
  field :platform         ,  type:String  #  eg ios/android
  field :family_id        ,  type:String  #  eg 1,2,3
  field :fetch_by         ,  type:String  #  system/user
  DATATABLE_COLUMNS = [:fetch_url,:user_id, :member_id,:request_status]

  def self.get_data(options)
    start_date = Date.today - 2.day
    end_date = Date.today
    data = Log::NhsApiLog.where(:created_at=>{"$gte"=>start_date,"$lte"=>end_date}).order_by("id desc")
    if options[:user_id].present?
      data = data.where(user_id:options[:user_id])
    end
    data
  end


  def self.get_requests(options={})
    if options.present?
      start_date = options[:start_date].blank? ? (Date.today - 30.days) : options[:start_date]
      end_date = options[:end_date].present? ? (options[:end_date].to_date + 1.day) : Date.today + 1
      data = Log::NhsApiLog.where(:created_at=>{"$gte"=>start_date,"$lte"=>end_date})
      if options[:email].present?
        user = User.where(email:options[:email]).last 
        data = data.where(user_id:user.member_id) if user.present?
      end
      data = data.order_by("id desc")
    else
      data = Log::NhsApiLog.get_data({})
    end
    data
  end


  def self.datatable_filter(search_value, search_columns)
    return all if search_value.blank?
    result = self
    filter = []
    search_columns.each do |key, value|
      
      if value['data'].present? 
        if value['data'] == "created_at"
           filter << {:created_at.gte=>(search_value).to_time} if (search_value.to_date rescue false)
        # elsif value['data'] == "request_status"
        #   if value['searchable'].present?
        #     if "failed".include?(search_value.downcase) 
        #       val = false
        #     elsif "success".include?(search_value.downcase) 
        #       val = true
        #     else
        #       val = nil
        #     end
        #     filter << {"request_status"=>val} if !val.nil?
        #   end
        else
          filter << {"#{value['data']}"=>/#{Regexp.escape(search_value)}/i} if value['searchable']
        end
      end
    end
    result = result.where("$or"=>filter) 
    result
  end  

  def self.datatable_order(order_column_index, order_dir)
    order_by("#{::Log::NhsApiLog::DATATABLE_COLUMNS[order_column_index]} #{order_dir}")
  end

  def self.create_record(current_user,member_id,request_data,options={})
    begin
      action = request_data[:action]
      if current_user.nil?
        fetch_by = "system"
        user_id = nil
        member = nil
        platform = nil
        ip_address = nil
        ip_location = nil # ::Log::EmisLog.get_location_by_ipvigilante(ip_address)
        family_id =  nil
      else
        fetch_by = "user"
        user_id = current_user.id rescue nil
        member = Member.find(member_id || current_user.id) rescue nil
        platform = (options[:platform] || current_user.devices.first.platform) rescue nil
        ip_address = (options[:ip_address] || current_user.gpsoc_api_requested_ip )rescue nil
        ip_location = nil # ::Log::EmisLog.get_location_by_ipvigilante(ip_address)
        family_id =  FamilyMember.where(member_id:member_id).last.family_id rescue nil
      end
      
      request_status = request_data[:request_status]
      log_record = ::Log::NhsApiLog.create(:fetch_by=>fetch_by, :request_status=>request_status,:family_id=>family_id,:platform=>platform,:user_id=>user_id,:ip_address=>ip_address, :location=> ip_location,:member_id=>member_id, :fetch_url=>action)
      # log_record.delay.update_ip_location
    rescue Exception=> e
      puts e.message
      Rails.logger.info "Error inside Emis create_record \n"
      Rails.logger.info e.message
      
    end
  end

  def update_ip_location
    log_record = self
    return nil if log_record.ip_address.blank?
    ip_location =  ::Log::NhsApiLog.get_location_by_ipvigilante(log_record.ip_address)
    log_record.location = ip_location
    member_id = log_record.user_id
    log_record.save
    # if ip_location.present? && ip_location != "United Kingdom"
    #   message = "member id : #{member_id} access EMIS from #{ip_location}"
    #   mail_msg = "Someone accessed EMIS from #{ip_location}"
    #   SystemSecurityLogger.info message
    #   identifier_id = 1
    #   category = "emis"
    #   level = 1
    #   ::System::Security.save_security_issue(member_id,message,category,level, identifier_id,mail_msg )
    # end
  end

  def self.get_location_by_ipvigilante(ip_address)
    begin
      data = JSON.parse(`curl -X GET https://ipvigilante.com/json/#{ip_address}`)
      country = data["data"]["country_name"]
    rescue Exception => e 
      country = nil
    end
    country
  end

end