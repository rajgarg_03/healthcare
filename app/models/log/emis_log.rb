class Log::EmisLog
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :action           ,  type:String
  field :user_id          ,  type:String # requester(logged in user's member id)
  field :member_id        ,  type:String # requested
  field :api_version      ,  type:String
  field :ip_address       ,  type:String
  field :location         ,  type:String
  field :patient_id       ,  type:String #nhs number
  field :message_id       ,  type:String
  # field :sequence_number,  type:String # not in use after 
  field :organization_id  ,  type:String
  field :request_status   ,  type:String
  field :request_type     ,  type:String # Get/Put/Post..
  field :message          ,  type:String # Get/Put/Post..

  field :platform     ,  type:String  #  eg ios/android
  field :family_id        ,  type:String  #  eg 1,2,3
  DATATABLE_COLUMNS = [:action,:user_id, :member_id,:patient_id, :message_id, :sequence_number,:organization_id,:request_status]

  def self.get_data(options)
    start_date = Date.today - 2.day
    end_date = Date.today
    data = Log::EmisLog.where(:created_at=>{"$gte"=>start_date,"$lte"=>end_date})
    if options[:user_id].present?
      data = data.where(user_id:options[:user_id])
    end
    data = data.order_by("id desc")
    data
  end


  def self.get_requests(options={})
    if options.present?
      start_date = options[:start_date].blank? ? (Date.today - 30.days) : options[:start_date]
      end_date = options[:end_date].present? ? (options[:end_date].to_date + 1.day) : Date.today + 1
      data = Log::EmisLog.where(:created_at=>{"$gte"=>start_date,"$lte"=>end_date})
      if options[:request_action].present?
        data = data.where(action:options[:request_action].to_s.strip)
      end
      if options[:request_status].present?
        data = data.where(request_status:options[:request_status])
      end
      if options[:email].present?
        member_id = User.where(email:options[:email]).last.member_id rescue nil
        data = data.where(user_id:member_id) 
      end
      data = data.order_by("id desc")
    else
      data = Log::EmisLog.get_data({})
    end
    data
  end


  def self.datatable_filter(search_value, search_columns)
    return where(nil) if search_value.length < 5
    result = self
    filter = []
    search_keys = search_columns.values.map{|a| a["data"]} & ["action", "request_status", "ip_address", "location", "member_id", "message_id",]
    search_keys.each do |field_key|
      if field_key == "request_status"
        if "failed".include?(search_value.downcase) 
          val = false
        elsif "success".include?(search_value.downcase) 
          val = true
        else
          val = nil
        end
        filter << {"request_status"=>val} if !val.nil?
      else
        filter << {"#{field_key}"=>/#{Regexp.escape(search_value)}/i} #if value['searchable']
      end
    end



    # search_columns.each do |key, value|
      
    #   if value['data'].present? 
    #     if value['data'] == "created_at"
    #       # filter << {:created_at.gte=>(search_value).to_time} if (search_value.to_date rescue false)
    #     elsif value['data'] == "request_status"
    #       if value['searchable'].present?
    #         if "failed".include?(search_value.downcase) 
    #           val = false
    #         elsif "success".include?(search_value.downcase) 
    #           val = true
    #         else
    #           val = nil
    #         end
    #         filter << {"request_status"=>val} if !val.nil?
    #       end
    #     else
    #       filter << {"#{value['data']}"=>/#{Regexp.escape(search_value)}/i} if value['searchable']
    #     end
    #   end
    # end
    result = result.where("$or"=>filter)  
      
    result
  end  

  def self.datatable_order(order_column_index, order_dir)
    order_by("#{::Log::EmisLog::DATATABLE_COLUMNS[order_column_index]} #{order_dir}")
  end

  def self.create_record(current_user,member_id,request_data,options={})
    begin
      action = request_data[:action]
      user_id = current_user.id rescue nil
      member = Member.find(member_id || current_user.id)
      platform = (options[:platform] || current_user.devices.first.platform) rescue nil
      ip_address = current_user.user.current_sign_in_ip rescue nil
      ip_location = nil # ::Log::EmisLog.get_location_by_ipvigilante(ip_address)
      # if ip_location.present? && ip_location != "United Kingdom"
      #   SystemSecurityLogger.info "member id : #{current_user.id} access EMIS from #{ip_location}"
      # end
      api_version = 5
      request_type = request_data[:request_type] || "Post"
      clinc_link_detail = ::Clinic::LinkDetail.get_clinic_link_detail(member_id,current_user,options)rescue nil
      user_details = clinc_link_detail.link_info.with_indifferent_access rescue  {}
      family_id =  FamilyMember.where(member_id:member_id).last.family_id rescue nil
      message_id = request_data[:message_id]
      message = request_data[:message]
      patient_id = clinc_link_detail.patient_uid rescue nil
      # sequence_number = request_data[:sequence_number]
      organization_id = request_data[:organization_id] || member.organization_uid
      request_status = request_data[:request_status]
      emis_record = ::Log::EmisLog.create(message:message, :request_type=>request_type, :request_status=>request_status,:family_id=>family_id,:organization_id=>organization_id, :message_id=>message_id,:patient_id=>patient_id,:platform=>platform,:user_id=>user_id,:ip_address=>ip_address, :location=> ip_location,:member_id=>member_id, :api_version=> api_version, :action=>action)
      emis_record.delay.update_ip_location
    rescue Exception=> e
      puts e.message
      Rails.logger.info "Error inside Emis create_record \n"
      Rails.logger.info e.message
      
    end
  end

  def update_ip_location
    emis_record = self
    ip_location =  ::Log::EmisLog.get_location_by_ipvigilante(emis_record.ip_address)
    emis_record.location = ip_location
    member_id = emis_record.user_id
    emis_record.save
    if ip_location.present? && ip_location != "United Kingdom"
      message = "member id : #{member_id} access EMIS from #{ip_location}"
      mail_msg = "Someone accessed EMIS from #{ip_location}"
      SystemSecurityLogger.info message
      identifier_id = 1
      category = "emis"
      level = 1
      ::System::Security.save_security_issue(member_id,message,category,level, identifier_id,mail_msg )
    end
  end

  def self.get_location_by_ipvigilante(ip_address)
    begin
      data = JSON.parse(`curl -X GET https://ipvigilante.com/json/#{ip_address}`)
      country = data["data"]["country_name"]
    rescue Exception => e 
      country = nil
    end
    country
  end

end