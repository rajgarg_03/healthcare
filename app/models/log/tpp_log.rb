class Log::TppLog
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :action           ,  type:String
  field :user_id          ,  type:String # requester
  field :member_id        ,  type:String # requested
  field :api_version      ,  type:String
  field :ip_address       ,  type:String
  field :location         ,  type:String
  field :patient_id       ,  type:String #nhs number
  field :message_id       ,  type:String
  field :sequence_number  ,  type:String
  field :organization_id  ,  type:String
  field :request_status   ,  type:String

  field :platform     ,  type:String  #  eg ios/android
  field :family_id        ,  type:String  #  eg 1,2,3
  DATATABLE_COLUMNS = [:action,:user_id, :member_id,:patient_id, :message_id, :sequence_number,:organization_id,:request_status]

  def self.get_data(options)
    start_date = Date.today - 2.day
    end_date = Date.today
    Log::TppLog.where(:created_at=>{"$gte"=>start_date,"$lte"=>end_date}).order_by("id desc")
  end


  def self.datatable_filter(search_value, search_columns)
    return all if search_value.blank?
    result = self
    filter = []
    search_columns.each do |key, value|
      
      if value['data'].present? 
        if value['data'] == "created_at"
           filter << {:created_at.gte=>(search_value).to_time} if (search_value.to_date rescue false)
        else
          filter << {"#{value['data']}"=>/#{Regexp.escape(search_value)}/i} if value['searchable']
        end
      end
    end
    result = result.where("$or"=>filter) 
    result
  end  

  def self.datatable_order(order_column_index, order_dir)
    order_by("#{::Log::TppLog::DATATABLE_COLUMNS[order_column_index]} #{order_dir}")
  end

  def self.create_record(current_user,member_id,request_data,options={})
    begin
      action = request_data[:action]
      user_id = current_user.id rescue nil
      member = Member.find member_id
      platform = (options[:platform] || current_user.devices.first.platform) rescue nil
      ip_location = nil
      api_version = 5
      user_details = ::Clinic::LinkDetail.get_clinic_link_detail(member_id,current_user,options).last.link_info.with_indifferent_access rescue  {}
      family_id =  FamilyMember.where(member_id:member_id).last.family_id rescue nil
      message_id = request_data[:message_id]
      patient_id = request_data[:patient_id] || user_details[:account_id]
      sequence_number = request_data[:sequence_number]
      organization_id = request_data[:organization_id] || member.organization_uid
      request_status = request_data[:request_status]
      ::Log::TppLog.create(:request_status=>request_status,:family_id=>family_id,:organization_id=>organization_id, :message_id=>message_id,:patient_id=>patient_id,:sequence_number=>sequence_number, :platform=>platform,:user_id=>user_id,:ip_address=>options[:ip_address], :location=> ip_location,:member_id=>member_id, :api_version=> api_version, :action=>action)
      
    rescue Exception=> e
      puts e.message
      Rails.logger.info "Error inside Tpp create_record \n"
      Rails.logger.info e.message
      
    end
  end

end