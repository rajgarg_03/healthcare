class Notification
  include Mongoid::Document
  include Mongoid::Timestamps
  field :title, type: String
  field :link, type: String
  field :member_id, type: String
  field :sender_id, type: String  
  field :type, type: String  
  field :child_member_id, type: String  
  field :checked, type: Boolean, :default => false
  field :user_notification_id, type: String 
  belongs_to :member, class_name: 'Member', foreign_key: :member_id
  belongs_to :sender, class_name: 'Member', foreign_key: :sender_id

  scope :unread, where(:checked => false)
  # validates :title, :link, :member, :presence => true
  # validates :title, :member, :presence => true
  index ({member_id:1})
  index ({child_member_id:1})
  index ({user_notification_id:1})
  index ({checked:1})
  
  def get_deep_link_data(current_user,api_version=1)
    notification = self 
    deeplink_data = {}
    begin
      usernotificationobj = notification.created_at <= (Date.today - 3.month) ? PushNotificationSplitedData : UserNotification
      if notification.user_notification_id.present?
        user_notification_obj =   usernotificationobj.find(notification.user_notification_id)
      else
        user_notification_obj =  usernotificationobj.where(type:notification.type, member_id:notification.child_member_id,message:notification.title).last 
      end
    rescue Exception=>e 
     user_notification_obj = nil
    end
    if user_notification_obj.present?
      deeplink_data = user_notification_obj.get_deeplink_data(current_user,api_version)
    end
    deeplink_data
  end

  def self.delete_notification(member_id)
    Notification.or({sender_id:member_id},{member_id:member_id},{child_member_id:member_id}).delete_all
  end
end
