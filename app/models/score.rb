class Score
  include Mongoid::Document
  include Mongoid::Timestamps
  WillPaginate.per_page = 10
  # field :name,           :type => String
  field :member_id,      :type=> String
  field :family_id,      :type=> String
  field :point,          :type=> Integer
  field :activity,       :type => String
  field :activity_type,  :type => String
  field :activity_id,    :type=> String
  # Points={"Health"=>20,"Vaccination"=> 20, "Member"=> 200, "Invitation" => 20}
  Points={"Signup"=>150,"Health"=>20,"Vaccination"=> 25, "Member"=> 75, "Invitation" => 50, "Family" => 20, "Child" => 30, "Milestone" => 20, "Timeline" => 20}
  belongs_to :member
  index({member_id:1})
  index({family_id:1})

  BadgeClass = {"NEWBIE"=> "badge-type2","ACTIVEBRONZE"=>"badge-type1", "ACTIVESILVER"=>"badge-type2","ACTIVEGOLD"=>"badge-type4",
                "SMARTBRONZE"=>"badge-type1", "SMARTSILVER"=>"badge-type2","SMARTGOLD"=>"badge-type4",
                "PROBRONZE"=>"badge-type1", "PROSILVER"=>"badge-type2","PROGOLD"=>"badge-type4","SPACE"=>"badge-type4" }

  BadgeNameHome =  {"NEWBIE"=> "","ACTIVEBRONZE"=>"ACTIVE", "ACTIVESILVER"=>"ACTIVE","ACTIVEGOLD"=>"ACTIVE",
                "SMARTBRONZE"=>"SMART", "SMARTSILVER"=>"SMART","SMARTGOLD"=>"SMART",
                "PROBRONZE"=>"PRO", "PROSILVER"=>"PRO","PROGOLD"=>"PRO","SPACE"=>"" }

  BadgeName =  {"NEWBIE"=> "NEWBIE","ACTIVEBRONZE"=>"ACTIVE", "ACTIVESILVER"=>"ACTIVE","ACTIVEGOLD"=>"ACTIVE",
                "SMARTBRONZE"=>"SMART", "SMARTSILVER"=>"SMART","SMARTGOLD"=>"SMART",
                "PROBRONZE"=>"PRO", "PROSILVER"=>"PRO","PROGOLD"=>"PRO","SPACE"=>"SPACE"} 

  BadgeType =  {"NEWBIE"=> "","ACTIVEBRONZE"=>"BRONZE", "ACTIVESILVER"=>"SILVER","ACTIVEGOLD"=>"GOLD",
                "SMARTBRONZE"=>"BRONZE", "SMARTSILVER"=>"SILVER","SMARTGOLD"=>"GOLD",
                "PROBRONZE"=>"BRONZE", "PROSILVER"=>"SILVER","PROGOLD"=>"GOLD","SPACE"=>"" }

  BadgeParent =  {"NEWBIE"=> "","ACTIVEBRONZE"=>"Parent", "ACTIVESILVER"=>"Parent","ACTIVEGOLD"=>"Parent",
                "SMARTBRONZE"=>"Parent", "SMARTSILVER"=>"Parent","SMARTGOLD"=>"Parent",
                "PROBRONZE"=>"Parent", "Parent"=>"Parent","PROGOLD"=>"Parent","SPACE"=>"Parent" }
 
  # BadgePoint = {"NEWBIE"=> 0,"ACTIVE BRONZE"=>100, "ACTIVE SILVER" => 200,"ACTIVE GOLD"=>300,
  #               "SMART BRONZE"=>400,"SMART SILVER"=>500,"SMART SILVER"=>600,"SMART GOLD"=>700,
  #               "PRO BRONZE"=>800, "PRO SILVER"=>900,"PRO GOLD"=>1000,
  #               "SPACE"=> 2000 }

  BadgePoint = {"NEWBIE"=> 150,"ACTIVE BRONZE"=>750, "ACTIVE SILVER" => 1000,"ACTIVE GOLD"=>1250,
                "SMART BRONZE"=>1500,"SMART SILVER"=>1750,"SMART GOLD"=>2000,"PRO BRONZE"=>2250, "PRO SILVER"=>2500,"PRO GOLD"=>2750,
                "SPACE"=> 3500 }

  def Score.badge(score)
    case score
      when 150..499
        "NEWBIE"
      when 500..999
        "ACTIVEBRONZE"
      when 1000..1249
         "ACTIVESILVER"
      when 1250..1499
         "ACTIVEGOLD"
      when 1500..1749
         "SMARTSILVER"
      when 1750..1999
        "SMARTBRONZE"
      when 2000..2249
        "SMARTGOLD"
      when 2250..2499
        "PROBRONZE"
      when 2500..2749
        "PROSILVER"
      when 2750..3499
        "PROGOLD"
      when 3500..350000
        "SPACE"
    end
  end

end


=begin
Entry point from
Health
vaccination
Milestone 
Family
=end