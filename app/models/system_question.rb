class SystemQuestion
  include Mongoid::Document
  include Mongoid::Timestamps
  QUESTION_TYPE = {"Yes-No" => 1, "High-Low" => 2}
  
  field :question_text, type: String
  field :question_type, type: String
  field :set_high, type: String
  field :high_label, type: String
  field :set_low, type: String
  field :low_label, type: String
  # field :questionnaire_id, type: Integer
  # field :answer
  belongs_to :system_questionnaire
  # has_and_belongs_to_many :questionnaire
  
  # has_many :answers
  validates :question_text, :question_type, :presence => true
  validates :set_high, :set_low, :presence => true, :if => :check_high_low
  
  def check_high_low
    question_type == "2"
  end
  
end