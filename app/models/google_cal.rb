class GoogleCal
  include Mongoid::Document  
   include Mongoid::Timestamps

  field :uid, type: String
  field :token, type: String
  field :refresh_token, type: String 
  field :email, type: String
  belongs_to :user

  def init_client
    client = Google::APIClient.new
    @token = self.token
    @refresh_token =self.refresh_token
    
    client.authorization.access_token = @token #token is taken from auth table
    client.authorization.client_id = APP_CONFIG['google_api_id']
    client.authorization.client_secret = APP_CONFIG['google_api_secret']
    client.authorization.refresh_token = @refresh_token
    return client
  end

  def get_refresh_token
    data = {
      :client_id => APP_CONFIG['google_api_id'],
      :client_secret => APP_CONFIG['google_api_secret'],
      :refresh_token => self.refresh_token,
      :grant_type => "refresh_token"
    }
    @response = ActiveSupport::JSON.decode(RestClient.post "https://accounts.google.com/o/oauth2/token", data)
    if @response["access_token"].present?
      self.token = @response["access_token"]
      save
      return @response["access_token"]
    else
      puts "No Token :("
    end
  rescue RestClient::BadRequest => e
    # Bad request
    puts "Error: bad request :("
  end

  def delete_google_event(event_id,type="event")
    google_event_id = type + event_id
    client = Google::APIClient.new 
    client.authorization.access_token = self.token
    service = client.discovered_api('calendar', 'v3')
    result = client.execute(:api_method => service.events.delete,:parameters => {'calendarId' => 'primary', 'eventId' => google_event_id})
    if result.status != 200
      client = Google::APIClient.new 
      client.authorization.access_token = get_refresh_token
      service = client.discovered_api('calendar', 'v3')
      result = client.execute(:api_method => service.events.delete,:parameters => {'calendarId' => 'primary', 'eventId' => google_event_id})
    end
  end

  def create_google_event(event, type="event",action="create",option={})
   client = Google::APIClient.new 
   google_event_id = type + event.id.to_s
    if  type == "jab"
    if event.due_on.blank? && !event.est_due_time.blank?
          status = "-Estimated due on"
        else
          status = "-Due On"
        end
      app_event = {
        'summary'  =>   (option["name"]|| option[:name] || "" )+ " Jab" +  option[:index].to_s + status,
        'id' => "jab" + event.id.to_s,
        'start' => {
          'date' => (event.due_on|| Date.today).in_time_zone(option["time_zone"]).to_date.strftime("%Y-%m-%d"),
          'timeZone' => ActiveSupport::TimeZone.find_tzinfo(option["time_zone"])
          },  
        'end' => {         
          'date' => (event.due_on || Date.today).in_time_zone(option["time_zone"]).to_date.strftime("%Y-%m-%d"),
          'timeZone' => ActiveSupport::TimeZone.find_tzinfo(option["time_zone"])
          },
      }
    else
      app_event = {
        'summary'  =>  event.name,
        'id' => "event" + event.id.to_s,
        'start' => {
          'dateTime' =>  event.start_date.to_datetime.rfc3339},  
        'end' => {         
          'dateTime' => (event.end_date || event.start_date).to_datetime.rfc3339}, 
      }
    end

    client.authorization.access_token = self.token || get_refresh_token
    service = client.discovered_api('calendar', 'v3')
    if action == "create"
      result = client.execute(:api_method => service.events.insert,:parameters => {'calendarId' => 'primary'},    :body => JSON.dump(app_event), :headers => {'Content-Type' => 'application/json'})
      if result.status != 200
        client = Google::APIClient.new 
        client.authorization.access_token = get_refresh_token
        service = client.discovered_api('calendar', 'v3')
        result = client.execute(:api_method => service.events.insert, :parameters => {'calendarId' => 'primary'}, :body => JSON.dump(app_event), :headers => {'Content-Type' => 'application/json'})
      end
    elsif action == "update"
      result = client.execute(:api_method => service.events.update, :parameters => {'calendarId' => 'primary', 'eventId' => google_event_id},:body_object => app_event,:headers => {'Content-Type' => 'application/json'})
      if result.status != 200
        client = Google::APIClient.new 
        client.authorization.access_token = get_refresh_token
        service = client.discovered_api('calendar', 'v3')
        result = client.execute(:api_method => service.events.update, :parameters => {'calendarId' => 'primary', 'eventId' => google_event_id},:body_object => app_event,:headers => {'Content-Type' => 'application/json'})
      end
    else
    end
    result
  end

  def get_all_calendars
    client = init_client
    service = client.discovered_api('calendar', 'v3')
    @result = client.execute(
      :api_method => service.calendar_list.list,
      :parameters => {},
      :headers => {'Content-Type' => 'application/json'})
  end

end

#_without_delay