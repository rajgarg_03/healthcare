class BasicSetupEvaluator
  include Mongoid::Document
  include Mongoid::Timestamps

  field :member_id, :type => String 
  field :welcome_screen_count, :type => Integer 
  field :user_families_count, :type => Integer 
  field :summary_screen_count, :type=> Integer
  field :family_welcome_screen_count, :type=> Integer

  belongs_to :member #NOTE: Belong to parent_member only 
  
  index({member_id:1})

def self.run(member,invitaion_id=nil,version=1,options={})
  
  case version
  when 1
    V1_run(member,invitaion_id)
  when 2
    V2_run(member,invitaion_id)
  when 3
    V3_run(member,invitaion_id)
  when 4
    V3_run(member,invitaion_id,version,options)
  when 5
    V3_run(member,invitaion_id,version,options)
  when 6
    V3_run(member,invitaion_id,version,options)
  else
    V3_run(member,invitaion_id)
  end
end
 #For V1 
def self.V1_run(member,invitaion_id=nil)
    wc = af = ac = bs = false
    count = 0
    @current_user = member
    check_for_wc = check_for_fc = check_for_ac = true
    data = [] 
    #TODO: Not being used now, uncomment when confirmed
    skipped_family_ids = [] #SkipBasicSetupAction.where(skipped: true,member_id:member.id,screen_name:"welcome_to_family_screen",:skipped_up_to.gt=>Time.now).pluck("screen_id")
    user_families_ids = member.user_families.map{|a| a.id.to_s}
    user_families_ids = user_families_ids - skipped_family_ids
    if check_for_wc
      wc_screens = []
      member_basic_setup_evalu = member.basic_setup_evaluator
      member_basic_setup_evalu ||= member.create_basic_setup_evaluator(welcome_screen_count: 0,summary_screen_count:0,  user_families_count: 0)
      if member_basic_setup_evalu["welcome_screen_count"] ==0
        wc = true
        bs = true
        member_basic_setup_evalu.welcome_screen_count = member_basic_setup_evalu.welcome_screen_count+1
        member_basic_setup_evalu.save
        # wc_screens << {"welcome_screen" => {:user_basic_info=>User.user_basic_info(member)} }
        wc_screens << {"welcome_screen" => {} }
      end

      #TODO: Not being used now, uncomment when confirmed
      skipped_invitation_ids = [] #SkipBasicSetupAction.where(skipped: true,member_id:member.id,screen_name:"accept_reject_screen",:skipped_up_to.gt=>Time.now).pluck("screen_id")
      member_received_join_family_invitations = (member.received_join_family_invitations - Invitation.in(:id=>skipped_invitation_ids))

      if member_received_join_family_invitations.present?
        wc = true
        bs = true
        check_for_af = false if user_families_ids.present?
        check_for_fc = false if user_families_ids.present?
        wc_screens << {"accept_reject_screen" => {:invitation_data=>member.received_invitations_data([member_received_join_family_invitations[0]])[0] } }
      end

      if member_basic_setup_evalu["user_families_count"] == 0 && user_families_ids.present? && invitaion_id.present?
        member_basic_setup_evalu.user_families_count =  member_basic_setup_evalu.user_families_count + 1
        member_basic_setup_evalu.save
        wc = true
        bs = true
        check_for_af = false
        check_for_fc = false
        wc_screens << {"welcome_to_family_screen" => {:family_id=>user_families_ids.last} }
      end
      if wc 
        data << {"NYWelcomeController" => wc_screens} 
        count = count +1
      end
    end

    if check_for_fc
      if user_families_ids.blank?
        af_screens =[]
        af = true
        bs = true
        check_for_ac = false
        count = count +1
        data << {"NYAddFamilyController"=> {:role=>nil}  } 
      end
    end

    if check_for_ac
      if member.childern_ids.blank?
        bs = true
        ac = true
        count = count +1
        families = Family.in(id:member.famlies_without_child).map{|a| a.id}
        #TODO: Not being used now, uncomment when confirmed
        skipped_family_ids = [] #SkipBasicSetupAction.where(skipped: true,member_id:member.id,screen_name:"welcome_to_family_screen",:skipped_up_to.gt=>Time.now).pluck("screen_id")
        families = families - skipped_family_ids
        data << {"NYAddChildController"=>  {:family_id=>families.last} }
      end
    end
     if member_basic_setup_evalu["summary_screen_count"].to_i == 0
       member_basic_setup_evalu.summary_screen_count = member_basic_setup_evalu["summary_screen_count"].to_i + 1
       member_basic_setup_evalu.save
       bs = true
       data << {"NYAllSetToGoScreen"=>  {} }
     end
    #data << {"NYAllSetToGoScreen"=>  {} } if bs
    controller_status =  {"controller_count"=> count,  "NYWelcomeController"=>wc, "NYAddFamilyController" =>af, "NYAddChildController"=> ac, "basic_setup"=>bs}
    return_data = []
    return_data << controller_status
    return_data << data
    # data << {"wc"=>wc}
    # data << {"af" =>af}
    # data << {"ac"=> ac}
    return [return_data.flatten,controller_status]
  end

# check_for_controller For V2
  def self.V2_run(member,invitaion_id=nil)
    wc = af = ac = bs = false
    @current_user = member
    count = 0
    check_for_wc = check_for_fc = check_for_ac = check_for_addspouse = true
    data = [] 
    #TODO: Not being used now, uncomment when confirmed
    skipped_family_ids = [] #SkipBasicSetupAction.where(skipped: true,member_id:member.id,screen_name:"welcome_to_family_screen",:skipped_up_to.gt=>Time.now).pluck("screen_id")
    user_families_ids = member.user_families.map{|a| a.id.to_s} 
    user_families_ids = user_families_ids - skipped_family_ids
    family_data  = BasicSetupEvaluator.family_screen_data(member,user_families_ids)
    

    if check_for_wc
      wc_screens = []
      member_basic_setup_evalu = member.basic_setup_evaluator
      member_basic_setup_evalu ||= member.create_basic_setup_evaluator(family_welcome_screen_count:0, welcome_screen_count: 0,summary_screen_count:0,  user_families_count: 0)
      #check for welcome screen
      if member_basic_setup_evalu["welcome_screen_count"] ==0
        wc = true
        bs = true
        member_basic_setup_evalu.welcome_screen_count = member_basic_setup_evalu.welcome_screen_count+1
        member_basic_setup_evalu.save
        wc_screens << {"welcome_screen" => {} }
      end

      #TODO: Not being used now, uncomment when confirmed
      skipped_invitation_ids = [] #SkipBasicSetupAction.where(skipped: true,member_id:member.id,screen_name:"accept_reject_screen",:skipped_up_to.gt=>Time.now).pluck("screen_id")
      
      user_created_families = member.families.pluck("id")
      family_user_combination = FamilyMember.where(member_id:member.id).in(:family_user_count=>[0,nil]).not_in(family_id:user_created_families).order_by("created_at desc")
      
      #check for family welcome screen
      if family_user_combination.present?
        accpted_family_ids = family_user_combination.pluck("family_id")
        user_accepted_families = Family.in(id:accpted_family_ids)
        family_user_combination.each do |fm|
          fm.family_user_count = 1
          fm.save
        end
        member_basic_setup_evalu.family_welcome_screen_count =  (member_basic_setup_evalu.family_welcome_screen_count + 1) rescue 1
        member_basic_setup_evalu.summary_screen_count = member_basic_setup_evalu["summary_screen_count"].to_i + 1
        member_basic_setup_evalu.save
        wc = true
        bs = true
        wc_screens << {"welcome_to_family_screen" =>family_screen_data(member, user_accepted_families)}
      end

      if wc 
        data << {"NYWelcomeController" => wc_screens} 
        count = count +1
      end
    end

    if check_for_fc
      if user_families_ids.blank?
        af_screens =[]
        af = true
        bs = true
        check_for_ac = false
        count = count +1
        data << {"NYAddFamilyController"=> {:role=>nil}  } 
      end
    end

    if check_for_ac
      if member.childern_ids.blank?
        bs = true
        ac = true
        count = count +1
        families = Family.in(id:member.famlies_without_child).map{|a| a.id}
        #TODO: Not being used now, uncomment when confirmed
        skipped_family_ids = [] #SkipBasicSetupAction.where(skipped: true,member_id:member.id,screen_name:"welcome_to_family_screen",:skipped_up_to.gt=>Time.now).pluck("screen_id")
        families = families - skipped_family_ids
        data << {"NYAddChildController"=>  {:family_id=>families.last} }
      end
    end

    if member_basic_setup_evalu["summary_screen_count"].to_i == 0 || af || ac
      member_basic_setup_evalu.summary_screen_count = member_basic_setup_evalu["summary_screen_count"].to_i + 1
      member_basic_setup_evalu.save
      bs = true 
      data << {"NYAllSetToGoScreen"=>  family_data }
    end
    controller_status =  {"controller_count"=> count,  "NYWelcomeController"=>wc, "NYAddFamilyController" =>af, "NYAddChildController"=> ac, "basic_setup"=>bs}
    return_data = []
    return_data << controller_status
    return_data << data
     
    return [return_data.flatten,controller_status]
  end

  def self.welcome_screen_status_for_user(member,api_version,options={})
    begin
      welcome_screen_status = true
      user  = member.user
      if ["NhsLogin","Apple"].include?(user.user_account_type)
        welcome_screen_status = false
      end
    rescue Exception => e 
      Rails.logger.info "Error inside welcome_screen_status_for_user...............#{e.message}"
      welcome_screen_status = true
    end
    welcome_screen_status
  end
  # check_for_controller For V3
  def self.V3_run(member,invitaion_id=nil,api_version=3,options={})
    wc = af = ac = bs  = proxy_setup_contoller = nhs_login_im1 = false
    @current_user = member
    user  = member.user
    is_nhs_login = user.account_type_allowed?("Nhs")
    welcome_screen_status =  BasicSetupEvaluator.welcome_screen_status_for_user(member,api_version,options)
    count = 0
    check_for_wc = check_for_fc = check_for_ac = check_for_addspouse = true
    data = [] 
    #TODO: Not being used now, uncomment when confirmed
    skipped_family_ids = [] #SkipBasicSetupAction.where(skipped: true,member_id:member.id,screen_name:"welcome_to_family_screen",:skipped_up_to.gt=>Time.now).pluck("screen_id")
    user_families_ids = member.user_family_ids
    family_exist = user_families_ids.present?
    user_families_ids = user_families_ids - skipped_family_ids
    api_version = api_version
    family_data  = BasicSetupEvaluator.family_screen_data(member,user_families_ids,api_version)
    # Check for Welcome Controller
    if check_for_wc 
      wc_screens = []
      member_basic_setup_evalu = member.basic_setup_evaluator
      member_basic_setup_evalu ||= member.create_basic_setup_evaluator(family_welcome_screen_count:0, welcome_screen_count: 0,summary_screen_count:0,  user_families_count: 0)
      #check for welcome screen
      # don't add welcome screen if user coming from alexa flow
      if welcome_screen_status && member_basic_setup_evalu["welcome_screen_count"] == 0 && !User.is_alexa_platform?(options[:platform])
        wc = true
        bs = true
        member_basic_setup_evalu.welcome_screen_count = member_basic_setup_evalu.welcome_screen_count+1
        member_basic_setup_evalu.save
        wc_screens << {"welcome_screen" => {} }
      end

      #TODO: Not being used now, uncomment when confirmed
      skipped_invitation_ids = [] #SkipBasicSetupAction.where(skipped: true,member_id:member.id,screen_name:"accept_reject_screen",:skipped_up_to.gt=>Time.now).pluck("screen_id")
      
      user_created_families = member.families.pluck("id")
      family_user_combination = FamilyMember.where(member_id:member.id).in(:family_user_count=>[0,nil]).not_in(family_id:user_created_families).order_by("created_at desc")
      
      #check for family welcome screen
      if family_user_combination.present?
        accpted_family_ids = family_user_combination.pluck("family_id")
        user_accepted_families = Family.in(id:accpted_family_ids)
        family_user_combination.each do |fm|
          fm.family_user_count = 1
          fm.save
        end
        member_basic_setup_evalu.family_welcome_screen_count =  (member_basic_setup_evalu.family_welcome_screen_count + 1) rescue 1
        member_basic_setup_evalu.summary_screen_count = member_basic_setup_evalu["summary_screen_count"].to_i + 1
        member_basic_setup_evalu.save
        wc = true
        bs = true
        wc_screens << {"welcome_to_family_screen" =>family_screen_data(member, user_accepted_families)}
      end

      if wc 
        data << {"NYWelcomeController" => wc_screens} 
        count = count +1
      end
    end

    # Check for NHS IM1 Controller
    fully_linked = @current_user.clinic_link_state > 2 # fully linked
    if fully_linked  && family_exist
      proxy_member_list = user.get_proxy_user_list(options) rescue nil
      if proxy_member_list.present? && proxy_member_list.select{|a| !a[:proxy_linked]}.present?
        bs = true
        proxy_setup_contoller = true
        nhs_login_im1 = false
        family_id = @current_user.user_families.first.id.to_s
        data << {"NYAddProxyController" => {:proxy_list=>proxy_member_list,:family_id=>family_id}} 
        count = count + 1
      end
    elsif is_nhs_login && !fully_linked
      refresh_token = ::Nhs::LoginDetail.where(user_id:user.id).last.refresh_token rescue nil
      if refresh_token.present?
        begin
          # Fetch user information
          token = ::Nhs::NhsApi.generate_token_via_refresh_token(refresh_token)["access_token"]
          user_info = ::Nhs::NhsApi.fetch_user_info(token)
          im_info = ::Nhs::NhsApi.format_nhs_login_user_info(@current_user,user_info,options)
          options[:current_user] = @current_user 
          options[:im1_account_id] =    im_info[:account_id]
          
          # Find Clinic organization
          clinic_info = ::Nhs::NhsApi.get_clinic_by_ods_code(@current_user,user.member_id,im_info[:practice_ods_code],options)
          # Check clinic if linkable
          clinic_type = ::Clinic::ClinicDetail.identify_clinic_type(@current_user,clinic_info,options)
          linkable_status = ::Clinic::ClinicDetail.clinic_linkable_status(clinic_type,options) == "active"
          # Setup clinic
          ::Clinic::Organization.setup_clinic_with_nhs(@current_user,clinic_type, clinic_info,options)
          @current_user.reload
          clinic_detail = @current_user.get_clinic_detail(@current_user,options) || Clinic::ClinicDetail.new
          member_clinic_info = clinic_detail.format_data(@current_user,@current_user,options) rescue nil
          data << {"NYNHSLoginIM1Controller" => {:clinic_detail=>member_clinic_info,:im_detail=>im_info,:linkable_status=>linkable_status,member_id:@current_user.id}} 
          bs = true
          nhs_login_im1 = true
          count = count + 1
        rescue Exception => e 
          Rails.logger.info "Error inside NYNHSLoginIM1Controller.............#{e.message}"
        end
      end
    end
    
  

    # Check for Family Controller
    if check_for_fc
      if user_families_ids.blank?
        af_screens =[]
        af = true
        bs = true
        check_for_ac = false
        count = count +1
        data << {"NYAddFamilyController"=> {:role=>nil}  } 
      end
    end
    # Chech add child controller if proxy_setup_contoller is not active
    if check_for_ac && !proxy_setup_contoller
      if member.childern_ids.blank?
        bs = true
        ac = true
        count = count +1
        families = Family.in(id:member.famlies_without_child).map{|a| a.id}
        #TODO: Not being used now, uncomment when confirmed
        skipped_family_ids = [] #SkipBasicSetupAction.where(skipped: true,member_id:member.id,screen_name:"welcome_to_family_screen",:skipped_up_to.gt=>Time.now).pluck("screen_id")
        families = families - skipped_family_ids
        data << {"NYAddChildController"=>  {:family_id=>families.last} }
      end
    end
    check_for_addspouse = !(wc || af || ac)  && !User.is_alexa_platform?(options[:platform])  #check no other action should available in Setup controller
    if check_for_addspouse
      add_spouse_status = false
  
      # ID in SkipBasicSetupAction is family id . 
      skip_addspouse_for_families = SkipBasicSetupAction.where(:skipped=>true,member_id:member.id.to_s,screen_name:"add_spouse_screen").or({:skipped_up_to.gt =>Time.now.in_time_zone},{:skipped_forever=>true}).pluck(:screen_id).map(&:to_s)
  
      all_families_list = user_families_ids - skip_addspouse_for_families
      invitation_for_father_from_family = Invitation.in(family_id:all_families_list).in(role:["Father","father"]).where(status:"invited").pluck(:family_id).map(&:to_s).uniq
      invitation_for_mother_from_family = Invitation.in(family_id:all_families_list).in(role:["Mother","mother"]).where(status:"invited").pluck(:family_id).map(&:to_s).uniq
      
      families_list = FamilyMember.in(family_id:all_families_list,role:["Father","Mother"]).map{|a| a.family_id.to_s}
      families_list = families_list + invitation_for_mother_from_family + invitation_for_mother_from_family


      family_with_father = FamilyMember.in(family_id:families_list).where(role:"Father").pluck(:family_id).map(&:to_s).uniq
      family_with_mother = FamilyMember.in(family_id:families_list).where(role:"Mother").pluck(:family_id).map(&:to_s).uniq
      
      family_without_mother = families_list - ( family_with_mother + invitation_for_mother_from_family)
      family_without_father = families_list - ( family_with_father + invitation_for_father_from_family)

      add_spouse_data = []
      family_without_mother.uniq.each do |family_id|
        add_spouse_data << {:add_spouse_screen=>{:family_id=>family_id,:role=>FamilyMember::ROLES[:mother]} }
      end
      family_without_father.uniq.each do |family_id|
        add_spouse_data << {:add_spouse_screen=>{:family_id=>family_id,:role=>FamilyMember::ROLES[:father]}}
      end
      if add_spouse_data.present?  
        bs = true
        add_spouse_status =  true
        data << {"NYAddSpouseController"=>  add_spouse_data }
        count = count + 1
      end
    end
    if ((member_basic_setup_evalu["summary_screen_count"].to_i == 0 || af || ac) && !User.is_alexa_platform?(options[:platform]))
      member_basic_setup_evalu.summary_screen_count = member_basic_setup_evalu["summary_screen_count"].to_i + 1
      member_basic_setup_evalu.save
      bs = true
      data << {"NYAllSetToGoScreen"=>  family_data }
    end
    
    if User.is_alexa_platform?(options[:platform]) && !af && !ac
      bs = true
      data << {"NYHelpScreenController"=>  member.alexa_help_screen_data(options[:platform], options[:app_name]) }
    end
    controller_status =  {"controller_count"=> count,"NYAddSpouseController"=> add_spouse_status, "NYWelcomeController"=>wc, "NYAddFamilyController" =>af, "NYAddChildController"=> ac, "basic_setup"=>bs,"NYNHSLoginIM1Controller"=>nhs_login_im1, "NYAddProxyController"=> proxy_setup_contoller}
    return_data = []
    return_data << controller_status
    return_data << data
     
    return [return_data.flatten,controller_status]
  end

  
 
  def self.family_screen_data(member, user_families_ids=[],api_version=1)
     @current_user = member
    user_families_ids = (user_families_ids.entries.blank? ?  member.user_family_ids : user_families_ids.entries) rescue []
    family_data = []
    user_families = Family.in(id:user_families_ids)
    user_families.each do |u_f|
      family_data << family_screen_data_prepare(u_f,{:api_version=>api_version})
    end
    family_data << family_screen_data_prepare(nil,{:api_version=>api_version}) if user_families_ids.blank?
    family_data
  end
  
  #BasicSetupEvaluator.family_screen_data_prepare(Family.first)
  def self.family_screen_data_prepare(family=nil,options={:invited=>false,:api_version=>1,:current_user=>nil})
    data = []
    if family.blank?
      data << {title:"No family created",description: "",completed:false}
      data << {title:"No child added",description: "",completed: false}
      data << {title:"No tools activated",description: "",completed: false}
      return {:family_id=>nil,:progress=> data }
    end
    completed_status = FamilyMember.in(family_id:family.id).in(:role=>FamilyMember::CHILD_ROLES).present?
    childerns_ids = family.family_members.in(role:FamilyMember::CHILD_ROLES).pluck("member_id")
    child_members = Member.in(id:childerns_ids)
    @current_user = @current_user || options[:current_user]
    all_tools = Nutrient.valid_to_launch(@current_user,options[:api_version]).where(:supported_api_version.lte=>options[:api_version]).order_by("position ASC").pluck("title") - ["Manage Tools"]
    tool_only_for_born = Nutrient.where(:tool_for.in =>["child"],:supported_api_version.lte =>options[:api_version]).valid_to_launch(@current_user,options[:api_version]).order_by("position ASC").map(&:title)
    tool_only_for_pregnancy = Nutrient.where(:tool_for.in =>["pregnancy"],:supported_api_version.lte =>options[:api_version]).valid_to_launch(@current_user,options[:api_version]).order_by("position ASC").map(&:title)
    
    data << {title:"Family created",description:family.created_at.to_api_date2,completed:true}
    child_members.each do |member|
      member_data = []
      updated_tools, not_updated_tools = members_acitvated_tools(member,all_tools,tool_only_for_born,tool_only_for_pregnancy)
      data << {title:"Tools updated for #{member.first_name}",description:updated_tools.join(", "),completed:updated_tools.present?} if updated_tools.present?
      data << {title:"Tools to be updated for #{member.first_name}", description: (not_updated_tools).join(", "),completed:(not_updated_tools).blank? } if not_updated_tools.present?
    
    end
    if data.blank? || child_members.blank?
      member_data = []
      member_data << {title:"Family created",description: family.created_at.to_api_date2,completed: true}
      member_data << {title:"No child added",description: "",completed: false}
      if options[:invited] == false
        member_data << {title:"No tools activated",description: "",completed: false}
      end
      data = member_data    
      end

    {:family_id=>family.id,:progress=> data }
  end

  def self.members_acitvated_tools(child_member,sys_tools,tool_only_for_born, tool_only_for_pregnancy)
      updated_tools = []
      updated_tools << "Documents" if child_member.documents.present?
      updated_tools << "Timeline" if child_member.timelines.present?
      updated_tools << "Calendar" if child_member.events.present?
    if child_member.is_expected?
      tool_list = sys_tools - tool_only_for_born
      updated_tools << "Prenatal Tests" if child_member.medical_events.present?
    else
      tool_list = sys_tools - tool_only_for_pregnancy
      updated_tools << "Immunisations" if child_member.vaccinations.present?
      updated_tools << "Measurements" if child_member.health_records.present?
      updated_tools << "Milestones" if child_member.milestones.present?
    end
    not_updated_tools = tool_list - updated_tools
    [updated_tools.uniq,not_updated_tools.uniq]
  end
end