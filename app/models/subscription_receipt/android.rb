class SubscriptionReceipt::Android
  include Mongoid::Document
  include Mongoid::Timestamps
  # field :secret_key,     :type => String
  field :purchase_token,    :type => String
  field :original_purchase_date_ms,     :type => String 
  field :cancellation_date, :type=> String
  field :expires_date_ms,   :type => String 
  field :auto_renew_status,     :type => Boolean
  field :payment_state,     :type => String
  field :purchase_date_ms,  :type => String 
  field :order_id,          :type => String
  field :cancel_reason,     :type => String
  field :family_id,         :type => String
  field :is_trial_period, :type=>Boolean
  field :product_id,        :type => String 


  belongs_to :member
  belongs_to :subscription


   
  index({member_id:1})
  index({subscription_id:1})
  index({family_id:1})

  #receipt_data=>{"product_id"=>,"purchase_token"=>}
  def self.validate_receipt(receipt_data,options={})
    begin
      receipt_data = receipt_data.with_indifferent_access rescue receipt_data
      package = receipt_data["package"] || GlobalSettings::AndroidSubscriptionPakageName
      product_id = receipt_data["product_id"] # "test_plan_silver"
      purchase_token = receipt_data["purchase_token"] #"fodimgigfhdnhleggfpflilc.AO-J1OxUf-fnFVEuBBjwxIYfT2Lx762wGGyrsqLT4q1vU0uJzMZS-XKhz2hHJTo4xSHUK6tzWGFC7eOZ7IYBr2OanvBLcOCNnMMxroUYd492vrXrpInARaeDRAa0e2tbuNG5E-pKyPcH"
      status_code = StatusCode::Status200
      status = true
      config = CandyCheck::PlayStore::Config.new(
        application_name:  GlobalSettings::AndroidSubscriptionAppName,
        application_version: '1.0',
        issuer: APP_CONFIG["play_store_issuer"],
        key_file: "#{Rails.root}" + "/lib/certificate/#{APP_CONFIG['play_store_key_file']}",
        key_secret: 'notasecret',
        cache_file: 'tmp/candy_check_play_store_cache'
      )
      verifier = CandyCheck::PlayStore::Verifier.new(config)
      verifier.boot!
       
      latest_receipt = verifier.verify_subscription(package, product_id, purchase_token).attributes 
      # {"kind"=>"androidpublisher#subscriptionPurchase", "startTimeMillis"=>"1513797777654", "expiryTimeMillis"=>"1513884142521", "autoRenewing"=>false, "priceCurrencyCode"=>"USD", "priceAmountMicros"=>"990000", "countryCode"=>"US", "developerPayload"=>"", "cancelReason"=>0, "userCancellationTimeMillis"=>"1513798384540", "orderId"=>"GPA.3370-1710-4555-56872"}
     
      formated_response = 
      {:original_purchase_date_ms => latest_receipt["startTimeMillis"],
       :purchase_date_ms => latest_receipt["startTimeMillis"],
       :expires_date_ms =>latest_receipt["expiryTimeMillis"],
       :cancellation_date =>latest_receipt["userCancellationTimeMillis"],
       :auto_renew_status => latest_receipt["autoRenewing"],
       :order_id => latest_receipt["orderId"],
       :cancel_reason => latest_receipt["cancelReason"],
       :is_trial_period=> false,
       :purchase_token =>purchase_token,
       :product_id => product_id
     }
       
     if options[:original_transaction_id].present? && options[:original_transaction_id].to_s != formated_response[:order_id].to_s
        status = false
        status_code = StatusCode::Status100
      elsif formated_response[:expires_date_ms].blank? || formated_response[:expires_date_ms].milli_second_string_to_date < (Time.now.utc.to_date - 1.day)
        status_code = StatusCode::Status100
        status = false
      end
       
      if !SubscriptionReceipt::Android.valid_receipt_for_family?(purchase_token,options[:family_id])
        status_code = StatusCode::Status401
        formated_response = nil
        status = false 
      end
    rescue Exception=>e 
      status = false
      status_code = 100
      Rails.logger.info e.message
      formated_response = {}
    end
    [status,formated_response,status_code]
  end

  
  def self.valid_receipt_for_family?(purchase_token,family_id)
    return true if family_id.blank?
    family = Family.find(family_id) if family_id.present?
    android_receipt = SubscriptionReceipt::Android.where(purchase_token:purchase_token).last
    return true if android_receipt.blank?
    
    family_subscription = Subscription.find_family_subscription(family_id).last
    receipt_family_subscription = Subscription.where(family_id:android_receipt.family_id.to_s)
    if receipt_family_subscription.expired_subscription.last || receipt_family_subscription.free_subscription.last || family_subscription.free_subscription.last
     return true
    end

    if (family_subscription.blank? || android_receipt.family_id.to_s == family_id.to_s)
      status = true
    else
      status = false
    end
    status
  end


  def self.create_receipt( receipt_data,secret_key,member_id,subscription_id,options={})
    begin
      status,response,status_code = options[:response].blank? ? validate_receipt(receipt_data) : [true, options[:response],options[:status_code] ] 
     
      if status_code == StatusCode::Status200 && response.present?
        response  = response.with_indifferent_access
        attribute_data1 = response

        subscription_object = Subscription.where(id:subscription_id).last
        SubscriptionReceipt::Android.where(purchase_token:response["purchase_token"]).delete_all
        SubscriptionReceipt::Android.where(family_id:subscription_object.family_id).delete_all
        SubscriptionReceipt::Android.where(member_id:subscription_object.member_id).delete_all
        subscription_receipt_object = SubscriptionReceipt::Android.new(response)
        subscription_receipt_object.member_id = member_id
        subscription_receipt_object.subscription_id = subscription_id
        subscription_receipt_object.family_id = subscription_object.family_id
        subscription_receipt_object.save!
        status = true
        subscription_object.update_subscription_with_updated_receipt(response)
      else
        raise Message::Subscription[:not_valid_user_to_update] if status_code == StatusCode::Status401
        raise "Unable to create receipt due to purchase verification failure"
      end
      Rails.logger.info "............Log for Android verification..................."
      Rails.logger.info subscription_receipt_object.inspect
      Rails.logger.info "............End of Log for Android verification..................."
    rescue Exception=>e
      status = false
      subscription_receipt_object = nil
      Rails.logger.info e.message
    end
    [status,subscription_receipt_object]
  end

 
  def self.update_receipt(receipt_data,source=nil)
    begin
      email = GlobalSettings::SupportEMail
      receipt_data = receipt_data.with_indifferent_access rescue receipt_data
      purchase_token =  receipt_data["purchase_token"] rescue nil
      product_id = receipt_data["product_id"]
      subscription_receipt = SubscriptionReceipt::Android.where(purchase_token:purchase_token).last
      if source == "play_store"
        status = true
        status_code = 200
        latest_receipt = subscription_receipt
        formated_response = 
            {:original_purchase_date_ms => latest_receipt["original_purchase_date_ms"],
             :purchase_date_ms => latest_receipt["purchase_date_ms"],
             :expires_date_ms =>latest_receipt["expires_date_ms"],
             :cancellation_date =>latest_receipt["cancellation_date"],
             :auto_renew_status => latest_receipt["auto_renew_status"],
             :order_id => latest_receipt["order_id"],
             :cancel_reason => latest_receipt["cancel_reason"],
             :is_trial_period=> false,
             :purchase_token =>purchase_token,
             :product_id => product_id
           }
        if receipt_data["subscription_state"] == "renewed"
          added_time = product_id.include?("year") ? 1.year : 1.month
          formated_response["expires_date_ms"]  = (Date.today + added_time).to_time.to_millisecond.to_s
          formated_response["purchase_date_ms"]  = (Date.today).to_time.to_millisecond.to_s
        elsif receipt_data["subscription_state"] == "cancelled"
          formated_response["cancellation_date"] =(Date.today).to_time.to_millisecond.to_s
          formated_response["auto_renew_status"] = false
        end
      else
        status,formated_response,status_code = SubscriptionReceipt::Android.validate_receipt(receipt_data) 
      end
      if status && subscription_receipt.present? &&  formated_response.present?
        subscription_receipt.update_attributes(formated_response)
        subscription_receipt.save
        subscription_object = Subscription.where(id:subscription_receipt.subscription_id,platform:"android").last
        current_subscription = Subscription.where(id:subscription_receipt.subscription_id,platform:"android").last
        if subscription_object.present?
          if !(subscription_object.assigned_by == "admin" && subscription.expiry_date > Time.at(formated_response[:expires_date_ms].to_f/1000))
            subscription_object.update_subscription_with_updated_receipt(formated_response)
            subscription_object.touch
            # log subscription event
            if (current_subscription.subscription_valid_certificate != subscription_object.subscription_valid_certificate) || (current_subscription.expiry_date != subscription_object.expiry_date)
              Subscription.log_event_for_subscription(current_subscription,subscription_object)
            end
          end
        else
	        Rails.logger.info "PlayStore Subscription not found for receipt-#{Rails.env}"
          #ActionMailer::Base.mail(:from=>email,:to=>email,:subject=>"PlayStore Subscription not found for receipt-#{Rails.env}",:body=>"receipt-id:#{subscription_receipt.id}").deliver if source == "play_store"
        end
      else
        Rails.logger.info "PlayStore Subscription receipt not found"
          #ActionMailer::Base.mail(:from=>email,:to=>email,:subject=>"PlayStore Subscription receipt not found -#{Rails.env}",:body=>"receipt-:#{receipt_data}").deliver if source == "play_store"
      end
    rescue Exception=>e 
       Rails.logger.info e.message
       Rails.logger.info receipt_data.inspect
       Rails.logger.info "PlayStore Subscription receipt update issue:#{source}"  
       # ActionMailer::Base.mail(:from=>"Nurturey",:to=>email,:subject=>"PlayStore Subscription receipt update issue-#{Rails.env}",:body=>e.backtrace).deliver
    end
  end
 
end