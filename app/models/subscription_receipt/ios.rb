class SubscriptionReceipt::Ios
  include Mongoid::Document
  include Mongoid::Timestamps
  # field :secret_key,     :type => String
  field :receipt,        :type => String
  field :receipt_type,     :type => String
  field :adam_id,     :type => String
  field :app_item_id,     :type => String
  field :bundle_id,     :type => String
  field :application_version,     :type => String
  field :download_id,     :type => String
  field :version_external_identifier,     :type => String
  field :receipt_creation_date_ms,     :type => String
  field :original_purchase_date_ms,     :type => String
  field :original_application_version,     :type => String
  field :request_date_ms, :type => String
  field :quantity,     :type => String
  field :expires_date_ms, :type=>String 
  field :product_id,     :type => String 
  field :transaction_id,     :type => String
  field :original_transaction_id,     :type => String
  field :purchase_date_ms,     :type => String 
  field :is_trial_period, :type=>Boolean
  field :buyer_name, :type=>String  
  field :web_order_line_item_id, :type=>String 
  field :family_id, :type=>String
  belongs_to :member
  belongs_to :subscription


  index({original_transaction_id:1})
  index({transaction_id:1})
  index({member_id:1})
  index({subscription_id:1})
  index({family_id:1})
 
  AttributeData = ["receipt_type","adam_id","app_item_id", "bundle_id","download_id", "application_version",  "version_external_identifier", 
    "receipt_creation_date_ms", "request_date_ms" , 
  "original_application_version"  ]
  
  InAppData = ['web_order_line_item_id',"original_purchase_date_ms" , 'is_trial_period', 'expires_date_ms', 'quantity','product_id','transaction_id','original_transaction_id','purchase_date_ms']
  

  # SubscriptionReceipt::Ios.validate_receipt
  def self.validate_receipt(receipt_data,secret_key=nil,options={})
    begin
      status = true
      status_code = 200
      config = CandyCheck::AppStore::Config.new(
      environment: APP_CONFIG["subscription_mode"]
        )
      verifier = CandyCheck::AppStore::Verifier.new(config)
      secret_key = secret_key || APP_CONFIG["secrect_key_for_ios_subscription"]

      receipt_data =  receipt_data.gsub(" ","+") #to fix url encode issue
      response = verifier.verify(receipt_data, secret_key)
      if response.attributes["in_app"].present?
        latest_receipt = response.attributes["in_app"].sort_by{|data| data["purchase_date_ms"].to_i}.last rescue nil
      else
        latest_receipt = response.attributes
      end
      formated_response = {}
      if latest_receipt.present?
         SubscriptionReceipt::Ios::AttributeData.each do |key|
         formated_response.merge!({key=>response.attributes[key]})
        end
         SubscriptionReceipt::Ios::InAppData.each do |key| 
          formated_response.merge!({key=>latest_receipt[key]})
        end
      end
      formated_response["expires_date_ms"] = latest_receipt["expires_date"] if formated_response["expires_date_ms"].blank?
      pending_renewal_status = latest_receipt["auto_renew_status"].present? ? latest_receipt["auto_renew_status"] == "true" : true
      if response.attributes['pending_renewal_info'].present?
        response.attributes['pending_renewal_info'].each do |pending_renwal_info|
          if pending_renwal_info["auto_renew_product_id"] == formated_response["product_id"]
            pending_renewal_status = pending_renwal_info["auto_renew_status"] == "1"
          end
        end
      end
      formated_response.merge!({"auto_renew_status"=>pending_renewal_status})
      
      
      if options[:original_transaction_id].present? && options[:original_transaction_id].to_s != formated_response["original_transaction_id"].to_s
        status = false
        status_code = StatusCode::Status401
      elsif formated_response["expires_date_ms"].blank? || Time.at(formated_response["expires_date_ms"].to_f/1000).to_date < (Time.now.in_time_zone.to_date - 1.day)
        status_code = StatusCode::Status100
        status = false
      end
      if !SubscriptionReceipt::Ios.valid_receipt_for_family?(formated_response["original_transaction_id"].to_s,options[:family_id])
        status_code = StatusCode::Status401
        formated_response = nil
        status = false 
      end
      Rails.logger.info "...............Special logging........................"
      Rails.logger.info formated_response.inspect
      Rails.logger.info "...............End Special logging........................"
      [status,formated_response,status_code ]
    rescue Exception=>e
      Rails.logger.info "Error in verification subscription:#{e.message}"
      status_code = StatusCode::Status100
      [false,nil,status_code]
    end
  end
  
  def self.valid_receipt_for_family?(original_transaction_id,family_id)
    return true if family_id.blank?
    family = Family.find(family_id) if family_id.present?
    
    ios_receipt = SubscriptionReceipt::Ios.where(original_transaction_id:original_transaction_id).last
    return true if ios_receipt.blank?

    family_subscription = Subscription.find_family_subscription(family_id)
    receipt_family_subscription = Subscription.where(family_id:ios_receipt.family_id.to_s)
    if receipt_family_subscription.expired_subscription.last || receipt_family_subscription.free_subscription.last || family_subscription.free_subscription.last
     return true
    end
    if (family_subscription.blank? || ios_receipt.family_id.to_s == family_id.to_s)
      status = true
    else
      status = false
    end
    status
  end

  def self.create_receipt(receipt_data,secret_key ,member_id,subscription_id,options={})
    begin

      status,response,status_code = options[:response].blank? ? SubscriptionReceipt::Ios.validate_receipt(receipt_data, secret_key) : [true, options[:response],options[:status_code]] 
      if status_code == StatusCode::Status200 && response.present? 
        subscription_object = Subscription.where(id:subscription_id).last
        attribute_data1 = response#.attributes
        subscription_receipt_object = SubscriptionReceipt::Ios.new(response)
        subscription_receipt_object["receipt"] = receipt_data
        subscription_receipt_object["buyer_name"] = options[:buyer_name] if options[:buyer_name].present?
        subscription_receipt_object.member_id = member_id
        subscription_receipt_object.subscription_id = subscription_id
        subscription_receipt_object.family_id = subscription_object.family_id
        SubscriptionReceipt::Ios.where(original_transaction_id:response["original_transaction_id"]).delete_all
        SubscriptionReceipt::Ios.where(family_id:subscription_object.family_id).delete_all
       
        # subscription_receipt_object.renew_count = (subscription_receipt_object.renew_count + 1) rescue 0
        subscription_receipt_object.save!

        status = true
        subscription_object.update_subscription_with_updated_receipt(response)
      else
        raise response.message 
      end
    rescue Exception=>e
      status = false
      subscription_receipt_object = nil
      Rails.logger.info e.message
    end
    [status,subscription_receipt_object]
  end

#  {"latest_receipt"=>"mckaqapqzx123sfjnbt","latest_receipt_info"=>
#{"original_purchase_date_pst"=>"2017-12-20 20:09:40 America/Los_Angeles", "is_in_intro_offer_period"=>"false", "purchase_date_ms"=>"1513842263000", "unique_identifier"=>"7a338ad05694d2554e3ab34b7682e2f682a25d1c", "original_transaction_id"=>"1000000361523278", "expires_date"=>"1513842563000", "transaction_id"=>"1000000361572803", "quantity"=>"1", "web_order_line_item_id"=>"1000000037290677", "original_purchase_date_ms"=>"1513829380000", "unique_vendor_identifier"=>"008CD4FF-811A-47B1-A145-CAC7C547BD9E", "expires_date_formatted_pst"=>"2017-12-20 23:49:23 America/Los_Angeles", "item_id"=>"1322883999", "expires_date_formatted"=>"2017-12-21 07:49:23 Etc/GMT", "original_purchase_date"=>"2017-12-21 04:09:40 Etc/GMT", "product_id"=>"com.app.nurturey.dummy", "purchase_date"=>"2017-12-21 07:44:23 Etc/GMT", "is_trial_period"=>"false", "purchase_date_pst"=>"2017-12-20 23:44:23 America/Los_Angeles", "bid"=>"com.app.nurturey", "bvrs"=>"1.1600.0"}
#, "environment"=>"Sandbox", "auto_renew_status"=>"true", "password"=>"2f72c4c634314e8e9df6d00358557ab3", "auto_renew_product_id"=>"com.app.nurturey.dummy", "notification_type"=>"INTERACTIVE_RENEWAL", "action"=>"update_subscription_from_apple_server", "controller"=>"api/v4/subscription", "format"=>"json", "subscription"=>{}}
  def self.update_receipt(receipt_data,latest_receipt_info={},source=nil,options={})
    begin
      email_to = GlobalSettings::SupportEMail
      email_from = GlobalSettings::SupportEMail
      receipt_data =  receipt_data.gsub(" ","+") rescue nil
      status,formated_response,status_code = SubscriptionReceipt::Ios.validate_receipt(receipt_data,nil,{})
      raise "Invalid receipt- blank response" if formated_response.blank?
      if source == "apple_server"
        subscription_receipt = SubscriptionReceipt::Ios.where(original_transaction_id:latest_receipt_info["original_transaction_id"]).last
      else
        subscription_receipt = SubscriptionReceipt::Ios.where(original_transaction_id:formated_response["original_transaction_id"]).last
        if subscription_receipt.blank? && options[:member_id].present?
          member_id = options[:member_id]
          Rails.logger.info "....Receipt searching by member.................."
          Rails.logger.info options.inspect
          Rails.logger.info "........................................."
          subscription_receipt = SubscriptionReceipt::Ios.where(member_id:member_id).last
        end
      end
      raise "Invalid receipt- unauthorized" if status_code == StatusCode::Status401
      status = StatusCode::Status200
     
      formated_response["expires_date_ms"] = formated_response["expires_date_ms"] || latest_receipt_info["expires_date"]
      if subscription_receipt.present?
        # ActionMailer::Base.mail(:from=>email_from,:to=>email_to,:subject=>"Ios Subscription not found for receipt",:body=>"receipt-id:#{subscription_receipt.id}").deliver if source == "apple_server"
        subscription_receipt.update_attributes(formated_response)

        subscription_object = Subscription.where(id:subscription_receipt.subscription_id,platform:"ios").last
        
        if subscription_object.present?
          # Update subscription with latest info
          subscription_manager =  SubscriptionManager.where(ios_product_id:subscription_receipt.product_id).last
          current_user = Member.find(subscription_object.member_id)
          family_id = subscription_object.family_id
          params = {}
          params[:receipt_end_date] = Time.at(formated_response["expires_date_ms"].to_f/1000).to_date  
          params[:receipt_response] = formated_response
          subscription_object.update_subscription(subscription_manager,api_version=4,current_user,family_id,"ios",params)
          if source != "apple_server"
            subscription_receipt.receipt = receipt_data
            subscription_receipt.save
          end
          # subscription_object.update_subscription_with_updated_receipt(formated_response)
        else
          status = StatusCode::Status100
          msg = "Ios Subscription not found for receipt"
          Rails.logger.info msg
          UserMailer.delay.notify_dev_for_error(msg,nil,receipt_data,"ios update_receipt")

          # ActionMailer::Base.mail(:from=>email_from,:to=>email_to,:subject=>"Ios Subscription not found for receipt-#{Rails.env}",:body=>"receipt-id:#{subscription_receipt.id}").deliver if source == "apple_server"
        end
      else
          status = StatusCode::Status100
          msg = "Ios Subscription receipt not found -"
          Rails.logger.info msg
          UserMailer.delay.notify_dev_for_error(msg,nil,receipt_data,"ios update_receipt")

           
          # ActionMailer::Base.mail(:from=>email_from,:to=>email_to,:subject=>"Ios Subscription receipt not found -#{Rails.env}",:body=>"receipt-:#{receipt_data}").deliver if source == "apple_server"
      end
    rescue Exception=>e 
        Rails.logger.info e.message
        Rails.logger.info e.backtrace
        Rails.logger.info receipt_data.inspect
        Rails.logger.info "Subscription receipt update issue-#{source}"
        UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,receipt_data,"ios update_receipt")

        status = StatusCode::Status100
       # ActionMailer::Base.mail(:from=>email_from,:to=>email_to,:subject=>"Subscription receipt update issue-#{Rails.env}",:body=>e.backtrace).deliver
    end
     return status
  end

end

=begin
{"receipt_type"=>"ProductionSandbox", "adam_id"=>0, 
  "app_item_id"=>0, "bundle_id"=>"com.nurturey.dev", 
  "application_version"=>"1.500.DEBUG", "download_id"=>0, 
  "version_external_identifier"=>0, 
  "receipt_creation_date"=>"2016-12-21 10:42:17 Etc/GMT",
  "request_date_ms"=>"1482328292298", 
   
    
    "original_purchase_date_ms"=>"1375340400000",
 
 "original_application_version"=>"1.0", "in_app"=>[
{"quantity"=>"1", "product_id"=>"com.devApp.yearly",
 "transaction_id"=>"1000000257974188",
 "original_transaction_id"=>"1000000257646392",
  "purchase_date"=>"2016-12-12 09:25:17 Etc/GMT", 
  "purchase_date_ms"=>"1481534717000", 
  "purchase_date_pst"=>"2016-12-12 01:25:17 America/Los_Angeles",
   "original_purchase_date"=>"2016-12-12 09:25:18 Etc/GMT", 
   "original_purchase_date_ms"=>"1481534718000", 
   "original_purchase_date_pst"=>"2016-12-12 01:25:18 America/Los_Angeles", 
   "expires_date"=>"2016-12-12 10:25:17 Etc/GMT", 
   "expires_date_ms"=>"1481538317000",
    "expires_date_pst"=>"2016-12-12 02:25:17 America/Los_Angeles"
    , "web_order_line_item_id"=>"1000000033893481", "is_trial_period"=>"false"}
=end
 