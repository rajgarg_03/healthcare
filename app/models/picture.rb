class Picture
  include Mongoid::Document
  include Mongoid::Paperclip
  TYPE={:thumb=>"thumb", :medium=>"medium", :full=>"full"}
  field :name, type: String
  field :status, type: String
  field :order_id, type: Integer  
  field :upload_class, :type => String, :default => "pic"
  field :processing, :type => Boolean
  field :member_id, :type=> String

  belongs_to :imageable, :polymorphic=>true

index ({member_id:1})
index ({imageable_id:1})
index ({imageable_type:1})

  has_mongoid_attached_file :local_image,
  :styles => lambda{ |f|
    ['image/jpeg', 'image/jpg','image/png', 'image/gif'].include?(f.content_type) ? f.instance.get_style_type : {}
  },
  :path => ":rails_root/public/assets/pictures/:id/:style/:basename.:extension",
  :url  => "/assets/pictures/:id/:style/:basename.:extension"#, :processors => [:cropper]

  def get_style_type
    if self.imageable_type == "FacePlus"
      {:large => "1096x1096>" }
    elsif self.imageable_type == "Child::SkinAssessment"
      {:large => "1096x1096>",:small => "<115x115>" }
    else
    {
      :thumb => "40x40>",
      :small => "115x115>", 
      :medium => '300x300>',
      :large => "500x500>" }
    end
  end
  validates_attachment_content_type :local_image, :content_type => ["image/jpg", "image/jpeg","image/JPG", "image/JPEG" ,"image/png","image/PNG", "image/gif","image/GIF"]


  has_mongoid_attached_file :photo,
  :styles => lambda{ |f|
    ['image/jpeg', 'image/jpg','image/png', 'image/gif'].include?(f.content_type) ? f.instance.get_style_type : {}
  },

  # avata 115 x 115
  #       40 X 40
  :storage => :s3,
  :s3_credentials => {
    :bucket => APP_CONFIG["s3_bucket"],
    :access_key_id => APP_CONFIG["s3_access_key_id"],
    :secret_access_key => APP_CONFIG["s3_secret_access_key"]
  },

  :url => ':s3_domain_url',
  :path => lambda{ |f| '/:class/' + "#{f.instance.upload_class}" + '/:id_partition/:style/:filename'},
  :s3_protocol => 'https',
  :default_url => "/images/:style/missing.png"
  #,:convert_options => { :all => '-background white -flatten +matte' }, :processors => [:cropper]

  validates_attachment_presence :local_image

  attr_accessor :crop_x, :crop_y, :crop_w, :crop_h
  # after_update :reprocess_image, :if => :cropping?

  after_save :queue_upload_to_s3

  # cancel post-processing now, and set flag...
  before_photo_post_process do |image|
    if image.local_image? && image.local_image_updated_at_changed?
      self.processing = true
      false # halts processing
    end
  end

  def self.binary_image_to_file(data,member,name="cover_image")
    content = data
    content.gsub!('\\r', "\r")
    content.gsub!('\\n', "\n")
    decode_base64_content = Base64.decode64(content)
    file_name =  "public/pictures/#{member.first_name.downcase}_#{name}.jpg"
    File.open(file_name, "wb") do |f|
      f.write(decode_base64_content)
    end
    [File.open(file_name),file_name]
  end

  def queue_upload_to_s3
    add_to_queue_flag = true 
    if upload_class == "face_plus"
      add_to_queue_flag = false
    elsif upload_class == "child_skin_assessment"
      add_to_queue_flag = false
    else
      add_to_queue_flag = true
    end
    Delayed::Job.enqueue(ImageJob.new(id, "Picture"), :priority => 3, :run_at => 1.seconds.from_now) if local_image? && local_image_updated_at_changed? && add_to_queue_flag
  end


  def upload_to_s3
    puts "In uploading to s3"
    self.photo = File.open(self.local_image.path, "r")
    save!
  end

# generate styles (downloads original first)
  def regenerate_styles!
    begin 
      puts "Processing images"
      self.photo.reprocess! 
      self.processing = false
      self.save!
    rescue Exception => e
      puts e.message  
    end
    
  end

  def cropping?
    !crop_x.blank? && !crop_y.blank? && !crop_w.blank? && !crop_h.blank?
  end
  
  def image_geometry(style = :original)
    @geometry ||= {}
    @geometry[style] ||= Paperclip::Geometry.from_file(photo.path(style))
  end

  def image_path(type)
    path = self.local_image.url(type)
    begin
      #path = self.photo.url(type)
      path = path.from(0).to(path.rindex('?')-1)
    rescue Exception => e
      puts e.message
    end
    return path
  end

  def photo_file_name_str
    if self.photo.url == "/images/original/missing.png"
      return self.local_image_file_name
    else
      return self.photo_file_name
    end
  end

  def photo_url_str(size = :original,api_version=1)    
    # if self.photo.url == "/images/original/missing.png"
    if (self.photo.url == "/images/original/missing.png") || FileTest.exist?(self.local_image.path)
      url = (APP_CONFIG['default_host']+self.local_image.url(size))
      if api_version >= 6
        url = ShaAlgo.encryption(url)
      end
      return url 
    else
      # return self.photo.url size
      return aws_url(size,api_version)      
    end
  end

  def photo_path_str(size = :original,api_version=1)    
    if (self.photo.url == "/images/original/missing.png") || FileTest.exist?(self.local_image.path)
      url =  self.local_image.path size
      if api_version >= 6
        url = ShaAlgo.encryption(url)
      end
    else
      # return self.photo.url size
      url =  aws_url(size,api_version)
    end
    url
  end

  def fbshare_image_url(size = :original,api_version=1)    
    if (self.photo.url == "/images/original/missing.png") || FileTest.exist?(self.local_image.path)
      url =  (APP_CONFIG['default_host']+self.local_image.url(size))
    else
      url =  self.get_photo_url(size,api_version)
    end
    if api_version >= 6
      url = ShaAlgo.encryption(url)
    end
    url
  end

  def complete_image_url(size = :original,api_version=1)    
    if (self.photo.url == "/images/original/missing.png") || FileTest.exist?(self.local_image.path)
      url =  (APP_CONFIG['default_host']+self.local_image.url(size))
      if api_version >= 6
        url = ShaAlgo.encryption(url)
      end
      url
    else
      # return self.photo.url size
      url =  aws_url(size,api_version)
    end
    url
  end

  def expected_s3_url(size=:original,api_version=1)
    url = 'https://' + APP_CONFIG['s3_bucket'] + '.s3.amazonaws.com/pictures/' + upload_class + '/' + id.to_s.scan(/.{4}/).join("/") + '/' + size.to_s + '/' + local_image_file_name
    if api_version >= 6
      url = ShaAlgo.encryption(url)
    end
    url
  rescue
   ''
  end

  def aws_url(size=:original,api_version=1)
    if (photo.url == "/images/original/missing.png")
      url = expected_s3_url(size,api_version)
    else
      url = get_photo_url(size,api_version)
      if api_version >= 6
        url = ShaAlgo.encryption(url)
      end
    end
    url
  end

  def get_photo_url(size=:original,version=1)
    # get_sts_session(size,version)
    photo = self.photo
    photo.url(size)
  end

  def get_sts_session(size=:original,api_version=1)
    photo = self.photo
    s3_object = photo.path(size).sub(/^\/+/, "")
    # sts = AWS::STS.new(:access_key_id =>APP_CONFIG["s3_access_key_id"],
    #                :secret_access_key => APP_CONFIG["s3_secret_access_key"])
    # session = sts.new_session(:duration => GlobalSettings::StsSessionExpireDuration)
    # s3 = AWS::S3.new(session.credentials)
    # bucket = s3.buckets[APP_CONFIG['s3_bucket']]
    object = AwsSts.instance.get_object(s3_object)
    url = object.url_for(:get).to_s
    url
  end  

  # private  
  def reprocess_image
    local_image.reprocess!
  end

end
