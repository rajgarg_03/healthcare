class SystemTimeline
  include Mongoid::Document
  field :name, type: String
  field :desc, type: String
  field :duration, type: String
  field :comment, type: String
  field :default_image_exists, :type => Boolean
  field :sequence, type: Integer
  field :category, :type=> String, :default=>"child"
  field :notification_message_for_instantly_push, type:String #{line1=>"text" ,line2=>pic, line3=>"text"}
  field :notification_message_for_schedule_push, type:String #{line1=>"text" ,line2=>pic, line3=>"text"}
  field :data_source, type: String, :default=>"nurturey"
  attr_accessor :timeline_at
  has_many :timelines, :as=> :timelined
  has_many :pictures, :as=> :imageable

  scope :for_category, ->(category) { where(:category => category) }
  index({id:1})
  index({category:1})
  def self.get_child_system_timlines(child,category="child")
    begin
      data = []
 
      time_in_zone = Time.zone.now.to_date
      if category == "pregnancy"
        system_timelines = SystemTimeline.where(:category => category).order_by(sequence: 'desc').only(:name,:id,:est_date,:duration)
        system_timelines.each do |sys_timeline|
          preg_date = child.birth_date - 40.weeks + eval(sys_timeline.duration)
         data << sys_timeline.attributes.merge({:est_date=>(preg_date).to_date4}) if preg_date <= time_in_zone
        end
      else
        child_age_duration = child.age_in_months > 120 ? "10.years" : child.age_duration
        system_timeline_sequence = SystemTimeline.where(:category => "child").where(duration: child_age_duration).first.sequence rescue 1
        system_timelines = SystemTimeline.where(:sequence.lte => system_timeline_sequence, :category => "child", :id.nin => child.timelines.where(system_entry: "true").pluck(:timelined_id)).order_by(sequence: 'desc').only(:name,:id,:est_date,:duration)
        system_timelines.each do |sys_timeline|
          est_time = (child.birth_date + eval(sys_timeline.duration))
          data << sys_timeline.attributes.merge({:est_date=>est_time.to_date4}) if est_time <= time_in_zone
        end
      end
    rescue Exception=> e
      data = []
    end
    data
  end

  def self.find_by_age(age_in_month)
   maping_name_with_age =  {1=>"First month birthday", 2=>"Second month birthday",3=> "Third month birthday",4=> "Fourth month birthday",5=> "Fifth month birthday", 6=>"Sixth month birthday", 7=>"Seventh month birthday",8=> "Eighth month birthday",9=> "Ninth month birthday",10=> "Tenth month birthday",11=> "Eleventh month birthday", 12=>"First birthday",24=> "Second birthday",36=> "Third birthday", 48=>"Fourth birthday",60=> "Fifth birthday",
      72=> "Sixth birthday", 84=>"Seventh birthday", 96=>"Eighth birthday",108=> "Ninth birthday",120=> "Tenth birthday"}
    SystemTimeline.where(category:"child",name:maping_name_with_age[age_in_month]).last
  end

  def get_birthday_image(type="small")
    begin
      system_entry = self.name
      if ["Fifth month birthday","Sixth month birthday", "Seventh month birthday", "Eighth month birthday", "Ninth month birthday", "Tenth month birthday"].include?(system_entry)
        system_entry = "Fifth month birthday" # default image is fifth birthday's image for 6th to 10th birthday 
      end
      "#{APP_CONFIG["s3_base_url"]}/default/timelines/#{type}/#{system_entry.gsub(' ','_').downcase}.jpg" #rescue ""
    rescue Exception=>e 
       nil
    end
  end

  def self.seed_notification_text_from_sheet
    q1 = Roo::Excelx.new(Rails.root.to_path + "/lib/data/birthday_wish.xlsx")
    q1.default_sheet = q1.sheets[0]
    q1.entries[2..-1].each do |data|
      system_timeline = SystemTimeline.where(name:data[0],category:"child").first
      notification_text = {}
      notification_text[:line1] =  data[3].gsub('\\\\','\\') rescue data[3]
      notification_text[:line2] =  data[4].gsub('\\\\','\\') rescue data[4] 
      notification_text[:line3] =  data[5].gsub('\\\\','\\') rescue data[5] 
      if system_timeline.present?
        system_timeline.notification_message_for_instantly_push = data[3].gsub('\\\\','\\') rescue data[3]
        system_timeline.notification_message_for_schedule_push = data[5].gsub('\\\\','\\') rescue data[3]
        
        system_timeline.save
      else
        Rails.logger.info "Not found #{data[0]} in system"
      end
       
    end
  end

  # Used in handhold  
  def self.save_system_timeline(current_user,member,system_timeline_ids,options={})
    child_member = member
    category = options[:category].present? ? options[:category] : "child"
    system_timelines = SystemTimeline.for_category(category).in(:id => system_timeline_ids).order_by(sequence: 'asc').entries
    if category.downcase == "pregnancy"
      entry_from = "preg"
      member_birth_date = child_member.birth_date - 40.weeks
      timelined_type = "Pregnancy"
      system_timelines.each do |system_timeline|
        timelined_at = member_birth_date  + eval(system_timeline.duration)
        timeline = Timeline.create({timelined_id:system_timeline.id, timeline_at:timelined_at ,name:system_timeline.name}.merge({system_entry: "true", member_id: child_member.id,added_by: current_user.id,entry_from: entry_from, timelined_type: timelined_type}))
      end
      #Add future timelines
      all_system_timelines = SystemTimeline.where(:category => category).not_in(:id => system_timeline_ids).order_by(sequence: 'asc').entries
      all_system_timelines.each do |system_timeline|
        timelined_at = member_birth_date + eval(system_timeline.duration)
        if timelined_at > Date.today
          timeline = Timeline.create({timelined_id:system_timeline.id, timeline_at:timelined_at ,name:system_timeline.name}.merge({system_entry: "true", member_id: child_member.id,added_by: current_user.id,entry_from: entry_from, timelined_type: timelined_type}))
        end
      end
    elsif category.downcase == "child"
      member_birth_date = child_member.birth_date
      child_age_duration = member.age_in_months > 120 ? "10.years" : member.age_duration
      system_timeline = SystemTimeline.where(duration: child_age_duration).first
      future_system_timeline_ids = SystemTimeline.where(:sequence.gt => system_timeline.sequence).order_by(sequence: 'asc').map(&:id) rescue []
      all_system_timeline_ids = (system_timeline_ids + future_system_timeline_ids).uniq
      Timeline.save_system_timeline_to_member(member.id,current_user.id,all_system_timeline_ids)
    end
  end

# Used for admin panel
   def update_record(params)
    begin
      system_timeline = self
      timeline_params = params["SystemTimeline".underscore]
      timeline_params[:duration] = params[:duration_day] + "." + params[:frequency]
      if system_timeline.update_attributes(timeline_params)
        status = true
      else
        status = false
      end
    rescue Exception => e
      Rails.logger.info e.message
      status = false 
    end
    [system_timeline,status]
  end


  def self.create_record(params)
    begin
      timeline_params = params["SystemTimeline".underscore]
      timeline_params[:duration] = params[:duration_day] + "." + params[:frequency]
      system_timeline = self.new(timeline_params)
      system_timeline.save!
      status = true
    rescue Exception => e
      Rails.logger.info e.message
      status = false 
    end
    [system_timeline,status]
  end

  def self.duplicate_record(params)
    record_to_be_duplicated = SystemTimeline.find(params[:record_id]).attributes
    record_to_be_duplicated.delete("_id")
    record_to_be_duplicated.delete("created_at")
    record = SystemTimeline.new(record_to_be_duplicated)
    record.save
    [record,nil]
  end

  def self.delete_record(record_id)
    begin
      system_timeline = SystemTimeline.find(record_id)
      system_timeline.delete
    rescue Exception => e
      Rails.logger.info e.message
    end
  end
  
end