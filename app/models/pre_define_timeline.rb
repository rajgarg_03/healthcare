class PreDefineTimeline
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :name, type: String
  field :api_version_supported, type: Integer
  field :sequence, type: Integer
  field :timeline_for, type:String # "child", "pregnancy"
  
  # data = {:pregnancy=>["First checkup","pregnancy timeline2" ],:child=>["First day at school","Made his/her friend in school","Loves shopping"]}
  def self.get_predefine_timeline(timeline_for=nil, current_user=nil, api_version_supported=2)
    data = {"pregnancy"=>[],"child"=>[]}
    timelines = where(:api_version_supported.lte => api_version_supported)
    timelines.each do |timeline|
     data[timeline.timeline_for.to_s] <<  timeline.name
    end
    data
  end
end


  