class Devices::HomeDevice
  include Mongoid::Document
  include Mongoid::Timestamps

  field :member_id, type: String
  field :platform, type: String
  validates_presence_of :member_id
  validates_presence_of :platform

  index({member_id:1})
  
  def self.add_device(platform,member_id,options={})
    begin
      params = {:platform=>platform,:member_id=>member_id}.merge(options)
      home_device = Devices::HomeDevice.where(:platform=>platform,:member_id=>member_id).last
      if home_device.blank?
        Devices::HomeDevice.new(:platform=>platform,:member_id=>member_id).save
      end
    rescue Exception=>e
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,params,"Error Devices::HomeDevice.create")
    end
  end
end