class SysMedicalEvent 
  include Mongoid::Document
  include Mongoid::Timestamps
  field :name, type: String
  field :desc, type: String
  field :country, type: String, default: "uk"
  field :member_id, type: String
  field :recommended, type: String 
  field :category, type: String, default: "Blood Test"
  field :expected_on, type: String, default: "0.weeks"
  field :category_title, type: String
  field :data_source, type: String, default:"nurturey"

  validates :name, presence: true
  index ({id:1})
  index ({country:1})
  index({category_title:1})
  MedicalTime ={"6.weeks"=> "6 weeks", "12.weeks"=>"12 weeks"} 
  
  def self.recommended_test_name(member=nil)
      country_code = User.where(member_id:member.family_mother_father.first.to_s).first.country_code rescue "other"
    # {:name=>["Blood Test", "Ultrasound Scan Test","Urine Test"]}
    recommended_test_with_component(nil,member,country_code)
  end
  
  # not in used now
  def self.medical_event_list#(member_id)
    data = []
    events = SysMedicalEvent.where(recommended:"true",member_id:nil)#.group_by {|a| a.category}
    events.each do |event|
     data << {name:"#{event.name} in #{MedicalTime[event.expected_on]}",Sys_Medi_Event_id:event.id
    }

    end
    data
  end

  # Used in Handhold
  def self.save_system_prenatal_test(member,system_prenatal_test_name_list,options={})
    country_code = member.get_country_code
    last_medical_event_index =  member.medical_events.order_by("event_index asc" ).last.event_index rescue 0
    system_prenatal_test_data = SysMedicalEvent.recommended_test_with_component(system_prenatal_test_name_list,member,country_code)
    system_prenatal_test_data.each do |system_prenatal_test|
      last_medical_event_index = system_prenatal_test[:name].strip[-1].to_i rescue 0
      start_time = nil 
      end_time = nil 
      prenatal_test = MedicalEvent.new(event_index: last_medical_event_index, end_time: end_time,start_time: start_time, fullday: true, est_due_time: system_prenatal_test[:est_due_date], member_id: member.id, opted: true, name:system_prenatal_test[:name],category:system_prenatal_test[:category],status: "Estimated",component:system_prenatal_test[:component])
      prenatal_test.save!
    end
  end


  def self.recommended_test_with_component(name=nil,member=nil,country_code="other")
    country_code = Member.member_country_code(country_code)
    country_code = "uk"
    category_titles = name.present? ? name : SysMedicalEvent.distinct("category_title")
    data = []

    # events = SysMedicalEvent.where(recommended:"true",member_id:nil,:category_title.in=> category_titles).group_by {|a| a.category}
    events = SysMedicalEvent.where(recommended:"true",member_id:nil,:category_title.in=> category_titles,:country=>/country_code$/i).group_by {|a| a.category}
    sanitize_country_code = Member.sanitize_country_code(country_code)
    events = SysMedicalEvent.where(recommended:"true",member_id:nil,:category_title.in=> category_titles).where(:country.in =>sanitize_country_code).group_by {|a| a.category}
    if member.blank?
      events.each do |k,v|
        if (events[k][1].recommended rescue false)
          event_expected_on = (SysMedicalEventTime.where(:country_code.in =>sanitize_country_code,category_title:events[k][0].category_title).last.test_expected_on || "0.weeks") rescue "0.weeks"
        else
          event_expected_on = "0.weeks"
        end
       data << {category:k, name:events[k][0].category_title, component:events[k].map(&:name).sort_by{|i| i.upcase},est_due_date: event_expected_on}
      end
    else
      events.each do |k,v|
        if (events[k][1].recommended rescue false)
          event_expected_on = (SysMedicalEventTime.where(:country_code.in=>sanitize_country_code,category_title:events[k][0].category_title).last.test_expected_on) rescue "0.weeks"
        else
          event_expected_on = "0.weeks"
        end
       data << {category:k, name:events[k][0].category_title,component:events[k].map(&:name).sort_by{|i| i.upcase},est_due_date: (member.birth_date - 40.weeks +  eval(event_expected_on) ).to_date.to_date2}
       data = data.sort_by{ |a|  a[:est_due_date].to_date}
      end
    end
    data
  end
end