class Report::AlexaRequestData
  include Mongoid::Document
  
  field :email ,type:String
  field :request_param ,type:String
  field :raw_request_param ,type:String
  field :response ,type:String
   
end