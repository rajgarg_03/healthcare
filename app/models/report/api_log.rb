class Report::ApiLog
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :action           ,  type:String
  field :controller       ,  type:String
  field :user_token       ,  type:String
  field :user_email       ,  type:String
  field :user_id          ,  type:String
  field :request_params   ,  type:Hash
  field :member_id        ,  type:String
  field :request_type     ,  type:String # event/json/html
  field :api_version      ,  type:String
  field :request_format   ,  type:String
  field :ip_address       ,  type:String
  field :location         ,  type:String

  field :event_source     ,  type:String  #  eg api,batch_job
  field :event_custom_id  ,  type:Integer #  eg 1,2,3
  field :family_id        ,  type:String  #  eg 1,2,3
  field :failed_attempt   ,  type:Integer  #  eg 1,2,3
  field :request_status   ,  type:String  #  eg "success/failed"
  field :user_country_code     ,  type:String  #  eg "UK/India"
  field :member_country_code     ,  type:String  #  eg "UK/India"
  field :family_country_code     ,  type:String  #  eg "UK/India"
  
  index ({event_custom_id: 1})
  index ({family_id: 1})
  index ({member_id: 1})
  index ({user_id: 1})
  index ({user_country_code: 1})
  index ({member_country_code: 1})
  index ({family_country_code: 1})

  DATATABLE_COLUMNS = [:family_id,:event_custom_id,:event_source, :created_at,:ip_address, :location, :user_email, :action,  :controller, :request_type, :request_format, :request_params]
  
  def self.datatable_filter(search_value, search_columns)
    return where(nil) if search_value.length < 4
    result = self
    filter = []
    search_keys = search_columns.values.map{|a| a["data"]} & ["ip_address", "location", "family_id", "action", "controller", "request_type", "request_status", "failed_attempt"]
    search_keys.each do |field_key|
      filter << {"#{field_key}"=>/#{Regexp.escape(search_value)}/i} #if value['searchable']
    end
     
    # search_columns.each do |key, value|
    #   if value['data'] != 'created_at'
    #     if value['data'] == "event_custom_id"
    #       filter << {"#{value['data']}"=>search_value} if value['searchable']
    #     elsif value['data'].present?
    #       filter << {"#{value['data']}"=>/#{search_value}/i} if value['searchable']
    #     end
    #   end
    # end
    where("$or"=>filter) 
  end  


  def self.datatable_order(order_column_index, order_dir)
    order_by("#{System::Scenario::ScenarioDetails::DATATABLE_COLUMNS[order_column_index]} #{order_dir}")
  end
  
  def self.set_db
    source_database = "api_logs"
    destination_database = APP_CONFIG["database_name"]
    Mongoid.override_database(source_database)
  end
  
  def self.reset_db
    destination_database = APP_CONFIG["database_name"]
    Mongoid.override_database(destination_database)
  end
  
    def self.get_user_for_api_action(options={},limit=20)
    begin
      data = []
      ::Report::ApiLog.set_db
      data = ::Report::ApiLog
      if options.present?
        data = data.where(options).order_by("id desc")
      else
        data = data.all
      end
      data = data.order_by("id desc").limit(limit).entries
      ::Report::ApiLog.reset_db
      data
    rescue Exception => e
      Rails.logger.info e.message
      puts e.message
      ::Report::ApiLog.reset_db
      data = []
    end
      
  end

  # Report::ApiLog.get_all_log_event_id
  def self.get_all_log_event_id(options={},limit=20)
    begin
      data = []
      ::Report::ApiLog.set_db
      data = ::Report::ApiLog
        data = data.distinct(:event_custom_id)
      ::Report::ApiLog.reset_db
    rescue Exception => e
      Rails.logger.info e.message
      puts e.message
      ::Report::ApiLog.reset_db
      data = []
    end
      data
  end


  def self.get_data(options={},limit=50)
    Rails.logger.info "Data called......................................................"
    begin
      data = []
     # user_filter_status, user_email_list = ::Report::ApiLog.get_user_selected(options)
     user_filter_status, user_ids = ::Report::ApiLog.get_user_selected(options)
      ::Report::ApiLog.set_db
      data = ::Report::ApiLog
      # if options.blank?
      #   data = data.limit(limit)
      # end
       if options[:start_date].present? && options[:end_date].present?
        data =  data.where(:created_at=>{"$lte"=>(options[:end_date].to_date + 1.day),"$gte"=>(options[:start_date].to_date)})
       end
       if user_filter_status
        # data =  data.where(:user_email.in=>user_email_list)
        data =  data.where(:user_id.in=>user_ids)
       end

       if options[:event_source].present?
          data =  data.where(:event_source=>options[:event_source])
       end
       if options[:event_custom_id].present?
          data =  data.where(:event_custom_id.in=>options[:event_custom_id].split(",").flatten)
       end
       if options[:controller_access].present?
        data =  data.where(:controller=>/#{options[:controller_access]}/i)
       end
       if options[:log_category].present?
         data =  data.where(request_type:options[:log_category])
       end
 
      if options[:datatable_filter] && options['search']['value'].present?
        data = data.datatable_filter(options['search']['value'], options['columns'])
      end
      # if options[:datatable_order]
      #   #data = data.datatable_order(options['order']['0']['column'].to_i,
      #    #                       options['order']['0']['dir'])
    
      # end
      count = data.count
      data =  data.order_by("id desc")

      # data = data.skip((options[:page].to_i - 1) * options[:per_page].to_i).limit(options[:per_page].to_i).entries
      options[:page] = options[:page].to_i > 0 ? options[:page] : 0
      data = data.paginate(:page=>options[:page],:per_page=>options[:per_page])#.entries

      ::Report::ApiLog.reset_db
      if options[:page].present?
        [ data,count]
      else
        data
    end
    rescue Exception => e
      Rails.logger.info e.message
      puts e.message
      count = 0
      ::Report::ApiLog.reset_db
      data = []
    end
       [ data,count]
  end
  
  def self.create_record(current_user_email,params,options={})
    begin
      action = params[:action]
      controller = params[:controller]
      return true  if action == "enable_push_notification" 
      source_database = "api_logs"
      user_id = (User.where(email:current_user_email).first.id rescue nil)
      destination_database = APP_CONFIG["database_name"]
      #::Report::ApiLog.set_db
      request_data = params.except(:password,:confirm_password,:token,:action,:controller)
      request_format = options[:request_format]
      request_type = action_request_type(action) || options[:request_type]
      request_type =  request_type.titleize
      api_controller_action = controller.split("/")[1]
      api_version =  api_controller_action == "v4" ? 4 : (api_controller_action.gsub("v","").to_i rescue 1)
      member_id = Report::ApiLog.get_key_value(params,"member_id")
      pregnancy_id = options[:pregnancy_id] || Report::ApiLog.get_key_value(params,"pregnancy_id") || Report::ApiLog.get_key_value(params,"preg_id")
      if pregnancy_id.present?
        member_id = Pregnancy.find(pregnancy_id).expected_member_id rescue nil
      end
      member_country_code = nil
      user_country_code = nil
      if member_id.present? 
        member = Member.where(id:member_id).last
        member_country_code =   member.get_country_code rescue nil
      end
      
      if user_id.present?
        user_country_code =   user.country_code rescue nil
      end


      family_id = Report::ApiLog.get_key_value(params,"family_id")
      family_country_code = nil
      if family_id.present?
        family_country_code = Family.find(family_id).country_code rescue nil 
      end
     family_country_code = family_country_code || user_country_code || member_country_code
      current_user_email = current_user_email || Report::ApiLog.get_key_value(params,"email")
      if params[:token].blank?
       api_version = 0
       current_user_email = (current_user_email || options[:current_user].email) rescue nil
      end

      request_data.delete(:authentication_token) rescue nil
      request_data.delete(:token) rescue nil
      request_data.delete(:password) rescue nil
      request_data.delete(:confirm_password) rescue nil
      request_data.delete(:receipt_data) rescue nil
      ip_location = ::Report::ApiLog.get_location(options[:ip_address]) if Rails.env == "production"
      Mongoid.override_database(source_database)
      ::Report::ApiLog.create(:family_country_code=>family_country_code, :member_country_code=>member_country_code,:user_country_code=>user_country_code, :request_params=>request_data, :family_id=>family_id, :event_source=>"api",:user_id=>user_id,:ip_address=>options[:ip_address], :location=> ip_location,:request_type=>request_type,:request_format=>request_format, :member_id=>member_id, :api_version=> api_version, :user_email=>current_user_email,:action=>action,:controller=>controller)
      Mongoid.override_database(destination_database)
     
      if member_id.present? && options[:update_activity_status] != false
        member = Member.where(id:member_id).last
        if member.present?
          activity_status = "Active"
          member.update_activity_status_v2(user_id,activity_status,options)
          #Update logged in user
          if user_id.present? 
            user = User.find(user_id)
            if user.member_id.to_s != member_id.to_s &&  user.activity_status_v2 != activity_status
              user.mark_user_activity_status_v2(activity_status,options.merge({:user_id => user.id,:update_family_activity_status => false}))
            end
          end
        end
      end
    rescue Exception=> e
      Rails.logger.info "Error inside ApiLog create_record\n"
      Rails.logger.info e.message
      Mongoid.override_database(destination_database)
    end
  end


  def self.log_event(user_id,family_id,event_custom_id,options={})
    begin
      source_database = "api_logs"
      user_id = user_id || (options[:current_user].user_id rescue nil)
      user = User.find(user_id) rescue User.new
      member_id = options[:member_id]
      if options[:pregnancy_id].present?
        member_id = Pregnancy.find(options[:pregnancy_id]).expected_member_id rescue nil
      end
      if member_id.present? 
        member = Member.where(id:member_id).last
        member_country_code =   member.get_country_code
      else
        member_country_code = nil
      end
      
      if user_id.present?
        user_country_code =   user.country_code rescue nil
      end
      family_id = family_id || options[:family_id]
      
      family_country_code = nil
      if family_id.present?
        family_country_code = Family.find(family_id).country_code rescue nil
      end
      family_country_code = family_country_code || user_country_code || member_country_code
      
      event_source = options[:event_source] || "api"
      destination_database = APP_CONFIG["database_name"]
      Mongoid.override_database(source_database)
      failed_attempt = options[:failed_attempt] || 0
      
      request_status = options[:status]
      data = ::Report::ApiLog.create(:family_country_code=>family_country_code, :user_country_code=>user_country_code,:member_country_code=>member_country_code, :request_status=>request_status, :failed_attempt=>failed_attempt, :event_source=>event_source,:family_id=>family_id,:event_custom_id=>event_custom_id,:user_id=>user_id,:ip_address=>options[:ip_address], :request_type=>"event", :member_id=>member_id,  :user_email=>user.email)
      data.update_attributes(:created_at=> options[:created_at]) if  options[:created_at].present?
      Mongoid.override_database(destination_database)
      
      if member_id.present? && options[:update_activity_status] != false
       
        if member.present?
          activity_status = "Active"
          member.update_activity_status_v2(user_id,activity_status,options)
        end
      end
    rescue Exception=> e
      Rails.logger.info "Error inside ApiLog log_event\n"
      Rails.logger.info e.message
      Mongoid.override_database(destination_database)
    end
    data
  end


  #Report::ApiLog.get_member_log_event(50)
  def self.get_member_log_event(member_id,event_custom_id=nil,options={})
    begin
      source_database = "api_logs"
      destination_database = APP_CONFIG["database_name"]
      Mongoid.override_database(source_database)
      data = ::Report::ApiLog.where(member_id:member_id)
      if event_custom_id.present?
        event_custom_id =  [event_custom_id].flatten
        data = data.where(:event_custom_id.in=>event_custom_id)
      end
      if options[:start_date].present? && options[:start_date].present?
        data = data.where(:created_at=>{"$gte"=>options[:start_date],"$lte"=>options[:end_date] })
      end
       
      if options[:request_status].present?
        data = data.where(request_status:options[:request_status])
      end
      if options[:get_count] == true
        data = data.count
      else
        data = data.order_by("id desc").limit(options[:limit]||10).entries
      end
      Mongoid.override_database(destination_database)
    rescue Exception=> e
      Rails.logger.info "Error inside ApiLog log_event\n"
      Rails.logger.info e.message
      Mongoid.override_database(destination_database)
    end
    data
  end
  
  #Report::ApiLog.get_log_event(50)
  def self.get_log_event(event_custom_id,user_id=nil,family_id=nil,options={})
    begin
      source_database = "api_logs"
      destination_database = APP_CONFIG["database_name"]
      Mongoid.override_database(source_database)
      data = ::Report::ApiLog.where(nil)
      if user_id.present?
        user_id =  [user_id].flatten
        data = data.where(:user_id.in=>user_id)
      end
      if family_id.present?
        family_id =  [family_id].flatten
        data = data.where(:family_id.in=>family_id)
      end
      if options[:member_country_code].present?
        sanitize_country_code = Member.sanitize_country_code(options[:member_country_code])
        data = data.where(:member_country_code.in=>sanitize_country_code)
      end

      if options[:user_country_code].present?
        sanitize_country_code = Member.sanitize_country_code(options[:user_country_code])
        data = data.where(:user_country_code.in=>sanitize_country_code)
      end
      if options[:family_country_code].present?
        sanitize_country_code = Member.sanitize_country_code(options[:family_country_code])
        data = data.where(:family_country_code.in=>sanitize_country_code)
      end

      event_custom_id =  [event_custom_id].flatten
      if event_custom_id.length > 1
        data = data.where(:event_custom_id.in=>event_custom_id)
      else
        data = data.where(:event_custom_id=>event_custom_id.first)
      end
      if options[:start_date].present? && options[:start_date].present?
        data = data.where(:created_at=>{"$gte"=>options[:start_date],"$lte"=>options[:end_date].to_date.end_of_day })
      end
      if options[:member_id].present?
        data = data.where(member_id:member_id)
      end
      if options[:request_status].present?
        data = data.where(request_status:options[:request_status])
      end
      if options[:get_count] == true
        data = data.only(:id).count
      elsif options[:selected_field].present?
        data = data.only(options[:selected_field].to_sym).entries
      else
        data = data.entries
      end
      Mongoid.override_database(destination_database)
    rescue Exception=> e
      Rails.logger.info "Error inside ApiLog log_event\n"
      Rails.logger.info e.message
      Mongoid.override_database(destination_database)
    end
    data
  end


  def self.get_user_selected(options)
    user_ids = []
    options[:first_name] = nil if options[:first_name] == "Name"
    options.delete(:email) if options[:email] == "Email"

    valid_critriea_keys = options.select{|k,v| v.present? && (k.downcase != v.downcase rescue true)}
    search_data = valid_critriea_keys.with_indifferent_access
    users_email = nil
    provider = options[:signup_provider]
  
    if provider.present?
      nurturey_provider_index = provider.index("Nurturey") rescue nil
      provider[nurturey_provider_index] = [nil,"","Nurturey"] if nurturey_provider_index.present?
      provider = provider.flatten
    end
    data = {:provider =>provider , :user_type=>search_data[:user_type], :stage=> search_data[:stage], :segment=>search_data[:segment], :activity_status=>search_data[:activity_status], :first_name=>search_data[:first_name], :email=>search_data[:email], :status=>search_data[:status] ,:country_code=>search_data[:country_code]}
    user_filter_status = false
    data = data.reject{|k,v| v.blank?}
    data.each do |k,v|
      begin
        data[k] = v.split(",")
      rescue Exception =>e 
    

      end
    end

    @users = User.where(nil)
    if data.present?
      user_filter_status = true
      data.each do |k,v|
        if k.to_s == "country_code" || k.to_s == "provider"
          val = (v.flatten)
          @users = @users.in(k=>val)
        else
          optRegexp = []
          [v].flatten.each do |opt|
            if k == :first_name || k == :email
              optRegexp.push(/#{opt}/i) 
            else
              optRegexp.push(/^#{opt}/i) 
            end
          end
          val = optRegexp
          @users = @users.in(k=>val)
        end
      end 
    end
  
    if search_data[:user_subscription].present?
      search_data[:user_subscription] = search_data[:user_subscription].split(",")
      user_filter_status = true
      members_with_premium_subscription = []
      free_subscription_member_ids = []
      subscriptions = search_data[:user_subscription].deep_copy
      subscription_manager_for_free = SubscriptionManager.free_subscription
      if subscriptions.include?(subscription_manager_for_free.title)
        members_with_premium_subscription = Subscription.where(:subscription_manager_id.ne=>subscription_manager_for_free).only(:member_id).pluck(:member_id)
        subscriptions.delete(subscription_manager_for_free.title)
        free_subscription_member_ids = Member.where(:id.nin=>members_with_premium_subscription).pluck(:id) #if members_with_premium_subscription.present?
      end 
      if subscriptions.present?
        subscription_manager_id = SubscriptionManager.where(:title.in=>subscriptions).pluck(:id)
        members_with_premium_subscription = Subscription.where(:subscription_manager_id.in=>subscription_manager_id).only(:member_id).pluck(:member_id)
      end
      subscription_selected_member_ids = free_subscription_member_ids + members_with_premium_subscription
      @users = @users.where(:member_id.in=>subscription_selected_member_ids) #if members_with_premium_subscription.present?
    end
    # users_email = @users.only("email").pluck(:email) if user_filter_status
    # [user_filter_status, users_email]
    if options[:return_member_id] == true 
      user_ids = @users.only("member_id").pluck(:member_id) if user_filter_status
    else
      user_ids = @users.only("id").pluck(:id) if user_filter_status
    end
    [user_filter_status, user_ids]
  end
  

  
  def self.get_location(ip_address)
    geolocation = Geocoder.search(ip_address).first.data rescue nil
    if geolocation.nil?
      begin
        data = JSON.parse(`curl -X POST http://ip-api.com/json/#{ip_address} --max-time 0.5`)
        geolocation = "#{data['city']}, #{data['country']},zip-code:#{data['zip']}" 
      rescue Exception => e 
        geolocation = nil
      end
    else
      if geolocation["city_name"].blank?
        begin
          data = JSON.parse(`curl -X POST http://ip-api.com/json/#{ip_address} --max-time 0.5`)
          geolocation = "#{data['city']}, #{data['country']},zip-code:#{data['zip']}" 
        rescue Exception =>e 
          geolocation = Geocoder.address(ip_address)
        end
      else
        geolocation = Geocoder.address(ip_address)
      end
    end
    geolocation
  end

  def self.get_key_value(hash,key)
    if hash.key?(key.to_s) 
      return hash[key.to_s] 
    elsif hash.key?(key.to_s.to_sym)
     return hash[key.to_s.to_sym] 
    end
    key_value = nil
    hash.each do |hash_key,value|
      if value.class.to_s == "Hash"
        key_value =  get_key_value(value,key)
      end
    end
    return key_value

     
  end

  def self.action_request_type(action_name)
    action_name =  action_name.to_s.downcase
    action = nil
    if ::Report::ApiLog.action_for_get?(action_name)
      action =  "get"
    elsif action_for_put?(action_name)
      action =  "put"
    elsif ::Report::ApiLog.action_for_delete?(action_name)
      action =  "delete"
    elsif ::Report::ApiLog.action_for_post?(action_name)
      action =  "post"
    else 
      action_type =  action_name.split("_")[0]
      if ["get","update"].include?(action_type)
        action = action_type
      elsif ["add","create"].include?(action_type)
        action = "post"
      elsif ["destroy","delete","remove"].include?(action_type)
        action = "delete"
      end
    end
    action
  end


  def self.action_for_put?(action_name)
    put_action_list = ["update", "update_subscription_from_apple_server", "update_subscription_from_play_store", "unsubscribe_from_subscription", "update_subscription_receipt", "update_kick_count", "update_kick_counter_schedule", "update_upcoming_kick_counter_schedule", "update_child_tooth_detail", "update_blood_group", "update_checkup_plan", "update_activity", "update_quiz_level", "update_allergy", "update_eye_data", "upload_cover_image", "upload_image", "upload_image_multipart", "skip_push_screen", "skipped_details", "enable_push_notification", "set_default_cover_image", "secure_update", "acitvate", "update_nutrient_status", "update_timeline", "update_status", "update_jab", "update_record", "update_vaccination", "skip_automated_timeline", "remind_later", "event_update", "join_family", "add_description", "change_category", "display_later", "skip", "accept", "decline", "change_email", "set_metric_system", "add_to_favorite", "remind_me_later", "update_art_ref", "user_feedback", "update_child", "verify_elder_for_family", "verify_elder", "update_elder", "update_preg_child", "mark_preg_complete", "update_preg", "update_timezone"]
    status =  put_action_list.include?(action_name)
    status
  end

  def self.action_for_post?(action_name)
    create_action_list =  ["create","create_or_update_subscription", "secure_signup", "sign_in", "add_kick_count", "add_kick_counter_schedule", "add_child_tooth_detail", "add_test_to_system", "add_test_to_checkup_plan", "add_schedule_for_member", "add_checkup_plan", "add_activity", "add_participant", "add_quiz_details", "add_allergy_to_allergy_manager", "add_allergy_to_member_list", "add_symptom_to_allergy_symptom_manager", "create_eye_data", "upload_image_for_skin_assessment", "upload_image", "add_multiple", "add_milestone_from_handhold", "dont_want_add_spouse", "save_role", "join_this_family", "create_child", "create_elder", "save_documents", "upload_file", "create_timeline_for_multi_member", "user_timeline", "create_timeline", "add_hh_system_timelines", "create_record", "create_record_from_alexa", "save_jab", "save_auto_time", "send_to_facebook", "create_event", "save_joined_family", "save_and_next", "add_invitee", "send_invites", "update_current_member", "create_new_component", "add_recommended_test", "add_component_to_medical_event", "create_system_timeline", "create_preg"]
    status =  create_action_list.include?(action_name)
    status
  end

  def self.action_for_delete?(action_name)
    delete_action_list = ["destroy", "delete", "delete_kick_count", "delete_checkup", "delete_checkup_tests", "delete_participant", "delete_activity", "delete_allergy", "delete_symptom", "destroy_eye_data", "delete_skin_assessment", "delete_account", "deactivate", "delete_attachment", "delete_timeline", "delete_parameter", "delete_jab", "delete_vaccin", "delete_timeline_pic", "event_delete", "delete_child_interest", "remove_picture", "remove", "delete_component_from_sys", "delete_parental_test", "delete_component_from_medical_event", "delete_preg", "delete_article_ref", "delete_child", "delete_elder_invitation", "remove_elder"]
    status =  delete_action_list.include?(action_name)
    status
  end

  def self.action_for_get?(action_name)
    get_action_list = ["index", "get_available_plans", "subscriptions_tool_list", "get_tool_list", "verify_subscription", "get_detail", "get_tooth_manager_data", "list_allergies", "get_checkup_plans", "get_system_tests", "list_all_activity", "list_participant", "daily_summary", "complete_summary", "list_all_allergies", "list_member_allergies", "list_all_allergy_symptom", "list", "skin_assessment_details", "get_list", "sign_in", "show", "push_msg", "user_sign_out", "secure_sign_in", "secure_alexa_sign_in", "basic_set_up_evaluator_for_user", "child_members", "sign_in_via_facebook", "user_dashboard", "recent_items", "user_events", "get_cover_image", "user_info", "get_face_info", "get_handhold_actions", "get_summary_screen", "set_version_controller", "hh_milestones", "milestones_for_graph", "member_milestone_list", "milestone_detail", "get_milestone_detail", "nutrients_status", "categories", "change_category", "search_family", "select_role", "find_member", "family_members", "new_child", "new_elder", "search", "types", "get_doc_detail", "get_pre_defined_timeline", "get_timeline_detail", "get_hh_system_timelines", "edit", "new_health_record", "get_health_detail", "get_zscore_detail", "health_info", "close_info_pop", "new_jab", "jab_detail", "jabs_month_wise", "upcoming_jabs", "immunisation_info", "edit_jab", "add_vaccin", "edit_vaccin", "pics", "media", "automated_timeline", "timeline_info", "facebook_preview", "get_data", "get_parent_data", "get_event_datail", "calendar_info", "dashboard", "get_family_data", "milestones_info", "display_picture", "documents_info", "download", "view", "check_all", "edit_email", "view_invite", "get_medical_event_list", "get_mother_list", "health_card", "get_preg_info", "get_sys_components", "edit_elder", "get_medical_event_detail", "edit_child", "get_default_timelines", "get_recommended_medical_event", "resend_invitation", "art_ref_detail", "recommended_art_ref", "edit_picture", "tool_activation", "hide_invited_user_block", "trigger_tour_step", "edit_current_member"]
    status =  get_action_list.include?(action_name)
    status
  end
  # ::Report::ApiLog.update_request_type
  def self.update_request_type(options={})
    begin
      source_database = "api_logs"
      destination_database = APP_CONFIG["database_name"]
      Mongoid.override_database(source_database)
      #::Report::ApiLog.set_db
      ::Report::ApiLog.all.each do |log_record| 
        begin
          action_name = log_record.request_type.downcase rescue ""
          action_type = ::Report::ApiLog.action_request_type(log_record.action)
          # puts "#{action_type} and #{action_name}"
          if action_type != action_name
            log_record.request_type = action_type.titleize
            log_record.save
          end
        rescue Exception => e 
          puts e.message
          Rails.logger.info e.message
          UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside update_request_type")
        end
      end
      Mongoid.override_database(destination_database)
    rescue Exception => e
      #puts e.message 
      Mongoid.override_database(destination_database)
    end
  end

  def self.flatten_hash(param, prefix=nil)
    param.each_pair.reduce({}) do |a, (k, v)|
      v.is_a?(Hash) ? a.merge(flatten_hash(v, "#{k}.")) : a.merge("#{k}".to_sym => v)
    end
  end


  def self.update_country_code_for_event
    begin
      source_database = "api_logs"
      destination_database = APP_CONFIG["database_name"]
      Mongoid.override_database(source_database)
      ::Report::ApiLog.where(:created_at.gte=>Date.today-3.month).each do |log_record| 
        begin
          user_id = log_record.user_id
          member_id = log_record.member_id
          log_record
        rescue Exception => e 
          puts e.message
          Rails.logger.info e.message
        end
      end
      Mongoid.override_database(destination_database)
    rescue Exception => e
      Mongoid.override_database(destination_database)
    end
  end




  def self.get_country(user_id,member_id,family_id=nil)
    begin
      source_database = "api_logs"
      destination_database = APP_CONFIG["database_name"]
      Mongoid.override_database(destination_database)
      member_country_code = nil
      user_country_code = nil
      family_country_code = nil
      if member_id.present?
        member_country_code = Member.find(member_id).get_country_code
      end
      if user_id.present?
        user_country_code = User.find(user_id).country_code
      end
      if family_id.present?
        family_country_code = Family.find(family_id).owner.get_country_code
      end

      Mongoid.override_database(source_database)
    rescue Exception => e
      Mongoid.override_database(source_database)
    end
    [user_country_code,member_country_code,family_country_code]
  end


# ::Report::ApiLog.update_country_code_for_event
  def self.update_country_code_for_event
    begin
      source_database = "api_logs"
      destination_database = APP_CONFIG["database_name"]
   
      Mongoid.override_database(source_database)
      ::Report::ApiLog.where(:created_at.gte=>Date.today-3.month).each do |log_record| 
        begin
          user_id = log_record.user_id
          member_id = log_record.member_id
          user_country_code,member_country_code,family_country_code = ::Report::ApiLog.get_country(user_id,member_id)
          log_record["user_country_code"] = user_country_code
          log_record["member_country_code"] = member_country_code
          log_record.save
        rescue Exception => e 
          puts e.message
          Rails.logger.info e.message
        end
      end
      Mongoid.override_database(destination_database)
    rescue Exception => e
      Mongoid.override_database(destination_database)
    end
  end

  # Report::ApiLog.update_family_country_code_for_event
   def self.update_family_country_code_for_event
    begin
      source_database = "api_logs"
      destination_database = APP_CONFIG["database_name"]
   
      Mongoid.override_database(source_database)
      ::Report::ApiLog.where(:created_at.gte=>Date.today - 6.month).where(:family_id.ne=>nil).each do |log_record| 
        begin
          family_id = log_record["family_id"]
          user_country_code,member_country_code,family_country_code = ::Report::ApiLog.get_country(nil,nil,family_id)
          log_record["family_country_code"] = family_country_code
          log_record.save
        rescue Exception => e 
          puts e.message
          Rails.logger.info e.message
        end
      end
      Mongoid.override_database(destination_database)
    rescue Exception => e
      Mongoid.override_database(destination_database)
    end
  end




end