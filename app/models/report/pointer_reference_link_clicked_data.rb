class Report::PointerReferenceLinkClickedData
  include Mongoid::Document
  include Mongoid::Timestamps
  field :pointer_reference_link_id, type:String
  field :pointer_id, type:String
  field :device_platform, type:String
  # belongs_to :article_ref
  belongs_to :user
  validates_presence_of :pointer_reference_link_id
  validates_presence_of :pointer_id
  validates_presence_of :user_id
 # validates_presence_of :device_platform

end