class Report::Event
  include Mongoid::Document
  field :custom_id,        :type=>Integer # eg. 1,2,3 
  field :name,             :type=>String  # eg. user marked active #used to retrive data
  field :category,         :type=>String  # eg. user/family/subscription
  field :title,            :type=> String # for display purpose
  field :event_actions,          :type=>Array, :default => []
  field :event_controllers,      :type=>Array, :default => []
  field :status,                 :type=>String, :default => "active" # active/inactive
  
  def custom_title
    "#{title} (id: #{custom_id})"
  end

  def self.get_event_data(controller , event_action)
    if controller.include? "api/"
      controller_strings = controller.split("/")
      controller_strings = controller_strings.drop(2)
      controller_without_prefix = controller_strings.join("/")
    else
      controller_without_prefix = controller
    end
    Report::Event.where(status:"active", :event_controllers.in => [controller_without_prefix], :event_actions.in => [event_action]).first
  end

  
  def self.update_title
    ::Report::Event.all.each do |event|
      if event.title.blank?
        puts event.name
        event.title = event.name
        event.save
      end
    end
  end
  
  def self.seed_event_data
    Report::Event.create(status:"active",custom_id:1, name:'mark user active',category:'user')
    Report::Event.create(status:"active",custom_id:2, name:'mark user inactive',category:'user')
    Report::Event.create(status:"active",custom_id:3, name:'mark user defunct',category:'user')

    Report::Event.create(status:"active",custom_id:4, name:'mark user segment trial',category:'user')
    Report::Event.create(status:"active",custom_id:5, name:'mark user segment starter',category:'user')
    Report::Event.create(status:"active",custom_id:6, name:'mark user segment adoptor',category:'user')
    Report::Event.create(status:"active",custom_id:7, name:'mark user segment ambassador',category:'user')
    
    Report::Event.create(status:"active",custom_id:8, name:'mark user stage prospect',category:'user')
    Report::Event.create(status:"active",custom_id:9, name:'mark user stage customer',category:'user')
    
    Report::Event.create(status:"active",custom_id:10, name:'mark user pricing_exemption none',category:'user')
    Report::Event.create(status:"active",custom_id:11, name:'mark user pricing_exemption basic',category:'user')
    Report::Event.create(status:"active",custom_id:12, name:'mark user pricing_exemption all',category:'user')
    
    
    # For Family
    Report::Event.create(status:"active",custom_id:13, name:'mark family active',category:'family')
    Report::Event.create(status:"active",custom_id:14, name:'mark family inactive',category:'family')
    Report::Event.create(status:"active",custom_id:15, name:'mark family defunct',category:'family')
    
    Report::Event.create(status:"active",custom_id:16, name:'mark family segment trial',category:'family')
    Report::Event.create(status:"active",custom_id:17, name:'mark family segment starter',category:'family')
    Report::Event.create(status:"active",custom_id:18, name:'mark family segment adoptor',category:'family')
    Report::Event.create(status:"active",custom_id:19, name:'mark family segment ambassador',category:'family')
    Report::Event.create(status:"active",custom_id:20, name:'mark family pricing_stage trial',category:'family')
    Report::Event.create(status:"active",custom_id:21, name:'mark family pricing_stage standard',category:'family')
    Report::Event.create(status:"active",custom_id:22, name:'mark family pricing_exemption basic',category:'family')
    Report::Event.create(status:"active",custom_id:23, name:'mark family pricing_exemption all',category:'family')
    Report::Event.create(status:"active",custom_id:24, name:'mark family pricing_exemption none',category:'family')

    Report::Event.create(status:"active",custom_id:25, name:'subscription upgraded',category:'subscription')
    Report::Event.create(status:"active",custom_id:26, name:'subscription cancelled',category:'subscription')
    Report::Event.create(status:"active",custom_id:27, name:'subscription renew',category:'subscription')
    Report::Event.create(status:"active",custom_id:28, name:'subscription added',category:'subscription')
    Report::Event.create(status:"inactive",custom_id:29, name:'user signup',category:'user')
    Report::Event.create(status:"active",custom_id:30, name:'show subscription popup',category:'subscription')
    Report::Event.create(status:"active",custom_id:31, name:'alexa mental math practice question response',category:'alexa')
    Report::Event.create(status:"active",custom_id:32, name:'alexa mental math practice quiz level update',category:'alexa')
    Report::Event.create(status:"active",custom_id:33, name:'family added',category:'family')
    Report::Event.create(status:"active",custom_id:34, name:'member added to family',category:'family')
    Report::Event.create(status:"active",custom_id:35, name:'pregnancy added to family',category:'family')
    Report::Event.create(status:"active",custom_id:36, name:'family deleted',category:'family')
    Report::Event.create(status:"active",custom_id:37, name:'member deleted from family',category:'family')
    Report::Event.create(status:"active",custom_id:38, name:'pregnancy deleted from to family',category:'family')
    Report::Event.create(status:"active",custom_id:39, name:'show app review popup',category:'subscription')
    Report::Event.create(status:"active",custom_id:40, name:'test added to checkup planner',category:'', event_actions:["add_test_to_checkup_plan"], event_controllers:["child/checkup_planner/checkup_schedule"])
    Report::Event.create(status:"active",custom_id:41, name:'checkup tests listed for child',category:'', event_actions:["get_checkup_plans"], event_controllers:["child/checkup_planner/checkup_schedule"])
    Report::Event.create(status:"active",custom_id:42, name:'checkup plan added for child',category:'', event_actions:["add_schedule_for_member"], event_controllers:["child/checkup_planner/checkup_schedule"])
    Report::Event.create(status:"active",custom_id:43, name:'checkup plan updated',category:'', event_actions:["update_checkup_plan"], event_controllers:["child/checkup_planner/checkup_schedule"])
    Report::Event.create(status:"active",custom_id:44, name:'checkup tests deleted from checkup planner',category:'', event_actions:["delete_checkup_tests"], event_controllers:["child/checkup_planner/checkup_schedule"])
    Report::Event.create(status:"active",custom_id:45, name:'checkup deleted from checkup planner',category:'', event_actions:["delete_checkup"], event_controllers:["child/checkup_planner/checkup_schedule"])
    Report::Event.create(status:"active",custom_id:46, name:'checkup test added to system by user',category:'', event_actions:["add_test_to_system"], event_controllers:["child/checkup_planner/checkup_schedule"])
    Report::Event.create(status:"active",custom_id:47, name:'available checkup planner system listed',category:'', event_actions:["get_system_tests"], event_controllers:["child/checkup_planner/checkup_schedule"])
    Report::Event.create(status:"active",custom_id:48, name:'available allergies in sytem for child get listed',category:'', event_actions:["list_all_allergies"], event_controllers:["child/health_card/allergy"])
    Report::Event.create(status:"active",custom_id:49, name:'allergies for child get listed',category:'', event_actions:["list_member_allergies"], event_controllers:["child/health_card/allergy"])
    Report::Event.create(status:"active",custom_id:50, name:'allergy added in allregry manager by user',category:'', event_actions:["add_allergy_to_allergy_manager"], event_controllers:["child/health_card/allergy"])
    Report::Event.create(status:"active",custom_id:51, name:'allergy added for child',category:'', event_actions:["add_allergy_to_member_list"], event_controllers:["child/health_card/allergy"])
    Report::Event.create(status:"active",custom_id:52, name:'allergy updated for child',category:'', event_actions:["update_allergy"], event_controllers:["child/health_card/allergy"])
    Report::Event.create(status:"active",custom_id:53, name:'allergy deleted for child',category:'', event_actions:["delete_allergy"], event_controllers:["child/health_card/allergy"])
    Report::Event.create(status:"active",custom_id:54, name:'allergy symptoms get listed',category:'', event_actions:["list_all_allergy_symptom"], event_controllers:["child/health_card/allergy_symptom"])
    Report::Event.create(status:"active",custom_id:55, name:'added symptom to allergy',category:'', event_actions:["add_symptom_to_allergy_symptom_manager"], event_controllers:["child/health_card/allergy_symptom"])
    Report::Event.create(status:"active",custom_id:56, name:'symptom deleted from allergy',category:'', event_actions:["delete_symptom"], event_controllers:["child/health_card/allergy_symptom"])
    Report::Event.create(status:"active",custom_id:57, name:'added record from eye chart get listed',category:'', event_actions:["list"], event_controllers:["child/health_card/eye_chart"])
    Report::Event.create(status:"active",custom_id:58, name:'record added to eye chart',category:'', event_actions:["create_eye_data"], event_controllers:["child/health_card/eye_chart"])
    Report::Event.create(status:"active",custom_id:59, name:'eye chart record updated',category:'', event_actions:["update_eye_data"], event_controllers:["child/health_card/eye_chart"])
    Report::Event.create(status:"active",custom_id:60, name:'eye chart record deleted',category:'', event_actions:["destroy_eye_data"], event_controllers:["child/health_card/eye_chart"])
    Report::Event.create(status:"active",custom_id:61, name:'mental maths get listed for child',category:'', event_actions:["complete_summary"], event_controllers:["child/mental_math/quiz_details"])
    Report::Event.create(status:"active",custom_id:62, name:'quiz detail added for child',category:'', event_actions:["add_quiz_details"], event_controllers:["child/mental_math/quiz_details"])
    Report::Event.create(status:"active",custom_id:63, name:'health report get listed for child',category:'', event_actions:["index"], event_controllers:["child/health_card"])
    Report::Event.create(status:"active",custom_id:64, name:'allergies get listed for member',category:'', event_actions:["list_allergies"], event_controllers:["child/health_card"])
    Report::Event.create(status:"active",custom_id:65, name:'blood group updated for member',category:'', event_actions:["update_blood_group"], event_controllers:["child/health_card"])
    Report::Event.create(status:"active",custom_id:66, name:'dental health list for child get listed',category:'', event_actions:["member_tooth_list"], event_controllers:["child/tooth_chart/chart_manager"])
    Report::Event.create(status:"active",custom_id:67, name:'added tooth get listed for member',category:'', event_actions:["toothList"], event_controllers:["child/tooth_chart/chart_manager"])
    Report::Event.create(status:"active",custom_id:68, name:'tooth detail added in dental health',category:'', event_actions:["add_child_tooth_detail"], event_controllers:["child/tooth_chart/chart_manager"])
    Report::Event.create(status:"active",custom_id:69, name:'tooth detail updated in dental health',category:'', event_actions:["update_child_tooth_detail"], event_controllers:["child/tooth_chart/chart_manager"])
    Report::Event.create(status:"active",custom_id:70, name:'tooth detail deleted from dental health',category:'', event_actions:["destroy"], event_controllers:["child/tooth_chart/chart_manager"])
    Report::Event.create(status:"active",custom_id:71, name:'pregnancy blood pressure report get listed',category:'', event_actions:["index"], event_controllers:["pregnancy/health_card/blood_pressure"])
    Report::Event.create(status:"active",custom_id:72, name:'blood pressure data added for pregnancy health card',category:'', event_actions:["create"], event_controllers:["pregnancy/health_card/blood_pressure"])
    Report::Event.create(status:"active",custom_id:73, name:'blood pressure data updated forpregnancy health card',category:'', event_actions:["update"], event_controllers:["pregnancy/health_card/blood_pressure"])
    Report::Event.create(status:"active",custom_id:74, name:'blood pressure data deleted for pregnancy health card',category:'', event_actions:["destroy"], event_controllers:["pregnancy/health_card/blood_pressure"])
    Report::Event.create(status:"active",custom_id:75, name:'haemoglobin detailed info get listed for pregnancy health card',category:'', event_actions:["index"], event_controllers:["pregnancy/health_card/hemoglobin"])
    Report::Event.create(status:"active",custom_id:76, name:'haemoglobin info added for pregnancy health card',category:'', event_actions:["create"], event_controllers:["pregnancy/health_card/hemoglobin"])
    Report::Event.create(status:"active",custom_id:77, name:'haemoglobin info updated for pregnancy health card',category:'', event_actions:["update"], event_controllers:["pregnancy/health_card/hemoglobin"])
    Report::Event.create(status:"active",custom_id:78, name:'haemoglobin info deleted from pregnancy health card',category:'', event_actions:["destroy"], event_controllers:["pregnancy/health_card/hemoglobin"])
    Report::Event.create(status:"active",custom_id:79, name:'measurement detailed info get listed for pregnancy health card',category:'', event_actions:["index"], event_controllers:["pregnancy/health_card/measurement"])
    Report::Event.create(status:"active",custom_id:80, name:'measurement info added for pregnancy health card',category:'', event_actions:["create"], event_controllers:["pregnancy/health_card/measurement"])
    Report::Event.create(status:"active",custom_id:81, name:'measurement details get listed for pregnancy health card',category:'', event_actions:["get_detail"], event_controllers:["pregnancy/health_card/measurement"])
    Report::Event.create(status:"active",custom_id:82, name:'measurement info updated for pregnancy health card',category:'', event_actions:["update"], event_controllers:["pregnancy/health_card/measurement"])
    Report::Event.create(status:"active",custom_id:83, name:'measurement info deleted for pregnancy health card',category:'', event_actions:["destroy"], event_controllers:["pregnancy/health_card/measurement"])
    Report::Event.create(status:"active",custom_id:84, name:'kick count detailed info get listed',category:'', event_actions:["index"], event_controllers:["pregnancy/kick_counter/kick_count"])
    Report::Event.create(status:"active",custom_id:85, name:'kick count record added in kick counter',category:'', event_actions:["add_kick_count"], event_controllers:["pregnancy/kick_counter/kick_count"])
    Report::Event.create(status:"active",custom_id:86, name:'kick count record updated in kick counter',category:'', event_actions:["update_kick_count"], event_controllers:["pregnancy/kick_counter/kick_count"])
    Report::Event.create(status:"active",custom_id:87, name:'kick count record deleted from kick counter',category:'', event_actions:["delete_kick_count"], event_controllers:["pregnancy/kick_counter/kick_count"])
    Report::Event.create(status:"active",custom_id:88, name:'kick counter schedule added',category:'', event_actions:["add_kick_counter_schedule"], event_controllers:["pregnancy/kick_counter/kick_count"])
    Report::Event.create(status:"active",custom_id:89, name:'kick counter schedule updated',category:'', event_actions:["update_kick_counter_schedule"], event_controllers:["pregnancy/kick_counter/kick_count"])
    Report::Event.create(status:"active",custom_id:90, name:'upcomming kick counter schedule updated',category:'', event_actions:["update_upcoming_kick_counter_schedule"], event_controllers:["pregnancy/kick_counter/kick_count"])
    Report::Event.create(status:"active",custom_id:91, name:'all pointers get listed',category:'', event_actions:["index"], event_controllers:["article_ref"])
    Report::Event.create(status:"active",custom_id:92, name:'pointer links listed for pointer',category:'', event_actions:["art_ref_detail"], event_controllers:["article_ref"])
    Report::Event.create(status:"active",custom_id:93, name:'pointer link clicked by user',category:'', event_actions:["save_pointer_reference_link_clicked_detail"], event_controllers:["article_ref"])
    Report::Event.create(status:"active",custom_id:94, name:'remind me later for pointer',category:'', event_actions:["remind_me_later"], event_controllers:["article_ref"])
    Report::Event.create(status:"active",custom_id:95, name:'add to favorite a pointer',category:'', event_actions:["add_to_favorite"], event_controllers:["article_ref"])
    Report::Event.create(status:"active",custom_id:96, name:'pointer deleted',category:'', event_actions:["delete_article_ref"], event_controllers:["article_ref"])
    Report::Event.create(status:"active",custom_id:97, name:'article details shown to user',category:'', event_actions:["show"], event_controllers:["article_ref"])
    Report::Event.create(status:"active",custom_id:98, name:'recommended pointer get listed',category:'', event_actions:["recommended_art_ref"], event_controllers:["article_ref"])
    Report::Event.create(status:"active",custom_id:99, name:'all event schedule for user get listed',category:'', event_actions:["index"], event_controllers:["calendar"])
    Report::Event.create(status:"active",custom_id:100, name:'event details from calendar get listed',category:'', event_actions:["get_event_datail"], event_controllers:["calendar"])
    Report::Event.create(status:"active",custom_id:101, name:'event added on calendar',category:'', event_actions:["create_event"], event_controllers:["calendar"])
    Report::Event.create(status:"active",custom_id:102, name:'event updated on calendar',category:'', event_actions:["event_update"], event_controllers:["calendar"])
    Report::Event.create(status:"active",custom_id:103, name:'event deleted from calendar',category:'', event_actions:["event_delete"], event_controllers:["calendar"])
    Report::Event.create(status:"active",custom_id:104, name:'calendar data for parent (all child) get listed',category:'', event_actions:["get_parent_data"], event_controllers:["calendar"])
    Report::Event.create(status:"active",custom_id:105, name:'faq topic list get listed from faq',category:'', event_actions:["get_topic_list"], event_controllers:["user/faq"])
    Report::Event.create(status:"active",custom_id:106, name:'faq subtopic get listed from faq',category:'', event_actions:["get_subtopic_list"], event_controllers:["user/faq"])
    Report::Event.create(status:"active",custom_id:107, name:'faq question details get listed from faq',category:'', event_actions:["get_question_detail"], event_controllers:["user/faq"])
    Report::Event.create(status:"active",custom_id:108, name:'get topic for subtopic with subtopic question ,answer and feedback detail',category:'', event_actions:["index"], event_controllers:["user/faq"])
    Report::Event.create(status:"active",custom_id:109, name:'feedback given by user for faq question',category:'', event_actions:["user_feedback"], event_controllers:["user/faq"])
    Report::Event.create(status:"active",custom_id:110, name:'clinic setup done',category:'', event_actions:["setup_clinic"], event_controllers:["clinic"])
    Report::Event.create(status:"active",custom_id:111, name:'all clinic avaiable in system get listed',category:'', event_actions:["get_clinic_list"], event_controllers:["clinic"])
    Report::Event.create(status:"active",custom_id:112, name:'user get autenticated by clinic (link with clinic)',category:'', event_actions:["autenticate_by_clinic"], event_controllers:["clinic"])
    Report::Event.create(status:"active",custom_id:113, name:'user confirmed his/her email to get verified in nurturey',category:'', event_actions:["show"], event_controllers:["confirmation"])
    Report::Event.create(status:"active",custom_id:114, name:'document get listed',category:'', event_actions:["index"], event_controllers:["documents"])
    Report::Event.create(status:"active",custom_id:115, name:'document details get listed',category:'', event_actions:["get_doc_detail"], event_controllers:["documents"])
    Report::Event.create(status:"active",custom_id:116, name:'document created by user',category:'', event_actions:["create"], event_controllers:["documents"])
    Report::Event.create(status:"active",custom_id:117, name:'document updated by user',category:'', event_actions:["update"], event_controllers:["documents"])
    Report::Event.create(status:"active",custom_id:118, name:'document deleted by user',category:'', event_actions:["destroy"], event_controllers:["documents"])
    Report::Event.create(status:"active",custom_id:119, name:'document attachment deleted by user',category:'', event_actions:["delete_attachment"], event_controllers:["documents"])
    Report::Event.create(status:"active",custom_id:120, name:'document attachment uploaded by user',category:'', event_actions:["upload_file"], event_controllers:["documents"])
    Report::Event.create(status:"active",custom_id:121, name:'user families listed',category:'', event_actions:["index"], event_controllers:["families"])
    Report::Event.create(status:"active",custom_id:122, name:'family details updated by user',category:'', event_actions:["update"], event_controllers:["families"])
    Report::Event.create(status:"active",custom_id:123, name:'user requested to join family',category:'', event_actions:["join_family"], event_controllers:["families"])
    Report::Event.create(status:"active",custom_id:124, name:'user search for family to join',category:'', event_actions:["search_family"], event_controllers:["families"])
    Report::Event.create(status:"active",custom_id:125, name:'family sent request to user to join',category:'', event_actions:["join_this_family"], event_controllers:["families"])
    Report::Event.create(status:"active",custom_id:126, name:'role in family selected by user',category:'', event_actions:["select_role"], event_controllers:["families"])
    Report::Event.create(status:"active",custom_id:127, name:'details get listed for family',category:'', event_actions:["show"], event_controllers:["families"])
    Report::Event.create(status:"active",custom_id:128, name:'user clicked on don\'t want to add spouse popup',category:'', event_actions:["dont_want_add_spouse"], event_controllers:["families"])
    Report::Event.create(status:"active",custom_id:129, name:'quiz level updated for child',category:'', event_actions:["update_quiz_level"], event_controllers:["child/alexa_quiz/quiz_details"])
    Report::Event.create(status:"active",custom_id:130, name:'action card dismised',category:'', event_actions:["dismiss_nutrient"], event_controllers:["users"])
    Report::Event.create(status:"active",custom_id:131, name:'all measurement records get listed',category:'', event_actions:["index"], event_controllers:["health"])
    Report::Event.create(status:"active",custom_id:132, name:'zscore details from measuremnet get listed by user',category:'', event_actions:["get_zscore_detail"], event_controllers:["health"])
    Report::Event.create(status:"active",custom_id:133, name:'measurment for(height/weight/bmi) details get listed for member',category:'', event_actions:["get_detail"], event_controllers:["health"])
    Report::Event.create(status:"active",custom_id:134, name:'measurement detail for a measurement record listed',category:'', event_actions:["get_health_detail"], event_controllers:["health"])
    Report::Event.create(status:"active",custom_id:135, name:'measurement record added by user',category:'', event_actions:["create_record"], event_controllers:["health"])
    Report::Event.create(status:"active",custom_id:136, name:'measurement record added via alexa by user',category:'', event_actions:["create_record_from_alexa"], event_controllers:["health"])
    Report::Event.create(status:"active",custom_id:137, name:'measurement record updated by user',category:'', event_actions:["update_record"], event_controllers:["health"])
    Report::Event.create(status:"active",custom_id:138, name:'measurement record deleted by user',category:'', event_actions:["delete_parameter"], event_controllers:["health"])
    Report::Event.create(status:"active",custom_id:139, name:'email unsubscribed by user',category:'', event_actions:["unsubscribe"], event_controllers:["home"])
    Report::Event.create(status:"active",custom_id:140, name:'family join request accepted',category:'', event_actions:["update_status"], event_controllers:["invitations"])
    Report::Event.create(status:"active",custom_id:141, name:'family join request rejected',category:'', event_actions:["update_status"], event_controllers:["invitations"])
    Report::Event.create(status:"active",custom_id:142, name:'recommended prenatal tests get listed',category:'', event_actions:["get_recommended_medical_event"], event_controllers:["medical_event"])
    Report::Event.create(status:"active",custom_id:143, name:'prenatal test component created',category:'', event_actions:["create_new_component"], event_controllers:["medical_event"])
    Report::Event.create(status:"active",custom_id:144, name:'component added to prenatal test',category:'', event_actions:["add_component_to_medical_event"], event_controllers:["medical_event"])
    Report::Event.create(status:"active",custom_id:145, name:'member perantal test get listed',category:'', event_actions:["index"], event_controllers:["medical_event"])
    Report::Event.create(status:"active",custom_id:146, name:'prenatal test added into system by user',category:'', event_actions:["add_recommended_test"], event_controllers:["medical_event"])
    Report::Event.create(status:"active",custom_id:147, name:'perantal test added for child',category:'', event_actions:["create"], event_controllers:["medical_event"])
    Report::Event.create(status:"active",custom_id:148, name:'prenatal test detail get listed',category:'', event_actions:["get_medical_event_detail"], event_controllers:["medical_event"])
    Report::Event.create(status:"active",custom_id:149, name:'prenatal test detail updated',category:'', event_actions:["update"], event_controllers:["medical_event"])
    Report::Event.create(status:"active",custom_id:150, name:'prenatal test document uploaded',category:'', event_actions:["upload_image"], event_controllers:["medical_event"])
    Report::Event.create(status:"active",custom_id:151, name:'details of all system component from prenatal tests get listed',category:'', event_actions:["get_sys_components"], event_controllers:["medical_event"])
    Report::Event.create(status:"active",custom_id:152, name:'component deleted from prenatal test',category:'', event_actions:["delete_component_from_medical_event"], event_controllers:["medical_event"])
    Report::Event.create(status:"active",custom_id:153, name:'system component deleted from prenatal test',category:'', event_actions:["delete_component_from_sys"], event_controllers:["medical_event"])
    Report::Event.create(status:"active",custom_id:154, name:'perantal test deleted',category:'', event_actions:["delete_parental_test"], event_controllers:["medical_event"])
    Report::Event.create(status:"active",custom_id:155, name:'new child added to family',category:'', event_actions:["create_child"], event_controllers:["member"])
    Report::Event.create(status:"active",custom_id:156, name:'child details updated',category:'', event_actions:["update_child"], event_controllers:["member"])
    Report::Event.create(status:"active",custom_id:157, name:'child deleted from family',category:'', event_actions:["delete_child"], event_controllers:["member"])
    Report::Event.create(status:"active",custom_id:158, name:'invite adult member to join family',category:'', event_actions:["create_elder"], event_controllers:["member"])
    Report::Event.create(status:"active",custom_id:159, name:'adult member info updated',category:'', event_actions:["update_elder"], event_controllers:["member"])
    Report::Event.create(status:"active",custom_id:160, name:'adult member removed from family',category:'', event_actions:["remove_elder"], event_controllers:["member"])
    Report::Event.create(status:"active",custom_id:161, name:'adult invitation to join family deleted',category:'', event_actions:["delete_elder_invitation"], event_controllers:["member"])
    Report::Event.create(status:"active",custom_id:162, name:'invitation resent to adult member to join family',category:'', event_actions:["resend_invitation"], event_controllers:["member"])
    Report::Event.create(status:"active",custom_id:163, name:'invite member joined family',category:'', event_actions:["verify_elder_for_family"], event_controllers:["member"])
    Report::Event.create(status:"active",custom_id:164, name:'invitation sent to adult member to join family',category:'', event_actions:["verify_elder"], event_controllers:["member"])
    Report::Event.create(status:"active",custom_id:165, name:'system milestones available for a child get listed',category:'', event_actions:["index"], event_controllers:["milestones"])
    Report::Event.create(status:"active",custom_id:166, name:'added milestones for child get listed',category:'', event_actions:["member_milestone_list", "memberMilestoneList"], event_controllers:["milestones"])
    Report::Event.create(status:"active",custom_id:167, name:'milestone detail get listed',category:'', event_actions:["get_milestone_detail", "milestone_detail"], event_controllers:["milestones"])
    Report::Event.create(status:"active",custom_id:168, name:'milestone added',category:'', event_actions:["create"], event_controllers:["milestones"])
    Report::Event.create(status:"active",custom_id:169, name:'milestone updated',category:'', event_actions:["update"], event_controllers:["milestones"])
    Report::Event.create(status:"active",custom_id:170, name:'milestones added(multiple)',category:'', event_actions:["add_multiple"], event_controllers:["milestones"])
    Report::Event.create(status:"active",custom_id:171, name:'milestone added( from handhold)',category:'', event_actions:["add_milestone_from_handhold"], event_controllers:["milestones"])
    Report::Event.create(status:"active",custom_id:172, name:'milestone deleted',category:'', event_actions:["destroy"], event_controllers:["milestones"])
    Report::Event.create(status:"active",custom_id:173, name:'image uploaded to milestone',category:'', event_actions:["upload_image"], event_controllers:["milestones"])
    Report::Event.create(status:"active",custom_id:174, name:'all user notification get listed',category:'', event_actions:["index"], event_controllers:["notifications"])
    Report::Event.create(status:"active",custom_id:175, name:'user notification(push/email) response added',category:'', event_actions:["mark_user_notification_responded"], event_controllers:["notifications"])
    Report::Event.create(status:"active",custom_id:176, name:'tool list get listed in manage tool',category:'', event_actions:["index"], event_controllers:["nutrients"])
    Report::Event.create(status:"active",custom_id:177, name:'tool activated from manage tool',category:'', event_actions:["acitvate"], event_controllers:["nutrients"])
    Report::Event.create(status:"active",custom_id:178, name:'tool deactivated from manage tool',category:'', event_actions:["deactivate"], event_controllers:["nutrients"])
    Report::Event.create(status:"active",custom_id:179, name:'available tools get listed',category:'', event_actions:["change_categories", "categories"], event_controllers:["nutrients"])
    Report::Event.create(status:"inactive",custom_id:180, name:'password updated',category:'', event_actions:["update", "secure_update"], event_controllers:["passwords"])
    Report::Event.create(status:"active",custom_id:181, name:'send reset password instructions',category:'', event_actions:["create"], event_controllers:["passwords"])
    Report::Event.create(status:"active",custom_id:182, name:'pregnancy healthcard get listed',category:'', event_actions:["health_card"], event_controllers:["pregnancy"])
    Report::Event.create(status:"active",custom_id:183, name:'pregnancy info get listed',category:'', event_actions:["get_preg_info"], event_controllers:["pregnancy"])
    Report::Event.create(status:"active",custom_id:184, name:'pregnancy details updated',category:'', event_actions:["update_preg"], event_controllers:["pregnancy"])
    Report::Event.create(status:"active",custom_id:185, name:'pregnancy info updated',category:'', event_actions:["update_preg_child"], event_controllers:["pregnancy"])
    Report::Event.create(status:"active",custom_id:186, name:'pregnancy predefined timelines get listed',category:'', event_actions:["get_default_timelines"], event_controllers:["pregnancy"])
    Report::Event.create(status:"active",custom_id:187, name:'pregnancy marked as completed',category:'', event_actions:["markPregnancyCompleted","mark_preg_complete"], event_controllers:["pregnancy"])
    Report::Event.create(status:"active",custom_id:188, name:'profile image added by user',category:'', event_actions:["upload_image"], event_controllers:["users"])
    Report::Event.create(status:"active",custom_id:189, name:'available subscription plan get listed',category:'', event_actions:["get_available_plans"], event_controllers:["subscription"])
    Report::Event.create(status:"active",custom_id:190, name:'subscription cancelled',category:'', event_actions:["unsubscribe_from_subscription"], event_controllers:["subscription"])
    Report::Event.create(status:"active",custom_id:191, name:'subscription receipt updated',category:'', event_actions:["update_subscription_receipt"], event_controllers:["subscription"])
    Report::Event.create(status:"active",custom_id:192, name:'checked for subscription',category:'', event_actions:["verify_subscription"], event_controllers:["subscription"])
    Report::Event.create(status:"active",custom_id:193, name:'predefined timeline get listed',category:'', event_actions:["get_pre_defined_timeline"], event_controllers:["timeline"])
    Report::Event.create(status:"active",custom_id:194, name:'user timeline get listed',category:'', event_actions:["user_timeline"], event_controllers:["timeline"])
    Report::Event.create(status:"active",custom_id:195, name:'timeline detail fetched',category:'', event_actions:["get_timeline_detail, show"], event_controllers:["timeline"])
    Report::Event.create(status:"active",custom_id:196, name:'timeline created',category:'', event_actions:["create_timeline"], event_controllers:["timeline"])
    Report::Event.create(status:"active",custom_id:197, name:'timeline created for multiple child',category:'', event_actions:["create_timeline_for_multi_member"], event_controllers:["timeline"])
    Report::Event.create(status:"active",custom_id:198, name:'timeline updated',category:'', event_actions:["update_timeline"], event_controllers:["timeline"])
    Report::Event.create(status:"active",custom_id:199, name:'image uploaded for a timeline by user',category:'', event_actions:["upload_image"], event_controllers:["timeline"])
    Report::Event.create(status:"active",custom_id:200, name:'timeline deleted',category:'', event_actions:["delete_timeline"], event_controllers:["timeline"])
    Report::Event.create(status:"inactive",custom_id:201, name:'user sign in',category:'', event_actions:["sign_in, secure_sign_in"], event_controllers:["users"])
    Report::Event.create(status:"active",custom_id:202, name:'user signin in alexa',category:'', event_actions:["secure_alexa_sign_in"], event_controllers:["users"])
    Report::Event.create(status:"active",custom_id:203, name:'user signin via facebook',category:'', event_actions:["sign_in_via_facebook"], event_controllers:["users"])
    Report::Event.create(status:"active",custom_id:204, name:'family child members list get listed',category:'', event_actions:["child_members"], event_controllers:["users"])
    Report::Event.create(status:"inactive",custom_id:205, name:'user sign out',category:'', event_actions:["user_sign_out"], event_controllers:["users"])
    Report::Event.create(status:"active",custom_id:206, name:'confirmation link resent to user',category:'', event_actions:["resend_confirmation_link"], event_controllers:["users"])
    Report::Event.create(status:"active",custom_id:207, name:'pushnotification enabled by user',category:'', event_actions:["enable_push_notification"], event_controllers:["users"])
    Report::Event.create(status:"active",custom_id:208, name:'default cover image is set',category:'', event_actions:["set_default_cover_image"], event_controllers:["users"])
    Report::Event.create(status:"active",custom_id:209, name:'cover image updated by user',category:'', event_actions:["upload_image_multipart", "upload_cover_image"], event_controllers:["users"])
    Report::Event.create(status:"active",custom_id:210, name:'list system immunisations list by user user country',category:'', event_actions:["new"], event_controllers:["vaccination"])
    Report::Event.create(status:"active",custom_id:211, name:'new immunisation added to system',category:'', event_actions:["add_vaccin"], event_controllers:["vaccination"])
    Report::Event.create(status:"active",custom_id:212, name:'jab list month wise get listed',category:'', event_actions:["jabs_month_wise"], event_controllers:["immunisation"])
    Report::Event.create(status:"active",custom_id:213, name:'immunisations listed for child',category:'', event_actions:["index"], event_controllers:["immunisation"])
    Report::Event.create(status:"active",custom_id:214, name:'new immunisation added for child',category:'', event_actions:["create"], event_controllers:["vaccination"])
    Report::Event.create(status:"active",custom_id:215, name:'immunisation updated for child',category:'', event_actions:["update_vaccination"], event_controllers:["vaccination"])
    Report::Event.create(status:"active",custom_id:216, name:'immunisation deleted for child',category:'', event_actions:["delete_vaccin"], event_controllers:["vaccination"])
    Report::Event.create(status:"active",custom_id:217, name:'immunisations detail get listed',category:'', event_actions:["jab_detail"], event_controllers:["immunisation"])
    Report::Event.create(status:"active",custom_id:218, name:'new jab added to vaccine',category:'', event_actions:["new_jab"], event_controllers:["immunisation"])
    Report::Event.create(status:"active",custom_id:219, name:'new jab with planned/administered date added to vaccine',category:'', event_actions:["save_jab"], event_controllers:["immunisation"])
    Report::Event.create(status:"active",custom_id:220, name:'jab detail updated',category:'', event_actions:["update_jab"], event_controllers:["immunisation"])
    Report::Event.create(status:"active",custom_id:221, name:'jab deleted from immunisations',category:'', event_actions:["delete_jab"], event_controllers:["immunisation"])
    
  # Events_for_clinicsetup_and_book_appointment
    Report::Event.create(status:"active",custom_id:222, name:'setup clinic for member',                                    category:'gpsoc', event_actions:["setup_clinic"], event_controllers:["clinic"])
    Report::Event.create(status:"active",custom_id:223, name:'list available clinic for member',                           category:'gpsoc', event_actions:["get_clinic_list"], event_controllers:["clinic"])
    Report::Event.create(status:"active",custom_id:224, name:'authenticate and link to clinic',                            category:'gpsoc', event_actions:["authenticate_by_clinic"], event_controllers:["clinic"])
    Report::Event.create(status:"active",custom_id:225, name:'get clinic linakble status',                                 category:'gpsoc', event_actions:["get_clinic_linkable_status"], event_controllers:["clinic"])
    Report::Event.create(status:"active",custom_id:226, name:'list services available by clinic',                          category:'gpsoc', event_actions:["get_clinic_services"], event_controllers:["clinic"])
    Report::Event.create(status:"active",custom_id:227, name:'delink with clinic account ',                                category:'gpsoc', event_actions:["delink_with_clinic"], event_controllers:["clinic"])
    Report::Event.create(status:"active",custom_id:228, name:'list user appointments',                                     category:'gpsoc', event_actions:["list_user_appointment"], event_controllers:["clinic/book_appointment"])
    Report::Event.create(status:"active",custom_id:229, name:'list available appointments for booking',                    category:'gpsoc', event_actions:["available_appointments"], event_controllers:["clinic/book_appointment"])
    Report::Event.create(status:"active",custom_id:230, name:'list available doctor from a clinic for appointment booking',category:'gpsoc', event_actions:["get_doctor_list"], event_controllers:["clinic/book_appointment"])
    Report::Event.create(status:"active",custom_id:231, name:'list purpose for appointment',                               category:'gpsoc', event_actions:["get_appointment_purpose_list"], event_controllers:["clinic/book_appointment"])
    Report::Event.create(status:"active",custom_id:232, name:'book an appointment with a doctor',                          category:'gpsoc', event_actions:["create_appointment"], event_controllers:["clinic/book_appointment"])
    Report::Event.create(status:"active",custom_id:233, name:'cancel booked appointment',                                  category:'gpsoc', event_actions:["cancel_appointment"], event_controllers:["clinic/book_appointment"])
    Report::Event.create(status:"active",custom_id:234, name:'reschedule booked appointment',                              category:'gpsoc', event_actions:["update_appointment"], event_controllers:["clinic/book_appointment"])
    Report::Event.create(status:"inactive",custom_id:235, name:'reschedule booked appointment',                              category:'gpsoc', event_actions:["update_appointment"], event_controllers:["clinic/book_appointment"])
    Report::Event.create(status:"active",custom_id:236, name:'get pregnancy week detail',                                  category:'pregnancy', event_actions:["get_pregnancy_week_detail"], event_controllers:["pregnancy"])
    Report::Event.create(status:"active",custom_id:237, name:'search gp practices in emis',                                category:'gpsoc', event_actions:["search_gp_practices"], event_controllers:["gp_soc"])
    Report::Event.create(status:"active",custom_id:238, title:'update cvtme purchase for family', name:'update cvtme subscription for family',category:'family', event_actions:["update_family_purchase_for_cvtme"], event_controllers:["child/cvtme/color_vision_test_result"])
    Report::Event.create(status:"active",custom_id:239, title:'update cvtme color vision test result', name:'update cvtme color vision test result',category:'family', event_actions:["update_family_purchase_for_cvtme"], event_controllers:["child/cvtme/result"])
    Report::Event.create(status:"active",custom_id:240, name:'user logged in successfully',category:'user', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:241, name:'user password changed  successfully',category:'user', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:242, name:'user signout successfully',category:'user', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:243, name:'user signup successfully',category:'user', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:244, title:'update cvtme purchase attempt count on expiry',           name:'update cvtme purchase attempt count on expiry',category:'family', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:245, title:'Otp generated',                                           name:'Otp generated',category:'gpsoc', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:246, title:'Otp verified',                                            name:'Otp verified',category:'gpsoc', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:247, title:'Show mental math free popup',                             name:'Show mental math free popup',category:'mental math', event_actions:["NA"], event_controllers:["NA"])
    
    Report::Event.create(status:"active",custom_id:248, name:'Get filtered content from syndicated content',             title:'Get filtered content from syndicated content',category:'nhs', event_actions:["get_filter_content"], event_controllers:["nhs/syndicated_content"])
    Report::Event.create(status:"active",custom_id:249, name:'Get category list syndicated content',                     title:'Get category list syndicated content' ,category:'nhs', event_actions:["get_category_list"], event_controllers:["nhs/syndicated_content"])
    Report::Event.create(status:"active",custom_id:250, name:'Get topic list for category list from syndicated content', title:'Get topic list for category list from syndicated content',category:'nhs', event_actions:["get_topic_list_for_category"], event_controllers:["nhs/syndicated_content"])
    Report::Event.create(status:"active",custom_id:251, name:'Show syndicated content',                                  title:'Show syndicated content',category:'nhs', event_actions:["get_content"], event_controllers:["nhs/syndicated_content"])
  end

  #  Need to followup with GP for enable GP ONlinefeature
  def event_for_gp_followup
    # Not implemented
    Report::Event.create(status:"active",custom_id:252, name:'No online account found',                                   title:'No online account found',                                 category:'emis', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:253, name:'GP not linking',                                            title:'GP not linking',                                          category:'emis', event_actions:["NA"], event_controllers:["NA"])

    # Implemented
    Report::Event.create(status:"active",custom_id:254, name:'No proxy list found for linked account',                    title:'No proxy list found for linked account',                  category:'emis', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:255, name:'No GP service enabled for fully linked with pin document',  title:'No GP service enabled for fully linked with pin document',category:'emis', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:256, name:'GP Account already exist for a pin document',               title:'GP Account already exist for a pin document',             category:'emis', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:257, name:'GP Account not found for a pin document',                   title:'GP Account not found for a pin document',                 category:'emis', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:258, name:'GP Account register with pin document failed',              title:'GP Account register with pin document failed',            category:'emis', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:259, name:'GP Account delink',                                         title:'GP Account delink',                                       category:'emis', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:260, name:'GP Account already exist for register with detail',         title:'GP Account already exist for register with detail',       category:'emis', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:261, name:'GP Account not found for register with detail',             title:'GP Account not found for register with detail',           category:'emis', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:262, name:'GP Account register with detail failed',                    title:'GP Account register with detail failed',                  category:'emis', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:263, name:'HealthReport not enabled',                                  title:'HealthReport not enabled',                              category:'emis', event_actions:["NA"], event_controllers:["NA"])

    # Not implemented
    Report::Event.create(status:"active",custom_id:264, name:'No GP found',                                               title:'No GP found',                                             category:'emis', event_actions:["NA"], event_controllers:["NA"])
    # Implemented
    Report::Event.create(status:"active",custom_id:265, name:'GP online user linking with linkage key',                   title:'GP online user linking with linkage key',                     category:'emis', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:266, name:'GP online user linking without linkage key',                title:'GP online user linking without linkage key',                  category:'emis', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:267, name:'Link child via proxy access',                               title:'Link child via proxy access',                              category:'emis', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:268, name:'Signup via NHS',                                            title:'Signup via NHS',                                           category:'gpsoc', event_actions:["NA"], event_controllers:["NA"])
    # GP Message Event
    Report::Event.create(status:"active",custom_id:269, name:'List GP Message',                                            title:'List GP Message',                          category:'gpsoc', event_actions:["list_messages"], event_controllers:["clinic/message"])
    Report::Event.create(status:"active",custom_id:270, name:'Delete GP Message',                                          title:'Delete GP Message',                          category:'gpsoc', event_actions:["delete_message"], event_controllers:["clinic/message"])
    Report::Event.create(status:"active",custom_id:271, name:'Send message to GP',                                         title:'Send message to GP',                          category:'gpsoc', event_actions:["send_message"], event_controllers:["clinic/message"])
    # GP Pharmacy Event
       
    Report::Event.create(status:"active",custom_id:272, name:'List user nominated GP pharmacy',                            title:'List user nominated GP pharmacy',                          category:'gpsoc', event_actions:["get_user_nominated_pharmacy"], event_controllers:["clinic/pharmacy"])
    Report::Event.create(status:"active",custom_id:273, name:'Update nominated GP pharmacy',                               title:'Update nominated GP pharmacy',                          category:'gpsoc', event_actions:["update_pharmacy_nomination"], event_controllers:["clinic/pharmacy"])
    Report::Event.create(status:"active",custom_id:274, name:'Delete nominated GP pharmacy',                               title:'Delete nominated GP pharmacy',                          category:'gpsoc', event_actions:["delete_pharmacy_nomination"], event_controllers:["clinic/pharmacy"])
    # GP Prescription Event

    Report::Event.create(status:"active",custom_id:275, name:'List requested prescription from GP',                        title:'List requested prescription from GP',                          category:'gpsoc', event_actions:["get_prescription_requests"], event_controllers:["clinic/prescribing"])
    Report::Event.create(status:"active",custom_id:276, name:'Request for prescription to GP ',                            title:'Request for prescription to GP ',                          category:'gpsoc', event_actions:["request_prescription"], event_controllers:["clinic/prescribing"])
    Report::Event.create(status:"active",custom_id:277, name:'List available GP prescription',                             title:'List available GP prescription',                          category:'gpsoc', event_actions:["list_medication_courses"], event_controllers:["clinic/prescribing"])
    Report::Event.create(status:"active",custom_id:278, name:'Cancel requested GP prescription',                           title:'Cancel requested GP prescription',                          category:'gpsoc', event_actions:["cancel_prescription"], event_controllers:["clinic/prescribing"])
    
    # Pregnanacy guid tile
    Report::Event.create(status:"active",custom_id:279, name:'List Nhs Guide tiles',                                       title:'List Nhs Guide tiles',                                      category:'nhs', event_actions:["get_nhs_guide_tiles"], event_controllers:["nhs/guide_tile"])
    # App refferal popup
    Report::Event.create(status:"active",custom_id:280, name:'Invite friends to nurturey',                                 title:'Invite friends to nurturey',                                category:'referral', event_actions:["invite_friend_to_nurturey"], event_controllers:["invitations"])
    Report::Event.create(status:"active",custom_id:281, name:'show app referral popup',                                    title:"show app referral popup",                                   category:'refferal', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:282, name:'skip app referral popup',                                    title:"skip app referral popup",                                   category:'refferal', event_actions:["skip_app_refferal_popup"], event_controllers:["invitations"])
    Report::Event.create(status:"active",custom_id:283, name:'mark child active',                                          title:"mark child active",                                         category:'member', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:284, name:'mark child inactive',                                        title:"mark child inactive",                                       category:'member', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:285, name:'mark pregnancy active',                                      title:"mark pregnancy active",                                     category:'pregnancy', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:286, name:'mark pregnancy inactive',                                    title:"mark pregnancy inactive",                                   category:'pregnancy', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:287, name:'Show message attachment',                                    title:'Show message attachment',                                   category:'gpsoc', event_actions:["get_message_attachment"], event_controllers:["clinic/message"])

  end
  

  def tpp_followup_event
    # Implemented
    Report::Event.create(status:"active",custom_id:290, name:'No proxy list found for linked account',                    title:'Tpp:No proxy list found for linked account',                  category:'tpp', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:291, name:'No GP service enabled for fully linked with pin document',  title:'Tpp:No GP service enabled for fully linked with pin document',category:'tpp', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:292, name:'GP Account delink',                                         title:'Tpp:GP Account delink',                                       category:'tpp', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:293, name:'HealthReport not enabled',                                  title:'Tpp:HealthReport not enabled',                                category:'tpp', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:294, name:'Link child via proxy access',                               title:'Tpp:Link child via proxy access',                             category:'tpp', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:295, name:'GP online user linking with linkage key',                   title:'Tpp:GP online user linking with linkage key',                  category:'tpp', event_actions:["NA"], event_controllers:["NA"])

    # Not Applicable
    # Report::Event.create(status:"active",custom_id:***, name:'GP Account already exist for a pin document',               title:'Tpp:GP Account already exist for a pin document',             category:'tpp', event_actions:["NA"], event_controllers:["NA"])
    # Report::Event.create(status:"active",custom_id:***, name:'GP Account not found for register with detail',             title:'Tpp:GP Account not found for register with detail',           category:'tpp', event_actions:["NA"], event_controllers:["NA"])
    # Report::Event.create(status:"active",custom_id:***, name:'GP Account not found for a pin document',                   title:'Tpp:GP Account not found for a pin document',                 category:'tpp', event_actions:["NA"], event_controllers:["NA"])
    # Report::Event.create(status:"active",custom_id:***, name:'GP Account register with detail failed',                    title:'Tpp:GP Account register with detail failed',                  category:'tpp', event_actions:["NA"], event_controllers:["NA"])
    # Report::Event.create(status:"active",custom_id:***, name:'GP Account already exist for register with detail',         title:'Tpp:GP Account already exist for register with detail',       category:'tpp', event_actions:["NA"], event_controllers:["NA"])
    # Report::Event.create(status:"active",custom_id:***, name:'No GP found',                                               title:'Tpp:No GP found',                                             category:'tpp', event_actions:["NA"], event_controllers:["NA"])
    # Report::Event.create(status:"active",custom_id:***, name:'No online account found',                                   title:'Tpp:No online account found',                                 category:'tpp', event_actions:["NA"], event_controllers:["NA"])
    # Report::Event.create(status:"active",custom_id:***, name:'GP not linking',                                            title:'Tpp:GP not linking',                                          category:'tpp', event_actions:["NA"], event_controllers:["NA"])
  end

  def nook_event
    Report::Event.create(status:"active",custom_id:296, name:'show nook popup',         title:'show nook popup',          category:'nook', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:297, name:'nook popup skipped',      title:'nook popup skipped',       category:'nook', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:298, name:'nook popup clicked',      title:'nook popup clicked',       category:'nook', event_actions:["NA"], event_controllers:["NA"])
    Report::Event.create(status:"active",custom_id:299, name:'show event list',         title:'show event list',          category:'calender', event_actions:["user_events"], event_controllers:["users"])
    Report::Event.create(status:"active",custom_id:300, name:'show timelines',          title:'show timelines',           category:'timeline', event_actions:["index"], event_controllers:["timeline"])
    Report::Event.create(status:"active",custom_id:301, name:'show vision health',      title:'show vision health',        category:'vision health', event_actions:["index"], event_controllers:["child/vision_health/color_vision_test_result"])
  end


end