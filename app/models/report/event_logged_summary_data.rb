class Report::EventLoggedSummaryData
  include Mongoid::Document
  include Mongoid::Timestamps 

  field :event_summary ,type:Hash
  field :event_log_date, type:Date
  
  def self.create_data(data_of_nth_day=0,options={})
    start_time = (Time.now - data_of_nth_day.days).beginning_of_day.to_time
    end_time = (Time.now - data_of_nth_day.days).end_of_day.to_time
    notification_date = start_time.to_date.to_s
    ::Report::EventLoggedSummaryData.where(:event_log_date=> start_time.to_date).delete_all
    event_summary =  ::Report::EventLoggedSummaryData.get_event_group_by_date({:start_date=>start_time,:end_date=>end_time,:data_to_save=>true}).last  || {} rescue {}
    
    data_to_save = {:event_log_date=> start_time.to_date,:event_summary=>event_summary }  
    ::Report::EventLoggedSummaryData.create(data_to_save)
  end
  
   def self.get_event_group_by_date(options)
    begin
      event_custom_ids = ::Report::Event.all.map(&:custom_id)
      ::Report::ApiLog.set_db
      start_date = (options[:start_date].blank? ?  (Time.now - 1.month) : options[:start_date]).to_time
      end_date = (options[:end_date].blank? ?  Time.now : options[:end_date]).to_time.end_of_day
      conditions = { :created_at => {"$gte"=>start_date,"$lte"=>end_date }  }
     
      critiria = { "$match" => conditions }
      projected_values = {"_id"=>0,"sent_date"=>"$_id"}
      group_by_value =   {
        "_id": { "$dateToString":{ format: "%Y-%m-%d", date:"$created_at" } },
        "totalCount": { "$sum": 1 }
      }
       
      event_value = {}
      event_custom_ids.each do|event_custom_id|
        projected_values[event_custom_id] = 1
        event_value[event_custom_id] = {
            "$sum"=> {
               "$cond": [ { "$eq": [ "$event_custom_id",  event_custom_id ] }, 1, 0]
            }
          }
      end
      group_by_value.merge!(event_value)
      hash_data =  ::Report::ApiLog.collection.aggregate([
         { "$match" => { :created_at => {"$gte"=>start_date,"$lte"=> (end_date + 1.day) }} },
          {"$group"=>
            group_by_value
          },
         {
           "$project"=> projected_values 
          }
         ])
        ::Report::ApiLog.reset_db
      hash_data
    rescue Exception=>e 
      ::Report::ApiLog.reset_db
      {}
    end   
  end
end