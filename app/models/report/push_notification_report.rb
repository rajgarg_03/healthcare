class Report::PushNotificationReport
              
  def self.data_for_identifier(group_by="created_at",destination=nil,selected_date=nil,params={})
    custome_destination_value = {"Measurements"=>["Measurements","measurement","measurements"], 
    "Immunisations"=>["immunisation","Immunisations"],
    "pointer"=>["pointer","pointers","Pointer","Pointers"],
    "pointers" => ["pointer","pointers","Pointer","Pointers"],
    "Manage Family"=>["Family", "manage_family","Manage_Family", "manage family","Manage Family"],
    "Home"=>["home","Home","picture"] 
     
    }
    destination_value =  {}
    (BatchNotification::BatchNotificationManager::destination_values - ["pointers", "NA"]).each do |category|
      begin
        destination_value[category] = [category,category.downcase,category.upcase]
      rescue Exception=> e 
        Rails.logger.info "Error in data_for_identifier"
        Rails.logger.info e.message
      end
    end
    destination_value.merge!(custome_destination_value)
     
    if selected_date.present?
       conditions = { group_by => {"$gte"=>selected_date.to_time, "$lte"=>selected_date.to_time + 1.day}  }
     else
        start_date = (params[:start_date].blank? ?  (Time.now - 1.month) : params[:start_date]).to_time
        end_date = (params[:end_date].blank? ?  Time.now : params[:end_date]).to_time.end_of_day
        conditions = { :created_at => {"$gte"=>start_date,"$lte"=>end_date }  }
    end


    if destination.present?
      conditions = conditions.merge!({:category=>{"$in"=>destination_value[destination]} })
    end

    #Check report for Push/Email
    if params.present? &&  params[:notify_by].present?
      if params[:notify_by] == "email"
        notify_by_values = ["email","both"]
      else 
        notify_by_values = ["push","both",nil,""]
      end
    else
      notify_by_values = ["push","both",nil,""]
    end
     
    # conditions = conditions.merge!({:notify_by=>{"$in"=>notify_by_values} }) 
 
    critiria = { "$match" => conditions }
    hash_data =  UserNotification.collection.aggregate([
     critiria,
     {"$group" => {
        "_id" =>  {"date":{"$dateToString":{ format: "%Y-%m-%d", "date": "$#{group_by}" }}, "type"=>"$type" }   ,
         "count" => {"$sum" => 1} , 
         
         "sent": {
                "$sum": {
                    "$cond": [ { "$eq": [ "$status",  "sent" ] }, 1, 0]
                }
            },

            "pending": {
                "$sum": {
                    "$cond": [ { "$eq": [ "$status",  "pending" ] }, 1, 0]
                }
            },
            "email_sent": {
                "$sum": {
                    "$cond": [ { "$eq": [ "$email_status",  "sent" ] }, 1, 0]
                }
            },

            "email_pending": {
                "$sum": {
                    "$cond": [ { "$eq": [ "$email_status",  "pending" ] }, 1, 0]
                }
            },
            "client_responded": {
                "$sum": {
                    "$cond": [ { "$eq": [ "$client_response_status",  true ] }, 1, 0]
                }
            },
            "push_response_count": {
                "$sum": {
                    "$cond": [ {"$setIsSubset"=> [  ["push"],{"$ifNull"=> ["$client_response_type",[] ]} ]  }, 1, 0]
                }
            },
            "email_response_count": {
                "$sum": {
                    "$cond": [ {"$setIsSubset"=> [ ["email"],{"$ifNull"=> ["$client_response_type",[] ]} ]  }, 1, 0]
                }
            }
      }} ,


  {
    "$group"=> {
     "_id"=> { date: "$_id.date" 
        
    },
    "categories"=> {
       "$push"=> {email_response_count:"$email_response_count", push_response_count:"$push_response_count",:client_responded=>"$client_responded",totalCount:"$totalCount", date: "$_id.date", title: "$_id.type", count: "$count",sent:"$sent",pending:"$pending", email_sent:"$email_sent",email_pending:"$email_pending" }
    }
  }
 },
  ]) 
    data = []
    hash_data.each do |a|
      date = a["_id"]["date"]
      info = {}

      totalPushResponse = 0
      totalClientResponse = 0
      totalEmailResponse = 0
      a["categories"].each do |detail|
      
        count = 0
        sent = 0
        pending = 0
        count = detail["count"].to_i + count
        sent = (detail["sent"] == 0 ? detail["email_sent"] :  detail["sent"] ).to_i + sent
        pending = (detail["email_pending"] == 0 && detail["pending"] == 0 ? 0 : (detail["pending"] > detail["email_pending"] ? detail["pending"] : detail["email_pending"])).to_i + pending
        client_responded = detail["client_responded"].to_i 
        emailResponse = detail["email_response_count"].to_i 
        pushResponse = detail["push_response_count"].to_i 
        totalClientResponse = totalClientResponse + client_responded
        totalEmailResponse = totalEmailResponse + detail["email_response_count"].to_i
        totalPushResponse = totalPushResponse + detail["push_response_count"].to_i
        
        if group_by=="created_at"
        
          info.merge!({"totalPushResponse"=>totalPushResponse,"totalEmailResponse"=>totalEmailResponse, "#{detail['title']}_email_response_count"=>emailResponse ,"#{detail['title']}_push_response_count"=>pushResponse, "totalClientResponse"=>totalClientResponse,"#{detail['title']}_client_responded"=> client_responded, "total_count"=> count, detail["title"]=>pending,"sent"=>sent,"pending"=>pending })
        else
          info.merge!({"totalPushResponse"=>totalPushResponse,"totalEmailResponse"=>totalEmailResponse,"#{detail['title']}_email_response_count"=>emailResponse ,"#{detail['title']}_push_response_count"=>pushResponse, "totalClientResponse"=>totalClientResponse,"#{detail['title']}_client_responded"=> client_responded, "total_count"=> count, detail["title"]=>sent,"sent"=>sent,"pending"=>pending })
        end
      end
      data << {date=>info}
     
    end
   data
end 
  def self.push_notification_report_data(group_by="created_at",params={})
    start_time = (params[:start_date].blank? ?  (Time.now - 1.month) : params[:start_date] ).to_time
    end_time = (params[:end_date].blank? ? Time.now : params[:end_date]).to_time
    options = params
    Report::PushNotificationReportData.where(:notification_date.gte=>(Date.today - 1.day) ).delete_all
    report_data = []
    if options[:data_to_save] != true
      if end_time < (Date.today - 1.day).to_time 
        report_data = Report::PushNotificationReportData.get_data(group_by,start_time,end_time,options)
      else
        report_data = Report::PushNotificationReportData.get_data(group_by,start_time,(end_time - 1.day),options)
      end
    end
    if params.present? &&  params[:notify_by].present?
      if params[:notify_by] == "email"
        notify_by_values = ["email","both"]
      else 
        notify_by_values = ["push","both",nil,""]
      end
    else
      notify_by_values = ["push","both",nil,""]
    end
    
    custome_destination_value = {"Measurements"=>["Measurements","measurement","measurements"], 
    "Immunisations"=>["immunisation","Immunisations"],
    "pointer"=>["pointer","pointers","Pointer","Pointers"],
    "pointers" => ["pointer","pointers","Pointer","Pointers"],
    "Manage Family"=>["Family", "manage_family","Manage_Family", "manage family","Manage Family"],
    "Home"=>["home","Home","picture"] 
    }

    destination_value =  {}
    (BatchNotification::BatchNotificationManager::destination_values - ["pointers", "NA"]).each do |category|
      begin
        destination_value[category] = [category,category.downcase,category.upcase]
      rescue Exception=> e 
        Rails.logger.info "Error in data_for_identifier"
        Rails.logger.info e.message
      end
    end
    
    destination_value.merge!(custome_destination_value)
    
    group_by_value =   {
      "_id": { "$dateToString":{ format: "%Y-%m-%d", date:"$#{group_by}" } },
      "totalCount": { "$sum": 1 },
      "sent": {
        "$sum": {
          "$cond": [ { "$eq": [ "$status",  "sent" ] }, 1, 0]
        }
      },
      
      "pending": {
        "$sum": {
          "$cond": [ { "$eq": [ "$status",  "pending" ] }, 1, 0]
        }
      },

      "email_sent": {
        "$sum": {
          "$cond": [ { "$eq": [ "$email_status",  "sent" ] }, 1, 0]
        }
      },
      
      "email_pending": {
        "$sum": {
          "$cond": [ { "$eq": [ "$email_status",  "pending" ] }, 1, 0]
        }
      },

      "client_responded": {
        "$sum": {
          "$cond": [ { "$eq": [ "$client_response_status",  true ] }, 1, 0]
        }
      },
     "push_response_count": {
          "$sum": {
              "$cond": [ {"$setIsSubset"=> [["push"], {"$ifNull"=> ["$client_response_type",[] ]} ]  }, 1, 0]
          }
      },
      "email_response_count": {
          "$sum": {
              "$cond": [ {"$setIsSubset"=> [ ["email"],{"$ifNull"=> ["$client_response_type",[] ]} ]  }, 1, 0]
          }

      }    
    }

    tool_values = {} 
   
    projected_values = {"email_sent"=>1,"email_pending"=>1, "email_response_count"=>1,"push_response_count"=>1, "_id"=>0,"sent_date"=>"$_id","client_responded"=>1,"totalCount"=>1,"sent"=>1,"pending"=>1}
    destination_value.each do |k,v|
      key_name = (k).split(" ").join("_")
      projected_values[key_name] = 1
      projected_values[key_name + "_pending"] = 1
      projected_values[key_name + "_pending_email"] = 1
      projected_values[key_name + "_sent_email"] = 1
      
      tool_values[key_name + "_pending"] =  {
        "$sum"=> {
          "$cond"=> [
            {"$and"=> [{"$setIsSubset"=> [ ["$category"], v ]  }, {"$eq"=>[ "$status",  "pending" ] }]} , 1, 0]
        }
      }
      tool_values[key_name] = {
        "$sum"=> {
          "$cond"=> [
            {"$and"=> [{"$setIsSubset"=> [ ["$category"], v ]  }, {"$eq"=>[ "$status",  "sent" ] }]} , 1, 0]
        }
      }
      tool_values[key_name + "_pending_email"] =  {
        "$sum"=> {
          "$cond"=> [
            {"$and"=> [{"$setIsSubset"=> [ ["$category"], v ]  }, {"$eq"=>[ "$email_status",  "pending" ] }]} , 1, 0]
        }
      }
      tool_values[key_name + "_sent_email"] = {
        "$sum"=> {
          "$cond"=> [
            {"$and"=> [{"$setIsSubset"=> [ ["$category"], v ]  }, {"$eq"=>[ "$email_status",  "sent" ] }]} , 1, 0]
        }
      }

    end
    group_by_value.merge!(tool_values)
    
    data_for_last_2_days = []
    if end_time >= (Date.today - 1.days) || options[:data_to_save] == true
      end_time = (end_time + 1.day).to_time if options[:data_to_save] != true
      if start_time < Date.today - 1.days && options[:data_to_save] != true
        start_time = (Date.today - 1.days).to_time
      end
      data_for_last_2_days = UserNotification.collection.aggregate([
        { "$match" => { :created_at => {"$gte"=>start_time,"$lte"=> (end_time + 1.day) }} },
        {"$group"=>
          group_by_value

        },
        {
          "$project"=> projected_values 
        }
      ])

    end
     
    (data_for_last_2_days + report_data) rescue data_for_last_2_days
  end



end