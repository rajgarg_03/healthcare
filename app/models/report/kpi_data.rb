class Report::KpiData
  include Mongoid::Document
  include Mongoid::Timestamps

  field :active_user,                  type:Integer
  field :inactive_user,                type:Integer
  field :defunct_user,                 type:Integer
  field :new_user,                     type:Integer
  field :active_family,                type:Integer
  field :inactive_family,              type:Integer
  field :new_family,                   type:Integer
  field :defunct_family,               type:Integer
  field :new_subscription,             type:Integer
  field :active_subscription,          type:Integer
  field :expire_subscription,          type:Integer
  field :cancelled_subscription,       type:Integer
  field :report_data,                  type:Hash
  field :created_date,                 type:Date

  def self.report_data(start_date=nil,end_date=nil)
    data = {}
    start_date = start_date || Date.today - 7.day
    end_date = end_date || Date.today
    # end_date = Date.today
    # options = {:start_date=>start_date.to_date.beginning_of_day,:end_date=>end_date.to_date.end_of_day}
    # # data[:active_user] = Report::ApiLog.get_log_event(custom_id=1,nil,nil,options).count 
    # data[:total_active_user] = User.where(:activity_status=>"Active").count
    # data[:active_user] = data[:total_active_user]
    
    # data[:new_signup_user] = Report::ApiLog.get_log_event(custom_id=243,nil,nil,options).count
    

    # data[:lost_active_user] = Report::ApiLog.get_log_event(custom_id=2,nil,nil,options).count
    # free_subscription_manager = SubscriptionManager.free_subscription
    # premium_subscription_ids = SubscriptionManager.premium_subscriptions.pluck(:id)
    # # kpi_data.new_subscription = Subscription.where(:subscription_manager_id.in=>premium_subscription_ids,:created_at.gte=>today_date).count
    # data[:active_subscription] = Subscription.not_expired.where(:subscription_manager_id.ne=>free_subscription_manager.id).count
    # data[:new_subscription] = Report::ApiLog.get_log_event(custom_id=28,nil,nil,options).count
    # data[:lost_subscription] = Report::ApiLog.get_log_event(custom_id=26,nil,nil,options).count
    
    # users_signup_in_3_month = User.where(:created_at.gte=>Date.today-3.month,:status.in=>["approved","confirmed"]).pluck(:id)
    # if users_signup_in_3_month.count > 0 
    #   new_subscription_event_in_3_month = Report::ApiLog.get_log_event(custom_id=28,nil,nil,{:start_date=>Date.today-3.month,:end_date=>Date.today}) 
    #   new_user_with_subscription = new_subscription_event_in_3_month.map(&:user_id).map(&:to_s) & users_signup_in_3_month.map(&:to_s)
    #   data[:percentage_conversion] = new_user_with_subscription.count.to_f/users_signup_in_3_month.count
    # else
    #   data[:percentage_conversion] = 0
    # end
    data = ::Report::KpiData.report_data_v1#(start_date,end_date)

    data
  end


  def self.data_on(date,options={})
    data = ::Report::KpiData.report_data_v1(date-7.day, date)
  end

   def self.report_data_v1(start_date=nil,end_date=nil)
    data = {}
    start_date = start_date || Date.today - 7.day
    created_date = end_date || Date.today
    end_date =   end_date || Date.today - 1.day
    may_1st_onward = "01-05-2020".to_date #Date.today - 6.month
    kpi_data = ::Report::KpiData.where(:created_date=>{"$gte"=>created_date, "$lt"=>created_date + 1.day}).last
    return kpi_data.report_data if kpi_data.present?
    country_code = Member.sanitize_country_code("uk") #["uk","UK","gb","GB"]
    user_country_code = "uk"
    member_country_code = "uk"
    options = {:start_date=>start_date.to_date.beginning_of_day,:end_date=>end_date.to_date.end_of_day}
    
    created_at_filter_for_week = {"$gte"=>start_date,"$lt"=>created_date}
    created_at_filter_1st_may = {"$gte"=>may_1st_onward,"$lt"=>created_date}
    # Signup Data
    last_6_month_signup = User.where(:country_code.in=>country_code).where(:created_at=>created_at_filter_1st_may ).only(:id).count
    last_week_signup = User.where(:country_code.in=>country_code).where(:created_at=>created_at_filter_for_week).only(:id).count
    delete_user_in_last_7_day = System::DeletedUserDetail.where(:country_code.in=>country_code).where(:created_at=>created_at_filter_for_week).only(:id).count
    user_verified_account_deleted_in_last_7_day = System::DeletedUserDetail.where(:account_status.in=>[User::STATUS[:approved],User::STATUS[:confirmed]]).where(:country_code.in=>country_code).where(:created_at=>created_at_filter_for_week).only(:id).count
    
    last_6_month_signup_with_account_verified = User.where(:country_code.in=>country_code).where(:status.in=>[User::STATUS[:approved],User::STATUS[:confirmed]]).where(:created_at=>created_at_filter_1st_may).only(:id).count
    last_week_signup_with_verified_account = User.where(:country_code.in=>country_code).where(:status.in=>[User::STATUS[:approved],User::STATUS[:confirmed]]).where(:created_at=>created_at_filter_for_week).only(:id).count
    
    data["signup"] = {user_verified_account_deleted_in_last_7_day:user_verified_account_deleted_in_last_7_day, last_week_signup_with_verified_account:last_week_signup_with_verified_account, last_6_month_signup_with_account_verified:last_6_month_signup_with_account_verified, last_6_month_signup:last_6_month_signup,:last_week=>last_week_signup,:delete_user_in_last_week=>delete_user_in_last_7_day}
    
    # Active user
    # total_active_user = Report::ApiLog.get_log_event(custom_id=1,nil,nil,{:get_count=>true}) 
    total_active_user = User.where(:country_code.in=>country_code).where(activity_status_v2:"Active").where(:created_at=>created_at_filter_1st_may).only(:id).count 
    last_6_month_active_user_with_account_verified = User.where(:country_code.in=>country_code).where(:activity_status_v2=>"Active").where(:status.in=>[User::STATUS[:approved],User::STATUS[:confirmed]]).where(:created_at=>created_at_filter_1st_may).count
    
    last_week_active_user =                User.where(:country_code.in=>country_code).where(:activity_status_v2=>"Active").where(:created_at=>created_at_filter_for_week).count
    last_week_user_with_account_verified = User.where(:country_code.in=>country_code).where(:activity_status_v2=>"Active").where(:status.in=>[User::STATUS[:approved],User::STATUS[:confirmed]]).where(:created_at=>created_at_filter_for_week).count
    
    active_mark_user_ids_in_this_week = ::Report::ApiLog.get_log_event(custom_id=1,nil,nil,{:user_country_code=>"uk", :start_date=>start_date,:end_date=>end_date,:selected_field=>"user_id"}).map(&:user_id)
    inactive_mark_user_ids_in_this_week  = ::Report::ApiLog.get_log_event(custom_id=2,nil,nil,{:user_country_code=>"uk", :start_date=>start_date,:end_date=>end_date,:selected_field=>"user_id"}).map(&:user_id)
    # active_mark_user_ids_in_last_week = ::Report::ApiLog.get_log_event(custom_id=1,nil,nil,{:user_country_code=>"uk", :start_date=>start_date  - 7.day,:end_date=>start_date,:selected_field=>"user_id"}).map(&:user_id)
    # inactive_mark_user_ids_in_last_week = ::Report::ApiLog.get_log_event(custom_id=2,nil,nil,{:user_country_code=>"uk", :start_date=>start_date - 7.day ,:end_date=>start_date,:selected_field=>"user_id"}).map(&:user_id)
    # Active to inactive user
    for_user_churn_positive = User.where(:id.in=>active_mark_user_ids_in_this_week).where(:created_at.lt=>start_date).only(:id).count #(active_mark_user_ids_in_this_week & inactive_mark_user_ids_in_last_week).count
    for_user_churn_negative = User.where(:id.in=>inactive_mark_user_ids_in_this_week).where(stage:'customer').only(:id).count #(active_mark_user_ids_in_last_week & inactive_mark_user_ids_in_this_week).count
    for_user_net_churn = for_user_churn_positive - for_user_churn_negative


    active_user_in_2nd_last_week = nil #::Report::ApiLog.get_log_event(custom_id=1,nil,nil,{:user_country_code=>user_country_code,:start_date=>(start_date - 7.days),:end_date=>start_date,:get_count=>true}) 
    
   
    data["active_user"] = {net_churn:for_user_net_churn, churn_negative:for_user_churn_negative, churn_positive:for_user_churn_positive,last_week_user_with_account_verified:last_week_user_with_account_verified, last_6_month_active_user_with_account_verified:last_6_month_active_user_with_account_verified, active_user_in_6_month:total_active_user,:last_week_active_user=>last_week_active_user}
    
    # ................................ Inactive user ................................
    total_inactive_user = nil #User.where(activity_status_v2:"Inactive").where(:country_code.in=>country_code).only(:id).count 
    total_inactive_user_in_last_6_month = User.where(activity_status_v2:"Inactive",stage:"customer").where(:country_code.in=>country_code).where(:created_at=>created_at_filter_1st_may).only(:id).pluck(:id).count 
    total_inactive_prospect_user_in_last_6_month = User.where(activity_status_v2:"Inactive",stage:"prospect").where(:country_code.in=>country_code).where(:created_at=>created_at_filter_1st_may).only(:id).pluck(:id).count 
    # user_stage_marked_customer_ids = ::Report::ApiLog.get_log_event(custom_id=9,nil,nil,{:user_country_code=>"uk", :start_date=>may_1st_onward,:end_date=>created_date,:selected_field=>"user_id"}).map(&:user_id)
    
    # inactive_user_mark_in_this_week = Report::ApiLog.get_log_event(custom_id=2,nil,nil,{:user_country_code=>user_country_code,:start_date=>start_date,:end_date=>end_date,:selected_field=>"user_id"}).map(&:user_id)
    # inactive_user_mark_in_last_week = Report::ApiLog.get_log_event(custom_id=2,nil,nil,{:user_country_code=>user_country_code,:start_date=>start_date - 7.day ,:end_date=>start_date,:selected_field=>"user_id"}).map(&:user_id)
    last_week_inactive_user = nil #Report::ApiLog.get_log_event(custom_id=2,nil,nil,{:user_country_code=>user_country_code,:start_date=>start_date,:end_date=>end_date,:get_count=>true}) 
    
    inactive_user_in_last_6_month_with_verified_account = nil #User.where(:status.in=>[User::STATUS[:approved],User::STATUS[:confirmed]]).where(activity_status_v2:"Inactive").where(:country_code.in=>country_code).where(:created_at=>created_at_filter_1st_may).only(:id).count 
    total_inactive_user_with_verified_account = nil #User.where(:status.in=>[User::STATUS[:approved],User::STATUS[:confirmed]]).where(activity_status_v2:"Inactive").where(:country_code.in=>country_code).only(:id).count 
        

    inactive_user_in_2nd_last_week = nil #Report::ApiLog.get_log_event(custom_id=2,nil,nil,{:user_country_code=>user_country_code,:start_date=>(start_date - 7.days),:end_date=>start_date,:get_count=>true}) 
    data["inactive_user"] = {total_inactive_prospect_user_in_last_6_month:total_inactive_prospect_user_in_last_6_month, total_inactive_user_with_verified_account:total_inactive_user_with_verified_account, inactive_user_in_last_6_month_with_verified_account:inactive_user_in_last_6_month_with_verified_account, total_inactive_user_in_last_6_month:total_inactive_user_in_last_6_month, total:total_inactive_user,:last_week=>last_week_inactive_user}

    #.................................. Family ........................................
    total_active_family_in_last_6_month = Family.where(activity_status_v2:"Active").where(:country_code.in=>country_code).where(:created_at=>created_at_filter_1st_may).count 
    last_week_active_family =  Family.where(activity_status_v2:"Active").where(:country_code.in=>country_code).where(:created_at=>created_at_filter_for_week).count 
    
    total_family_in_last_6_month = Family.where(:country_code.in=>country_code).where(:created_at=>created_at_filter_1st_may).count 
    last_week_added_family =  Family.where(:country_code.in=>country_code).where(:created_at=>created_at_filter_for_week).count 
    

    active_marked_family_ids_in_this_week = ::Report::ApiLog.get_log_event(custom_id=13,nil,nil,{:family_country_code=>"uk", :start_date=>start_date,:end_date=>end_date,:selected_field=>"family_id"}).map(&:family_id).uniq
    active_marked_family_ids_in_last_week = ::Report::ApiLog.get_log_event(custom_id=13,nil,nil,{:family_country_code=>"uk", :start_date=>start_date  - 7.day,:end_date=>start_date,:selected_field=>"family_id"}).map(&:family_id).uniq
    
    inactive_marked_family_ids_in_this_week  = ::Report::ApiLog.get_log_event(custom_id=14,nil,nil,{:family_country_code=>"uk", :start_date=>start_date,:end_date=>end_date,:selected_field=>"family_id"}).map(&:family_id).uniq
    # inactive_marked_family_ids_in_last_week = ::Report::ApiLog.get_log_event(custom_id=14,nil,nil,{:family_country_code=>"uk", :start_date=>start_date - 7.day,:end_date=>start_date,:selected_field=>"family_id"}).map(&:family_id).uniq
   
    # Active to inactive Family
    for_family_churn_positive = Family.where(:id.in=>active_marked_family_ids_in_this_week).where(:created_at.lt=>start_date).only(:id).count
    for_family_churn_negative = Family.where(:id.in=>inactive_marked_family_ids_in_this_week).where(:created_at.lt=>start_date).only(:id).count
    
    for_family_net_churn = for_family_churn_positive - for_family_churn_negative
    family_deleted_in_last_week = ::Report::ApiLog.get_log_event(custom_id=36,nil,nil,{:family_country_code=>"uk", :start_date=>start_date,:end_date=>end_date,:get_count=>true})

    data["active_family"] = {family_deleted_in_last_week:family_deleted_in_last_week, last_week_added_family: last_week_added_family ,total_family_in_last_6_month:total_family_in_last_6_month, net_churn:for_family_net_churn, churn_negative:for_family_churn_negative, churn_positive:for_family_churn_positive,total_active_family_in_last_6_month:total_active_family_in_last_6_month, :last_week_active_family=>last_week_active_family}

    # ..............................................Active Kid.............................
    total_kid_in_last_6_month = ChildMember.where(:country_code.in=>country_code).where(:created_at=>created_at_filter_1st_may).where(pregnancy_status:false).count 
    total_active_kid_in_last_6_month = ChildMember.where(:country_code.in=>country_code).where(:created_at=>created_at_filter_1st_may).where(pregnancy_status:false, activity_status_v2:"Active" ).count 
    new_kid_added_in_last_week = ChildMember.where(:country_code.in=>country_code).where(:created_at=>created_at_filter_for_week).where(pregnancy_status:false, activity_status_v2:"Active" ).count 
    
    kid_deleted_in_last_week = ::Report::ApiLog.get_log_event(custom_id=157,nil,nil,{:member_country_code=>"uk", :start_date=>start_date,:end_date=>end_date,:get_count=>true})
    total_active_kid_in_this_week = Report::ApiLog.get_log_event(custom_id=283,nil,nil,{:member_country_code=>member_country_code,:start_date=>start_date,:end_date=>end_date,:selected_field=>"member_id"}).map(&:member_id) 
    total_inactive_kid_in_this_week = Report::ApiLog.get_log_event(custom_id=284,nil,nil,{:member_country_code=>member_country_code,:start_date=>start_date,:end_date=>end_date,:selected_field=>"member_id"}).map(&:member_id)
    total_active_kid_in_last_week = Report::ApiLog.get_log_event(custom_id=283,nil,nil,{:member_country_code=>member_country_code,:start_date=>(start_date - 7.day) ,:end_date=>start_date,:selected_field=>"member_id"}).map(&:member_id) 
    total_inactive_kid_in_last_week = Report::ApiLog.get_log_event(custom_id=284,nil,nil,{:member_country_code=>member_country_code,:start_date=>start_date - 7.day ,:end_date=>start_date,:selected_field=>"member_id"}).map(&:member_id)
  
    for_kid_churn_positive = total_active_kid_in_this_week.count #(total_active_kid_in_this_week & total_inactive_kid_in_last_week).count
    for_kid_churn_negative = total_inactive_kid_in_this_week.count# (total_active_kid_in_last_week & total_inactive_kid_in_this_week).count
    for_kid_net_churn = for_kid_churn_positive - for_kid_churn_negative

    data["kid"] =  {kid_deleted_in_last_week:kid_deleted_in_last_week, new_kid_added_in_last_week:new_kid_added_in_last_week, total_kid_in_last_6_month:total_kid_in_last_6_month, total_active_kid_in_last_6_month:total_active_kid_in_last_6_month,  churn_positive:for_kid_churn_positive, churn_negative:for_kid_churn_negative, net_churn:for_kid_net_churn}

  # ........................................... Active Pregnancy ..................................
    
    total_pregnancy_ids_in_last_6_month = ChildMember.where(:country_code.in=>country_code).where(:created_at=>created_at_filter_1st_may).where(pregnancy_status:true).only(:id).pluck(:id) 
    total_active_pregnancy_ids_in_last_6_month = ChildMember.where(:country_code.in=>country_code).where(:created_at=>created_at_filter_1st_may).where(pregnancy_status:true, activity_status_v2:"Active" ).only(:id).pluck(:id)
    
    total_pregnancy_in_last_6_month = Pregnancy.where(:expected_member_id.in=>total_pregnancy_ids_in_last_6_month, status:"expected").only(:id).count
    total_active_pregnancy_in_last_6_month = Pregnancy.where(:expected_member_id.in=>total_active_pregnancy_ids_in_last_6_month, status:"expected").only(:id).pluck(:id).count
    total_pregnancy_completed_in_last_6_month = Pregnancy.where(:expected_member_id.in=>total_pregnancy_ids_in_last_6_month, status:"completed").only(:id).count
    pregnancy_deleted_in_last_week = ::Report::ApiLog.get_log_event(custom_id=38,nil,nil,{:member_country_code=>"uk", :start_date=>start_date,:end_date=>end_date,:get_count=>true})
    
    pregnancy_added_in_last_week = ChildMember.where(:country_code.in=>country_code).where(:created_at=>created_at_filter_for_week).where(pregnancy_status:true).only(:id).count
    # Event not logged so using pregnancy record . will activate event query in 15 day
    # pregnancy_completed_in_last_week = ::Report::ApiLog.get_log_event(custom_id=187,nil,nil,{:member_country_code=>member_country_code,:start_date=>start_date,:end_date=>end_date,:get_count=>true}) 
    pregnancy_completed_in_last_week_ids = Pregnancy.where(:updated_at=>created_at_filter_for_week).where(status:"completed").only(:expected_member_id).pluck(:expected_member_id)
    pregnancy_completed_in_last_week = ChildMember.where(:id.in=>pregnancy_completed_in_last_week_ids).where(:country_code.in=>country_code).only(:id).count
    
    total_active_pregnancy_in_this_week = Report::ApiLog.get_log_event(custom_id=285,nil,nil,{:member_country_code=>member_country_code,:start_date=>start_date,:end_date=>end_date,:selected_field=>"member_id"}).map(&:member_id) 
    total_inactive_pregnancy_in_this_week = Report::ApiLog.get_log_event(custom_id=286,nil,nil,{:member_country_code=>member_country_code,:start_date=>start_date,:end_date=>end_date,:selected_field=>"member_id"}).map(&:member_id)
    # total_active_pregnancy_in_last_week = Report::ApiLog.get_log_event(custom_id=285,nil,nil,{:member_country_code=>member_country_code,:start_date=>(start_date - 7.day) ,:end_date=>start_date,:selected_field=>"member_id"}).map(&:member_id) 
    # total_inactive_pregnancy_in_last_week = Report::ApiLog.get_log_event(custom_id=286,nil,nil,{:member_country_code=>member_country_code,:start_date=>start_date - 7.day ,:end_date=>start_date,:selected_field=>"member_id"}).map(&:member_id)
  
    for_pregnancy_churn_positive = total_active_pregnancy_in_this_week.count #(total_active_pregnancy_in_this_week & total_inactive_pregnancy_in_last_week).count
    for_pregnancy_churn_negative = total_inactive_pregnancy_in_this_week.count #(total_active_pregnancy_in_last_week & total_inactive_pregnancy_in_this_week).count
    for_pregnancy_net_churn = for_pregnancy_churn_positive - for_pregnancy_churn_negative



    data["pregnancy"] = {pregnancy_deleted_in_last_week:pregnancy_deleted_in_last_week, pregnancy_completed_in_last_week:pregnancy_completed_in_last_week, total_pregnancy_in_last_6_month:total_pregnancy_in_last_6_month, active_pregnancy_in_last_6_month:total_active_pregnancy_in_last_6_month, pregnancy_added_in_last_week:pregnancy_added_in_last_week, total_pregnancy_completed_in_last_6_month:total_pregnancy_completed_in_last_6_month, churn_positive:for_pregnancy_churn_positive, churn_negative:for_pregnancy_churn_negative, net_churn:for_pregnancy_net_churn}
    
    # .............................NHS Login signup ...........................
      total_nhs_signup = User.where(provider:"nhs",:created_at.lt=>created_date).count
      last_week_nhs_signup = User.where(provider:"nhs",:created_at=>created_at_filter_for_week).count
      data["nhs_signup"] = {total:total_nhs_signup,:last_week=>last_week_nhs_signup}
    
    # ............................NHS login detail ..................................
      total_nhs_login = ::Nhs::LoginDetail.where(:created_at.lt=>created_date).count
      last_week_nhs_login = ::Nhs::LoginDetail.where(:created_at=>created_at_filter_for_week).count
      data["nhs_login"] = {total:total_nhs_login,:last_week=>last_week_nhs_login}
 
    # ............................GP links...........................................
      total_gp_link = ::Clinic::LinkDetail.where(:created_at.lt=>created_date).count
      new_added_last_week = ::Clinic::LinkDetail.where(:created_at=>created_at_filter_for_week).count
      tpp_delink_account_in_last_week = ::Report::ApiLog.get_log_event(custom_id=292,nil,nil,{:member_country_code=>member_country_code,:start_date=>start_date ,:end_date=>start_date,:get_count=>true})
      emis_delink_account_in_last_week = ::Report::ApiLog.get_log_event(custom_id=259,nil,nil,{:member_country_code=>member_country_code,:start_date=>start_date ,:end_date=>start_date,:get_count=>true})
      
      data["gp_link"] = {total:total_gp_link,:new_added_last_week=>new_added_last_week,:tpp_delink_account_in_last_week=>tpp_delink_account_in_last_week,emis_delink_account_in_last_week:emis_delink_account_in_last_week }
 
    # .............................Proxy links........................................
      total_proxy_link = ::Clinic::LinkDetail.where("link_info.proxy_user_status"=>true).where(:created_at.lt=>created_date).count
      new_proxy_linking_last_week = ::Clinic::LinkDetail.where("link_info.proxy_user_status"=>true,:created_at=>created_at_filter_for_week).count
      data["proxy_link"] = {total:total_proxy_link,:new_proxy_linking_last_week=>new_proxy_linking_last_week}
      data[:created_date] = created_date
      kpi_data = ::Report::KpiData.new(report_data:data,:created_date=>created_date).save
    data
  end

  # ::Report::KpiData.save_report_data(start_date)
  def self.save_report_data(start_date)
    start_date = start_date || Date.today
    report_date = start_date
    data = ::Report::KpiData.report_data_v1(start_date - 7.day, start_date)
    # kpi_data = ::Report::KpiData.new(report_data:data,:created_date=>report_date).save
    kpi_data
  end



  def self.list_event_data(start_date=nil,end_date=nil,options={})
    data = {}
    start_date = start_date || Date.today - 7.day
    end_date = end_date || Date.today
    end_date = Date.today
    options = {:start_date=>start_date.to_date.beginning_of_day,:end_date=>end_date.to_date.end_of_day}
    
  end

  def self.save_report_data1
    today_date = Date.today-1.day
    kpi_data = ::Report::KpiData.new
    kpi_data.active_user = User.where(:activity_status_v2=>"Active").count
    kpi_data.inactive_user = User.where(:activity_status_v2.ne=>"Inactive").count
    kpi_data.defunct_user = User.where(:activity_status_v2.ne=>"Defunct").count
    kpi_data.new_user = User.where(:created_at.gte=>today_date).count
    
    kpi_data.active_family = Family.where(:activity_status_v2=>"Active").count
    kpi_data.inactive_family = Family.where(:activity_status_v2.ne=>"Inactive").count
    kpi_data.new_family = Family.where(:created_at.gte=>today_date).count
    kpi_data.defunct_family = Family.where(:activity_status_v2.ne=>"Defunct").count
    

    free_subscription_manager = SubscriptionManager.free_subscription
    premium_subscription_ids = SubscriptionManager.premium_subscriptions.pluck(:id)
    kpi_data.new_subscription = Subscription.where(:subscription_manager_id.in=>premium_subscription_ids,:created_at.gte=>today_date).count
    kpi_data.active_subscription = Subscription.not_expired.where(:subscription_manager_id.ne=>free_subscription_manager.id).count
    kpi_data.expire_subscription  = Subscription.expired_subscription.count
    kpi_data.cancelled_subscription = Subscription.where(subscription_valid_certificate:false).count
    kpi_data.created_date = today_date
    kpi_data.save
  end

  def self.filter_user(current_user,options={})
    options[:first_name] = nil if options[:first_name] == "Name"
    options[:email] = nil if options[:email] == "Email"
    options[:status] = nil if options[:status] == "Status"
    if options[:country_code].blank?
      options[:country_code] = nil  
    else
      options[:country_code] = options[:country_code].map{|code| eval(code)}.flatten
    end

    if options[:activity_status].blank?
      options[:activity_status] = nil  
    else
      options[:activity_status] = options[:activity_status]
    end

    if options[:segment].blank?
      options[:segment] = nil  
    else
      options[:segment] = options[:segment]
    end
    if options[:user_type].blank?
      options[:user_type] = nil  
    else
      options[:user_type] = options[:user_type]
    end
    
    if options[:stage].blank?
      options[:stage] = nil  
    else
      options[:stage] = options[:stage]
    end
    if options[:email_domain_status].blank?
      options[:email_domain_status] = nil  
    else
      options[:email_domain_status] = options[:email_domain_status]
    end

    data = {:email_domain_status=>options[:email_domain_status],:user_type=>options[:user_type], :stage=> options[:stage], :segment=>options[:segment], :activity_status=> options[:activity_status], :first_name=>options[:first_name], :email=>options[:email], :status=>options[:status] ,:country_code=>options[:country_code]}
    
    data = data.reject{|k,v| v.blank?}
    if options[:page].blank?
      session[:data] = data 
    else
      data = session[:data]
    end
    options[:first_name] = data[:first_name]
    first_query =  false
    if data.present?
      data.each do |k,v|
        if k.to_s == "country_code"
          val = (v)
        else
          optRegexp = []
          [v].flatten.each do |opt|
            if k == :first_name || k == :email
              optRegexp.push(/#{opt}/i) 
            else
              optRegexp.push(/^#{opt}/i) 
            end
          end
          val = optRegexp
        end
        if first_query
          if k == :first_name || k == :email
            @users = @users.where(k=>val[0])
          else
            @users = @users.in(k=>val)
          end
        else

          first_query = true
          if k == :first_name || k == :email
            @users = User.where(k=>val[0])
          else
            @users = User.in(k=>val)
          end
        end
      end 
    else
      @users =  nil
    end
    if options[:user_subscription].present?
      members_with_premium_subscription = []
      free_subscription_member_ids = []
      subscriptions = options[:user_subscription].deep_copy
        subscription_manager_for_free = SubscriptionManager.free_subscription
      if subscriptions.include?(subscription_manager_for_free.title)
        members_with_premium_subscription = Subscription.where(:subscription_manager_id.ne=>subscription_manager_for_free).only(:member_id).pluck(:member_id)
        subscriptions.delete(subscription_manager_for_free.title)
        free_subscription_member_ids = Member.where(:id.nin=>members_with_premium_subscription).pluck(:id) #if members_with_premium_subscription.present?
      end 
      if subscriptions.present?
        subscription_manager_id = SubscriptionManager.where(:title.in=>subscriptions).pluck(:id)
        members_with_premium_subscription = Subscription.where(:subscription_manager_id.in=>subscription_manager_id).only(:member_id).pluck(:member_id)
      end
      subscription_selected_member_ids = free_subscription_member_ids + members_with_premium_subscription
      @users = (@users|| User).where(:member_id.in=>subscription_selected_member_ids) #if members_with_premium_subscription.present?
    end
    @users      
  end
end