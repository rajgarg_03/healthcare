class Report::UserBasicSummaryReportData
  include Mongoid::Document
  include Mongoid::Timestamps 
  
  field :report_data ,type:Hash
  field :country_list, type:Array
  field :analytic_data,type: Hash
  field :total_count_data,type: Hash

  def fetch_data
    @report = []
    @all_countries = []
    all_user_countries = valid_country_list
    @countries = User.distinct(:country_code)
    all_user_countries[nil] = [nil,""]  if (!all_user_countries.include?(nil) && !all_user_countries.include?(""))
    all_user_countries.each do |country_code,country_name|
      begin
        @count_data = []
        if country_code.blank?
          iso_country_name = country_code
        else
          iso_country_name = (ISO3166::Country[country_code].data["name"] || country_code) rescue country_code
        end
        @all_countries << iso_country_name 
        all_user_ids = User.in(country_code: country_name ).only(:id).pluck("id")
        all_user_ids.each_slice(2000) do |user_ids|
          member_ids = ParentMember.in(user_id: user_ids ).pluck("id")
          family_ids = Family.in(member_id: member_ids ).pluck("id")
          children = FamilyMember.in(family_id: family_ids).in(role: ["Son","Daughter"]).pluck("member_id").flatten.uniq
          milestones =   Milestone.in(member_id:children).pluck(:id)
          measurements = Health.in(member_id:children).pluck(:id)
          immunisations = Vaccination.where({'member_id' => { "$in" => children} }).where(opted: "true").pluck("id")
          documents = Document.where({'member_id' => { "$in" => children} }).pluck("id")
          jabs = Jab.in(vaccination_id:immunisations).count

          timelines = Timeline.where({'member_id' => { "$in" => children} }).pluck("id") 
          picture_count = 0
          picture_count = picture_count + Picture.in(imageable_id: immunisations).pluck("id").count
          picture_count =  picture_count + Picture.in(imageable_id: milestones  ).pluck("id").count
          picture_count = picture_count + Picture.in(imageable_id: member_ids ).pluck("id").count
          picture_count = picture_count + Picture.in(imageable_id: children ).pluck("id").count
          picture_count = picture_count + Picture.in(imageable_id: timelines ).pluck("id").count
          @count_data << {documents: documents.count, jabs: jabs, users:user_ids.count,   pics:picture_count, immunisations: immunisations.count,timelines:timelines.count, children: children.count,milestones:milestones.count,measurements:measurements.count }
        end
        report_data = {}     
        @count_data.each do |data|
          data.each do |key,val|
            report_data[key] = report_data[key].to_i + val
          end
        end
        report_data[:country] = iso_country_name
        
        @report << report_data  
        Report::UserBasicSummaryReportData.where(created_at.lt=>(Date.today - 3.week) ).delete_all
      rescue Exception=>e 
         Rails.logger.info e.message   
      end
    end
    analytic_data = {}
    analytic_data["users_with_at_least_one_family"] =  Family.all.pluck(:member_id).uniq.count rescue 0
    analytic_data["number_of_families"] =  Family.count
    analytic_data["number_of_families_with_more_than_one_adults"] =   FamilyMember.not_in(role:["Son","Daughter"]).group_by(&:family_id).select{|k,v| v.count > 1 }.count rescue 0
    analytic_data["number_of_families_with_more_than_one child"]  =   FamilyMember.in(role:["Son","Daughter"]).group_by(&:family_id).select{|k,v| v.count > 1 }.count  rescue 0
    analytic_data["number_of_families_with_at_least_one_child"]   =   FamilyMember.in(role:["Son","Daughter"]).group_by(&:family_id).select{|k,v| v.count >= 1 }.count rescue 0 
    analytic_data["number_of_users_with_at_least_2_repeat_visits"] =  User.gte(:sign_in_count=> 2).count rescue 0
    analytic_data["number_of_users_with_only_1_login"] =  User.where(:sign_in_count=> 1).count rescue 0
    analytic_data["number_of_users_with_at_least_2_logins_in_last_30_days"] =  UserLoginDetail.user_login_activity(30,2)
    analytic_data["number_of_users_with_at_least_4_logins_in_last_30_days"] =  UserLoginDetail.user_login_activity(30,4)
    analytic_data["average_number_of_logins_per_user"] =  (User.sum(:sign_in_count)/User.count.to_f).round(2) rescue 0
    total_count_data = {}
    total_count_data["user"]  = User.count 
    total_count_data["children"]  = ChildMember.count  
    total_count_data["vaccination"]  = Vaccination.where(opted: "true").count 
    total_count_data["jab"]  =  Jab.count - Jab.in(vaccination_id:Vaccination.where(:opted.ne=> "true").pluck(:id)).count 
    total_count_data["timeline"]  = Timeline.count 
    total_count_data["measurement"]  = Health.count 
    total_count_data["milestone"]  = Milestone.count 
    total_count_data["picture"]  = Picture.count
    total_count_data["document"]  = Document.count

    report_object = Report::UserBasicSummaryReportData.new(total_count_data: total_count_data, country_list:(@all_countries.compact.uniq + [nil]),report_data:@report,analytic_data:analytic_data).save
    end
  
  def  valid_country_list
     
    User.in(country_code:["uk","UK"]).update_all({country_code:"GB"})
    User.in(country_code:["India","india"]).update_all({country_code:"IN"})
    country_list = {}    
    user_country_codes = User.distinct(:country_code).map{|a| a.try(:downcase)}.uniq
    valid_country_code = user_country_codes.select{|country| ISO3166::Country[country]}
    
    valid_country_code.each do |i|
      country_list[i]  = [i, i.upcase]
    end

    invalid_country_code = (user_country_codes - valid_country_code).compact
     
    # Try to get valid code by search with name  
    valid_country_with_name = []
    invalid_country_code.each do |i|      
      if (temp = ISO3166::Country.find_by_name(i.titleize)).present?
        code = temp[0]
        valid_country_with_name << i
        if country_list[code.downcase].present?
          country_list[code.downcase] << i
        else
          country_list[code.downcase] = [i]
        end
      end
    end
    countries_not_found_with_name_and_code = invalid_country_code - valid_country_with_name
    countries_not_found_with_name_and_code.each do |i|
      country_list[i] = [i]
    end    
     
    country_list
  end
  
  #’number of user signed in at least once every N days in last M days’….so if I chose N = 30 and M=180…then it will give me users who have signed at least once every 30 days for the last 180 days (once every month for the last 6 months)
  def self.signin_atleast_at_interval_in_duration(duration=180,interval=30)
    duration = duration.to_i
    interval = interval.to_i
    no_of_interval = duration/interval
    #{ "$match" => { "user_id" => {"$in"=>gg }, :created_at => {"$gte"=>(Time.now -  duration.day).to_date.to_time }} },
    data = UserLoginDetail.collection.aggregate([
     { "$match" => {   :created_at => {"$gte"=>(Time.now.utc -  duration.day).to_date.to_time }} },
        { "$group" => {
            "_id" => {
                 
                "interval" => 
                   {"$trunc" => 
                  {
                   "$divide" => [ 
                    {"$subtract" => [   (Time.now.utc.to_date.to_time  - 1.day)  ,  ("$created_at") ]},
                     interval* 1000 * 60 * 60 * 24] 
                }
              }
            },
            "count"=> { "$sum": 1 },
            "data" => {"$addToSet"=> "$user_id"},
            "date_data" => {"$addToSet"=> {"$dateToString": { "format": "%Y-%m-%d", "date": "$created_at" }}}
            } 
        } ,
      {"$project" => {
      "login_count"=> { "$size": "$data" } ,
      "data" => "$data",
      "date_data" => "$date_data"
      }},
      {"$sort" => { "login_count" => 1}}
    ])
     
    signin_count = 0
     # puts "Data is #{data}"
    if data.count  >= no_of_interval
      k = data[0]["data"]
      data.each do |i|
        k = k & i["data"]
        return signin_count = 0 if k.count == 0 
      end
      signin_count = k.count
    else
      signin_count = 0
    end
    return signin_count

  end

 
 
end