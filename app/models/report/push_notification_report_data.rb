class Report::PushNotificationReportData
  include Mongoid::Document
  include Mongoid::Timestamps 
  
  field :assign_email_notification_data ,type:Hash
  field :sent_email_notification_data ,type:Hash
  field :assign_push_notification_data ,type:Hash
  field :sent_push_notification_data ,type:Hash
  
  field :notification_date, type:Date
  
  def self.create_data(data_of_nth_day=0,options={})
    start_time = (Time.now - data_of_nth_day.days).beginning_of_day.to_time
    end_time = (Time.now - data_of_nth_day.days).end_of_day.to_time
    notification_date = start_time.to_date.to_s
    ::Report::PushNotificationReportData.where(:notification_date=> start_time.to_date).delete_all
    assign_email_notification_data =  Report::PushNotificationReport.push_notification_report_data("created_at",{:start_date=>start_time,:end_date=>end_time,:notify_by=>"email", :data_to_save=>true}).last  || {} rescue {}
    sent_email_notification_data =  Report::PushNotificationReport.push_notification_report_data("updated_at",{:start_date=>start_time,:end_date=>end_time,  :notify_by=>"email", :data_to_save=>true}).last  || {} rescue {}
    assign_push_notification_data  =  Report::PushNotificationReport.push_notification_report_data("created_at",{:start_date=>start_time,:end_date=>end_time,:notify_by=>"push" , :data_to_save=>true}).last  || {} rescue {}
    sent_push_notification_data    =  Report::PushNotificationReport.push_notification_report_data("updated_at",{:start_date=>start_time,:end_date=>end_time,:notify_by=>"push" , :data_to_save=>true}).last  || {} rescue {}
    
    assign_email_notification_data = {} if assign_email_notification_data["sent_date"] != notification_date
    sent_email_notification_data = {} if sent_email_notification_data["sent_date"] != notification_date
    assign_push_notification_data = {} if assign_push_notification_data["sent_date"] != notification_date
    sent_push_notification_data = {} if sent_push_notification_data["sent_date"] != notification_date
    
    data_to_save = {:notification_date=> start_time.to_date,:assign_email_notification_data=>assign_email_notification_data, :sent_email_notification_data  => sent_email_notification_data , :assign_push_notification_data => assign_push_notification_data, :sent_push_notification_data => sent_push_notification_data   }  
    ::Report::PushNotificationReportData.create(data_to_save)
  end

  def self.get_data(group_by, start_time,end_time,options={})
    begin
      data = ::Report::PushNotificationReportData.where(:notification_date=>{"$gte"=>start_time,"$lte"=>end_time})
      notify_by = options[:notify_by] || "push" rescue "push"
      if group_by == "created_at"
        if notify_by == "push"
          data = data.where(:assign_push_notification_data.ne=>{}).pluck(:assign_push_notification_data)
        else
          data = data.where(:assign_email_notification_data.ne=>{}).pluck(:assign_email_notification_data)
        end
      elsif group_by == "updated_at"
        if notify_by == "push"
          data = data.where(:sent_push_notification_data.ne=>{}).pluck(:sent_push_notification_data)
        else
          data = data.where(:sent_email_notification_data.ne=>{}).pluck(:sent_email_notification_data)
        end
      end
    rescue Exception => e 
      puts e.message
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside user notification report-#{Rails.env}")
      data = nil
    end
    data
  end
end