class Report::PointerAssignedData
  include Mongoid::Document
  include Mongoid::Timestamps
   
  def self.save_assigned_data
    data = ArticleRef.assigned_pointer_group_by_system_pointer_id(nil,{:group_by=>"custome_id"})
    # data = {1=>33,2=>21..}
    Report::PointerAssignedData.new(data).save
  end

  def self.get_assigned_pointers_data
    Report::PointerAssignedData.where(:created_at.gte=>Date.today).last
  end

end