class Report::BatchJobMailerData
  include Mongoid::Document
  
  field :started_at ,type:Time
  field :finished_at ,type:Time
  field :batch_job_name ,type: String
  field :total_mail_to_sent ,type:Integer
  field :mail_sent_count ,type:Integer
  field :mail_not_sent_count ,type:Integer
  field :country_code ,type:String
  field :argument_list, type: Hash 
end