class Album
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :name, type: String
  field :desc, type: String
  # field :order_id, type: Integer
  # field :ref_id, type: Integer 
  # field :band_id, type: String
  # field :family_id, type: Integer 
  field :album_date, :type => Date, :default => Date.today
  belongs_to :family 
  has_many :pictures, :as => :imageable, :dependent => :destroy
end
