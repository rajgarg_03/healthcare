# It store info about which user has what role to access a module like pointer, report, tools etc

class UserRoleForAdminPanel
  include Mongoid::Document  
  include Mongoid::Timestamps
  
  field :email, type: String
  field :role, type: String
  field :member_id, type:String

  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i }
  
  # All modile list availble in admin panel
  ModuleNameMapping = ["NhsApiLog", "BatchNotificationManager", "ActionCardListMaintenance","ModuleAccessMaintenance","PointersMaintenance","SubscriptionMaintenance","SummeryReports&UserList","SystemRolesMaintenance","ToolsListMaintenance","UserRoleMaintenance","FacePlusPlus","SystemTemplate","SubscriptionReport","SystemFaq","ApiLog","ScenarioDetails", "NHSRequestManager", "NhsVideo","KpiData","AffiliateLinks","Clinic","SystemSettings","EventLog","EmisLog",'CvtmePurchaseEventLog','NhsSyndicatedContent','SmileADesignContest',"TppLog",'NhsLogin/GPLinked AccountManager','UnregisterFromEmail','PushNotificationReport']

end

