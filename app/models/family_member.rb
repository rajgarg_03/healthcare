class FamilyMember
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Document


  field :member_id,          type: String
  field :family_id,          type: String
  field :role,               type: String
  field :family_user_count,  type: Integer, :default=>0
  field :activity_status,    type: String, :default=>"Active" # show family members activey status
  field :role_added_by,      type: String, :default=>"user" # eg. "user/system"
  field :activity_status_v2, type: String, :default=>"Active" # New logic for activity status
  
  belongs_to :family
  belongs_to :member

  validates :member_id, uniqueness: {scope: :family_id, message: "Already added in this family"}

  attr_accessor :first_name, :last_name, :email, :confirm_email ,:role_name ,:user_id , :status, :email_verification_hash
  
  index ({member_id:1})
  index ({family_id:1})
  index ({role:1})

  WillPaginate.per_page = 5
  
  STATUS = {:accepted=>'accepted', :sent => 'sent', :reject => 'rejected', :deleted => 'deleted'}
  ROLES = {administrator: "administrator", :admin => "Creator",:son => 'Son', :daughter => 'Daughter',:mother=>'Mother',:father=>'Father',nanny: "Nanny", :grand_mother=>'GrandMother',:grand_father=>'GrandFather', :aunt=>'Aunt',:uncle=>'Uncle',:guardian=>'Guardian', :brother => 'Brother', :sister => 'Sister'}
  IMAGE_CLASS = {:nanny=>'icon-user-female', :other => 'icon-user', :creator => 'icon-user',:administrator => 'icon-user' ,:son => 'icon-user-child-boy', :daughter => 'icon-user-child-girl', :brother => 'icon-user-male', :sister => 'icon-user-female',:mother=>'icon-user-female',:father=>'icon-user-male', :grand_mother=>'icon-user-female',:grand_father=>'icon-user-male', :aunt=>'icon-user-female',:uncle=>'icon-user-male',  :sibling=>'user-avatar',:guardian=>'user-avatar'}
  CHILD_ROLES = [ROLES[:son], ROLES[:daughter]]
  ELDER_ROLES = ROLES.values  - [ROLES[:son], ROLES[:daughter], ROLES[:admin], ROLES[:administrator] ]
  OTHER_ROLES = ROLES.values  - [ROLES[:son], ROLES[:daughter]]


  def is_child?
    member._type == 'ChildMember'
  end   

  def is_parent?
    member._type == 'ParentMember'
  end   

  def is_admin?
    role == FamilyMember::ROLES[:admin]
  end

  def update_family_member_data(options={})
    begin
      family_member = self
      member_id = family_member.member_id
      member = Member.find(member_id)
      family = Family.find(family_member.family_id)
      # Update member stage
      user = User.where(member_id:member_id).last
      stage_value = nil
      options[:family_id] = family.id
      user.delay.update_user_stage(stage_value,options)
      # Update user and family segment
      member.update_family_and_user_segment(nil,options)
      # Update family trial period 
      family.delay.update_family_trial_days(trial_days_value=nil,options)
      
      # Update family pricing exemption 
      family.delay.update_family_pricing_exemption(options)
      
    rescue Exception=> e 
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,{member_id:member_id},"Error inside update_family_member_data")
    end
  end  

end