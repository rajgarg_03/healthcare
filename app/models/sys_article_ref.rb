#SysArticleRef Store all System Defined Pointers
class SysArticleRef
  include Mongoid::Document
  include Mongoid::Timestamps
  # field :scenarios,                         :type=> String
  # field :name,                              :type => String# scenario in tech
  # field :score,                             :type => String
  # field :N_factor,                          :type => String 
  # field :ref_start_date,                    :type => String
  # field :ref_peak_rel_date,                 :type => String
  # field :ref_expire_date,                   :type => String
  # field :start_day_rel,                     :type => String
  # field :peak_day_rel,                      :type => String
  # field :category,                          :type=> String
  # field :stage,                             :type=> String
  # field :condition,                         :type=> String
  # field :criteria,                          :type=> String #condition in tech
  # field :ds,                                :type=> String
  # field :dp,                                :type=> String
  # field :dexp,                              :type=> String

  field :custome_id,                         :type=> Integer
  field :topic,                              :type=> String
  field :description,                        :type=> String # topic in tech
  field :remark,                             :type=> String
  field :status,                             :type=> String, :default=>"Active"
  field :link_url,                           :type=> String
  field :valid_for_role,                     :type=> String
  field :notification_expiry,                :type=> String
  field :notification_expiry_tech,           :type=> String
  field :exclusion ,                         :type=> String 
  field :pointer_type ,                      :type=> String # eg "general/Affiliate" 
  # field :system_scenario_detail_custome_id , :type=> Integer 
  

  has_many :article_refs
  # validates :condition, presence: true
  validates :topic, presence: true

  
  attr_accessor :system_scenario_detail_custome_id
  index ({custome_id: 1})
  index ({topic: 1})
  # index ({system_scenario_detail_custome_id: 1})
  index ({status: 1})
  index ({pointer_type: 1})
  index ({valid_for_role: 1})
  
  def scenario_detail
    system_pointer = self
    System::Scenario::ScenarioLinkedToTool.where(tool_class:system_pointer.class.to_s,linked_tool_id:system_pointer.id.to_s).last.scenario_detail rescue nil
  end

  def title
    self.topic
  end

  def update_scenario(system_scenario_custom_id)
    system_pointer = self
    System::Scenario::ScenarioLinkedToTool.add_to_list(system_pointer,system_scenario_custom_id)
  end

  def get_pointer_history(day_count=30,group_by="created_at")
     
    # conditions = { group_by => {"$gte"=>selected_date.to_time, "$lte"=>selected_date.to_time + 1.day}  }
    pointer_manager_id = self.id
    
    hash_data =  ArticleRef.collection.aggregate([
        { "$match" => { :sys_article_ref_id =>pointer_manager_id , :created_at => {"$gte"=>Time.now - day_count.days } } },
         {
          "$group"=> {
          "_id"=>  {"date":{"$dateToString":{ format: "%Y-%m-%d", "date": "$#{group_by}" }} }   ,
           "count" => {"$sum" => 1} , 
           "sent": {
                "$sum": {
                    "$cond": [ { "$eq": [ "$status",  "sent" ] }, 1, 0]
                }
            },

            "pending": {
                "$sum": {
                    "$cond": [ { "$eq": [ "$status",  "pending" ] }, 1, 0]
                }
            }
          }}   
       ])
     
    data = []
    hash_data.each do |fetched_data|
      date = fetched_data["_id"]["date"]
      info = {"total_count"=>fetched_data["count"],"sent"=>fetched_data["sent"],"pending"=>fetched_data["pending"]}
      data << {date=>info}
    end
   data

  end
end
 