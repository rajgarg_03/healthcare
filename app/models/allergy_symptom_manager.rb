class AllergySymptomManager
  include Mongoid::Document 
  include Mongoid::Timestamps 
  field :name, type: String
  field :member_id, type: String
  field :category, type: String
  field :country_code, type: String
  field :status, type: String, default:"active" # eg. active/deleted
   
  
  index ({country_code:1})
  index ({member_id:1})
  index ({status:1})
  index ({category:1})

  def self.add_new_symptom(current_user,params)
    country_code = params[:country].present? ? params[:country] : (current_user.user.country_code || "GB" rescue "GB")
    AllergySymptomManager.create(country_code:country_code, member_id:current_user.id,name:params[:name],category:params[:category])
  end
  

  def self.valid(current_user,added_symptom_manager_ids=[])
    if added_symptom_manager_ids.present?
      where({"$or"=>[{:id.in=>added_symptom_manager_ids},{status:"active"}]})
    else
      where(status:"active")
    end 
  end

  def self.in_country(current_user=nil,country_code=nil)
    country_code = (country_code || current_user.user.country_code || "GB") rescue "GB"
    country_code = AllergySymptomManager.distinct(:country_code).map(&:downcase).include?(country_code.downcase) ? country_code : "GB"
    sanitize_country_code = Member.sanitize_country_code(country_code)
    where(:country_code.in=>sanitize_country_code)
  end

  def self.get_list(current_user,allergy_id)
    begin  
      user_added_symptom_manager = AllergySymptomManager.where(member_id:current_user.id).sort_by{|a| a.name.downcase} 
      country_code = nil
      if allergy_id.present?
        symptoms_added_in_allergy  = (Child::HealthCard::Allergy.find(allergy_id).allergy_symptoms || {}) rescue {}
        added_symptom_manager_ids = symptoms_added_in_allergy.map(&:to_s) #symptoms_added_in_allergy.map{|symptom| symptom["_id"].to_s }.flatten
        child_member_id = Child::HealthCard::Allergy.find(allergy_id).member_id
        child_member =  ChildMember.find child_member_id
        country_code = User.where(member_id:child_member.family_mother_father.first.to_s).first.country_code.upcase rescue nil
      end
      system_symptom_manager = AllergySymptomManager.valid(current_user,added_symptom_manager_ids).in_country(current_user,country_code).where(member_id:nil).sort_by{|a| a.name.downcase} 
      allergy_symptom_manager_list = user_added_symptom_manager + system_symptom_manager
      
      data = []
      if allergy_id.present?
        allergy_symptom_manager_list.map do |allergy_symptom|
          allergy_symptom["added"] = added_symptom_manager_ids.include?(allergy_symptom.id.to_s)
          data << allergy_symptom
        end
      else
        allergy_symptom_manager_list.map do |allergy_symptom|
          allergy_symptom["added"] = false
          data << allergy_symptom
        end
      end
      data
    rescue Exception => e 
      Rails.logger.info "Error in symptom get list #{current_user}.inspect ----#{allergy_id}"
      Rails.logger.info e.message
      data = []
    end
  end


  # For admin Panel
  def update_record(params)
    begin
      allergy_symptom_params = params["AllergySymptomManager".underscore]
      allergy_symptom = self
      if allergy_symptom.update_attributes(allergy_symptom_params)
        status = true
      else
        status = false
      end
    rescue Exception => e
      Rails.logger.info e.message
      status = false 
    end
    [allergy_symptom,status]
  end


  def self.create_record(params)
    begin
      allergy_symptom_params = params["AllergySymptomManager".underscore]
      system_allergy_symptom = self.new(allergy_symptom_params)
      system_allergy_symptom.save!
      status = true
    rescue Exception => e
      Rails.logger.info e.message
      status = false 
    end
    [system_allergy_symptom,status]
  end
  
  def self.duplicate_record(params)
    record_to_be_duplicated = AllergySymptomManager.find(params[:record_id]).attributes
    record_to_be_duplicated.delete("_id")
    record_to_be_duplicated.delete("created_at")
    system_allergy_symptom = AllergySymptomManager.new(record_to_be_duplicated)
    system_allergy_symptom.save!
    system_allergy_symptom
  end
  def self.delete_record(record_id)
    begin
      system_allergy_symptom = AllergySymptomManager.find(record_id)
      #SystemJab.where(system_vaccin_id:system_vaccine.id).delete_all
      system_allergy_symptom.status = "deleted"
      system_allergy_symptom.save
    rescue Exception => e
      Rails.logger.info e.message
    end
  end
  

   
  def self.duplicate_all_records(params)
    begin
      raise "Already added data for selected country" if AllergySymptomManager.where(:country_code.in=> Member.sanitize_country_code(params[:destination_country_code])).present?
      sanitize_country_code = Member.sanitize_country_code(params[:source_country_code])
      system_allergy_symptom_list = AllergySymptomManager.where(:country_code.in=>sanitize_country_code).valid(current_user=nil)
      system_allergy_symptom_list.each do |system_allergy_symptom|
        allergy_symptom_params = {:record_id=>system_allergy_symptom.id}
        # allergy_params["country_code"] = params[:destination_country_code]
        system_allergy_symptom_obj = AllergySymptomManager.duplicate_record(allergy_symptom_params)
        system_allergy_symptom_obj.country_code = params[:destination_country_code]
        system_allergy_symptom_obj.save
      end
      status = true
      message = ""
      update_member_allergy_symptom_manager_for_added_country(params[:destination_country_code])
    rescue Exception=> e 

      Rails.logger.info "Error in AllergySymptomManager.duplicate_all_records"
      Rails.logger.info e.message
      status = false
      message = e.message
    end
    [status,message]
  end
  
  def self.update_member_allergy_symptom_manager_for_added_country(country_code)
    country_code = country_code.upcase
    Child::HealthCard::Allergy.distinct(:member_id).each do |child_id|
      begin
        child = ChildMember.find(child_id)
        member = child
        member_country_code = User.where(member_id:member.family_mother_father.first.to_s).first.country_code.upcase
        if member_country_code == country_code
          Child::HealthCard::Allergy.where(member_id:child_id.to_s).each do |allergy|
            allergy_symptom_manager = AllergySymptomManager.where(:id.in=>allergy.allergy_symptoms)
            existing_symptom_manager = AllergySymptomManager.in_country(nil,member_country_code).where(:name.in=>allergy_symptom_manager.map(&:name))
            if existing_symptom_manager.present?
              allergy.allergy_symptoms = existing_symptom_manager.map(&:id)
              allergy.save
            end
          end
        end
      rescue Exception=> e 
        Rails.logger.info "Inside update_member_allergy_manager_for_added_country"
        Rails.logger.info e.message
      end
    end
  end

end
