class Family
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :name,                     type: String
  field :family_uid,               type: String
  field :status,                   type: Integer, default: 0
  field :member_id,                type: String
  field :activity_status,          type: String, :default=>"Active" # show family members activey status
  field :segment,                  type: String # eg. trial/starter/adoptor/ambassador
  field :trial_days,               type: Integer, :default=> 7 
  field :pricing_stage,            type: String, :default=>"trial" # eg standard/trial 
  field :pricing_exemption,        type: String, :default=>"basic" # eg. "none/basic/all"
  field :created_by,               type: String, :default=>"user" # eg. "user/system"
  field :organization_uid,         type: String
  field :ods_code,                 type: String
  field :last_activity_done_on,    type: Time
  field :country_code,             type: String  # family owner country code. used for analytics for now
  field :activity_status_v2,          type: String, :default=>"Active" # show family members activey status

  validates :name, :member_id, :presence => true
  cattr_accessor :api_version, :current_user

  belongs_to :member
  has_many :family_members, :dependent => :destroy  
  has_many :albums,     :dependent => :destroy
  has_many :invitations #, :dependent => :destroy
  has_many :pregnancies #, :dependent => :destroy  

  after_create :add_family_member, :assign_points
  after_destroy :delete_points 
  before_create :generate_family_uid
  
  STATUS = {:deleted => 1, :active => 0}

  index ({id:1})
  index ({member_id:1})
  index ({family_uid: 1})
  index ({ods_code: 1})
  
  def family_owner_pronoun(options={})
    begin
      family = self
      family_member = FamilyMember.where(member_id:family.member_id,family_id:family.id.to_s).last
      role = family_member.role.downcase
      pronoun = "his"
      if ['sister','aunt','grandmother',"nanny",'mother','daughter'].include?(role)
        pronoun = "her"
      else
        pronoun = "his"
      end
    rescue Exception => e 
       pronoun = "his"
    end
    pronoun
  end

  def delete_family(current_user,options={})
    family = self
    family_id = family.id.to_s
    options = options.merge({:family_elder_member_ids=> family.family_elder_member_ids,:current_user=>current_user})

    valid_to_delete = false
    msg = Message::API[:error][:delete_family]
    family_subscription = Subscription.find_family_subscription(family_id).last
    # family is subscribed for free subscription
    if family_subscription.blank? || family_subscription.subscription_listing_order < 1
      valid_to_delete = true
    # elsif family_subscription.member_id.to_s == current_user.id.to_s
    #   valid_to_delete = true
    else
      msg = Message::Subscription[:not_valid_user_to_delete]
      raise msg
    end
    if valid_to_delete
      family_child_members = FamilyMember.where(family_id:family.id).in(role:FamilyMember::CHILD_ROLES).pluck("member_id")#.map(&:to_s)
      family_child_members  = family_child_members.map(&:to_s)
      # Pregnancy.in(expected_member_id:family_child_members).delete_all
      current_user.delete_family_depenedent_records(family_child_members,family_id.to_s,options)
      FamilyMember.where(family_id:family_id).delete_all
      family_ids = [family_id]
      report_event = Report::Event.where(name:"family deleted").last
      ::Report::ApiLog.log_event(current_user.user_id,family.id,report_event.custom_id,options) rescue nil
   
      family.delete
      # ChildMember.new.update_familyuser_stage_on_add_or_delete_member(family_ids,options) 
      msg = Message::API[:success][:delete_family] 
    end
    msg
  end

  def user
    member.user
    # User.find self.primary_owner_id
  end

  def users
   User.in(id: user_families.map(&:user_id)) 
  end

  def active_members
    family_members.where(:status.ne => 1)
  end

  def self.find_by_family_id(id)
    self.where(:family_id => id).first
  end
  
  def self.elder_members_with_app_version(family_id,ios_app_version_in_number,android_app_version_in_number,options={})
    family_elder_member_ids = FamilyMember.where(:family_id=>family_id,:role.nin=>["Son","Daughter"]).pluck(:member_id).uniq
    Device.or({:app_version_in_number.gte=>android_app_version_in_number,:platform=> "android"},{:app_version_in_number.gte=>ios_app_version_in_number,:platform=> "ios"}).where(:member_id.in=>family_elder_member_ids).entries
  end

  def family_members_count
    self.family_members.count
  end

def ordered_family_members_api(current_user)
    expected_children = {}
    completed_pregnancy_children = {}
    pregnancy_added_child = {}
    family = self

    continuing_pregnancy = Pregnancy.where(family_id:family.id,:status=>"expected")
    continuing_pregnancy.map{ |pregnancy_data| expected_children[pregnancy_data.expected_member_id.to_s]= [pregnancy_data.id, pregnancy_data.status,pregnancy_data.member_id] } rescue {}
    
    complete_pregnancy = Pregnancy.where(family_id:family.id, :status=>"completed")
    complete_pregnancy.map{ |pregnancy_data| completed_pregnancy_children[pregnancy_data.expected_member_id.to_s]= [pregnancy_data.id, pregnancy_data.status,pregnancy_data.member_id] } rescue {}
    
    complete_pregnancy.pluck(:added_child).flatten.map{|a| pregnancy_added_child[a]= "completed" } rescue {}
    #complete_pregnancy.map{|a| pregnancy_added_child[a.expected_member_id] = "completed" } rescue {}
    
    expected_children_ids = expected_children.keys
    pregnancy_added_child_ids = pregnancy_added_child.keys
    completed_pregnancy_children_ids = completed_pregnancy_children.keys
    children_ids =  family_members.not_in(:member_id=>(completed_pregnancy_children_ids + expected_children_ids + pregnancy_added_child_ids)).where({'role' => {"$in" => FamilyMember::CHILD_ROLES }}).pluck(:member_id)
    
    # ordered_members = (Member.includes(:family_members).order_by("birth_date desc").where(:id.in=>children_ids).map(&:family_members)).flatten
    
    ordered_members_ids = (Member.includes(:family_members).order_by("birth_date desc").where(:id.in=>children_ids).pluck(:id))
    ordered_members = FamilyMember.where(:member_id.in=>ordered_members_ids).sort_by{|family_member| ordered_members_ids.index(family_member.member_id)}
    
    ordered_expected_children_ids = Member.where(:id.in=>expected_children_ids).order_by("birth_date desc").pluck(:id)#.flatten
    ordered_expected_children = FamilyMember.where(:member_id.in=>ordered_expected_children_ids).sort_by{|family_member| ordered_expected_children_ids.index(family_member.member_id)}
    
    ordered_pregnancy_added_children_ids = Member.where(:id.in=>pregnancy_added_child_ids).order_by("birth_date desc").pluck(:id)#.flatten
    ordered_pregnancy_added_children = FamilyMember.where(:member_id.in=>ordered_pregnancy_added_children_ids).sort_by{|family_member| ordered_pregnancy_added_children_ids.index(family_member.member_id)}
    
    completed_pregnancy = FamilyMember.in(member_id:complete_pregnancy.map(&:expected_member_id))
     
     children_records = (
      ordered_expected_children + 
      ordered_members + 
      family.family_members.where(:role => FamilyMember::ROLES[:admin]).entries + 
      family.family_members.where(:member_id => current_user.id).entries + 
      family.family_members.where({'role' => {"$in" => FamilyMember::ELDER_ROLES }, :member_id.ne => current_user.id}).entries +
      family.family_members.where(:role => nil).entries +
      ordered_pregnancy_added_children  + completed_pregnancy 
    ).uniq

     [children_records, expected_children.merge(completed_pregnancy_children),pregnancy_added_child]
  end


#  For api 
  def family_member_list(api_version=1)
    family =self
    api_version = Family.api_version || api_version
    @current_user = Family.current_user 
    members = []
    creator = nil
    options = {}
    system_tool_ids = Nutrient.valid_to_launch(@current_user,api_version,options).pluck(:id)
    options[:system_tool_ids] = system_tool_ids
    ordered_members,preg_ids,preg_added_child  =  family.ordered_family_members_api(family.member)
    profile_image_mapping_with_id = {}
    Picture.in(:imageable_field=>[:profile_image, nil], :imageable_id=>ordered_members.map(&:id)).map{|profile_image| profile_image_mapping_with_id[profile_image.imageable_id.to_s] = profile_image } rescue nil
    ordered_members.each do |m|
      child_mem = m.member
      if child_mem.present?
        mem = child_mem.attributes
        clinic_detail = child_mem.get_clinic_detail(@current_user,options) || Clinic::ClinicDetail.new
        mem[:clinic_details] = clinic_detail.format_data(@current_user,child_mem,options) rescue nil
        mem["clinic_link_state"] =  child_mem.get_clinic_link_state(@current_user,options)
        mem["status"] = "member"
        mem["family_member_id"] = m.id
        mem["role"] = m.role
        mem["is_expected"] = (preg_ids[child_mem.id.to_s].present? rescue false)
        mem["pregnancy_id"] = nil
        mem["pregnacy_status"] = nil  
        mem["age"] = ApplicationController.helpers.calculate_age(child_mem.birth_date) rescue ""
        if child_mem.user_id.present?
          user =  child_mem.user
          @code =  "GB" if user.country_code.downcase == "uk" rescue nil
          @code = @code || user.country_code
          mem["location"] = @code
          @code = nil
        end
        if mem["is_expected"] # || is_added_child
          mem["pregnancy_id"] = preg_ids[child_mem.id.to_s][0]
          mem["pregnacy_status"] = preg_ids[child_mem.id.to_s][1]
          mem["pregnancy_week"] = Pregnancy.calculate_pregnancy_week(child_mem.birth_date.to_time)[0]
          mem["role"] = nil
          mem["age"] = Pregnancy.calulate_age(child_mem.birth_date,mem["pregnacy_status"]) rescue ""
          mem["mother_id"] = preg_ids[child_mem.id.to_s][2]
          mem["mother_name"] = Member.find(preg_ids[child_mem.id.to_s][2]).first_name rescue nil 
        end
        mem["tools"] = child_mem.user_id.present? ? [] : member_tools(child_mem,@current_user,api_version,options)
        mem["profile_image"] = (profile_image_mapping_with_id[child_mem.id.to_s] || child_mem.current_profile_image_url("small",api_version)) rescue ""
        mem["aws_profile_image_url"] = (profile_image_mapping_with_id[child_mem.id.to_s] || child_mem.profile_image.aws_url("small",api_version) rescue "")
        if m.role == FamilyMember::ROLES[:admin] || m.role == FamilyMember::ROLES[:administrator] 
          creator = mem
          creator["role"] = "administrator"
          members <<  creator
        else
          members << mem
        end
      end
    end
      
     invite_list =  invitations.includes(:member).where(:sender_type => 'Family', :status => "invited")
     invite_list.each do |invite|
      mem = invite.member.attributes rescue {}
      mem["role"] = invite.role
      mem["status"] = "Join request sent"
      mem["invitation_id"] = invite.id
      members << mem
     end
      invite_list = invitations.includes(:member).where(:sender_type => 'Member', :status => "invited")
      invite_list.each do |invite|
      mem = invite.member.attributes rescue {}
      mem["status"] = "Join request received"
      mem["role"] = invite.role
      mem["invitation_id"] = invite.id
      # sender = inv.sender_id.present? ? inv.sender : inv.member
      receiver = invite.sender_id.present? ? invite.member : invite.family.owner
      mem["receiver_member_id"] = receiver.id rescue ""
      mem["receiver_email"] = receiver.email rescue ""
      members << mem
     end
     members
  end
  
  def member_tools(member,current_user,api_version=1,options={})
    member_tool_list = Nutrient.get_member_tools(member,current_user,api_version,options)
    member_tool_list
  end
  
  def owner
    member
  end  

  def sent_invitations
    invitations.where(:sender_type => 'Family', :status => "invited")
  end

  def received_invitations
    invitations.where(:sender_type => 'Member', :status => "invited")
  end

  def ordered_family_members(current_user)
    (
      family_members.where({'role' => {"$in" => FamilyMember::CHILD_ROLES }}) + 
      family_members.where(:role => FamilyMember::ROLES[:admin]) + 
      family_members.where(:member_id => current_user.id) + 
      family_members.where({'role' => {"$in" => FamilyMember::ELDER_ROLES }, :member_id.ne => current_user.id}) +
      family_members.where(:role => nil)
    ).uniq
  end

  def having_member?(member)
    family_members.where(member_id: member.id)
  end  

  def url_name
    name.gsub(".","")
  end  

  def add_family_member(role=nil,options={})
    role ||= FamilyMember::ROLES[:admin]
    family = self
    
    if family.country_code.blank?
      family.update_attribute(:country_code, family.owner.user.country_code)
    end

    if options[:update_family_member] == true
      family_member = FamilyMember.where(member_id: family.member_id, family_id: family.id).last
      family_member.role = role
    else
      family_member = FamilyMember.new(member_id: family.member_id, family_id: family.id, role: role, family_user_count:1)
      member = family_member.member
      report_event = Report::Event.where(name:"family added").last
      ::Report::ApiLog.log_event(member.user_id,family.id,report_event.custom_id,options.merge({:member_id=>member.id})) rescue nil
    end
    family_member.save
    family_member.update_family_member_data(options)
    family_member
  end

  def no_of_kids
    no_of_kids = family_members.where({'role' => {"$in" => FamilyMember::CHILD_ROLES }}).uniq.count
    expected_member_count = Pregnancy.where(family_id: self.id).count rescue 0
    no_of_kids - expected_member_count
  end

  def no_of_mothers
    family = self
    no_of_mothers = family.family_members.where(:role => FamilyMember::ROLES[:mother]).uniq.count
    invited_member = Invitation.where(family_id:family.id,role:"Mother",status:"invited").uniq.count
    no_of_mothers + invited_member#.count  
  end

  def no_of_elders
    no_of_mothers = family_members.not_in(:role => FamilyMember::CHILD_ROLES).uniq.count
  end

  def no_of_pregent_member
    family = self
    no_of_mothers = family.family_members.where(:role => FamilyMember::ROLES[:mother]).map{|fm| fm.member_id.to_s}
    #Pregnancy.in(member_id:no_of_mothers).uniq.count
    invited_member = Invitation.where(family_id:family.id,role:"Mother").map{|a| a.member_id.to_s}
    Pregnancy.where(:family_id=>family.id ).in(member_id:(no_of_mothers+invited_member+[nil])).uniq.count
  end
  def no_of_pregnancy
    Pregnancy.where(family_id:self.id).uniq.count
  end
  
  def update_family_uid
    begin
      get_family_uid = rand(10**10)
    end while Family.where(family_uid: get_family_uid).present?
    self.update_attribute(:family_uid, get_family_uid)
  end

  #NOTE: Override to_json method and provide extra data
  def as_json(options = {})
    json = super(options)
    if options.has_key?(:include_owner_details)
      json['owner_name'] = owner.full_name
      json['owner_profile_pic'] = owner.profile_pic_for_api
    end  
    json
  end
  # for API-36
  def subscription
     subscription = Subscription.find_family_subscription(self.id.to_s).last
     subscription_manager = subscription.subscription_manager rescue nil
    if subscription.present? && Subscription.is_premium?(subscription_manager.subscription_listing_order)
      {:subscription_level=>subscription_manager.subscription_listing_order, :is_subscribed=>true, :subscribed_by_member_id=>subscription.member_id, :subscription_plan_category=>subscription_manager.category.camelize, :subscription_plan=>subscription_manager.title}
    else
      default_subscription_manager = SubscriptionManager.free_subscription
      {:subscription_level=>default_subscription_manager.subscription_listing_order, :is_subscribed=>false, :subscribed_by_member_id=>nil, :subscription_plan=>default_subscription_manager.title,:subscription_plan_category=>default_subscription_manager.category.camelize}
    end
  end

  def cvtme_purchase
    family = self
    cvtm_setting_values = Child::VisionHealth::Purchase.purchase_detail_setting rescue {}
    cvtme_purchase = ::Child::VisionHealth::Purchase.where(family_id:family.id).last || ::Child::VisionHealth::Purchase.default_subscription(family.id)
    available_attempt_count = cvtme_purchase.available_attempt_count
    require_cvtme_purchase = available_attempt_count <= 0
    if cvtme_purchase.total_attempt_allowed == -1
      available_attempt_count = "NA"
    end
    expiry_in_days = cvtme_purchase.get_expiry_days(cvtm_setting_values)
    {:_id=>cvtme_purchase.id,:purchase_date=>cvtme_purchase.purchase_date, :purchase_expiry_in_days=>expiry_in_days, :family_id=>cvtme_purchase.family_id, :require_cvtme_purchase=>require_cvtme_purchase, :purchased_by=>cvtme_purchase.member_id,:total_attempt_taken=>cvtme_purchase.total_attempt_taken,:total_attempt_remaining=>available_attempt_count , :platform=>cvtme_purchase.platform,:purchase_type =>cvtme_purchase.purchase_type}.merge(cvtm_setting_values)
  end

  # all purchase (cvtme,etc..)
  def other_purchase
    family = self
    cvtm_setting_values = Child::VisionHealth::Purchase.purchase_detail_setting rescue {}
    cvtme_purchases = ::Child::VisionHealth::PurchaseHistory.where(family_id:family.id).entries
    data  = []
    cvtme_purchases.each do |cvtme_purchase|
      cvtme_purchase = cvtme_purchase.attributes.merge(cvtm_setting_values)
      cvtme_purchase = cvtme_purchase.with_indifferent_access
      data <<  {
        _id: cvtme_purchase[:_id],
        purchase_date: cvtme_purchase[:purchase_date],
        family_id: cvtme_purchase[:family_id],
        purchased_by: cvtme_purchase[:member_id],
        platform: cvtme_purchase[:platform],
        android_product_id: cvtme_purchase[:android_product_id],
        android_app_price: cvtme_purchase[:android_app_price],
        ios_app_price: cvtme_purchase[:ios_app_price],
        ios_product_id:cvtme_purchase[:ios_product_id],
        identifier:"CVTME",
        product_name: "CVTME online color blindness test"
      }
    end
    data
  end

  def self.get_first_family_first_child(current_user)
    families = current_user.families.order_by('created_at asc')
    child = ''
    families.each do |family|
      family.family_members.order_by('created_at asc').each do |family_member|
        if (['Son', 'Daughter'].include?(family_member.role) rescue false)
          member_id = family_member.member_id
          child = Member.find(member_id) rescue ''
        end
        break if child.present?
      end
    end
    child
  end
  
  # get youngest child in family
  def family_elder_member_ids
    family =  self
    FamilyMember.where(family_id:family.id,:role.in=>FamilyMember::OTHER_ROLES).pluck(:member_id).uniq
  end

  def family_youngest_child
    family =  self
    member_ids =  FamilyMember.where(family_id:family.id.to_s,:role.in=>FamilyMember::CHILD_ROLES).pluck(:member_id).map(&:to_s)
    members = ChildMember.where(:id.in=>member_ids).order_by("birth_date desc")
    member_id = nil
    youngest_child = nil
    begin
      members.each do |member|
        if !member.is_expected?
          member_id = member.id.to_s
          youngest_child = member
          break
        end
      end
    rescue Exception=> e
    end

    if youngest_child.blank?
      youngest_child = members.first
    end
    youngest_child
  end
  
  def update_family_segment(signin_count=nil,options={})
    family = self
    signin_count = signin_count || family.get_family_user_signin_count
    if signin_count <= GlobalSettings::FamilySegment[:trial]
      segment_value = "trial"
    elsif signin_count <= GlobalSettings::FamilySegment[:starter]
      segment_value = "starter"
    elsif signin_count <= GlobalSettings::FamilySegment[:adoptor]
      segment_value = "adoptor"
    else
      segment_value = "ambassador"
    end
    if family.segment != segment_value
      family.segment = segment_value
      family.save
      report_event = Report::Event.where(name:"mark family segment #{segment_value}").last
      ::Report::ApiLog.log_event(user_id=nil,family.id,report_event.custom_id,options) rescue nil
    end
  end
  
  def get_family_user_signin_count(options={})
    family = self
    User.where(:member_id.in=>family.family_elder_member_ids).pluck(:sign_in_count).map(&:to_i).sum
  end

  def update_family_trial_days(trial_days_value=nil,options={})
    family = self
    trial_days_value = trial_days_value || family.get_family_user_max_trial_day(options)
    if family.trial_days != trial_days_value
      family["trial_days"] = trial_days_value
      family.save!
      family.reload
      family.update_family_pricing_stage(options)
    end
  end

  def update_family_pricing_stage(options={})
    family = self
     
    if (family.created_at + (family.trial_days).days) >= Date.today
      pricing_stage_value = "trial"
    else
      pricing_stage_value = "standard"
    end
    if pricing_stage_value != family.pricing_stage
      report_event = Report::Event.where(name:"mark family pricing_stage #{pricing_stage_value}").last
      ::Report::ApiLog.log_event(user_id=nil,family.id,report_event.custom_id,options) rescue nil
    end
  end
  

  def get_family_user_max_trial_day(options={})
    family = self
    User.where(:member_id.in=>family.family_elder_member_ids).pluck(:trial_days).max || 7
  end

  def update_family_pricing_exemption(options={})
    family = self
    pricing_exemption_value = family.get_family_user_max_pricing_exemption(options)
    
    if pricing_exemption_value != family.pricing_exemption
      family.update_attribute(:pricing_exemption,pricing_exemption_value)
      report_event = Report::Event.where(name:"mark family pricing_exemption #{pricing_exemption_value}").last
      ::Report::ApiLog.log_event(user_id=nil,family.id,report_event.custom_id,options)
    end
  end

  def get_family_user_max_pricing_exemption(options={})
    family = self
    exemption_list = User.where(:member_id.in=>family.family_elder_member_ids).pluck(:pricing_exemption)
    if exemption_list.blank?
      exemption_list = [GlobalSettings::PricingExemptionOrder["basic"]]
    end
    value = exemption_list.map{|a| GlobalSettings::PricingExemptionOrder[a].to_i}.max
    GlobalSettings::PricingExemptionOrder.key(value)
  end

  def mark_family_activity_status(activity_status,options={})
    family = self
    user_id = options[:user_id]
    # no activity log logging
    family.update_attribute(:activity_status,activity_status)
    FamilyMember.where(:family_id=>family.id).update_all(:activity_status=>activity_status)
    children_ids = FamilyMember.where(:family_id=>family.id,:role.in=>FamilyMember::CHILD_ROLES).only(:member_id).pluck(:member_id)
    ChildMember.where(:id.in=>children_ids).update_all(:activity_status=>activity_status)
  end

  def mark_family_activity_status_v2(activity_status,options={})
    family = self
    # TODO: need to update logic for last activity status update
    #if activity_status.downcase == "active"
     # family.update_attribute(:last_activity_done_on,Time.now)
    #end
    if family.activity_status_v2.to_s.downcase != activity_status.downcase
      user_id = options[:user_id]
      options[:update_activity_status] = false
      family.update_attribute(:activity_status_v2, activity_status)
      report_event = options[:report_event] || Report::Event.where(name:"mark family #{activity_status.downcase}").last
      ::Report::ApiLog.log_event(user_id,family.id,report_event.custom_id,options) rescue nil
      FamilyMember.where(:family_id=>family.id).update_all(:activity_status_v2=>activity_status)
    end
  end

  private

  def assign_points
    Score.create(activity_type: "Family", activity_id: self.id, activity: "Created Family #{self.name}", point: Score::Points["Family"], member_id: self.owner.id, family_id: self.id)
  end  

  def delete_points
    Score.where(activity_type: "Family", member: self.owner.id, family_id: self.id).first.destroy
  end

  def verify_family_id
    rand_id = rand(10**10)
    if !Family.where(:family_uid =>rand_id).first
      rand_id
    else
      rand_id = rand(10**10)
      verify_family_id(rand_id)
    end
    self.family_uid = rand_id
  end  

  def generate_family_uid
    begin
      self.family_uid = rand(10**10)
    end while Family.where(family_uid: self.family_uid).present?
  end
  
  

  
  
  # for api 
  def self.family_to_json(family_data,api_version=1,current_user)
    Family.api_version = api_version
    Family.current_user = current_user
    case_value = api_version > 3
    case case_value
    when true then
      family_data.to_json(:methods => [:subscription,:family_member_list,:no_of_kids, :no_of_mothers,:no_of_elders,:no_of_pregent_member,:no_of_pregnancy] )
    else
      family_data.to_json(:methods => [:family_member_list,:no_of_kids, :no_of_mothers,:no_of_elders,:no_of_pregent_member,:no_of_pregnancy] )
    end
  end

end
