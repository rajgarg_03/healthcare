class SkipBasicSetupAction
  include Mongoid::Document
  include Mongoid::Timestamps

  field :member_id, :type => String 
  field :screen_name, :type => String
  field :screen_id, :type=> String 
  field :skip_count, :type => Integer , :default=> 0
  field :skipped_up_to, :type=> Time
  field :skipped, :type=> Boolean
  field :skipped_forever, :type=> Boolean, :default=>false

end
