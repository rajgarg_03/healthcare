class FacePlus
  include Mongoid::Document
  include Mongoid::Timestamps  
  
  has_many :pictures, :as => :imageable, :dependent => :destroy


  def self.get_response_from_face_plus_plus(picture)
    begin 
      api_secret = APP_CONFIG["faceplusplus_secret"] #"p5eNWLCB2TYFbBtk7rr5jAQp7TQ3qmP0"
      api_key =  APP_CONFIG["faceplusplus_key"] # "JhrTRDJmhECb3oy4yQ0sTd42FePDM3C4"
      face_detect_end_point = APP_CONFIG["face_plus_plus_face_detect_url"] # https://api-us.faceplusplus.com/facepp/v3/detect 
      face_analyze_end_point = APP_CONFIG["face_plus_plus_face_analyze_url"] # https://api-us.faceplusplus.com/facepp/v3/face/analyze
      skin_data =  []
      face_token = []
      image_path = picture.aws_url(:large)
      data = JSON.parse(`curl -X POST #{face_detect_end_point} -F api_key=#{api_key} -F api_secret=#{api_secret} -F image_url=#{image_path} -F return_landmark=2 -F return_attributes=gender,age`)
      raise Message::API[:error][:upload_image_for_skin_assessment] if data["error_message"].present?
      raise "Multiple face detected." if data["faces"].count > 1 
      raise "No face detected." if data["faces"].count < 1 
      data["faces"].each do |faces|
        face_token << faces["face_token"]
      end
      face_token.each do |face_token|
        skin_data << JSON.parse(`curl -X POST #{face_analyze_end_point} -F api_key=#{api_key} -F api_secret=#{api_secret} -F face_tokens=#{face_token} -F  return_attributes=skinstatus,ethnicity`)
      end
     msg = nil 
     skin_data
   rescue Exception=>e
    Rails.logger.info e.message
    Rails.logger.info "Error in get_response_from_face_plus_plus ........."
    Rails.logger.info face_token.inspect
    Rails.logger.info skin_data.inspect
    skin_data = nil
    msg = e.message
   end
   [skin_data,msg]
  end
  
  def self.get_skin_detail(picture)
    response,msg = get_response_from_face_plus_plus(picture)
    if response.present?
      begin
        data = response[0]["faces"][0]["attributes"]["skinstatus"]
        data["skin_health"] = data.delete("health")
        data
      rescue Exception=>e
        Rails.logger.info "Error in get_skin_detail..." 
        Rails.logger.info response.inspect
        raise Message::API[:error][:upload_image_for_skin_assessment]
      end
    else
      raise msg
    end
  end

end