# It will store All define role with access level .It will be system defined role
# name is name of role 
# access_level is integer value 
#for readonly  = 1, write = 2 , delete = 3, Delete permission will be highest role
#Admin, Dev, Analyst
class RoleForAdminPanel
	include Mongoid::Document  
  include Mongoid::Timestamps
  
  field :name, type:String
  field :access_level, type:Integer
  field :access_level_name, type: String
  
  validates_presence_of :access_level_name
  validates_presence_of :access_level
  validates_presence_of :name
  validates_uniqueness_of :name
  
  LevelmappingWithName = {"Read"=>1,"Write"=>2,"Delete"=>3}
end