class Subscription
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :start_date, type:Date # subscription purchase date
  field :last_renewal_date, type:Date # subscription last renewal date
  field :cancellation_date, type:Date      # subscription expiry date / cancel date 
  field :expiry_date, type:Date   # subscription expiry date 
  field :free_trial_end_date, type:Date # free trial end date
  field :subscription_valid_certificate, type:String # true/false/grace
  field :platform, type:String  # android,ios
  field :product_id, type:String
  field :buyer_name, type:String
  field :added_premium_tool_count, type:Integer, :default=>0 # added premium tool count that can be addedd for subscription 
  field :added_basic_tool_count, type:Integer, :default=>0 # added premium tool count that can be addedd for subscription. value 
  field :assigned_by, :type=>String, :default=>"user"         
  
  belongs_to  :member
  belongs_to  :family
  belongs_to :subscription_manager
  
  validates :family_id, uniqueness: true
  validates :family_id, presence: true  
  
  index({family_id:1})
  index({member_id:1})
  index({subscription_manager_id:1})

  # Used to avoid hard coded string 
  PlanType = {:basic=>"basic", :premium=>"premium"}
  # PlanCategory = {:free=>"Free", :gold=>"Gold",:silver=>"Silver"}
  Level = {"free"=>0, "gold"=>2,"silver"=>1 }  
  
  def self.is_free?(listing_order,subscription=nil,options={})
    listing_order == Subscription::Level["free"]
  end

  def self.is_premium?(listing_order,subscription=nil,options={})
    listing_order > Subscription::Level["free"]
  end

  #Get subscription listing order
  def subscription_listing_order(subscription_manager=nil)
    subscription = self
    return Subscription::Level["free"] if subscription.subscription_manager_id.blank?

    subscription_manager = subscription_manager || subscription.subscription_manager
    subscription_manager.subscription_listing_order
  end

  # check user can add tool in subscribed plan. max tool count is not exceeded for Subscription plan
  def is_add_tool_count_limit_exceed?(tool_type="premium",subscription_manager=nil,options={})
    subscription_plan = self
    family = Family.find(subscription_plan.family_id)
    return false if family.pricing_exemption == "all"
    subscription_manager = subscription_manager || subscription_plan.subscription_manager
    subscription_listing_order = subscription_manager.subscription_listing_order
    tool_count_to_exclude = options[:checking_for_show_subscription_status] == true ? 0 : 0    
    # -1 (tool count is unlimited)
    return true if Subscription.is_premium?(subscription_listing_order) && subscription_plan.expired?
    if tool_type == Nutrient::ToolType[:basic]
     status =  true #subscription_plan.get_max_basic_tool_count(subscription_manager) == -1 || subscription_plan.get_max_basic_tool_count(subscription_manager) > (subscription_plan.added_basic_tool_count - tool_count_to_exclude)
    else
      status = subscription_plan.get_max_premium_tool_count(subscription_manager) == -1  || subscription_plan.get_max_premium_tool_count(subscription_manager) > (subscription_plan.added_premium_tool_count - tool_count_to_exclude)
    end
     !status
  end

  def self.why_message_text(subscription_plan_to_check,subscribed_plan,current_user,valid_user_to_subscribe,check_valid_platform,valid_for_tool_subscription,not_exceed_active_tool_count,api_version=4)
    message = ""
    subscribed_plan_listing_order = subscribed_plan.subscription_listing_order
    subscription_plan_to_check_listing_order = subscription_plan_to_check.subscription_listing_order
    # subscribed for same plan or checking for free plan with valid active tool for free
    if (valid_user_to_subscribe && not_exceed_active_tool_count && Subscription.is_free?(subscribed_plan_listing_order) )
      return message = ""
    end
    
    if !valid_user_to_subscribe

      user_subscription = Subscription.where(member_id:current_user.id).premium_subscription(current_user,api_version).last
      if user_subscription.present? && !user_subscription.expired? 
        if user_subscription.id.to_s != subscribed_plan.id.to_s
          message = Message::Subscription[:m4] #"You can subscribe only one family."
        elsif !check_valid_platform
          message = Message::Subscription[:m5] #"This family is subscribed from <iPhone/Android>. Use <iPhone/Android> app to change subscription."
        else
          message = Message::Subscription[:m3] #"The family is subscribed by another member. Only he(she) can change subscription for this family"
        end
      else
        message = Message::Subscription[:m3] #"The family is subscribed by another member. Only he(she) can change subscription for this family"
      end
    elsif !check_valid_platform
      message = Message::Subscription[:m5] #"This family is subscribed from <iPhone/Android>. Use <iPhone/Android> app to change subscription."
    elsif !not_exceed_active_tool_count && subscription_plan_to_check_listing_order < subscribed_plan_listing_order
      message = Message::Subscription[:m7] #"You have to upgrade your subscription plan to activate this tool"
    elsif !valid_for_tool_subscription || (!not_exceed_active_tool_count && subscription_plan_to_check_listing_order >= subscribed_plan_listing_order)
      message = Message::Subscription[:m6] #"You have to upgrade your subscription plan to activate this tool"
    elsif (subscribed_plan.member_id.to_s == current_user.id.to_s && subscribed_plan_listing_order == subscription_plan_to_check_listing_order) ||  (not_exceed_active_tool_count && Subscription.is_free?(subscription_plan_to_check_listing_order) )
      return message = ""
    end
    message 
  end

  def end_or_renewal_date_text_with_color(can_cancel,can_renew,end_date)
    begin
      text = ""
      color_code = "#000000"
      if self.is_subscription_in_grace_period?
        color_code = "#d9401c"
        text = "Renewal is overdue"
      elsif can_cancel == true
        text = "Next renewal on " + end_date
      elsif can_renew == true
        text = "Subscription ends on " + end_date
      end
      
    rescue Exception=> e 
      Rails.logger.info e.message
    end
    [text,color_code]
  end
  
  def get_max_basic_tool_count(subscription_manager=nil)
    subscription_manager = subscription_manager || self.subscription_manager
    subscription_manager.max_basic_tool_count
  end

  def get_max_premium_tool_count(subscription_manager=nil)
    subscription_manager = subscription_manager || self.subscription_manager
    subscription_manager.max_premium_tool_count
  end
  
  def self.active_tool_count_not_exceed?(subscribed_plan,user_subscription)
      (user_subscription.get_max_basic_tool_count == -1 || user_subscription.get_max_basic_tool_count >= subscribed_plan.added_basic_tool_count)  && (user_subscription.get_max_premium_tool_count == -1 || user_subscription.get_max_premium_tool_count >= subscribed_plan.added_premium_tool_count)
  end

  def get_end_date
    self.expiry_date
  end

  def self.get_subscription_platform(current_user,subscribed_plan,subscribed_plan_manager)
    listing_order = subscribed_plan.subscription_listing_order(subscribed_plan_manager)
    subscribed_plan.platform
  end

  def self.is_valid_platform?(platform,param_platform)
    platform.blank? || (param_platform.downcase == platform.downcase) rescue false
  end

  def self.subscription_has_sufficient_level_for_tool?(user_subscription_manager,required_subscription_listing_order)
    required_subscription_listing_order.to_i <= user_subscription_manager.subscription_listing_order
  end

  def self.is_subscribed?(user_subscription,family_id,user_subscription_manager=nil)
    begin
      listing_order = user_subscription.subscription_listing_order(user_subscription_manager)
      user_subscription.family_id.to_s == family_id.to_s ||  Subscription.is_free?(listing_order)
    rescue Exception => e
      false
    end
  end
  
  def self.can_renew?(current_user,user_subscription,subscribed_plan,is_valid_user_to_subscribe,is_valid_platform,valid_for_tool_subscription,subscribed_plan_manager)
    subscribed_plan_listing_order = subscribed_plan_manager.subscription_listing_order
    # Can't renew if subscription is either free or not cancelled
    return false if Subscription.is_free?(subscribed_plan_listing_order) || subscribed_plan.subscription_valid_certificate == "true"

    if user_subscription["subscription_valid_certificate"] == "false" && !subscribed_plan.expired? && is_valid_user_to_subscribe   && is_valid_platform  && valid_for_tool_subscription
      true
    else
      false
    end
  end

  def self.can_cancel?(current_user,user_subscription,subscribed_plan,is_valid_user_to_subscribe,is_valid_platform,valid_for_tool_subscription,subscribed_plan_manager)
    subscribed_plan_listing_order = subscribed_plan_manager.subscription_listing_order
    # Can't cancel if subscription is either free or cancelled
    return false if Subscription.is_free?(subscribed_plan_listing_order) || subscribed_plan.subscription_valid_certificate == "false"

    if ["grace","true"].include?(user_subscription["subscription_valid_certificate"]) && is_valid_user_to_subscribe   && is_valid_platform  && valid_for_tool_subscription
      true
    else
      false
    end
  end
  
  def self.is_recommended?(can_subscribe,user_subscription_manager,tool_require_subscription_level,current_user,api_version)
   # show recommended from setting -> subscription 
   # return false if tool_require_subscription_level == Subscription::Level["free"]
   if can_subscribe
      # make gold plan recommended by default for now
      tool_require_subscription_level = SubscriptionManager.premium_subscriptions.valid_subscription_manager(current_user,api_version).first.subscription_listing_order
      tool_require_subscription_level == user_subscription_manager.subscription_listing_order
   else
    return false
   end
  end
  

  def self.free_trial_end_date_message(current_user,family_id,user_subscription,subscribed_plan,options={})
    begin
      family = Family.find(family_id)
      msg = nil
      free_access_to_premium_tool_status = System::Settings.has_free_access_to_premium_tool_status?(current_user,options)
      return [true,nil] if free_access_to_premium_tool_status
      free_trial_available_status = true
      return msg if user_subscription.id.to_s != subscribed_plan.id.to_s
      if family.pricing_exemption == "none"
        if options[:subscribed_plan_manager].present?
          subscribed_plan_manager = options[:subscribed_plan_manager]
          listing_order = subscribed_plan_manager.subscription_listing_order
        else
          listing_order = subscribed_plan.subscription_listing_order
        end
        if Subscription.is_free?(listing_order)
          family_trial_end_date = family.created_at + (family.trial_days).days
          if family_trial_end_date < Date.today
            msg = Message::Subscription[:subscription_trial_days_end_on] %{:trial_date=>family_trial_end_date.to_date.to_date4}
            free_trial_available_status = false
          else
            msg = Message::Subscription[:subscription_trial_days_end_at] %{:trial_date=>family_trial_end_date.to_date.to_date4}
          end
        end
      end
    rescue Exception => e 
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside free_trial_end_date_message")
      msg = nil
    end
    [free_trial_available_status, msg]
  end

  # formatted_data_for_api(subscription_manager,family_id,subscribed_plan,current_user,api_version,{:tool_require_subscription_level=>tool_require_subscription_level, :valid_user_to_subscribe=>valid_user_to_subscribe})
  def self.formatted_data_for_api(user_subscription,family_id,subscribed_plan,current_user,api_version,options={:tool_require_subscription_level=>0, :valid_user_to_subscribe=>false})
    
    user_subscription_manager = user_subscription.class.to_s == "SubscriptionManager" ? user_subscription : SubscriptionManager.find(user_subscription.subscription_manager_id)
    subscribed_plan_manager = SubscriptionManager.find(subscribed_plan.subscription_manager_id)
    
    platform = subscribed_plan.platform #Subscription.get_subscription_platform(current_user,subscribed_plan,subscribed_plan_manager)
    user_device_platform = current_user.device_platform
    params_platform = options[:platform].present? ? options[:platform] : user_device_platform
    check_valid_platform = platform.present? ? Subscription.is_valid_platform?(platform,params_platform): true 
     
    valid_user_to_subscribe = options[:valid_user_to_subscribe]
      
    #valid_plan_to_subscribe = user_subscription.subscription_listing_order > (subscribed_plan.subscription_listing_order rescue 0)
    valid_for_tool_subscription = Subscription.subscription_has_sufficient_level_for_tool?(user_subscription_manager,options[:tool_require_subscription_level])
    not_exceed_active_tool_count = Subscription.active_tool_count_not_exceed?(subscribed_plan,user_subscription)
    can_subscribe = (user_subscription_manager.subscription_listing_order != subscribed_plan_manager.subscription_listing_order && valid_user_to_subscribe   && check_valid_platform  && valid_for_tool_subscription && not_exceed_active_tool_count)
    is_recommended = Subscription.is_recommended?(can_subscribe,user_subscription_manager,options[:tool_require_subscription_level],current_user,api_version) 
    # click on why on device to see why subscription validation failed
    why_message = Subscription.why_message_text(user_subscription,subscribed_plan, current_user,valid_user_to_subscribe,check_valid_platform,valid_for_tool_subscription,not_exceed_active_tool_count,api_version)
    can_cancel = Subscription.can_cancel?(current_user, user_subscription,subscribed_plan, valid_user_to_subscribe,check_valid_platform,valid_for_tool_subscription,subscribed_plan_manager)
    can_renew = Subscription.can_renew?(current_user,user_subscription,subscribed_plan,valid_user_to_subscribe,check_valid_platform,valid_for_tool_subscription,subscribed_plan_manager)
    free_trial_available_status,free_trial_end_date_message = Subscription.free_trial_end_date_message(current_user,family_id,user_subscription,subscribed_plan,options)
    data = {
      
      "plan_name"=> user_subscription_manager.title,
      "is_subscribed"=> Subscription.is_subscribed?(user_subscription,family_id,user_subscription_manager),
      "is_recommended"=> is_recommended,
      "free_trial"=> free_trial_end_date_message,
      "can_subscribe" => can_subscribe,
      "plan_info"=> user_subscription_manager.plan_info,
      "why_message" => why_message,
      "plan_price"=>"0",
      "can_cancel" => can_cancel,
      "can_renew" => can_renew,
      "currency" =>"GBP",
      "free_trial_end_date_message" => free_trial_end_date_message
    }
    # data for other subscriptions available
    if user_subscription.class.to_s == "SubscriptionManager"
      start_date = user_subscription.plan_duration.present? ? Date.today.to_date4 : nil rescue nil
      end_date = user_subscription.calculate_end_date.to_date4 rescue nil
      product_id = user_subscription.subscription_product_id(params_platform)
      data.merge!({"subscription_identifier"=>user_subscription.identifier, "subscription_category"=>user_subscription.category, "start_date"=> start_date,"end_date"=> end_date, "product_id"=>product_id, "plan_duration"=>(user_subscription.plan_duration|| "Year"),"id"=>user_subscription.id , "subscription_manager_id"=>user_subscription.id}) 
    else
    # data for subscribed subscription 
      start_date = (user_subscription.start_date.to_date4 rescue nil)
      end_date = (user_subscription.get_end_date.to_date4 rescue nil)
      subscription_manager = user_subscription_manager
      product_id = subscription_manager.subscription_product_id(params_platform)
      # product_id = user_subscription.product_id.blank? ? subscription_manager.subscription_product_id(platform) : user_subscription.product_id
      plan_duration = (subscription_manager.plan_duration || "Year")
      subscription_end_or_renewal_date_text,text_color_code = user_subscription.end_or_renewal_date_text_with_color(can_cancel,can_renew,end_date)
      data.merge!({"subscription_identifier"=>subscription_manager.identifier,   "subscription_category"=>subscription_manager.category, "plan_duration" => plan_duration, "end_or_renewal_date_text_color"=>text_color_code, "end_or_renewal_date_text" => subscription_end_or_renewal_date_text, "start_date"=> start_date,"end_date"=> end_date, "platform" =>user_subscription.platform, "product_id"=>product_id,"subscription_id"=>user_subscription.id,"id"=>user_subscription.subscription_manager_id, "subscription_manager_id"=>user_subscription.subscription_manager_id}) 
    end
    data
  end
  
  def self.create_subscription(subscription_manager,api_version,current_user,family_id,platform,params={:receipt_end_date=>nil})
      # clear invalid and expired subscription for family
      Subscription.where(member_id:current_user.id,family_id:nil).delete_all rescue nil
      Subscription.where(family_id:family_id,:expiry_date.lt=>Time.now.in_time_zone.to_date).delete_all rescue nil
      Subscription.where(family_id:family_id,:expiry_date => nil).delete_all rescue nil
      Subscription.clear_member_id_from_expired_subscription(current_user,api_version)
      platform = (platform || params[:platform]).downcase rescue ""
      receipt_response = params[:receipt_response]
      product_id = subscription_manager.subscription_product_id(platform) rescue nil
      end_date = params[:receipt_end_date].present? ? params[:receipt_end_date] : nil
      end_date = nil if Subscription.is_free?(subscription_manager.subscription_listing_order)
      start_date = end_date.present? ? Date.today : nil
      tool_activated_in_family = Nutrient.active_tool_list_for_family(current_user,family_id)
      requested_subscription = Subscription.create(
        added_premium_tool_count: (tool_activated_in_family["premium"].count rescue 0),
        added_basic_tool_count: (tool_activated_in_family["basic"].count rescue 0),
        platform:platform,
        buyer_name:params[:buyer_name],
        product_id:product_id, 
        family_id: family_id, 
        member_id: (current_user.id rescue nil), 
        subscription_manager_id:subscription_manager.id, 
        start_date:start_date, 
        assigned_by: "user",
        expiry_date: end_date)
        requested_subscription.update_subscription_with_updated_receipt(receipt_response)
        # send_subscription_taken_email(current_user,family_id,requested_subscription)
        options = {:ip_address=>params[:ip_address]}
        Subscription.log_event_for_subscription(nil,requested_subscription,options)
    requested_subscription
  end

  def send_subscription_taken_email(current_user,family_id,subscription)
    begin
      premium_subscription_manager_ids = SubscriptionManager.premium_subscriptions.pluck(:id).map(&:to_s)
      premium_subscription_histroy_count = Subscription::History.where(:subscription_manager_id.in=>premium_subscription_manager_ids,:family_id=>family_id,:member_id=>current_user.id).count
      if premium_subscription_histroy == 1
        if Subscription.is_premium?(subscription_manager.subscription_listing_order)
          UserMailer.delay.subscription_taken(current_user,family_id)
        end
      end
    rescue Exception=> e 
      Rails.logger.info "mail not sent to subcscibed user after subscription"
      Rails.logger.info e.message
    end
  end

  def update_subscription(subscription_manager,api_version,current_user,family_id,platform,params={:receipt_end_date=>nil})
    Subscription.clear_member_id_from_expired_subscription(current_user,api_version)
    self.reload
    subscription = self
    subscription_obj = Subscription.find(subscription.id)
    platform = (platform || params[:platform]).downcase rescue ""
    product_id = subscription_manager.subscription_product_id(platform) rescue nil
    # check family exist
    family = Family.find(family_id)
    if Subscription.is_free?(subscription_manager.subscription_listing_order.to_i)
      params[:receipt_response] = {}
      params[:receipt_end_date] = nil
    end
    end_date = params[:receipt_end_date].present? ? params[:receipt_end_date] : nil
    start_date = end_date.present? ? Date.today : nil
    tool_activated_in_family = Nutrient.active_tool_list_for_family(current_user,family_id)
    
    buyer_name = params[:buyer_name].present? ? params[:buyer_name] : subscription.buyer_name
    receipt_response = params[:receipt_response]
    subscription.update_attributes(
      buyer_name: buyer_name,
      family_id: family_id,
      member_id: current_user.id,
      platform: platform,
      product_id:product_id, 
      subscription_manager_id:subscription_manager.id, 
      start_date:start_date, 
      expiry_date: end_date,
      assigned_by: "user", 
      added_basic_tool_count: (tool_activated_in_family["basic"].count rescue 0),
      added_premium_tool_count: (tool_activated_in_family["premium"].count rescue 0)
      )
     data = subscription.update_subscription_with_updated_receipt(receipt_response)
     # log event for subscripiton changes
    options = {:ip_address=>params[:ip_address]}
    if (subscription_obj.expiry_date != end_date) ||  (subscription_obj.subscription_valid_certificate != subscription.subscription_valid_certificate)
      Subscription.log_event_for_subscription(subscription_obj,subscription,options)
    end 
     # send_subscription_taken_email(current_user,family_id,subscription)
     data
  end

  # Add kpi data for subscription 
  def self.log_event_for_subscription(current_subscription,requested_subscription,options={})
    begin
      previous_subscription_listing_order = current_subscription.subscription_manager.subscription_listing_order.to_i rescue 0
      updated_subscription_listing_order = requested_subscription.subscription_manager.subscription_listing_order.to_i
      current_user = requested_subscription.member
      if requested_subscription.subscription_valid_certificate == "false"
        report_event = Report::Event.where(name:"subscription cancelled").last
      elsif previous_subscription_listing_order == 0 && updated_subscription_listing_order > 0 
        report_event = Report::Event.where(name:"subscription added").last
      elsif  updated_subscription_listing_order > previous_subscription_listing_order
        report_event = Report::Event.where(name:"subscription upgraded").last
      elsif updated_subscription_listing_order > 0 && updated_subscription_listing_order == previous_subscription_listing_order
        report_event = Report::Event.where(name:"subscription renew").last
      end
      ::Report::ApiLog.log_event(current_user.user_id,family_id,report_event.custom_id,options) rescue nil
    rescue Exception=>e 
      Rails.logger.info "Error inside log_event_for_subscription"
      Rails.logger.info e.message
    end
  end

  def self.clear_member_id_from_expired_subscription(current_user,api_version)
    free_subscription_manager =  SubscriptionManager.free_subscription
    Subscription.where(member_id:current_user.id, subscription_manager_id:free_subscription_manager.id).update_all(member_id:nil)
    Subscription.where(member_id:current_user.id).expired_subscription.update_all(member_id:nil)
  end
  
  # List all subscribed and available subscription for family 
  def self.get_subscriptions_list(family_id,member_id,api_version,tool_require_subscription_level=0,options={})
    current_user = Member.find(member_id)
    platform = options[:platform] || current_user.device_platform

    family_subscription_plan = options[:subscription] || Subscription.family_free_or_premium_subscription(family_id)
    
    #Create Free Subscription if not subscbribe or default subscription. User can subscribe plan
    family_subscription_plan =  family_subscription_plan ||  Subscription.find_create_subscription(current_user,family_id,platform,api_version)
    
    # check user is valid to upgrade/downgrade for subscription plan
    valid_user_to_subscribe,msg = Subscription.valid_user_to_subscribe_plan_for_family(member_id,family_id,platform,check_platform=false,api_version)
    
    subscribed_plan = family_subscription_plan 
    subscribed_subscription_manager = subscribed_plan.subscription_manager 
    subscribed_plan_listing_order = subscribed_subscription_manager.subscription_listing_order rescue Subscription::Level["free"]
    
    subscription_plans = {:subscribed_plan=>[],:other_plans=>[]}
    subscribed_platform  = nil
    subscribed_plan.update_attribute(:platform,platform) if Subscription.is_free?(subscribed_plan_listing_order)
    
    #subscribed plan for user 
    subscription_plans[:subscribed_plan] << Subscription.formatted_data_for_api(subscribed_plan,family_id,subscribed_plan,current_user,api_version,{:tool_require_subscription_level=>tool_require_subscription_level, :valid_user_to_subscribe=>valid_user_to_subscribe,:platform=>platform})
    subscribed_platform = subscribed_plan.platform
    is_subscription_expired = Subscription.is_premium?(subscribed_plan_listing_order) && subscribed_plan.expired?
    
    # other available plan for family
    SubscriptionManager.valid_subscription_manager(current_user,api_version).not_in(id:subscribed_subscription_manager.id).each do |subscription_manager|
      subscription_plans[:other_plans] << Subscription.formatted_data_for_api(subscription_manager,family_id,subscribed_plan,current_user,api_version,{:tool_require_subscription_level=>tool_require_subscription_level, :valid_user_to_subscribe=>valid_user_to_subscribe,:platform=>platform})
    end
    subcription_plan_member_id = subscribed_plan.member_id || member_id
    # return subscription plan(subscribed and other avaialble plan), subscribed_plan member id, subscribed_plan family id, subscription_listing_order
    [subscription_plans,subcription_plan_member_id, family_id, subscribed_platform,subscribed_plan_listing_order,is_subscription_expired ] 
  end



  

 # all tool in basic and premiumm categroy
  def self.tool_list(current_user,api_version,flatten_structure=false,tool_id=nil,options={})
    if flatten_structure == true
      data = []
      if options[:member_id].present?
        active_tool_ids = MemberNutrient.where(member_id:options[:member_id]).pluck(:nutrient_id)
      else
        active_tool_ids = []
      end
      if options[:member_id].present? && options[:show_all_tool] != "true"
        cp_valid_tools = Nutrient.valid_to_launch(current_user,api_version,{:show_feature_type=>"tools"}).in(id:active_tool_ids).order_by("position ASC")
        cp_valid_tools_name = cp_valid_tools.pluck(:title)
        adult_tools_name = Nutrient.adult_tool_valid_for_launch(current_user,api_version).pluck(:title) - ["Menu"]
        general_tools = []
         
        adult_tool_list = []
      else
        # cp_valid_tools = Nutrient.valid_to_launch(current_user,api_version,{:show_feature_type=>"tools"})
        cp_valid_tools = Nutrient.valid_to_launch(current_user,api_version,{:show_feature_type=>"all"}).where(:identifier.nin=>["Manage Tools"]).order_by("position asc")
        cp_valid_tools_name = cp_valid_tools.pluck(:title)
        adult_tools_name = Nutrient.adult_tool_valid_for_launch(current_user,api_version).pluck(:title) - ["Menu"]
        
        adult_tool_list =  Nutrient.adult_tool_valid_for_launch(current_user,api_version).in(title:(adult_tools_name - cp_valid_tools_name)).order_by("position asc")
        general_tools = [] #Nutrient.critriea_for_valid_to_launch(current_user,api_version,{:show_feature_type=>"general"})
      end
      (cp_valid_tools+adult_tool_list+general_tools) .each do |tool|
        is_child_tools = ["child","CP"].include?(tool.tool_for)
        is_pregnancy_tools = ["pregnancy","CP"].include?(tool.tool_for)
        is_adult_tools = adult_tools_name.include?(tool.title)
        is_premium = tool.tool_category == "premium"
        launch_status = tool.launch_status
        tool_activated = active_tool_ids.include?(tool.id)
        data << {:tool_activated=>tool_activated, :launch_status=>launch_status, :name=> tool.title, :identifier=>tool.identifier, :image=>tool.title, :tool_category=>tool.tool_for,:is_child_tools=>is_child_tools,:is_pregnancy_tools=>is_pregnancy_tools ,:is_adult_tools=>is_adult_tools,:is_premium=>is_premium}
      end
       sorted_data = data.sort_by{|a| (a[:tool_activated]== true ? 0 :1) }
      return sorted_data
    else
      data = {}
      premium_tool_status =  false
      data[:basic_tools] = {:child_tools=>[],:adult_tools=>[]}
      data[:premium_tools] = {:child_tools=>[],:adult_tools=>[]}
      basic_child_tools = Nutrient.valid_to_launch(current_user,api_version,{:show_feature_type=>"tools"}).where(tool_category:Subscription::PlanType[:basic]).in(tool_for:["pregnancy","CP","child"]).order_by("position ASC").pluck(:title).uniq
      basic_adult_tools =  Nutrient.adult_tool_valid_for_launch(current_user,api_version).where(tool_category:Subscription::PlanType[:basic]).order_by("position ASC").pluck(:title).uniq
      all_basic_tools = basic_adult_tools + basic_child_tools
      basic_child_tools.each do |tool|
         data[:basic_tools][:child_tools] << {:name=> tool, :image=>tool}
      end
       
      basic_adult_tools.each do |tool|
         data[:basic_tools][:adult_tools] << {:name=> tool, :image=>tool}
      end 
      Nutrient.valid_to_launch(current_user,api_version,{:show_feature_type=>"tools"}).where(tool_category:Subscription::PlanType[:premium]).in(tool_for:["pregnancy","CP","child"]).order_by("position ASC").pluck(:title).uniq.each do |tool|
         data[:premium_tools][:child_tools] << {:name=> tool, :image=>tool}
         premium_tool_status = true if tool.present?
      end 
      Nutrient.adult_tool_valid_for_launch(current_user,api_version).where(tool_category:Subscription::PlanType[:premium]).order_by("position ASC").pluck(:title).uniq.each do |tool|
         premium_tool_status = true if tool.present?
         data[:premium_tools][:adult_tools] << {:name=> tool, :image=>tool}
      end
      if !premium_tool_status
        [data[:basic_tools],nil]
      else
        [data[:basic_tools],data[:premium_tools]]
      end
    end
  end


  #options= {:response}
  def create_subscription_receipt(receipt_data,key,options={:platform=>nil})
    subscription = self
    platform =  options[:platform].downcase rescue nil
    if platform == "ios"
      status, response = SubscriptionReceipt::Ios.create_receipt(receipt_data,key,subscription.member_id,subscription.id,options)
    elsif platform == "android"
      status, response = SubscriptionReceipt::Android.create_receipt(receipt_data,key,subscription.member_id,subscription.id,options)
    end
    return response
  end

  # verify ios recipt data from Apple server/ Play store receipt
  def self.validate_subscription_receipt(receipt_data,key, platform,options={})
    is_valid = false
    status = false
    platform = platform.downcase rescue nil
     
    if platform == "ios"
       status, response,status_code = SubscriptionReceipt::Ios.validate_receipt(receipt_data,key,options)
    elsif platform == "android"
       status, response,status_code = SubscriptionReceipt::Android.validate_receipt(receipt_data,options)
    end
    if status && status_code == StatusCode::Status200 && response.present?
      is_valid = true
    else
      is_valid = false
      response = nil
    end
    [is_valid,response,status_code]  
    
  end

  def is_subscription_in_grace_period?(subscription_manager=nil)
    begin
       subscription = self
       subscription_manager = subscription_manager || subscription.subscription_manager
       return false if Subscription.is_free?(subscription_manager.subscription_listing_order)
       subscription.expiry_date < Date.today && subscription.grace_period_date(subscription_manager) >= Date.today
    rescue Exception=>e
      Rails.logger.info e.message
      false 
    end
  end

  def update_subscription_with_updated_receipt(receipt_response)
    subscription = self
     
    receipt_response = receipt_response.with_indifferent_access if receipt_response.present?
    subscription_manager = SubscriptionManager.find(subscription.subscription_manager_id)
    # if receipt_response is available then update subscription else assign free subscription 
    if receipt_response.present?
      auto_renew_status = receipt_response[:auto_renew_status].nil? && receipt_response[:cancellation_date].blank? ? true : receipt_response[:auto_renew_status]
      subscription.subscription_valid_certificate = false
       
      subscription.last_renewal_date = nil
      
      subscription.start_date = receipt_response[:original_purchase_date_ms].milli_second_string_to_date
      if receipt_response[:purchase_date_ms].present? && receipt_response[:original_purchase_date_ms] != receipt_response[:purchase_date_ms]
        subscription.last_renewal_date = receipt_response[:purchase_date_ms].milli_second_string_to_date rescue nil
      end
      cancellation_date = receipt_response[:cancellation_date].milli_second_string_to_date rescue nil
       
      # Deactivate all premium tool for cancelled subscription if its expired
      if cancellation_date.present? && (cancellation_date < Date.today rescue false)
        subscription.deactivate_premium_tools_for_expire_subscription
      end
      user_cancelled_subscription_date =  (cancellation_date || Date.today) if auto_renew_status == false 
      # expiry date same as cancellation_date if cancellation_date present
      subscription.expiry_date = cancellation_date || (receipt_response[:expires_date_ms].milli_second_string_to_date rescue nil)
      subscription.cancellation_date = cancellation_date || subscription.cancellation_date || user_cancelled_subscription_date
      subscription.free_trial_end_date = nil
      # set free trial date equal to expiry date if is_trial_period is true in receipt response
      subscription.free_trial_end_date = subscription.expiry_date if receipt_response[:is_trial_period].to_s == "true"
      # grace period date will be same as cancellation date if cancellation date present
      if receipt_response[:cancellation_date].present? || !auto_renew_status || user_cancelled_subscription_date.present?
        subscription.subscription_valid_certificate = "false"
      elsif receipt_response[:cancellation_date].blank? && subscription.is_subscription_in_grace_period?(subscription_manager)
        subscription.subscription_valid_certificate = "grace"
      elsif !subscription.expired?
        subscription.subscription_valid_certificate = "true"
        subscription.cancellation_date = nil
      end 
      
      subscription.save!
    elsif Subscription.is_free?(subscription_manager.subscription_listing_order.to_i)
        subscription.subscription_valid_certificate = "true"
        subscription.free_trial_end_date = nil
        subscription.last_renewal_date = nil
        subscription.expiry_date = nil
        subscription.cancellation_date = nil
        subscription.save!
        # Deactivate all premium tools
        subscription.deactivate_premium_tools_for_expire_subscription
    end
    Subscription::History.create_record(subscription,receipt_response)
    subscription
  end
  
  def delete_subscription
    subscription_id  = self.id
    SubscriptionReceipt::Ios.where(subscription_id:subscription_id).delete_all
    SubscriptionReceipt::Android.where(subscription_id:subscription_id).delete_all
    self.delete
  end

  def grace_period_date(subscription_manager=nil)
    subscription = self
    subscription_manager = subscription_manager || subscription.subscription_manager
    return nil if Subscription.is_free?(subscription_manager.subscription_listing_order)
    if subscription.subscription_valid_certificate == "false"
      subscription.expiry_date
    else
      subscription.expiry_date + (eval(subscription_manager.grace_period) rescue 0.days)
    end
  end

  # check subscription is expired or not . returns true/false
  def expired?(subscription_manager=nil)
    subscription =  self 
    subscription_manager = subscription_manager || subscription.subscription_manager
    # if subscription is free or basic 
    return false if Subscription.is_free?(subscription_manager.subscription_listing_order)
    current_date = Date.today
    
    # check for valid cancellation date 
    return true if subscription.subscription_valid_certificate == "false" && current_date > subscription.expiry_date
    # check for valid expiry or free trail date 
    if current_date <= subscription.expiry_date || (current_date <= subscription.free_trial_end_date rescue false) || current_date <= subscription.grace_period_date(subscription_manager)
      status = false
    else 
       status = true
    end
    status
  end
  # returns active record association object
  def self.not_expired
    current_date = Date.today
    free_subscription_manager = SubscriptionManager.free_subscription
    condition_list = [{subscription_manager_id:free_subscription_manager.id} ,{:free_trial_end_date.gte=>current_date}]
    SubscriptionManager.premium_subscriptions.where(:supported_api_version.ne=> -1).each do |subscription_manager|
      grace_period = eval(subscription_manager.grace_period)
      condition_list << {:subscription_valid_certificate=> { "$in" => ["grace","true"]} , :expiry_date.gte=> (Date.today - grace_period),:subscription_manager_id=>subscription_manager.id }
      condition_list << {:subscription_valid_certificate => "false", :expiry_date.gte=> (Date.today),:subscription_manager_id=>subscription_manager.id }
    end

    self.where({"$or"=>condition_list})
  end

  def self.expired_subscription
    current_date = Date.today
    condition_list = [{:subscription_valid_certificate => "false", :expiry_date.lt=> current_date} ,{:free_trial_end_date.lt =>current_date}]
    SubscriptionManager.premium_subscriptions.where(:supported_api_version.ne=> -1).each do |subscription_manager|
      grace_period = eval(subscription_manager.grace_period)
      condition_list << {:expiry_date.lt=> (current_date - grace_period),:subscription_manager_id=>subscription_manager.id }
    end
    free_subscription_manager_id = SubscriptionManager.free_subscription.id
    self.where({"$or"=>condition_list}).not_in(subscription_manager_id:free_subscription_manager_id)
  end
   
  # find member family subcription
  def self.find_family_member_subscription(member_id,family_id)
     where(family_id:family_id,member_id:member_id).not_expired
  end

  def self.find_family_subscription(family_id)
     Subscription.where(family_id:family_id).not_expired
  end
  def self.free_subscription
    subscription_manager = SubscriptionManager.free_subscription
    where(subscription_manager_id:subscription_manager.id)
  end

  def self.premium_subscription(current_user,api_version)
    subscription_manager_ids = SubscriptionManager.premium_subscriptions.valid_subscription_manager(current_user,api_version).map(&:id)
    where({'subscription_manager_id' => { "$in" => subscription_manager_ids} })
  end
  def self.default_subscription(family_id)
    subscription_manager = SubscriptionManager.free_subscription
    family_existing_subscription = Subscription.where(family_id:family_id).last
    
    tool_activated_in_family = Nutrient.active_tool_list_for_family(current_user=nil,family_id)
    added_basic_tool_count = (tool_activated_in_family["basic"].count rescue 0)
    added_premium_tool_count = (tool_activated_in_family["premium"].count rescue 0)
    
    if family_existing_subscription.present? 
      if family_existing_subscription.expired?
        family_existing_subscription.deactivate_premium_tools_for_expire_subscription
        family_existing_subscription.update_attributes({:product_id=>nil,added_basic_tool_count:added_basic_tool_count,added_premium_tool_count:added_premium_tool_count, subscription_manager_id:subscription_manager.id,family_id:family_id})
        family_existing_subscription
      else
        family_existing_subscription
      end
    else
      Subscription.new(added_basic_tool_count:added_basic_tool_count,added_premium_tool_count:added_premium_tool_count, subscription_manager_id:subscription_manager.id,family_id:family_id)
    end
  end

  def self.family_free_or_premium_subscription(family_id)
    Subscription.find_family_subscription(family_id).last || Subscription.where(:family_id=>family_id).not_expired.last || Subscription.default_subscription(family_id)
  end

  def self.user_premium_or_free_subscription(member_id)
    find_member_subscription(member_id).last || Subscription.where(member_id:member_id).not_expired.last || Subscription.default_subscription(nil)
  end
  
  def self.find_member_subscription(member_id)
     Subscription.where(member_id:member_id).not_expired
  end

  def self.find_create_subscription(current_user=nil,family_id=nil,platform, api_version)
     # Subscribe to free subscription manager if not subscribe to any other
     
      free_subscription_manager = SubscriptionManager.valid_subscription_manager(current_user,api_version).free_subscription
      subscription = nil
      #check family exist
      family = Family.find(family_id) if family_id.present?
      if current_user.present? && family_id.present?
        subscription = Subscription.find_family_member_subscription(current_user.id,family_id).last || Subscription.where(member_id:current_user.id,family_id:family_id).free_subscription.last
      end
      return subscription if subscription.present?
      
      if family_id.present?
        subscription = Subscription.family_free_or_premium_subscription(family_id)
      elsif current_user.present?
          subscription = Subscription.find_member_subscription(current_user.id).last || Subscription.where(member_id:current_user.id).free_subscription.last
      end

    subscription = (subscription || Subscription.create_subscription(free_subscription_manager,api_version,current_user,family_id,platform))
    subscription
  end

  def self.subscription_screen(current_user,tool_id,required_subscription_level,family_id,api_version=1,source=nil)
    begin
      tool = Nutrient.find(tool_id)
      return nil if source.to_s.downcase == "manage tools"
      required_subscription_manager = SubscriptionManager.valid_subscription_manager(current_user,api_version).where(:subscription_listing_order.gte =>required_subscription_level).order_by("subscription_listing_order asc").first
      required_subscription_manager = required_subscription_manager || SubscriptionManager.valid_subscription_manager(current_user,api_version).order_by("subscription_listing_order desc").first
      subscription_screen_data = {
        :screen_name => tool.info_screen,
        :subscription_plan => {
          :plan_type=> required_subscription_manager.plan_type,
          :title => required_subscription_manager.title,
          :duration => required_subscription_manager.plan_duration,
          :ios_product_id => required_subscription_manager.ios_product_id,
          :android_product_id => required_subscription_manager.android_product_id
        }
      }
    rescue Exception => e
      subscription_screen_data = nil
    end
  subscription_screen_data
  end

  def self.add_default_subscription(current_user,family_id,platform,api_version)
    subscription =  find_create_subscription(current_user,family_id,platform,api_version)
  end

  def self.valid_user_to_subscribe_plan_for_family(member_id,family_id,platform=nil,check_platform=false,api_version=4)
    msg = nil
    status = true
     current_user = Member.find(member_id)
    user_device_platform = platform.present? ? platform : current_user.device_platform
    user_premium_subscription = Subscription.find_member_subscription(member_id).premium_subscription(current_user,api_version).last
    # check user not subscribed for multiple family
    if(user_premium_subscription.present? && user_premium_subscription.family_id.to_s != family_id.to_s)
      status = false
      msg = Message::Subscription[:m4]
      return [status,msg]
    end
    # check if subscription for family is taken by other user
    family_premium_subscription =  Subscription.find_family_subscription(family_id).premium_subscription(current_user,api_version).last
    if family_premium_subscription.present? && family_premium_subscription.member_id.to_s != member_id.to_s
      status = false
      msg = Message::Subscription[:m3]
      return [status,msg]
    end 
    #check user has subscription for family with same device
    family_user_subscription = Subscription.find_family_member_subscription(member_id,family_id).last || Subscription.new
    if family_user_subscription.present? && check_platform == true
      # if user has Primum subscription with same device or basic plan with same/different platform
      if family_user_subscription.platform == user_device_platform || Subscription.is_free?(family_user_subscription.subscription_listing_order.to_i)
        return [true , nil ]
      else
        return [false,Message::Subscription[:m5]]
      end
    end
    family_subscription = Subscription.family_free_or_premium_subscription(family_id) || Subscription.new
    user_subscription = user_premium_subscription || Subscription.new
    #   User and Family Subscribed for default(Free) subscription
    if (Subscription.is_free?(family_subscription.subscription_listing_order.to_i) && Subscription.is_free?(user_subscription.subscription_listing_order.to_i))
      msg = nil
      return [true,msg]
    end
    [status,msg]
  end

  #Update added too count in subscription
  def update_subscription_tool_count(tool,family_id,tool_action="activated")
     subscription = self
    if tool_action == "activated"
      if  tool.tool_category == Nutrient::ToolType[:premium]
        subscription.added_premium_tool_count = subscription.added_premium_tool_count + 1
      else
        subscription.added_basic_tool_count = subscription.added_basic_tool_count + 1
      end
    end
    if tool_action == "deactivated"
      if  tool.tool_category == Nutrient::ToolType[:premium]
        subscription.added_premium_tool_count = subscription.added_premium_tool_count - 1 if subscription.added_premium_tool_count > 0
      else
        subscription.added_basic_tool_count = subscription.added_basic_tool_count - 1 if subscription.added_basic_tool_count > 0
      end
    end
    subscription.save
  end


  def deactivate_premium_tools_for_expire_subscription(premium_tool_ids=[])
    subscription = self
    tool_ids_to_deactive = premium_tool_ids.present? ? premium_tool_ids : Nutrient.premium_tools.pluck(:id)
    ios_app_version_in_number = 2001001 # 2.1.2
    android_app_version_in_number = 2001000 # 2.1.0
    family_id = subscription.family_id
    family_member_with_api_version_greater_than_4 = Family.elder_members_with_app_version(family_id,ios_app_version_in_number,android_app_version_in_number)
    if !family_member_with_api_version_greater_than_4.present?
      child_member_ids = FamilyMember.where(:family_id=>subscription.family_id).in(:role => FamilyMember::CHILD_ROLES).pluck(:member_id)
      MemberNutrient.in(member_id:child_member_ids).in(nutrient_id:tool_ids_to_deactive).delete_all
      subscription.added_premium_tool_count = 0
      subscription.save
    end
  end
end
