class Message
 include Mongoid::Document
 def self.method_missing(m, *args)
    "You will be provided with a message"
  end
 def method_missing(m, *args)
    "You will be provided with a message"
  end
  def self.api_message_join_family(family,success=true)
  end
  Alexa = {add_child_msg:"Please add child to your family",
           match_not_found:"I could not understand, please provide me, child name"

  } 

  API = { :success =>
          { update_immunisation: "I have updated the immunisations details",
            add_immunisation:"I have added all the selected immunisations", 
            delete_immunisation:"I have deleted the immunisation",
            create_immunisation:"I have added the new immunisation in your list", 
            delete_jab:"I have deleted the jab", 
            update_jab:"I have updated the jab details", 
            save_jab:"I have added a new jab",
            event_delete:"I have deleted your calendar entry", 
            update_event:"I have updated your calendar entry", 
            event_create:"I have added your calendar entry",
            delete_document: "I have deleted the document",
            update_timeline: "I have updated the post",
            create_timeline: "I have created the new post", 
            delete_timeline:"I have deleted the post",
            create_system_timeline: "", 
            deacitvate_nutrient:"I have deactivated the tool", 
            acitvate_nutrient: "I have activated the tool",
            milestone_delete:"I have marked the milestone unaccomplished", 
            milestone_update:"I have updated the milestone details",
            milestone_create: "I have added the selected milestones to accomplished list",
            get_health_detail: "Health found", 
            health_update_record: "I have updated the record",
            health_delete_record: "I have deleted the record",
            create_elder:"I have sent an email to the invitee",
            elder_already_exist: "User already is part of ",
            remove_elder:"I have deleted the adult member",
            update_elder:"I have updated your details",
            update_current_member: "I have updated your details",
            delete_child:"I have removed the child", 
            update_child: "I have updated the details" ,
            create_child: "I have added the new child in " ,
            delete_family: "I have removed the family", 
            update_family: "I have updated the family details", 
            create_famly: "I have created the new family",
            create_preg: "",
            delete_preg: "I have removed the pregnancy",
            update_preg: "I have updated the details",
            update_preg_child: "I have updated the details",
            mark_preg_complete: "",
            join_family_request: "I have sent your request again" ,
            resend_invitation: "I have sent your request again", 
            set_default_cover_image: "Default cover image is set successfully",
            update_setting: "I have updated your settings",
            password_update: "Your password has been changed successfully",
            create_medical_event: "I have added the new test",
            update_medical_event: "I have updated the test",
            prenatal_upload_image: "",
            delete_parental_test: "I have deleted the test",
            add_component_to_medical_event: "I have added all the selected components",
            delete_component: "I have deleted the component",
            create_new_component: "I have added the new component in your list",  
            delete_component_from_system:"",
            add_document: "I have added the document",
            update_document: "I have updated the document",
            delete_doc_attach: "Your attachment has been deleted", 
            delete_elder_invitation:"I have deleted your request", 
            decline_invitation:"You have successfully declined the request",
            accept_invitation:"You have successfully accepted the invite",
            upload_cover_image:"I have updated your cover image", 
            upload_profile_picture: "I have updated the details",
            save_role:" Your joining request has been sent to the creator of the family",
            upload_image:"Image has been uploaded",

            :password_invalid => "Sorry, we could not sign you in. Please check your sign in details and try again",
            :email_not_registered => "Sorry, we could not sign you in. Please check your sign in details and try again",
            :email_not_verified => "Please verify your Email ID before proceeding further. Contact 'Support@nurturey.com' for any assistance",
            :preg_health_create => "I have added the detail",
            :preg_health_update => "I have updated the detail",
            :preg_health_delete =>   "I have deleted the record",
            :preg_hemoglob_delete => "I have deleted the record",
            :preg_hemoglob_create => "I have updated the detail",
            :preg_hemoglob_update => "I have updated the detail",

            :preg_pregbp_delete => "I have deleted the record",
            :preg_pregbp_create => "I have updated the detail",
            :preg_pregbp_update => "I have updated the detail",
            :enable_push_notification => "I have updated the detail",
            :upgrade_subscription => "I have updated the detail",
            :update_tooth => 'I have updated the tooth details',
            :add_tooth => 'I have added the tooth details',
            :delete_tooth => 'I have deleted the tooth details',
            :update_quiz_level => "I have updated %{child_name}'s Nurturey maths level",
            :add_kick_count => "I have added kick count",
            :delete_kick_count => "I have deleted the detail",
            :add_kick_counter_schedule => "I have added schedule for kick count ",
            :update_kick_counter_schedule => "I have updated the detail",
            :update_kick_count => "I have updated the detail",
            :eye_sight => "I have added the detail",
            :update_eye_sight => 'I have updated the detail',
            :delete_eye_sight => 'I have deleted the detail',
            :add_test_to_checkup_plan => "I  have added all the selected test to checkup plan",
            :delete_checkup_plan=> "I have deleted the record",
            :add_checkup_plan => "I have added the new checkup plan",
            :update_checkup_plan => "I have updated checkup plan details",
            :add_test_to_system=> "I have added details",
            :delete_checkup_tests=>'I have deleted the test',
            :delete_checkup =>'I have deleted the checkup plan',
            :upload_image_for_skin_assessment=> 'I have updated the details',
            :delete_skin_assessment => 'I have deleted the record',
            :add_quiz_details => 'I have added the details',
            :update_faq_feedback => 'I have updated the details',
            :setup_clinic=> "Clinic setup successfully",
            :cvt_record_fetched=> "Vision test record(s) fetched",
            :vision_event_added=> "User vision event added", 
            :update_family_purchase_for_cvtme=> "I have updated cvtme purchase detail" 
          },
          
          :error=>
          { update_immunisation:"Sorry! I could not fulfill your request due to an error. Please try again", 
            add_immunisation:"Sorry! I could not fulfill your request due to an error. Please try again",
            delete_immunisation:"Sorry! I could not fulfill your request due to an error. Please try again", 
            create_immunisation: "Sorry! I could not fulfill your request due to an error. Please try again", 
            delete_jab: "Sorry! I could not fulfill your request due to an error. Please try again", 
            update_jab: "Sorry! I could not fulfill your request due to an error. Please try again", 
            save_jab: "Sorry! I could not fulfill your request due to an error. Please try again",
            update_event: "Sorry! I could not fulfill your request due to an error. Please try again", 
            event_create: "Sorry! I could not fulfill your request due to an error. Please try again",
            event_delete: "Sorry! I could not fulfill your request due to an error. Please try again",
            delete_document: "Sorry! I could not fulfill your request due to an error. Please try again",
            update_timeline: "Sorry! I could not fulfill your request due to an error. Please try again",  
            create_timeline: "Sorry! I could not fulfill your request due to an error. Please try again",  
            delete_timeline: "Sorry! I could not fulfill your request due to an error. Please try again",
            create_system_timeline: " ",  
            deacitvate_nutrient: "Sorry! I could not fulfill your request due to an error. Please try again", 
            acitvate_nutrient: "Sorry! I could not fulfill your request due to an error. Please try again",
            milestone_delete:"Sorry! I could not fulfill your request due to an error. Please try again", 
            milestone_update:"Sorry! I could not fulfill your request due to an error. Please try again",
            milestone_create:"Sorry! I could not fulfill your request due to an error. Please try again",
            health_update_record:"Sorry! I could not fulfill your request due to an error. Please try again",
            health_delete_record: "Sorry! I could not fulfill your request due to an error. Please try again",
            get_health_detail: "Sorry! I could not fulfill your request due to an error. Please try again",
            update_elder:"Sorry! I could not fulfill your request due to an error. Please try again", 
            create_elder:"Sorry! I could not fulfill your request due to an error. Please try again", 
            remove_elder:"Sorry! I could not fulfill your request due to an error. Please try again",
            update_current_member: "Sorry! I could not fulfill your request due to an error. Please try again",
            delete_child:"Sorry! I could not fulfill your request due to an error. Please try again",
            update_child: "Sorry! I could not fulfill your request due to an error. Please try again",
            create_child: "Sorry. I could not add the new child. Please try again",
            delete_family: "Sorry! I could not fulfill your request due to an error. Please try again",
            update_family: "Sorry! I could not fulfill your request due to an error. Please try again",
            create_famly: "Sorry! I could not fulfill your request due to an error. Please try again",
            create_preg: "",
            delete_preg: "Sorry! I could not fulfill your request due to an error. Please try again",
            update_preg: "Sorry! I could not fulfill your request due to an error. Please try again",
            update_preg_child: "Sorry! I could not fulfill your request due to an error. Please try again",
            mark_preg_complete: "Sorry! I could not fulfill your request due to an error. Please try again",
            resend_invitation:"Sorry! I could not fulfill your request due to an error. Please try again",
            delete_elder_invitation:"Sorry! I could not fulfill your request due to an error. Please try again", 
            decline_invitation:"Sorry! I could not fulfill your request due to an error. Please try again", 
            accept_invitation:"Sorry! I could not fulfill your request due to an error. Please try again",
            upload_profile_picture: "Sorry! I could not fulfill your request due to an error. Please try again", 
            upload_cover_image: "Sorry! I could not fulfill your request due to an error. Please try again",  
            update_setting: "Sorry! I could not fulfill your request due to an error. Please try again",
            password_update: "Sorry! I could not fulfill your request due to an error. Please try again",
            
            create_medical_event:"Sorry! I could not fulfill your request due to an error. Please try again",
            prenatal_upload_image: "Sorry! I could not fulfill your request due to an error. Please try again",
            delete_parental_test: "Sorry! I could not fulfill your request due to an error. Please try again",
            update_medical_event: "Sorry! I could not fulfill your request due to an error. Please try again",
            
            add_component_to_medical_event:"Sorry! I could not fulfill your request due to an error. Please try again",
            delete_component: "Sorry! I could not fulfill your request due to an error. Please try again",
            delete_component_from_system: "Sorry! I could not fulfill your request due to an error. Please try again",
            create_new_component: "Sorry! I could not fulfill your request due to an error. Please try again",
            set_default_cover_image: "Sorry! I could not fulfill your request due to an error. Please try again",
            delete_doc_attach: "Sorry! I could not fulfill your request due to an error. Please try again",
             
            save_role: "Sorry! I could not fulfill your request due to an error. Please try again",
            upload_image: "Sorry! I could not fulfill your request due to an error. Please try again",
            
            add_document: "Sorry! I could not fulfill your request due to an error. Please try again",
            update_document: "Sorry! I could not fulfill your request due to an error. Please try again",
            
            :preg_health_create => "Sorry! I could not fulfill your request due to an error. Please try again",
            :preg_health_update => "Sorry! I could not fulfill your request due to an error. Please try again",
            :preg_health_delete => "Sorry! I could not fulfill your request due to an error. Please try again",    
            
            :preg_hemoglob_delete => "Sorry! I could not fulfill your request due to an error. Please try again",
            :preg_hemoglob_create => "Sorry! I could not fulfill your request due to an error. Please try again",
            :preg_hemoglob_update => "Sorry! I could not fulfill your request due to an error. Please try again",

            :preg_pregbp_delete => "Sorry! I could not fulfill your request due to an error. Please try again",
            :preg_pregbp_create => "Sorry! I could not fulfill your request due to an error. Please try again",
            :preg_pregbp_update => "Sorry! I could not fulfill your request due to an error. Please try again",
            :enable_push_notification => "Sorry! I could not fulfill your request due to an error. Please try again",
            :upgrade_subscription => "Sorry! I could not fulfill your request due to an error. Please try again",
            :update_tooth => 'Sorry! I could not fulfill your request due to an error. Please try again',
            :add_tooth => 'Sorry! I could not fulfill your request due to an error. Please try again',
            :tooth_type_check => 'Sorry! I could not fulfill your request due to an error. Please try again',
            :tooth_chart_manager_check => 'Sorry! I could not fulfill your request due to an error. Please try again',
            :tooth_child_id_check => 'Sorry! I could not fulfill your request due to an error. Please try again',
            :delete_tooth => 'Sorry! I could not fulfill your request due to an error. Please try again',
            :update_quiz_level => 'Sorry! I could not fulfill your request due to an error. Please try again',
            :add_kick_count => 'Sorry! I could not fulfill your request due to an error. Please try again',
            :delete_kick_count => 'Sorry! I could not fulfill your request due to an error. Please try again',
            :add_kick_counter_schedule => 'Sorry! I could not fulfill your request due to an error. Please try again',
            :update_kick_counter_schedule => 'Sorry! I could not fulfill your request due to an error. Please try again',
            :update_kick_count => 'Sorry! I could not fulfill your request due to an error. Please try again',
            :start_and_end_date_empty => "Please enter kick count's start time and end time",
            :kick_count_time_comp => "Start time should be less than end time",
            :kick_count_count_empty => "Please provide kick count's count",
            :kick_count_start_date_null => "Please provide start date",
            :eye_sight => "Sorry! I could not fulfill your request due to an error. Please try again",
            :update_eye_sight => 'Sorry! I could not fulfill your request due to an error. Please try again',
            :delete_eye_sight => 'Sorry! I could not fulfill your request due to an error. Please try again',
            :eye_date_validation => 'Date is missing, please provide it',
            :eye_data_validation => 'Please provide atleast 1 parameter',
            :add_test_to_checkup_plan => "Sorry! I could not fulfill your request due to an error. Please try again",
            :delete_checkup_plan=> "Sorry! I could not fulfill your request due to an error. Please try again",
            :add_checkup_plan => "Sorry! I could not fulfill your request due to an error. Please try again",
            :update_checkup_plan => "Sorry! I could not fulfill your request due to an error. Please try again",
            :add_test_to_system => "Sorry! I could not fulfill your request due to an error. Please try again",
            :checkup_planner_test_title_missing => "Please enter test's title",
            :tite_checkup_plan_missing => "Please enter checkup's title",
            :date_checkup_plan_missing => "Please enter checkup's date",
            :system_ids_blank_checkup_plan => "Please select at least one test",
            :delete_checkup_tests => "Sorry! I could not fulfill your request due to an error. Please try again",
            :delete_checkup =>"Sorry! I could not fulfill your request due to an error. Please try again",
            :upload_image_for_skin_assessment=> "Sorry! I could not fulfill your request due to an error. Please try again",
            :delete_skin_assessment => "Sorry! I could not fulfill your request due to an error. Please try again",
            :setup_clinic=> "Failed to setup clinic",
            :cvt_record_fetched=> "Sorry! I could not fulfill your request due to an error. Please try again",
            :update_family_purchase_for_cvtme=> "Sorry! I could not fulfill your request due to an error. Please try again",
            :get_syndicated_content=> "Sorry! I could not fulfill your request due to an error. Please try again",
            :orion_login_message => "Sorry! I could not fulfill your request due to an error. Please try again"
                  
          }
        }

GLOBAL = {:success =>
          {
            :milestone_create=>"I have added the selected milestones to accomplished list",
            :health_create_record=>"I have added the record",
            :verify_elder_for_family_reject=>"You have successfully declined the request" ,
            :verify_elder_for_family_accept=>"You have successfully declined the request" ,
            :verify_elder_reject=>"You have rejected the request" ,
            :verify_elder_accpet=>"You have rejected the request" ,
            :user_destroy=> "User deleted", 
            :create_timeline=> "Your post has been created", 
            :password_create=> "You will receive an email if we have a record of your account. Don't forget to check your spam folder", 
            :password_forgot_password=>"Please request for forgot Password again", 
            :password_update=> "Your password has been changed successfully", 
            :password_blank=>"Password can't be blank", 
            :password_mismatch=>"You have entered incorrect old password",            
            :facebook=> "Post updated on Facebook successfully", 
            :add_multiple=> "Selected milestones have been added to the accomplished list", 
            :dont_want_add_spouse => "Sucessfully proccess the request",
            :family_user_register=>"Please register with us to proceed", 
            
            :create_elder=>"An email has been sent to the invitee. Visit <a href='families' data-remote=true>Manage Family</a>",
            :create_child=>"Your child has been added. Visit <a href='families' data-remote=true>Manage Family</a>", 
            :send_invites=> "Thanks, email invitations have been sent to your friends", 
            :jab_detail=>"No jab found", 
             
            :unsubscribe=>"unsubscribe successfully ", 
            :forgot_password=>"You will receive an email if we have a record of your account. Don't forget to check your spam folder.<br/><a href='/users/sign_in' class='btn btn-type2 login_btn'>Login</a>",
            :family_update=>'Family was successfully updated', 
            :search_family =>"No family found, please try again", 
            :doc_create=> "Your document has been uploaded", 
            :destroy_album=>"Album destroy successfully",
            :destroy_album_photo=>"Photo destroy successfully",
            :add_photo=>" Photo added successfully "},
        :error =>
          { 
            :milestone_create=>"Sorry! I could not fulfill your request due to an error. Please try again",
            :user_destroy=>"Can't delete yourself",
            :associate_to_family=> "Family ID not found" , 
            :unregistered_email=> "The email you entered is not registered", 
            :add_photo=>"There were some problems uploading the photo"}
          }
DEVISE = {:m1 =>"Please verify your email ID through the link we have emailed to you. Do check your spam folder, if you haven't received the email",
  :m2=>"You have tried to register using this email in the past, but did not verify your email. We are sending the verification email again. Kindly verify your email to continue",
  :m3 =>"Your email has been verified. As we are offering only limited number of registrations for Nurturey Beta, you will be able to login after we approve your registration. You will shortly receive an email from Nurturey's support team. In case of any questions. please write to support@nurturey.com",
  :m4 => "This email ID is already registered with us. Please go to Login",
  :m5 => "Please fill valid ",
  :m6=> "Please fill a valid Email ID and Password",
  :m7=> "Please fill a valid Email ID and Password",
  :m8=> "Verification email has been sent to your email",
  :m9 => "No user found with this email",
  :m10=> "I have resent the verification email to you. Don't forget to check your spam folder",
  :m11=> "Secondary email verified",
  :m12=> "This email is already associated to this family ",
  :m13=> "Please choose a password with 8 characters or more",
  :m14=> "Passwords do not match",
  :m15=> "Please fill the new password",
  :m16=> "Logout successfully",
  :m17=> "Current password is incorrect",
  :m18=> "You need to verify your email ID, before performing this function",
  :m19=>"Password does not match the password criteria",
  :m20=>"<span style='color: gray;display:block;text-align:left;'>The new password should contain at least one character from each of the following: <br/>
     • capital letters (A-Z)<br/>
     • small letters (a-z)<br/>
     • numbers (0-9)</span>",
  :m21=>"Your password can't be same as the previous four password",
  :m22=> "Access denied. Please sign in again",
  :m23=>"Sorry, your account has been blocked by admin. Kindly write to support@nurturey.com to solve the issue",
  :m24=> "Too many incorrect password attempts. Account has been blocked. Kindly contact admin at support@nurturey.com to solve the issue",
  :m25 =>"Kindly choose a stronger password as the current one is too weak",
  :m26 => "Sorry, you used a different method to signup. Please use that to log in to your account"
  }
   
  Subscription={:m1=>"Subscription created successfully",
  :m2=>"Subscription not created successfully",
  :m3=> "The subscription was taken by another member of the family. Only he or she can change this subscription",
  :m4=> "You can take a subscription for only one family in your account. However, another member of the family can take the subscription from their device",
  :m5 => "The subscription has been taken from another device. Please use the same device to amend the subscription",
  :m6 => "You need a different plan to be able to activate this tool",
  :m7=>"You need to deactivate some of the tools to be able to downgrade to this plan",
  :tool_Subscribed => "Tool is Subscribed",
  :tool_not_subscribed =>"Tool is not subscribed",
  :not_subscribed => "User not subscribed for this subscription",
  :subscribed => "You are already subscribed",
  :verify_subscription_error => "Error in verify subscription",
  :success => "success",
  :can_not_degrade_plan => "can not degrade plan",
  :not_valid_user_to_update => "You can’t upgrade subscription for this family",
  :unauthorized_to_upgrade => "You are unauthorized to upgrade subscription for this family",
  :sufficient_level => "sufficient level",
  :insufficient_level => "insufficient level",
  :not_valid_user_to_delete=> "You can’t delete a family for which a premium plan is active",
  :subscription_expired=> "Subscription has been expired. Please renew or upgrade.",
  :subscription_trial_days_end_on=> "Trial period ended on %{trial_date}.",
  :subscription_trial_days_end_at=> "Trial period end date %{trial_date}."
  
  }

  Clinic = {
    :tool_data_sync=>{
      "100" => "Nurturey was not able to pull information from NHS/GP due to a connectivity issue. Hence, you may not be able to see information recorded by NHS/GP in the sections below.",
      "200" => "If you believe there is missing or inaccurate information, please do check the paper red book provided by NHS or with your GP. Such cases may occur due to an update delay or technical error in pulling info from NHS/GP.",
      "403" => "If you believe there is missing or inaccurate information, please do check the paper red book provided by NHS or with your GP. Such cases may occur due to an update delay or technical error in pulling info from NHS/GP.",
      "401" => "If you believe there is missing or inaccurate information, please do check the paper red book provided by NHS or with your GP. Such cases may occur due to an update delay or technical error in pulling info from NHS/GP.",
      "not_linked"=>"You are not linked to %{member_name}'s GP/NHS online account. Hence, %{member_name}'s NHS records will not be visible below.\n\nLink Now"
    },
    :health_card_widget=>{
      :NoGpLinked=>'To access this feature, kindly setup your GP account',
      :GpSetupIncomplated=>'To access this feature, go to Manage GP and complete your GP linking',
      :NoAccess=>'You do not have access to this account. Please get in touch with GP to gain access',
      :AccountVerification=>"Account verification is pending. Please verify your account",
      :LimitedAccessWithoutPinDocument=>"Please enter your account linkage details provided by GP for full access to online services. Please contact your GP to get these details",
      :LimitedAccessWithoutProxySelected=>'Child is not linked with your account. Please get in touch with GP to gain access',
      :LimitedAccessUserNotLinked=>"Your account details are required to gain full access to your child's health records",
      :FacebookAccount=> "Sorry, GP Linking is not available for Facebook login. Please sign in through email or NHS login to access this feature",
      :AppleAccount=> "Sorry, GP Linking is not available for Apple signin. Please sign in through email or NHS login to access this feature"
    },
    :no_access => 'Access not permitted',
    :limited_access=> "Limited access to online GP services",
    :service_disabled=>"Your GP has disabled this feature. Please ask your GP to enable",
    :full_access => "Full access to GP online services",
    :no_clinic_setup_access_reason=> 'You do not have access to this account. Please get in touch with GP to gain access',
    :no_clinic_access_blocked_reason=> 'You do not have access to this account. Please get in touch with Nurturey support to gain access',
    :limited_access_reason1 => "Your account details are required to gain full access to your child's health records",
    :limited_access_reason2 => "Please enter %{member_name}'s account details provided by the GP. If you do not have details, please contact your GP",
    :limited_access_reason3=>'Child is not linked with your account. Please get in touch with GP to gain access',
    :limited_access_reason4=>"Your account details are required to gain full access to your child's health records",
    :limited_access_reason5 => "Please enter your account linkage details provided by GP for full access to online services. Please contact your GP to get these details",
    :account_verification_pending=> "Complete account verification",
    :account_verification_pending_access_reason1=> "You have not verified %{member_name}'s account yet. Verify %{member_name}'s account to gain full access",
    :account_verification_pending_access_reason2=> "Account verification is pending. Please verify your account",
    :user_authenticated =>"Authenticated successfully",
    :user_authentication_failed=> "Sorry! Unable to authenticate user.Please try again",
    :incorrect_otp => 'OTP entered is incorrect. Please enter correct OTP to proceed',
    :first_name_not_matched=> 'Sorry! Unable to authorized account due to account name mismatch. Please try again',
    :age_criteria_failed_to_registration=> 'Sorry! Unable to authorized account due to age restriction. Please try again',
    :birth_date_does_not_matched => 'Sorry! Unable to authorized account due to date of birth mismatch. Please try again',
    :gp_not_linked => "Select the GP Practice",
    :gp_setup_incomplete=>"Link to GP Practice now",
    :gp_setup_incomplete_access_reason => 'You do not have access to this account. Please complete GP linking',
    :gp_not_linked_access_reason => 'You do not have access to this account. Please setup your GP account',
    :unable_to_connect_server=>'Sorry, Unable to connect Clinic services',
    :clinic_access_limit_exceeded=>"Sorry, you've exceeded the daily access limit for this feature. Please try again tomorrow",
    :clinic_access_blocked=> "Sorry, this feature has been disabled by the admin. Please email support@nurturey.com to solve this issue",
    :gp_search_error=> "We are unable to connect to NHS's list of GP's. Please try again later",
    :gp_not_found_nearby=> "Sorry, we can't find any GP's listed near that postcode. Please check the postcode or try with a different one",
    :gp_not_enable_with_nurturey=>"Kindly check your linkage details. If correct, then your GP is yet to be online with Nurturey. We'll keep you updated"
   }

  # NhsLogin={
  #     :error_message=> 'Sorry! I could not fulfill your request due to an error. Please try again',
  #     :invalid_request=> 'The request is missing a required parameter, includes an unsupported parameter value (other than grant type), repeats a parameter, includes multiple credentials, utilizes more than one mechanism for authenticating the client, or is otherwise malformed.',
  #     :invalid_client=> 'Client authentication failed (e.g., unknown client, no client authentication included, or unsupported authentication method).',
  #     :invalid_grant=> 'The provided authorization grant (e.g., authorization code, resource owner credentials) or refresh token is invalid, expired, revoked, does not match the redirection URI used in the authorization request, or was issued to another client.',
  #     :unauthorized_client => 'The authenticated client is not authorized to use this authorization grant type.',
  #     :unsupported_grant_type =>'The authorization grant type is not supported by the authorization serverPlatform.',
  #     :invalid_scope=> 'The requested scope is invalid, unknown, malformed, or exceeds the scope granted by the resource owner',
  #     :invalid_token=> 'The access token provided is expired, revoked, malformed, or invalid for other reasons. The resource SHOULD respond with the HTTP 401 (Unauthorized) status code. The client MAY request a new access token and retry the protected resource request.',
  #     :insufficient_scope=> 'The request requires higher privileges than provided by the access token. The resource server SHOULD respond with the HTTP 403 (Forbidden) status code and MAY include the “scope” attribute with the scope necessary to access the protected resource'
  # }

  NhsLoginFailed = "We are working to resolve these issues. Thank you for your patience.<br/><br/>If you urgently need medical help or advice but it's not a life-threatening situation, call 111 free from any phone to speak to an NHS adviser. The 24-hour NHS 111 service can give you healthcare advice or direct you to the local service that can help you best."
  NhsGenricMessage = 'Sorry, the request could not be fulfilled due to an error with NHS Login processing. Please try to login again. You can contact NHS login support if the issue persists. In case of an urgency, please contact your GP/health provider directly by phone or email to seek necessary health service.'
  NhsLogin={
      :error_message=> Message::NhsGenricMessage,
      :invalid_request=> Message::NhsGenricMessage,
      :invalid_client=> Message::NhsGenricMessage,
      :invalid_grant=> Message::NhsGenricMessage,
      :unauthorized_client => Message::NhsGenricMessage,
      :unsupported_grant_type =>Message::NhsGenricMessage,
      :invalid_scope=> Message::NhsGenricMessage,
      :invalid_token=> Message::NhsGenricMessage,
      :insufficient_scope=> Message::NhsGenricMessage
  }

  Emis={
    :permission_denied=>"Permission denied. Please get access",
    :Login=>{
      :unauthorized_to_access=>'Sorry! User is not authorized to access records',
      :unable_to_get_session => 'Sorry! Unable to authorized account due to an error. Please try again'
    },
    :Messages=>{
      :unable_to_get_message=> 'Sorry! Unable to get messages due to an error. Please try again',
      :unable_to_get_message_detail => 'Sorry! Unable to get message detail due to an error. Please try again',
      :message_deleted=>"Message deleted successfully",
      :unable_to_delete_message => 'Sorry! Unable to delete message due to an error. Please try again',
      :unbale_to_get_message_receiptent_list => 'Sorry! Unable to get message receiptent list due to an error. Please try again',
      :message_read_status_updated=> "Message status updated successfully",
      :message_sent=> "Message sent successfully",
      :message_not_sent=> "Sorry! message could not sent due to an error. Please try again"
    },
    :Pharmacy=>{
      :unable_to_get_nominated_pharmacy=> 'Sorry! Unable to get nominated pharmacy due to an error. Please try again',
      :unable_to_list_pharmacy=> 'Sorry! Unable to get list pharmacy due to an error. Please try again',
      :pharmacy_nomination_updated => 'Pharmacy nomination updated successfully',
      :pharmacy_nomination_not_updated=> 'Sorry! Unable to get update pharmacy nomination due to an error. Please try again',
      :pharmacy_nomination_deleted => 'Pharmacy nomination deleted successfully',
      :pharmacy_nomination_not_deleted=> 'Sorry! Unable to delete pharmacy nomination due to an error. Please try again',
    },
    :HealthCard=>{
      :service_disabled=>"Sorry, your GP hasn't given access to - %{item_type}. Kindly contact your GP to get access",
      :unable_to_list=>'Sorry! Unable to list %{item_type} due to an error. Please try again'
    },
    :Prescribing=>{
      :unable_to_list_prescription=> 'Sorry! Unable to list prescription due to an error. Please try again',
      :prescription_requested=> "Request for prescription sent successfully",
      :prescription_not_requested=> 'Sorry! Unable to request for prescription due to an error. Please try again',
      :unable_to_list_requested_prescription=> 'Sorry! Unable to list requested prescription due to an error. Please try again',
      :prescription_request_cancelled => "Prescription Request cancelled successfully",
      :prescription_request_not_cancelled => "Sorry! Unable to cancel prescription request due to an error. Please try again"
    },
    :RegisterUser=>{
      :exisitng_account=>'The user account already exists',
      :no_record_found=> 'A single patient record could not be found or more than one patient record found',
      :missing_or_invalid_data=> 'Missing or invalid data',
      :register_with_pin_document_failed=> 'Sorry! Unable to register user due to an error. Please try again'
    },
    :BookAppointment=>{
      :success=>{
        :booking_created=>"Appointment booked successfully",
        :appointment_canceled =>"Appointment cancelled successfully",
      },
      :error=>{
       :slot_unavailable=>'Sorry! Slot is not available for booking',
       :unable_to_list_appointment=>'Sorry! Unable to get appointments due to an error. Please try again',
       :unable_to_cancel_appointment => 'Sorry! Unable to cancel appointment due to an error. Please try again',
       :unable_to_book_appointment=> 'Sorry! Unable to book appointment due to an error. Please try again'
      }
    }
  }
  Tpp={
    :permission_denied=>"Permission denied. Please get access",
    :Login=>{
      :unauthorized_to_access=>'Sorry! User is not authorized to access records',
      :unable_to_get_session => 'Sorry! Unable to authorized account due to an error. Please try again'
    },
    :Messages=>{
      :unable_to_get_message=> 'Sorry! Unable to get messages due to an error. Please try again',
      :unable_to_get_message_detail => 'Sorry! Unable to get message detail due to an error. Please try again',
      :message_deleted=>"Message deleted successfully",
      :unable_to_delete_message => 'Sorry! Unable to delete message due to an error. Please try again',
      :unbale_to_get_message_receiptent_list => 'Sorry! Unable to get message receiptent list due to an error. Please try again',
      :message_read_status_updated=> "Message status updated successfully",
      :message_read_status_not_updated=> "Message status updated successfully",
      :message_sent=> "Message sent successfully",
      :message_not_sent=> "Sorry! message could not sent due to an error. Please try again"
    },
    :Pharmacy=>{
      :unable_to_get_nominated_pharmacy=> 'Sorry! Unable to get nominated pharmacy due to an error. Please try again',
      :unable_to_list_pharmacy=> 'Sorry! Unable to get list pharmacy due to an error. Please try again',
      :pharmacy_nomination_updated => 'Pharmacy nomination updated successfully',
      :pharmacy_nomination_not_updated=> 'Sorry! Unable to get update pharmacy nomination due to an error. Please try again',
      :pharmacy_nomination_deleted => 'Pharmacy nomination deleted successfully',
      :pharmacy_nomination_not_deleted=> 'Sorry! Unable to delete pharmacy nomination due to an error. Please try again',
    },
    :Prescribing=>{
      :unable_to_list_prescription=> 'Sorry! Unable to list prescription due to an error. Please try again',
      :prescription_requested=> "Request for prescription sent successfully",
      :prescription_not_requested=> 'Sorry! Unable to request for prescription due to an error. Please try again',
      :unable_to_list_requested_prescription=> 'Sorry! Unable to list requested prescription due to an error. Please try again',
      :prescription_request_cancelled => "Prescription Request cancelled successfully",
      :prescription_request_not_cancelled => "Sorry! Unable to cancel prescription request due to an error. Please try again"
    },
    :RegisterUser=>{
      :exisitng_account=>'The user account already exists',
      :no_record_found=> 'A single patient record could not be found or more than one patient record found',
      :missing_or_invalid_data=> 'Missing or invalid data',
      :register_with_pin_document_failed=> 'Sorry! Unable to register user due to an error. Please try again'
    },
    :BookAppointment=>{
      :booking_created=>"Appointment booked successfully",
      :booking_not_created=>"Sorry! Unable to book Appointment",
      :unable_to_get_available_appointment=> "Sorry! Unable to get available appointments due to an error. Please try again",
      :appointment_canceled =>"Appointment cancelled successfully",
      :slot_unavailable=>'Sorry! Slot is not available for booking',
      :unable_to_list_appointment=>'Sorry! Unable to get appointments due to an error. Please try again',
      :unable_to_cancel_appointment => 'Sorry! Unable to cancel appointment due to an error. Please try again',
      :unable_to_book_appointment=> 'Sorry! Unable to book appointment due to an error. Please try again'
      }
    
  }
 

end