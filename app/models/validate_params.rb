class ValidateParams

  class << self
    def check_date(*args)
      args = args.flatten
      check_blank(args) || check_datetime_format(args)
    end

    def check_blank(*args)
      args = args.flatten
      invalidate = []
      args.each do |arg|
        invalidate << arg.blank?
        invalidate << (arg.downcase == 'null' || arg.downcase == 'nil') if arg.instance_of?(String)
      end
      invalidate.include?(true)
    end

    def check_datetime_format(*args)
      args = args.flatten
      invalidate = []
      args.each do |arg|
        invalidate << (arg.to_datetime rescue true)
      end
      invalidate.include?(true)
    end

    def check_positive_number(*args)
      args = args.flatten
      invalidate = []
      args.each do |arg|
        invalidate << (arg.to_i < 0)
      end
      invalidate.include?(true)
    end

  end
end
