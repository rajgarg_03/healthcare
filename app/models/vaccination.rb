  class Vaccination
    include Mongoid::Document  
    include Mongoid::Timestamps

    field :name, type: String
    field :desc, type: String
    field :sys_vaccin_id, type: String
    field :country, type: String
    field :member_id, type: Integer
    field :opted, type: String 
    field :category, type: String, default: "Injection" # Oral/Injection/nasal spray
    field :data_source,type:String, default:"nurturey" 
    belongs_to :member
    has_many :jabs
    has_many :timelines, :as=> :timelined
    Country = {"india"=>"India","IN"=>"India", "in"=>"India", "us"=>"US","GB"=>"UK", "gb"=>"UK","uk"=>"UK","ae"=>"AE","AE"=>"AE","ca"=>"CA","CA"=>"CA","AU"=>"AU","au"=>"AU","za"=>"ZA","ZA"=>"ZA","sg"=>"SG","SG"=>"SG","NZ"=>"NZ","nz"=>"NZ"}
    Category= {"Oral"=>"icon-vaccine-drop", "Drops"=>"icon-vaccine-drop","Tabs" => "icon-vaccine-tab", "Injection"=>"icon-vaccine-inj",nil=>"icon-vaccine-inj"}
    ActionList = {"skip_screen_name"=>"Milestone_list","proceed_screen_name"=> "vaccination_handhold", "proceed_handhold_screen"=> "Milestone_list" }
    ColourForStatus = {"overdue"=>"red", "planned"=>"black", "due"=>"black", "estimated"=>"black","administered"=>"green","upcoming"=>"yellow"}

    index ({member_id:1})
    index ({sys_vaccin_id:1})
    
    #before_save :set_route

    # def set_route
    #   if self["category"].present?
    #     self["route"] = self["category"]
    #   end
    # end

    # def category
    #   self["route"] || self["category"]
    # end

    def add_to_sys_vaccin(member_id)
      vaccine = self
       sys_vaccin = SystemVaccin.create(name:self.name,member_id:member_id,category:self.category)
       vaccine.sys_vaccin_id = sys_vaccin.id
    end

    def total_jabs_count
      jabs.count
    end
    
    def self.next_jab_date(member_id)
     begin
        vaccination_list  = Vaccination.where(member_id: member_id).not_in(:opted => "false").pluck("id")
        next_jab = Jab.in(:vaccination_id=> vaccination_list).or({:due_on.gte => DateTime.now},{:due_on=> nil,:est_due_time.gte => DateTime.now}).map{ |j| j["vaccin_date"] = (j.start_time || (j.due_on|| j.est_due_time));j }
        next_jab_date = next_jab.sort_by{|jab| jab["vaccin_date"]}.first["vaccin_date"] rescue nil
        next_jab_date = next_jab_date.to_time.in_time_zone.strftime("%d %b %y") rescue nil
      rescue Exception=>e 
        Rails.logger.info "Error inside.....next_jab_date....#{e.message}"
        next_jab_date = nil
      end
      next_jab_date
    end
    def get_category(options={})
      vaccine = self
      if options[:immunisation_version].to_i > 0
        ::SystemVaccin::CategoryMapping[vaccine.category]
      else
        vaccine.category
      end
    end
    def self.last_jab_date(member_id, format="to_date5")
      begin
        vaccination_list  = Vaccination.where(member_id: member_id).not_in(:opted => "false").pluck("id")
        last_jab = Jab.where(:vaccination_id.in=> vaccination_list,:due_on.lte => DateTime.now).administered.order_by("due_on desc").first
        last_jab_due_on = last_jab.due_on_from_data_source || last_jab.due_on
        last_jab_date = last_jab_due_on.to_time.in_time_zone.to_date.send(format) rescue nil
      rescue Exception => e
        Rails.logger.info "Error inside .....last_jab_date ...#{e.message}"
        last_jab_date = nil
      end
      last_jab_date
    end

    # Vaccination.immunisation_graph_data(current_user,member,options)
    # options = {:graph_data_for_list=>true}
    def self.immunisation_graph_data(current_user,member,options={})
      tool_identifiers = {"immunisations"=>"Immunisations"}.with_indifferent_access
      immunisation_chart_data = Vaccination.vaccination_chart_data(current_user,member,options)
      updated_at = Vaccination.last_jab_date(member.id, format="to_date")
      immunsation_updated_at_text = Member.updated_at_text(updated_at,level=1,options)
      age_range = immunisation_chart_data.delete(:current_age_range)
      extra_info  =  {:current_age_range=>age_range, :x_axis_title=>"Age (months)",:y_axis_title=>"Doses",:updated_at_text=>immunsation_updated_at_text } rescue {}
      if options[:graph_data_for_list] == true
        {:chart_type=>"immunisations",:tool_identifier=>"Immunisations", :chart_data=>immunisation_chart_data[:immunisations]}.merge(extra_info)
      else
        [immunisation_chart_data,extra_info]
      end
    end

    def self.vaccination_report_for_member(member)
      begin
      vaccination_list  = Vaccination.where(member_id: member.id).not_in(:opted => "false").pluck("id")
      planned_jabs = Jab.in(vaccination_id: vaccination_list).where(status:"Planned").not_in(due_on:nil).count
      administrator_jabs = Jab.in(vaccination_id: vaccination_list).administered.count
      #pending_jabs = Jab.in(vaccination_id: vaccination_list).not_in(status:["Administered"]).where(due_on:nil).count
      #next_jab = Jab.in(vaccination_id: vaccination_list).not_in(status:["Administered"]).or({:due_on.gt=>Date.today},{:est_due_time=>Date.today}).order_by("due_on desc, est_due_time desc").map{|a| a.due_on || a.est_due_time}
      
      next_jab = Jab.in(:vaccination_id=> vaccination_list).not_administered_jab.or({:due_on.gte => DateTime.now},{:due_on=> nil,:start_time.gte => DateTime.now}).map{ |j| j["vaccin_date"] = (j.start_time || (j.due_on|| j.est_due_time));j }
      due_jabs = Jab.in(:vaccination_id=> vaccination_list).not_administered_jab.or({:due_on.gte => DateTime.now},{:due_on=> nil,:start_time.gte => DateTime.now}).count
      pending_jabs = Jab.in(:vaccination_id=> vaccination_list).not_administered_jab.or({:due_on.lt => DateTime.now},{:due_on=> nil,:start_time.lte => Date.today}).count
      next_jab_date = next_jab.sort_by{|jab| jab["vaccin_date"]}.first["vaccin_date"] rescue nil
      next_jab_date = next_jab_date.to_time.in_time_zone.strftime("%d %b %y") rescue nil
      # next_jab_date = next_jab_date.to_date.strftime("%d %b") rescue nil
      data = {}
      
      data.merge!({"administered_jabs"=>administrator_jabs})
      data.merge!({"planned_jabs"=>planned_jabs})
      data.merge!({"pending_jabs"=>pending_jabs})
      data.merge!({"due_jabs"=>due_jabs})
      data.merge!({"next_jab"=> next_jab_date})
      
    rescue
    end
    data
    end
    
    # Vaccination.vaccination_chart_data(current_user,member,options)
    def self.vaccination_chart_data(current_user,member,options={})
      vaccination_list  = Vaccination.where(member_id: member.id).not_in(:opted => "false").pluck("id")
      # planned_jabs = Jab.in(vaccination_id: vaccination_list).where(status:"Planned").not_in(due_on:nil)
      # administrator_jabs = Jab.in(vaccination_id: vaccination_list).administered
    
      # administrator_jabs = Jab.in(vaccination_id: vaccination_list).administered
      # due_jabs = Jab.in(:vaccination_id=> vaccination_list).not_administered_jab.or({:due_on.gte => DateTime.now},{:due_on=> nil,:start_time.gte => DateTime.now})
      # pending_jabs = Jab.in(:vaccination_id=> vaccination_list).not_administered_jab.or({:due_on.lt => DateTime.now},{:due_on=> nil,:start_time.lte => Date.today})
      all_jabs = Jab.in(:vaccination_id=> vaccination_list)
      data = []
      # data <<  Vaccination.get_formated_data_for_chart(pending_jabs,"late",member)
      # data <<  Vaccination.get_formated_data_for_chart(due_jabs,"upcoming",member)
      # data <<  Vaccination.get_formated_data_for_chart(administrator_jabs,"administered",member)
      data <<  Vaccination.get_formated_data_for_chart(all_jabs,"estimated",member)
      data = data.flatten.compact.sort_by{|a| a[:age_in_month]}
      group_by_age_in_month_data = data.group_by{|a| a[:age_in_month]}
      chart_data = {}
      group_by_age_in_month_data.each do |k,v|
        v = v.sort_by{|a| (a[:jab_date] || (Date.today + 30.year).to_time )}
        chart_data[k] = v.map do |value| 
          {title:value[:title],status:value[:status],jab_id:value[:id]}
        end 
      end
      # graph_x_range_data = {1=>"0-1",2=>"2-3",3=>"3-6",4=>"6-12",5=>"12-24",6=>"24-36",7=>"36-48",8=>"48-60",9=>"60"}
      graph_x_range_data =   {1=>"0-2",2=>"3-6",3=>"7-12",4=>"13-24",5=>"25-36",6=>"37-60",7=>"61-96",8=>"96+"}
      data =  []
      member_age_in_month = member.age_in_months
      current_age_range = nil
      graph_x_range_data.each do |age_in_month,range|
        x_data = []
        begin
          lower_range,upper_range = range.split("-").map(&:to_i)
          if upper_range.blank?
            upper_range = chart_data.keys[-1].to_i + 1
            range = "#{lower_range}+"
          end
          (lower_range..upper_range).each do |month|
            x_data << chart_data[month]
          end
          current_age_range = range if (lower_range..upper_range).include?(member_age_in_month)
        rescue Exception =>e 
          Rails.logger.info e.message
        end
        x_data = x_data.compact.flatten#.sort_by{|a| (a[:status]|| "")}
        data << {x_value:range,y_values:x_data} if x_data.present?
      end
      {:immunisations=>data,current_age_range:current_age_range}
     end
    
    def self.get_formated_data_for_chart(jab_data,status,member,options={})
      data  = []
      chart_status = {"overdue"=>"late", "administered"=>"completed","upcoming"=>"upcoming", "due"=>"upcoming","estimated"=>"estimated"}
      data << jab_data.map do |jab| 
        begin
          age = ApplicationController.helpers.calculate_age((jab[:due_on] || jab[:est_due_time]),member.birth_date)
          jab_date = (jab[:due_on] || jab[:est_due_time])
          if age
            age_in_month = Milestone.convert_to_months(age) rescue 0
          end
          text,color,jab_status = Vaccination.jab_status_text(jab,options)
          {title:"",jab_date: jab_date, id:jab.id, status:(chart_status[jab_status]||jab_status),:age_in_month=>age_in_month}
        rescue Exception=>e 
          Rails.logger.info  e.message
        end
      end
      data
    end

    def completed_jabs_count
      jabs.administered.count
    end

    def self.add_system_vaccin_to_member(member,current_user)
      # sys_vaccin = SystemVaccin.where(member_id: current_user.id).entries + SystemVaccin.where(member_id: nil, vaccin_type: "Recommended", country:Vaccination::Country[current_user.user.country_code] || "UK").entries
      country_code = current_user.user.country_code || "UK" rescue "UK"
      sanitize_country_code = Member.sanitize_country_code(country_code)
      
      if  member.family_members.first.role == FamilyMember::ROLES[:daughter]
        sys_vaccin = SystemVaccin.where(member_id: nil, vaccin_type: "Recommended", :country.in=>sanitize_country_code).entries
      else
        sys_vaccin = SystemVaccin.where(member_id: nil, vaccin_type: "Recommended", :country.in=>sanitize_country_code).not_in(:only_for=>[FamilyMember::ROLES[:daughter]]).entries
      end
      sys_vaccin.each do |sys_vaccin|
        member_vaccin = Vaccination.new(name:sys_vaccin.name, member_id:member.id,sys_vaccin_id:sys_vaccin.id, :opted=>"true")#.save
          if member_vaccin.save
           (sys_vaccin.system_jabs.blank? ? [Jab.new] : sys_vaccin.system_jabs).each do |jab|
           due_on = member.birth_date + eval(jab.due_on) rescue nil
           Jab.new(vaccination_id:member_vaccin.id, name:jab.name,est_due_time:due_on , jab_type: jab.jab_type, route:jab.route, jab_location:jab.jab_location,system_jab_id:jab.id).save
          end
        end
      end      
    end
    def self.get_estimate_age(jab_expected_on,vaccine_category)
      begin
      return '' if vaccine_category == "Custom" || vaccine_category.blank?
      age = ApplicationController.helpers.calculate_age4(Date.today - eval(jab_expected_on)).split(" and")[0]
      return "#{vaccine_category} • Usually given at birth" if age == "today"
      "#{vaccine_category} • Given at an estimated age of " + age
    rescue Exception=>e 
     Rails.logger.info e.message
    end
    end

  # used in api 5
    # Not in use for api version 6
    def self.vaccination_list(member,current_user,options={})
      @member = member
      data = []
      country_code = current_user.user.country_code || "UK" rescue "UK"
      sanitize_country_code = Member.sanitize_country_code(country_code)
      
      system_vaccine_exist_for_country = SystemVaccin.where(:member_id=>nil,:country.in=>sanitize_country_code)
      if system_vaccine_exist_for_country.blank?
        country_code = "Uk"
        sanitize_country_code = Member.sanitize_country_code(country_code)
      end
      member_selected_vaccin = @member.vaccinations.not_in(:opted => "false")#.sort_by{|i| i.name.upcase}
      selected_sys_vaccin_ids = member_selected_vaccin.pluck(:sys_vaccin_id)
      data =  SystemVaccin.in(id:selected_sys_vaccin_ids).sort_by{|i| i.name.upcase}
      data = data.map do |system_vaccine| 
        system_vaccine.get_json_data(member,current_user,opted_value=true,options)
      end
      user_vaccinations = SystemVaccin.where(member_id: current_user.id)#.sort_by{|i| i.name.upcase} 
      if  @member.family_members.first.role == FamilyMember::ROLES[:daughter]
        recommended_vaccin = SystemVaccin.where(:vaccin_type =>"Recommended").where(member_id: nil,:country.in=>sanitize_country_code) 
        non_recommended_vaccin = SystemVaccin.where(:vaccin_type.ne=> "Recommended" ).where(member_id: nil,:country.in=>sanitize_country_code)
      else
        recommended_vaccin = SystemVaccin.where(:vaccin_type =>"Recommended").not_in(:only_for=>[FamilyMember::ROLES[:daughter]]).where(member_id: nil,:country.in=>sanitize_country_code)
        non_recommended_vaccin = SystemVaccin.where(:vaccin_type.ne=> "Recommended" ).not_in(:only_for=>[FamilyMember::ROLES[:daughter]]).where(member_id: nil,:country.in=>sanitize_country_code)
      end

      @vaccin_list = user_vaccinations.sort_by{|i| i.name.upcase} + recommended_vaccin.sort_by{|i| i.name.upcase} + non_recommended_vaccin.sort_by{|i| i.name.upcase}
      
      if data.blank?
        @vaccin_list = @vaccin_list.map do |system_vaccine|
                       system_vaccine.get_json_data(member,current_user,opted_value=nil,options)
                      end
       
      else
        @vaccin_list = @vaccin_list.map do |system_vaccine| 
                        system_vaccine.get_json_data(member,current_user,opted_value=false,options)
                      end
       
      end
      data <<  (@vaccin_list.flatten.uniq - data)
      data.flatten.uniq
    end

    def get_name
      system_vaccination = SystemVaccin.where(id:self.sys_vaccin_id).pluck(:name).last
      (system_vaccination || self.name) rescue self.name
    end
    
    def self.vaccination_priority_list(current_user,member,api_version,options)
      current_date  = Time.now.in_time_zone.to_date
      day_range_upcoming = 1.month  # + 1 month
      day_range_past = 6.month   # -6 month
      vaccine_ids = [] 
      system_vaccine_ids = []
      data = []
      
      vaccinations = Vaccination.where(member_id:member.id).only(:id,:sys_vaccin_id)
      vaccinations.each do |vaccination|
        begin
          SystemVaccin.find(vaccination["sys_vaccin_id"])
          vaccine_ids << vaccination["id"]
          system_vaccine_ids << vaccination["sys_vaccin_id"]
        rescue Exception=>e 
          next
        end
      end
      jabs = Jab.where(:vaccination_id.in=>vaccine_ids).not_administered_jab.or({:due_on=>{"$lte"=>(current_date + day_range_upcoming), "$gte"=>(current_date - day_range_past)}},{:due_on=>nil,:est_due_time=>{"$lte"=>(current_date + day_range_upcoming), "$gte"=>(current_date - day_range_past)}})
      jabs = jabs.sort_by{|jab| jab.due_on||jab.est_due_time}.reverse
      selected_jab_vaccination_ids = jabs.map(&:vaccination_id)
      system_vaccine_group_by_id = SystemVaccin.system_vaccine_group_by_id(system_vaccine_ids)
      vaccination_group_by_vaccination_id = Vaccination.vaccine_group_by_id(selected_jab_vaccination_ids)
      jabs_group_by_vaccination_ids = Vaccination.jabs_group_by_vaccine(jabs.map(&:vaccination_id))
      options[:new_line_in_text] = false
      options[:member] = member
      jabs.each do |jab|
        jab_attr = {}
        vaccine =  vaccination_group_by_vaccination_id[jab.vaccination_id].first
        system_vaccine = system_vaccine_group_by_id[vaccine["sys_vaccin_id"]].first rescue {}
        jab_attr["vaccine_name"] = system_vaccine["name"]
        jab_number = jabs_group_by_vaccination_ids[jab.vaccination_id].index(jab.id)+1
        jab_attr["status_text"], jab_attr["status_color_code"],status_for_colour    =  Vaccination.jab_status_text(jab,options) rescue ''
        jab_attr["name"] = jab_number.ordinalize
        jab_attr["color"] = Vaccination::ColourForStatus[status_for_colour]
        jab_attr["due_on"] = (jab.due_on_from_data_source || jab.due_on).to_api_date2 rescue nil
        jab_attr["est_due_time"] = jab.est_due_time.to_api_date2 rescue nil
        jab_attr["id"] = jab.id
        jab_attr["vaccine_id"] = vaccine["_id"]
        jab_attr["immunisation_type "] = jab["jab_type"]
        data << jab_attr
      end
      data
    end

    def self.vaccine_group_by_id(vaccine_ids=nil,options={})
      selection_critriea =  { "_id" => {"$in"=>vaccine_ids }}
      hash_data =  Vaccination.collection.aggregate([
        { "$match" => selection_critriea },
        {"$group" => {
          "_id" => "$_id",
           "jab_ids" => {"$push"=> "$$ROOT"}
        }} ,
        {"$sort" => { "_id" => 1}}
      ])
      hash_data
      data = {}
      hash_data.each do |info|
        data[info["_id"]] = info["jab_ids"]
      end
      data
    end

    def self.jabs_group_by_vaccine(vaccination_ids=nil,options={})
      selection_critriea = { "vaccination_id" => {"$in"=>vaccination_ids }}
      if options[:all_fields] == true
        selected_field = {"$push"=> "$$ROOT"}
      else
        selected_field = {"$push"=> "$_id"}
      end
      hash_data =  Jab.collection.aggregate([
        { "$match" => selection_critriea },
        {"$group" => {
          "_id" => "$vaccination_id",
           "jab_ids" => selected_field
        }} ,
        {"$sort" => { "_id" => 1}}
       ])
      hash_data
      data = {}
      hash_data.each do |info|
        data[info["_id"]] = info["jab_ids"]
      end
      data
    end
    
   
    def self.vaccination_with_jabs(vaccinations=[],options={})
      data = []
      options[:member] = (vaccinations.last.member rescue nil)
      member = options[:member]
      current_user = options[:current_user]
      vaccinations = vaccinations.entries if vaccinations.class.to_s == "Mongoid::Criteria"
      vaccinations = vaccinations.class.to_s == "Array" ? vaccinations : [vaccinations]
      system_vaccination_names = {}
      data_source = "nurturey"
      clinic_req_status = ""
      system_mapped_nhs_data = {}
      
      if member.present?
        if member.is_user_fully_linked_with_clinic?(current_user,options)
          clinic_detail = member.get_clinic_detail(current_user,options)
          data_source = clinic_detail.get_clinic_type(options)
          nhs_vaccine, system_mapped_nhs_data,clinic_req_status = SystemVaccin.get_nhs_immunisation_jabs(current_user,member,options)
        elsif member.clinical_safty_message_enabled_for_app_version?(current_user,options)
          clinic_req_status = "not_linked"
        end
      end
      # system_vaccine_ids = vaccinations.map(&:sys_vaccin_id)
      # SystemVaccin.in(id:system_vaccine_ids).map{|system_vaccine| system_vaccination_names.merge!(system_vaccine.id.to_s=>system_vaccine.name)}
      options[:without_content] = true
      vaccinations.each do |vaccin|
        vaccin.jabs.update_all(data_source:"nurturey",due_on_from_data_source:nil)
        hash_data = vaccin.attributes
        if system_mapped_nhs_data[vaccin.sys_vaccin_id.to_s].present?
          system_mapped_nhs_jabs = system_mapped_nhs_data[vaccin.sys_vaccin_id.to_s] || []
          hash_data["data_source"] = data_source # "emis"
        end
        hash_data["total_jabs_count"] = vaccin.jabs.count
        begin
          system_vaccine = SystemVaccin.find(vaccin.sys_vaccin_id)
        rescue Exception=>e 
          next
        end
        # hash_data["name"] = system_vaccination_names[vaccin.sys_vaccin_id.to_s]
        hash_data["name"] = system_vaccine.name 
        hash_data["syndicated_content"] = system_vaccine.nhs_syndicate_content(options) rescue nil
        
        hash_data["jabs"] = []
        vaccine_jabs = vaccin.jabs.order_by("id asc")
        jab_ids = vaccine_jabs.map(&:id)
        vaccine_jabs.each do |jab|
          system_jab_id = Jab.identify_and_save_system_jab_id(current_user,member,jab,vaccin)
          jab_id = jab.id
          nhs_jab = (system_mapped_nhs_jabs||[]).select{|a| a[:system_jab_id].to_s == system_jab_id.to_s }.first || {}
          if nhs_jab.present?
            nhs_jab = nhs_jab.with_indifferent_access
             jab.due_on_from_data_source = nhs_jab[:due_on]
             jab.data_source = data_source
             # Rails.logger.info "system_jab_id for data_source.................. #{system_jab_id}"
             jab.save
          end

          index = jab_ids.index(jab_id)+1 
          jab_attr = {} #jab.attributes.to_hash
          jab_attr["system_jab_id"]  = system_jab_id
          jab_attr["index"]  = index
          jab_attr["_id"] = jab_id
          jab_attr["status"] = jab.get_jab_status
          jab_attr["vaccination_id"] = vaccin.id
          jab_attr["desc"] = jab.desc
          jab_attr["doc_name"] = jab.doc_name
          jab_attr["data_source"] = jab.data_source || "nurturey"
          jab_attr["facility"] = jab.facility
          jab_attr["note"] =  jab.note
          start_time,end_time = start_end_time(jab)
          jab_attr["fullday"] = jab.fullday
          jab_attr["start_time"] = start_time # (jab.start_time.in_time_zone.to_formatted_s(:time)  rescue nil)
          jab_attr["end_time"] =  end_time #(jab.end_time.in_time_zone.to_formatted_s(:time) rescue nil)
          if options[:jab_status_text_version].to_i > 0
            jab_attr["status_text"], jab_attr["status_color_code"],jab_status_value    =  Vaccination.jab_status_text1(jab,options)
          else
            jab_attr["status_text"], jab_attr["status_color_code"],jab_status_value    =  Vaccination.jab_status_text(jab,options)
          end
          jab_attr["jab_location"] = jab.jab_location 
          jab_attr["route"] =  jab.get_jab_route(options)
          vaccine_trade_name = jab.get_manufacturer(vaccin,options)
          if options[:immunisation_version].to_i > 0
            # to support updated ui 
            # jab_attr["route"] =  jab.jab_type
            jab_attr["trade_name"] =  vaccine_trade_name
            hash_data["trade_name"] = vaccine_trade_name
            jab_attr["name"] = index.ordinalize
            jab_attr["jab_status_value"] = jab_status_value
            jab_attr["color"] = ::Vaccination::ColourForStatus[jab_status_value]
            jab_attr["due_on"] = (jab.due_on_from_data_source || jab.due_on).to_api_date1 rescue nil
            jab_attr["est_due_time"]    = jab.est_due_time.to_api_date1 rescue nil
            jab_attr["due_on_from_data_source"] = (jab.due_on_from_data_source.to_date).to_api_date1 rescue nil

            # jab_attr["start_time"] = (jab.due_on||jab.est_due_time).to_api_date3
          else
            jab_attr["due_on_from_data_source"] = (jab.due_on_from_data_source.to_date).to_api_date1 rescue nil
            jab_attr["due_on"] = (jab.due_on_from_data_source || jab.due_on).to_api_date1 rescue nil
            jab_attr["est_due_time"] = jab.est_due_time.to_api_date1 rescue nil
            jab_attr["manufacturer"] =  jab.manufacturer
            jab_attr["name"] = "Jab #{index}"
          end

          hash_data["jabs"] << jab_attr
        end
        vaccin.reload
        hash_data["completed_jabs_count"] = vaccin.completed_jabs_count
        data << hash_data
      end
      if options[:immunisation_version].to_i > 0
        [data,clinic_req_status]
      else
        data
      end
    end
    # Vaccination.vaccination_by_date(member,vaccinations,options)
    def self.vaccination_by_date(member,vaccinations=[],options={})
      vaccination_list,clinic_req_status = Vaccination.vaccination_with_jabs(vaccinations,options)
      data = []
      unplanned_jab_data = []
      vaccine_group_by_ids = vaccination_list.group_by{|a| a["_id"].to_s}
      jabs_group_by_due_on = vaccination_list.map{|vaccine| vaccine["jabs"]}.flatten.group_by{|jab| (jab["due_on_from_data_source"] || jab["due_on"]||jab["est_due_time"]).to_date rescue nil}
      # jabs_group_by_due_on =jabs_group_by_due_on.sort_by{|k,v| (k.to_date rescue "")}
      jabs_group_by_due_on.each do |due_on,jabs|
          jab_data = []
          completed_jabs_count = jabs.select{|a| a["jab_status_value"] == "administered"}.count
          total_jabs_count = jabs.count
          jabs.each do |jab|
            jab["vaccine"] = vaccine_group_by_ids[jab["vaccination_id"].to_s][0].except("jabs")
            jab_data << jab
          end
        if due_on.present?
          due_on_text = ApplicationController.helpers.calculate_age2(member.birth_date,due_on.to_date)
         
          data << {due_on_text:due_on_text, due_on:due_on.to_date,jabs:jab_data,completed_jabs_count:completed_jabs_count,total_jabs_count:total_jabs_count} 
        else
          unplanned_jab_data << {:due_on_text=>"Unplanned" ,due_on:"Unplanned",jabs:jab_data,completed_jabs_count:completed_jabs_count,total_jabs_count:total_jabs_count} 
        end
      end
      data = data.sort_by{|a| a[:due_on]}
      [(data + unplanned_jab_data),clinic_req_status]
    
    end

    def self.start_end_time(jab)
      begin
        jab_due_on = (jab.due_on_from_data_source || jab.due_on || jab.est_due_time).to_date #rescue Date.today
        start_time = jab.start_time.utc.to_formatted_s(:time).to_i  rescue 0
        end_time = jab.end_time.utc.to_formatted_s(:time).to_i  rescue 0
        if start_time >= end_time
          st_date = jab_due_on.to_date 
          start_time  = st_date.to_time1("09:00").utc.to_formatted_s(:time) 
          end_time =   st_date.to_time1("10:00").utc.to_formatted_s(:time) 
        else
          start_time = jab.start_time.utc.to_formatted_s(:time)  rescue nil
          end_time = jab.end_time.utc.to_formatted_s(:time)  rescue nil
        end
      rescue
        start_time, end_time = [nil,nil]
      end
      [start_time, end_time]
    end

    # def self.sorted_member_jabs(vaccinations)
    #   vaccins = {}
    #   data = []
    #   vaccinations = [Vaccination.last]
    #   vaccinations.each do |vaccination| 
    #     vaccins["vaccination"] = vaccination.attributes
    #     vaccins["vaccination"]["jabs"] = vaccination.jabs.order_by("est_due_time ASC").entries.map(&:est_due_time)
    #     data << vaccins
    #   end
    #   data
    # end

    def self.vaccines_group_by_member(vaccine_ids)
      data = {}
      hash_data =  Vaccination.collection.aggregate([
        { "$match" =>  {"_id"=> {"$in"=> vaccine_ids } }},
        {"$group" => {
          "_id" =>  "$member_id",
          "data" => {"$push"=> "$_id"}
             
        }}  ,
       {"$project": {vaccination_ids: "$data"}}
      ]) 
      hash_data.each do |vaccine_data|
       data[vaccine_data["_id"]] =  vaccine_data["vaccination_ids"]
     end
     data
    end
  # used for all vaccination list(by name ,by Date)
    def self.jab_status_text1(jab,options={})
      immunisation_version = options[:immunisation_version].to_i
      status = jab.get_jab_status 
      jab_due_on = (jab.due_on_from_data_source || jab.due_on).to_time rescue nil
      due_on = jab_due_on || jab.est_due_time
      est_days = DateTime.now + 10.days
      est_late_days = DateTime.now - 6.days
      due_on = due_on.to_date rescue nil
      yellow_color, red_color, grey_color = '#f5c423', '#d9401c', '#9b9b9b'
      if immunisation_version.to_i > 0
        begin
         due_on_date_in_str = (jab_due_on || jab.est_due_time).to_api_date3 
        rescue Exception=>e 
         due_on_date_in_str = ""
        end
        due_on_text =  jab_due_on.present? ? "Due" : "Due"
        new_line = options[:new_line_in_text] == false ? "" : "\n"
        estimated_on_text = "Estimated"
        administered_text =  "Done"
        member =  options[:member]
        if status == "Administered"
          # if options[:new_line_in_text] == false
          #   administered_text = "Done on " + new_line + ApplicationController.helpers.calculate_age(member.birth_date,jab_due_on) 
          # else
            administered_text = "Done on " + new_line + due_on_date_in_str
          # end
        end
      else
        due_on_text = "Due"
        estimated_on_text = "Estimated due date"
        due_on_date_in_str = (jab_due_on || jab.est_due_time).to_api_date2 rescue ""
        new_line = ""
        if status == "Administered"
          administered_text = "Administered on " + due_on_date_in_str
        end
      end
      return ["#{due_on_text} on ", grey_color, "due"] if due_on.blank?
      
      if status == 'Administered'
        [administered_text, grey_color,"administered"]
      elsif due_on < Date.today #est_late_days
        timedifference = TimeDifference.between(due_on.to_date, DateTime.now.to_date).humanize
        text = "Late by #{new_line}" + timedifference.to_s 
        [text, red_color,'overdue']
      elsif due_on >= Date.today #upcoming
        if due_on <= est_days
           day_count = (due_on.to_date - Time.now.to_date).to_i
           text = day_count == 0 ? "Today" : "due in #{day_count} #{'day'.pluralize(day_count)}"
          color = yellow_color
          [text,color,"upcoming"]
        else
          # text = (status == "Estimated") ? "#{estimated_on_text} on" : "Planned on"
          jab_status = (status == "Estimated") ? "estimated" : due_on_text.downcase
          text =  due_on_date_in_str
          color = grey_color
          [text,color,jab_status]
        end
   
      else
        ["#{status} on " + due_on_date_in_str, grey_color,status.downcase]
      end
    end

    # used for upcoming and past immunisation 
    def self.jab_status_text(jab,options={})
      immunisation_version = options[:immunisation_version].to_i
      status = jab.get_jab_status 
      jab_due_on = (jab.due_on_from_data_source || jab.due_on).to_time rescue nil
      due_on = jab_due_on || jab.est_due_time
      est_days = DateTime.now + 10.days
      est_late_days = DateTime.now - 6.days
      due_on = due_on.to_date rescue nil
      yellow_color, red_color, grey_color = '#f5c423', '#d9401c', '#9b9b9b'
      if immunisation_version.to_i > 0
        begin
         due_on_date_in_str = (jab_due_on || jab.est_due_time).to_api_date3 
        rescue Exception=>e 
         due_on_date_in_str = ""
        end
        due_on_text =  jab_due_on.present? ? "Planned" : "Due"
        new_line = options[:new_line_in_text] == false ? "" : "\n"
        estimated_on_text = "Estimated"
        administered_text =  "Done"
        member =  options[:member]
        if status == "Administered"
          if options[:new_line_in_text] == false
            administered_text = "Done at age " + new_line + ApplicationController.helpers.calculate_age(member.birth_date,jab_due_on) 
          else
            administered_text = "Done on " + new_line + due_on_date_in_str
          end
        end
      else
        due_on_text = "Due"
        estimated_on_text = "Estimated due date"
        due_on_date_in_str = (jab_due_on || jab.est_due_time).to_api_date2 rescue ""
        new_line = ""
        if status == "Administered"
          administered_text = "Administered on " + due_on_date_in_str
        end
      end
      return ["#{due_on_text} on ", grey_color, "due"] if due_on.blank?
      
      if status == 'Administered'
        [administered_text, grey_color,"administered"]
      elsif due_on < Date.today #est_late_days
        timedifference = TimeDifference.between(due_on.to_date, DateTime.now.to_date).humanize
        text = "Late by #{new_line}" + timedifference.to_s 
        [text, red_color,'overdue']
      elsif due_on >= Date.today #upcoming
        if due_on <= est_days
           day_count = (due_on.to_date - Time.now.to_date).to_i
           text = day_count == 0 ? "Today" : "Upcoming in #{day_count} #{'day'.pluralize(day_count)}"
          color = yellow_color
          [text,color,"upcoming"]
        else
          text = (status == "Estimated") ? "#{estimated_on_text} on" : "Planned on"
          jab_status = (status == "Estimated") ? "estimated" : due_on_text.downcase
          text = text + " #{new_line}" + due_on_date_in_str
          color = grey_color
          [text,color,jab_status]
        end
   
      else
        ["#{status} on " + due_on_date_in_str, grey_color,status.downcase]
      end
    end
    
    # Vaccination.update_flu_vaccine_jabs
    def self.update_flu_vaccine_jabs
      system_vaccine = SystemVaccin.where(name:"Flu",:country.in=>["uk","UK","gb","GB"]).last
      system_jabs = system_vaccine.system_jabs
      ChildMember.all.batch_size(20).each do |member|
        country_code = User.where(member_id:member.family_mother_father.first.to_s).first.country_code rescue "other"
        if ["uk","UK","gb","GB"].include?(country_code)
          if !member.is_expected?
            vaccine = member.vaccinations.where(sys_vaccin_id:system_vaccine.id).last
            if vaccine.present?
              jabs =  vaccine.jabs
              administered_jab = jabs.select{|a| a.due_on.present? }.last
              system_jab_ids_added = jabs.map(&:system_jab_id).compact.map(&:to_s)
              system_jab_year_added = jabs.map{|a| (a[:due_on] || a[:est_due_time])}.compact.map(&:to_date).map(&:year)
              if(member.birth_date.month <= 8)
                offset_year = 0
              else
                offset_year = 1
              end
              system_jabs.each do |system_jab|
                if !system_jab_ids_added.include?(system_jab.id.to_s)
                  system_jab_year = (member.birth_date + eval(system_jab.due_on)).year
                  estimated_jab_due_date = (member.birth_date + eval(system_jab.due_on)) + offset_year.year

                  if administered_jab.present?
                    administered_jab_date = administered_jab.due_on
                    due_on  = "#{administered_jab_date.day}-#{administered_jab_date.month}-#{member.birth_date.year + offset_year}".to_date
                  else
                    due_on  = "01-09-#{member.birth_date.year + offset_year}".to_date
                  end
                  due_on = due_on.to_date + eval(system_jab.due_on)
                  if administered_jab.blank?
                    jab_added_status = false
                  else
                    # check if jab administered in due time range (Sept to Jan )
                    # jab_added_status = Jab.where(:vaccination_id=>vaccine.id).or({:due_on=>{"$gte"=>due_on,"$lte"=>"31-01-#{due_on.year+1}"},:est_due_time=>{"$gte"=>estimated_jab_due_date,"$lte"=>"31-01-#{estimated_jab_due_date.year+1}"} }).last
                    jab_added_status = Jab.where(:vaccination_id=>vaccine.id).where(:due_on=>{"$gte"=>due_on,"$lte"=>"31-03-#{due_on.year+1}"} ).last
                  end
                  if !jab_added_status
                    # Skip already added jab
                    st_date = due_on.to_date
                    start_at  = st_date.to_time1("09:00")
                    end_at =   st_date.to_time1("10:00")
                    Jab.new(vaccination_id:vaccine.id,est_due_time:due_on , jab_type: system_jab.jab_type, route:system_jab.route, jab_location:system_jab.jab_location,system_jab_id:system_jab.id).save
                  end
                end
              end
            end
          end
        end
      end
    end

    def delete_system_estimated_jab_from_vaccine(jab_index,options={})
      vaccine = self 
      if jab_index >= 1
        jab = vaccine.jabs[jab_index - 1]
      else
        jab = vaccine.jabs[jab_index]
      end
      if jab.present? && jab.due_on.blank? && jab.est_due_time.present?
        jab.delete
      end
    end

  end
