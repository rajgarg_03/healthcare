class MemberNutrient
   include Mongoid::Document
   field :status, :type => String, :default=>"active"
   field :position, :type => Integer
   belongs_to :nutrient
   belongs_to :member
   index ({member_id:1})
   index ({nutrient_id:1})
   index ({position:1})
   index ({status:1})
end