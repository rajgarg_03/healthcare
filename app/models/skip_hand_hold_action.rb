class SkipHandHoldAction
  include Mongoid::Document
  include Mongoid::Timestamps

  field :member_id, :type => String 
  field :screen_name, :type => String
  field :screen_id, :type=> String 
  field :skip_count, :type => Integer , :default=> 0
  field :skipped_up_to, :type=> Time
  field :skipped, :type=> Boolean
  field :skipped_by, :type=> String
  
  index({member_id:1})
  index({screen_name:1})
  index({skipped:1})
  index({skipped_by:1})
  index({skipped_up_to:1})
  
end