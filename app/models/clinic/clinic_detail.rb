class Clinic::ClinicDetail
  include Mongoid::Document  
  include Mongoid::Timestamps
  
  field :organization_uid,         :type => String 
  field :name,                     :type => String 
  field :address,                  :type => String 
  field :postcode,                 :type => String
  field :longitude,                :type => String
  field :latitude,                 :type => String
  field :clinic_type,              :type => String # smrthi/emis
  field :clinic_id,                :type => String # 
  field :clinic_link_status,       :type => String # Active/coming_soon
  field :phone_no,                 :type => String #  
  field :email,                    :type => String #  
  field :ods_code,                 :type => String # 
  
        
  index({ods_code:1})  
  index({organization_uid:1})  
  index({clinic_type:1})  
  TppAccountLength = 9

  def self.find_by_code_or_organization_id(ods_code,organization_id,options={})
    if ods_code.present?
      where(ods_code:ods_code).last
    elsif organization_id.present?
      where(organization_uid:organization_id).last
    else
      nil
    end
  end
  
  def self.get_clinic_detail_for_organization(organization,options={})
    ::Clinic::ClinicDetail.find_by_code_or_organization_id(organization["ods_code"],organization["uid"],options)
  end
  
  def is_proxy_delink_enbled?(current_user,member,options={})
    clinic_detail = self 
    if clinic_detail.is_orion?(options)
      false
    elsif clinic_detail.is_smrthi?(options)
      false
    else
      true
    end
  end
  # options[:current_user]
  def self.save_clinic_detail(organization_obj,options={})
    clinic_attr = {:name=>organization_obj.name,
      :address=>organization_obj.address,
      :postcode=>organization_obj.postcode,
      :longitude=>(organization_obj.longitude rescue nil),
      :latitude=>(organization_obj.latitude rescue nil),
      :phone_no=>(organization_obj.phone_no rescue nil),
      :email=> (organization_obj.email rescue nil),
      :clinic_id=>(organization_obj.clinic_id rescue nil),
      :ods_code=> (organization_obj.ods_code rescue nil),
      :clinic_type=>options[:organization_type]}
      Rails.logger.info "Clinic detail for save #{clinic_attr.inspect}..................."
    clinic_attr[:clinic_link_status] = Clinic::ClinicDetail.clinic_linkable_status(options[:organization_type])
    clinic_detail  =  ::Clinic::ClinicDetail.get_clinic_detail_for_organization(organization_obj,options)

    if clinic_detail.blank?
      clinic_detail  =  Clinic::ClinicDetail.new(clinic_attr)
      clinic_detail.organization_uid = organization_obj.uid
      clinic_detail.ods_code = organization_obj.ods_code
      clinic_detail.save!
    end
    clinic_detail
  end

  def self.get_clinic_linkable_class(current_user,member,module_name,options={})
    if member.nil?
      clinic_detail = current_user.get_clinic_detail(current_user,options)
    else
      clinic_detail = member.get_clinic_detail(current_user,options)
    end
    clinic_type = clinic_detail.clinic_type.downcase rescue ""
    if clinic_type == "smrthi"
      class_name = eval("::Clinic::Smrthi::" + module_name)
    elsif "emis" == (clinic_type)# == "emis"
      class_name = eval("::Emis::" + module_name)
    elsif clinic_type == "tpp"
      class_name = eval("::Clinic::Tpp::" + module_name)
     elsif clinic_type == "orion"
      class_name = eval("::Clinic::Orion::" + module_name)
    else
      class_name = eval("::Emis::" + module_name)
    end
    class_name
  end

  #  Depricated
  def is_gpsoc?(options={})
    is_emis?(options)
  end

  # return "active/coming_soon"
  def self.clinic_linkable_status(organization_type,options={})
    Rails.logger.info "organization_type for clinic_linkable_status : #{organization_type}"
    current_user = options[:current_user]
    user = current_user.user|| User.new(email:current_user.email) rescue User.new
   # return "active" if user.user_type.downcase == "internal"
    
    clinic_detail = Clinic::ClinicDetail.new(clinic_type:organization_type)
    if clinic_detail.is_emis?(options)
      status = "active"
    elsif clinic_detail.is_tpp?(options)
      if user.present? && user.can_access_tpp_service?(options)
        status = "active"
      else
        status = "coming_soon"
      end
    elsif clinic_detail.is_smrthi?(options)
      status = "active"
    elsif clinic_detail.is_orion?(options)
      status = "active"
    else
      status = "coming_soon"
    end
    status
  end

  # return true/false
  def is_linkable?(current_user,options={})
    begin
      clinic_detail = self
      options[:current_user] = current_user
      user = current_user.user
      if clinic_detail.clinic_link_status.blank?
        clinic_link_status = ::Clinic::ClinicDetail.clinic_linkable_status(clinic_detail.clinic_type,options)
      else
        clinic_link_status = clinic_detail.clinic_link_status
      end 
      # To be removed for Live user
      if  clinic_detail.is_tpp?(options)
        if (user.can_access_tpp_service?(options))
          clinic_link_status = "Active"
        else   
          clinic_link_status = "coming_soon"
        end
      end
      linkable = clinic_link_status.to_s.downcase == "active" ? true : false
    rescue Exception => e 
      Rails.logger.info "Error inside is_linkable? ....checking linkable status  #{e.message}"
      linkable = false
    end
    linkable
  end 

  def is_emis?(options={})
    clinic_detail = self
    if ["gpsoc","emis"].include?(clinic_detail.clinic_type.to_s.downcase)
      true
    else
      false
    end
  end
  
  def is_tpp?(options={})
    clinic_detail = self
    if ["tpp"].include?(clinic_detail.clinic_type.to_s.downcase)
      true
    elsif options[:im1_account_id].present?
      options[:im1_account_id].to_s.length == ::Clinic::ClinicDetail::TppAccountLength  #  tpp account if account id length == 9 
    elsif clinic_detail.clinic_type == "unknown"
      true
    else
      false
    end
  end

  def is_smrthi?(options={})
    clinic_detail = self
    clinic_detail.clinic_type.to_s.downcase == "smrthi"
  end

   def is_orion?(options={})
    clinic_detail = self
    clinic_detail.clinic_type.to_s.downcase == "orion"
  end
  
  def get_clinic_type(options={})
    begin
      clinic_detail = self
      if clinic_detail.is_emis?(options)   
        "emis" 
      elsif clinic_detail.is_tpp?(options)
        "tpp"
      elsif clinic_detail.is_orion?(options)
        'orion'
      elsif clinic_detail.is_smrthi?(options)
        'smrthi'
      else
        'unknown'
      end
    rescue Exception => e
      Rails.logger.info "Error inside get_clinic_type.... #{e.message}"  
      'unknown'
    end
  end

  def self.identify_clinic_type(current_user,organization,options={})
    begin
      clinic_type = "unknown"
      begin
        user = current_user.user || User.new(email:current_user.email)
      rescue Exception=> e 
        user = nil
      end
      postcode = organization[:postcode]
      ods_code = organization[:ods_code]
      clinic_detail = ::Clinic::ClinicDetail.find_by_code_or_organization_id(organization[:ods_code],organization[:uid])
      if clinic_detail.present? && clinic_detail.clinic_type.present? && clinic_detail.clinic_type != "unknown"
        Rails.logger.info "Gp practice identified as #{clinic_detail.clinic_type} type..from DB for ods code ......  #{ods_code}..................."
        return clinic_detail.clinic_type
      end
      emis_data = ::Emis::Client.get_clinic_detail_by_postcode(current_user,postcode,ods_code,options)
      if emis_data[:practice_status]
        Rails.logger.info "Gp practice identified as emis type..for ods code ......  #{ods_code}..................."
        clinic_type = "emis"
      elsif user.can_access_tpp_service?(options)
        clinic_type = "tpp"
      end
    rescue Exception=>e 
      Rails.logger.info "Error inside identify_clinic_type .........ods code : #{ods_code}.............."
      Rails.logger.info e.message
    end
    clinic_type
  end  
  # options = {:current_user=>current_user}
  def self.update_clinic_detail(organization_obj,options={})
    clinic_attr = {:name=>organization_obj.name,
      :address=>organization_obj.address,
      :clinic_id=>(organization_obj.clinic_id rescue nil),
      :postcode=>organization_obj.postcode,
      :longitude=>(organization_obj.longitude rescue nil),
      :latitude=>(organization_obj.latitude rescue nil),
      :phone_no=>(organization_obj.phone_no rescue nil),
      :ods_code=> (organization_obj.ods_code rescue nil),
      :clinic_type=>options[:organization_type]}
 
    clinic_detail  =  ::Clinic::ClinicDetail.get_clinic_detail_for_organization(organization_obj,options)
    clinic_attr[:clinic_link_status] = ::Clinic::ClinicDetail.clinic_linkable_status(options[:organization_type])
    if clinic_detail.present?
      clinic_detail.update_attributes(clinic_attr)
    else
      ::Clinic::ClinicDetail.new(clinic_attr).save
    end
  end

  
  
  def self.clinic_services_available(member,current_user,options)
    begin  
      options[:current_user] = current_user
      class_name = ::Clinic::ClinicDetail.get_clinic_linkable_class(current_user,member,"Login",options)
      if options[:is_full_access] == true
        response = class_name.available_services(member.id,options)
      else
        response = {:status=>200}
      end
      if response[:status] == 200
        if options[:is_full_access] == true
          response[:services]
        else
          {:appointment_booking=>true, :repeat_prescription=>false, :view_health_records=>false, :manage_messages=>false,:immunisations=>false,:health_card=>{}}
        end
      else
        {:appointment_booking=>false, :repeat_prescription=>false, :view_health_records=>false, :manage_messages=>false,immunisations:false,:health_card=>{}}
      end
    rescue Exception=>e 
      Rails.logger.info "Error inside clinic_services_available ...............#{e.message}"
      {:appointment_booking=>false, :repeat_prescription=>false, :view_health_records=>false, :manage_messages=>false}
    end
  end

  def get_title_for_clinic_service_available(current_user,member,available_services,options={})
    clinic_detail = self
    options[:current_user] = current_user
    if clinic_detail.is_emis?(options)
      health_card = available_services[:health_card] || {}
      tab_list = [{active_status: health_card[:Immunisations], name:"Immunisations", action:"immunisations"}, 
      {active_status:(health_card[:Allergy]||false),       name: "Allergy",       action:"allergy"}, 
      {active_status:(health_card[:Consultations]||false), name: "Consultations", action:"consultations"}, 
      {active_status:(health_card[:Problems]||false),      name: "Problems",      action:"problems"}, 
      {active_status:(health_card[:Medication]||false),    name: "Medication",    action:"medication"}, 
      {active_status:(health_card[:Investigations]||false),name: "Investigations",action:"investigations"}, 
      {active_status:(health_card[:Documents]||false),     name: "Documents",     action:"documents"}]
    elsif clinic_detail.is_tpp?(options)
      tab_list = [{active_status: true, name:"Childhood Vaccinations", action:"Immunisations"}, 
        {active_status: true, name: "Patient Record",    action:"RequestPatientRecord"}, 
        {active_status: true, name: "Patient Overview",  action:"ViewPatientOverview"}, 
        {active_status: true, name: "Test Results",      action:"TestResultsView"}]
    elsif clinic_detail.is_orion?(options)
      tab_list = [{active_status: true, name:"Immunisations", action:"immunisations"}]
    else
      tab_list = []
    end
    tab_list 
  end

  def self.delink_with_clinic(member,current_user,options={})
    begin
      member_id = member.id
      options[:current_user] = current_user
      member = Member.find(member_id)
      class_name = ::Clinic::ClinicDetail.get_clinic_linkable_class(current_user,member,"Login",options)
      response = class_name.delink_with_clinic(member_id,options)
      if response[:status] == 100
        UserMailer.delay.notify_dev_for_error(response[:message],response,options,"unable to delink account clinic/clinic_detail delink_with_clinic for member #{member_id}")
      end
    rescue Exception=>e
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside clinic/clinic_detail delink_with_clinic for member #{member_id}")
    end   
  end

  def clinic_services(member,options={})
    data = []
    gp_data = {}
    clinic_detail = self
    is_emis = clinic_detail.is_emis?(options)
    is_tpp = clinic_detail.is_tpp?(options)
    is_orion = clinic_detail.is_orion?(options)
    current_user = options[:current_user]
    organization_uid =  member.organization_uid
    if options[:is_full_access].present?
      is_full_access = options[:is_full_access]
    else
      is_full_access,access_level,msg,action_identifier,access_reason = ::Clinic::User.get_access_level(current_user,member.id,options)
    end
    current_user_can_access_clinic_services = current_user.can_access_clinic_services?
    record_for_text = options[:current_user].id.to_s == member.id.to_s ? "your " : "your child's "
    if current_user_can_access_clinic_services
      data << {"title": "Link your child's online account","description": "Link your child's online account"}
      if (options[:clinic_services_status][:appointment_booking] rescue false)
        data << {"title": "Book & manage appointment","description": "Book online appointments with your GP, nurse or clinician at a time that suits you"}
      end
    end
    gp_data[:action] = []
 
    if is_full_access && current_user_can_access_clinic_services
      if is_emis || is_tpp
        clinic_services_status = options[:clinic_services_status] ||  {:appointment_booking=>true, :repeat_prescription=>true, :view_health_records=>true, :manage_messages=>true,:immunisations=>true}
        # Add data 
        if clinic_services_status[:repeat_prescription]
          data << {"title": "Order repeat prescription","description": "Request repeat prescriptions online, with delivery of your prescription to your preferred pharmacy"}
        end
        if clinic_services_status[:view_health_records]  
          data << {"title": "View #{record_for_text}health records","description": "Keep track of your medical records, including easy access to test results and details of #{record_for_text} measurements details"}
        end
        if clinic_services_status[:manage_messages] 
          data << {"title": "Send and receive messages","description": "Get and send messages from GP"}
        end
        
        # Add Action
        # if clinic_services_status[:appointment_booking]  
          gp_data[:action] << {active_status:clinic_services_status[:appointment_booking], "title": "Appointments","action_identifier": "manage_appointment"}
        # end
      
        # if clinic_services_status[:repeat_prescription]  
          gp_data[:action] << { active_status:clinic_services_status[:repeat_prescription],"title": "Prescriptions","action_identifier": "manage_prescription"}
        # end

        # if clinic_services_status[:manage_messages] 
          gp_data[:action] << {active_status:  clinic_services_status[:manage_messages] , "title": "Messages","action_identifier": "manage_messages"}
        # end

        # if clinic_services_status[:immunisations]  
        # end

        if member.is_child? 
           gp_data[:action] << {active_status: clinic_services_status[:immunisations], "title": "Immunisations","action_identifier": "immunisations","description": "Updated"}
          # if clinic_services_status[:view_health_records] 
            gp_data[:action] << {active_status:clinic_services_status[:view_health_records], "title": "Measurements","action_identifier": "Measurements","description": "Updated"}
          # end
          gp_data[:action] << {active_status: true, "title": "Documents","action_identifier": "Documents","description": "Updated"}
        end
      elsif is_orion
        clinic_services_status =  {:appointment_booking=>true, :repeat_prescription=>false, :view_health_records=>true, :manage_messages=>false,:immunisations=>false}

         if member.is_child? 
           gp_data[:action] << {active_status:true, "title": "Immunisations","action_identifier": "immunisations","description": "Updated"}
         end
      end
    else
      clinic_services_status =  {:appointment_booking=>current_user_can_access_clinic_services, :repeat_prescription=>false, :view_health_records=>false, :manage_messages=>false, :immunisations=>false}
      data << {"title": "View #{record_for_text}health records","description": "Keep track of your medical records, including easy access to test results and details of #{record_for_text} measurements details"}
      gp_data[:action] << {active_status: clinic_services_status[:appointment_booking],"title": "Appointments","action_identifier": "manage_appointment"}
      gp_data[:action] << {active_status: clinic_services_status[:repeat_prescription],"title": "Prescriptions","action_identifier": "manage_prescription"}
      gp_data[:action] << {active_status: clinic_services_status[:manage_messages] , "title": "Messages","action_identifier": "manage_messages"}
      if member.is_child? 
        gp_data[:action] << {active_status: clinic_services_status[:immunisations], "title": "Immunisations","action_identifier": "immunisations","description": "Updated"}
        gp_data[:action] << {active_status:clinic_services_status[:view_health_records], "title": "Measurements","action_identifier": "Measurements","description": "Updated"}
        gp_data[:action] << {active_status: false, "title": "Documents","action_identifier": "Documents","description": "Updated"}
        if is_orion
          gp_data[:action] = [{active_status: false, "title": "Immunisations","action_identifier": "immunisations","description": "Updated"}]
        end
      end
    end
    [data,gp_data]
  end
 
  # smrthi :clinic_link_detail = {:organization_uid=>'13753', :login_id=>"2397@gmail.com", :password=>"testnut2397"}
  # EMIS - pin doc:
  # clinic_link_detail = {"account_id"=>"2325028826","account_linkage_key"=>"TcdbmCgdUANr7","practice_ods_code"=>"A28826","surname"=>"","date_of_birth"=>"1993-04-08"},options={:register_with_pindocument=>true,:is_proxy_selected=>true/false,:proxy_user_id=>} # emis - register pin document
  # EMIS - without pin doc: 
  # clinic_link_detail = {"practice_ods_code"=>"A28826", "surname"=>"KUSHWAHA", "date_of_birth"=>"1993-04-08", "gender"=>"M", "house_name_no"=>"9", "postcode"=>"BR5 1QB", "email"=>"ronit1.garg@gmail.com", "mobile_no"=>""},options={:register_with_pindocument=>false,:is_proxy_selected=>true/false,:proxy_user_id=>} # emis - register without pin document 
  # Orion - doc:
  # clinic_link_detail = {"first_name"=> "" ,practice_ods_code"=>"orion", "surname"=>"KUSHWAHA", "date_of_birth"=>"1993-04-08", "email"=>"ronit.garg@gmail.com", "patient_id"=>AAA-233-HUY12"",:register_with_pindocument=>true} 
 # options[:current_user]
  def self.get_authentication_from_clinic(current_user,member_id,clinic_link_detail,options={})
    member = Member.find(member_id)
    clinic_link_detail = clinic_link_detail.with_indifferent_access rescue clinic_link_detail
    ods_code = clinic_link_detail["practice_ods_code"]
    options[:im1_account_id] = clinic_link_detail[:account_id]
    organization_uid =  clinic_link_detail[:organization_uid] || member.organization_uid
    clinic_detail = ::Clinic::ClinicDetail.find_by_code_or_organization_id(ods_code,organization_uid,options)
    if clinic_detail.is_smrthi?(options)
      status,response = clinic_detail.clinic_system_authentication(current_user,clinic_link_detail,member_id,options)
    
    elsif clinic_detail.is_emis?(options) || clinic_detail.is_orion?(options)
      status,response = clinic_detail.clinic_system_authentication(current_user,clinic_link_detail,member_id,options)

      # response  = {"data"=>{"patient_relationship_id"=>"3a3794e3-0b18-4f50-968d-b1ed12ebcfb0", "patient_relationship_key"=>"IhmlSR0Uw1aQICZhmadLcUchjrNwDTxUcqmwFRNZjrM=",  "title"=>"Master", "first_names"=>"Mike", "surname"=>"Kushwaha", "calling_name"=>"Mike", "patient_identifiers"=>{"value"=>nil, "type"=>"NHS Number"}, "date_of_birth"=>'Thu, 22 Sep 2016'.to_date, "contact_details"=>{"email_address"=>"dharmendra@nurturey.com", "mobile_number"=>"07737697939", "telephone_number"=>nil, "address"=>{"house_name_flat_number"=>"91", "number_street"=>"Willett Ways", "village"=>nil, "town"=>"Petts Wood", "county"=>nil, "postcode"=>"BR5 1QB"}, "status"=>"Current"}, "gender"=>"M"}, "message"=>nil, "status"=>200}
      # status = "authenticated"
    elsif clinic_detail.is_tpp?(options)
      status,response = clinic_detail.clinic_system_authentication(current_user,clinic_link_detail,member_id,options)
      if (status == "authenticated" || status == 200) && (clinic_detail.clinic_type.blank? ||  clinic_detail.clinic_type =='unknown')
        clinic_detail.update_attribute(:clinic_type,"tpp")
      end
    else
      clinic_link_detail[:message] = ::Message::Clinic[:gp_not_enable_with_nurturey]
      status,response = ["Unauthenticated",clinic_link_detail]
    end
    if status == "authenticated" || status == 200

      clinic_link_state = 3 # set linking is completed
      if options[:register_with_pindocument].nil?
        options[:access_level] = 1 # linked with non gpsoc with full access
      else
        options[:access_level] = options[:register_with_pindocument] == true ? 1 : 2
      end
      options[:authentication_response] = response
      options[:current_user] = current_user
      Clinic::LinkDetail.save_record(member_id,clinic_link_detail,organization_uid,options)
      Clinic::User.save_record(member_id,organization_uid,clinic_link_state,"user",options)
    end
    [status,response[:message]]
  end

  # user_details = {:user_name=>"2397@gmail.com", :user_password=>"testnut2397"} # smrthi
  # user_details = {"account_id"=>"2325028826","practice_ods_code"=>"A28826","surname"=>"","date_of_birth"=>"1993-04-08"},options={:register_with_pindocument=>true} # orion
  # user_details = {"account_id"=>"2325028826","account_linkage_key"=>"TcdbmCgdUANr7","practice_ods_code"=>"A28826","surname"=>"","date_of_birth"=>"1993-04-08"},options={:register_with_pindocument=>true} # emis - register pin document
  # user_details = {"practice_ods_code"=>"A28826", "surname"=>"KUSHWAHA", "date_of_birth"=>"1993-04-08", "gender"=>"M", "house_name_no"=>"9", "postcode"=>"BR5 1QB", "email"=>"ronit1.garg@gmail.com", "mobile_no"=>""},options={:register_with_pindocument=>false} # emis - register without pin document 
  # user_details = {"first_name"=> "" ,practice_ods_code"=>"orion", "surname"=>"KUSHWAHA", "date_of_birth"=>"1993-04-08", "email"=>"ronit.garg@gmail.com", "patient_id"=>AAA-233-HUY12"",:register_with_pindocument=>true} # Orion
  def clinic_system_authentication(current_user,clinic_link_detail,member_id,options={})
    clinic_detail = self
    user_details = clinic_link_detail
    options[:current_user] = current_user
    if clinic_detail.is_smrthi?(options)
        user_data = Clinic::Smrthi::Login.authenticateUser(user_details,options)
        if user_data[:status] == 200
          status = Clinic::Smrthi::Login.authentication_status(user_data)
        else
          status = "unauthenticated"
        end
    elsif clinic_detail.is_emis?(options)
        user_data = Emis::Login.authenticateUser(member_id,user_details,options)
        # GpsocLogger.info  "..................Authentication response ..................................................."
        status = user_data[:status] == 200 ? "authenticated" : "unauthenticated"
    elsif clinic_detail.is_tpp?(options)
        user_data = ::Clinic::Tpp::Login.authenticateUser(member_id,user_details,options)
        status = user_data[:status] == 200 ? "authenticated" : "unauthenticated"
    elsif clinic_detail.is_orion?(options)
      user_data = clinic_link_detail
      status = "authenticated"
    else
      status = "authenticated"
    end
   [ status,user_data]
  end
  
  def format_data(current_user,member,options={})
    options[:current_user] = current_user
    clinic_detail =  member.get_clinic_detail(current_user,options) || self
    im_info = ::Clinic::LinkDetail.get_clinic_link_detail(member.id,current_user,options) || {}
    organization_uid = clinic_detail.organization_uid.blank? ? clinic_detail.ods_code : clinic_detail.organization_uid
    data = {  
      :id=>clinic_detail.id,
      :organization_uid=>organization_uid,
      :title=>clinic_detail.name,
      :postcode=>clinic_detail.postcode,
      :ods_code=>clinic_detail.ods_code,
      :longitude =>clinic_detail.longitude,
      :latitude =>clinic_detail.latitude,
      :address=>clinic_detail.address,
      :phone_no=>clinic_detail.phone_no,
      :email=>clinic_detail.email,
      :user_phone_no => im_info["phone_no"],
      :user_email => im_info["email"],
      :organization_type=>clinic_detail.clinic_type,
      :delinkable=> (clinic_detail.is_delinakable?(options) rescue '')
    }
      is_full_access, access_level, msg , action_identifier,access_reason = ::Clinic::User.get_access_level(current_user,member.id,options)
      data[:is_full_access] = is_full_access
      data[:action_identifier] = action_identifier
      data[:access_reason] = access_reason
      data[:show_two_factor_auth] = action_identifier == "AccountVerification"
      data[:access_level] = access_level
      data[:message_for_access] = msg
    data  
  end

  def is_delinakable?(options={})
    return false # disable delinking for now
    clinic_detail = self
    if clinic_detail.is_smrthi?(options)
      true
    elsif clinic_detail.is_orion?(options)
      true
    elsif clinic_detail.is_emis?(options)
      true
    elsif clinic_detail.is_tpp?(options)
      true
    else
      true
    end 
  end
  
  def self.setup_member_clinic_for_proxy_linking(current_user,member_id,ods_code,options={})
    organization_obj = ::Nhs::NhsApi.get_clinic_by_ods_code(current_user,member_id,ods_code,options)
    member = Member.find(member_id)
    member.update_attribute(:ods_code ,ods_code) if member.ods_code.blank?
    if organization_obj.present?
      options[:clinic_detail] = organization_obj.attributes
      options[:current_user] = current_user
      organization_uid = organization_obj.uid
      clinic_link_state = 2 
      ::Clinic::User.save_record(member_id,organization_uid,clinic_link_state,"user",options)
    else
      raise 'No GP found to setup. Please try with correct practice code'
    end
  end

  def get_organisation_data
    clinic_detail = self
    organisation_data = {name: clinic_detail.name,
      uid: clinic_detail.organization_uid,
      :address=>(clinic_detail.address rescue nil),
      :postcode=>clinic_detail.postcode,
      :longitude=>(clinic_detail.longitude rescue nil),
      :latitude=>(clinic_detail.latitude rescue nil),
      :phone_no=> clinic_detail.phone_no,
      :clinic_id=>clinic_detail.clinic_id,
      :ods_code=> clinic_detail.ods_code ,
      :clinic_type=>clinic_detail.clinic_type 
    }
    organization_obj = Clinic::Organization.new(organisation_data)
    organization_obj
  end


  def self.healthcare_widget(current_user,api_version=5,options={})
    user = current_user.user
    member_id = current_user.id
    user_country = Member.member_country_code(user.country_code)
    title_list = {"gb"=>'NHS and health services',  "ae" => 'Health services'}
    title = title_list[user_country] || 'Doctor services'
    current_user_can_access_clinic_services = current_user.can_access_clinic_services?
    # To restrict any providereg FB, Apple etc to access clinic 
    account_type_can_access_clinic_services = user.can_access_clinic_services_with_allowed_account_type?
    is_full_access,access_level,msg,action_identifier,access_reason = ::Clinic::User.get_access_level(current_user,member_id,options)
    if  user_country == "gb"
      booking_status = current_user.clinic_link_state > 2
      manage_clinic_status = true
      if !account_type_can_access_clinic_services
        if user.user_account_type == "Facebook" 
          manage_clinic_status = false
          booking_status = false
          is_full_access = false
          action_identifier = "FacebookAccount"
        elsif user.user_account_type == "Apple"
          manage_clinic_status = false
          booking_status = false
          is_full_access = false
          action_identifier = "AppleAccount"
        end
      end
       
        actions = [
          {active_status: true, :identifier=> 'nhs_library',:title=>'NHS library'},
          {active_status: manage_clinic_status, :identifier=> 'manage_clinic',:title=>'Manage GP'},
          {active_status: booking_status, :identifier=>"manage_appointment",:title=>'Appointments'},
          {active_status: is_full_access, :identifier=>"manage_prescription",:title=>'Prescriptions'},
          {active_status: is_full_access, :identifier=>"manage_messages",:title=>'Messages'}
          
        ]
    elsif user_country == "ae" && (current_user.device_platform == "ios" || user.is_internal?)
      actions = [
          {active_status: manage_clinic_status, :identifier=> 'manage_clinic',:title=>'Link Malaffi'},
          {status: true,:identifier=>"book_appointment",:title=>'Book with SEHA'}
        ]
    elsif user_country == "in" && user.is_internal?
      title = 'Doctor services'
      actions = [
        {active_status:true, :identifier=> 'manage_clinic',:title=>'Manage clinic'},
        {active_status:true, :identifier=>"manage_appointment",:title=>'Manage appointments'}
      ]
    else
      title = ''
      actions = []
    end

    access_reason = ::Message::Clinic[:health_card_widget][action_identifier.to_sym]
    if !current_user_can_access_clinic_services
      if !current_user.clinic_services_active?
        access_reason = ::Message::Clinic[:clinic_access_blocked]
      else
        access_reason = ::Message::Clinic[:clinic_access_limit_exceeded]
      end
    end
    data = {access_reason:access_reason, :title=>title,:actions=>actions}
  end

   
end