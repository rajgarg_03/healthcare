class Clinic::UserOtp
  include Mongoid::Document  
  include Mongoid::Timestamps
  # User from organization linked
  field :member_id,                :type => String 
  field :user_id,                  :type => String  
  field :otp,                      :type => String
  
  #options[:current_user]
  def self.generate_otp(member_id,current_user,options={})
    begin
      otp_receiving_source = options[:otp_receiving_source] || "email"
      user_id = current_user.user_id
      otp = rand(1e5...1e6).to_i
      family_id = FamilyMember.where(member_id:member_id).last.family_id rescue nil  
      ::Clinic::UserOtp.where(member_id:member_id).delete_all
      ::Clinic::UserOtp.create(otp:otp,member_id:member_id,user_id:user_id)
      clinic_link_detail = ::Clinic::LinkDetail.get_clinic_link_detail(member_id,current_user,options)
      clinic_link_detail.update_attribute(:otp_status,0) # set otp status 'generated'
      if otp_receiving_source == "email"
        email = clinic_link_detail.ods_code == "orion" ?  AppConfig["superadmin_email"] : clinic_link_detail.email
        ClinicMailer.otp_for_clinic_access_authentication(email, member_id,user_id,otp,options).deliver! #rescue nil
      elsif otp_receiving_source == "mobile" || otp_receiving_source == "phone"
        msg = "Nurturey: Account verification code #{otp}"
        phone_number =  clinic_link_detail.phone_number
        Clinic::UserOtp.send_sms(current_user,member_id,phone_number,msg,options)
      end
      status = true
    rescue Exception => e 
      Rails.logger.info e.message
      status = false
    end
    custom_id = 245
    options[:status] = status ? 'success' : 'failed'
    ::Report::ApiLog.log_event(user_id,family_id,custom_id,options) rescue nil
    status
  end


   def self.verify_otp(member_id,current_user,otp, options={})
    begin
      raise 'Please provide OTP' if otp.blank?
      family_id = FamilyMember.where(member_id:member_id).last.family_id rescue nil  
      otp_record = Clinic::UserOtp.where(otp:otp,member_id:member_id,user_id:current_user.user_id).last
      raise 'The OTP entered is incorrect' if otp_record.blank?
      clinic_link_detail = ::Clinic::LinkDetail.get_clinic_link_detail(member_id,current_user,options)
      clinic_link_detail.update_attribute(:otp_status,1) # set otp status 'verified'
      otp_record.delete
      status = true
    rescue Exception => e 
      Rails.logger.info e.message
      status = false
    end
    options[:status] = status ? 'success' : 'failed'
    custom_id = 246
    ::Report::ApiLog.log_event(user_id,family_id,custom_id,options) rescue nil
    status
  end


  def self.send_sms(current_user,member_id,phone_number,msg,options={})
    phone_number =  phone_number.to_s.gsub("+","")
    user = current_user.user
    user_country_code_list = Member.sanitize_country_code(user.country_code)
    if user_country_code_list.include?("uk")
      if phone_number[0] == "0"
        phone_number = phone_number[1..-1]
      end
      if phone_number[0..1] != "44"
        phone_number = "+44" + phone_number 
      end
    elsif user_country_code_list.include?("in")
      if phone_number[0..1] != "91"
        phone_number = "+91" + phone_number
      end
    end
    Aws.config.update({
      region: 'eu-west-2',
      credentials: Aws::Credentials.new(APP_CONFIG["ses_access_key_id"], APP_CONFIG["ses_secret_access_key"])
    })
    sns = Aws::SNS::Client.new(region: 'eu-west-2')
    sns.publish({phone_number:phone_number,message:msg})
  end
  
end