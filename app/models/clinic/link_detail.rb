class Clinic::LinkDetail
  include Mongoid::Document  
  include Mongoid::Timestamps
  
  field :member_id,                     :type => String 
  field :parent_member_id,              :type => String 
  field :link_info,                     :type => Hash   
  field :user_info,                     :type => Hash    
 
  field :patient_uid,                   :type => String  # third party uniq patient id
  field :otp_status,                    :type => Integer # (0:genaerated,1 verified)
  field :accessible_to_members,         :type => Hash, :default=>{}    # 1,2,3 (1=> full, 2 => partial, 3=> none){"23i2u3123"=>2,"123232323"=>1, "3333333"=>0}  # list of member id who can access recordeg{member_id:access_level} 
  field :linked_proxy_users,            :type => Array ,:default=>[]  # ["23i2u3123","123232323"] 
  field :phone_number,                  :type => String
  field :email,                         :type => String
  field :organization_uid,              :type => String  
  field :ods_code,                      :type => String  
   
  
  def two_factor_auth_required?
    clinic_link_detail = self
    ods_code =  (clinic_link_detail.ods_code || clinic_link_detail.link_info["practice_ods_code"] ) rescue ""
    clinic_detail = ::Clinic::ClinicDetail.find_by_code_or_organization_id(ods_code,clinic_link_detail.organization_uid)
    false if clinic_detail.is_smrthi?()
    return false if (clinic_link_detail.link_info["proxy_user_status"] rescue false)
    two_factor_authentication_required = clinic_link_detail.otp_status.to_i == 0 ? true : false
    two_factor_authentication_required 
  end

  # options = {:current_user}
  def self.save_record(member_id,clinic_link_detail,organization_uid,options={})
    member = Member.find(member_id)
    Rails.logger.info '................ ....................................................'
    Rails.logger.info '................ response before saving clinic link detail..........'
    #Rails.logger.info "................ .........#{options.inspect}...organization_uid: #{organization_uid}........................................"
    Rails.logger.info "................ .clinic_link_detail........"
    current_user = options[:current_user]
    clinic_detail = member.get_clinic_detail(current_user,options)
    ods_code = clinic_detail.ods_code
    Clinic::LinkDetail.where(member_id:member_id,:parent_member_id=>current_user.id).delete_all
    if clinic_detail.clinic_type.downcase == "smrthi"
      session_key = options[:authentication_response]["userAuthToken"]
      clinic_link_detail[:auth_token] = session_key
      patient_uid = options[:authentication_response]["PatientDetails"]["patientId"]
      phone_number = options[:authentication_response]["PatientDetails"]["patientPhone"]
      email = options[:authentication_response]["PatientDetails"]["patientEmail"]
      otp_status = 1 # no need to otp verification
      linked_proxy_users = []
      Clinic::LinkDetail.save_user_session_detail(member_id,session_key,options)
    elsif clinic_detail.is_orion?(options)
      otp_status = nil
      patient_uid = clinic_link_detail["patient_id"]
      phone_number = options[:authentication_response][:phone_number]
      date_of_birth =  options[:authentication_response][:date_of_birth]
      email = options[:authentication_response][:email]
      accessible_to_members = {current_user.id.to_s=> 1 }
    elsif clinic_detail.is_emis?(options)
      otp_status = nil
      options[:authentication_response] = (options[:authentication_response] || {}).with_indifferent_access
      options[:authentication_response] = options[:authentication_response][:data]
      patient_uid = options[:authentication_response][:patient_context_guid]
      clinic_link_detail[:access_identity_guid] = options[:authentication_response][:access_identity_guid]
      clinic_link_detail[:patient_context_guid] = options[:authentication_response][:patient_context_guid]
      clinic_link_detail[:proxy_user_status] = options[:is_proxy_selected] 
      # remove account id and linkage if if there
      clinic_link_detail.delete(:account_linkage_key)
      clinic_link_detail.delete(:account_id)
      phone_number = options[:authentication_response][:mobile_number]
      date_of_birth =  options[:authentication_response][:date_of_birth]
      email = options[:authentication_response][:email]
      accessible_to_members = {current_user.id.to_s=>(clinic_link_detail["register_with_pindocument"] == "true" || options[:is_proxy_selected] == true) ? 1 : 2 }

      linked_proxy_users = [options[:proxy_user_id]].flatten.compact
    elsif ["tpp"].include?(clinic_detail.clinic_type.downcase)
      otp_status = nil
       
      options[:authentication_response] = (options[:authentication_response] || {}).with_indifferent_access
      options[:authentication_response] = options[:authentication_response]
      patient_uid = options[:authentication_response][:patientId]
      clinic_link_detail[:onlineUserId] = options[:authentication_response][:onlineUserId]
      clinic_link_detail[:patientId] = options[:authentication_response][:patientId]
      clinic_link_detail[:passphrase] = options[:authentication_response][:passphrase]
      clinic_link_detail[:proxy_user_status] = options[:is_proxy_selected] 
      phone_number = options[:authentication_response][:mobile_number]
      date_of_birth =  options[:authentication_response][:date_of_birth]
      email = options[:authentication_response][:email]
      accessible_to_members = {current_user.id.to_s=>(options[:is_proxy_selected] == true ? 1 : 1)}
      linked_proxy_users = [options[:proxy_user_id]].flatten.compact

    else
      patient_uid = nil
    end
    clinic_link_detail.delete(:account_linkage_key) rescue nil
    options[:authentication_response].delete("account_linkage_key")  rescue nil 
    link_detail = Clinic::LinkDetail.new(ods_code:ods_code, parent_member_id:current_user.id,linked_proxy_users: linked_proxy_users, accessible_to_members:accessible_to_members, organization_uid:organization_uid, phone_number:phone_number, email:email, otp_status:otp_status, patient_uid:patient_uid, member_id:member_id,user_info:options[:authentication_response], link_info:clinic_link_detail)
    link_detail.save
    GpsocLogger.info  "..................Link detail saved .................................................."
  end

  def self.save_user_session_detail(member_id,session_key,session_id,options={})
    parent_member_id = options[:parent_member_id] || options[:current_user].id
    ::Clinic::UserClinicSession.where(member_id:member_id,parent_member_id:parent_member_id).delete_all
    ::Clinic::UserClinicSession.new(parent_member_id:parent_member_id,member_id:member_id,session_key:session_key,session_id:session_id,sequence_number:options[:sequence_number]).save!
  end

  def self.get_operation_list(member,options={})
    current_user = options[:current_user]
    clinic_detail = member.get_clinic_detail(current_user,options)
    service_list,gp_data = clinic_detail.clinic_services(member,options)
    service_list
  end


  # clinic link detail for self /child member eg EMIS/TPP proxy user
  def self.get_clinic_link_detail(member_id,current_user,options={})
    # if member_id == current_user.id
    #   ::Clinic::LinkDetail.where(member_id:member_id).last
    # else
      ::Clinic::LinkDetail.where(member_id:member_id,parent_member_id:current_user.id).last
    # end
  end 


  def self.get_linked_proxy_users(current_user,options={})
    ::Clinic::LinkDetail.where(parent_member_id:current_user.id)
  end

  def self.link_proxy_user(current_user,member_id,ods_code, patient_uid,options={})
    begin
      Clinic::ClinicDetail.setup_member_clinic_for_proxy_linking(current_user,member_id,ods_code,options)
    rescue Exception=>e 
      return {status:100, message:e.message}
    end
    # Get parent clinic link detail
    user_clinic_link_detail = ::Clinic::LinkDetail.get_clinic_link_detail(current_user.id,current_user,options)
    clinic_link_detail =  user_clinic_link_detail.link_info
    clinic_link_detail[:patient_context_guid] = patient_uid
    clinic_link_detail = clinic_link_detail.with_indifferent_access
    
    options[:is_proxy_selected] = true
    options[:register_with_pindocument] = true # only pin document registred user can link proxy user
    options[:current_user] = current_user
    options[:proxy_user_id] = current_user.id
    options[:proxy_user_link_flow] = true
    authenticate_status,msg = Clinic::ClinicDetail.get_authentication_from_clinic(current_user,member_id,clinic_link_detail,options)
    Rails.logger.info "Proxy linking completed for member #{member_id} with status .....#{authenticate_status}"
    # Delink proxy with same patient uid
    # if  authenticate_status == "authenticated"
    #   member_ids = ::Clinic::LinkDetail.where(:parent_member_id=> current_user.id, patient_uid:patient_uid,:member_id.ne=>member_id).pluck(:member_id)
    #   Rails.logger.info "Removing duplicate proxy linking for member ids..................... #{member_ids.inspect}"
    #   member_ids.each do |member_id|
    #     ::Clinic::Organization.delink_clinic(member_id,current_user.organization_uid,options)
    #   end
    # end
    [authenticate_status,msg]
  end

  def self.delink_proxy_user(current_user,linked_proxy_member_ids=nil,options={})
    if linked_proxy_member_ids.blank?
      linked_proxy_member_ids = ::Clinic::LinkDetail.get_linked_proxy_users(current_user,options).map(&:member_id)
    end
    # Delink child not in proxy list 
    if !options[:proxy_user_data].nil?
      patient_uids = options[:proxy_user_data].select{|a| a[:proxy_active_status]}.map {|a| a[:patient_uid]}
      linked_proxy_member_uids  = ::Clinic::LinkDetail.get_linked_proxy_users(current_user,options).map(&:patient_uid)
      removed_proxy_user_from_gpsoc_uids = linked_proxy_member_uids - patient_uids
      removed_proxy_user_from_gpsoc = ::Clinic::LinkDetail.where(:patient_uid.in=>removed_proxy_user_from_gpsoc_uids,:parent_member_id=>current_user.id).map(&:member_id).map(&:to_s)
    
    else
      removed_proxy_user_from_gpsoc = []
    end
    # Delink user with age above 13 years and not in Proxy list
    (linked_proxy_member_ids) .each do |member_id|
      begin
        member = Member.find(member_id)
        clinic_detail = member.get_clinic_detail(current_user,options)
        if clinic_detail.is_proxy_delink_enbled?(current_user,member,options)
          if member.is_expected? ||  (member.is_child? && member.age_in_months > (GlobalSettings::EmisRegistrationAllowedAgeLimit * 12) )
            ::Clinic::Organization.delink_clinic(member_id,member.organization_uid,options)
          end
        end
      rescue Exception=>e 
        Rails.logger.info "Error inside delink_proxy_user.....1.... #{e.message}"
        Rails.logger.info e.message
      end 
    end

    (removed_proxy_user_from_gpsoc).each do |member_id|
      begin
        member = ChildMember.where(id:member_id).last
        if member.present?
          clinic_detail = member.get_clinic_detail(current_user,options)
          if clinic_detail.is_proxy_delink_enbled?(current_user,member,options)
            ::Clinic::Organization.delink_clinic(member_id,member.organization_uid,options) if member.present?
          end
        end
      rescue Exception=>e 
        Rails.logger.info "Error inside delink_proxy_user.....2...#{e.message}."
        Rails.logger.info e.message
      end 
    end

  end
end