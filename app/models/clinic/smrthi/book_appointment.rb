class Clinic::Smrthi::BookAppointment
  
# Description – This will return the future appointments of patient . 
# Method Name - Appointments/GetPatAppointmentList 
# Parameter – patientDetails (send json with details)
# Sample - { "patientId": "2397", "userEmail": "2397@gmail.com", "userAuthToken": "s5,!NAktLgFwhg@,PLI.HymxBYnHnWHAeeefQJToG$QTI6._byWrTNZPrX,JkpET_mkgaA@TguC^txYKZ bX"}
# Return Value – Json string (Example – [ {
# "patientID": "3097","patientName": "SHAFRIN", "mobileNo": "9444898644", "appointmentDate": "19/07/2019", 
# "appointmentTime": "17:30", "doctorCode": "SUB",
# "doctorName": "DR. Subramanian (SUB)", "appointmentSource": "WEB", "locationName": "SMRTHI", "addressLine1": "92, Dr Ranga Road,", "addressLine2": "",
# "addressLine3": "Alwarpet", "city": "CHENNAI", "country": "TAMILNADU", "pincode": "600018"
# }
#  ]
  def self.getPatienAppointment(member_id,options)
    begin
      # { "patientId": "2397", "userEmail": "2397@gmail.com", "userAuthToken": "s5,!NAktLgFwhg@,PLI.HymxBYnHnWHAeeefQJToG$QTI6._byWrTNZPrX,JkpET_mkgaA@TguC^txYKZ bX"}
      member = Member.find(member_id)
      auth_token = ::Clinic::UserClinicSession.where(member_id:member_id).last.session_key
      action_url = "Appointments/GetPatAppointmentList"
      message =  {:patientDetails=>{
          :userAuthToken=>auth_token
          }
        }
      data = []
      response = Clinic::Smrthi::BookAppointment.make_request(action_url,message)
      (response || []).each do |appointment|
      data << {:id=>appointment[:appointment_id],
                  :purpose=>appointment["visitReason"], 
                  :start_time=>Time.find_zone("Kolkata").parse(appointment["appointmentDate"] + " " + appointment["appointmentTime"]).to_time,
                  :end_time=>nil, 
                  :appointment_id=>appointment[:appointment_id],
                  :doctor_name=>appointment["doctorName"],
                  :doctor_clinic_id =>appointment["clinician_id"],
                  :appointment_purpose=>appointment["visitReason"],
                  :appointment_status=>"confirmed",
                  :member_id=>member_id,
                  :organization_uid=>member.organization_uid,
                  :can_cancel=>false,
                  :can_reschedule=> false,
                  :doctor_specialization=>appointment["doctorSpecialisation"]
                }
      end
      data
      {:appointments=>data,:status=>200}
    rescue Exception=>e
      # APPOINTMENT_004 
      if e.message == "Invalid Auth Key"
         Clinic::Smrthi::BookAppointment.get_auth_token(member_id,options)
        retry  
      end
      {:appointments=>[],:status=>100,:error=> e.message,message:"Unable to get appointment for now. Try again later"}
    end

  end

  def self.get_auth_token(member_id,options={})
    ::Clinic::Smrthi::ApiRequest.get_auth_token(member_id,options)
    # clinic_link_detail = Clinic::LinkDetail.where(member_id:member_id).last.link_info
    # user_details = {:login_id=>clinic_link_detail["login_id"], :password=>clinic_link_detail["password"]}
    # organization_uid = clinic_link_detail["organization_uid"]#="WKI72JH"
    # response =  Clinic::Smrthi::Login.authenticateUser(user_details,options)
    # session_key = response["userAuthToken"]
    # options[:parent_member_id] = clinic_link_detail.parent_member_id
    # Clinic::LinkDetail.save_user_session_detail(member_id,session_key,options)
 
  end

  # options= {:doctor_details=>{:doctor_clinic_id=>17,:doctor_id=>16}}
  def self.get_available_appointments(organization_uid,member_id,options={})
  	 
   begin
      clinic_detail = Clinic::ClinicDetail.where(organization_uid:organization_uid).last
      location_id = clinic_detail.clinic_id
      action_url = 'Appointments/GetAppointmentSlots'
      doctor_details = options[:doctor_details].with_indifferent_access
      message =  {:doctorDetails=>{
        :locationId=> (location_id || 1).to_s,
        :apiKey=>APP_CONFIG["smrthi_access_key"],
        :appmntDate=> Date.today.strftime("%d/%m/%Y"),
        :doctorClinicId=> doctor_details[:doctor_clinic_id].to_s, 
       
        :doctorId=>doctor_details[:doctor_id].to_s
        }
      }
      response = Clinic::Smrthi::BookAppointment.make_request(action_url,message)
      data = {}
      # {"Api_ErrorCode"=>"DOCTORAPPMNTCOUNT_002", "Api_ErrorDesc"=>"Unknown error occurred. Please try later."}
      if ((response["Api_ErrorDesc"].present? && response["Api_ErrorDesc"] == "Unknown error occurred. Please try later.") rescue false)
        data[:message] = response["Api_ErrorDesc"]
        status = 100
      else
        response.each do |appointment_details|
          appointment_date = appointment_details["appointmentDate"].to_date.to_s
          location_id = appointment_details["locationId"]
          doctor_id = appointment_details["doctorId"]
          
          data[appointment_date] = data[appointment_date] || []
          appointment_details["Slots"].each do |slot|
            doctor_clinic_id =   slot["doctorClinicId"]
            doctor_code =   slot["doctorCode"]
            slot["windows"].each do |slot_window|
              start_time ,end_time = slot_window["window"].split("-")
              start_time = Time.parse(Date.today.to_s + " " + start_time).strftime("%H:%M")
              end_time = Time.parse(Date.today.to_s + " " + end_time).strftime("%H:%M")
              data[appointment_date] << {:doctor_id=>doctor_id,:location_id=>location_id, :doctor_code=>doctor_code, :doctor_clinic_id=>doctor_clinic_id,start_time:start_time, end_time:end_time, :slot_id=>slot_window["slotId"],:appointment_count=>slot["appointmentCount"]}
            end
          end
        end
      end
      status = 200
    rescue Exception=>e
      status = 100 
      data = {}
      Rails.logger.info "Error inside get_doctor_detail-- #{e.message}"
      puts "Error inside get_doctor_detail-- #{e.backtrace}"
      Rails.logger.info "Error inside get_doctor_detail-- #{e.backtrace}"
      if e.message == "Invalid Auth Key"
         Clinic::Smrthi::BookAppointment.get_auth_token(member_id,options)
        retry  
      end
    end
    [status,data]
  end

  def self.get_doctor_list(organization_uid,member_id,options)
    begin
      clinic_detail = Clinic::ClinicDetail.where(organization_uid:organization_uid).last
      location_id = clinic_detail.clinic_id
      action_url = 'Appointments/GetDoctorsList'
      message =  {:locationDetails=>{
        :locationId=> (location_id || 1).to_s,
        :apiKey=>APP_CONFIG["smrthi_access_key"]
        }
      }
      response = Clinic::Smrthi::BookAppointment.make_request(action_url,message)
      data = []
      response.each do |doctor_detail|
        detail = {}
        detail[:visit_purpose] = doctor_detail["visitPurpose"].split(",") rescue nil
        name = (doctor_detail["title"].strip + " " + doctor_detail["Name"].strip)
        specialization = doctor_detail["specialization"].titleize
        detail[:doctor_id] = doctor_detail["doctorId"]
        data << detail.merge({"name"=> name,"specialization"=>specialization})
      end
    rescue Exception=>e 
      Rails.logger.info "Error inside get_doctor_detail-- #{e.message}"
      puts "Error inside get_doctor_detail-- #{e.message}"
      
      data = []
      if e.message == "Invalid Auth Key"
         Clinic::Smrthi::BookAppointment.get_auth_token(member_id,options)
        retry  
      end
    end
    data
  end
  
  def self.make_request(action_url,message,options={})
    Clinic::Smrthi::ApiRequest.make_request(action_url,message,options)
  #   begin
  #     Rails.logger.info ".............................."
  #     Rails.logger.info message.inspect
  #     Rails.logger.info ".............................."
  #     header = {'Content-Type'=> 'text/json'}
  #     uri = URI.parse("http://www.nurtureyapi.heca.in/#{action_url}")
  #     http = Net::HTTP.new(uri.host, uri.port)
  #     request = Net::HTTP::Post.new(uri.request_uri, header)
  #     request.body = message.to_json
  #     response = http.request(request)
  #     response_data = JSON.parse(response.body)
  #   rescue Exception=>e 
  #     Rails.logger.info "Error inside make_request-- #{e.message}"
  #     puts "Error inside make_request-- #{e.message}"
      
  #     response_data = {}
  #   end
  #   Rails.logger.info ".............................."
  #   Rails.logger.info response_data.inspect
  #   Rails.logger.info ".............................."
  #   raise response_data["Api_ErrorDesc"] if (response_data["Api_ErrorDesc"] == "Invalid Auth Key" rescue false)
  # response_data
  end

  def self.cancel_appointment(organization_uid,member_id,options={})
    {id:Random.rand(99999999999),status:200}
  end

  # :appointment_details=>{:slot_id=> , :doctor_code=>'SUB',:start_date=> '10-07-2019',:start_time=>'9:00',:end_time=>'10:00',:appointment_purpose=>'consulatation'} }  
  def self.book_appointment(organization_uid,member_id,appointment_details,options={})
    begin
      clinic_link_detail = Clinic::LinkDetail.where(member_id:member_id).last
      user_info = clinic_link_detail.user_info || {} rescue {}
      # patient_id = user_info["patientID"] || (user_info["userEmail"].split("@")[0] rescue nil) || "3097"
      # patient_name = user_info["userName"] || "SHAFRIN"
      mobile_no = appointment_details[:mobile_no] || user_info["userPhone"] || "9840112481"
      auth_code = ::Clinic::UserClinicSession.where(member_id:member_id).last.session_key
      action_url = 'Appointments/BookAppointment'
      Rails.logger.info "................................................"
      Rails.logger.info appointment_details.inspect

      message =  {:appointmentDetails=>{
        :appointmentDate=> appointment_details[:start_date].to_date.strftime("%d/%m/%Y"),
        :appointmentTime=>"",
        :visitReason=>appointment_details[:appointment_purpose],
        :doctorCode => appointment_details[:doctor_code],
        :slotId=>appointment_details[:slot_id],
        :mobileNo=>mobile_no,
        :authCode=> auth_code,
        :apiKey=>APP_CONFIG["smrthi_access_key"]
        }
      }

      # message = {appointmentDetails:{ "apiKey": "hecatest", "patientID": "3097","patientName": "SHSAFRIN", "mobileNo": "9840112481", "appointmentDate":"18/07/2019", "appointmentTime":"", "doctorCode":"SUB","visitReason":"nurturey test" } }
      response = Clinic::Smrthi::BookAppointment.make_request(action_url,message)
      if response["Api_ResponseCode"] == "APPOINTMENT SUCCESS" && response["AppointmentDetails"].present?
       # {"AppointmentDetails"=>{"patientID"=>"3097", "patientName"=>"SHAFRIN ", "mobileNo"=>"9840112397", "appointmentDate"=>"29/08/2019", "appointmentTime"=>"11:00", "doctorCode"=>"MEE", "doctorName"=>"DR. MEENAKSHI (MEE)", "appointmentSource"=>"NURTUREY", "locationName"=>"SMRTHI", "addressLine1"=>"92, Dr Ranga Road,", "addressLine2"=>"", "addressLine3"=>"Alwarpet", "city"=>"CHENNAI", "country"=>"TAMILNADU", "pincode"=>"600018"}, "Api_ResponseCode"=>"APPOINTMENT SUCCESS", "Api_ResponseDesc"=>"Appointment booked with Dr.J.MEENAKSHI on 29/08/2019 Time:11:00AM for SHAFRIN  at SMRTHI,Alwarpet. "}
       data = response["AppointmentDetails"]
       {id:nil,status:200,doctor_specialization:appointment_details[:doctor_specialization], doctor_code:data["doctorCode"],doctor_name:data["doctorName"],doctor_clinic_id:appointment_details[:doctor_clinic_id],message:response["Api_ResponseDesc"]}
      else

      {message:response["Api_ResponseDesc"],status:100}
      # {"Api_ResponseCode"=>"APPOINTMENT SUCCESS", "Api_ResponseDesc"=>"Appointment booked with Dr.S.SUBRAMANIAN on 18/07/2019 Time:7:40PM for SHSAFRIN at SMRTHI,Alwarpet. "}
      end
    rescue Exception=>e 
      Rails.logger.info "Error inside book_appointment-- #{e.message}"
      puts "Error inside book_appointment-- #{e.message}"
      if e.message == "Invalid Auth Key"
         Clinic::Smrthi::BookAppointment.get_auth_token(member_id,options)
        retry  
      end
      {error:e.message,status:100}
    end
  end

  def self.update_appointment(organization_uid,member_id,appointment_details,options={})
    {id:Random.rand(99999999999),status:200,doctor_specialization:appointment_details[:doctor_specialization], doctor_code:appointment_details[:doctor_code],doctor_name:appointment_details[:doctor_name],doctor_clinic_id:appointment_details[:doctor_clinic_id]}
  end
end