class Clinic::Smrthi::Prescribing

  def self.get_medication_courses(organization_uid,member_id,options={})
    return {status:200,medication_courses:[]}
  end

  def self.request_prescription(medication_course_ids,organization_uid,member_id,options={})
    return {status:200}
  end

  def self.get_prescription_requests(from_date, organization_uid,member_id,options={})
    return {requested_prescription_list: [] , status:200}

  end


  def self.cancel_prescription(request_id,organization_uid,member_id,options={})
    return {status:200}
  end

end