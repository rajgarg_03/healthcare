class Clinic::Smrthi::Message
  def self.get_messages(organization_uid,member_id,options={})
    return {status:200,message_list:[]}
  end

  def self.get_message_detail(message_id,organization_uid,member_id,options={})
    return {message_detail:{}, status:200}
  end

   # Emis::Message.delete_message(message_id, organization_uid,member_id,options)
  def self.delete_message(message_id,organization_uid,member_id,options={})
    return {:message=>"Message deleted successfully", status:200}
  end
 
  def self.update_message_read_status(message_id,message_read_state,member_id,options={})
    return {:message=>"", status:200}
  end
  
  def self.send_message(subject,msg,organization_uid,member_id,options={})
    return {:message=>"Message sent successfully", status:200}
  end
end