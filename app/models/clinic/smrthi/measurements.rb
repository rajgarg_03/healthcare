class Clinic::Smrthi::Measurements
#   Get Patient Measurements
#   Description – This method can be used to get the patient details for the given user. Patient details
#                 will be given as json string. This will also have the measurements for each visit.

  def self.get_patient_measurements(organization_uid,member_id,options={})
    retry_count = 0
    begin
      current_user = options[:current_user]
      api_version = options[:api_version]
      clinic_link_detail = Clinic::LinkDetail.where(member_id:member_id).last
      user_info = clinic_link_detail.user_info || {} rescue {}
      auth_code = ::Clinic::UserClinicSession.where(member_id:member_id).last.session_key rescue nil
      last_sync_date = ::Clinic::Smrthi::ApiRequest.last_sync_date(member_id,current_user,api_version,options)

      action_url = 'Patients/GetPatientMeasurements'
      message = {
        :userDetails =>{
          :userAuthToken=>auth_code,
          :lastSyncDate=>(last_sync_date).strftime("%d/%m/%Y"),
        }
      }
      response = Clinic::Smrthi::ApiRequest.make_request(action_url,message,options)
      data = []
      if response.blank?
        status = 200
      elsif (response["Api_ErrorDesc"].present? rescue false)
        raise response["Api_ErrorDesc"]
        status = 100
      else
        status = 200
        # response = [{
        #   "patientIdentity": 2397,
        #   "patientId": "3097",
        #   "visitDate": "19/03/2013",
        #   "height": "63.0",
        #   "weight": "7.04",
        #   "patientPhone": "9444898644",
        #   "patientEmail": "2397@GMAIL.COM",
        #   "patientName": "SHAFRIN ",
        #   "patientDOB": "16/09/2012"
        #   }]
        response.each do |measurement|
          measurement = measurement.with_indifferent_access
          measurement_data = {
            :member_id=>member_id,
            :updated_at=>measurement["visitDate"],
            :height=>measurement["height"],
            :weight=>measurement["weight"],
            :data_source=>"smrthi",
            :added_by=>"system"
          }.with_indifferent_access
          data = {:health=>measurement_data,:length_unit=>"cms",:weight_unit=>"kgs"}.with_indifferent_access
          Health.create_measurement_record(current_user,data)
        end
      end
    rescue Exception=>e
      Rails.logger.info "Error inside get_patient_measurements-- #{e.message}"
      puts "Error inside get_patient_measurements-- #{e.message}"
      if e.message == "Invalid user auth token"
         Clinic::Smrthi::BookAppointment.get_auth_token(member_id,options)
         retry_count = retry_count + 1
        retry  if retry_count < 3
      end
    end
    data
  end
  
end
 