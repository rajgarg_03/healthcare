class Clinic::Smrthi::Documents
  
  # Emis::Prescribing.user_nominated_pharmacy(member_id,options)
  def self.list_document(member_id,options={})
    begin
      return {documents: [], status:200}
    rescue Exception=>e 
      return {message:e.message, status:100}
    end
  end

  def self.get_document_content(member_id,document_id,options={})
    begin
      return {document_data: nil, status:200}
    rescue Exception=>e 
      return {message:e.message, status:100}
    end
  end


end