class Clinic::Smrthi::Pharmacy
  
  # Clinic::Smrthi::Prescribing.user_nominated_pharmacy(member_id,options)
  def self.user_nominated_pharmacy(member_id,options={})
    begin
      return {pharmacies: {}, status:200}
    rescue Exception=>e 
      return {message:e.message, status:100}
    end
  end


  # Clinic::Smrthi::Prescribing.user_nominated_pharmacy(member_id,options)
  def self.list_pharmacies(member_id,options={})
    begin
      return {pharmacies: [], status:200}
    rescue Exception=>e 
      return {message:e.message, status:100}
    end
  end



  # Clinic::Smrthi::Prescribing.update_pharmacy_nomination(member_id,options)
  def self.update_pharmacy_nomination(member_id,options={})
    begin
      return {message:'Pharmacy nomaination updated successfully' , status:200}
    rescue Exception=>e 
      return {message:response["Message"], status:100}
    end
  end


  # Clinic::Smrthi::Prescribing.delete_pharmacy_nomination(member_id,options)
  def self.delete_pharmacy_nomination(member_id,options={})
    begin
      return {message: 'Pharmacy nomaination deleted successfully', status:200}
    rescue Exception=>e 
      return {error:response["Message"], status:100}
    end
  end

end