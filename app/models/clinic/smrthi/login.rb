class Clinic::Smrthi::Login
  # user_details = {:user_name=>"jdjfs",:user_password=>"sads"}
  # user_details = {:login_id=>"2397", :password=>"6dFKQWWA+2rZzDm/0pwPFg=="}
  # user_details = {:login_id=>"3714FIRSTNAME", :password=>"testnut3714"}
  
  def self.authenticateUser(user_details,options={})
    begin
      header = {'Content-Type'=> 'text/json'}
      user_auth_detail = {:userAuthDetails=> {
        :loginId=>(user_details[:login_id]|| user_details[:user_id]),
        :Password=> (user_details[:user_password]|| user_details[:password]),
         :apiKey=>APP_CONFIG["smrthi_access_key"]
        }
      }
      uri = URI.parse("#{APP_CONFIG["smrthi_access_url"]}/Login/AuthenticateUser")
      http = Net::HTTP.new(uri.host, uri.port)
      if uri.scheme == 'https'
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_PEER
      end
      request = Net::HTTP::Post.new(uri.request_uri, header)
      request.body = user_auth_detail.to_json
      response = http.request(request)
      # {"PatientDetails"=>{"patientIdentity"=>0, "patientId"=>"3097", "patientPhone"=>"9444898644", "patientEmail"=>nil, "patientName"=>"SHAFRIN ", "patientDOB"=>"16/09/2012"}, "userName"=>"2397FIRSTNAME 2397LASTNAME", "userPhone"=>"9840112397", "userEmail"=>"2397@gmail.com", "userAuthToken"=>"qki5UnrqhNu#e7n9VDn#Nfo2qhQ#xrVYo@$LX&EQQjEpdQZY8jMvvNxSeF9zFEB9e,yR7F^-jQA6-zBvERvf33"}
      response_data = JSON.parse(response.body)
      Rails.logger.info ".............................."
      Rails.logger.info response_data.inspect
      Rails.logger.info ".............................."
      response_data = response_data.with_indifferent_access
      if response_data["PatientDetails"].present?
        response_data["status"] = 200
      else
        response_data["status"] = 100
        response_data["message"] = response_data["Api_ErrorDesc"]
      end
    rescue Exception=>e 
      response_data = {"status"=>100,"message"=> e.message}
    end
    response_data.with_indifferent_access
  end

  def self.delink_with_clinic(member_id,options={})
  end

  def self.available_services(member_id,options={})
    {appointment_booking: true, repeat_prescription:false, view_medical_records: false,manage_messages:false}
  end

  def self.get_patient_uid(response)
    response["PatientDetails"]["patientId"]
  end

  def self.authentication_status(response)
    patient_uid = Clinic::Smrthi::Login.get_patient_uid(response)
    status = 'unauthenticated'
    if response["userAuthToken"].present?
      patient_with_patient_uid = Clinic::LinkDetail.where(patient_uid:patient_uid).last
      if patient_with_patient_uid.present?
        status = "multiple_account_exist"
      else
        status = "authenticated"
      end
    end
    status
  end

  # user_details = {:login_id=>"3714FIRSTNAME", :password=>"testnut3714"}
  def self.getPatienDetail(current_user,member_id,user_details,options={})
    begin
      response_result = Clinic::Smrthi::Login.authenticateUser(user_details,options)
      # response_result = {"PatientDetails"=>{"patientIdentity"=>0, "patientId"=>"3097", "patientPhone"=>"9444898644", "patientEmail"=>nil, "patientName"=>"SHAFRIN ", "patientDOB"=>"16/09/2012"}, "userName"=>"2397FIRSTNAME 2397LASTNAME", "userPhone"=>"9840112397", "userEmail"=>"2397@gmail.com", "userAuthToken"=>"qki5UnrqhNu#e7n9VDn#Nfo2qhQ#xrVYo@$LX&EQQjEpdQZY8jMvvNxSeF9zFEB9e,yR7F^-jQA6-zBvERvf33"}
      user_detail = response_result["PatientDetails"]
      data = {
        :first_name=>response_result["userName"],
        :last_name=> nil,
        :patient_id => (user_detail["patientId"]),
        :patient_identifier => (user_detail["patientId"]),
        :date_of_birth=> user_detail["patientDOB"].to_date,
        :mobile_number => response_result["userPhone"],
        :email => response_result["userEmail"],
        :address=>nil,
        :gender=> nil
      }
    rescue Exception=>e 
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside smrithi  getPatienDetail")
      data = {}
    end
    data
  end

end