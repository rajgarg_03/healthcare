class Clinic::Smrthi::ApiRequest
  ApiKey =  APP_CONFIG["smrthi_access_key"]
  def self.make_request(action_url,message,options={})
    begin
      Rails.logger.info ".............................."
      Rails.logger.info message.inspect
      Rails.logger.info ".............................."
      header = {'Content-Type'=> 'text/json'}
      uri = URI.parse("#{APP_CONFIG["smrthi_access_url"]}/#{action_url}")
      http = Net::HTTP.new(uri.host, uri.port)
      if uri.scheme == 'https'
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_PEER
      end
      request = Net::HTTP::Post.new(uri.request_uri, header)
      request.body = message.to_json
      response = http.request(request)
      response_data = JSON.parse(response.body)
    rescue Exception=>e 
      Rails.logger.info "Error inside make_request-- #{e.message}"
      puts "Error inside make_request-- #{e.message}"
      
      response_data = {}
    end
    Rails.logger.info ".............................."
    Rails.logger.info response_data.inspect
    Rails.logger.info ".............................."
    raise response_data["Api_ErrorDesc"] if (response_data["Api_ErrorDesc"] == "Invalid Auth Key" rescue false)
  response_data
  end

  def self.get_auth_token(member_id,options={})
    clinic_link_detail = Clinic::LinkDetail.where(member_id:member_id).last.link_info
    user_details = {:login_id=>clinic_link_detail["login_id"], :password=>clinic_link_detail["password"]}
    organization_uid = clinic_link_detail["organization_uid"]#="WKI72JH"
     response =  Clinic::Smrthi::Login.authenticateUser(user_details,options)
    session_key = response["userAuthToken"]
    options[:parent_member_id] = clinic_link_detail.parent_member_id
    Clinic::LinkDetail.save_user_session_detail(member_id,session_key,options)
    # Clinic::LinkDetail.save_record(member_id,clinic_link_detail,organization_uid,options)
 
  end


  def self.last_sync_date(member_id,current_user,api_version=5,options={})
    begin
      last_sync_date = Health.where(member_id:member_id,data_source:"smrthi").order_by("created_at desc").first.created_at
    rescue Exception=>e 
      last_sync_date = Date.today - 5.year
    end
    last_sync_date
  end


end