class Clinic::UserClinicSession
   include Mongoid::Document  
  include Mongoid::Timestamps
  # User from organization linked
  field :member_id,                :type => String 
  field :parent_member_id,         :type => String  # session associated with parent member id eg EMIS/TPP proxy
  field :session_key,              :type => String  # Appliation session id
  field :session_id,               :type => String  # user session id
  field :sequence_number,          :type => String  
  field :message_id,               :type => String  
  field :link_token,               :type => String  # emis user token for v2
end