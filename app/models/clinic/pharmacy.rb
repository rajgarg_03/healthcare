class Clinic::Pharmacy
include Mongoid::Document  
  include Mongoid::Timestamps
  #options = {current_user=>}
  def self.list_pharmacies(current_user,member,options={})
    member_id = member.id
    clinic_class = ::Clinic::ClinicDetail.get_clinic_linkable_class(current_user,member,"Pharmacy",options)
    clinic_class.list_pharmacies(member_id,options)
  end


  def self.user_nominated_pharmacy(current_user,member,options={})
    member_id = member.id
    clinic_class = ::Clinic::ClinicDetail.get_clinic_linkable_class(current_user,member,"Pharmacy",options)
    clinic_class.user_nominated_pharmacy(member_id,options)
  end


  def self.update_pharmacy_nomination(current_user,member,options={})
    member_id = member.id
    clinic_class = ::Clinic::ClinicDetail.get_clinic_linkable_class(current_user,member,"Pharmacy",options)
    clinic_class.update_pharmacy_nomination(member_id,options)
  end


  def self.delete_pharmacy_nomination(current_user,member,options={})
    member_id = member.id
    clinic_class = ::Clinic::ClinicDetail.get_clinic_linkable_class(current_user,member,"Pharmacy",options)
    clinic_class.delete_pharmacy_nomination(member_id,options)
  end

  
    
   

end