# UAE clinic/ organisation 
class Clinic::Seha::Organization
  include Mongoid::Document  
  include Mongoid::Timestamps
  
  field :name,                     :type => String 
  field :phone_no,                 :type => String # active/coming_soon
  field :email,                    :type => String # 
  field :services,                 :type => Array
  field :address,                  :type => String 
  field :postcode,                 :type => String
  field :longitude,                :type => String
  field :latitude,                 :type => String
  field :clinic_type,              :type => String, default: "seha"
  field :clinic_link_status,       :type => String, default: "active" # active/coming_soon
  field :organization_uid,         :type => String 

  def self.get_clinic_list(current_user,member_id,options={})
    if options[:clinic_with_drive_through]
      Clinic::Seha::Organization.where(:services=>"Drive Through").entries
    else
      Clinic::Seha::Organization.all.entries
    end
  end

end