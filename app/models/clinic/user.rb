class ::Clinic::User
  include Mongoid::Document  
  include Mongoid::Timestamps
  # User from organization linked
  field :member_id,                :type => String 
  field :parent_member_id,         :type => String  # To support proxy linking
  field :ods_code,                 :type => String  
  field :organization_uid,         :type => String  
  field :setup_by,                 :type => String  # system/user
  field :access_level,             :type => Integer #clinic services access level  1,2,3 (1=> full, 2 => partial, 3=> none)
  field :link_via_nhs_login,       :type => Boolean, :default => false     #NHS specific -clinic services access level  1,2,3 (1=> full, 2 => partial, 3=> none)
 
  index({ods_code:1})
  index({member_id:1})
  index({parent_member_id:1})
  index({organization_uid:1})

  # 1 register with pin
  # 2 register without pin

  def self.get_clinic_user(member_id,current_user,options={})
    ::Clinic::User.where(member_id:member_id,parent_member_id:current_user.id).last
  end
  
  def self.get_access_level(current_user,member_id,options={})
    user_clinic = ::Clinic::User.get_clinic_user(member_id,current_user,options)
    access_reason = nil
    member = Member.find(member_id)
    member_id = member.id
    member_clinic_link_state = member.get_clinic_link_state(current_user,options)
    # No access to elder member
    if !member.is_child? && current_user.id != member.id
      is_full_access = false
      access_level = 3
      action_identifier = 'NoAccess'
      msg = ::Message::Clinic[:no_access]# "Access not available"
      access_reason = ::Message::Clinic[:no_clinic_setup_access_reason]#"You do not have access to this account. Please get in touch with GP to gain access"
      return [is_full_access,access_level,msg,action_identifier,access_reason]
    end

    member_clinic_link_detail = ::Clinic::LinkDetail.get_clinic_link_detail(member_id,current_user,options)
    clinic_is_setup = member_clinic_link_state >= 3 

    current_user_clinic_link_detail = ::Clinic::LinkDetail.get_clinic_link_detail(current_user.id,current_user,options)
    clinic_detail = member.get_clinic_detail(current_user,options)
    
    if member_clinic_link_state == 0 # no clinic setup/ user not linked with child
      service_access_level = -1
      is_full_access = false
      access_level =  -1
      action_identifier = 'NoGpLinked'
      if member_id == current_user.id
        msg = ::Message::Clinic[:gp_not_linked] #"GP not linked yet"
      else
        msg = ::Message::Clinic[:gp_setup_incomplete]   #'Link to GP Practice now'
      end
      access_reason =::Message::Clinic[:gp_not_linked_access_reason] #'You do not have access to this account. Please setup your GP account'
    elsif member_clinic_link_state < 3 # clinic setup but not linked
      service_access_level = 3
      is_full_access = false
      access_level = 3
      action_identifier = 'GpSetupIncomplated'
      msg =::Message::Clinic[:gp_setup_incomplete]   #"GP setup is incomplete"
      access_reason =::Message::Clinic[:gp_setup_incomplete_access_reason] #'You do not have access to this account. Please complete GP linking'
    elsif user_clinic.present?
      service_access_level = (user_clinic.access_level || 3) rescue 3
    end 
    if clinic_is_setup
      user_access_level,two_factor_auth_required = ::Clinic::User.accessible_to_user(member_id,current_user,options)
      if service_access_level == 1 && user_access_level == 1 # full access
        is_full_access = true
        access_level = 1
        action_identifier = 'FullAccess'
        msg = ::Message::Clinic[:full_access] #"Full access to online GP"
      elsif  service_access_level == 3 || user_access_level == 3 # no access
        is_full_access = false
        access_level = 3
        action_identifier = 'NoAccess'
        msg = ::Message::Clinic[:no_access] #"Access not available"
        # if current_user.clinic_account_status == "Blocked"
        #   access_reason =::Message::Clinic[:no_clinic_access_blocked_reason]#'You do not have access to this account. Please get in touch with GP to gain access'
        # else
          access_reason = ::Message::Clinic[:no_clinic_setup_access_reason]#'You do not have access to this account. Please get in touch with GP to gain access'
        # end
      elsif service_access_level == 2 || user_access_level == 2 # limited acces
        is_full_access = false
        access_level = 2
        
        if current_user.clinic_link_state < 3
          action_identifier = 'LimitedAccessUserNotLinked'
          access_reason = ::Message::Clinic[:limited_access_reason1]#   "Your account details are required to gain full access to your child's health records"
        elsif service_access_level == 2
          action_identifier = 'LimitedAccessWithoutPinDocument'
          if member_id != current_user.id
            #child member
            access_reason =  ::Message::Clinic[:limited_access_reason2] %{:member_name=>member.first_name} # "Please enter #{member.first_name} account details provided by the GP. If you do not have details, please contact your GP"
          else
            access_reason =  ::Message::Clinic[:limited_access_reason5] # "Please enter your account linkage details provided by GP for full access to online services. Please contact your GP for to get these details"
          end 
        elsif user_access_level == 2
          action_identifier = 'LimitedAccessWithoutProxySelected'
          access_reason = ::Message::Clinic[:limited_access_reason3] # 'Child is not linked with your account. Please get in touch with GP to gain access'
        else
          action_identifier = nil
          access_reason = ::Message::Clinic[:limited_access_reason1] # "Your account details are required to gain full access to your child's health records"
        end
          
        msg = ::Message::Clinic[:limited_access]# "Limited access to online GP"
      else
        is_full_access = false
        access_level = 3
        action_identifier = 'NoAccess'
        msg = ::Message::Clinic[:no_access]
        access_reason = ::Message::Clinic[:no_clinic_setup_access_reason] # "You do not have access to this account. Please get in touch with GP to gain access"
      end
    end

    if  access_level < 3 && clinic_is_setup
      if two_factor_auth_required  && member_id != current_user.id
        is_full_access = false
        access_level = 2
        action_identifier = 'AccountVerification'
        msg = ::Message::Clinic[:account_verification_pending]# "Account verification is pending"
        access_reason = ::Message::Clinic[:account_verification_pending_access_reason1] %{:member_name=>member.first_name} # "You have not verified #{member.first_name}'s account yet. Verify #{member.first_name}'s account to gain full access"
      elsif member_id == current_user.id && current_user_clinic_link_detail.present? && current_user_clinic_link_detail.two_factor_auth_required?
        action_identifier = 'AccountVerification'
        is_full_access = false
        access_level = 2
        msg =::Message::Clinic[:account_verification_pending]# "Account verification is pending"
        access_reason =::Message::Clinic[:account_verification_pending_access_reason2] # "Account verification is pending. Please verify your account"
      end
    end
    user_country = Member.member_country_code(current_user.user.country_code)
    if user_country == "ae"
      msg = msg.gsub("GP", "clinic").gsub("Practice ","")
    end
    # access_level = [3=>no_access,2=>limited_access, 1=>full_access]
    [is_full_access,access_level, msg,action_identifier,access_reason]
  end


  def self.accessible_to_user(member_id,current_user,options={})
    member = Member.find(member_id)
    member_id = member.id
    clinic_detail = member.get_clinic_detail(current_user,options)
    return [3,false] if  (member.ods_code.blank? &&  member.organization_uid.blank?) || clinic_detail.blank? # no access
    return [1,false] if clinic_detail.clinic_type.downcase == "smrthi" # FUll access, no auth required
    
    clinic_link_detail = ::Clinic::LinkDetail.get_clinic_link_detail(member_id,current_user,options)
    two_factor_auth_required = clinic_link_detail.two_factor_auth_required?# ? 0 : 1
    access_level = (clinic_link_detail.accessible_to_members || {})[current_user.id.to_s].to_i
    return [1,two_factor_auth_required] if member_id == current_user.id
    return [3,two_factor_auth_required] if (member_id != current_user.id && !member.is_child?)
    # [access_level,clinic_link_detail_otp_status]
     
    [access_level,two_factor_auth_required]
  end

  #options = {:current_user=>current_user,:clinic_detail=>{}}
  def self.save_record(member_id,organization_uid,clinic_link_state,setup_by="user",options={})
    GpsocLogger.info  "..................Clinic User updating ..................................................."
    member = Member.find(member_id)
    current_user = options[:current_user]
    clinic_detail =  member.get_clinic_detail(current_user,options)
    options[:access_level] =  options[:access_level] || 3

    if clinic_detail.blank?
      if options[:clinic_detail].present?
        # options[:clinic_detail] = {:name=>,:uid=>,:address=>,:postcode=>}
        organization_obj = ::Clinic::Organization.new(options[:clinic_detail])
      else
        options[:organization_uid] = organization_uid
        organization_obj = ::Clinic::Organization.get_clinic(member,options)
      end
      ::Clinic::ClinicDetail.save_clinic_detail(organization_obj,options)
    end

    clinic_detail =  member.get_clinic_detail(current_user,options)

    clinic_user = ::Clinic::User.get_clinic_user(member_id,current_user,options)
    GpsocLogger.info  "..................Clinic User #{clinic_user.inspect} ..................................................."
    if clinic_user.blank?
      GpsocLogger.info  "..................cleaning up accont info ..................................................."
      # ::Clinic::User.where(member_id:member_id,parent_member_id:current_user.id).delete_all
      # ::Clinic::LinkDetail.where(member_id:member_id,parent_member_id: current_user.id).delete_all
      clinic_user = ::Clinic::User.new(ods_code:clinic_detail.ods_code,parent_member_id:current_user.id, member_id:member_id,organization_uid:organization_uid,setup_by:setup_by)
      clinic_user.save
    else
      clinic_user.update_attributes({ods_code:clinic_detail.ods_code, organization_uid:organization_uid})
    end

    if options[:access_level].present?
      clinic_user.update_attribute(:access_level,options[:access_level])
    end
    GpsocLogger.info  "..................Access for Clinic set ......"#{clinic_user.inspect}............................................"

    member =  Member.find(member_id)
    member.organization_uid = organization_uid
    member.ods_code = clinic_detail.ods_code
    member.clinic_link_state = clinic_link_state
    member.save(validate:false)
    GpsocLogger.info  "..................Member link state updated ......"#{member.inspect}............................................"

  end

  def self.delete_record(member_id,organization_uid,options={})
    begin
      Rails.logger.info "Delink clinic for member id ..........................#{member_id}"
      member = Member.where(id:member_id).last
      if member.present?
        current_user = options[:current_user]
        if current_user.id != member.id && ::Clinic::LinkDetail.where(member_id:member_id).count < 2
          member.clinic_link_state = 2
        elsif current_user.id == member.id
          # Delink proxy accounts 
          proxy_member_linked = ::Clinic::LinkDetail.where(parent_member_id:member_id).map(&:member_id) - [member_id.to_s]
          proxy_member_linked.each do |proxy_member_id|
            ::Clinic::User.delete_record(proxy_member_id,organization_uid,options)
          end
          member.clinic_link_state = 2
          ::Clinic::ClinicDetail.delink_with_clinic(member,current_user,options)
        end
        member.save(validate:false)
        clinic_user = ::Clinic::User.get_clinic_user(member_id,current_user,options)
        clinic_user.access_level = 3
        clinic_user.save
        # Keep IM detail for testing if setting is disabled for delinking
        im1_delinking_enable_status = System::Settings.im1DelinkingEnabled?(current_user,options)
        if current_user.id == member.id && !im1_delinking_enable_status
          ::Clinic::DelinkedLinkDetail.save_record(current_user,options)
        end
        ::Clinic::LinkDetail.where(member_id:member_id,parent_member_id:current_user.id).delete_all
        ::Clinic::UserClinicSession.where(member_id:member_id,parent_member_id:current_user.id).delete_all
      end
    rescue Exception=>e 
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside clinic/user delink account for member #{member_id}")
    end
  end


  def get_services_for_user(options={})
    clinic_user = self
    current_user = options[:current_user]
    member_id = clinic_user.member_id
    member = Member.find(member_id)
    if options[:is_full_access].present?
      is_full_access = options[:is_full_access]
    else
      is_full_access,access_level,msg,action_identifier,access_reason = ::Clinic::User.get_access_level(current_user,member_id,options)
      options[:is_full_access] = is_full_access
    end
    ::Clinic::ClinicDetail.clinic_services_available(member,current_user,options)
  end

  def self.get_user_detail(current_user,member_id,options={})
    begin
      options[:current_user] = current_user
      member = Member.find(member_id)
      clinic_detail = ::Clinic::ClinicDetail.where(organization_uid:member.organization_uid).last
      clinic_link_detail = ::Clinic::LinkDetail.get_clinic_link_detail(member_id,current_user,options)
      user_details = clinic_link_detail.link_info rescue {}
      if  clinic_detail.is_smrthi?(options)
          user_data = ::Clinic::Smrthi::Login.getPatienDetail(current_user,member_id, user_details,options)
      elsif clinic_detail.is_orion?(options)
          user_data = clinic_link_detail.user_info rescue {}
          # ::Clinic::Orion::Login.getPatienDetail(current_user,member_id, user_details,options)
      elsif clinic_detail.is_emis?(options)
          user_data,status = ::Emis::Login.getPatientInfo(current_user,member_id, user_details,options)
      elsif clinic_detail.is_tpp?(options)
          user_data,status = ::Clinic::Tpp::Login.getPatientInfo(current_user,member_id, user_details,options)
     
      else
        user_data = nil
      end
      if user_data.present? && status == 200
        clinic_link_detail.phone_number = user_data[:mobile_number]
        clinic_link_detail.email = user_data[:email]
        clinic_link_detail.user_info["email"] = user_data[:email]
        clinic_link_detail.save
      end
    rescue Exception=>e 
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside clinic/user get_user_detail for member #{member_id}")
      user_data = nil 
    end
    user_data
  end

end