class Clinic::Prescribing
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :member_id,                :type => String 
  field :medication_course_ids,     :type => Array   # prescription medication ids
  field :event_type,               :type => String   # presciption_ordered/presciption_cancelled
  field :data_source,              :type => String   # emis/tpp/nurturey
  field :clinic_type,              :type => String   # emis/tpp
  field :request_guid,             :type => String   # emis/tpp

  index({member_id:1})
  index({request_guid:1})

  #options = {current_user=>}
  def self.get_medication_courses(current_user,member,options={})
    organization_uid = member.organization_uid
    member_id = member.id
    clinic_class = ::Clinic::Prescribing.get_clinic_class(member,options)
    data = clinic_class.get_medication_courses(organization_uid,member_id,options)
    
    data
  end

  def self.request_prescription(current_user,member,medication_course_ids,options={})
    organization_uid = member.organization_uid
    member_id = member.id
    clinic_class = ::Clinic::Prescribing.get_clinic_class(member,options)
    response = clinic_class.request_prescription(medication_course_ids,organization_uid,member_id,options)
    if response[:status] == 200
      request_guid = response[:request_guid]
      prescribing = ::Clinic::Prescribing.new(request_guid:request_guid, event_type:"presciption_ordered",member_id:member_id,medication_course_ids:medication_course_ids, clinic_type:response[:clinic_type],:data_source=>"nurturey")
      if ::System::Settings.im1ClinicRecordSavingEnabled?(current_user,"prescribing",options)
        prescribing.save
      end
    end
    response
  end
   
 
  def self.get_prescription_requests(current_user,member,from_date,options={})
    organization_uid = member.organization_uid
    member_id = member.id
    clinic_class = ::Clinic::Prescribing.get_clinic_class(member,options)
    data = clinic_class.get_prescription_requests(from_date, organization_uid,member_id,options)
    if data[:status] == 200
      if ::System::Settings.im1ClinicRecordSavingEnabled?(current_user,"prescribing",options)
        data[:requested_prescription_list].each do |prescription|
          request_guid = prescription["RequestGuid"]
          medication_course_ids = prescription[:medication_course_ids]
          prescription_exist = Clinic::Prescribing.where(:request_guid=>request_guid).last
          if prescription_exist.blank?
            prescribing = ::Clinic::Prescribing.new(request_guid:request_guid, event_type:"presciption_ordered",member_id:member_id,medication_course_ids:medication_course_ids, clinic_type:data[:clinic_type],:data_source=>data[:clinic_type])
            prescribing.save
          end
        end
      end
    end
    data
  end

  def self.cancel_prescription(current_user,member,request_id,options={})
    organization_uid = member.organization_uid
    member_id = member.id
    clinic_class = ::Clinic::Prescribing.get_clinic_class(member,options)
    response = clinic_class.cancel_prescription(request_id,organization_uid,member_id,options)
    if response[:status] == 200
      if ::System::Settings.im1ClinicRecordSavingEnabled?(current_user,"prescribing",options)
        prescribing = ::Clinic::Prescribing.where(request_guid:request_id).last || ::Clinic::Prescribing.new(request_guid:request_id,event_type:"presciption_cancelled",member_id:member_id,medication_course_ids:medication_course_ids, clinic_type:response[:clinic_type],:data_source=>"nurturey")
        prescribing.event_type = "presciption_cancelled"
        prescribing.save
      end
    end
    response
  end

  def self.get_clinic_class(member,options={})
    clinic_detail = Clinic::Organization.get_clinic(member,options)
    clinic_class = ::Clinic::ClinicDetail.get_clinic_linkable_class(options[:current_user],member=nil,"Prescribing",options.merge({:clinic_detail=>clinic_detail}))
    return clinic_class
  end

end