class Clinic::Tpp::Measurements
#   Get Patient Measurements
#   Description – This method can be used to get the patient details for the given user. Patient details
#                 will be given as json string. This will also have the measurements for each visit.
  
  def self.get_patient_measurements(organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      request_data = {}
      
      clinic_link_detail =  ::Clinic::LinkDetail.where(member_id:member_id).last
      user_link_detail = clinic_link_detail.link_info.with_indifferent_access
       # <ListSlots apiVersion="1" unitId="C84010" patientId="71df200000000000" onlineUserId="75ee400000000000" startDate="2014-11-25T00:00:00.0Z" numberOfDays="15" uuid="88888888-8888-8888-8888-888888888888"></ListSlots>
      request_body =  ::Clinic::Tpp::Client.request_body_data(current_user,member_id,options)
       
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.RequestPatientRecord(request_body) 
      end
 

      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options = {}
      suid_required = true
      request_options.merge!({:request_type=>"Post", :request_uri=> "RequestPatientRecord", :request_body=>request_body})
      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
      if status == 200

        observation_data = []
        response["RequestPatientRecordReply"]["Event"].each do |a| 
          items =   [a["Item"]].flatten.reverse
          items.each do |item|
            term = item["details"]
            if term.include?("O/E - weight") 
              value =  term[/\)(.+)\(/, 1]
              term = "weight"
              unit = "kgs"
            elsif term.include?("O/E - height")
              value = term[/.*\((.+)\)/, 1]
              term = "height"
              unit = "inches"
            end
            if term == "height" || term == "weight"
                observation_data << {"EffectiveDate"=> a["date"].to_date.to_s,"Term"=>term, "NumericValue"=>value.to_f,"NumericUnits"=>unit}
               
            end
          end
        end

        observation_data = observation_data.flatten.compact.select{|observation| observation["Term"].present? }
        measurement_records = ::Clinic::Tpp::Measurements.get_formatted_measurement_data(member_id, observation_data,options)
        measurement_records = measurement_records.sort.to_h
        measurement_ids = Health.where(member_id:member_id,:data_source=>"tpp").pluck(:id)
        Timeline.where(timelined_type: "Health", timelined_id: measurement_ids).delete_all
        Health.where(member_id:member_id,:data_source=>"tpp").delete_all
        measurement_records.each do |updated_at,data|
          begin
            data[:health].delete(nil)
            data.delete(nil)
            data = data.with_indifferent_access
            data[:health] = data[:health].with_indifferent_access
            if data[:health].present?
              Health.create_measurement_record(current_user,data)
            end
          rescue Exception =>e 
            Rails.logger.info e.message
          end
        end
        @retry_count = 0
      end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
    end
  end

   def self.get_formatted_measurement_data(member_id,records,options={})
    measurement_unit = {"height"=>"length_unit","weight"=>"weight_unit"}
    health_data = {}
    # group_by_date = records.group_by{|a| a[:value][:availability_date_time].to_s}
    group_by_date = records.group_by{|a| a["EffectiveDate"].to_s}

    group_by_date.each do |date,health_records|
      measurement_data = {:health=>{}}
      health_records.each do |record|
        data = record
        measurement_parameter = data["Term"]

        record_date = date.to_date.to_s
        
        if measurement_parameter.present?
          metric_unit = measurement_unit[measurement_parameter]
          measurement_data[metric_unit]= (data["NumericUnits"].pluralize rescue nil )
          measurement_data[:health].merge!({:member_id=>member_id,measurement_parameter=> data["NumericValue"],:data_source=>"tpp",:added_by=>"system",:updated_at=>record_date })
        end
      end
      health_data[date] = measurement_data
    end
    health_data
  end



   

   

  def self.get_measurement_parameter(term)
    if term.include?("height")
      parameter = "height"
      value = term[/.*\((.+)\)/, 1]
      unit = "inches"
    elsif term.include?("weight")
      parameter = "weight"
      value =  term[/\)(.+)\(/, 1]
      unit = "kgs"
    end
    [parameter,value,unit]
  end
  def self.get_patient_measurements1(organization_uid,member_id,options={})
    retry_count = 0
    begin
      data = []
      current_user = options[:current_user]
      api_version = options[:api_version]
    rescue Exception=>e
      Rails.logger.info "Error inside get_patient_measurements-- #{e.message}"
      puts "Error inside get_patient_measurements-- #{e.message}"
    end
    data
  end
  
end
 