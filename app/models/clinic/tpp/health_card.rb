class Clinic::Tpp::HealthCard
  def self.get_data(type,current_user,member_id,options={})
    request_body =  ::Clinic::Tpp::Client.request_body_data(current_user,member_id,options)

    if type == "TestResultsView"
      action = "TestResultsView"
      # Tpp allows 2 month interval data
      start_date = Date.today - 59.days
      end_date = Date.today 
      request_body.merge!({startDate:start_date, "endDate": end_date })
      request = Nokogiri::XML::Builder.new do |xml|
        xml.TestResultsView(request_body) 
      end
    elsif type == "RequestPatientRecord"
      action = "RequestPatientRecord"
      request = Nokogiri::XML::Builder.new do |xml|
        xml.RequestPatientRecord(request_body) 
      end
    elsif type == "ViewPatientOverview"
      action = "ViewPatientOverview"
      request = Nokogiri::XML::Builder.new do |xml|
        xml.ViewPatientOverview(request_body) 
      end
    elsif type == "Immunisations"
      action = "RequestChildhoodVaccs"
      request = Nokogiri::XML::Builder.new do |xml|
        xml.RequestChildhoodVaccs(request_body) 
      end
    end
    [action,request]
  end

  # Get Patient Allergy
  # None, Allergies, Consultations, Documents, Immunisations, Medication, Problems, TestResults
  # options = {:item_type=>"ViewPatientOverview/RequestPatientRecord/TestResultsView",:current_user=>current_user}
  # ::Clinic::Tpp::HealthCard.get_patient_health_record(member_id,options)
  def self.get_patient_health_record(member_id,options={})
    begin
      current_user = options[:current_user]
      member = Member.find(member_id)
      # request_data = options[:request_data] || {}
      request_options = {}
      item_type = options[:item_type]
      
      clinic_link_detail =  ::Clinic::LinkDetail.where(member_id:member_id).last
      user_link_detail = clinic_link_detail.link_info.with_indifferent_access
      # <TestResultsView  unitId="X9999" patientId="71df200000000000" onlineUserId="75ee400000000000" startDate="2010-11-20T00:00:00.0Z" endDate="2014-11-30T00:00:00.0Z"></TestResultsView>
      # request_body =  ::Clinic::Tpp::Client.request_body_data(current_user,member_id,options)
       
      action,builder = Clinic::Tpp::HealthCard.get_data(item_type,current_user,member_id,options)
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
       
      suid_required = true
      request_options.merge!({:request_type=>"Post", :request_uri=> action, :request_body=>request_body})
      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
    
      # [{"MedicalRecord"=>{"PatientGuid"=>"5a09eb8f-7e0b-4630-ad48-87e8e05d124f", "Title"=>"Master", "Forenames"=>"Mike", "Surname"=>"Singh", "Sex"=>"Male", "DateOfBirth"=>"2016-09-22T00:00:00", "Allergies"=>[{"ObservationType"=>"Allergy", "Episodicity"=>"First", "NumericValue"=>nil, "NumericOperator"=>nil, "NumericUnits"=>nil, "DisplayValue"=>nil, "TextValue"=>nil, "Range"=>nil, "Abnormal"=>false, "AbnormalReason"=>nil, "AssociatedText"=>nil, "EventGuid"=>"0d41cfe3-fca8-461d-9332-773d36a192e7", "Term"=>"Egg allergy", "AvailabilityDateTime"=>"2020-01-30T06:16:29.357", "EffectiveDate"=>{"DatePart"=>"YearMonthDay", "Value"=>"2020-01-30T00:00:00"}, "CodeId"=>637141000006110, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}], "Consultations"=>nil, "Documents"=>nil, "Immunisations"=>nil, "Medication"=>nil, "Problems"=>nil, "TestResults"=>nil, "Users"=>[{"UserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "Title"=>"Dr", "Forenames"=>"Nurturey", "Surname"=>"Test", "Role"=>"General Medical Practitioner", "Organisation"=>"Nurt services"}]}, "FilterDetails"=>{"ItemFilter"=>"Allergies", "ItemFilterFromDate"=>nil, "ItemFilterToDate"=>nil, "FreeTextFilterFromDate"=>nil}}, 200]
      if status == 200
        observation_data = []
         
        if [ "TestResultsView"].include?(item_type)
          item_data = response["Item"]
        elsif item_type == "RequestPatientRecord"
          # Measurements
          item_data = response["RequestPatientRecordReply"]["Event"] || []
        elsif item_type == "ViewPatientOverview"
          item_data = []
          ["DrugSensitivities","Drugs","PastRepeats","CurrentRepeats","Allergies"].each do |keys_data|
            response[keys_data] = response[keys_data] - [{}] 
            if response[keys_data].present?
              response[keys_data][0]["Item"].flatten.compact.each do |item_info|
                item_data << item_info  
              end
            end
          end
          # ["Allergies"]].flatten.compact#.select{|observation| observation["Term"].present? }
        elsif item_type == "Immunisations"
          # Immunisation
          item_data = response["RequestChildhoodVaccsReply"]["ChildhoodVacc"]
        end
        allergy_records = ::Clinic::Tpp::HealthCard.get_formatted_health_data(member_id,item_data,item_type,options)
        if allergy_records.blank?
          allergy_records = [] 
        end
        message = nil
      elsif status == 403
        status = status
        message = response["Message"] || response["Error"]["technicalMessage"]
      else
        status = 100
        raise (response["Message"] || 'Error inside get_patient_allergies') if @retry_count.to_i < 2
        message = response["Message"]
        allergy_records = []
      end    
    rescue Exception=>e 
      ::Clinic::Tpp::Client.clear_member_session(current_user,member_id,options)
      @retry_count = @retry_count.to_i + 1
      @error = nil 
      retry if @retry_count.to_i <= 2
      status = 100
      message  = "Unable to get #{item_type} data"
      @retry_count = 0
      @error = e.message
    end
    {data:allergy_records,:status=>status,:message=>message,:error=>@error}
  end

  def self.get_formatted_testViewResult(date,records,member_id,options={})
    data = []
    records.each do |item|
      data << {name:item["description"],
       id:item["id"],
       uid: item["id"],
       data_source: "tpp",
       html_content:item['content'],
       type:item["description"],
       updated_at:(item["date"].to_date.to_date4.to_s rescue nil),
       date: (item["date"].to_date.to_s rescue nil)
      }
    end
    data
  end


  def self.get_formatted_requestPatientVaccination(date,records,member_id,options={})
    data = []
    records.each do |item|
      data << {name:(item["vaccContent"].to_s + " " + item["vaccPart"].to_i.ordinalize),
       id:nil,
       consentType: item["consentType"],
       data_source: "tpp",
       type:item["vaccPart"],
       date: (item["dateConsentGiven"].to_date.to_s rescue nil),
       updated_at: ("Due on " + item["dateDue"].to_date.to_date4.to_s rescue nil)
      }
    end
    data
  end

  def self.get_formatted_requestPatientRecord(date,records,member_id,options={})
    data = []
    records.each do |items|
      [items["Item"]].flatten.compact.each do |item|
      data << {name:item["details"],
         id:item["id"],
         type:item["type"],
         data_source: "tpp",
         done_by: items["doneBy"],
         date: (items["date"].to_date.to_s rescue nil),
         updated_at: (items["date"].to_date.to_date4.to_s rescue nil)
        }
      end
    end
    data
  end


   def self.get_formatted_viewPatientOverview(date,records,member_id,options={})
    data = []
    records.each do |item|
      data << {name:item["content"],
       id:item["id"],
       type:item["description"],
       data_source: "tpp",
       date: (item["date"].to_date.to_s rescue nil),
       updated_at: (item["date"].to_date.to_date4.to_s rescue nil)

      }
    end
    data
  end

  def self.get_formatted_health_data(member_id,records,item_type,options={})
    data = {}
    if records.present?
      if item_type == "Immunisations"
        group_by_date = records.group_by{|a| a["dateConsentGiven"].to_date.to_s}
      else
        group_by_date = records.group_by{|a| a["date"].to_date.to_s}
      end
      group_by_date.each do |date,records|
        if item_type == "TestResultsView"
          record_data = ::Clinic::Tpp::HealthCard.get_formatted_testViewResult(date,records,member_id,options)
        elsif item_type == "RequestPatientRecord"
          record_data = ::Clinic::Tpp::HealthCard.get_formatted_requestPatientRecord(date,records,member_id,options)
        elsif item_type == "Immunisations"
          # Immunisations
          record_data = ::Clinic::Tpp::HealthCard.get_formatted_requestPatientVaccination(date,records,member_id,options)
        elsif item_type =="ViewPatientOverview"
          record_data = ::Clinic::Tpp::HealthCard.get_formatted_viewPatientOverview(date,records,member_id,options)
          
        end
        data[date] = record_data
      end
    end
    data
  end
  
  def self.medication_data_format(date,records,member_id,options={})
    # [{"DrugName"=>"Co-beneldopa 25mg/100mg modified-release capsules", "Dosage"=>"1", "Quantity"=>1.0, "QuantityUnit"=>"capsule", "QuantityRepresentation"=>"1 capsule", "IsMixture"=>false, "Mixture"=>nil, "FirstIssueDate"=>"2020-02-02T12:26:50.957", "LastIssueDate"=>"2020-02-02T12:26:50.957", "PrescriptionType"=>"Repeat", "DrugStatus"=>"Active", "EventGuid"=>"4895ccfb-e02f-4594-8ca7-6046b5d28d13", "Term"=>"Co-beneldopa 25mg/100mg modified-release capsules", "AvailabilityDateTime"=>"2020-02-02T12:26:25.34", "EffectiveDate"=>{"DatePart"=>"YearMonthDay", "Value"=>"2020-02-02T00:00:00"}, "CodeId"=>359341000033113, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}] 
    data = []
    records.each do |record| 
      name = record["Term"].to_s + " \n" + record["QuantityRepresentation"] 
      data << ({:member_id=>member_id,:name=>name, :value=> record["NumericValue"],:data_source=>"tpp",:added_by=>"system",:updated_at=>date.to_date.to_date4 })
    end
    data 
  end

  def self.problem_data_format(date,records,member_id,options={})
    # [{"Status"=>"Active", "Significance"=>"Significant", "ProblemEndDate"=>nil, "Observation"=>{"ObservationType"=>"Allergy", "Episodicity"=>"First", "NumericValue"=>nil, "NumericOperator"=>nil, "NumericUnits"=>nil, "DisplayValue"=>nil, "TextValue"=>nil, "Range"=>nil, "Abnormal"=>false, "AbnormalReason"=>nil, "AssociatedText"=>nil, "EventGuid"=>"3f33081a-809f-45d5-bb11-56e740cfe776", "Term"=>"Cow's milk allergy", "AvailabilityDateTime"=>"2020-01-31T14:23:55.353", "EffectiveDate"=>{"DatePart"=>"YearMonthDay", "Value"=>"2019-01-10T00:00:00"}, "CodeId"=>26953014, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}}] 
    data = []
    records.each do |record| 
      name = record["ObservationType"].to_s + " \n" + record["Term"] 
      data << ({:member_id=>member_id,:name=>name, :value=> record["NumericValue"],:data_source=>"tpp",:added_by=>"system",:updated_at=>date.to_date.to_date4 })
    end
    data 
  end

  def self.documents_data_format(date,records,member_id,options={})
    # data =[{"DocumentGuid"=>"386854ae-b452-460b-912d-4b67aebc953b", "Observation"=>{"ObservationType"=>"Document", "Episodicity"=>"Unknown", "NumericValue"=>nil, "NumericOperator"=>nil, "NumericUnits"=>nil, "DisplayValue"=>nil, "TextValue"=>nil, "Range"=>nil, "Abnormal"=>false, "AbnormalReason"=>nil, "AssociatedText"=>nil, "EventGuid"=>"883a758f-4599-4f3d-a3d7-90d75f4dafe6", "Term"=>"Administration", "AvailabilityDateTime"=>"2020-01-31T14:22:23.55", "EffectiveDate"=>{"DatePart"=>"YearMonthDayTime", "Value"=>"2020-01-31T14:22:08"}, "CodeId"=>2548615018, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}, "Size"=>27126, "PageCount"=>1, "Extension"=>"pdf", "Available"=>true}]  []
    data = []
    records.each do |record| 
      document_guid = record["EventGuid"]
      name = "#{record['Term']} \nDocument-" + document_guid.split("-").last
      last_modified = record["AvailabilityDateTime"]
      data << ({:member_id=>member_id,:name=>name, :value=>document_guid ,:data_source=>"tpp",:added_by=>"system",:updated_at=>date.to_date.to_date4 })
    end
    data 
  end

  def self.test_results_data_format(date,records,member_id,options={})
    # [{[{"ObservationType"=>"Value", "Episodicity"=>"None", "NumericValue"=>45.0, "NumericOperator"=>nil, "NumericUnits"=>"cm", "DisplayValue"=>"45 cm", "TextValue"=>"45", "Range"=>nil, "Abnormal"=>false, "AbnormalReason"=>nil, "AssociatedText"=>nil, "EventGuid"=>"7c8454ec-da98-4f80-89ac-3e88de112523", "Term"=>"O/E - height", "AvailabilityDateTime"=>"2020-01-31T14:17:17.493", "EffectiveDate"=>{"DatePart"=>"YearMonthDay", "Value"=>"2018-01-26T00:00:00"}, "CodeId"=>253669010, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}, {"ObservationType"=>"Value", "Episodicity"=>"None", "NumericValue"=>3.5, "NumericOperator"=>nil, "NumericUnits"=>"kg", "DisplayValue"=>"3.5 kg", "TextValue"=>"3.5", "Range"=>nil, "Abnormal"=>false, "AbnormalReason"=>nil, "AssociatedText"=>nil, "EventGuid"=>"d0867500-f72c-4845-b197-d18cb465fcde", "Term"=>"O/E - weight", "AvailabilityDateTime"=>"2020-01-31T14:18:39.543", "EffectiveDate"=>{"DatePart"=>"YearMonthDay", "Value"=>"2018-01-26T00:00:00"}, "CodeId"=>253677014, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}] "DrugName"=>"Co-beneldopa 25mg/100mg modified-release capsules", "Dosage"=>"1", "Quantity"=>1.0, "QuantityUnit"=>"capsule", "QuantityRepresentation"=>"1 capsule", "IsMixture"=>false, "Mixture"=>nil, "FirstIssueDate"=>"2020-02-02T12:26:50.957", "LastIssueDate"=>"2020-02-02T12:26:50.957", "PrescriptionType"=>"Repeat", "DrugStatus"=>"Active", "EventGuid"=>"4895ccfb-e02f-4594-8ca7-6046b5d28d13", "Term"=>"Co-beneldopa 25mg/100mg modified-release capsules", "AvailabilityDateTime"=>"2020-02-02T12:26:25.34", "EffectiveDate"=>{"DatePart"=>"YearMonthDay", "Value"=>"2020-02-02T00:00:00"}, "CodeId"=>359341000033113, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}] 
    data = []
    records.each do |record| 
      name =  [record['Term'], record['DisplayValue'] ].compact.join(" : ")# "#{record['Term']} : #{record['DisplayValue']}" 
      data << ({:member_id=>member_id,:name=>name, :value=> record["DisplayValue"],:data_source=>"tpp",:added_by=>"system",:updated_at=>date.to_date.to_date4 })
    end
    data 
  end
  
  def self.consultations_data_format(date,records,member_id,options={})
    data = []
    records.each do |record| 
      record["Sections"].each do |section|
        section_name = section["Header"]
        section["Observations"].each do |observation|
          name = section_name + " \n" + observation["Term"].to_s
          data << ({:member_id=>member_id,:name=>name, :value=> record["NumericValue"],:data_source=>"tpp",:added_by=>"system",:updated_at=>date.to_date.to_date4 })
        end
      end
    end
    data 
  end  
end
 