class Clinic::Tpp::Pharmacy
  
  # Clinic::Tpp::Pharmacy.user_nominated_pharmacy(member_id,options)
  def self.user_nominated_pharmacy(member_id,options={})
    response = ::Clinic::Tpp::Pharmacy.list_pharmacies(member_id,options)
    if response[:status] == 200
      pharmacies = response[:pharmacies].select{|pharmacy| pharmacy[:nominated] == true}
      pharmacies = pharmacies.map{|pharmacy| ::Clinic::Tpp::Pharmacy.formated_pharmacy_ttp_data(pharmacy,options)}
      {:pharmacies=>pharmacies,status:200}
    else
      response
    end
  end

  def self.formated_pharmacy_ttp_data(pharmacy,options)
 # pharmacy = {"name"=>"Tylee Andrew Ltd", "distance"=>"0.21", "odsCode"=>"FK853", "Address"=>{"address"=>"123 Main Street, Leeds, LS1 1XX"}
    {
      "Distance": pharmacy["distance"],
      "DistanceUnits": nil,
      "Name": pharmacy["name"],
      "Nominated": (pharmacy["nominated"].to_s.downcase == "y"),
      "NacsCode": pharmacy["odsCode"],
      "Telephone": "string",
      "Address": {
        "HouseNameFlatNumber": pharmacy["Address"]["address"],
        "NumberStreet": nil,
        "Village": nil,
        "Town": nil,
        "County": nil,
        "Postcode": nil
      }
    }

  end
  # Clinic::Tpp::Pharmacy.user_nominated_pharmacy(member_id,options)
  def self.list_pharmacies(member_id,options={})
    begin
      current_user = options[:current_user]
      member = Member.find(member_id)
     # <RequestCommunityPharmacy apiVersion="1" unitId="C86030" patientId="71df200000000000" onlineUserId="75ee400000000000"></RequestCommunityPharmacy>
      request_body =  ::Clinic::Tpp::Client.request_body_data(current_user,member_id,options)
       
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.RequestCommunityPharmacy(request_body) 
      end
      suid_required = true
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options = ({:request_type=>"Post", :request_uri=> "RequestCommunityPharmacy", :request_body=>request_body})
      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
      # response,status =  [{"RequestCommunityPharmacyReply"=>{"patientId"=>"71df200000000000", "onlineUserId"=>"75ee400000000000", "uuid"=>"88888888-8888-8888-88888888888888888", "Pharmacy"=>[{"name"=>"Tylee Andrew Ltd", "distance"=>"0.21", "odsCode"=>"FK853", "Address"=>{"address"=>"123 Main Street, Leeds, LS1 1XX"}}, {"name"=>"Boots", "distance"=>"0.87", "odsCode"=>"FQ500", "nominated"=>"y", "Address"=>{"address"=>"123 Main Street, Leeds, LS1 1XX"}}]}},200]
      if (status != 200 )
        return {message:response["Error"]["technicalMessage"], status:status}
      else
        pharmacies_list = response["RequestCommunityPharmacyReply"]["Pharmacy"]
        pharmacies = pharmacies_list.map{|pharmacy| ::Clinic::Tpp::Pharmacy.formated_pharmacy_ttp_data(pharmacy,options)}
        return {pharmacies: pharmacies, status:200}
        @retry_count = 0
      end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      return {message: ::Message::Tpp[:Pharmacy][:unable_to_list_pharmacy], :error=>e.message ,status:100}
    end
  end



  # ::Clinic::Tpp::Pharmacy.update_pharmacy_nomination(member_id,options)
  # request_data = {:NacsCode=>, :Postcode=>}
  def self.update_pharmacy_nomination(member_id,options={})
    begin
      current_user = options[:current_user]
      member = Member.find(member_id)
      request_data = options[:request_data] || {}
      # <UpdateCommunityPharmacy apiVersion="1" unitId="C84010" patientId="71df200000000000" onlineUserId="75ee400000000000" pharmacyCodeNew="X9999"></UpdateCommunityPharmacy>
      request_body =  ::Clinic::Tpp::Client.request_body_data(current_user,member_id,options)
      request_body.merge!({pharmacyCodeNew:request_data[:NacsCode]})
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.UpdateCommunityPharmacy(request_body) 
      end
      suid_required = true
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options = ({:request_type=>"Post", :request_uri=> "UpdateCommunityPharmacy", :request_body=>request_body})
      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
      # response,status =  [{"UpdateCommunityPharmacyReply"=>{"patientId"=>"71df200000000000", "onlineUserId"=>"75ee400000000000", "uuid"=>"88888888-8888-8888-88888888888888888", "Pharmacy"=>[{"name"=>"Tylee Andrew Ltd", "distance"=>"0.21", "odsCode"=>"FK853", "Address"=>{"address"=>"123 Main Street, Leeds, LS1 1XX"}}, {"name"=>"Boots", "distance"=>"0.87", "odsCode"=>"FQ500", "nominated"=>"y", "Address"=>{"address"=>"123 Main Street, Leeds, LS1 1XX"}}]}},200]
      if (status != 200 )
        return {message:response["Error"]["technicalMessage"], status:100}
      else
        return {:message=>::Message::Tpp[:Pharmacy][:pharmacy_nomination_updated], status:200}
        @retry_count = 0
      end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      return {message: ::Message::Tpp[:Pharmacy][:pharmacy_nomination_not_updated], :error=>e.message ,status:100}
    end
  end


  # ::Clinic::Tpp::Pharmacy.delete_pharmacy_nomination(member_id,options)
  # request_data = {:NacsCode=>}
  def self.delete_pharmacy_nomination(member_id,options={})
    begin
      # current_user = options[:current_user]
      # request_options = {}
      # request_data = options[:request_data] || {}
      # user_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
      # session_id, link_token = ::Emis::Client.get_session_id_and_link_token(current_user,member_id,options)
      # request_body = {'UserPatientLinkToken': link_token}.merge(request_data) 
      # request_header = {'X-API-SessionId'=>session_id}
      # request_options.merge!({:request_type=>"Delete", :request_uri=> "/pharmacies/nomination", :request_body=>request_body,:request_header=>request_header})
      # response,status = ::Emis::Client.make_request(current_user,member_id,request_options,options)
      # if (status != 200 )
        return {message: ::Message::Tpp[:Pharmacy][:pharmacy_nomination_deleted], status:100}
      # else
      #   return {message: 'Pharmacy nomaination deleted successfully', status:200}
      #   @retry_count = 0
      # end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      return {message: ::Message::Tpp[:Pharmacy][:pharmacy_nomination_not_deleted], status:100}
    end
  end
  
   

end