class Clinic::Tpp::Prescribing
  
  # ::Clinic::Tpp::Prescribing.get_acute_medication(member_id,options)
  def self.get_acute_medication(member_id,options={})
    begin

      current_user = options[:current_user]
      member = Member.find(member_id)
      # <ListAcuteMedication apiVersion="1" uuid="49995FF0-6832-11E5-922C-DD8277A0DF3D" unitId="DS123" patientId="8a2a88e000000000" onlineUserId="8a2a88e000000000"></ListAcuteMedication>
      request_body =  ::Clinic::Tpp::Client.request_body_data(current_user,member_id,options)
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.ListAcuteMedication(request_body) 
      end
      suid_required = true
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options = ({:request_type=>"Post", :request_uri=> "ListAcuteMedication", :request_body=>request_body})
      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
      #response,status =  [{"ListAcuteMedicationReply"=>{"patientId"=>"8a2a88e000000000", "onlineUserId"=>"8a2a88e000000000", "uuid"=>"49995FF0-6832-11E5-922C-DD8277A0DF3D", "Medication"=>{"drugId"=>"0d3679d800000000", "type"=>"Acute", "drug"=>"Aspirin 300mg tablets", "details"=>"32 tablet - take 1 or 2 3 times/day\nLast Issued: 01 Oct 2015", "requestable"=>"y"}}},200]
      if status == 200
        medication_list = response["ListAcuteMedicationReply"]["Medication"]
        medication_list = [medication_list].flatten
      else
        medication_list = []
      end
    rescue Exception=>e 
      medication_list = []
    end
    medication_list
  end

  # ::Clinic::Tpp::Prescribing.get_repeat_medication(member_id,options)
  def self.get_repeat_medication(member_id,options={})
    begin
      current_user = options[:current_user]
      member = Member.find(member_id)
      # <ListRepeatMedication apiVersion="1" uuid="49995FF0-6832-11E5-922C-DD8277A0DF3D" unitId="DS123" patientId="8a2a88e000000000" onlineUserId="8a2a88e000000000"></ListRepeatMedication>
      request_body =  ::Clinic::Tpp::Client.request_body_data(current_user,member_id,options)
       
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.ListRepeatMedication(request_body) 
      end
      suid_required = true
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options = ({:request_type=>"Post", :request_uri=> "ListRepeatMedication", :request_body=>request_body})
      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
      #response,status =  [{"ListRepeatMedicationReply"=>{"patientId"=>"8a2a88e000000000", "onlineUserId"=>"8a2a88e000000000", "uuid"=>"49DC0E40-6832-11E5-922C-DD8277A0DF3D", "Medication"=>{"drugId"=>"285e700000000000", "type"=>"Repeat", "drug"=>"Ibuprofen 400mg capsules", "details"=>"10 capsule - take one every 4 hrs\nLast Issued: Never", "requestable"=>"y"}}},200]
      if status == 200
        medication_list = response["ListRepeatMedicationReply"]["Medication"]
        medication_list = [medication_list].flatten
      else
        medication_list = []
      end
    rescue Exception=>e 
      medication_list = []
    end
    medication_list
  end
  
  def self.formate_tpp_medication(medication,options)
    quantity = medication["details"].split(" - ").first
    dosage,last_issued = medication["details"].split(" - ").last.split("\n")
    last_issued_date = last_issued.split("Last Issued:").last.strip
    {
      :Name=>medication["drug"],
      :MedicationCourseGuid=>medication["drugId"],
      :PrescriptionType=>medication["type"],
      :Dosage=>dosage,
      :MostRecentIssueDate=> (last_issued_date.to_date.to_time rescue nil),
      :QuantityRepresentation=>quantity,
      :CanBeRequested=> medication["requestable"] == "y"
    }
  end


  # ::Clinic::Tpp::Prescribing.get_medication_courses(organization_uid,member_id,options)
  def self.get_medication_courses(organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      medicaton_list =  ::Clinic::Tpp::Prescribing.get_acute_medication(member_id,options).compact
      repeat_medication_list = ::Clinic::Tpp::Prescribing.get_repeat_medication(member_id,options).compact
      medicaton_list = repeat_medication_list + medicaton_list
      courses = medicaton_list.map{|medication| ::Clinic::Tpp::Prescribing.formate_tpp_medication(medication,options)}
      return {courses: courses, status:200}
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      return {message: ::Message::Tpp[:Prescribing][:unable_to_list_prescription], error:e.message, status:100}
    end
  end
 
  # Tpp
  # medication_course_ids = [{:id=>"9ce0766e-b51f-4f68-8426-841e178af08c",:type=>'Acute',:notes=>""},{:id=>"9ce0766e-b51f-4f68-8426-841e178af08c",:type=>"Repeat"}]
  #::Prescribing.request_prescription(medication_course_ids,organization_uid,member_id,options)
  #  options = {:current_user=>current_user,:request_data=>{:RequestComment=>'Hi'}}
  def self.request_prescription(medication_course_ids,organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      member = Member.find(member_id)
      # <RequestMedication apiVersion="1" uuid="838756A0-257E-11E5-B584-D5EDE7EBA782" unitId="X9999" patientId="d299100000000000" onlineUserId="d299100000000000" notes="Can I collect these before the weekend please?">
      #   <Medication drugId="5135100000000000" type="Repeat"/>
      #   <Medication drugId="29d9000000000000" notes="Back pain is getting worse" type="Acute"/>
      # </RequestMedication>
      request_data = options[:request_data].with_indifferent_access  
      request_body =  ::Clinic::Tpp::Client.request_body_data(current_user,member_id,options)
      request_body.merge!({:notes=>request_data[:RequestComment]})
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.RequestMedication(request_body) do 
          medication_course_ids.each do |medication|
            xml.Medication({"drugId"=>medication[:id],"type"=>medication[:type],"notes"=>medication[:notes]})
          end
        end
      end
      suid_required = true
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options = ({:request_type=>"Post", :request_uri=> "RequestMedication", :request_body=>request_body})
      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
      #response,status = [{"RequestMedicationReply"=>{"patientId"=>"d299100000000000", "onlineUserId"=>"d299100000000000", "message"=>"This prescription will be available to collect from the 11 Jul 2015", "uuid"=>"838756A0-257E-11E5-B584-D5EDE7EBA782"}},200]
     
     if (status != 200 )
        msg  = response["Error"]["technicalMessage"] || response["Error"]["userFriendlyMessage"]
        return {message:msg, error:msg, status:status}
      else
        return {message:response["RequestMedicationReply"]["message"], status:200}
        @request_prescription_retry_count = 0
      end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @request_prescription_retry_count = @request_prescription_retry_count.to_i + 1 
      retry if @request_prescription_retry_count.to_i <= 2
      return {message:response["Error"]["technicalMessage"], error:response["Error"]["technicalMessage"], status:100}
    end
  end

  # ::Clinic::Tpp::Prescribing.get_prescription_requests((Date.today - 1.month),organization_uid,member_id,options)
  def self.get_prescription_requests(from_date, organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      member = Member.find(member_id)
      # <GetPrescriptionRequests apiVersion="1" uuid="CA32C4E0-257E-11E5-B584-D5EDE7EBA782" unitId="X9999" patientId="d299100000000000" onlineUserId="d299100000000000"/>
      request_body =  ::Clinic::Tpp::Client.request_body_data(current_user,member_id,options)
      
       
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.GetPrescriptionRequests(request_body) 
      end
      suid_required = true
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options = ({:request_type=>"Post", :request_uri=> "GetPrescriptionRequests", :request_body=>request_body})
      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
      # response,status =  [{"GetPrescriptionRequestsReply"=>{"patientId"=>"d299100000000000", "onlineUserId"=>"d299100000000000", "uuid"=>"CA32C4E0-257E-11E5-B584-D5EDE7EBA782", "PrescriptionRequestHeader"=>[{"prescriptionRequestHeaderId"=>"a863200000000000", "date"=>"2015-07-08T15:35:00.0Z", "notes"=>"Can I collect these before the weekend please?", "PrescriptionRequest"=>[{"prescriptionRequestId"=>"c893100000000000", "drug"=>"Ibuprofen 5% gel", "details"=>"30 gram - Apply 3 times/day. ", "notes"=>"Back pain is getting worse"}, {"prescriptionRequestId"=>"d893100000000000", "drug"=>"Calpol Infant 120mg/5ml oral suspension 5ml sachets sugar free (McNeil Products Ltd)", "details"=>"1 sachet - use As directed. "}]}, {"prescriptionRequestHeaderId"=>"9863200000000000", "date"=>"2015-07-08T15:36:56.0Z", "notes"=>"", "PrescriptionRequest"=>{"prescriptionRequestId"=>"f893100000000000", "drug"=>"Aspirin 150mg suppositories", "details"=>"10 suppository - insert 3-4 every 4 hrs. "}}]}},200]
      if (status != 200)
        return {requested_prescription_list:[],:message=> response["Error"]["technicalMessage"], status:status}
      else
        
      data = []
      prescription_request_headers = response["GetPrescriptionRequestsReply"]["PrescriptionRequestHeader"]
      # [{"prescriptionRequestHeaderId"=>"a863200000000000", "date"=>"2015-07-08T15:35:00.0Z", "notes"=>"Can I collect these before the weekend please?", "PrescriptionRequest"=>[{"prescriptionRequestId"=>"c893100000000000", "drug"=>"Ibuprofen 5% gel", "details"=>"30 gram - Apply 3 times/day. ", "notes"=>"Back pain is getting worse"}, {"prescriptionRequestId"=>"d893100000000000", "drug"=>"Calpol Infant 120mg/5ml oral suspension 5ml sachets sugar free (McNeil Products Ltd)", "details"=>"1 sachet - use As directed. "}]}, {"prescriptionRequestHeaderId"=>"9863200000000000", "date"=>"2015-07-08T15:36:56.0Z", "notes"=>"", "PrescriptionRequest"=>{"prescriptionRequestId"=>"f893100000000000", "drug"=>"Aspirin 150mg suppositories", "details"=>"10 suppository - insert 3-4 every 4 hrs. "}}]
      [prescription_request_headers].compact.flatten.each do |prescription_req_header|

        [prescription_req_header["PrescriptionRequest"]].flatten.each do |prescription_req|

          header_data= {:request_id=>prescription_req['prescriptionRequestId'],
                        :request_header_id=>prescription_req_header['prescriptionRequestHeaderId'],
                        :request_notes=>prescription_req_header["notes"],
                        :date=>prescription_req_header["date"].to_date
                        }
          # data <<  prescription_req.merge(header_data)
          data << ::Clinic::Tpp::Prescribing.formate_tpp_prescription_data(prescription_req.merge(header_data))
        end
      end
      @retry_count = 0
      return {requested_prescription_list:data, status:200}
      end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      return {message:response["Error"]["technicalMessage"],:error=>e.message, requested_prescription_list:data, status:100}
    end
  end
 
  def self.formate_tpp_prescription_data(data)
    data = data.with_indifferent_access
    # {"request_id"=>'',request_header_id=>'',"request_notes"=>'',"date"=>"", "prescriptionRequestId"=>"c893100000000000", "drug"=>"Ibuprofen 5% gel", "details"=>"30 gram - Apply 3 times/day. ", "notes"=>"Back pain is getting worse"}
   begin
    requested_medication_courses = {
          
          "Dosage": data["details"],
          "ExpiryDate": nil,
          "MedicationCourseGuid": nil,
          "MostRecentIssueDate": nil,
          "Name": data["drug"],
          "NextIssueDate":nil,
          "PrescriptionType": "Unknown",
          "QuantityRepresentation": nil,
          "ReviewDate": nil,
          "MostRecentIssueMethod": nil,
          "CanBeRequested": true,
          "Duration": 0
        }
    {
      "RequestGuid": data["request_id"],
      "RequestHeaderId": data["request_header_id"],
      "DateRequested": data[:date].to_date,
      "RequesterComment": (data["notes"] || data[:request_notes]),
      "ClinicianComment": nil,
      "RequestedByDisplayName": nil,
      "RequestedByForenames": nil,
      "RequestedBySurname": nil,
      "RequestedByTitle": nil,
      "date_requested": data[:date].to_date,
      "RequestedMedicationCourses": [requested_medication_courses],
      "requested_medication_courses":[requested_medication_courses]
    }
        
   rescue Exception=>e 
     {}
   end
 end

  # medication_course_ids = ["b2ab7f85-b1ef-4ec3-9ecc-33eb690482e1","9ce0766e-b51f-4f68-8426-841e178af08c"]
  #::Clinic::Tpp::Prescribing.cancel_prescription(medication_course_ids,organization_uid,member_id,options)
  def self.cancel_prescription(request_id,organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      member = Member.find(member_id)
      # <AmendPrescriptionRequest apiVersion="1" uuid="1F49E941-257F-11E5-B584-D5EDE7EBA782" unitId="X9999" patientId="d299100000000000" onlineUserId="d299100000000000" prescriptionRequestHeaderId="a863200000000000" notes="Can I collect these before Thursday lunchtime?">
      #   <PrescriptionRequest prescriptionRequestId="d893100000000000" notes="No longer needed" cancelled="y"/>
      # </AmendPrescriptionRequest>
      request_header_id = options[:request_header_id]
      request_header_note = options[:request_header_note]

      request_body =  ::Clinic::Tpp::Client.request_body_data(current_user,member_id,options)
      if request_header_id.present?
        request_body.merge!({:prescriptionRequestHeaderId=> request_header_id})
      end
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.AmendPrescriptionRequest(request_body){
          xml.PrescriptionRequest({"prescriptionRequestId"=>request_id,"cancelled"=>"y","notes"=>options[:cancellation_reason]})
        }
      end
      suid_required = true
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options = ({:request_type=>"Post", :request_uri=> "AmendPrescriptionRequest", :request_body=>request_body})
      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
    
      if (status != 200 )
        return {message:response["Error"]["technicalMessage"], error:response["Error"]["technicalMessage"], status:status}
      else
        return {message:"Prescription Request cancelled succesfully", status:200}
        @retry_count = 0
      end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      return {message:response["Error"]["technicalMessage"],:error=>e.message, status:100}
    end
  end

  
   

end