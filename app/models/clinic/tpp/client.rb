class Clinic::Tpp::Client

  def self.get_session_uid(current_user,member_id,options={})
    user_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
    if user_session.blank?
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      clinic_link_detail = ::Clinic::LinkDetail.where(member_id:member_id).last
      user_details = clinic_link_detail.link_info.with_indifferent_access
      
      response = ::Clinic::Tpp::Login.get_session_id(current_user,member_id,user_details,options)
      if response[:status] ==  100
        return [nil,100,response["message"]]
      end
        user_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
    
    end 
    [user_session.session_id,200,nil]
  end

  def self.clear_member_session(current_user,member_id,options={})
      ::Clinic::UserClinicSession.where(member_id:member_id,parent_member_id:current_user.id).delete_all
    end


  def self.request_body_data(current_user,member_id,options={})
     clinic_link_detail =  ::Clinic::LinkDetail.where(member_id:member_id).last
      user_link_detail = clinic_link_detail.link_info.with_indifferent_access
      request_body = {
        "apiVersion":1,
        "patientId": user_link_detail[:patientId],
        "unitId": user_link_detail[:practice_ods_code],
        "onlineUserId": user_link_detail["onlineUserId"],
        "uuid": SecureRandom.uuid.upcase}
  end
    
  def self.make_request(current_user,member_id,request_options,suid_required=true,options={})
    begin
      data = {}
      if current_user.clinic_account_status != "Active"
         data = { "Message"=> ::Message::Clinic[:clinic_access_blocked], status:403}
         return [data,403]
        end
      if current_user.is_gpsoc_api_request_limit_exceed?(options)
        data = { "Message"=> ::Message::Clinic[:clinic_access_limit_exceeded], status:403}
        return [data,403]
      end
      status = 200
      clinic_link_detail = ::Clinic::LinkDetail.where(member_id:member_id).last
      pinDocument = clinic_link_detail.link_info rescue {}
      
      if suid_required == false
        suid = ""
      else
        suid,session_status,message =  ::Clinic::Tpp::Client.get_session_uid(current_user,member_id,options)
      end
       if  session_status == 100
        return {message:message,status:100}
       end     
      header = { 'suid'=> suid, 'Content-Type'=> 'application/xml','type'=>request_options[:request_uri].to_s}.select{|key,val| val.present?}
      header.merge!(request_options[:request_header]) if request_options[:request_header].present?
      TppLogger.info  '...................Request..Action ..................'
      TppLogger.info  "..................#{request_options[:request_uri].to_s}.................."
      TppLogger.info  '...................Request...Headers values...................'
      TppLogger.info  header.inspect
      puts '.....................Request.Headers values...................'

      uri = URI.parse(APP_CONFIG['tpp_end_point'] + request_options[:request_uri].to_s)

      http = Net::HTTP.new(uri.host, uri.port)
      if uri.scheme == 'https'
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_PEER
      end
      # certificates added
      http.cert = OpenSSL::X509::Certificate.new(File.read("#{Rails.root}/#{APP_CONFIG['tpp_certificate_file_path']}"))
      http.ca_file = "#{Rails.root}/#{APP_CONFIG['tpp_ca_file_path']}"
      http.key = OpenSSL::PKey::RSA.new(File.read("#{Rails.root}/#{APP_CONFIG['tpp_key_file_path']}"))
      
      request_body = request_options[:request_body]
      #  if request_options[:request_uri].to_s == "ListServiceAccesses" 
      #   request_body = '<ListServiceAccesses apiVersion="1" unitId="HSCIC22" patientId="2619300000000000" onlineUserId="2619300000000000" carerId="2619300000000000"></ListServiceAccesses>'

      # end  
      TppLogger.info '......................Request Body...................'
      request_data = Hash.from_xml(request_body) rescue {}
      request_data =  ::Report::ApiLog.flatten_hash(request_data)
      request_data[:passphrase] = "*********" if request_data[:passphrase].present?
        
      TppLogger.info '......................Request Body...................'
      TppLogger.info  request_data.inspect
      message_id  = Report::ApiLog.get_key_value(Hash.from_xml(request_body),"uuid") rescue ''
      request = Net::HTTP::Post.new(uri.request_uri, header)
      request.body = request_body.to_s if request_body.present?
     
      response = http.request(request)
     

      TppLogger.info '......................Response Code...................'
      TppLogger.info  response.code rescue nil

      if ["MessageRecipients","TestResultsView",'ViewPatientOverview','RequestBinaryData'].include?(request_options[:request_uri].to_s)
         
        response_data = XmlSimple.xml_in response.body
      else
        response_data = Hash.from_xml(response.body)
      end
      TppLogger.info  '......................Response...................'
      if response_data["errorCode"].present?
        response_data = {"Error"=> response_data}
      end

      if response_data["Error"].present? || response_data["technicalMessage"] || Rails.env != "production"
        TppLogger.info  response.body.inspect rescue nil
      end

      if response_data["technicalMessage"] == "Not authenticated" ||  (response_data["Error"].present? && response_data["Error"]["technicalMessage"] == "Not authenticated")
        raise response_data["technicalMessage"] ||response_data["Error"]["technicalMessage"]
      elsif response_data["Error"].present? 
        if response_data["Error"]['userFriendlyMessage'].include?("You don't have access")
          status = 403
        else
          status = 100 
        end     
          response_data["Error"]["technicalMessage"] = response_data["Error"]["technicalMessage"] || response_data["Error"]['userFriendlyMessage']
          response_data["Error"]['userFriendlyMessage'] = response_data["Error"]['userFriendlyMessage'] || response_data["Error"]['technicalMessage']
          response_data["Message"] = response_data["Error"]['userFriendlyMessage']
      elsif response.code.to_s == "200"
        if request_options[:request_uri].to_s == "Authenticate"
          response_data["AuthenticateReply"]["suid"] = response.to_hash["suid"].first rescue nil
        end
        
      end
    
    rescue Exception=>e 
      TppLogger.info "................ Erorr description......................."
      TppLogger.info "................ #{e.message}......................."
      if @makeRequest_retry_count.to_i < 2
        ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
        @makeRequest_retry_count = @makeRequest_retry_count.to_i  + 1
        TppLogger.info "................Retrying Tpp session........."
        retry
        response_data = {"Error"=>{"userFriendlyMessage"=>"Unable to connect Tpp server","technicalMessage"=>"Unable to connect Tpp server" }}
      status = 100
      end  
      if e.message == "Not authenticated"
        TppLogger.info "................Clear expire session for......."
        ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      end  
      # UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,request_options,"Error inside emis make_request : #{options[:request_uri]} ")
      # raise e.message if @user_session_required == true
    end
     # log event 
    action =  request_options[:request_uri].to_s
    request_status = status == 200 ? "true" : "false"
    request_data = {:request_status=> request_status, :action=> action, :patient_id=>(pinDocument["account_id"] rescue nil),:message_id=>message_id}    
    current_user.log_tpp_request(member_id,request_data,options)

    [response_data,status]
  end


  

end