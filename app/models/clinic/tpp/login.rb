class Clinic::Tpp::Login
   
  def get_clinic_linkable_status(user_details,options={})
    true
  end


  def self.application_info(current_user=nil)
    device = current_user.devices.last || {} rescue {}
    devcie_type = (device["platform"] || "IOS").upcase
    version = device["app_version"] || "1.1"
    {
      "name":"Nurturey",
      "version": version,
      "deviceType": devcie_type ,
      "providerId":"1998fbfaecc76615"
    }
  end

  # v2

  # Api 2.0.0.0
   # user_details = {account_id:364823672,:account_linkage_key=>'\XY7&t66MNaqcsr6',surname:'Alter',date_of_birth:'1954-09-16',practice_ods_code:'HSCIC22'}
   # if proxy linking
   # user_details ={"first_name"=>"Shanna", "account_id"=>"790653319", "surname"=>"Bloom", "register_with_pindocument"=>"true", "date_of_birth"=>"26 Jun 1967", "practice_ods_code"=>"HSCIC25", "onlineUserId"=>"f18a300000000000", "patientId"=>"f18a300000000000", "passphrase"=>"PCycCT0BdTucw5R6iNR3oRN3nHA0dKsdtDeHmwrRR++EPWoBpPNgqbHR59Q6dhy8Xt2c/CObOrAOb4R8xe8lCMof3Wp/UySUnEJ+kimy6warVjUgcvVjdr86DeHXVdmgk6gxUbXVc9UkYYjDUA2ggy9+dUn+kJvw/5W0Rr8jJ5I=", "proxy_user_status"=>false, "patient_context_guid"=>"8d9a300000000000"}
   def self.authenticateUser(member_id,user_details,options={})
    options[:member_id] = member_id
    member = Member.find(member_id)
    ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
    current_user = options[:current_user]
     
    if options[:proxy_user_link_flow] == true
      user_details["patientId"] = user_details[:patient_context_guid]
    end
    pin_document = user_details.with_indifferent_access

    if options[:register_with_pindocument] == true
     # pinDocument = {"account_id"=>"2325028826","account_linkage_key"=>"TcdbmCgdUANr7","practice_ods_code"=>"A28826","surname"=>"","date_of_birth"=>"1993-04-08"}
      response = ::Clinic::Tpp::RegisterUser.register_with_pin_document(pin_document,options)
      # response = {:data=>{"onlineUserId"=>"2619300000000000", "passphrase"=>"4oy3F+EVawfeWq4j41oU0bOC2by1GZ00knnnRuOy78AX1haVh6ne+8WDdGzPLfq95C9RKehQes4If0j30DtN4f0YWr1iksgtJtd2wdN/Iyi3B8u+qh1K845QUAGmPJBGuxCBY+jnfYi+30zzMMaXqdRQOZ2CEiR3z6457GU/K2U=", "uuid"=>"F4C2B0F0-35EE-11EA-BEAB-00155D3D2A05"},status:200}
    else
      # user_details = {:title=>'Mr',:first_name=>"HHHH",:surname=>'Garg',:date_of_birth=> "1991-01-01", :gender=>"Male", :house_name_no=>"10 Rillington Place",:street=>"Smart Street",:town=>"ontario", :postcode=>"BR5 1QB", :practice_ods_code=>"A28826", :telephone_number=>"",:mobile_number=>"", :email=>"hh@ff.com", :village=>"",:country=>"" }.with_indifferent_access
     response = ::Clinic::Tpp::RegisterUser.register_with_patient_detail(user_details,options)
    end
     
 
    response = response.with_indifferent_access
    return response if response[:status] == 100
    is_child_member = member.user_id.blank?
    # user_details = {account_id:364823672, practice_code:'HSCIC22', "passphrase"=>"4oy3F+EVawfeWq4j41oU0bOC2by1GZ00knnnRuOy78AX1haVh6ne+8WDdGzPLfq95C9RKehQes4If0j30DtN4f0YWr1iksgtJtd2wdN/Iyi3B8u+qh1K845QUAGmPJBGuxCBY+jnfYi+30zzMMaXqdRQOZ2CEiR3z6457GU/K2U=", "uuid"=>"E16A85D0-3636-11EA-BEAB-00155D3D2A05"}
    user_detail_data = {}

    user_details = response 
    user_details[:practice_code] =  pin_document[:practice_ods_code]
    user_details[:practice_ods_code] =  pin_document[:practice_ods_code]
    user_details[:account_id] =  pin_document[:account_id]
     
    if member.birth_date.blank?
      member.birth_date = pin_document[:date_of_birth]
      member.save
    end
    options[:session_request_response] = response 
    if options[:is_proxy_selected] == true
      user_detail_data,status =  ::Clinic::Tpp::Login.getPatientInfo(current_user,member_id,user_details,options)
    elsif member.is_child? && member.age_in_months < (13*12) # 13 years
      user_detail_data,status =  ::Clinic::Tpp::Login.getPatientInfo(current_user,member_id,user_details,options)
    elsif !member.is_child? && member.age_in_months > (16*12)
     user_detail_data,status =  ::Clinic::Tpp::Login.getPatientInfo(current_user,member_id,user_details,options)
    else
      response =  {:message=>::Message::Tpp[:Message][:Login][:unauthorized_to_access],:status=>100}.with_indifferent_access
    end 

    if options[:is_proxy_selected] == true
      report_event_custom_id = 294 #Link child via proxy access
      status_text = status == 200 ? "success" : "failed"
      ::Report::ApiLog.log_event(current_user.user_id,options[:family_id],report_event_custom_id,options.merge({:status=>status_text})) rescue nil
    end  

    # if user_detail_data.present? #&& options[:is_proxy_selected] != true
    #   if user_detail_data[:first_name].downcase != member.first_name.downcase
    #     response =  {:message=>'User is not authorized to access records',:status=>100}.with_indifferent_access
    #   end
    # end

    response.merge!(user_detail_data) #rescue nil
    response
  end

   # user_details = {:passphrase=>'',:practice_code=>'A28826'}
   # user_details = {account_id:364823672, practice_code:'HSCIC22', "passphrase"=>"4oy3F+EVawfeWq4j41oU0bOC2by1GZ00knnnRuOy78AX1haVh6ne+8WDdGzPLfq95C9RKehQes4If0j30DtN4f0YWr1iksgtJtd2wdN/Iyi3B8u+qh1K845QUAGmPJBGuxCBY+jnfYi+30zzMMaXqdRQOZ2CEiR3z6457GU/K2U=", "uuid"=>"E16A85D0-3636-11EA-BEAB-00155D3D2A05"}
  def self.getPatientInfo(current_user,member_id,user_details,options={})
    begin
      if !options[:session_request_response].present?
        user_details = user_details.with_indifferent_access
        current_user = options[:current_user]
        response = ::Clinic::Tpp::Login.get_session_id(current_user,member_id,user_details,options)
      else
        response = options[:session_request_response]
      end
      demographic_request_data = {:patientId => response["patientId"],
                                :unitId => user_details["practice_ods_code"],
                                :onlineUserId =>  response["onlineUserId"],
                                :uuid => SecureRandom.uuid.upcase,
                                :demographicsType=> "Patient",
                                :apiVersion=>1
                              }
      demograpic_response = ::Clinic::Tpp::Login.get_patient_demographic(member_id,demographic_request_data,options)
      
      if demograpic_response[:status] == 200 
        demograpic_response = demograpic_response[:data] rescue  {}
      else
        demograpic_response = {}
      end
      address_detail = response["PatientAddressDetails"]["Address"]["address"] 
      # {"patientId"=>"2619300000000000", "isSelfAccess"=>"y", "usualGp"=>"Dr HM Irving", "SiteDetails"=>{"unitName"=>"HSCIC Unsupported Test Environment V", "unitNameAlternative"=>"", "appointmentsTelephone"=>"", "prescriptionTelephone"=>"", "generalTelephone1"=>"01132547067", "generalTelephone2"=>"", "emergencyTelephone"=>"", "Address"=>{"address"=>"2 Trevelyan Square, Leeds, West Yorkshire, LS1 6AE", "addressType"=>"0"}}, "PatientAddressDetails"=>{"Address"=>{"address"=>"46 Winsover Road, Spalding, Lincolnshire, PE11 1EN", "addressType"=>"0"}}, "dateOfBirth"=>"1954-09-16T00:00:00.0Z", "gender"=>"Male", "NationalId"=>"9435993931", "PersonName"=>{"name"=>"Mr Idan Ranon Alter"}, "passphrase"=>"627fOjniZKFpnbtOWKF0+n+FqfRUdAStRu3bRe6wRBagfIWylITVSruTBQLWdZPmeiF6aQsDrAlpEoM8JWWvhuP8HsAECqKOqvQPUHt1333SrEEZxwcsM+dbC7qO04j8/TwlhVsw2Tw+GtN8QRbTqVr0FdT+EEzOnmGWJ8Fv0f0=", "onlineUserId"=>"2619300000000000", "suid"=>"wRfnCwfey7vcqjNugpe0a8alUGKb0DBiqVw1/7moIFTvvMnMce+yIC3+OZUwibFZjH99U23Kh+3mFY2Lh6HLz5f8bUSlEfkUNpYbANrfRId738v/J+x1iqtU1HmrShhYVa5m9pijqn7nNJSl9lL5KONcvc9X+JSnYmI0qdc81CQ=", "practice_code"=>"HSCIC22", "account_id"=>"364823672"}
      contact_details = {}
        full_name_with_title = response["PersonName"]["name"]
        first_name,last_name = Clinic::Tpp::RegisterUser.get_first_and_last_name(full_name_with_title,options)

      data = {
        :first_name=>first_name,
        :last_name=> last_name,
        :nhs_number => ( nil),
        :patient_identifier => ( response["patientId"]),
        :date_of_birth=>( response["dateOfBirth"].to_date.to_date6 rescue nil),
        :mobile_number => demograpic_response[:mobile_no],
        :email => ((contact_details["EmailAddress"]|| demograpic_response[:email]) rescue nil),
        :address=> address_detail ,
        :gender=> (response["gender"])
      }
       
      GpsocLogger.info  ".................Data from get patient detail.............................................."
      # GpsocLogger.info  "................#{data.inspect}.............................................."
      GpsocLogger.info  "................................................................................."

      status = 200
    rescue Exception=> e 
      @getPatientInfo_retry_count = @getPatientInfo_retry_count.to_i + 1
      GpsocLogger.info  "..............................................................."
      GpsocLogger.info  "getPatientInfo: Error msg is :#{e.message}"
      puts "...........getPatientInfo..Retry count --  #{@getPatientInfo_retry_count.to_i}"
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      retry if @getPatientInfo_retry_count.to_i <= 2
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside Tpp  getPatienDetail")
      data = {}
      status = 100
      @getPatientInfo_retry_count = 0
    end
    [data,status]
  end


  def self.get_patient_demographic(member_id,request_data,options={})
    begin
      current_user = options[:current_user]
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.RetrieveDemographics(request_data) 
      end
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options = {}
      suid_required = true
      request_options.merge!({:request_type=>"Post", :request_uri=> "RetrieveDemographics", :request_body=>request_body})
      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
      #response,status =  [{"MedicalRecord"=>{"PatientGuid"=>"5a09eb8f-7e0b-4630-ad48-87e8e05d124f", "Title"=>"Master", "Forenames"=>"Mike", "Surname"=>"Singh", "Sex"=>"Male", "DateOfBirth"=>"2016-09-22T00:00:00", "Allergies"=>nil, "Consultations"=>nil, "Documents"=>nil, "Immunisations"=>[{"ObservationType"=>"Immunisation", "Episodicity"=>"None", "NumericValue"=>nil, "NumericOperator"=>nil, "NumericUnits"=>nil, "DisplayValue"=>nil, "TextValue"=>nil, "Range"=>nil, "Abnormal"=>false, "AbnormalReason"=>nil, "AssociatedText"=>nil, "EventGuid"=>"9fe87ee5-4e7e-46e3-8b88-86b4e9ca84bd", "Term"=>"Influenza vaccination", "AvailabilityDateTime"=>"2020-01-30T06:39:56.043", "EffectiveDate"=>{"DatePart"=>"YearMonthDay", "Value"=>"2020-01-30T00:00:00"}, "CodeId"=>142934010, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}, {"ObservationType"=>"Immunisation", "Episodicity"=>"None", "NumericValue"=>nil, "NumericOperator"=>nil, "NumericUnits"=>nil, "DisplayValue"=>nil, "TextValue"=>nil, "Range"=>nil, "Abnormal"=>false, "AbnormalReason"=>nil, "AssociatedText"=>nil, "EventGuid"=>"d9e4d77b-d8c3-4fc9-a0dd-db5a76d3f372", "Term"=>"1st hepatitis A vaccination", "AvailabilityDateTime"=>"2020-01-30T06:41:06.473", "EffectiveDate"=>{"DatePart"=>"YearMonthDay", "Value"=>"2020-01-30T00:00:00"}, "CodeId"=>264177017, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}, {"ObservationType"=>"Immunisation", "Episodicity"=>"None", "NumericValue"=>nil, "NumericOperator"=>nil, "NumericUnits"=>nil, "DisplayValue"=>nil, "TextValue"=>nil, "Range"=>nil, "Abnormal"=>false, "AbnormalReason"=>nil, "AssociatedText"=>nil, "EventGuid"=>"32e9cbf1-a55f-4e61-91fc-ee1446bb53e6", "Term"=>"Immunisation given", "AvailabilityDateTime"=>"2020-01-30T06:18:40.763", "EffectiveDate"=>{"DatePart"=>"YearMonthDay", "Value"=>"2020-01-30T00:00:00"}, "CodeId"=>62701000000116, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}], "Medication"=>nil, "Problems"=>nil, "TestResults"=>nil, "Users"=>[{"UserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "Title"=>"Dr", "Forenames"=>"Nurturey", "Surname"=>"Test", "Role"=>"General Medical Practitioner", "Organisation"=>"Nurt services"}]}, "FilterDetails"=>{"ItemFilter"=>"Immunisations", "ItemFilterFromDate"=>nil, "ItemFilterToDate"=>nil, "FreeTextFilterFromDate"=>nil}}, 200]
      if status == 200
        data = response["RetrieveDemographicsReply"]
        mobile_no = data["Person"]["TelephoneNumbers"]["mobileTelephone"]|| data["Person"]["TelephoneNumbers"]["workTelephone"] || data["Person"]["TelephoneNumbers"]["homeTelephone"]
        email =  data["Person"]["emailAddress"]
        contact_detail = {email:email,mobile_no:mobile_no}
      else
        contact_detail = {}
        message = response["Message"]
        
      end    
    rescue Exception=>e 
      ::Clinic::Tpp::Client.clear_member_session(current_user,member_id,options)
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      status = 100
      @retry_count = 0
      message = 'Unable to get immunisations from clinic'
      @error = e.message
    end
    {data:contact_detail,:status=>status,:message=>message,:error=>@error}
  end
  
  # user_details = {account_id:364823672,:account_linkage_key=>'\XY7&t66MNaqcsr6',surname:'Alter',date_of_birth:'1954-09-16',practice_ods_code:'HSCIC22'}
  # If proxy
  # user_details = {"first_name"=>"Shanna", "account_id"=>"790653319", "surname"=>"Bloom", "register_with_pindocument"=>"true", "date_of_birth"=>"26 Jun 1967", "practice_ods_code"=>"HSCIC25", "onlineUserId"=>"f18a300000000000", "patientId"=>"8d9a300000000000", "passphrase"=>"PCycCT0BdTucw5R6iNR3oRN3nHA0dKsdtDeHmwrRR++EPWoBpPNgqbHR59Q6dhy8Xt2c/CObOrAOb4R8xe8lCMof3Wp/UySUnEJ+kimy6warVjUgcvVjdr86DeHXVdmgk6gxUbXVc9UkYYjDUA2ggy9+dUn+kJvw/5W0Rr8jJ5I=", "proxy_user_status"=>false, "patient_context_guid"=>"8d9a300000000000"}
  # ::Clinic::Tpp::Login.get_session_id(current_user,member_id,user_details,options)
  def self.get_session_id(current_user,member_id,user_details,options={})
    user_details = user_details.with_indifferent_access
    user_details[:date_of_birth] = user_details[:date_of_birth].to_date.strftime("%Y-%m-%d") rescue user_details[:date_of_birth]
      begin
      if user_details["passphrase"].blank?
        link_account = ::Clinic::Tpp::Login.link_account(current_user,member_id,user_details,options)
        if link_account[:status] == 200
          passphrase_for_authenticate = link_account[:data]["passphrase"]
          uuid = link_account[:data]["uuid"]
        else
          return link_account
        end
      else
        passphrase_for_authenticate = user_details["passphrase"]
        uuid = SecureRandom.uuid.upcase
      end
 
      # link_account = {"onlineUserId"=>"2619300000000000", "passphrase"=>"UUQDVXzsY/ajG1zLd0lYcyxwThRje1AdRjKWBtPhp9xCxUB39kjJu8tu1fdZdlztTAZ2S/jd5PHPG5KJ0X7+jWBGNQsPE3lZNRT8pvJFk+1hbPPAf2WkjTLxUeXJactbGngcrQE8A3ODvX3Q1dlQ3boasMXOH9CCSk/7OgCpOGg=", "uuid"=>"88888888-8888-8888-8888-888888888888"}
      authentication_details = {account_id:user_details[:account_id],:passphrase=>passphrase_for_authenticate,practice_ods_code:user_details[:practice_ods_code],uuid:uuid}
      options[:user_details] = user_details
      authentication_response = ::Clinic::Tpp::Login.authenticate_account(current_user,member_id,authentication_details,options)
      
      # ::Clinic::Tpp::Login.select_patient(current_user,member_id,options)
      return authentication_response if authentication_response[:status] == 100
      if user_details["patientId"].present?
        patient_access_data = [authentication_response["data"]["Registration"]["PatientAccess"]].flatten.select{|a| a["patientId"] == user_details["patientId"]}.first#["patientId"]

      else
        patient_access_data = [authentication_response["data"]["Registration"]["PatientAccess"]].flatten.select{|a| a["isSelfAccess"] == "y"}.first#["patientId"]
      end  
      patient_id = patient_access_data["patientId"]
      patient_info_data = [authentication_response["data"]["Person"]].flatten.select{|a| a["patientId"] == patient_id }.first#["patientId"]
      response = patient_access_data.merge(patient_info_data)
      response["passphrase"] = passphrase_for_authenticate
      response["onlineUserId"] = authentication_response[:data]["onlineUserId"]
      response["suid"] = authentication_response[:data]["suid"]
      response[:status] = 200
      TppLogger.info ".................................Suid..............."
      TppLogger.info ".................................#{authentication_response[:data]["suid"]}..............."
    rescue Exception=>e
      # user_details["passphrase"] = nil 
      TppLogger.info "................Error inside..get_session_id...............#{e.message}..............."
      TppLogger.info "................Retrying count...............#{@session_id_retry_count}..............."
      @session_id_retry_count = @session_id_retry_count.to_i + 1
      retry if @session_id_retry_count.to_i < 2
      response = {:message=>::Message::Tpp[:Login][:unable_to_get_session],error:e.message,:status=>100}
    end
    response
    
  end


  # authentication_details = {account_id:364823672,:passphrase=>'UUQDVXzsY/ajG1zLd0lYcyxwThRje1AdRjKWBtPhp9xCxUB39kjJu8tu1fdZdlztTAZ2S/jd5PHPG5KJ0X7+jWBGNQsPE3lZNRT8pvJFk+1hbPPAf2WkjTLxUeXJactbGngcrQE8A3ODvX3Q1dlQ3boasMXOH9CCSk/7OgCpOGg=',practice_ods_code:'HSCIC22',uuid:''}
  def self.authenticate_account(current_user,member_id,authentication_details,options={})
    begin
      @authenticate_account_retry_count = @authenticate_account_retry_count.to_i  
      # clink_link_detail =  ::Clinic::LinkDetail.where(member:member_id).last
      authentication_details = authentication_details.with_indifferent_access
      request_body = {
        "apiVersion":1,
        "accountId": authentication_details[:account_id],
        "passphrase": authentication_details[:passphrase],
        "unitId": authentication_details[:practice_ods_code],
        "uuid": (authentication_details[:uuid] || SecureRandom.uuid.upcase)
      }
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.Authenticate(request_body){
          xml.Application(::Clinic::Tpp::Login.application_info(current_user))
        }
      end
      request_options = {}
      suid_required = false
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options.merge!({:request_type=>"Post", :request_uri=> "Authenticate", :request_body=>request_body})
      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
      
      # TppLogger.info "Account authenticated response status #{status}.........and response  #{response.inspect}........."
      user_details = options[:user_details].with_indifferent_access rescue nil
      if (response["Error"]["userFriendlyMessage"].present? rescue false)
        response_data = {:message=>response["Error"]["userFriendlyMessage"] , :error=>response["Error"]["userFriendlyMessage"],:status=>100}.with_indifferent_access
      elsif status == 200
        # {"AuthenticateReply"=>{"patientId"=>"2619300000000000", "onlineUserId"=>"2619300000000000", "uuid"=>"F14CE150-38F2-11EA-BEAB-00155D3D2A05", "User"=>{"Person"=>{"patientId"=>"2619300000000000", "dateOfBirth"=>"1954-09-16T00:00:00.0Z", "gender"=>"Male", "NationalId"=>"9435993931", "PersonName"=>{"name"=>"Mr Idan Ranon Alter"}}}, "Registration"=>{"unitId"=>"HSCIC22", "unitName"=>"HSCIC Unsupported Test Environment V", "medicationOrderingWindowDays"=>"7", "PatientAccess"=>{"patientId"=>"2619300000000000", "isSelfAccess"=>"y", "usualGp"=>"Dr HM Irving", "SiteDetails"=>{"unitName"=>"HSCIC Unsupported Test Environment V", "unitNameAlternative"=>"", "appointmentsTelephone"=>"", "prescriptionTelephone"=>"", "generalTelephone1"=>"01132547067", "generalTelephone2"=>"", "emergencyTelephone"=>"", "Address"=>{"address"=>"2 Trevelyan Square, Leeds, West Yorkshire, LS1 6AE", "addressType"=>"0"}}, "PatientAddressDetails"=>{"Address"=>{"address"=>"46 Winsover Road, Spalding, Lincolnshire, PE11 1EN", "addressType"=>"0"}}}}, "Person"=>{"patientId"=>"2619300000000000", "dateOfBirth"=>"1954-09-16T00:00:00.0Z", "gender"=>"Male", "NationalId"=>"9435993931", "PersonName"=>{"name"=>"Mr Idan Ranon Alter"}}, "suid"=>"Mf9DIMoWDix/5r5qDPDC6i+4pqKZJ6tV61iki9qkmaLMBvCdZ/TmsgTZMP3Ll8etrlE5RBGU28sDV1jB0IwdsZq6eXxbs9+Mg6V5rvFJGi7rqt/u7qsTEHcH+V2bCOpr+cgbL3EMSByse5XAQJtKSeDImPP2/+VN3UMcf9hqE/Y="}}
        session_id = response["AuthenticateReply"]["suid"]
        ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
        ::Clinic::UserClinicSession.new(member_id:member_id,session_id:session_id).save
        clinic_link_detail = ::Clinic::LinkDetail.get_clinic_link_detail(member_id,current_user,options)
        if clinic_link_detail.present?
          clinic_link_detail.link_info["passphrase"] = authentication_details[:passphrase]
          clinic_link_detail.save
        elsif user_details.present?
          if user_details["patientId"].present?
            patient_access_data = [response["AuthenticateReply"]["Registration"]["PatientAccess"]].flatten.select{|a| a["patientId"] == user_details["patientId"]}.first#["patientId"]
          else
            patient_access_data = [response["AuthenticateReply"]["Registration"]["PatientAccess"]].flatten.select{|a| a["isSelfAccess"] == "y"}.first#["patientId"]
          end
          user_details["onlineUserId"] = response["AuthenticateReply"]["onlineUserId"]
          user_details["patientId"] = patient_access_data["patientId"]
        else 
          patient_access_data = [response["AuthenticateReply"]["Registration"]["PatientAccess"]].flatten.select{|a| a["isSelfAccess"] == "y"}.first#["patientId"]
          patient_id = patient_access_data["patientId"]
          online_userid =  response["AuthenticateReply"]["onlineUserId"]
          user_details = authentication_details.merge({:onlineUserId=>online_userid,:patientId=>patient_id})
        end


        options[:user_details] = user_details
        response_data = {:data=>response["AuthenticateReply"],:suid=>session_id, :status=>200}.with_indifferent_access
      else
        response_data = {:message=>'Unable to Authenticate Account' , :error=>response["Error"]["technicalMessage"],:status=>100}.with_indifferent_access
      end
      ::Clinic::Tpp::Login.select_patient(current_user,member_id,options) if status == 200
    rescue Exception => e
      TppLogger.info "................Error inside Authentication...#{e.message}.."
      authentication_details[:passphrase] = nil 
      TppLogger.info "................Retrying Authentication....count...#{@authenticate_account_retry_count}.."
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside tpp authenticate_account : #{options[:request_uri]} ")
      @authenticate_account_retry_count = @authenticate_account_retry_count.to_i + 1
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      # retry if @authenticate_account_retry_count <= 2 
      response_data  = {:error=>e.message,:message=>::Message::Clinic[:unable_to_connect_server], :status=>100}.with_indifferent_access
    end
    @authenticate_account_retry_count = 0
    response_data
  end



  # user_details = {account_id:364823672,:account_linkage_key=>'\XY7&t66MNaqcsr6',surname:'Alter',date_of_birth:'1954-09-16',practice_ods_code:'HSCIC22'}
  # link Account
  def self.link_account(current_user,member_id,user_details,options={})
    begin
      @link_account_retry_count = @link_account_retry_count.to_i  
      # clink_link_detail =  ::Clinic::LinkDetail.where(member:member_id).last
      user_details = user_details.with_indifferent_access
      request_body = {
        "apiVersion":1,
        "accountId": user_details[:account_id],
        "passphrase": user_details[:account_linkage_key],
        "lastName": user_details[:surname],
        "dateOfBirth": user_details[:date_of_birth].to_time.iso8601,
        "organisationCode"=> user_details[:practice_ods_code]
      }
      # TO handle invalid xml request due to double quotes
      valid_to_make_request = request_body.select{|k,v| v.blank?}.blank?

        builder = Nokogiri::XML::Builder.new do |xml|
          xml.LinkAccount(request_body){
            xml.Application(::Clinic::Tpp::Login.application_info(current_user))
          }
        end
        request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")


      request_options = {}
      suid_required = false
      request_options.merge!({:request_type=>"Post", :request_uri=> "LinkAccount", :request_body=>request_body})
      if valid_to_make_request
        response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
      else
        response,status = [{"Error"=>{"userFriendlyMessage"=>"Invalid credentials"}},100]
      end
      if (response["Error"]["userFriendlyMessage"].present? rescue false)
        response_data = {:message=>response["Error"]["userFriendlyMessage"], :error=>response["Error"]["userFriendlyMessage"],:status=>100}.with_indifferent_access
      elsif status == 200
        # {"onlineUserId"=>"2619300000000000", "passphrase"=>"UUQDVXzsY/ajG1zLd0lYcyxwThRje1AdRjKWBtPhp9xCxUB39kjJu8tu1fdZdlztTAZ2S/jd5PHPG5KJ0X7+jWBGNQsPE3lZNRT8pvJFk+1hbPPAf2WkjTLxUeXJactbGngcrQE8A3ODvX3Q1dlQ3boasMXOH9CCSk/7OgCpOGg=", "uuid"=>"88888888-8888-8888-8888-888888888888"}
        response_data = {:data=>response["LinkAccountReply"], :status=>200}.with_indifferent_access
      else
       clinic_linked_detail = ::Clinic::DelinkedLinkDetail.where(email:current_user.email).last
        if  clinic_linked_detail.present? && (current_user.user.is_internal? || current_user.user.is_beta_user?)
          link_info = clinic_linked_detail.link_info
          response_data = {}
          response_data["onlineUserId"] = link_info["onlineUserId"]
          response_data["passphrase"] = link_info["passphrase"]
          response_data["uuid"] = SecureRandom.uuid.upcase
          response_data = {:data=>response_data, :status=>200}.with_indifferent_access

        else
          response_data = {:message=>'Unable to link your account with TPP', :error=>response["Error"]["technicalMessage"],:status=>100}.with_indifferent_access
        end
      end
    rescue Exception => e 
      TppLogger.info "Error inside link_account................... #{e.message}"
      TppLogger.info "Retrying Link Account................... #{@link_account_retry_count}"
  
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside tpp link_account : #{options[:request_uri]} ")
      @link_account_retry_count = @link_account_retry_count.to_i + 1
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      # retry if @link_account_retry_count <= 2 
      response_data  = {:message=>e.message,:status=>100}.with_indifferent_access
    end
    @link_account_retry_count = 0
    response_data
  end

  def self.delink_with_clinic(member_id,options={})
      current_user = options[:current_user]
      TppLogger.info "..................Delink with clinic............"
      report_event_custom_id = 292 #Report::Event.where(name:"GP Account delink").last
      options[:ip_address] = current_user.gpsoc_api_requested_ip
      ::Report::ApiLog.log_event(current_user.user_id,options[:family_id],report_event_custom_id,options.merge({:status=>"success"})) rescue nil
      response = {:status=>200,:message=>"Delink with clinic"}
  end


  def self.select_patient(current_user,member_id,options={})
    begin
      TppLogger.info "..................  select_patient request............"
      
      @select_patient_retry_count = @select_patient_retry_count.to_i  
      clinic_link_detail =  ::Clinic::LinkDetail.get_clinic_link_detail(member_id,current_user,options)
      if clinic_link_detail.present?
        user_details = clinic_link_detail.link_info.with_indifferent_access
      else
        user_details = options[:user_details]
      end
      request_body = {
        "apiVersion":1,
        "patientId": user_details[:patientId],
        "onlineUserId": user_details[:onlineUserId],
        "uuid": SecureRandom.uuid.upcase,
        "unitId": user_details[:practice_ods_code]
      }
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.PatientSelected(request_body) 
      end
      request_options = {}
      suid_required = true
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options.merge!({:request_type=>"Post", :request_uri=> "PatientSelected", :request_body=>request_body})
      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
      # TppLogger.info ".....................select_patient response .......... #{response.inspect}..........."
      if (response["Error"]["userFriendlyMessage"].present? rescue false)
        response_data = {:message=>response["Error"]["userFriendlyMessage"], :error=>response["Error"]["userFriendlyMessage"],:status=>100}.with_indifferent_access
      elsif status == 200
        # {"onlineUserId"=>"2619300000000000", "passphrase"=>"UUQDVXzsY/ajG1zLd0lYcyxwThRje1AdRjKWBtPhp9xCxUB39kjJu8tu1fdZdlztTAZ2S/jd5PHPG5KJ0X7+jWBGNQsPE3lZNRT8pvJFk+1hbPPAf2WkjTLxUeXJactbGngcrQE8A3ODvX3Q1dlQ3boasMXOH9CCSk/7OgCpOGg=", "uuid"=>"88888888-8888-8888-8888-888888888888"}
        response_data = {:data=>response["PatientSelectedReply"], :status=>200}.with_indifferent_access
      else
        response_data = {:message=>'Unable to link your account with TPP', :error=>(response["Error"]["technicalMessage"] rescue 'error'),:status=>100}.with_indifferent_access
      end
    rescue Exception => e 
      TppLogger.info "Error inside select_patient................... #{e.message}"
      TppLogger.info "Retrying Select Patient................... #{@select_patient_retry_count}"
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside tpp Select patient : #{options[:request_uri]} ")
      @select_patient_retry_count = @select_patient_retry_count.to_i + 1
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      # retry if @select_patient_retry_count <= 2 
      response_data  = {:message=>e.message,:status=>100}.with_indifferent_access
    end
    @select_patient_retry_count = 0
    response_data
  end


  # Get application session ID
  def self.available_services(member_id,options={})
    begin
      request_data = {}
      current_user = options[:current_user]
      clinic_link_detail =  ::Clinic::LinkDetail.where(member_id:member_id).last
      user_link_detail = clinic_link_detail.link_info.with_indifferent_access
      request_body = {
        "apiVersion":1,
        "patientId":user_link_detail[:patientId] ,
        "unitId": user_link_detail[:practice_ods_code],
        "onlineUserId": user_link_detail["onlineUserId"],
        "carerId": user_link_detail["onlineUserId"]
      }
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.ListServiceAccesses(request_body) 
      end
      request_options = {}
      suid_required = true
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options.merge!({:request_type=>"Post", :request_uri=> "ListServiceAccesses", :request_body=>request_body})
      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
      if (response["Error"]["userFriendlyMessage"] rescue false)
        raise response["Error"]["userFriendlyMessage"] 
      end
       service_data =  {appointment_booking:true,
                       repeat_prescription:false,
                       view_health_records:false,
                       manage_messages:false,
                       immunisations:false}
      response["ListServiceAccessesReply"]["ServiceAccess"].each do |service|
        case  service["description"]
          when "Appointments"
            service_data[:appointment_booking] = service["status"] == "A"
          when "Messaging"
            service_data[:manage_messages] = service["status"] == "A"
          when  "Request Medication"
            service_data[:repeat_prescription] = service["status"] == "A"
          when "Summary Record"
            service_data[:view_health_records] = service["status"] == "A"
          when "Full Clinical Record"
            service_data[:immunisations] = service["status"] == "A"
        end  
      end

      if !service_data.values.include?(true)
        report_event_custom_id = 291 #Report::Event.where(name:'No GP service enabled for fully linked with pin document').last
        ::Report::ApiLog.log_event(current_user.user_id,options[:family_id],report_event_custom_id,options.merge({:status=>"success"})) rescue nil
      elsif service_data[:view_health_records] != true
        report_event_custom_id = 293 #Report::Event.where(name:'HealthReport not enabled').last
        ::Report::ApiLog.log_event(current_user.user_id,options[:family_id],report_event_custom_id,options.merge({:status=>"success"})) rescue nil
      end

      response = {status:200, :services=>service_data}
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @available_services_retry_count =  @available_services_retry_count.to_i + 1
      if @available_services_retry_count <= 2
        retry
      end
      response = {status:100, message:e.message }
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside Tpp login end_user_session")
    end
      @available_services_retry_count = 0
    response
  end
  

  def request_for_service(current_user,member_id,options)
    request_data = {}
    appointment_data = {}
    clinic_link_detail =  ::Clinic::LinkDetail.where(member_id:member_id).last
    user_link_detail = clinic_link_detail.link_info.with_indifferent_access
    request_body = {
      "apiVersion":1,
      "patientId": user_link_detail[:patientId],
      "unitId": user_link_detail[:practice_ods_code],
      "onlineUserId": user_link_detail["onlineUserId"],
      "uuid": clinic_link_detail.user_info["uuid"]
      }
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.RequestAccessToService(request_body){
          xml.Item("Add","desc": 'Messaging') 
           
        }
      end
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options = {}

      suid_required = true
      request_options.merge!({:request_type=>"Post", :request_uri=> "RequestAccessToService", :request_body=>request_body})
      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
    
    
  end
   

end

