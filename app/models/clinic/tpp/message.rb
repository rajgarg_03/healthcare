class Clinic::Tpp::Message
  
  def self.formatted_massage(message,message_list,msg_type,options={})
    message["sent"] = message["sent"].to_s.gsub("Z","")
    data = message
    lastest_time = message["sent"].to_s.gsub('z','').gsub('Z','')
    data["MessageId"] = message["conversationId"].to_s

    data["Subject"] = nil #message["messageText"]
    data["Content"] = message["messageText"]
    data["SentDateTime"] = message["sent"].to_s.gsub('z','').gsub('Z','')
    data["Recipients"] = {"Name": message["recipient"],"RecipientType":nil,"RecipientGuid":nil}
    data["HasUnreadReplies"] = message["read"] != "y" && message["incoming"] !="y"
    data["IsDeleted"] = message["deleted"] == "y"
    data["Sender"] = message["sender"]
    data["details_available"] = true
    if message["messageId"] == message["conversationId"] || msg_type == "unread_message"
      data["message_detail"] =  ::Clinic::Tpp::Message.get_message_detail_from_list(message,message_list,msg_type, options)
      data["ReplyCount"] = data["message_detail"]["MessageReplies"].count rescue 0
      data["HasUnreadReplies"] = data["message_detail"]["HasUnreadReplies"]
      data["UnreadRepliesCount"] = data["message_detail"]["UnreadRepliesCount"]
    end
    data["LatestUpdatedAt"]  = data["message_detail"]["LatestUpdatedAt"] 
    data
  end

  
  # Clinic::Tpp::Message.get_message_detail(message_id, organization_uid,member_id,options)
  # def self.get_message_detail(message_id,organization_uid,member_id, options={})
  #   begin
  #     current_user = options[:current_user]
  #     read_unread_messages = ::Clinic::Tpp::Message.get_messages(organization_uid,member_id,options)
  #     recipient_list = (::Clinic::Tpp::Message.get_message_receiptent_list(current_user,member_id,options)[:recipients] || []) rescue []
  #     if read_unread_messages[:status] == 200
  #       message_list = (read_unread_messages[:messages][:read_message] || []) + (read_unread_messages[:messages][:unread_message] || [])
  #     else
  #       message_list = []
  #     end
  #     data1 = {}
  #     message =  message_list.select{|a| a["messageId"] == message_id}.first
  #      ::Clinic::Tpp::Message.formatted_massage(message,message_list,'read_message',options)
        
  #     if message.blank?
  #       return {status:100, :message=>"Message has been deleted"}
  #     end
  #     replies = message_list.select{|a| a["conversationId"] == message_id}
  #     reply_data = []
  #     data1["ClientApplicationName"] = nil
  #     data1["MessageId"] = message_id.to_s
  #     data1["Sender"] =  message["sender"]

  #     data1["Subject"] = nil
  #     data1["Content"] = message["messageText"]
  #     data1["SentDateTime"] = message["sent"].to_s.gsub('z','').gsub('Z','')
  #     data1["attachment_id"] =  message["binaryDataId"]

  #     data1["Recipients"] = []
  #     if message["recipient"].present?
  #       [message["recipient"]].flatten.each do |recipient|
  #         recipient_detail = recipient_list.select {|a| a["Name"] == recipient}.first
  #         if recipient_detail.present?
  #           data1["Recipients"] << recipient_detail
  #         end
  #       end
  #     end
  #     if data1["Recipients"].blank?
  #       data1["Recipients"] = {"Name": message["recipient"],"RecipientType":nil,"RecipientGuid":nil}
  #     end
  #     data1["IsDeleted"] = message["deleted"] == "y"
  #     replies.each do |reply_message|

  #       if reply_message["messageId"] != reply_message["conversationId"] && message["messageId"] != reply_message["messageId"]
  #         # msg_type == "read_message" ||
  #         if ( reply_message["read"] == "y" || reply_message["incoming"] =="y")
  #           reply_data << 
  #           { "Sender": reply_message["sender"],
  #             "SentDateTime": reply_message["sent"].to_s.gsub('z','').gsub('Z',''),
  #             "SentDateTimeStr": reply_message["sent"].to_s.gsub('z','').gsub('Z',''),
  #             "IsUnread": (reply_message["read"] != "y" && reply_message["incoming"] != "y" ),
  #             "ReplyContent": reply_message["messageText"] ,
  #             "IsLegacy": false ,
  #             "MessageId": reply_message["messageId"] ,
  #             "ReplyToMessageId": reply_message["conversationId"] ,
  #             "MessageType": nil,
  #             "attachment_id": reply_message["binaryDataId"]

  #           }
  #         end
  #       end
  #     end
  #     data1["HasUnreadReplies"] = reply_data.map{|a| a[:IsUnread]}.include?(true)
  #     data1["UnreadRepliesCount"] = reply_data.select{|a| a[:IsUnread] == true }.count
  #     data1["MessageReplies"] = reply_data.sort_by{|a| a[:SentDateTimeStr].to_time}#.reverse
  #     data1["RepliesAttachment"] = reply_data.select{|a| a[:attachment_id].present? }.count

  #     status = 200
  #     message = nil
  #   rescue Exception => e
  #     status = 100 
  #     data1 = nil
  #     message = ::Message::Tpp[:Messages][:unable_to_get_message_detail]
  #   end
  #   {status:status,message_detail:data1,message:message}
  # end

  # Not in use for now 
  #  options = {:current_user=>current_user,:start_date=>(Date.today-7.days).to_time,:end_date=>(Date.today+1.day).to_time }
  # ::Clinic::Tpp::Message.get_message_list(organization_uid,member_id,options)
  def self.get_message_list(organization_uid,member_id,options={})
    current_user = options[:current_user]
    member = Member.find(member_id)
   # <MessagesView apiVersion="1" unitId="C84710" patientId="71df200000000000" onlineUserId="75ee400000000000" startDate="2014-11-17T00:00:00.0Z" endDate="2014-12-05T00:00:00.0Z"></MessagesView>
    request_body =  ::Clinic::Tpp::Client.request_body_data(current_user,member_id,options)
    request_body.merge!({
      "startDate": (options[:start_date] || Date.today - 1.year).to_time.utc.iso8601,
      "endDate": (options[:end_date] || Date.today).to_time.utc.iso8601,
    })
    builder = Nokogiri::XML::Builder.new do |xml|
      xml.MessagesView(request_body) 
    end
    suid_required = true
    request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
    request_options = ({:request_type=>"Post", :request_uri=> "MessagesView", :request_body=>request_body})
    response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
    if status == 200
      if response["MessagesViewReply"]["Message"].present?
        message_list = response["MessagesViewReply"]["Message"]
        messages = message_list.select{|a| a["deleted"] != "y"}
      else
        messages = []
      end
    else
      error_msg = response["Error"]["technicalMessage"]
      return {status:status,message:error_msg, error:error_msg}
    end
  end


  # Clinic::Tpp::Message.get_message_detail(message_id, organization_uid,member_id,options)
  def self.get_message_detail_from_list(message,message_list,msg_type, options={})
    # ::Clinic::Tpp::Message.get_messages(organization_uid,member_id,options)
    data1 = {}
    replies = message_list.select{|a| a["conversationId"] == message["conversationId"]}
    reply_data = []
    data1["ClientApplicationName"] = nil
    data1["Sender"] =  message["sender"]
    data1["MessageId"] = message["conversationId"].to_s
    data1["Subject"] = nil
    data1["Content"] = message["messageText"]
    data1["SentDateTime"] = message["sent"].to_s.gsub('z','').gsub('Z','')
    data1["Recipients"] = {"Name": message["recipient"],"RecipientType":nil,"RecipientGuid":nil}
    data1["attachment_id"] = message["binaryDataId"]
    data1["IsDeleted"] = message["deleted"] == "y"
    replies.each do |reply_message|

      if reply_message["messageId"] != reply_message["conversationId"] && message["messageId"] != reply_message["messageId"]
        if (msg_type == "read_message" || reply_message["read"] == "y" || reply_message["incoming"] == "y")
          reply_data << 
          { "Sender": reply_message["sender"],
            "SentDateTime": reply_message["sent"].to_s.gsub('z','').gsub('Z',''),
            "SentDateTimeStr": reply_message["sent"].to_s.gsub('z','').gsub('Z',''),
            "IsUnread": (reply_message["read"] != "y" && reply_message["incoming"] != "y"),
            "ReplyContent": reply_message["messageText"] ,
            "IsLegacy": false ,
            "MessageId": reply_message["messageId"] ,
            "ReplyToMessageId": reply_message["conversationId"] ,
            "MessageType": nil,
            "attachment_id": reply_message["binaryDataId"]
          }
        end
      end
    end
    message_sent_received = reply_data.map{|a| a[:SentDateTimeStr]} + [message["sent"].to_s.gsub('z','')]
    message_updated_at  = message_sent_received.map(&:to_time).sort.last
    data1["HasUnreadReplies"] = reply_data.map{|a| a[:IsUnread]}.include?(true)
    data1["UnreadRepliesCount"] = reply_data.select{|a| a[:IsUnread] == true }.count
    data1["MessageReplies"] = reply_data.sort_by{|a| a[:SentDateTimeStr].to_time}#.reverse
    data1["RepliesAttachment"] = reply_data.select{|a| a[:attachment_id].present? }.count
    data1["LatestUpdatedAt"] = message_updated_at
    data1
  end
  

  def self.get_message_detail(message_id,organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      member = Member.find(member_id)
      request_body =  ::Clinic::Tpp::Client.request_body_data(current_user,member_id,options)
      request_body.merge!({
        "startDate": (options[:start_date] || Date.today - 1.year).to_time.utc.iso8601,
        "endDate": (options[:end_date] || Date.today).to_time.utc.iso8601,
      })
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.MessagesView(request_body) 
      end
      suid_required = true
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options = ({:request_type=>"Post", :request_uri=> "MessagesView", :request_body=>request_body})
       response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
      # response,status = [ {"MessagesViewReply"=>{"patientId"=>"2619300000000000", "onlineUserId"=>"2619300000000000", "uuid"=>"271DC32A-A9F3-4F3B-8E5A-EC395069A665", "Message"=>[{"messageId"=>"0b21000000000000", "sent"=>"2020-01-20T07:15:17.0Z", "staffOrGroupOrTeamName"=>"Dr Jack Christopher", "messageText"=>"The practice has granted ALTER, Idan (Mr) access to the following services:\\r\nFull Clinical Record", "read"=>"y", "conversationId"=>"0b21000000000000", "sender"=>"Dr Jack Christopher", "recipient"=>"Mr Idan Alter"}, {"messageId"=>"ba21000000000000", "sent"=>"2020-01-21T08:14:43.0Z", "staffOrGroupOrTeamName"=>"Admin Nurturey", "messageText"=>"ok", "conversationId"=>"0b21000000000000", "sender"=>"Admin Nurturey", "recipient"=>"Mr Idan Alter"}, {"messageId"=>"3b21000000000000", "sent"=>"2020-01-21T11:51:49.0Z", "staffOrGroupOrTeamName"=>"Dr Jack Christopher", "incoming"=>"y", "conversationId"=>"3b21000000000000", "sender"=>"Mr Idan Alter", "recipient"=>"Dr Jack Christopher"}, {"messageId"=>"0c21000000000000", "sent"=>"2020-01-21T11:51:05.0Z", "staffOrGroupOrTeamName"=>"Dr Jack Christopher", "incoming"=>"y", "conversationId"=>"0c21000000000000", "sender"=>"Mr Idan Alter", "recipient"=>"Dr Jack Christopher"}, {"messageId"=>"7a21000000000000", "sent"=>"2020-01-21T11:42:34.0Z", "staffOrGroupOrTeamName"=>"Dr Jack Christopher", "incoming"=>"y", "messageText"=>"Hi te", "conversationId"=>"7a21000000000000", "sender"=>"Mr Idan Alter", "recipient"=>"Dr Jack Christopher"}, {"messageId"=>"73a1000000000000", "sent"=>"2020-02-04T10:38:02.0Z", "incoming"=>"y", "messageText"=>"Hello", "conversationId"=>"73a1000000000000", "sender"=>"Mr Idan Alter", "recipient"=>"HSCIC Unsupported Test Environment V"}, {"messageId"=>"82a1000000000000", "sent"=>"2020-02-06T13:30:36.0Z", "staffOrGroupOrTeamName"=>"Dr Jack Christopher", "messageText"=>"HI Idan, how are you?", "read"=>"y", "conversationId"=>"23a1000000000000", "sender"=>"Dr Jack Christopher", "recipient"=>"Mr Idan Alter"}, {"messageId"=>"83a1000000000000", "sent"=>"2020-02-06T05:32:04.0Z", "staffOrGroupOrTeamName"=>"Dr Jack Christopher", "incoming"=>"y", "conversationId"=>"83a1000000000000", "sender"=>"Mr Idan Alter", "recipient"=>"Dr Jack Christopher"}, {"messageId"=>"93a1000000000000", "sent"=>"2020-02-06T05:35:22.0Z", "staffOrGroupOrTeamName"=>"Dr Jack Christopher", "incoming"=>"y", "conversationId"=>"93a1000000000000", "sender"=>"Mr Idan Alter", "recipient"=>"Dr Jack Christopher"}, {"messageId"=>"23a1000000000000", "sent"=>"2020-02-06T13:30:01.0Z", "staffOrGroupOrTeamName"=>"Dr Jack Christopher", "incoming"=>"y", "read"=>"y", "conversationId"=>"23a1000000000000", "sender"=>"Mr Idan Alter", "recipient"=>"Dr Jack Christopher"}, {"messageId"=>"33a1000000000000", "sent"=>"2020-02-10T07:56:11.0Z", "staffOrGroupOrTeamName"=>"Dr Jack Christopher", "incoming"=>"y", "conversationId"=>"33a1000000000000", "sender"=>"Mr Idan Alter", "recipient"=>"Dr Jack Christopher"}, {"messageId"=>"b1a1000000000000", "sent"=>"2020-02-10T09:06:31.0Z", "staffOrGroupOrTeamName"=>"Dr Jack Christopher", "messageText"=>"Hello Idan how are you?", "conversationId"=>"43a1000000000000", "sender"=>"Dr Jack Christopher", "recipient"=>"Mr Idan Alter"}, {"messageId"=>"c1a1000000000000", "sent"=>"2020-02-10T11:11:12.0Z", "staffOrGroupOrTeamName"=>"Dr Jack Christopher", "messageText"=>"hello2", "conversationId"=>"43a1000000000000", "sender"=>"Dr Jack Christopher", "recipient"=>"Mr Idan Alter"}, {"messageId"=>"43a1000000000000", "sent"=>"2020-02-10T08:05:43.0Z", "staffOrGroupOrTeamName"=>"Dr Jack Christopher", "incoming"=>"y", "messageText"=>"Test 2 feb10", "read"=>"y", "conversationId"=>"43a1000000000000", "sender"=>"Mr Idan Alter", "recipient"=>"Dr Jack Christopher"}]}},200]
      if status == 200
        if response["MessagesViewReply"]["Message"].present?
          message_list = response["MessagesViewReply"]["Message"]
          messages = message_list.select{|a| a["deleted"] != "y"}
          messages = [messages].flatten
          orginal_message = messages.select{|a| a["messageId"] == message_id && a["messageId"] == a["conversationId"] }
          messages = orginal_message.map{|message| ::Clinic::Tpp::Message.formatted_massage(message,message_list,'read_message',options)}.sort_by{|message| message["LatestUpdatedAt"]}.reverse
          message_detail = messages.first["message_detail"]
        
        else
          message_detail  = nil
          message =  ::Message::Tpp[:Messages][:unable_to_get_message_detail]
        end
        return {status:200,message:message  ,message_detail:message_detail}
      else
        error_msg =  ::Message::Tpp[:Messages][:unable_to_get_message_detail]
        return {status:status,message:error_msg, error:error_msg}
      end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @getMessage_retry_count = @getMessage_retry_count.to_i + 1 
      retry if @getMessage_retry_count.to_i <= 2
      return {status:100, error:e.message, :message=> ::Message::Tpp[:Messages][:unable_to_get_message_detail]}
    end
  end

  #  options = {:current_user=>current_user,:start_date=>(Date.today-7.days).to_time,:end_date=>(Date.today+1.day).to_time }
  # ::Clinic::Tpp::Message.get_messages(organization_uid,member_id,options)
  def self.get_messages(organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      member = Member.find(member_id)
     # <MessagesView apiVersion="1" unitId="C84710" patientId="71df200000000000" onlineUserId="75ee400000000000" startDate="2014-11-17T00:00:00.0Z" endDate="2014-12-05T00:00:00.0Z"></MessagesView>
      request_body =  ::Clinic::Tpp::Client.request_body_data(current_user,member_id,options)
      request_body.merge!({
        "startDate": (options[:start_date] || Date.today - 1.year).to_time.utc.iso8601,
        "endDate": (options[:end_date] || Date.today).to_time.utc.iso8601,
      })
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.MessagesView(request_body) 
      end
      suid_required = true
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options = ({:request_type=>"Post", :request_uri=> "MessagesView", :request_body=>request_body})
       response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
      # response,status = [ {"MessagesViewReply"=>{"patientId"=>"2619300000000000", "onlineUserId"=>"2619300000000000", "uuid"=>"271DC32A-A9F3-4F3B-8E5A-EC395069A665", "Message"=>[{"messageId"=>"0b21000000000000", "sent"=>"2020-01-20T07:15:17.0Z", "staffOrGroupOrTeamName"=>"Dr Jack Christopher", "messageText"=>"The practice has granted ALTER, Idan (Mr) access to the following services:\\r\nFull Clinical Record", "read"=>"y", "conversationId"=>"0b21000000000000", "sender"=>"Dr Jack Christopher", "recipient"=>"Mr Idan Alter"}, {"messageId"=>"ba21000000000000", "sent"=>"2020-01-21T08:14:43.0Z", "staffOrGroupOrTeamName"=>"Admin Nurturey", "messageText"=>"ok", "conversationId"=>"0b21000000000000", "sender"=>"Admin Nurturey", "recipient"=>"Mr Idan Alter"}, {"messageId"=>"3b21000000000000", "sent"=>"2020-01-21T11:51:49.0Z", "staffOrGroupOrTeamName"=>"Dr Jack Christopher", "incoming"=>"y", "conversationId"=>"3b21000000000000", "sender"=>"Mr Idan Alter", "recipient"=>"Dr Jack Christopher"}, {"messageId"=>"0c21000000000000", "sent"=>"2020-01-21T11:51:05.0Z", "staffOrGroupOrTeamName"=>"Dr Jack Christopher", "incoming"=>"y", "conversationId"=>"0c21000000000000", "sender"=>"Mr Idan Alter", "recipient"=>"Dr Jack Christopher"}, {"messageId"=>"7a21000000000000", "sent"=>"2020-01-21T11:42:34.0Z", "staffOrGroupOrTeamName"=>"Dr Jack Christopher", "incoming"=>"y", "messageText"=>"Hi te", "conversationId"=>"7a21000000000000", "sender"=>"Mr Idan Alter", "recipient"=>"Dr Jack Christopher"}, {"messageId"=>"73a1000000000000", "sent"=>"2020-02-04T10:38:02.0Z", "incoming"=>"y", "messageText"=>"Hello", "conversationId"=>"73a1000000000000", "sender"=>"Mr Idan Alter", "recipient"=>"HSCIC Unsupported Test Environment V"}, {"messageId"=>"82a1000000000000", "sent"=>"2020-02-06T13:30:36.0Z", "staffOrGroupOrTeamName"=>"Dr Jack Christopher", "messageText"=>"HI Idan, how are you?", "read"=>"y", "conversationId"=>"23a1000000000000", "sender"=>"Dr Jack Christopher", "recipient"=>"Mr Idan Alter"}, {"messageId"=>"83a1000000000000", "sent"=>"2020-02-06T05:32:04.0Z", "staffOrGroupOrTeamName"=>"Dr Jack Christopher", "incoming"=>"y", "conversationId"=>"83a1000000000000", "sender"=>"Mr Idan Alter", "recipient"=>"Dr Jack Christopher"}, {"messageId"=>"93a1000000000000", "sent"=>"2020-02-06T05:35:22.0Z", "staffOrGroupOrTeamName"=>"Dr Jack Christopher", "incoming"=>"y", "conversationId"=>"93a1000000000000", "sender"=>"Mr Idan Alter", "recipient"=>"Dr Jack Christopher"}, {"messageId"=>"23a1000000000000", "sent"=>"2020-02-06T13:30:01.0Z", "staffOrGroupOrTeamName"=>"Dr Jack Christopher", "incoming"=>"y", "read"=>"y", "conversationId"=>"23a1000000000000", "sender"=>"Mr Idan Alter", "recipient"=>"Dr Jack Christopher"}, {"messageId"=>"33a1000000000000", "sent"=>"2020-02-10T07:56:11.0Z", "staffOrGroupOrTeamName"=>"Dr Jack Christopher", "incoming"=>"y", "conversationId"=>"33a1000000000000", "sender"=>"Mr Idan Alter", "recipient"=>"Dr Jack Christopher"}, {"messageId"=>"b1a1000000000000", "sent"=>"2020-02-10T09:06:31.0Z", "staffOrGroupOrTeamName"=>"Dr Jack Christopher", "messageText"=>"Hello Idan how are you?", "conversationId"=>"43a1000000000000", "sender"=>"Dr Jack Christopher", "recipient"=>"Mr Idan Alter"}, {"messageId"=>"c1a1000000000000", "sent"=>"2020-02-10T11:11:12.0Z", "staffOrGroupOrTeamName"=>"Dr Jack Christopher", "messageText"=>"hello2", "conversationId"=>"43a1000000000000", "sender"=>"Dr Jack Christopher", "recipient"=>"Mr Idan Alter"}, {"messageId"=>"43a1000000000000", "sent"=>"2020-02-10T08:05:43.0Z", "staffOrGroupOrTeamName"=>"Dr Jack Christopher", "incoming"=>"y", "messageText"=>"Test 2 feb10", "read"=>"y", "conversationId"=>"43a1000000000000", "sender"=>"Mr Idan Alter", "recipient"=>"Dr Jack Christopher"}]}},200]
      if status == 200
        if response["MessagesViewReply"]["Message"].present?
          message_list = response["MessagesViewReply"]["Message"]
          

          messages = message_list.select{|a| a["deleted"] != "y"}
          messages = [messages].flatten
          orginal_message = messages.select{|a| a["messageId"] == a["conversationId"] }
          
          messages = orginal_message.map{|message| ::Clinic::Tpp::Message.formatted_massage(message,message_list,'read_message',options)}.sort_by{|message| message["LatestUpdatedAt"]}.reverse
          
          read_message = messages.select{|message| !message["HasUnreadReplies"]}
          unread_message = messages.select{|message|  message["HasUnreadReplies"]}
          
          read_message  = read_message.sort_by{|message| message["LatestUpdatedAt"] }.reverse
          unread_message  = unread_message.sort_by{|message| message["LatestUpdatedAt"] }.reverse
        else
          read_message = nil
          unread_message = nil
        end
        return {status:200,  messages:{:read_message=>read_message,:unread_message=>unread_message }}
      else
        error_msg = response["Error"]["technicalMessage"]
        return {status:status,message:error_msg, error:error_msg}
      end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @getMessage_retry_count = @getMessage_retry_count.to_i + 1 
      retry if @getMessage_retry_count.to_i <= 2
      return {status:100, error:e.message, :message=>::Message::Tpp[:Messages][:unable_to_get_message]}
    end
  end

 

   # Emis::Message.delete_message(message_id, organization_uid,member_id,options)
  def self.delete_message(message_id,organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      member = Member.find(member_id)
     # <MessagesView apiVersion="1" unitId="C84710" patientId="71df200000000000" onlineUserId="75ee400000000000" startDate="2014-11-17T00:00:00.0Z" endDate="2014-12-05T00:00:00.0Z"></MessagesView>
      request_body =  ::Clinic::Tpp::Client.request_body_data(current_user,member_id,options)
      # request_body.merge!({messageId:message_id})
      # delete entire message thread
      request_body.merge!({conversationId:message_id})
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.MessageDelete(request_body) 
      end
      suid_required = true
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options = ({:request_type=>"Post", :request_uri=> "MessageDelete", :request_body=>request_body})
      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
     
      if (status != 200)
        return {error:response["Error"]["technicalMessage"],message:response["Error"]["technicalMessage"], status:status}
      else
        @retry_count = 0
        return {:message=>::Message::Tpp[:Messages][:message_deleted], status:200}
      end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      return {error:e.message, :message=>::Message::Tpp[:Messages][:unable_to_delete_message], status:100}
    end
  end


  def self.format_tpp_receipient_data(recipient,options={})
    # Recipients\":[{"Name":"TEST, Nurturey (Dr)","RecipientType":"User","RecipientGuid":"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}]}]
    {"Name"=>recipient["content"],"RecipientType"=>recipient["description"],"RecipientGuid"=>recipient["id"]}
  end

  #::Clinic::Tpp::Message.get_message_receiptent_list(current_user,member_id,options)
  def self.get_message_receiptent_list(current_user,member_id,options={})
    begin
      current_user = options[:current_user]
      member = Member.find(member_id)
       
      # <MessageRecipients apiVersion="1" unitId="C84710" patientId="71df200000000000" onlineUserId="75ee400000000000"></MessageRecipients>
      request_body =  ::Clinic::Tpp::Client.request_body_data(current_user,member_id,options)
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.MessageRecipients(request_body) 
      end
      suid_required = true
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options = ({:request_type=>"Post", :request_uri=> "MessageRecipients", :request_body=>request_body})
      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
      if (status != 200)
        return {message:response["technicalMessage"] ,error:response["technicalMessage"], status:100}
      else
        recipients = response["Item"].group_by{|a| a["id"]}
        usual_gp_recipients = response["Item"].select{|c| c["description"] == "UnitRecipient"}.group_by{|a| a["id"]}
        recipients = recipients.merge(usual_gp_recipients).values.flatten
        recipients = recipients.map{|a| ::Clinic::Tpp::Message.format_tpp_receipient_data(a,options)}
        recipients = recipients.select{|a| a["RecipientType"] == "Recipient"}
        @retry_count = 0
        return {:recipients=>recipients, status:200}
      end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      return {error: e.message, message:response["technicalMessage"], status:100}
    end
  end
  
  def self.update_message_read_status(message_id,message_read_state,member_id,options={})
    begin
      current_user = options[:current_user]
      member = Member.find(member_id)
      # <MessageMarkAsRead apiVersion="1" unitId="C84710" patientId="71df200000000000" onlineUserId="75ee400000000000"> <Message messageId="ab00000000000000"></Message>
      # <Message messageId="cd00000000000000"></Message>   </MessageMarkAsRead>
      message_ids = [message_id].compact.flatten
      request_body =  ::Clinic::Tpp::Client.request_body_data(current_user,member_id,options)
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.MessageMarkAsRead(request_body) do 
          message_ids.map do |msg_id|
            xml.Message({messageId:msg_id})
          end
        end
      end
      suid_required = true
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options = ({:request_type=>"Post", :request_uri=> "MessageMarkAsRead", :request_body=>request_body})
      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
      
      if (status != 200)
        return {message:response["Error"]["technicalMessage"], status:100}
      else
        @retry_count = 0
        return {:message=>::Message::Tpp[:Messages][:message_read_status_updated], status:200}
      end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      return {:message=>::Message::Tpp[:Messages][:message_read_status_not_updated], error:e.message, status:100}
    end
  end
  
  #  not in use:options = {:current_user=>current_user, recipients{:recipientId=>'e546410000000000',:conversationId=>''}}
  # options ={:current_user=>current_user,:recipients=>["10xn342cuywekdsu09"]}
  # ::Clinic::Tpp::Message.send_message(subject,msg,organization_uid,member_id,options)
  def self.send_message(subject,msg,organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      member = Member.find(member_id)
      recipient_id = [options[:recipients]].flatten.first
       
     # <MessageCreate apiVersion="1" unitId="X99999" patientId="71df200000000000" onlineUserId="75ee400000000000" recipientId="ffff000000000000" message="test message" uuid="88888888-8888-8888-8888-888888888888" conversationId="7af5100000000000"></MessageCreate>
      request_body =  ::Clinic::Tpp::Client.request_body_data(current_user,member_id,options)
      request_body.merge!({recipientId:recipient_id,message:msg})
      request_body[:conversationId] = options[:conversation_id] if options[:conversation_id].present?
      file_data = options[:file_data]
      file_name = options[:file_name]
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.MessageCreate(request_body){
          if file_data.present?
            xml.BinaryData(fileName:file_name){
              xml.BinaryDataPage({binaryDataValue:file_data})
            }
          end
        }
      end
      suid_required = true
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options = ({:request_type=>"Post", :request_uri=> "MessageCreate", :request_body=>request_body})
      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
   
     if (status != 200)
        return {:message=>response["Error"]["technicalMessage"] , error:response["Error"]["technicalMessage"], status:100}
      else
        @retry_count = 0
        if options[:conversation_id].present?
          message_list_response = ::Clinic::Tpp::Message.get_message_detail(options[:conversation_id], organization_uid,member_id,options)
          message_detail = message_list_response[:message_detail]
        else
          message_detail = nil
        end
        return {:message_detail=>message_detail, :message=>::Message::Tpp[:Messages][:message_sent], status:200}
      end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      return {:message=>::Message::Tpp[:Messages][:message_not_sent],error:e.message, status:100}
    end
  end


  def self.get_message_attachment(member_id,binary_data_id,options={})
    begin
      current_user = options[:current_user]
      member = Member.find(member_id)
      request_body =  ::Clinic::Tpp::Client.request_body_data(current_user,member_id,options)
      request_body.merge!({binaryDataId:binary_data_id})
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.RequestBinaryData(request_body) 
      end
      suid_required = true
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options = ({:request_type=>"Post", :request_uri=> "RequestBinaryData", :request_body=>request_body})
      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
      # response["BinaryData"][0]["BinaryDataPage"]
      
      if (status != 200)
        return {message:response["technicalMessage"] ,error:response["technicalMessage"], status:100}
      else
        @retry_count = 0
        return {:file_data=>response["BinaryData"][0], status:200}
      end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      return {:message=>::Message::Tpp[:Messages][:message_not_sent],error:e.message, status:100}
    end
  end
end