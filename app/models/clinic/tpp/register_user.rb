class Clinic::Tpp::RegisterUser
  def self.get_proxyUser(organization_uid,member_id,options={})
    begin
      proxy_users = []
      data = []
      current_user = options[:current_user]
      clinic_name = Clinic::ClinicDetail.where(organization_uid:current_user.organization_uid).last.name rescue ""
      clinic_link_detail =  ::Clinic::LinkDetail.where(member_id:current_user.id).last
      user_details = clinic_link_detail.link_info.with_indifferent_access
      begin
        authentication_details = {account_id:user_details[:account_id],:passphrase=>user_details[:passphrase],practice_ods_code:user_details[:practice_ods_code]}
        TppLogger.info "................Make authenticate account request.."
        authentication_response = ::Clinic::Tpp::Login.authenticate_account(current_user,member_id,authentication_details,options)
        if authentication_response["error"] ==  "There was a problem logging on"
          raise  authentication_response["error"]
        end
      rescue Exception => e
        TppLogger.info ".............Error inside get_proxyUser...#{e.message}........."
        @get_proxy_retry_count = @get_proxy_retry_count.to_i + 1
        link_account = ::Clinic::Tpp::Login.link_account(current_user,member_id,user_details,options)
        user_details[:passphrase] = link_account[:data]["passphrase"]
        # retry if @get_proxy_retry_count < 2
      end
      TppLogger.info "................Account authenticated.."

      proxy_users = authentication_response["data"]["Registration"]["PatientAccess"]
      patient_info = authentication_response["data"]['Person'].group_by{|a| a["patientId"]}
      proxy_users = proxy_users.reject{|a| a["isSelfAccess"] == "y"}
      if proxy_users.present?
        proxy_users.each do |proxy_user|
          patient_id = proxy_user["patientId"]
          link_detail = ::Clinic::LinkDetail.where(parent_member_id:current_user.id,patient_uid: patient_id).last
          linked = link_detail.present?
          linked_member_id = link_detail.member_id rescue nil
          user_info = patient_info[patient_id].last
          full_name_with_title = user_info['PersonName']['name'].to_s
          first_name,last_name = Clinic::Tpp::RegisterUser.get_first_and_last_name(full_name_with_title,options)
          # full_name = full_name_with_title.gsub(/Mr |Mrs |Miss | Master |Ms |Mx |Dr /,"").strip 
          # last_name = full_name.split(" ").last.strip
          # first_name = full_name.gsub(last_name,"").strip
          data << {:full_name=>full_name_with_title ,
                  :first_name => first_name,
                  :last_name =>last_name,
                  :age_value=> (((Date.today - (user_info['dateOfBirth'].to_date))/365).to_i rescue nil),
                  :dob=> (user_info['dateOfBirth'].to_date.to_date6 rescue nil),
                  :age=> ApplicationController.helpers.calculate_age(user_info['dateOfBirth']),
                  :gender => user_info['gender'],
                  :clinic_name=>proxy_user['usualGp'],
                  :ods_code=> authentication_response["data"]["Registration"]["unitId"],
                  :proxy_active_status=>true,
                  :patient_uid=> patient_id,
                  :association_type=>proxy_user["relationship"],
                  :proxy_linked=>linked,
                  :proxy_linked_member_id=>linked_member_id
                  }
          
        end
      else
        options[:ip_address] = current_user.gpsoc_api_requested_ip
        report_event_custom_id = 290 #Report::Event.where(name:"No proxy list found for linked account").last
        ::Report::ApiLog.log_event(current_user.user_id,options[:family_id],report_event_custom_id,options) rescue nil
      end
    rescue Exception=>e 
      TppLogger.info "................Error inside get_proxyUser.....#{e.message}.."
      Rails.logger.info e.message
    end
    data
  end

  def self.get_first_and_last_name(full_name,options={})
    begin
      
      full_name = full_name.gsub(/Mr |Mrs |Miss | Master |Ms |Mx |Dr /,"").strip 
      last_name = full_name.split(" ").last.strip
      first_name = full_name.gsub(last_name,"").strip
    rescue Exception => e
      first_name =   full_name
      last_name = nil
      TppLogger.info "Error inside get_first_and_last_name ......."    
    end
    [first_name,last_name]
  end
  
  def self.register_with_patient_detail(registeration_detail, options={})
  end
  
  # pinDocument = {account_id:364823672,:account_linkage_key=>'\XY7&t66MNaqcsr6',surname:'Alter',date_of_birth:'1954-09-16',practice_ods_code:'HSCIC22'}
  def self.register_with_pin_document(pinDocument,options={})
    member_id = options[:member_id]
    current_user = options[:current_user]
    user_details = pinDocument
    # ::Clinic::Tpp::Login.link_account(current_user,member_id,user_details,options)
    response = ::Clinic::Tpp::Login.get_session_id(current_user,member_id,user_details,options)
    report_event_custom_id = 295 #GP online user linking with linkage key
    status_text = response[:status] == 200 ? "success" : "failed"
    ::Report::ApiLog.log_event(current_user.user_id,options[:family_id],report_event_custom_id,options.merge({:status=>status_text})) rescue nil
    response
  end
    
end