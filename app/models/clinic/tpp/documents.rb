class Clinic::Tpp::Documents
  
  # Emis::Prescribing.user_nominated_pharmacy(member_id,options)
  def self.list_document(member_id,options={})
    begin
      current_user = options[:current_user]
      options[:item_type] = 'TestResultsView'
      response  = ::Clinic::Tpp::HealthCard.get_patient_health_record(member_id,options)
      if response[:status] == 200
        Document.where(member_id:member_id,data_source:"tpp").delete_all
        formated_data = []
        response[:data].each do |date,documents|
          documents.each do |document|
            if document[:html_content].present?
              file_name = document[:id]
              document_guid = document[:id]
              last_modified = date
              Document.new(html_content:document[:html_content], created_at:last_modified, last_modified:last_modified, file_updated_at:last_modified, file_file_name: file_name,file_content_type:"html",file_file_size:nil,  uid:document_guid,member_id:member_id,
                           data_source:"tpp", name:file_name,document_type:document[:type]).save
              formated_data << {html_content:document[:html_content],:record_uid=>document_guid,:record_date=>(last_modified.to_date rescue nil), :member_id=>member_id,:name=>file_name, :value=> file_name,:data_source=>"tpp",:added_by=>"system",:updated_at=>(last_modified.to_date.to_date7 rescue nil) }
            end
          end
        end
        @retry_count = 0
        {data:formated_data, status:200}
      else
        response
      end

    rescue Exception=>e 
      return {message:response["Message"], status:100}
    end
  end
end