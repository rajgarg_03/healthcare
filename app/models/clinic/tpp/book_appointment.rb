class Clinic::Tpp::BookAppointment
  # Clinic::Tpp::BookAppointment.get_doctor_list(organization_uid,member_id,options)
  # options = {:current_user=>current_user,:start_date=>(Date.today),:number_of_days=>15}
  def self.get_doctor_list(organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]

      
      # <ListSlots apiVersion="1" unitId="C84010" patientId="71df200000000000" onlineUserId="75ee400000000000" startDate="2014-11-25T00:00:00.0Z" numberOfDays="15" uuid="88888888-8888-8888-8888-888888888888"></ListSlots>
      numberOfDays =  15
      start_date = (options[:start_date] || Date.today).to_time.utc.iso8601
      no_of_days = (options[:number_of_days] || 15).to_i
      request_body =  ::Clinic::Tpp::Client.request_body_data(current_user,member_id,options)
      request_body.merge!({startDate:start_date, "numberOfDays": no_of_days })
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.ListSlots(request_body) 
      end

      doctor_list =  nil
      suid_required = true
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options = ({:request_type=>"Post", :request_uri=> "ListSlots", :request_body=>request_body})
      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
      # response,status = [{"ListSlotsReply"=>{"patientId"=>"71df200000000000", "onlineUserId"=>"75ee400000000000", "uuid"=>"88888888-8888-8888-8888-888888888888", "bookableDays"=>"21", "Session"=>[{"sessionId"=>"c508b00000000000", "type"=>"Session", "staffId"=>"1fc1000000000000", "staffName"=>"Mr Mark Ramsden", "staffGender"=>"Not set", "Site"=>{"name"=>"The View Surgery", "telephoneNumber"=>"01132050080", "Address"=>{"line1"=>"Mill House", "line2"=>"54 Troy Road", "line3"=>"Horsforth", "line4"=>"Leeds", "line5"=>"LS18 5TN"}}, "Slot"=>[{"startDate"=>"2014-11-25T13:45:00.0Z", "endDate"=>"2014-11-25T13:55:00.0Z", "type"=>"Slot", "confirm"=>"y"}, {"startDate"=>"2014-11-25T13:55:00.0Z", "endDate"=>"2014-11-25T14:05:00.0Z", "type"=>"Slot", "confirm"=>"y"}, {"startDate"=>"2014-11-25T14:05:00.0Z", "endDate"=>"2014-11-25T14:15:00.0Z", "type"=>"Slot", "confirm"=>"y"}, {"startDate"=>"2014-11-25T14:15:00.0Z", "endDate"=>"2014-11-25T14:25:00.0Z", "type"=>"Slot", "confirm"=>"y"}]}, {"sessionId"=>"d508b00000000000", "name"=>"Asthma Smoking Clinic", "type"=>"Asthma Clinic", "staffId"=>"2da2000000000000", "staffName"=>"Simon Smith", "staffGender"=>"Not set", "Site"=>{"name"=>"The View Surgery", "telephoneNumber"=>"01132050080", "Address"=>{"address"=>"Mill House, 54 Troy Road, Horsforth, Leeds, LS18 5TN"}}, "Slot"=>[{"startDate"=>"2014-11-25T13:45:00.0Z", "endDate"=>"2014-11-25T14:00:00.0Z", "type"=>"Slot", "confirm"=>"y"}, {"startDate"=>"2014-11-25T14:00:00.0Z", "endDate"=>"2014-11-25T14:15:00.0Z", "type"=>"Slot", "confirm"=>"y"}, {"startDate"=>"2014-11-25T14:15:00.0Z", "endDate"=>"2014-11-25T14:30:00.0Z", "type"=>"Slot", "confirm"=>"y"}, {"startDate"=>"2014-11-25T14:30:00.0Z", "endDate"=>"2014-11-25T14:45:00.0Z", "type"=>"Slot", "confirm"=>"y"}, {"startDate"=>"2014-11-25T14:45:00.0Z", "endDate"=>"2014-11-25T15:00:00.0Z", "type"=>"Slot", "confirm"=>"y"}]}]}},200]
      if status == 200 
        doctor_list = []
        appointment_data = []
        session_list = [response["ListSlotsReply"]["Session"]].flatten
        session_list.each do |session_detail|
          clinician_data = ::Clinic::Tpp::BookAppointment.formatted_clinician_data(session_detail)
          doctor_list <<  clinician_data
          appointment_data << ::Clinic::Tpp::BookAppointment.formatted_slot_data(session_detail,options)
        end
        
        appointment_data = appointment_data.flatten
        if doctor_list.present?
          doctor_data = []
          doctor_list.group_by{|a| a[:name]}.each do  |doctor_name,detail|
            doctor_data << detail.first
          end
          doctor_list = doctor_data
        end
        if doctor_list.present?
          doctor_list.each do |doctor_detail|
            available_appointments = appointment_data.select{|a| a[:clinician_id] == doctor_detail[:clinician_id]}.group_by{|a| a[:date]}
            slots_count_with_appointment_date = available_appointments.map {|appointment_date,slots| {appointment_date=>slots.count} }.inject(:merge)
            sorted_available_appointments = available_appointments.keys.map(&:to_s)
            slots_data = {
              :available_appointments=>available_appointments,
              :sorted_available_appointments=> sorted_available_appointments,
              :slots_count_with_appointment_date=>slots_count_with_appointment_date
            }
            doctor_detail[:slots] = slots_data
          end
        end
      elsif status == 403 
        doctor_list = nil
      else
        raise 'Error in Get Doctor List appointment' 
      end
       
    rescue Exception => e 
      TppLogger.info  ".....Error inside .........ListSlots................................................."
      TppLogger.info  ".....#{e.message}................................................"
      doctor_list =  nil
    end
    # if doctor_list.present?
    #   doctor_data = []
    #   doctor_list.group_by{|a| a[:name]}.each do  |doctor_name,detail|
    #     doctor_data << detail.first
    #   end
    #   doctor_list = doctor_data
    # end
    doctor_list
  end
   
  def self.formatted_clinician_data(clinician_datail,options={})
    #clinician_datail =  "ClinicianId"=>3386, "DisplayName"=>"TEST, ATTechnology (Mr)", "Forenames"=>"ATTechnology", "Surname"=>"Test", "Title"=>"Mr", "Sex"=>"Male", "JobRole"=>"General Medical Practitioner", "Languages"=>[]}
    clinician_name = ::Clinic::Tpp::BookAppointment.get_clinician_name(clinician_datail)
    clinician_id = clinician_name
    return {} if clinician_datail.blank?
      data  = { :clinician_id => clinician_id,
      session_id:clinician_datail["sessionId"].to_s, 
      :name=>clinician_name,
      :title => clinician_datail["Title"],
      :gender => (clinician_datail["staffDetails"].include?("Female")? "Female": "Male"),
      :type => nil,
      :specialization=> nil,
      :languages => nil
    }
    data
  end

  def self.get_clinician_name(clinician_datail)
    clinician_name = (clinician_datail["staffDetails"].to_s.gsub('(Female)','').gsub('(Male)','')).strip
  end
  
  def self.formatted_slot_data(session_detail,options={})
    begin
      data = []
      session_id = session_detail["sessionId"]
      clinician_name = (::Clinic::Tpp::BookAppointment.get_clinician_name(session_detail))
      clinician_id = clinician_name
      session_detail["Slot"].each do |slot_detail|
       data << {
          :start_time=>slot_detail["startDate"].to_time.strftime("%H:%M"),
          :date => slot_detail["startDate"].to_date,
          :end_time=>slot_detail["endDate"].to_time.strftime("%H:%M"), 
          :id=>session_id.to_s,
          :appointment_id=>session_id.to_s,
          :slot_id=>session_id.to_s,
          :clinician_name=>clinician_name,
          :clinician_id => clinician_id,
          :clinician_gender => (session_detail["staffDetails"].include?("Female")? "Female": "Male")
        }
      end
    rescue Exception =>e 
      puts e.message 
      Rails.logger.info e.message
      data = nil
    end
    data
  end      
 
  # options = {:request_data => {:ClinicianId=>"1fc1000000000000", :FromDateTime=>(Time.now + 7.day), :ToDateTime=>(Time.now + 8.day)}}
  def self.get_available_appointments(organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      request_data = options[:request_data] || {}
      start_date = (request_data[:FromDateTime] || Date.today).to_time.utc.iso8601
      status = StatusCode::Status200
      
      appointment_data =  []
       # <ListSlots apiVersion="1" unitId="C84010" patientId="71df200000000000" onlineUserId="75ee400000000000" startDate="2014-11-25T00:00:00.0Z" numberOfDays="15" uuid="88888888-8888-8888-8888-888888888888"></ListSlots>
      options[:request_data][:FromDateTime] = options[:request_data][:FromDateTime] || (Date.today)
      numberOfDays =  (options[:request_data][:ToDateTime].to_date - options[:request_data][:FromDateTime].to_date).to_i rescue 15
      request_body =  ::Clinic::Tpp::Client.request_body_data(current_user,member_id,options)
      request_body.merge!({startDate:options[:request_data][:FromDateTime].to_time.utc.iso8601,
        "numberOfDays": numberOfDays.to_i
      })
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.ListSlots(request_body) 
      end
      suid_required = true
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options = {:request_type=>"Post", :request_uri=> "ListSlots", :request_body=>request_body}

      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
      # response,status = [{"ListSlotsReply"=>{"patientId"=>"71df200000000000", "onlineUserId"=>"75ee400000000000", "uuid"=>"88888888-8888-8888-8888-888888888888", "bookableDays"=>"21", "Session"=>[{"sessionId"=>"c508b00000000000", "type"=>"Session", "staffId"=>"1fc1000000000000", "staffName"=>"Mr Mark Ramsden", "staffGender"=>"Not set", "Site"=>{"name"=>"The View Surgery", "telephoneNumber"=>"01132050080", "Address"=>{"line1"=>"Mill House", "line2"=>"54 Troy Road", "line3"=>"Horsforth", "line4"=>"Leeds", "line5"=>"LS18 5TN"}}, "Slot"=>[{"startDate"=>"2014-11-25T13:45:00.0Z", "endDate"=>"2014-11-25T13:55:00.0Z", "type"=>"Slot", "confirm"=>"y"}, {"startDate"=>"2014-11-25T13:55:00.0Z", "endDate"=>"2014-11-25T14:05:00.0Z", "type"=>"Slot", "confirm"=>"y"}, {"startDate"=>"2014-11-25T14:05:00.0Z", "endDate"=>"2014-11-25T14:15:00.0Z", "type"=>"Slot", "confirm"=>"y"}, {"startDate"=>"2014-11-25T14:15:00.0Z", "endDate"=>"2014-11-25T14:25:00.0Z", "type"=>"Slot", "confirm"=>"y"}]}, {"sessionId"=>"d508b00000000000", "name"=>"Asthma Smoking Clinic", "type"=>"Asthma Clinic", "staffId"=>"2da2000000000000", "staffName"=>"Simon Smith", "staffGender"=>"Not set", "Site"=>{"name"=>"The View Surgery", "telephoneNumber"=>"01132050080", "Address"=>{"address"=>"Mill House, 54 Troy Road, Horsforth, Leeds, LS18 5TN"}}, "Slot"=>[{"startDate"=>"2014-11-25T13:45:00.0Z", "endDate"=>"2014-11-25T14:00:00.0Z", "type"=>"Slot", "confirm"=>"y"}, {"startDate"=>"2014-11-25T14:00:00.0Z", "endDate"=>"2014-11-25T14:15:00.0Z", "type"=>"Slot", "confirm"=>"y"}, {"startDate"=>"2014-11-25T14:15:00.0Z", "endDate"=>"2014-11-25T14:30:00.0Z", "type"=>"Slot", "confirm"=>"y"}, {"startDate"=>"2014-11-25T14:30:00.0Z", "endDate"=>"2014-11-25T14:45:00.0Z", "type"=>"Slot", "confirm"=>"y"}, {"startDate"=>"2014-11-25T14:45:00.0Z", "endDate"=>"2014-11-25T15:00:00.0Z", "type"=>"Slot", "confirm"=>"y"}]}]}},200]

      if response["ListSlotsReply"]["Session"].present?
        session_list = [response["ListSlotsReply"]["Session"]].flatten
        session_list.each do |session_detail|
          appointment_data << ::Clinic::Tpp::BookAppointment.formatted_slot_data(session_detail,options)
        end
        appointment_data = appointment_data.flatten
        if request_data[:ClinicianId].present?
          selected_clinician_appointments = appointment_data.select{|a| a[:clinician_id] == request_data[:ClinicianId]}
        else
          selected_clinician_appointments = appointment_data
        end
      
        slots_data = selected_clinician_appointments.group_by{|a| a[:date].to_s}
      else
        @error_message = response["Error"]["userFriendlyMessage"] ||  response["Error"]["technicalMessage"]
        slots_data = {}
        status = StatusCode::Status100
      end
    rescue Exception => e
      TppLogger.info  "..............................................................."
      TppLogger.info  "error msg is :#{e.message}"
      @error_message = ::Message::Tpp[:BookAppointment][:unable_to_get_available_appointment]
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      TppLogger.info "Inside exception retry count:  #{@retry_count} .................." 
      @retry_count = @retry_count.to_i + 1
      retry if @retry_count.to_i <= 2
       @retry_count = 0
      status = StatusCode::Status100
    end
    doctor_list = nil
    appointments = {:message=>@error_message, :slots_data=>slots_data,:doctor_list=>doctor_list}
    [status,appointments]
  end


  # options = {current_user=>current_user,}
  # appointment_details = {:appointment_id=>113036,:start_time=>(Time.now),:end_time=>(Time.now+15.min),:appointment_purpose=>'Consultation',:telephone_no=>'8826936913', :telephone_contact_type=> "Unknown"}
  # ::Clinic::Tpp::BookAppointment.book_appointment(organization_uid,member_id,appointment_details,options)
  def self.book_appointment(organization_uid,member_id,appointment_details,options={})
    begin
      current_user = options[:current_user]
      #<BookAppointment apiVersion="1" unitId="C84010" patientId="71df200000000000" onlineUserId="75ee400000000000" sessionId="a508b00000000000" startDate="2014-11-26T14:00:00.0Z" endDate="2014-11-26T14:10:00.0Z" notes="Persistant cough" uuid="88888888-8888-8888-8888-888888888888"></BookAppointment>
      request_body =  ::Clinic::Tpp::Client.request_body_data(current_user,member_id,options)
      request_body.merge!({
        "startDate": appointment_details[:start_time].to_time.utc.iso8601,
        "endDate": appointment_details[:end_time].to_time.utc.iso8601,
        "notes": appointment_details[:appointment_purpose],
        "sessionId": appointment_details[:appointment_id]
      })
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.BookAppointment(request_body) 
      end
      suid_required = true
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options = {:request_type=>"Post", :request_uri=> "BookAppointment", :request_body=>request_body}

      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
      # response,status = [{"BookAppointmentReply"=>{"patientId"=>"71df200000000000", "onlineUserId"=>"75ee400000000000", "message"=>"Remember to bring your medication!!!", "uuid"=>"88888888-8888-8888-8888-888888888888"}},200]

      if response["Error"].present?# && response["Error"]["technicalMessage"] == "Invalid sessionId"
        message = response["Error"]["userFriendlyMessage"] || response["Error"]["technicalMessage"]
        {id: appointment_details[:appointment_id].to_s,:source=>"tpp",status:100,message: message }
      elsif response["BookAppointmentReply"].present?
        {id: appointment_details[:appointment_id].to_s,:source=>"tpp",status:200,message: ::Message::Tpp[:BookAppointment][:booking_created]}
      else
        {id:slot_id.to_s,:source=>"tpp",status:200,message: ::Message::Tpp[:BookAppointment][:booking_created]}
      end
    rescue Exception => e
      if @book_appointment_retry_count.to_i <= 2
        ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
        @book_appointment_retry_count = @book_appointment_retry_count.to_i + 1 
        retry 
      end
       @book_appointment_retry_count = 0
      {message: ::Message::Tpp[:BookAppointment][:booking_not_created] ,status:100}
    end
  end

  # options = {:appointment_details=>appointment_details,:current_user=>current_user}
  # appointment_details = {:appointment_id=>'113037'}
  # ::Clinic::Tpp::BookAppointment.cancel_appointment(member_id,options)
  def self.cancel_appointment(organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      request_options = {}
      # byebug
      appointment_id = options[:appointment_details][:appointment_id]
      #<BookAppointment apiVersion="1" unitId="C84010" patientId="71df200000000000" onlineUserId="75ee400000000000" sessionId="a508b00000000000" startDate="2014-11-26T14:00:00.0Z" endDate="2014-11-26T14:10:00.0Z" notes="Persistant cough" uuid="88888888-8888-8888-8888-888888888888"></BookAppointment>
      request_body =  ::Clinic::Tpp::Client.request_body_data(current_user,member_id,options)
      request_body.merge!({
        "apptId": appointment_id
      })
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.CancelAppointment(request_body) 
      end
      suid_required = true
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options.merge!({:request_type=>"Post", :request_uri=> "CancelAppointment", :request_body=>request_body})
      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
      # response,status = [{"BookAppointmentReply"=>{"patientId"=>"71df200000000000", "onlineUserId"=>"75ee400000000000", "message"=>"Remember to bring your medication!!!", "uuid"=>"88888888-8888-8888-8888-888888888888"}},200]
      if (status == 200) 
        {:message=>::Message::Tpp[:BookAppointment][:appointment_canceled], id:appointment_id,status:200}
      else
        {error:response["Error"]["technicalMessage"], message:response["Error"]["technicalMessage"], status:100}
      end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      {id:appointment_id,status:100,:message=>::Message::Tpp[:BookAppointment][:unable_to_cancel_appointment]}
    end
  end
  

  def self.update_appointment(organization_uid,member_id,appointment_details,options={})
    {id:Random.rand(99999999999),status:200}
  end
  
  
  # options= {:current_user=>current_user, :appointment_details=>{:appointment_id=>''}}
  # ::Clinic::Tpp::BookAppointment.getPatienAppointment(member_id,options)
  def self.getPatienAppointment(member_id,options={})
    begin
      current_user = options[:current_user]
      request_options = {}
      member = Member.find(member_id)
      # <ViewAppointments apiVersion="1" unitId="C84010" patientId="71df200000000000" onlineUserId="75ee400000000000" futureAppointments="Y" uuid="88888888-8888-8888-8888-888888888888"></ViewAppointments>
      request_body =  ::Clinic::Tpp::Client.request_body_data(current_user,member_id,options)
      request_body.merge!({
        "futureAppointments": "Y"
      })
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.ViewAppointments(request_body) 
      end
      suid_required = true
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options.merge!({:request_type=>"Post", :request_uri=> "ViewAppointments", :request_body=>request_body})
      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
      # response,status = [{"ViewAppointmentsReply"=>{"patientId"=>"71df200000000000", "onlineUserId"=>"75ee400000000000", "uuid"=>"88888888-8888-8888-8888-888888888888", "Appointment"=>{"apptId"=>"fc44f00000000000", "startDate"=>"2014-11-26T14:00:00.0Z", "endDate"=>"2014-11-26T14:10:00.0Z", "staffName"=>"Mr Mark Ramsden", "status"=>"Booked", "reminder"=>"Remember to bring your medication!!!", "slotType"=>"Slot", "sessionType"=>"Session", "canBeCancelled"=>"y", "Site"=>{"name"=>"The View Surgery", "Address"=>{"line1"=>"Mill House", "line2"=>"Troy Road", "line3"=>"Horsforth", "line4"=>"Leeds", "line5"=>"LS18 5TN"}}}}},200]
      if status == 200
        data = []
        if response["ViewAppointmentsReply"]["Appointment"].present?
          appointments = [response["ViewAppointmentsReply"]["Appointment"]].flatten.compact
          # session_data = [session_data].group_by{|a| a["SessionId"]}
          appointments.each do |appointment_slot|
            doctor_name = (appointment_slot["staffName"].to_s.gsub('Clinician:','').strip )
            doctor_clinic_id = doctor_name
            data << {
            :id=>appointment_slot["apptId"],
            :appointment_id=>appointment_slot["apptId"],
            :purpose=>nil, 
            :appointment_purpose=>nil,
            :start_time=>appointment_slot["startDate"].to_time.utc,
            :end_time=>appointment_slot["endDate"].to_time.utc, 
            :doctor_name=>doctor_name,
            :doctor_clinic_id =>doctor_clinic_id,
            :can_cancel=>(appointment_slot["canBeCancelled"]|| true),
            :can_reschedule=> false,
            :doctor_specialization=> nil,
            :organization_uid=>member.organization_uid,
            :member_id=>member_id,
            :appointment_status=>appointment_slot["appointmentStatus"]
            }
          end
          data = data.sort_by{|a| a[:start_time]}.reverse rescue data
        end
        data = {:appointments=>data,:status=>200}
      else
        message = response["Error"]["userFriendlyMessage"] || response["Error"]["technicalMessage"]
        data  = {:appointments=>[],:error=> response["Error"]["technicalMessage"],message:message,status:status}
      end
     
    rescue Exception=> e
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      @retry_count = 0
      data = {:appointments=>[],:error=> e.message,message: ::Message::Tpp[:BookAppointment][:unable_to_list_appointment],status:100}
    end
    data
  end

end