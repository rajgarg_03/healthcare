class Clinic::Tpp::Immunisations
  # Get Patient Immunisations
  # None, Allergies, Consultations, Documents, Immunisations, Medication, Problems, TestResults
  def self.get_patient_immunisations(member_id,options={})
    begin
      current_user = options[:current_user]
      request_data = {}
      
      clinic_link_detail =  ::Clinic::LinkDetail.where(member_id:member_id).last
      user_link_detail = clinic_link_detail.link_info.with_indifferent_access
      request_body =  ::Clinic::Tpp::Client.request_body_data(current_user,member_id,options)
       
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.RequestChildhoodVaccs(request_body) 
      end
      request_body =   (builder.doc.root.to_xml).gsub("\n","")# CGI.unescapeHTML(builder.doc.root.to_xml).gsub("\n","")
      request_options = {}
      suid_required = true
      request_options.merge!({:request_type=>"Post", :request_uri=> "RequestChildhoodVaccs", :request_body=>request_body})
      response,status = ::Clinic::Tpp::Client.make_request(current_user,member_id,request_options,suid_required, options)
      if status == 200
        # response =  [{"MedicalRecord"=>{"PatientGuid"=>"5a09eb8f-7e0b-4630-ad48-87e8e05d124f", "Title"=>"Master", "Forenames"=>"Mike", "Surname"=>"Singh", "Sex"=>"Male", "DateOfBirth"=>"2016-09-22T00:00:00", "Allergies"=>nil, "Consultations"=>nil, "Documents"=>nil, "Immunisations"=>[{"ObservationType"=>"Immunisation", "Episodicity"=>"None", "NumericValue"=>nil, "NumericOperator"=>nil, "NumericUnits"=>nil, "DisplayValue"=>nil, "TextValue"=>nil, "Range"=>nil, "Abnormal"=>false, "AbnormalReason"=>nil, "AssociatedText"=>nil, "EventGuid"=>"9fe87ee5-4e7e-46e3-8b88-86b4e9ca84bd", "Term"=>"Influenza vaccination", "AvailabilityDateTime"=>"2020-01-30T06:39:56.043", "EffectiveDate"=>{"DatePart"=>"YearMonthDay", "Value"=>"2020-01-30T00:00:00"}, "CodeId"=>142934010, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}, {"ObservationType"=>"Immunisation", "Episodicity"=>"None", "NumericValue"=>nil, "NumericOperator"=>nil, "NumericUnits"=>nil, "DisplayValue"=>nil, "TextValue"=>nil, "Range"=>nil, "Abnormal"=>false, "AbnormalReason"=>nil, "AssociatedText"=>nil, "EventGuid"=>"d9e4d77b-d8c3-4fc9-a0dd-db5a76d3f372", "Term"=>"1st hepatitis A vaccination", "AvailabilityDateTime"=>"2020-01-30T06:41:06.473", "EffectiveDate"=>{"DatePart"=>"YearMonthDay", "Value"=>"2020-01-30T00:00:00"}, "CodeId"=>264177017, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}, {"ObservationType"=>"Immunisation", "Episodicity"=>"None", "NumericValue"=>nil, "NumericOperator"=>nil, "NumericUnits"=>nil, "DisplayValue"=>nil, "TextValue"=>nil, "Range"=>nil, "Abnormal"=>false, "AbnormalReason"=>nil, "AssociatedText"=>nil, "EventGuid"=>"32e9cbf1-a55f-4e61-91fc-ee1446bb53e6", "Term"=>"Immunisation given", "AvailabilityDateTime"=>"2020-01-30T06:18:40.763", "EffectiveDate"=>{"DatePart"=>"YearMonthDay", "Value"=>"2020-01-30T00:00:00"}, "CodeId"=>62701000000116, "AuthorisingUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "EnteredByUserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2"}], "Medication"=>nil, "Problems"=>nil, "TestResults"=>nil, "Users"=>[{"UserInRoleGuid"=>"80d1fea2-75ec-42f5-ba74-0ba1554781f2", "Title"=>"Dr", "Forenames"=>"Nurturey", "Surname"=>"Test", "Role"=>"General Medical Practitioner", "Organisation"=>"Nurt services"}]}, "FilterDetails"=>{"ItemFilter"=>"Immunisations", "ItemFilterFromDate"=>nil, "ItemFilterToDate"=>nil, "FreeTextFilterFromDate"=>nil}}, 200]
        observation_data = []
        observation_data =  response["RequestChildhoodVaccsReply"]["ChildhoodVacc"]
        # Select only adminsitered jabs
        # 0 Consent given.
        # 1 Consent refused.
        if options[:only_administered] == true
          observation_data = observation_data.select{|a| a["consentType"] == "0"}
        end
        immunisation_records = ::Clinic::Tpp::Immunisations.get_formatted_immunisation_data(member_id, observation_data,options)
        #immunisation_records = {"2020-01-30T00:00:00"=>[{:code_id=>846639, member_id=>nil, :name=>"Influenza vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 30 Jan 20"}, {:code_id=>8466339, :member_id=>nil, :name=>"1st hepatitis A vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 30 Jan 20"}], "2020-02-02T00:00:00"=>[{:member_id=>nil, :name=>"First typhoid vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 02 Feb 20"}, {:member_id=>nil, :name=>"1st hepatitis B vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 02 Feb 20"}], "2020-02-02T08:09:00"=>[{:member_id=>nil, :name=>"First diphtheria vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 02 Feb 20"}]}
        message = nil
      elsif status == 403
        status = 403
        message = response["Message"] || response["Error"]["technicalMessage"]
      else
        status = 100
        message = response["Message"]
        raise (response["Message"] || 'Error inside get_patient_immunisations') if @retry_count.to_i < 2
      end    
    rescue Exception=>e 
      ::Clinic::Tpp::Client.clear_member_session(current_user,member_id,options)
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      status = 100
      @retry_count = 0
      message = 'Unable to get immunisations from clinic'
      @error = e.message
    end
    {data:immunisation_records,:status=>status,:message=>message,:error=>@error}
  end

   def self.get_formatted_immunisation_data(member_id,records,options={})
    data = {}
    group_by_date = records.group_by{|a| a["dateGiven"].to_s}
    group_by_date.each do |date,records|
      record_data = []
      records.each do |record|
        record_date = date.to_date.to_s

        record_data << ({:code_id=>nil,:tpp_term=>"#{record["vaccContent"]} #{record["vaccPart"]}" ,:member_id=>member_id,:name=>record["vaccContent"], :value=> record["vaccPart"],:data_source=>"tpp",:added_by=>"system",:updated_at=>"Administered on #{date.to_date.to_date7}" })
      end
      data[date] = record_data
    end
    data
  end
 
  
end
 