class Clinic::DelinkedLinkDetail
  include Mongoid::Document  
  include Mongoid::Timestamps
  
  field :email,                         :type => String # Nurturey email
  field :link_info,                     :type => Hash   
  field :user_info,                     :type => Hash    
  field :patient_uid,                   :type => String  # third party uniq patient id
  field :phone_number,                  :type => String
  field :nhs_email,                     :type => String # Nhs/gpsoc email
  field :organization_uid,              :type => String 

  def self.save_record(current_user,options)
    begin
      link_detail = ::Clinic::LinkDetail.where(member_id:current_user.id).last
      ::Clinic::DelinkedLinkDetail.where(email:current_user.email).delete_all
      data = {
        email:current_user.email,
        link_info:link_detail.link_info,
        user_info:link_detail.user_info,
        patient_uid: link_detail.patient_uid, 
        phone_number: link_detail.phone_number,
        organization_uid: link_detail.organization_uid,
        nhs_email: link_detail.email 
      }
      record = ::Clinic::DelinkedLinkDetail.new(data)
      record.save!
    rescue Exception => e
      Rails.logger.info "link_details #{link_detail.inspect}"
      Rails.logger.info "Error inside Clinic::DelinkedLinkDetail save_record... #{e.message}"
    end

  end 
end