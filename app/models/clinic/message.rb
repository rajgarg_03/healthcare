class Clinic::Message
 
  #options = {current_user=>}
  def self.get_message_list(current_user,member,options={})
    organization_uid = member.organization_uid
    member_id = member.id
    clinic_class = ::Clinic::Message.get_clinic_class(member,options)
    clinic_class.get_messages(organization_uid,member_id,options)
  end

  def self.get_message_detail(current_user,member,message_id,options={})
    organization_uid = member.organization_uid
    member_id = member.id
    clinic_class = ::Clinic::Message.get_clinic_class(member,options)
    clinic_class.get_message_detail(message_id,organization_uid,member_id,options)
  end

  def self.get_message_receiptent_list(current_user,member,options={})
    organization_uid = member.organization_uid
    member_id = member.id
    clinic_class = ::Clinic::Message.get_clinic_class(member,options)
    clinic_class.get_message_receiptent_list(current_user,member_id,options)
  end
   
 
  def self.delete_message(current_user,member,message_id,options={})
    organization_uid = member.organization_uid
    member_id = member.id
    clinic_class = ::Clinic::Message.get_clinic_class(member,options)
    clinic_class.delete_message(message_id,organization_uid,member_id,options)
  end

  def self.send_message(current_user,member,subject,msg,options={})
    member_id = member.id
    organization_uid = member.organization_uid
    clinic_class = ::Clinic::Message.get_clinic_class(member,options)
    clinic_class.send_message(subject,msg,organization_uid,member_id,options)
  end

  def self.update_message_read_status(current_user,member,message_id,message_read_state,options={})
    member_id = member.id
    organization_uid = member.organization_uid
    clinic_class = ::Clinic::Message.get_clinic_class(member,options)
    response = clinic_class.update_message_read_status(message_id,message_read_state,member_id,options)
  end

  def self.get_message_attachement_data(current_user,member,attachment_id,options={})
    member_id = member.id
    options[:current_user] = current_user
    organization_uid = member.organization_uid
    clinic_class = ::Clinic::Message.get_clinic_class(member,options)
    response = clinic_class.get_message_attachment(member.id,attachment_id,options)

  end

  def self.get_clinic_class(member,options={})
    clinic_detail = Clinic::Organization.get_clinic(member,options)
    clinic_class = ::Clinic::ClinicDetail.get_clinic_linkable_class(options[:current_user],member=nil,"Message",options.merge({:clinic_detail=>clinic_detail}))
    return clinic_class
  end

end