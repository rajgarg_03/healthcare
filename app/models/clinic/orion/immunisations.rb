class Clinic::Orion::Immunisations
  # Get Patient Immunisations
  # None, Allergies, Consultations, Documents, Immunisations, Medication, Problems, TestResults

  def self.get_patient_immunisations(member_id,options={})
    begin
      current_user = options[:current_user]
      request_options = {}
      request_data = options[:request_data] || {}
      immunisation_records = []
      request_body = {"patient.identifier"=>'ORION%7CAAAA-0124-8'}.merge(request_data) 
      request_header = {}

      request_options.merge!({:request_type=>"Get", :request_uri=> "/fhir/1.0/Immunization", :request_body=>request_body,:request_header=>request_header})
      response,status = ::Clinic::Orion::Client.make_request(current_user,member_id,request_options,options)
      if status == 200
        # response =  [{"entry"=>[{"resource"=>{"status"=>"completed", "patient"=>{"reference"=>"Patient/IFAUCQJNGAYTENBNHBAE6USJJ5HA"}, "extension"=>[{"url"=>"http://fhir.hl7.org.nz/dstu2/StructureDefinition/agent", "extension"=>[{"url"=>"role", "valueCoding"=>{"code"=>"author", "system"=>"http://hl7.org/fhir/provenance-participant-role", "display"=>"Author"}}, {"url"=>"actorId", "valueIdentifier"=>{"value"=>"OHCP"}}]}, {"url"=>"http://fhir.hl7.org.nz/dstu2/StructureDefinition/agent", "extension"=>[{"url"=>"role", "valueCoding"=>{"code"=>"custodian", "system"=>"http://hl7.org/fhir/provenance-participant-role", "display"=>"Custodian"}}, {"url"=>"actorId", "valueIdentifier"=>{"value"=>"SYS_A"}}]}], "resourceType"=>"Immunization", "vaccineCode"=>{"coding"=>[{"code"=>"359606008", "system"=>"http://www.SYS_A/SNM", "display"=>"Anastomosis of esophagus, antethoracic"}]}, "doseQuantity"=>{"unit"=>"MG", "value"=>0.5, "system"=>"http://www.SYS_A/ISO+"}, "vaccinationProtocol"=>[{"doseSequence"=>20}], "contained"=>[{"resourceType"=>"Organization", "identifier"=>[{"system"=>"http://www.SYS_A/MVX", "value"=>"MSD"}], "id"=>"1", "name"=>"Merck & Co., Inc."}], "lotNumber"=>"W2371234567", "date"=>"1995-09-23", "identifier"=>[{"system"=>"AZVAC", "value"=>"V49"}], "id"=>"9b941aa28ea73bc998c2c9956e59b826c55e26fd86129132fde5d34a3ab48581", "manufacturer"=>{"reference"=>"#1"}}, "link"=>[{"url"=>"/fhir/1.0/explain/patientsealopen", "relation"=>"orionhealth:describe-patient-seal-open"}], "fullUrl"=>"/fhir/1.0/Immunization/9b941aa28ea73bc998c2c9956e59b826c55e26fd86129132fde5d34a3ab48581"}]}}, 200]
        observation_data = []
        observation_data = response["entry"]
        immunisation_records = ::Clinic::Orion::Immunisations.get_formatted_immunisation_data(member_id, observation_data,options)
        #immunisation_records = {"2020-01-30T00:00:00"=>[{:code_id=>846639, member_id=>nil, :name=>"Influenza vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 30 Jan 20"}, {:code_id=>8466339, :member_id=>nil, :name=>"1st hepatitis A vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 30 Jan 20"}], "2020-02-02T00:00:00"=>[{:member_id=>nil, :name=>"First typhoid vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 02 Feb 20"}, {:member_id=>nil, :name=>"1st hepatitis B vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 02 Feb 20"}], "2020-02-02T08:09:00"=>[{:member_id=>nil, :name=>"First diphtheria vaccination", :value=>nil, :data_source=>"emis", :added_by=>"system", :updated_at=>"Administered on 02 Feb 20"}]}
        message = nil
      else
        # status = 100
        status = ::Clinic::Orion::Client.get_status(status,options)
        message = response["Message"]
        raise (response["Message"] || 'Error inside get_patient_immunisations') if (@retry_count.to_i < 2 && status != StatusCode::Status403)
      end    
    rescue Exception=>e 
      puts '@retry_count.to_i is'
      options[:get_new_access_token] = true
      ::Clinic::Orion::Client.create_end_user_session(current_user,member_id,options)
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      status = 100
      @retry_count = 0
      message = ::Message::Emis[:HealthCard][:unable_to_list] %{:item_type=>"Immunisations"}
      @error = e.message
    end
    {data:immunisation_records,:status=>status,:message=>message,:error=>@error}
  end

   def self.get_formatted_immunisation_data(member_id,records,options={})
    data = {}
    group_by_date = records.group_by{|a| a["resource"]["date"].to_s}
    group_by_date.each do |date,immunisation_records|
      record_data = []
      immunisation_records.each do |record|
        record_date = date.to_date.to_s
        immunisation = record["resource"]
        vaccine_detail = immunisation["vaccineCode"]["coding"].first || {} rescue {}
        value = immunisation["doseQuantity"]["value"].to_s + " " + immunisation["doseQuantity"]["unit"]
        vaccine_name = vaccine_detail["display"].gsub("VACCINE","").strip
        record_data << ({:code_id=>vaccine_detail["code"] ,:member_id=>member_id,:name=>vaccine_name, :value=> value,:data_source=>"orion",:added_by=>"system",:updated_at=>"Administered on #{date.to_date.to_date7}" })
      end
      data[date] = record_data
    end
    data
  end
 
  
end
 