class Clinic::Orion::Login
  def self.available_services(member_id,options={})
    service_data = {
      appointment_booking:true,
      repeat_prescription:false,
      view_health_records:true,
      manage_messages: false,
      immunisations: true,
      health_card:{
        Immunisations:  true,
        Allergy:        false,
        Consultations:  false ,
        Problems:       false,
        Medication:     false,
        Investigations: false,
        Documents:      false
       }
     }
    data =  {services:service_data, status:200}
  end
end