class Clinic::Orion::HealthCard
  # Get Patient Immunisations
  # None, Allergies, Consultations, Documents, Immunisations, Medication, Problems, TestResults

  def self.get_patient_health_record(member_id,options={})
    begin
      item_type = options[:item_type]
      current_user = options[:current_user]
      request_options = {}
      request_data = options[:request_data] || {}
      if item_type == "Immunisations"
        response = ::Clinic::Orion::Immunisations.get_patient_immunisations(member_id,options)
      else
       response = {status:200}
      end
    rescue Exception=>e 
      message = ::Message::Emis[:HealthCard][:unable_to_list] %{:item_type=>item_type}
      @error = e.message
      response = {status:100, message:message,:error=>@error}
    end
    response
  end
end