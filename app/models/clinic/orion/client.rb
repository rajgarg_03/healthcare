class Clinic::Orion::Client

 def self.get_link_details_for_session(current_user,member_id,options={})
    clinic_link_detail = ::Clinic::LinkDetail.get_clinic_link_detail(member_id,current_user,options)
    user_details = clinic_link_detail.link_info.with_indifferent_access rescue nil
  end

  def self.get_status(api_response_status,options={})
    api_response_status == StatusCode::Status403 ? StatusCode::Status403: 100
  end

  def self.get_session_detail(member_id,current_user,options={})
    ::Clinic::UserClinicSession.where(member_id:member_id, parent_member_id:current_user.id).last
  end

  def self.create_end_user_session(current_user,member_id,options={})
    begin
      orion_login_detail = ::Orion::LoginDetail.where(member_id:member_id).last
      access_token = orion_login_detail.access_token rescue nil
      if access_token.blank? || options[:get_new_access_token] == true
        data = ::Orion::LoginDetail.get_token_with_refresh_token(orion_login_detail.refresh_token)
        if data[:status] == 200
          access_token = data[:access_token]
        else
          access_token = nil
        end
      end
    rescue Exception=>e 
      Rails.logger.info "Error inside create_end_user_session #{e.message}" 
    end
    access_token
  end
  

  def self.get_session_detail(member_id,current_user,options={})
    ::Clinic::UserClinicSession.where(member_id:member_id, parent_member_id:current_user.id).last
  end

  def self.make_request(current_user,member_id,request_options,options={})
    begin
        data = {}
        if current_user.clinic_account_status != "Active"
         data = { "Message"=> ::Message::Clinic[:clinic_access_blocked], status:403}
         return [data,403]
        end
        if current_user.is_orion_api_request_limit_exceed?(options)
          data = { "Message"=> ::Message::Clinic[:clinic_access_limit_exceeded], status:403}
          return [data,403]
        end
        status = 200

        access_token = ::Clinic::Orion::Client.create_end_user_session(current_user,member_id,options)
         
        message_id = SecureRandom.uuid
        header = {'Authorization'=>"Bearer #{access_token}", 'Accept'=> 'application/json'}
        header.merge!(request_options[:request_header]) if request_options[:request_header].present?
        OrionLogger.info  '...................Request..Action ..................'
        OrionLogger.info  "..................#{request_options[:request_uri].to_s}.................."
        OrionLogger.info  '...................Request...Headers values...................'
        OrionLogger.info  header.inspect
        
        end_point_url =  request_options[:end_point] ||  APP_CONFIG['orion_api_base_url']
        uri = URI.parse("#{end_point_url}" + request_options[:request_uri].to_s)

        http = Net::HTTP.new(uri.host, uri.port)
        if uri.scheme == 'https'
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_PEER
        end
        
          
        request_body = request_options[:request_body]
        OrionLogger.info '......................Request Body...................'
        OrionLogger.info  request_body.inspect
        puts '......................Request Body...................'

        if request_options[:request_type] == "Get"
          uri.query = URI.encode_www_form(request_body) if request_body.present?
          request = Net::HTTP::Get.new(uri.request_uri, header)
        elsif request_options[:request_type] == "Delete"
          uri.query = URI.encode_www_form(request_body) if request_body.present?
          request = Net::HTTP::Delete.new(uri.request_uri, header)
          request.body = request_body.to_json if request_body.present?
        elsif request_options[:request_type] == "Put"
          request = Net::HTTP::Put.new(uri.request_uri, header)
          request.body = request_body.to_json if request_body.present?
        else 
          request = Net::HTTP::Post.new(uri.request_uri, header)
          request.body = request_body.to_json if request_body.present?
        end
        response = http.request(request)
        OrionLogger.info  '......................Response...................'
        OrionLogger.info  response.body.inspect rescue nil
        OrionLogger.info '......................Response Code...................'
        OrionLogger.info  response.code rescue nil

        if response.code.to_i >= 200 && response.code.to_i < 300 
          data =  JSON.parse(response.body) rescue response.body
          status = 200
          @makeRequest_retry_count = 0
        else
          data =  JSON.parse(response.body) rescue response.body
          status = response.code.to_i
         istatus = 100
          if status == 500
            status = 100
          elsif status == 403
          else
            raise 'retry request'  if @makeRequest_retry_count.to_i < 2
          end 
        end
      rescue Exception=>e 
          OrionLogger.info "................ Erorr description......................."
          OrionLogger.info "................ #{e.message}......................."
        if @makeRequest_retry_count.to_i < 2
          @makeRequest_retry_count = @makeRequest_retry_count.to_i  + 1
          OrionLogger.info "................Retrying Orion session........."
          puts "............................Retrying Orion session........."
          retry 
        end  
        status = 100
        UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,request_options,"Error inside Orion make_request : #{options[:request_uri]} ")
        raise e.message if @user_session_required == true
      end
      # log event 
      action =  request_options[:request_uri].to_s
      request_status = status == 200 ? true : false
      message = data["Message"]
      request_data = {message:message, :request_type=>request_options[:request_type],:request_status=> request_status, :action=> action, :patient_id=>(nil),:message_id=>message_id}    
      current_user.log_orion_request(member_id,request_data,options)

      [data,status]
  end
end