class Clinic::Document
  include Mongoid::Document  
  include Mongoid::Timestamps
  #options = {current_user=>}
  def self.list_document(current_user,member,options={})
    member_id = member.id
    clinic_class = ::Clinic::ClinicDetail.get_clinic_linkable_class(current_user,member,"Documents",options)
    clinic_class.list_document(member_id,options)
  end

  #options = {current_user=>}
  def self.document_detail(current_user,member,document_id,options={})
    member_id = member.id
    clinic_class = ::Clinic::ClinicDetail.get_clinic_linkable_class(current_user,member,"Documents",options)
    clinic_class.get_document_content(member_id,document_id,options)
  end
end