class Clinic::BookAppointment
  include Mongoid::Document  
  include Mongoid::Timestamps
  # User from organization linked
  field :member_id,                :type => String 
  field :ods_code,         :type => String  
  # field :booking_uid,              :type => String  # provided by third party api if any
  field :appointment_purpose,      :type => String  # purpose for booking eg. consulation, surgery..  
  field :start_time,               :type => Time    #
  field :end_time,                 :type => Time    #
  field :cancel_time,              :type => Time    #When user canclled appointment
  field :appointment_status,       :type => String  #confirmed/cancelled
  field :doctor_name,              :type => String   
  field :doctor_code,              :type => String  # third party identifier if any 
  field :doctor_specialization,    :type => String  #Anesthesiologists/Cardiologists
  field :doctor_clinic_id,         :type => String  # doctor clinic id used for smrthi
  field :appointment_id,           :type => String  # provided by third party api if any
  field :organization_uid,         :type=>  String   # Not in use 
  field :data_source,              :type=>  String   # Nurturey/EMIS/TPP.. 
  
  index({member_id:1})
  index({appointment_id:1})
  #options = {current_user=>}
  def self.get_doctor_list(organization_uid,member_id,options={})
    member = Member.find(member_id)
    clinic_class = ::Clinic::BookAppointment.get_clinic_class(member,options)
    clinic_class.get_doctor_list(organization_uid,member_id,options)
    # [{:name=>'Dr Wangoo',:specialization=>"Endocrinologists"},{:name=>'Dr Vinod',:specialization=>"Dermatologists"}]
    # response = clinic_class.get_doctor_list(organization_uid,options)
  end


  def self.get_appointment_purpose_list(member,options={})
    ["Consultation", "Followup","Checkup","Medical Test"]
  end
  

  def get_json_data
    appointment = self
    data = appointment.attributes
    data["appointment_id"] = appointment.appointment_id || appointment.id
    data["_id"] = data["appointment_id"]
    data["can_cancel"] =  false #= 'confirmed'
    data["can_reschedule"] =  false #data['appointment_status'] == 'confirmed'
    data
  end
  

  def self.save_appointment_from_appointment_list(member_id,appointments,options={})
    current_user = options[:current_user]
    member = Member.find(member_id)
    appointments.each do |appointment|
      appointment_exist = Clinic::BookAppointment.where(member_id:member_id,appointment_id:appointment[:appointment_id]).last.present?
      if !appointment_exist
        data = {
          :appointment_purpose =>appointment[:appointment_purpose], 
          :start_time  => appointment[:start_time],
          :end_time   => appointment[:end_time],
          :appointment_status=> appointment[:appointment_status],
          :doctor_name   =>appointment[:doctor_name],
          :doctor_clinic_id=> appointment[:doctor_clinic_id],
          :doctor_code=> appointment[:doctor_clinic_id],
          :doctor_specialization=>appointment[:doctor_specialization],
          :appointment_id=>appointment[:appointment_id],
          :organization_uid=>member.organization_uid,
          :ods_code=>member.ods_code,
          :member_id=>member_id,
          :data_source=>appointment[:data_source]
        }
        appointment = Clinic::BookAppointment.new(data)
        if ::System::Settings.im1ClinicRecordSavingEnabled?(current_user,"appointment",options)
          appointment.save
        end
        appointment
      end
    end
  end
  
  def self.list_user_appointment(current_user,member_id,options={})
    begin
      options[:current_user] = current_user
      clinic_class = ::Clinic::BookAppointment.get_clinic_class(current_user,options)
      response = clinic_class.getPatienAppointment(member_id,options)
      appointments = response[:appointments]

      status = response[:status]
      if status == 200
        ::Clinic::BookAppointment.save_appointment_from_appointment_list(member_id,appointments,options)
      end
      message = response[:message]
    rescue Exception=>e 
      appointments = []
      status = 100
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside list_user_appointment")
    end
    [appointments,status,message]
  end

  def self.get_available_appointments(organization_uid,member_id,options={})
    member = Member.find(member_id)
    clinic_class = ::Clinic::BookAppointment.get_clinic_class(member,options)
    status,response = clinic_class.get_available_appointments(organization_uid,member_id,options) 
  end

  
  def cancel_appointment(cancellation_reason,options={})
    appointment = self
    ods_code = appointment.ods_code
    member_id = appointment.member_id
    current_user = options[:current_user]
    member = Member.find(member_id)
    options[:appointment_details] = {:appointment_id=>appointment.appointment_id,:cancellation_reason=>cancellation_reason}
    clinic_class = ::Clinic::BookAppointment.get_clinic_class(member,options)
    response = clinic_class.cancel_appointment(ods_code,member_id,options) 
    if response[:status] == 200
      if ::System::Settings.im1ClinicRecordSavingEnabled?(current_user,"appointment",options)
        appointment.update_attributes({:appointment_status=>'cancelled',:cancel_time=>Time.now,:data_source=>"nurturey"}) rescue nil
      end
      status = true
    else
      status = false
    end
    status
  end
 
  
  # Not supported feature for now
  # :appointment_details=>{:doctor_name=>"", :doctor_clinic_id=>"", :doctor_specialization=>"", :doctor_code=>'SUB',:start_date=> '10-07-2019',:start_time=>'9:00',:end_time=>'10:00',:appointment_purpose=>'consulatation'} }  
  def update_appointment(appointment_details,options={})
    appointment = self
    member_id = appointment.member_id
    member = Member.find(member_id)
    clinic_class = ::Clinic::BookAppointment.get_clinic_class(member,options)
    response =  response.with_indifferent_access rescue response
    response = clinic_class.update_appointment(organization_uid,member_id,appointment_details,options) 
    status = false
    if response[:status] == 200
      if true #appointment.update_attributes(appointment_details)
        status = true
        appointment = appointment.get_json_data
      else
        appointment = nil
      end
    end
    [appointment,status]
  end


  # smrthi
  # :appointment_details=>{:doctor_name=>"", :doctor_clinic_id=>"", :doctor_specialization=>"", :doctor_code=>'SUB',:start_date=> '10-07-2019',:start_time=>'9:00',:end_time=>'10:00',:appointment_purpose=>'consulatation'} }  
  # EMIS
  # :appointment_details=>{appointment_id=>'',:appointment_purpose=>'consulatation'} }  
  def self.save_appointment(organization_uid,member_id,appointment_details,options={})
    member = Member.find(member_id)
    current_user = options[:current_user]
    clinic_class = ::Clinic::BookAppointment.get_clinic_class(member,options)
    response = clinic_class.book_appointment(organization_uid,member_id,appointment_details,options) 
    response =  response.with_indifferent_access rescue response
    status = false
    if response[:status] == 200
      if ["emis","tpp"].include?(response[:source])
        begin
          appointment = Clinic::BookAppointment.new(
            appointment_id:response[:id],
            booking_uid:  response[:id],
            doctor_name:appointment_details[:doctor_name], 
            doctor_clinic_id:appointment_details[:doctor_clinic_id], 
            doctor_specialization:appointment_details[:doctor_specialization], 
            doctor_code:appointment_details[:doctor_clinic_id], 
            appointment_status:"confirmed",
            appointment_purpose:appointment_details[:appointment_purpose], 
            member_id:member_id,
            ods_code:current_user[:ods_code],
            start_time:appointment_details[:start_time].utc.iso8601,
            end_time:appointment_details[:end_time].utc.iso8601,
            data_source:"nurturey"
          )

        rescue Exception=> e
          Rails.logger.info "Error inside save_appointment in saving appointment ......#{e.message}" 
        end
        # appointment = {doctor_name:appointment_details[:doctor_name], doctor_clinic_id:appointment_details[:doctor_clinic_id], doctor_specialization:appointment_details[:doctor_specialization], doctor_code:appointment_details[:doctor_code], appointment_id:response[:id], appointment_status:"confirmed",appointment_purpose:appointment_details[:appointment_purpose], member_id:member_id,organization_uid:organization_uid,start_time: appointment_details[:start_time].utc.iso8601,end_time:appointment_details[:end_time].utc.iso8601}
      else
         # {"AppointmentDetails"=>{"patientID"=>"3097", "patientName"=>"SHAFRIN ", "mobileNo"=>"9840112397", "appointmentDate"=>"29/08/2019", "appointmentTime"=>"11:00", "doctorCode"=>"MEE", "doctorName"=>"DR. MEENAKSHI (MEE)", "appointmentSource"=>"NURTUREY", "locationName"=>"SMRTHI", "addressLine1"=>"92, Dr Ranga Road,", "addressLine2"=>"", "addressLine3"=>"Alwarpet", "city"=>"CHENNAI", "country"=>"TAMILNADU", "pincode"=>"600018"}, "Api_ResponseCode"=>"APPOINTMENT SUCCESS", "Api_ResponseDesc"=>"Appointment booked with Dr.J.MEENAKSHI on 29/08/2019 Time:11:00AM for SHAFRIN  at SMRTHI,Alwarpet. "}
        appointment = Clinic::BookAppointment.new(appointment_id:response[:id], doctor_name:response[:doctor_name], doctor_clinic_id:response[:doctor_clinic_id], doctor_specialization:response[:doctor_specialization], doctor_code:response[:doctor_code], appointment_status:"confirmed",appointment_purpose:appointment_details[:appointment_purpose], member_id:member_id,organization_uid:organization_uid,start_time:appointment_details[:start_time],end_time:appointment_details[:end_time],data_source:"nurturey")
        appointment
      end
      if ::System::Settings.im1ClinicRecordSavingEnabled?(current_user,"appointment",options)
        appointment_saved = appointment.save
      else
        appointment_saved = true
      end
      if appointment_saved
        status = true
        if response[:source] == "emis" || response[:source] == "tpp"
          appointment[:can_reschedule] = false
          appointment[:can_cancel] = true
        else
          appointment = appointment.get_json_data
        end
        appointment[:message] = response[:message]
      else
        appointment = response
      end
    else
      appointment = response
    end
    [appointment,status]
  end

  def self.get_clinic_class(member,options={})
    clinic_detail = Clinic::Organization.get_clinic(member,options)
    clinic_class = ::Clinic::ClinicDetail.get_clinic_linkable_class(options[:current_user],member=nil,"BookAppointment",options.merge({:clinic_detail=>clinic_detail}))
    return clinic_class
  end

end