class Clinic::HealthRecord
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :member_id, type:String
  field :category, type:String
  field :record_uid, type:String


  def self.get_clinical_data(member,current_user,category,options={})
    member_id = pmember.id
    options[:current_user] = current_user
    supported_category = {"::Clinic::Orion::HealthCard"=>["Immunisations"],"Emis::HealthCard"=>["Consultations","Medication","Problems","TestResults","Documents","Allergy","Immunisations"]}
    clinic_class = ::Clinic::ClinicDetail.get_clinic_linkable_class(current_user,member,"HealthCard",options)
    save_health_record = ::System::Settings.im1ClinicRecordSavingEnabled?(current_user,"health_records",options)
    options[:save_health_record] = save_health_record
    if supported_category[clinic_class.to_s].include?(category)
      response = clinic_class.get_patient_health_record(member_id,options)
      if response[:status] == 200
        health_card_records = response[:data].values.flatten.sort_by{|a| ["updated_at"]}.reverse rescue []
        if save_health_record
          Clinic::HealthRecord.save_records(health_card_records,category,options)
        end
      end
    else
      response = {status:100,:error=>"Not support for #{category}"}
    end 
  end
  

  def self.save_records(member,health_card_records,category,options={})
    current_user = options[:current_user]
    record_category = category || "health_records"
    save_health_record = options[:save_health_record] || ::System::Settings.im1ClinicRecordSavingEnabled?(current_user,record_category,options) 
    if save_health_record
      Rails.logger.info "......................................Saving Health card data.................................."
      health_card_records.each do |health_card_record|
        record_exist = ::Clinic::HealthRecord.where(member_id:member.id,:record_uid=>health_card_record[:record_uid]).last
        if !record_exist
          data = health_card_record.deep_dup
          data["category"] = category
          data.delete(:updated_at)
          Clinic::HealthRecord.create(data)
        end
      end
    end
  end
end