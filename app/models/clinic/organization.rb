# Used in admin panel
class Clinic::Organization
   
  include Mongoid::Document  
  include Mongoid::Timestamps
  
  field :name,                     :type => String 
  field :uid,                      :type => String  #uniq id 
  field :clinic_id,                :type => String  #eg. Smrthi location_id 
  field :custom_id,                :type => Integer 
  field :address,                  :type => String 
  field :postcode,                 :type => String
  field :longitude,                :type => String
  field :latitude,                 :type => String
  field :phone_no,                 :type => String
  field :email,                    :type => String
  field :clinic_type,              :type => String #smrthi..
  field :ods_code,                 :type => String #smrthi..
  validates_presence_of :name
  # validates_presence_of :uid
  validates_presence_of :custom_id
  
  index({uid:1})  
  index({ods_code:1})  


  def self.get_clinic(member,options={})
    member = member || Member.new
    ods_code = options[:ods_code].blank? ?  member.ods_code : options[:ods_code]
    organization_uid = options[:organization_uid].blank? ? member.organization_uid :  options[:organization_uid]
    clinic = ::Clinic::ClinicDetail.find_by_code_or_organization_id(ods_code,organization_uid,options)
    return clinic if clinic.present?
    if ods_code.present?
      where(ods_code:member.ods_code).last
    elsif organization_uid.present?
      where(uid:organization_uid).last
    else
      nil
    end
  end

  def self.create_organization(current_user,data,options={})
    organization = ::Clinic::Organization.new(data)
    options[:current_user] = current_user
    organization_custom_id = ::Clinic::Organization.max(:custom_id)|| 0
    organization.custom_id = organization_custom_id + 1
    if organization.save
      organization.ods_code = organization.uid
      organization.save
      status = true
      ::Clinic::ClinicDetail.save_clinic_detail(organization,options)
    else
      status = false
    end
    [organization,status]
  end


  def update_organization(current_user,data,options={})
    organization = self
    if organization.update_attributes(data)
      organization.ods_code = organization.uid
      organization.save
      status = true
      ::Clinic::ClinicDetail.update_clinic_detail(organization,options)
    else
      status = false
    end
    [organization,status]
  end

  def self.get_clinic_list(current_user,member_id,options={})
    member = Member.where(id:member_id).last
    user_ods_code = member.ods_code rescue nil
    clinic_list = []
    if user_ods_code.blank?
      user_ods_code = ::Clinic::OrganizationUser.where(email:current_user.email).last.ods_code rescue nil
      if user_ods_code.blank?
        clinic_list << ::Clinic::Organization.where(:ods_code.ne=>user_ods_code).entries
      else
        clinic_list << ::Clinic::Organization.where(:ods_code=>user_ods_code).last  rescue nil
      end
    else
      clinic_list << ::Clinic::Organization.where(:ods_code.ne=>user_ods_code).entries
    end
    clinic_list = clinic_list.flatten.compact
    ::Clinic::Organization.format_clinic_list_response(clinic_list,current_user,member,options)
  end

  


  def self.format_clinic_list_response(clinic_list, current_user,member,options)
    data = []
    clinic_list.each do |clinic|
      clinic_detail = ::Clinic::ClinicDetail.get_clinic_detail_for_organization(clinic,options)
      data << clinic_detail.format_data(current_user,member,options)
    end
    data
  end

  # options[:current_user]
  def self.set_clinic(member_id,organization_uid,options={})
    current_user = options[:current_user]
    member = Member.find(member_id)
    clinic_detail = options[:clinic_detail] || ::Clinic::Organization.get_clinic(current_user,options)
    ods_code = clinic_detail[:ods_code] rescue nil
    if organization_uid.blank? || ods_code == organization_uid 
      # Fetch and Update organization_uid
      begin
        organization = ::Nhs::NhsApi.get_clinic_by_ods_code(current_user,member_id,ods_code,options) 
        if organization.present?
          organization_uid = organization["uid"]
          clinic_detail[:uid] = organization_uid
          options[:organization_uid] = organization_uid
	        member.update_attribute(:organization_uid,organization_uid) if member.organization_uid != organization_uid
        end
      rescue Exception =>e
        Rails.logger.info "Error inside Fetch and Update organization_uid ........#{e.message}"
      end
    end
    member = Member.find(member_id)
    member.update_attribute(:ods_code,ods_code)

    family_ids = options[:family_ids] || FamilyMember.where(member_id:member_id).pluck(:family_id)
    Family.where(:id.in=>family_ids,ods_code:nil,organization_uid:nil).update_all({ods_code:ods_code, organization_uid:organization_uid})
    family_member_ids = FamilyMember.where(:family_id.in=>family_ids).map(&:member_id).map(&:to_s)
    
    if options[:clinic_detail].present?
      clinic_detail[:organization_id] = organization_uid
      # Set clinic type eg . emis/tpp/smriti etc
      clinic_type = Clinic::ClinicDetail.identify_clinic_type(current_user,clinic_detail,options)
        if Rails.env == "production" || clinic_type != "unknown"
         options[:organization_type] = clinic_type 
        end
      end
    
    clinic_link_state = 2 # setup by user
    options[:clinic_detail] = options[:clinic_detail] || clinic_detail.attributes
     # Save/update ClinicDetail, User and Organization detail
    ::Clinic::User.save_record(member_id,organization_uid,clinic_link_state,"user",options)
    member_with_clinic_detail = Member.where(:id.in=>family_member_ids,:organization_uid.nin=>[nil,'']).map(&:id).map(&:to_s)
    (family_member_ids - member_with_clinic_detail).each do |member_id|
      clinic_link_state = 1 # set up by system
      member = Member.find(member_id)
      if !member.is_expected?
        member.update_attributes({ods_code:ods_code, organization_uid:organization_uid,clinic_link_state:1})
        ::Clinic::User.save_record(member_id,organization_uid,clinic_link_state,setup_by="system",options)
      end
    end
  end
  
  # ::Clinic::Organization.setup_clinic_with_nhs(current_user,clinic_type, clinic_info,options={})
  def self.setup_clinic_with_nhs(current_user,clinic_type, clinic_info,options={})
    begin
      if current_user.organization_uid != clinic_info.uid && current_user.clinic_link_state < 3
        options[:type] = clinic_type
        options[:current_user] = current_user
        organization_uid = clinic_info.uid
        options[:organization_type] = clinic_type
        options[:ods_code] = clinic_info.ods_code
        options[:organization_id] = organization_uid
        options[:clinic_detail] =  { longitude: clinic_info.longitude, latitude:clinic_info.latitude, email: clinic_info.email, phone_no:clinic_info.phone_no , :ods_code=>clinic_info.ods_code, :name=>clinic_info.name,:uid=>clinic_info.uid,:address=>clinic_info.address,:postcode=>clinic_info.postcode}.with_indifferent_access
        member_id = current_user.id
        Rails.logger.info "options value for setup ....... #{options} ....."
        Rails.logger.info "organization_uid ....... #{organization_uid} ....."
        ::Clinic::Organization.set_clinic(member_id,organization_uid,options)
        member = Member.find( current_user.id )
        Rails.logger.info "Member setup status ..for member #{current_user.id}...organization_uid: #{organization_uid}......#{current_user.ods_code != clinic_info.ods_code}"
      end
    rescue Exception=> e 
      Rails.logger.info "Error inside gp setup................................ "
      Rails.logger.info "GP couldn't setup for current User #{current_user.id} due to #{e.message}"
    end
  end

  def self.delink_clinic(member_id,organization_uid,options={})
    begin
       
      current_user = options[:current_user]
      ::Clinic::User.delete_record(member_id,organization_uid,options)
     rescue Exception => e
       Rails.logger.info  "Error inside delink_clinic ......... #{e.message}"
     end 
  end

end