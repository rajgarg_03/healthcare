# Used in admin panel
class Clinic::OrganizationUser
  include Mongoid::Document  
  include Mongoid::Timestamps
  # user for onboard
  field :name,                    :type => String 
  field :email,                   :type => String  # uniq id 
  field :uid,                     :type => String  # user uniq id 
  field :phone_no,                :type => Integer 

  belongs_to :organization, class_name:"Clinic::Organization"
  validates_presence_of :name
  validates_presence_of :organization_id
  # validates_presence_of :uid
  
  def self.import_from_xls(organization_id,file,current_user,options={})
    q1 = Roo::Excelx.new(file.path,file_warning: :ignore)
    q1.default_sheet = q1.sheets[0]
    status = true
     
    q1.entries[1..-1].each do |entry|
      begin
        Clinic::OrganizationUser.new(
          name:entry[1],
          email: entry[0],
          uid: entry[2],
          phone_no: entry[3],
          organization_id: organization_id
        ).save!
      
      rescue Exception=>e 
        Rails.logger.info "Error- Data not imported for #{entry}"
        puts "Error- Data not imported for #{entry}"
        status = false
      end
      break if status== false
    end
  status
  end
end