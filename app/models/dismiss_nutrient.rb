#Used in dismiss card record
class DismissNutrient
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :parent_id, type: String
  field :child_id, type: String
  field :nutrient_name, type: String
  field :dismiss_up_to, type: Date
  field :skipped_forever, :type=> Boolean
 
  validates :parent_id, :child_id, :nutrient_name , presence: true
  index({parent_id:1})
  index({nutrient_name:1})
  index({child_id:1})
end