class Utility

  # def self.base64_to_hex(base64_string)
  #   base64_string.unpack("m0").first.unpack("H*").first
  # end
  def self.country_list
    country_list = ISO3166::Country.all_names_with_codes.to_h
    country_list["United Kingdom"]= "UK"
    country_list.to_a
  end
  def self.hex_to_base64(hex_string)
    [[hex_string].pack("H*")].pack("m0")
  end

 def self.base64_to_hex(base64_string)
    base64_string.unpack("m0").first
  end

  def self.page_info(data,current_page,per_page=5)
    total_count = data.count
    if total_count > 0 
      current_page = current_page.to_i
      total_page = total_count/per_page
      total_page = total_page + 1 if (total_count%5 !=0)
      next_page = (current_page + 1) if ((total_page - current_page) >0)
    else
      current_page = 0
      total_page = 0
      next_page = 0
    end
    [total_page,(next_page || total_page),total_count,current_page]

  end

  # Copy data from on Db to another DB
  def self.copy_data(source_database="web_reloaded_stage",destination_database="web_reloaded_stage1",activity_status="Inactive",options={:delete_from_source_database=> true})
    Rails.application.eager_load!
    # users =  User.with(database: source_database).all
    # Except User and Member Move all data to acrchive
    models = Mongoid.models.map(&:to_s).uniq - ["Vaccination","Pregnancy", "Delayed::Backend::Mongoid::Job"]
    models = ["Vaccination","Pregnancy"] + models
    model_list = models.map{|a| eval(a)}
    member_ids = FamilyMember.where(:activity_status=>activity_status).pluck(:member_id).uniq
    member_ids.each do |member_id|
      begin

        model_list.each do |klass|
          if klass.to_s == "Jab"
            vaccine_ids =  Vaccination.with(database: destination_database).where(:member_id=>member_id.to_s).pluck(:id).map(&:to_s)
            record_exist =  klass.with(database: destination_database).where(:vaccination_id.in=>vaccine_ids)
          elsif klass.to_s == "Pregnancy"
            record_exist =  klass.with(database: destination_database).where(:expected_member_id=>member_id.to_s)
          elsif klass.to_s.match(/pregnancy/i).present?
            pregnancy_obj_id =  Pregnancy.with(database: destination_database).where(:expected_member_id=>member_id.to_s).last.id rescue nil
            record_exist =  klass.with(database: destination_database).where(:expected_member_id=>pregnancy_obj_id.to_s)
          else
            record_exist =  klass.with(database: destination_database).where(:member_id=>member_id.to_s)
          end
          if record_exist.blank?
            if klass.to_s == "Jab"
              vaccine_ids = Vaccination.with(database: source_database).where(member_id:member_id).pluck(:id).map(&:to_s)
              source_data_obj = klass.with(database: source_database).where(:vaccination_id.in=>vaccine_ids).entries
            elsif klass.to_s == "Pregnancy"
              source_data_obj = klass.with(database: source_database).where(expected_member_id:member_id).entries
            elsif klass.to_s.match(/pregnancy/i).present?

              pregnancy_obj = Pregnancy.with(database: source_database).where(expected_member_id:member_id).last
              if pregnancy_obj.present?
                source_data_obj = klass.with(database: source_database).or({pregnancy_id:pregnancy_obj.id.to_s},{:member_id=>member_id.to_s}).entries
              else
                source_data_obj = []
              end
            else
              source_data_obj = klass.with(database: source_database).where(member_id:member_id).entries
            end
            if source_data_obj.present?
              source_data_obj.each do |data_obj_val|
                data =  data_obj_val.attributes
                data["_id"] = Moped::BSON::ObjectId(data["_id"])
                Mongoid.override_database(destination_database)
                destination_obj = klass.new(data)
                destination_obj.id = data["_id"]
                # if klass.to_s == "User"
                  destination_obj.save(validate:false)
                if options[:delete_from_source_database] == true
                  Mongoid.override_database(source_database)
                   klass.find(data_obj_val.id).delete
                else 
                  Mongoid.override_database(source_database)
                end
              end
            end
          end
        end
         
      rescue Exception=>e 
        puts "Error in Copy data................"
        puts e.message
        Mongoid.override_database(source_database)
      end
    end
  end


  def self.copy_all_data(source_database="web_reloaded_stage",destination_database="web_reloaded_stage1")
    Rails.application.eager_load!
    models = Mongoid.models.map(&:to_s).uniq
    model_list = models.map{|a| eval(a)}
    model_list.each do |klass|
      begin
        klass.with(database: destination_database).delete_all
        data_obj = klass.with(database: source_database).all.entries
        if data_obj.present?
          data_obj.each do |data_obj_val|
            data =  data_obj_val.attributes
            data["_id"] = Moped::BSON::ObjectId(data["_id"])
            Mongoid.override_database(destination_database)
            destination_obj = klass.create(data) 
            destination_obj = klass.new(data) 
            destination_obj.id = data["_id"]
            destination_obj.save(validate:false)
            Mongoid.override_database(source_database)
          end
        end
      rescue Exception=>e 
        puts e.message
        Mongoid.override_database(source_database)
      end
    end
  end
  
  # Utility.mark_inactive_families(activity_status="Inactive")
  def self.mark_inactive_families(activity_status="Inactive")
    # family_status = activity_status.downcase
    # report_event = Report::Event.where(name:"mark family #{family_status}").last
    options = {}
    options[:event_source] = "system"
    # options[:report_event] = report_event
    Family.where(:activity_status.ne=>activity_status).no_timeout.batch_size(200).each do |family|
      begin
        update_activity_status = Utility.get_family_active_members(family,activity_status)
        if update_activity_status && family.activity_status != activity_status
          family.mark_family_activity_status(activity_status,options)
        end
      rescue Exception=>e 
      end
    end
  end

  def self.mark_inactive_users(duration,start_time="1.year",activity_status="Inactive")
    duration_time = Date.today - eval(duration)

    # user not logged in since duration time
    if start_time.present?
      start_time = Date.today - eval(start_time)
      users =  User.where(:last_sign_in_at=>{"$lte"=>duration_time,"$gt"=>start_time},:activity_status.ne=>activity_status)#.update_all(:activity_status=>activity_status)
    else
      users = User.where(:last_sign_in_at.lte=>duration_time,:activity_status.ne=>activity_status)#.update_all(:activity_status=>activity_status)
    end
    options = {}
    options[:event_source] = "system"
    options[:user_report_event] = Report::Event.where(name:"mark user #{activity_status.downcase}").last
    users.no_timeout.batch_size(200).each do |user|
      begin
        user.mark_user_activity_status(activity_status,options)
        # member = user.member
        # user.update_attribute(:activity_status,activity_status)
        # member.update_attribute(:activity_status,activity_status)
        # Utility.add_event_for_activity(user_id,nil,report_event)
      rescue Exception=>e 
        Rails.logger.info "Error inside Utility mark_inactive_users #{e.message}"
        Rails.logger.info e.message
      end
    end
     
  end
  #........................Start of # Updated logic for mark activity status ...............
  #Utility.update_child_member_activity_v2("Active","40-50","0-30")
  #Utility.update_child_member_activity_v2("Inactive","270-360","8-10")
  #Utility.update_child_member_activity_v2("Inactive","0-360","31+")
  def self.update_child_member_activity_v2(activity_status,child_member_age_group_in_days,acitivty_duration_to_check_in_days)
    begin  
      child_member_age_group_in_days = child_member_age_group_in_days.split("-").map(&:to_i)
      if child_member_age_group_in_days.count < 2
        child_member_age_group_in_days = [child_member_age_group_in_days.last,nil]
      end
    
      #[40-50]
      # 04 sep 
      birth_start_date = Date.today - child_member_age_group_in_days.last.days rescue nil
      
      # 14 sep
      birth_end_date = Date.today - child_member_age_group_in_days.first.days rescue nil
      birth_date_filter = {}
      if  birth_start_date.present?
        birth_date_filter["$gte"]= birth_start_date
      end

      if  birth_end_date.present?
        birth_date_filter["$lte"]= birth_end_date
      end

      acitivty_duration_to_check_in_days = acitivty_duration_to_check_in_days.split("-").map(&:to_i)
      if acitivty_duration_to_check_in_days.count < 2
        acitivty_duration_to_check_in_days = [acitivty_duration_to_check_in_days.last,nil]
      end


      # 24 oct
      activity_start_date = Date.today - (acitivty_duration_to_check_in_days.last).days rescue nil
      # 25 Aug
      activity_end_date = Date.today - acitivty_duration_to_check_in_days.first.days rescue nil
      
      activity_date_filter = {}
       if  activity_start_date.present?
        activity_date_filter["$gte"]= activity_start_date
      end

      if  activity_end_date.present?
        activity_date_filter["$lte"]= activity_end_date
      end
      # ChildMember.where(:birth_date=>birth_date_filter,:last_activity_done_on=>activity_date_filter).update_all(:activity_status=>activity_status)
      ChildMember.where(:birth_date=>birth_date_filter,:last_activity_done_on=>activity_date_filter).no_timeout.batch_size(200).each do |member|
          if !member.is_expected? && member.activity_status_v2 != activity_status
            member.update_activity_status_v2(nil,activity_status,{:member_id=>member.id,:event_source=>"batch_job"}) rescue nil
          end
      end
      rescue Exception=>e 
        Rails.logger.info "Error inside Utility update_child_member_activity_v2 #{e.message}"
        Rails.logger.info e.message
      end
      # ChildMember.where(:birth_date=>birth_date_filter,:last_activity_done_on=>activity_date_filter)

  end

  #Utility.update_pregnancy_activity_status("Inactive",pregnancy_week,"31+")
  def self.update_pregnancy_activity_status_v2(activity_status,pregnancy_week,acitivty_duration_to_check_in_days)
    begin  
      if pregnancy_week.present?
        birth_date_filter = {}
        # Thu, 25 Feb 2021
        week_start_date = Date.today + 40.week - pregnancy_week.week
        # Thu, 04 Mar 2021
        week_end_date = Date.today + 40.week - pregnancy_week.week + 7.day
        birth_date_filter["$gte"]= week_start_date
        birth_date_filter["$lte"]= week_end_date
      end

      acitivty_duration_to_check_in_days = acitivty_duration_to_check_in_days.split("-").map(&:to_i)
      if acitivty_duration_to_check_in_days.count < 2
        acitivty_duration_to_check_in_days = [acitivty_duration_to_check_in_days.last,nil]
      end

      activity_start_date = Date.today - (acitivty_duration_to_check_in_days.last).days rescue nil
      activity_end_date = Date.today - acitivty_duration_to_check_in_days.first.days rescue nil
      
      activity_date_filter = {}
      if activity_start_date.present?
        activity_date_filter["$gte"]= activity_start_date
      end

      if activity_end_date.present?
        activity_date_filter["$lte"]= activity_end_date
      end
      if pregnancy_week.present?
        pregnancies =  Pregnancy.where(:status=>"expected",:expected_on=>birth_date_filter,:last_activity_done_on=>activity_date_filter)
      else
        pregnancies =  Pregnancy.where(:status=>"expected",:last_activity_done_on=>activity_date_filter)
      end
      pregnancies.each do |pregnancy|
       member = Member.where(id:pregnancy.expected_member_id).last
        if member.present? && member.activity_status_v2 != activity_status
          member.update_activity_status_v2(nil,activity_status,{:member_id=>member.id,:event_source=>"batch_job"})
        end
      end
    rescue Exception=>e 
      Rails.logger.info "Error inside Utility mark_inactive_users #{e.message}"
      Rails.logger.info e.message
    end
  end
  
  def self.update_family_activity_status_v2
    begin
      options = {event_source:"batch_job"}  
      options[:update_activity_status] = false
      Family.no_timeout.batch_size(200).each do |family|
        # includes ChildMember and Pregnancy
        begin  
          family_child_member_ids = FamilyMember.where(family_id:family.id).in(role:FamilyMember::CHILD_ROLES).pluck("member_id")
          active_member = ChildMember.where(:id.in=>family_child_member_ids,:activity_status_v2=>"Active").limit(1).count
          activity_status = active_member > 0 ? "Active" : "Inactive"
          if family.activity_status_v2 != activity_status
            family.mark_family_activity_status_v2(activity_status,options) rescue nil
          end
        rescue Exception => e 
          Rails.logger.info e.message
        end
      end
    rescue Exception => e
      Rails.logger.info "Error inside Utility update_pregnancy_activity_status_v2 #{e.message}"
      Rails.logger.info e.message
    end
  end

  
  # Utility.update_user_activity_status_v2(acitivty_duration_to_check_in_month)
  def self.update_user_activity_status_v2(acitivty_duration_to_check_in_month)
    begin  
      User.no_timeout.batch_size(200).each do |user|
        begin
          if user.last_sign_in_at.present?  && user.last_sign_in_at >= (Date.today - acitivty_duration_to_check_in_month.to_i.months)
            activity_status = user.has_active_family? ? "Active" : "Inactive"
          else
            activity_status = "Inactive"
          end
          if activity_status != user.activity_status_v2 
            user.mark_user_activity_status_v2(activity_status,{event_source:"batch_job",user_id:user.id,update_activity_status:false})
          end
        rescue Exception => e 
          Rails.logger.info "update_user_activity_status_v2 .. #{e.message}"
        end
      end
    rescue Exception => e 
  
    end
  end
  #........................End of # Updated logic for mark activity status ...............


  def self.get_family_active_members(family,activity_status,options={})
    elder_member_ids = FamilyMember.where(family_id:family.id.to_s,:role.in=>FamilyMember::ELDER_ROLES + ["Creator"]).pluck(:member_id).map(&:to_s).uniq
    active_members = User.where(:member_id.in=>elder_member_ids,:activity_status=>"Active").pluck(:member_id).map(&:to_s).uniq
    inactive_members = User.where(:member_id.in=>elder_member_ids,:activity_status=>activity_status).pluck(:member_id).map(&:to_s).uniq
    status = false
    if active_members.present?
      status = false
    else
     status = elder_member_ids.count == inactive_members.count
    end
    status
  end

  def self.get_words_array(data_string)
    string_array = data_string.split(",") 

    string_array.each_with_index do |element,index|
      string_array[index] = element.strip()
    end

    return string_array
  end
  
  def self.add_event_for_activity(user_id,family_id,report_event,options={})
    begin
      options[:event_source] = "system"
      ::Report::ApiLog.log_event(user_id,family_id,report_event.custom_id,options) rescue nil
    rescue Exception => e
      Rails.logger.info "Error inside Utility add_event_for_user #{e.message}"
    end
  end



  def self.get_keywords_exists_in_string(keywords,data_string)
    matched_kewords = []
    keywords.each do |keyword| 
      lower_case_keyword = keyword.downcase
      if data_string.include?(lower_case_keyword) 
        matched_kewords.push(lower_case_keyword)
      end
    end
    return matched_kewords
  end

end