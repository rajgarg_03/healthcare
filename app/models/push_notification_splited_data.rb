class PushNotificationSplitedData
	include Mongoid::Document
  include Mongoid::Timestamps

  # field :child_id, type: String
  field :type, type: String # eg:Pointer/Action/UserAction/birthday_wish/m003 its a identifier for push type
  field :message, type: String 
  field :status, type: String, default: "pending" # for push notification eg: pending/sent
  field :email_status, type: String, default: "pending" # used for email sent eg: pending/sent
  field :priority, type: Integer #0 for mandatory in batch, 1 pointer and real time,  other are logicwise
  field :identifier, type:String # eg: for pointer s_id: for action card_id
  field :user_id, type:String # user to whom notification sent
  field :score, type: Float # used for pointer to sort
  field :push_error_message , type:String # error message if notification push fails
  field :notification_expiry_date , type:Date # 
  field :country_code, type:String  
  field :batch_no, type:Integer  
   
  # Deeplink fields (used in mobile app)
  field :content_category, type: String # to show image or video link type "image/video"
  field :content_url, type:String   #link for eg "http://nxyt.tiny.url/images/"
  field :destination , type:String #eg. "pointers","manage_family","measurment..." this is destination when notification is clicked eg timeline, milestone etc
  field :destination_type , type:String # Eg, "User", Child,Pregnancy
  field :action_name , type:String #Eg Add,Update
  field :family_id , type:String
  field :role , type:String # Son, Daughter,User
  field :feed_id, type:String
  field :user_notification_id, type:String
  field :schedule_push_date, type:Date # Date on which push has to be sent
  field :category, :type=>String # to identifiy which type of tool . 


  index({user_id:1})
  index({status:1})
  index({type:1})
  #index({schedule_push_date:1})
  index({notification_expiry_date:1})
  index({batch_no:1})
  index({country_code:1})
  index({destination:1})
  index({category:1})

  def get_deeplink_data(current_user,api_version=1)
    begin
      user_notification = UserNotification.new(self.attributes)
      user_notification.get_deeplink_data(current_user,api_version)
    rescue 
      {}
    end
  end
  
  def self.copy_data_from_user_notification
  	user_notifications = UserNotification.where(:created_at.lte=>Date.today - 3.month) 
    num_docs = user_notifications.count
    group_size = 2
    num_groups = (Float(num_docs) / group_size).ceil
    puts "#{num_docs} documents found. #{num_groups} groups calculated."

    1.upto(num_groups) do |group|
      new_notification_list = []
      notification_group = user_notifications.paginate(:page => group, :per_page => group_size)
        notification_group.each do |notification|
          new_notitification = notification.attributes.except("_id")
          new_notitification["user_notification_id"] = notification.id
          new_notification_list << new_notitification
        end

      PushNotificationSplitedData.collection.insert(new_notification_list)
       
    end
     user_notifications = UserNotification.where(:created_at.lte=>Date.today - 3.month).delete_all
     PushNotificationSplitedData.where(:created_at.lte=>Date.today - 18.month).delete_all
  end
end