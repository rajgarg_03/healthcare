class UserNotification::EmailDataForValidation
	include Mongoid::Document
   
  field :remaining_push_count_to_sent ,type:Integer
  field :user_id ,type:String   
  field :country_code ,type:String
  field :created_at, type:Date
  # field :added_push_count_by_type, type: Hash 
  field :sent_push_count_by_type, type: Hash, :default => Hash.new 
  belongs_to :user
  index({user_id:1})
  
  
  def self.update_data(user_id,tool_name,add_new=false)
   
   tool_name = tool_name.downcase rescue tool_name
    # create new record if not created in current date
    record_obj = UserNotification::EmailDataForValidation.where(:user_id=>user_id,created_at: Date.today).last
    if record_obj.blank?
      UserNotification::EmailDataForValidation.where(:user_id=>user_id).delete_all
      record_obj = UserNotification::EmailDataForValidation.new(created_at: Date.today, remaining_push_count_to_sent:GlobalSettings::TotalMaxAllowedPush, user_id:user_id)
    end
    # Update push counter
    if add_new != true
      sent_push_count_by_type_copy = record_obj.sent_push_count_by_type.deep_copy
      if tool_name.present?
        sent_push_count_by_type_copy[tool_name] = sent_push_count_by_type_copy[tool_name].to_i + 1
        record_obj.sent_push_count_by_type = sent_push_count_by_type_copy
      end
      if tool_name != "pointers" 
        record_obj.remaining_push_count_to_sent = record_obj["remaining_push_count_to_sent"].to_i - 1
      end
    end
    record_obj.save
    record_obj
  end
end


