class User::SsoLoginDetail
  include Mongoid::Document
  include Mongoid::Timestamps
  field :provider,                :type => String
  field :uid,                     :type => String
  field :user_id,                 :type => String
  
  index({user_id:1})
  index({uid:1})
  
  def self.save_login_detail(provider,uid,user_id,options={})
    begin
      login_detail = ::User::SsoLoginDetail.where(uid:uid,:provider=>provider,user_id:user_id).last
      provider = provider.present? ? provider : "Nurturey"
      if login_detail.blank?
        login_detail = User::SsoLoginDetail.new(provider:provider,uid:uid,user_id:user_id)
        login_detail.save
      end
      user = User.find(user_id)
      user.update_allowed_provider_for_signin(provider,options)
    rescue Exception=>e 
      Rails.logger.info "Error inside User::SsoLoginDetail. save_login_detail .....#{e.message}"
    end  
  end

end