class SysGreeting
  include Mongoid::Document
  include Mongoid::Timestamps
  field :title,             :type => String
  field :description,      :type => String
  field :criteria, :type=> String
  field :score,      :type => String
  field :custom_id, :type=>Integer
  field :stage, :type=> String



  def self.assign_greeting(user_id,child_member_id=nil,option={:greeting_list=>nil,:member_obj=>nil,:category=>"all"})
    begin
      member = option[:member_obj] || Member.find(child_member_id.to_s)
      sys_greetings = option[:greeting_list] || SysGreeting.all
      country_code = User.where(id:user_id).first.country_code rescue "other"
      country_code = country_code || "other"
      sys_greetings.each do |sys_greeting|
        begin
            data = save_for_member(user_id,sys_greeting,member,country_code.downcase)
        rescue Exception=>e
          puts e.message + " id :" + sys_greeting.id + "member id:" + member.id.to_s + " custom_id: "+ sys_greeting.custom_id.to_s
        end
      end
      # ArticleRef.create(kk)
    rescue Exception=>e
      puts e.message + "member-id: #{member}"
    end
  end

   

  def self.save_for_member(user_id,sys_greeting,member,country_code="other")
    sys_greeting_category  = sys_greeting.category
    return true if sys_greeting.criteria.nil?
    valid_for_member = false
    valid_expir_date = false
    country_code = "india" if country_code == "in"
    case sys_greeting_category
      when "pregnancy"
        pregnancy = Pregnancy.where(expected_member_id:member.id.to_s,status:"expected").first
        expected_on = pregnancy.expected_on if pregnancy.present?
        valid_for_member =  pregnancy.present? && (eval(sys_greeting.criteria)) 
      when "milestone"
        mileston_conditions = sys_greeting.criteria.split("#")
        milestone_name, milestone_status = mileston_conditions[0].split(":")
        sys_milestone = SystemMilestone.where(title:milestone_name.strip).first
        milestone = Milestone.where(system_milestone_id:sys_milestone.id,member_id:member.id).first rescue nil
        valid_for_member = milestone.present? 
      when "member"
        pregnancy = Pregnancy.where(expected_member_id:member.id.to_s).first
        valid_for_member = pregnancy.nil? && (!sys_greeting.criteria.nil? && eval(sys_greeting.criteria)) rescue false
    end
    if sys_greeting_category != "vaccination"
      valid_expir_date = eval(sys_greeting.ref_expire_date) > Date.today if valid_for_member
      valid_for_member = (valid_expir_date == true)
    end
    if valid_for_member 
      begin
        name =  eval(sys_greeting.name) 
      rescue Exception=>e
       name = sys_greeting.name
      end

      begin
        des =  eval(sys_greeting.description) 
      rescue Exception=>e
        des = sys_greeting.description
      end

      data = 
        { 
          :priority=>3,
          :user_id=>user_id,
          :member_id=>sys_greeting.member_id.to_s,
          :message=>sys_greeting.title, 
          :status=>"pending",
          :identifier=>sys_greeting.custom_id,
          :type=>"greeting",
          :score=>2
        }
      obj = UserNotification.new(data).save
      # if !obj.upsert
      #   UserNotification.where(user_id:user_id,custome_id:sys_greeting.custome_id,member_id:member.id.to_s).delete_all
      #   obj.save
      # end
    end
  end

end