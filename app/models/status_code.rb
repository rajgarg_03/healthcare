class StatusCode
  Status100 = 100
  Status200 = 200
  Status103 = 103
  Status401 = 401
  Status403 = 403
end