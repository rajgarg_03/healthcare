class Nook::Products


  def self.list(current_user,options={})
    base_url ="#{APP_CONFIG["default_host"]}/assets/nook"
    product_list =  [
      {
        "product_name": "New Mama's Vitamin Bundle",
        "product_link_url": "https://nurtureyshop.myshopify.com/collections/frontpage/products/post-pregnancy-moms-nutrition-quarterly",
        "product_image_url": "#{base_url}/panel1.jpg",
        "product_price": "36.50",
        "product_old_price": "40.55",
        "currency": "£"
      },
      {
        "product_name": "Little Champ Nutrition",
        "product_link_url": "https://nurtureyshop.myshopify.com/collections/frontpage/products/little-champ-nutrition-age-1-3-quarterly",
        "product_image_url": "#{base_url}/panel2.jpg",
        "product_price": "17.10",
        "product_old_price": "18.00",
        "currency": "£"
      },

      {
        "product_name": "Pregnancy Nutrition Bundle",
        "product_link_url": "https://nurtureyshop.myshopify.com/collections/frontpage/products/pregnancy-bundle-quarterly",
        "product_image_url": "#{base_url}/panel3.jpg",
        "product_price": "25.04",
        "product_old_price": "27.82",
        "currency": "£"
      }
    ]
    product_list
  end

end
 
