class Pregnancy
  include Mongoid::Document
  include Mongoid::Timestamps
  field :name, type: String
  field :status, type: String , :default => "expected"
  field :child_count , type: String, :default => "1"
  field :expected_on , type: Date 
  field :expected_member_id , type: String
  field :added_by, type: String
  field :desc, type: String
  field :added_child, type: Array
  field :show_kick_count_schedule_screen, type: Boolean, default: true
  field :last_activity_done_on,    :type => Time # last record access/updated

  belongs_to :member
  belongs_to :family
  belongs_to :expected_member, class_name: 'ChildMember', foreign_key: :expected_member_id
  
  has_many :pregnancy_measurements,:class_name=> 'Pregnancy::HealthCard::Measurement', :dependent => :destroy
  has_many :timelines, :as=> :timelined, :dependent => :destroy
  has_many :article_refs
  has_many :pregnancy_blood_pressure, :class_name=> 'Pregnancy::HealthCard::BloodPressure'
  has_many :pregnancy_hemoglobin, :class_name=> 'Pregnancy::HealthCard::Hemoglobin'
  
  validates_presence_of :expected_on
  validates_presence_of :expected_member_id

  index ({id:1})
  index({expected_member_id:1})
  index ({member_id:1})
  index ({family_id:1})
  index ({status:1})

  PREGNANCY_MAX_WEEK = 43
  KICK_COUNTER_DEFAULT_WEEK = 28
  KICK_COUNTER_START_TIME = '17:00'

  #Used in app dashboard data for prenatal dashboard widget 
  def self.prenatal_dashboard_widget(current_user,options={})
    family_ids = current_user.user_families.map{|a| a.id.to_s}
    pregnancy_dashboard_widget = []
    preganancy_list = Pregnancy.in(family_id:family_ids).where(:status=> "expected")
    preganancy_list.each do |pregnancy|
      pregnancy_dashboard_widget << pregnancy.tool_graph_data rescue nil  
    end
    pregnancy_dashboard_widget
  end

  #Used in app dashboard data for pregnancy widget 
  def self.pregnancy_widget(current_user,options={})
    begin
      family_ids = current_user.user_families.map{|a| a.id.to_s}
      pregnancy_progress_widget = []
      preganancy_list = Pregnancy.in(family_id:family_ids).where(:status=> "expected")
      if preganancy_list.present?
        pregnancy_progress_bar_description_by_week = Pregnancy.get_pregnancy_progress_bar_description_by_week
        preganancy_list.each do |pregnancy|
          pregnancy_week,pregnancy_status =  Pregnancy.calculate_pregnancy_week(pregnancy.expected_on)
          pregnancy_bar_manager = PregnancyProgressBarManager.where(pregnancy_week:pregnancy_week).last
          if pregnancy_bar_manager.blank?
            pregnancy_bar_manager = pregnancy_week < 3 ? PregnancyProgressBarManager.new : PregnancyProgressBarManager.last
          end
          pregnancy_week = 1 if pregnancy_week.to_i == 0
          title = pregnancy_bar_manager.title(pregnancy,pregnancy_week, current_user)
          pregnancy_age =  pregnancy_status == "overdue" ? "Overdue" : Pregnancy.calulate_age(pregnancy.expected_on,pregnancy_status)
          pregnancy_progress_widget << {pregnancy_progress_bar_description_by_week:pregnancy_progress_bar_description_by_week,pregnancy_age:pregnancy_age,member_id:pregnancy.expected_member_id, action: pregnancy_bar_manager.action,text_to_highlight: pregnancy_bar_manager.text_to_highlight , pregnancy_max_duration:pregnancy_bar_manager.pregnancy_max_duration, title:title, week_left:pregnancy_bar_manager.week_left, description:pregnancy_bar_manager.description,"pregnancy_status"=>pregnancy_status,  "pregnancy_id"=> pregnancy.id,"pregnancy_name"=> pregnancy.name,"family_id"=> pregnancy.family_id,"mother_id"=> pregnancy.member_id,"mother_name"=>(pregnancy.member.first_name rescue nil),"pregnancy_week"=> pregnancy_week}
        end
      end
    rescue Exception=> e 
      pregnancy_progress_widget = []
    end
    pregnancy_progress_widget
  end

  def self.get_pregnancy_progress_bar_description_by_week
    data = {}
    PregnancyProgressBarManager.all.each do |pregnancy_progress_bar|
      data[pregnancy_progress_bar.pregnancy_week] = {:description=>pregnancy_progress_bar.description}
    end
    data
  end


  def self.calculate_pregnancy_week(expected_on)
    expected_on = expected_on.to_date
    if expected_on <= Date.today
      diff_days = (Date.today -  expected_on).to_i
      pregnancy_week = 40 + ((diff_days/7) + 1 rescue 1 )
      pregnancy_status = "overdue"
    else
      preg = Pregnancy.new
      diff_days = (Date.today - (expected_on - 40.weeks)).to_i
      x = diff_days%7== 0 ? 0 : 1
      pregnancy_week = (Pregnancy.new.pregnancy_week(Date.today,expected_on).to_i  + x) rescue 1
      pregnancy_status  = "expected"
    end
    [pregnancy_week,pregnancy_status]
  end
 
    def pregnancy_week(on_date=Date.today,pregnancy_expected_on=nil,add_days=false)
      on_date = on_date.to_date
      begin
        expected_date = (pregnancy_expected_on || self.expected_on).to_date
        if (expected_date < on_date rescue false)
          diff_days = (on_date - expected_date).to_i + (40 * 7)
        elsif (expected_date - 40.weeks) > on_date
          diff_days = 0
        else
          diff_days = (on_date - (expected_date - 40.weeks)).to_i
        end
        if add_days == true
          weeks = diff_days/7
          days = diff_days%7
          [weeks,days]
        else
          weeks = diff_days/7
        end
      rescue Exception=> e
        Rails.logger.info e.message
        Rails.logger.info "Error in pregnancy week"
        return "0"
      end
    end

    def self.calulate_age(expected_date,status="expected")
       return "Completed" if status == "completed"
      if expected_date < Date.today

        diff_days = (Date.today - expected_date).to_i#.days 
        text = " overdue"
      elsif (expected_date - 40.weeks) > Date.today
        diff_days = 0
        text = ""
      else
        diff_days = (Date.today - (expected_date - 40.weeks)).to_i 
        text = ""
      end
      weeks = diff_days/7
      days = diff_days%7
      age = []
      age << weeks.to_s + " week" if   weeks == 1
      age << weeks.to_s + " weeks" if weeks > 1 
      age << days.to_s  + " day" if days == 1
      age << days.to_s  + " days" if days > 1
      age.join(" and ") + text
    end

    def preg_cond(option={})
      if option[:week]
        week = option[:week]
         start_time = (week - 1).weeks + 1.day
        (self.expected_on - 40.weeks + start_time).to_date <= Date.today 
      elsif option[:days]
        start_time = option[:days].to_i
        (self.expected_on - 40.weeks + start_time) <= Date.today 
      else
        false
      end   
    end
    
    def get_country_code(options={})
      pregnancy = self
      if pregnancy.member_id.present?
        member = Member.find(pregnancy.member_id)
        member_country_code = member.user.country_code || "UK" rescue "UK"
      else
        member = Member.find(pregnancy.expected_member_id)
        member_country_code = User.where(member_id:member.family_mother_father.first.to_s).first.country_code rescue "UK"
        member_country_code = member_country_code || "UK"
      end
      country_code = Member.sanitize_country_code(member_country_code)
    end

    def self.get_pregnancy_week_detail(pregnancy_id,pregnancy_week,options={})
      pregnancy = Pregnancy.where(id:pregnancy_id).last
      country_code = pregnancy.get_country_code(options) rescue ["GB"]
      # member = Member.find(pregnancy.expected_member_id)
      # member_country_code = User.where(member_id:member.family_mother_father.first.to_s).first.country_code rescue "UK"
      # member_country_code = member_country_code || "UK"
      # country_code = Member.sanitize_country_code(member_country_code)
      if !country_code.include?("GB")
        options[:show_nhs_syndicated_content] = false
      end
      pregnancy_week_detail = ::System::Pregnancy::PregnancyWeekDetail.where(:country_code.in=> country_code, pregnancy_week:pregnancy_week,status:"Active").last
      if pregnancy_week_detail.blank?
        country_code = Member.sanitize_country_code("uk")
        pregnancy_week_detail = ::System::Pregnancy::PregnancyWeekDetail.where(:country_code.in=> country_code, pregnancy_week:pregnancy_week,status:"Active").last || System::Pregnancy::PregnancyWeekDetail.new
      end
      pregnancy_week_detail.formatted_data(options) rescue {}
    end
    
    def delete_pregnancy(current_user,member,api_version=1)
      pregnancy = self
      options = {:current_user=>current_user,:log_child_deleted_event=>false,:member_id=>member.id,pregnancy: 'delete'}
      family_id = options[:family_id] || FamilyMember.where(member_id:pregnancy.expected_member_id).last.family_id rescue nil
      # report_event = Report::Event.where(name:"pregnancy deleted from to family").last
      # ::Report::ApiLog.log_event(current_user.user_id,family_id,report_event.custom_id,options) rescue nil
      member.member_nutrients(api_version,current_user).delete_all rescue nil
      member.medical_events.delete_all rescue nil
      member.article_refs.delete_all rescue nil
     
      delete_kick_count_data(current_user, api_version, options)
      member.delete_child_member(member.id,options)
      Pregnancy.where(id:pregnancy.id).delete_all
    end
    
    def delete_kick_count_data(current_user, api_version=1, options = {})
      begin
        Pregnancy::KickCounter::ScheduledKick.delete_all_system_upcoming_data_for_pregnancy(current_user,id)
        Pregnancy::KickCounter::Scheduler.where(pregnancy_id: id).delete_all
        Pregnancy::KickCounter::KickCountDetail.where(pregnancy_id: id).delete_all unless options[:pregnancy] == 'mark_as_complete'
      rescue Exception=>e
        Rails.logger.info "Error in deleting the pregnancy #{current_user}.inspect}"
        Rails.logger.info e.message
      end
    end

    def add_kick_counts(current_user, api_version=1)
      begin
        params = {}
        kick_scheduled_week = PREGNANCY_MAX_WEEK - KICK_COUNTER_DEFAULT_WEEK
        kick_counter_start_date = expected_on.to_date - kick_scheduled_week.weeks

        kick_counter_start_date = (kick_counter_start_date.to_date < created_at.to_date) ?
                                                created_at : kick_counter_start_date
        Pregnancy::KickCounter::ScheduledKick.delete_all_system_upcoming_data_for_pregnancy(current_user,id)
        params[:scheduler_data] =
                          {
                            kick_counter_start_date: kick_counter_start_date,
                            reminder_time: KICK_COUNTER_START_TIME,
                            pregnancy_id: self.id,
                            added_by: 'system'
                          }

        Pregnancy::KickCounter::Scheduler.create_update_kick_schedule(current_user, params)
      rescue Exception => e
        Rails.logger.info e.message
        Rails.logger.info "*******************Error in add kick count********************"
      end
    end
  
  def tool_graph_data
    pregnancy  = self
    pregnancy_id = pregnancy.id
    data = []
    # BloodPressure
    bp_data = Pregnancy::HealthCard::BloodPressure.where(pregnancy_id:pregnancy_id).order_by("test_date desc")
    last_updated_bp_record = bp_data.first || Pregnancy::HealthCard::BloodPressure.new
    last_bp_updated_record  = last_updated_bp_record.updated_bp.gsub(",", " at").gsub("BPM", " bpm") rescue nil
    chart_data,week_age,combine_data = Pregnancy::HealthCard::BloodPressure.chart_data_value(pregnancy,bp_data)
    who_chart_data,who_min_max_chart_axis = Pregnancy::HealthCard::BloodPressure.who_chart_data
    bp_min_max_axis = Pregnancy::HealthCard.chart_x_y(combine_data + who_chart_data[:systolic][0] + who_chart_data[:diastolic][0])
    bp_status = last_updated_bp_record.bp_result #rescue 'Normal'
    updated_at_text = Pregnancy.update_at_text_value(last_updated_bp_record.updated_at )
    bp_chart_data = {
        :who_chart_data=> 
          { :systolic => who_chart_data[:systolic][0], 
            :diastolic=> who_chart_data[:diastolic][0]
          } , 
        :systolic_chart_data=>chart_data[:systolic_chart_data],
        :pulses_chart_data=>chart_data[:pulses_chart_data],
        :diastolic_chart_data=>chart_data[:diastolic_chart_data], 
        :chart_min_max_axis=>bp_min_max_axis, 
        :last_updated_record=>last_bp_updated_record,
        :status=>bp_status,
        :updated_at_text => updated_at_text
      }

    data << {:syndicated_content=>Pregnancy::HealthCard::BloodPressure.nhs_syndicate_content(pregnancy), :chart_type=>"Blood Pressure",:tool_identifier=>'Pregnancy Health Card', :chart_data=>bp_chart_data, :title=>"Blood Pressure",:last_record=>last_bp_updated_record}


    hemoglobin_data = Pregnancy::HealthCard::Hemoglobin.where(pregnancy_id:pregnancy_id).order_by("test_date desc")
    last_hemoglobin_record = hemoglobin_data.first || Pregnancy::HealthCard::Hemoglobin.new
    last_hemoglobin_updated_record  = last_hemoglobin_record.last_updated_record rescue nil
    chart_data = hemoglobin_data.present? ? hemoglobin_data.collect{|rec| [pregnancy.pregnancy_week(rec.test_date),rec[:hemoglobin_count].to_f.round(1)]} : []
    who_chart_data = WhoData::Pregnancy::Chart.hemoglobin
    hemoglob_result = last_hemoglobin_record.hemoglob_result
    
    updated_at_text = Pregnancy.update_at_text_value(last_hemoglobin_record.updated_at)
    haemoglobin_chart_min_max_axis = Pregnancy::HealthCard.chart_x_y(chart_data + who_chart_data[:low][0] + who_chart_data[:high][0])
    haemoglobin_chart_data = {:who_chart_data=> 
                                {:low=> who_chart_data[:low][0],
                                  :high=> who_chart_data[:high][0] 
                                },
                              :status => hemoglob_result,
                              :updated_at_text => updated_at_text,
                              :last_updated_record=>last_hemoglobin_updated_record, 
                              :chart_data=> chart_data,
                              :chart_min_max_axis=> haemoglobin_chart_min_max_axis 
                            }

    data << {:syndicated_content=>Pregnancy::HealthCard::Hemoglobin.nhs_syndicate_content(pregnancy),:chart_type=>"Haemoglobin",:tool_identifier=>'Pregnancy Health Card', :chart_data=>haemoglobin_chart_data, :title=>"Haemoglobin",:last_record=>last_hemoglobin_updated_record}


    blank_values = ["0", "0.0", "", nil]
    bmi_chart,weight_chart,who_data = Pregnancy::HealthCard::Measurement.chart_info(pregnancy)
    chart_min_max_axis = Pregnancy::HealthCard.chart_x_y(bmi_chart + who_data[:min][0] + who_data[:max][0])
    last_bmi_record = pregnancy.pregnancy_measurements.not_in(:bmi => blank_values).order_by("test_date desc").first   || Health.new
    updated_at_text = Pregnancy.update_at_text_value(last_bmi_record.updated_at)
     
    measurement_chart_data = {
      :chart_min_max_axis => chart_min_max_axis,
      :who_chart_data => {min: who_data[:min][0], max: who_data[:max][0]},
      :updated_at_text => updated_at_text,
      :last_updated_record=>last_bmi_record.bmi, 
      :status=>'',
      :bmi_chart=>bmi_chart,
    }  
    data << {:syndicated_content=>Pregnancy::HealthCard::Measurement.nhs_syndicate_content(pregnancy), :chart_type=>"Measurement",:tool_identifier=>'Pregnancy Health Card', :chart_data=>measurement_chart_data, :title=>"BMI",:last_record=>last_bmi_record.bmi}

   {:pregnancy_id=>pregnancy_id,:member_id=>pregnancy.expected_member_id,:charts=>data}
  end

  def self.update_at_text_value(updated_at_val)
    updated_text_value = "Update now"
    updated_at_value = ApplicationController.helpers.calculate_age4(Date.today,updated_at_val,level=1) rescue nil
    if updated_at_value.blank?
      updated_at_text = updated_text_value
    elsif updated_at_value == "today"
      updated_at_text = "Updated #{updated_at_value}" rescue updated_text_value
      
    else  
      updated_at_text = "Updated #{updated_at_value} ago" rescue updated_text_value
    end
    updated_at_text
  end

end
