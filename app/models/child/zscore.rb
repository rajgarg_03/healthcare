class Child::Zscore

  

  def self.get_tool_id
    begin
      tool_name = "Z score"
      tool_id = Nutrient.where(identifier:"Z score").last.id
    rescue Exception=>e 
      nil
    end
  end

  def self.calculate_zscore(member_id,zscore_data_for_sample=false,options={:measurement_id=>nil})
  
    return zscore = {"height"=>0,"weight"=>0,"bmi"=>0} if zscore_data_for_sample
    zscore = {}
    who_data = {}  
    member_id = member_id.to_s
    member = Member.find(member_id)
    gender = member.male? ? 1 : 2
      
    update_at_value = options[:measurement_id].present? ? Health.find(options[:measurement_id]).updated_at.in_time_zone : Health.where(member_id:member_id).order_by("updated_at desc").limit(1).first.updated_at
    observed_measurement = {}
    observed_measurement["height"] = Health.where(:updated_at.lte => update_at_value, member_id: member_id).not_in("height"=>[0,0.0,nil,""]).order_by("updated_at asc").limit(1).last.height.to_f rescue nil
    observed_measurement["weight"] =  Health.where(:updated_at.lte => update_at_value,member_id: member_id).not_in("weight"=>[0,0.0,nil,""]).order_by("updated_at asc").limit(1).last.weight.to_f rescue nil 
    observed_measurement["bmi"] =  Health.where(:updated_at.lte => update_at_value,member_id: member_id).not_in("bmi"=>[0,0.0,nil,""]).order_by("updated_at asc").limit(1).last.bmi.to_f rescue nil
  
    # last_measurement = Health.where(member_id:member_id).order_by("updated_at asc").limit(1).last 
    age =  ApplicationController.helpers.calculate_age(member.birth_date,update_at_value) rescue nil
    age_in_month = Milestone.convert_to_months(age)
     
    who_data["height"] = WhoHeight.where(gender:gender,month:age_in_month).in(measure_unit: nil).last
    who_data["weight"] = WhoWeight.where(gender:gender,month:age_in_month).in(measure_unit: nil).last
    who_data["bmi"] = WhoBmi.where(gender:gender,month:age_in_month).last
    
    zscore["height"] =   (observed_measurement["height"] - who_data["height"].M.to_f).to_f/(who_data["height"].M.to_f * who_data["height"].S.to_f) rescue nil
    zscore["weight"] =   (observed_measurement["weight"] - who_data["weight"].M.to_f).to_f/(who_data["weight"].M.to_f * who_data["weight"].S.to_f) rescue nil
    zscore["bmi"] =     (observed_measurement["bmi"] - who_data["bmi"].M.to_f).to_f/(who_data["bmi"].M.to_f * who_data["bmi"].S.to_f) rescue nil
    
    zscore
  end
  
  def self.get_calculated_zscore(member_id,zscore_data_for_sample=false,options={})
    return zscore = {"height"=>0,"weight"=>0,"bmi"=>0} if zscore_data_for_sample
    zscore = {}
    begin
      last_measurement = Health.where(member_id:member_id).order_by("updated_at asc").limit(1).last
      if last_measurement["bmi_zscore"].blank?
        zscore = Child::Zscore.calculate_zscore(member_id,zscore_data_for_sample,options)
      else
        zscore["height"] = last_measurement["height_zscore"]
        zscore["weight"] = last_measurement["weight_zscore"]
        zscore["bmi"] = last_measurement["bmi_zscore"]
      end
    rescue Exception=> e
      Rails.logger.info "Error in get_calculated_zscore"
      Rails.logger.info e.message
    end
    zscore
  end


  def self.zscore_to_text(zscore,round_off_val=2)
    data = {}
    zscore.each do |measurement,score| 
      data[measurement] = score.present? ? score.to_f.round_to(round_off_val).to_s : nil
    end
    data
  end
  
  def self.zscore_analysis_for_height(member,zscore_for_height)
    zscore = zscore_for_height
    if zscore > 3
      {:current_status =>{:status=>"Green", :message=>"#{member.first_name} is very tall"}, :special_msg_for_box=> "Tallness is rarely a problem, especially if parents are also tall." }
    elsif zscore <= 3 && zscore >2
      {:current_status =>{:status=>"Green", :message=>"#{member.first_name} seems to be quite tall among the peer group."}, :special_msg_for_box=>nil }
    elsif zscore <= 2 && zscore >1
      {:current_status =>{:status=>"Green", :message=>"#{member.first_name} is taller than average but of normal height."}, :special_msg_for_box=>nil }
    elsif zscore <= 1 && zscore >-1
      {:current_status =>{:status=>"Green", :message=>"#{member.first_name} is of average height among the peer group."}, :special_msg_for_box=>nil }
    elsif zscore <= -1 && zscore >-2
      {:current_status =>{:status=>"Green", :message=>"#{member.first_name} is shorter than average but of normal height."}, :special_msg_for_box=>nil }
    elsif zscore <= -2 && zscore >=-3
      {:current_status =>{:status=>"Amber", :message=>"The record suggests stunted growth. May need further assessment."}, :special_msg_for_box=>nil }
    elsif zscore < -3
      {:current_status =>{:status=>"Amber", :message=>"The record suggests severely stunted growth. Need further assessment by a doctor."}, :special_msg_for_box=>nil }
    end
  end

  def self.zscore_analysis_for_weight(member,zscore_for_weight)
    zscore = zscore_for_weight
    if zscore > 3
      {:current_status =>{:status=>"Amber", :message=>"The weight record suggests growth problem. This will be better assessed from BMI record."}, :special_msg_for_box=> nil }
    elsif zscore <= 3 && zscore >2
      {:current_status =>{:status=>"Amber", :message=>"The weight record suggests growth problem. This will be better assessed from BMI record."}, :special_msg_for_box=>nil }
    elsif zscore <= 2 && zscore >1
      {:current_status =>{:status=>"Amber", :message=>"The weight record suggests growth problem. This will be better assessed from BMI record."}, :special_msg_for_box=>nil }
    elsif zscore <= 1 && zscore >-1
     {:current_status => {:status=>"Green", :message=>"#{member.first_name} is of normal weight."}, :special_msg_for_box=>nil }
    elsif zscore <= -1 && zscore >-2
     {:current_status => {:status=>"Green", :message=>"#{member.first_name} is lighter than the average but of normal weight."}, :special_msg_for_box=>nil }
    elsif zscore <= -2 && zscore >=-3
     {:current_status => {:status=>"Amber", :message=>"The record suggests Underweight."}, :special_msg_for_box=>nil }
    elsif zscore < -3
      {:current_status =>{:status=>"Amber", :message=>"The record suggests Severely underweight."}, :special_msg_for_box=>nil }
    end
  end


  def self.zscore_analysis_for_bmi(member,zscore_for_bmi)
    zscore = zscore_for_bmi
    data = {}
    
    if zscore > 3
      {:current_status =>{:status=>"Amber", :message=>"The record suggests obese status."}, :special_msg_for_box=> nil }
    
    elsif zscore <= 3 && zscore >2
     {:current_status => {:status=>"Amber", :message=>"The record suggests overweight status."}, :special_msg_for_box=>nil }
    elsif zscore <= 2 && zscore >1
      {:current_status =>{:status=>"Amber", :message=>"The record suggests possible risk of overweight."}, :special_msg_for_box=>nil }
    elsif zscore <= 1 && zscore >-1
      {:current_status =>{:status=>"Green", :message=>"#{member.first_name}’s BMI is normal."}, :special_msg_for_box=>nil }
    elsif zscore <= -1 && zscore >-2
      {:current_status =>{:status=>"Green", :message=>"#{member.first_name}’s BMI is normal."}, :special_msg_for_box=>nil }
    elsif zscore <= -2 && zscore >=-3
      {:current_status =>{:status=>"Amber", :message=>"The record suggests wasted status."}, :special_msg_for_box=>nil }
    elsif zscore < -3
      {:current_status =>{:status=>"Amber", :message=>"The record suggests severely wasted status."}, :special_msg_for_box=>nil }
    end
  end
  
  def self.zscore_analysis(zscore,member)
    zscore_analysis = {}
    zscore_analysis["height"] = zscore_analysis_for_height(member,zscore["height"]) if zscore["height"].present?
    zscore_analysis["weight"] = zscore_analysis_for_weight(member,zscore["weight"]) if zscore["weight"].present?
    zscore_analysis["bmi"] =    zscore_analysis_for_bmi(member,zscore["bmi"]) if zscore["bmi"].present?
    data = []
    zscore_analysis.each do |k,v|
      special_msg_for_box = v.delete(:special_msg_for_box)
      data << zscore_analysis[k].merge(:trend_line=>{:message=>nil,:status=>nil},:type=>k,:suggestions=>nil, :suggested_actions=>special_msg_for_box).delete_blank
    end
    data
  end
  
  #who_zscore_data = [{who_height_chart_data=>{"zscore_-3":}..month:},{who_weight_chart_data=>{"zscore_-3":}..month:}]
  #member_data = {:height=>[ [xaxis_data,y_axis_data]] },:weight=>[]}
  def self.chart_min_max_data(who_zscore_data,member_data)
    data = {}
    key_map = {"who_height_chart_data"=>"height", "who_weight_chart_data"=>"weight","who_bmi_chart_data"=>"bmi"}
    ["height","weight","bmi"].each do |a|
       x =  member_data[a].to_h rescue {}
       who_month_data = who_zscore_data[a].delete(:month)
       sorted_y_data = (who_zscore_data[a].values + x.values).flatten.sort
       sorted_x_data = (who_month_data +   x.keys).flatten.sort
       who_zscore_data[a][:month] = who_month_data
       data["#{a}_min_max_axis"] = {:min_x=>sorted_x_data[0],:max_x=>sorted_x_data[-1],:min_y=>sorted_y_data[0],:max_y=>sorted_y_data[-1] }
    end
   data
  end
   

end