class Child::VisionHealth::PurchaseHistory

  include Mongoid::Document  
  include Mongoid::Timestamps 

  field :family_id, type: String
  field :member_id, type: String #subscription taken by
  field :purchase_type, type:String # free,premium
  field :subscription_level, type:Integer #1,2
  field :total_attempt_taken, type:Integer #1,2
  field :total_attempt_allowed, type:Integer #users total test attempt available after purchase or expire
  field :platform, type:String # ios/android
  field :purchase_receipt, type:String
  field :total_attempt_added_by_purchase, type:Integer #
  field :purchase_date, type:Time
  SubscriptionLevel = {"free"=>0,"premium"=>1}

  index({family_id:1})
end