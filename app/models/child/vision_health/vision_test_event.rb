class Child::VisionHealth::VisionTestEvent
	include Mongoid::Document  
	include Mongoid::Timestamps 

	field :member_id, type: String
	field :event_type, type:String #schedule, skip
    
    index({member_id:1}) 
    index({event_type:1}) 

	validates_uniqueness_of :member_id

	def self.add_event(member_id,event_type)
		event = Child::VisionHealth::VisionTestEvent.where(:member_id=>member_id)
		if event == nil 
			event = Child::VisionHealth::VisionTestEvent.new 
			event.member_id = member_id
		end	
		event.event_type = event_type
	end

	def self.get_event(member_id, options={})
		return Child::VisionHealth::VisionTestEvent.where(:member_id=>member_id).first
	end
end