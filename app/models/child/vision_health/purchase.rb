class Child::VisionHealth::Purchase

  include Mongoid::Document  
  include Mongoid::Timestamps 

  field :family_id, type: String
  field :member_id, type: String #subscription taken by
  field :purchase_type, type:String # free,premium
  field :subscription_level, type:Integer #1,2
  field :total_attempt_taken, type:Integer #1,2
  field :total_attempt_allowed, type:Integer #users total test attempt available after purchase or expire
  field :platform, type:String # ios/android
  field :purchase_receipt, type:String
  field :total_attempt_added_by_purchase, type:Integer #
  field :purchase_date, type:Time
  field :admin_credit, type:Integer
  SubscriptionLevel = {"free"=>0,"premium"=>1}

  index({family_id:1})
  
  def self.allowed_attempt_setting
    # ::System::Settings.where(:name=>"cvtmeSettings").last.setting_values.with_indifferent_access
    ::Child::VisionHealth::Purchase.purchase_detail_setting
  end

  def self.purchase_detail_setting
    ::System::Settings.where(:name=>"cvtmeSettings").last.setting_values.with_indifferent_access
  end
  

  def self.update_family_purchase_for_cvtme(current_user,api_version,purchase_type,family_id,options)
    subscription_level = ::Child::VisionHealth::Purchase::SubscriptionLevel[purchase_type]
    cvtme_purchase = ::Child::VisionHealth::Purchase.where(family_id:family_id).last
    total_attempt_allowed = ::Child::VisionHealth::Purchase.allowed_attempt_setting[purchase_type].to_i
    platform = options[:platform]
    purchase_receipt = options[:purchase_receipt]
    purchase_history_status = options.delete(:update_purchase_history)
    sync_status = options.delete(:sync_data)

    if cvtme_purchase.nil?
      purchase_date = subscription_level == 0 ? nil : Time.now.in_time_zone
      total_attempt_added_by_purchase = total_attempt_allowed.to_i
      cvtme_purchase = ::Child::VisionHealth::Purchase.create(purchase_date:purchase_date,:purchase_receipt=>purchase_receipt,:total_attempt_added_by_purchase=>total_attempt_added_by_purchase, :platform=>platform,:total_attempt_taken=>0,total_attempt_allowed:total_attempt_allowed,subscription_level:subscription_level, purchase_type:purchase_type, family_id:family_id,member_id:current_user.id)
    else
      if total_attempt_allowed != -1
        total_attempt_added_by_purchase = cvtme_purchase.total_attempt_added_by_purchase.to_i + total_attempt_allowed.to_i
        existing_allowed_attempt_allowed = cvtme_purchase.total_attempt_allowed.to_i
        total_attempt_allowed = total_attempt_allowed + existing_allowed_attempt_allowed
      else
        total_attempt_allowed = -1
        total_attempt_added_by_purchase = -1
      end
      cvtme_purchase.update_attributes({member_id:current_user.id,purchase_date: Time.now.in_time_zone,:total_attempt_added_by_purchase=>total_attempt_added_by_purchase, :purchase_receipt=>purchase_receipt, :platform=>platform, total_attempt_allowed:total_attempt_allowed,subscription_level:subscription_level,:purchase_type=>purchase_type})
    end
    options[:status] = true
    # create purchase history
    family_id = cvtme_purchase.family_id
    if purchase_history_status == true
      Rails.logger.info "...........create hitory for cvtme purchase #{cvtme_purchase.id}........"
       history_data = cvtme_purchase.attributes
       history_data.delete("_id")
      Child::VisionHealth::PurchaseHistory.new(history_data).save
    end
    if sync_status == true
      member_ids = FamilyMember.where(fmily_id:family_id,:role.in =>FamilyMember::CHILD_ROLES).pluck(:member_id)
      member_ids.each do |member_id|
        begin
          Rails.logger.info "...........data sync for member_id : #{member_id}........"
          member = Member.find(member_id)
          data = Child::VisionHealth::TestColorVisionApi.get_results(member_id)
          Child::TestColorVisionApisionHealth::CvtTest.create_or_update_record(current_user,options[:api_version],data,member,options) if data.present? 
        rescue Exception =>e 
          Rails.logger.info "...........Issue in cvtmr data sync........"
          Rails.logger.info e.message
        end
      end
    end
    ::Report::ApiLog.log_event(current_user.user_id,cvtme_purchase.family_id,238,options) rescue nil
    #cvtme_purchase.reload rescue nil
    cvtme_purchase
  end

  def update_attempt_count(current_user,api_version,attempt_count,options={})
    cvtme_purchase = self
    cvtme_purchase.update_on_purchase_expired(current_user,api_version,options)
    # cvtme_purchase.reload
    total_available_attempt = cvtme_purchase.available_attempt_count 
    # if cvtme_purchase.total_attempt_allowed == -1 && total_available_attempt > 0
      cvtme_purchase_attempt_count = cvtme_purchase.total_attempt_taken.to_i + attempt_count
      cvtme_purchase.update_attribute(:total_attempt_taken,cvtme_purchase_attempt_count)
    # end
    # cvtme_purchase.reload
    if cvtme_purchase.available_attempt_count == 0
      purchase_type = "free"
      subscription_level = ::Child::VisionHealth::Purchase::SubscriptionLevel[purchase_type]
      cvtme_purchase.update_attributes({:subscription_level=>subscription_level,:purchase_type=>purchase_type})
    end

    cvtme_purchase
  end

  def update_on_purchase_expired(current_user,api_version,options={})
    cvtme_purchase = self
    setting = ::Child::VisionHealth::Purchase.purchase_detail_setting
    expiry_in_days = setting[:total_expiry_in_days].to_i
    return false if expiry_in_days == -1
    return false if cvtme_purchase.purchase_date.blank?
    if (cvtme_purchase.purchase_date + expiry_in_days.days) < Date.today
      total_attempt_allowed = cvtme_purchase.total_attempt_taken
      cvtme_purchase.update_attributes(total_attempt_allowed:total_attempt_allowed)
      report_event = ::Report::Event. where(name:'update cvtme purchase attempt count on expiry').last
      current_user = current_user || Member.find(cvtme_purchase.member_id)
      ::Report::ApiLog.log_event(current_user.user_id,cvtme_purchase.family_id,report_event.custom_id,options) rescue nil
    end
  end

  def self.default_subscription(family_id)
    purchase_type = "free"
    subscription_level = ::Child::VisionHealth::Purchase::SubscriptionLevel[purchase_type]
    total_attempt_allowed = ::Child::VisionHealth::Purchase.allowed_attempt_setting[purchase_type]
    cvtme_purchase = ::Child::VisionHealth::Purchase.new(family_id:family_id, :total_attempt_taken=>0,total_attempt_allowed:total_attempt_allowed,subscription_level:subscription_level, purchase_type:purchase_type)
    cvtme_purchase.save 
    cvtme_purchase
  end
  
  def get_expiry_days(cvtm_setting_values,options={})
    cvtme_purchase = self 
    expiry_in_days_setting = cvtm_setting_values[:total_expiry_in_days]
    return -1 if expiry_in_days_setting == "NA" || expiry_in_days_setting == -1
    return 0 if cvtme_purchase.purchase_date.blank?
    expire_in_days = cvtm_setting_values[:total_expiry_in_days].to_i - (Date.today - cvtme_purchase.purchase_date.to_date).to_i
    expire_in_days > 0 ? expire_in_days : 0
  end


  def available_attempt_count
    cvtme_purchase = self
    if cvtme_purchase.total_attempt_allowed.to_i == -1 
      total_attempt_remaining = 10
    elsif cvtme_purchase.total_attempt_allowed.to_i > cvtme_purchase.total_attempt_taken.to_i
      total_attempt_remaining = cvtme_purchase.total_attempt_allowed.to_i - cvtme_purchase.total_attempt_taken.to_i
    else
      total_attempt_remaining = 0
    end
    total_attempt_remaining
  end
  
  def can_access_cvtme_test_result?
    cvtme_purchase = self
    available_attempt_count = cvtme_purchase.available_attempt_count
    require_cvtme_purchase = available_attempt_count <= 0
    if require_cvtme_purchase == true && cvtme_purchase.total_attempt_added_by_purchase.to_i < 1 
      false
    else
      true
    end
  end


  def accessible_record_count
    cvtme_purchase = self
    if cvtme_purchase.total_attempt_added_by_purchase.to_i <= -1
      nil
    else
      cvtme_purchase.total_attempt_added_by_purchase
    end
  end

end