class Child::VisionHealth::TestColorVisionApi

# https://demo.testingcolorvision.com/v2/api/results?api_key=e4da3b7fbbce2345d7772b0674a318d5&AICC_SID=608D8323-63C8-43A9-AD97-64C134E4DA16")
  def self.get_result(aicc_sid)
    begin
      end_point = APP_CONFIG["cvtme_api_url"] || ""
      api_key = APP_CONFIG['test_color_vision_api_key']      
      uri = URI(end_point + '/results')
      uri.query = "api_key="+api_key+"&AICC_SID="+aicc_sid
      request = Net::HTTP::Get.new(uri.request_uri)
      response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
          http.request(request)
      end
      return JSON.parse (response.body)
    rescue Exception => e 
      {}
    end
  end

  def self.get_results(member_id)
    begin
      end_point = APP_CONFIG["cvtme_api_url"] || ""
      last_sync_date = Child::VisionHealth::CvtTest.last_sync_date(member_id)
      uri = URI(end_point + '/all_results')
      api_key = APP_CONFIG['test_color_vision_api_key']
      query_string = "api_key="+api_key+"&patient_id="+member_id
      if last_sync_date.present?
        query_string = query_string + "&last_record_date=#{last_sync_date}"
      end      
      uri.query = query_string

      request = Net::HTTP::Get.new(uri.request_uri)
      response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
          http.request(request)
      end
      response_data = JSON.parse (response.body)
      if (response_data["error"].present? rescue false)
        data = []
      else
        data = response_data
      end
    rescue Exception=>e 
      data = []
    end
    data
  end

end



