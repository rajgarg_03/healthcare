class Child::VisionHealth::CvtTest

  include Mongoid::Document  
  include Mongoid::Timestamps 

  field :aicc_sid, type: String
  field :member_id, type: String #:default=>[] #keywords passed by admin
  
  field :added_on, type:Time
  field :updated_on, type:Time

  #fields specifically for videos
  field :test_token, type:String
  field :calculated_results, type:String
    # field :results, type: Array, :default=>[]

  field :suggested_action_description, type:String, :default => ""
  field :show_suggested_action, type:Boolean, :default => false
  field :show_book_appointment, type:Boolean, :default => false

  # index({member_id:1})
  # index({checkup_date:1})
  has_many :results, class_name: "Child::VisionHealth::CvtTestResult"
  validates_uniqueness_of :aicc_sid
  
  index({member_id:1}) 
  index({aicc_sid:1}) 

  def self.create_or_update_record(current_user,api_version,data, member, options={})
    member_id = member.id
    family_id = options[:family_id]
    cvtme_purchase = ::Child::VisionHealth::Purchase.where(family_id:family_id).last
    if cvtme_purchase.blank?
      purchase_type = "free"
      cvtme_purchase = ::Child::VisionHealth::Purchase.update_family_purchase_for_cvtme(current_user,api_version,purchase_type,family_id,options)
    end
    test_added_count = 0
    data.each do |test_data|
      cvt_test = Child::VisionHealth::CvtTest.where(:aicc_sid => test_data["patient"]["org_sess"]).first
      if cvt_test == nil 
        cvt_test = ::Child::VisionHealth::CvtTest.new
        test_added_count = test_added_count + 1
      end
      
      cvt_test.aicc_sid = test_data["patient"]["org_sess"]
      cvt_test.member_id = member_id
      cvt_test.added_on = test_data["created_at"]
      cvt_test.updated_on = test_data["updated_at"]
      cvt_test.test_token = test_data["test_token"]
      cvt_test.calculated_results = test_data["calculated_results"]

      results = test_data["results"]
      results.each do |result|
        if result["screening_status"].downcase == "fail"
          cvt_test.suggested_action_description = "A more detailed test from an ophthalmologist is suggested"
          cvt_test.show_suggested_action = true
          cvt_test.show_book_appointment = true
          break
        end
      end
      cvt_test.save
      Child::VisionHealth::CvtTestResult.create_or_update_record(test_data["results"], cvt_test.aicc_sid, cvt_test.test_token, member_id)
    end
    total_attempt_count = test_added_count
    cvtme_purchase.update_attempt_count(current_user,api_version,total_attempt_count,options)
  end

  def self.fetch_all_test_results(member_id ,options={})
     return Child::VisionHealth::CvtTestResult.where(:aicc_sid=>aicc_sid).entries
  end

  def self.last_sync_date(member_id)
    begin
      sync_date = Child::VisionHealth::CvtTest.where(member_id:member_id).last.created_at
      sync_date.strftime("%Y-%m-%d 00:00:00")
    rescue Exception=>e 
      nil
    end
    sync_date
  end

def self.prepare_result(aicc_sid, member_id, options={}) 
      cvt_test = Child::VisionHealth::CvtTest.where(:aicc_sid => aicc_sid).first
      if cvt_test == nil
        return {}
      end
      cvt_test_results = Child::VisionHealth::CvtTestResult.where(:aicc_sid=>aicc_sid).entries
      cvt_test["result_data"] = cvt_test_results
      child_member = Member.where(:id=>member_id).first.fetch_color_vision_related_info(cvt_test.updated_on)
      cvt_test["child_member_info"] = child_member
      return cvt_test
end

  def self.get_last_test_record(member_id, options={})
    begin
      family = FamilyMember.where(:member_id=>member_id).last.family
      cvtme_purchase = ::Child::VisionHealth::Purchase.where(family_id:family.id).last || ::Child::VisionHealth::Purchase.default_subscription(family.id)
      return {} if !cvtme_purchase.can_access_cvtme_test_result?
      cvt_test = Child::VisionHealth::CvtTest.where(:member_id => member_id).order_by("updated_on desc").first
      
      cvt_test_results = Child::VisionHealth::CvtTestResult.where(:aicc_sid=>cvt_test.aicc_sid).limit(cvtme_purchase.accessible_record_count).entries
      cvt_test["result_data"] = cvt_test_results
      child_member = Member.where(:id=>member_id).first.fetch_color_vision_related_info(cvt_test.updated_on)
      cvt_test["child_member_info"] = child_member
    rescue Exception=>e 
      cvt_test = {}
    end
    cvt_test
  end

  def self.prepare_child_test_results(member_id ,options={})
    begin  
      family = FamilyMember.where(:member_id=>member_id).last.family
      cvtme_purchase = ::Child::VisionHealth::Purchase.where(family_id:family.id).last || ::Child::VisionHealth::Purchase.default_subscription(family.id)
      return [] if !cvtme_purchase.can_access_cvtme_test_result?
      all_tests = Child::VisionHealth::CvtTest.where(:member_id=>member_id).order_by("updated_on desc").limit(cvtme_purchase.accessible_record_count).entries
      child_member = Member.where(:id=>member_id).first
      total_results = []
      all_tests.each do |cvt_test|
        cvt_test["result_data"] = Child::VisionHealth::CvtTestResult.where(:aicc_sid=>cvt_test[:aicc_sid]).entries
        cvt_test["child_member_info"] = child_member.fetch_color_vision_related_info(cvt_test.updated_on)
        total_results.push(cvt_test)
      end
    rescue Exception=>e
      total_results = []
    end
    total_results    
  end

end
