class Child::VisionHealth::CvtTestResult

  include Mongoid::Document  
  include Mongoid::Timestamps 

  field :aicc_sid, type: String
  field :test_token, type: String
  field :member_id, type: String
  
  field :section_id, type: Integer
  field :section_category, type:String
  field :section_type, type:String
  field :num_plates, type: Integer
  field :correct, type: Integer
  field :screening_status, type: String
  field :diagnostic_status, type: String


  belongs_to :cvt_test, class_name: 'Child::VisionHealth::CvtTest', foreign_key: :aicc_sid
  belongs_to :cvt_test, class_name: 'Child::VisionHealth::CvtTest', foreign_key: :test_token

  # validates_uniqueness_of :link

  def self.create_or_update_record(data, aicc_sid, test_token, member_id, options={}) 
    data.each do |result_data|
      result = Child::VisionHealth::CvtTestResult.where(:aicc_sid=>aicc_sid, :section_category=>result_data["section_cat"]).first
      if result == nil 
        result = self.new()
      end
      result.aicc_sid = aicc_sid
      result.test_token = test_token
      result.member_id = member_id

      result.section_id = result_data["section_number"]
      result.section_category = result_data["section_cat"]
      result.section_type = result_data["section_type"]
      result.num_plates = result_data["num_plates"]
      result.correct = result_data["correct"]
      result.screening_status = result_data["screening_status"]
      result.diagnostic_status = result_data["diagnostic_status"]

      result.save
    end 
  end

end
