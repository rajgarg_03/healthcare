class Child::HealthCard::EyeChart
  include Mongoid::Document
  include Mongoid::Timestamps
  field :member_id, type: String
  field :id, type: String
  field :left_spherical_value, type: String
  field :left_cylindrical_value, type: String
  field :left_axis_value, type: String
  field :left_vision_value, type: String
  field :right_spherical_value, type: String
  field :right_cylindrical_value, type: String
  field :right_axis_value, type: String
  field :right_vision_value, type: String
  field :date, type: Date

  belongs_to :member

  class << self
    def create_record(params)
      create(params)
    end

    def get_list(member_id)
      eye_data_array = []
      child_eye_data = where(member_id: member_id).order_by("date desc")
      child_eye_data.each do |eye_data|
        eye_data_array << get_detail_in_hash(eye_data)
      end
      eye_data_array
    end

    def order_by_date(member_id)
      eye_data_array = []
      child_eye_data = where(member_id: member_id).order_by("date desc")
      child_eye_data.each do |eye_data|
        eye_data_array << get_detail_in_hash(eye_data)
      end
      eye_data_array
    end

    def update_record(params)
      eye_data = where(id: params[:id]).first
      eye_data.update_attributes(params.except(:member_id, :id))
    end

    private

    def get_detail_in_hash(data)
      data_hash = {}
      data_hash[:id] = data[:id]
      data_hash[:member_id] = data[:member_id]
      data_hash[:left_spherical_value] = data[:left_spherical_value].nil? ? data[:left_spherical_value] : data[:left_spherical_value].to_f.round(2)
      data_hash[:left_cylindrical_value] = data[:left_cylindrical_value].nil? ? data[:left_cylindrical_value] : data[:left_cylindrical_value].to_f.round(2)
      data_hash[:left_axis_value] = data[:left_axis_value].nil? ? data[:left_axis_value] : data[:left_axis_value].to_i
      data_hash[:left_vision_value] = data[:left_vision_value].nil? ? data[:left_vision_value] : data[:left_vision_value].to_i
      data_hash[:right_spherical_value] = data[:right_spherical_value].nil? ? data[:right_spherical_value] : data[:right_spherical_value].to_f.round(2)
      data_hash[:right_cylindrical_value] = data[:right_cylindrical_value].nil? ? data[:right_cylindrical_value] : data[:right_cylindrical_value].to_f.round(2)
      data_hash[:right_axis_value] = data[:right_axis_value].nil? ? data[:right_axis_value] : data[:right_axis_value].to_i
      data_hash[:right_vision_value] = data[:right_vision_value].nil? ? data[:right_vision_value] : data[:right_vision_value].to_i
      data_hash[:date] = data[:date].to_date.to_date4
      member_birth_date = Member.find(data[:member_id]).birth_date rescue Date.today
      age_diff = ApplicationController.helpers.calculate_age2(member_birth_date.to_date, data[:date].to_date)
      data_hash[:member_age] = age_diff
      data_hash[:updated_at] = "Last updated on #{data.date.to_date.to_date4}"
      data_hash
    end
  end #self end

end
