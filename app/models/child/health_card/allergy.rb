class Child::HealthCard::Allergy
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :name, type: String
  field :allergy_symptoms, type:Array 
  field :severity, type: String
  field :allergy_date, type:Date
  field :notes, type:String
  field :category, type:String

  belongs_to :allergy_manager
  belongs_to :member
  
  def self.add_allergy(current_user,params)
    member  = Member.find(params[:member_id])
    allergy_manager_ids = params[:allergy_manager_ids] - (Child::HealthCard::Allergy.where(member_id:params[:member_id]).pluck(:allergy_manager_id).map(&:to_s))
    allergy_managers  = AllergyManager.in(id:allergy_manager_ids)
    if allergy_managers.present?
      allergy_managers.each do |allergy_manager|
        category = allergy_manager.category
        symptoms = allergy_manager.allergy_symptoms rescue nil
        calculated_allergy_date = (member.birth_date + eval(allergy_manager.allergy_date)) rescue nil
        allergy_name = allergy_manager.name
        Child::HealthCard::Allergy.create(category:category, allergy_manager_id: allergy_manager.id, allergy_date: calculated_allergy_date, member_id:params[:member_id],name:allergy_name,allergy_symptoms:symptoms)
      end
    end 
  end

  def update_allergy(current_user,params)
    allergy_symptoms = AllergySymptomManager.in(id:params[:allergy_symptoms]).pluck(:id)
    params[:category] = params[:category] || self.category
    self.update_attributes(category: params[:category], notes: (params[:notes]||self.notes), severity:(params[:severity]||self.severity) ,allergy_date:params[:allergy_date],name:(params[:name]||self.name),allergy_symptoms:allergy_symptoms)
  end
  
  def get_name
    allergy = self
    allergy_manger = AllergyManager.where(id:allergy.allergy_manager_id.to_s).last
    (allergy_manger.name || allergy.name ) rescue allergy.name
  end

  def in_json
    allergy = self
    data = allergy.clone
    data.id = allergy.id
    data.name = data.get_name
    data.allergy_symptoms = AllergySymptomManager.in(id:allergy.allergy_symptoms).map(&:attributes)
    data
  end

  def self.get_list(current_user,member_id)
    data = Child::HealthCard::Allergy.where(member_id:member_id).sort_by{|a| a.get_name.downcase}
    data.map(&:in_json)
  end
end