class Child::ActivityPlanner::Activity
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :title, type: String
  field :participants, type:Array 
  field :category, type:String #Club,Play Date, Birthday Party
   
  field :start_time, type:Time
  field :end_time, type:Time
  field :location_name, type: String # our's home, other's home, other location
  field :location_address, type: String
  #field :status, type: String,:default=>"Estimated"
  belongs_to :member
  
  index({activity_date:1})  
  index({member_id:1})  
  
  def self.month_wise_data(records)
     
    records = records.group_by{|a| a.start_time.strftime('%B %Y')}
    data = {}
    today_date = DateTime.now.in_time_zone.to_date
    month_end_date = DateTime.now.in_time_zone.end_of_month.to_date
    records.each do |time_of_activity,data_val|
      #time_of_activity = time_of_activity.to_date
      # if time_of_activity  < today_date
      #   puts time_of_activity
      #   data['Overdue'] << data_val.map(&:to_json) 
      #   data['Overdue'] = data['Overdue'].flatten 
      # elsif (time_of_activity >= today_date) && time_of_activity <= month_end_date
      #   data['This Month'] << data_val.map(&:to_json)
      #   data['This Month']  = data['This Month'].flatten
      # elsif (time_of_activity >= (today_date + 1.month).at_beginning_of_month)  && (time_of_activity <= (today_date + 1.month).at_end_of_month)
      #   data['Next Month'] << data_val.map(&:to_json)
      #   data['Next Month'] = data['Next Month'].flatten 
      # elsif (time_of_activity > (today_date + 1.month))
      data[time_of_activity] = data[time_of_activity] || []
      data[time_of_activity] << data_val.map(&:to_json) 
      data[time_of_activity] = data[time_of_activity].flatten
        # data[time_of_activity] = data[time_of_activity].flatten
      # end
    end
    data
  end


  def to_json
    obj = self
    data = obj.attributes.to_h
    location = obj.location_address.present? ? obj.location_address : obj.location_name
    data["activity_name"] = "#{obj.category.humanize} at #{location}" 
    data["activity_date"] = obj.start_time.in_time_zone.strftime("%d %B %Y")
    data["start_time"] = obj.start_time.in_time_zone.strftime("%I:%M %P")
    data["end_time"] = obj.end_time.in_time_zone.strftime("%I:%M %P")
    data["participants"] = Child::ActivityPlanner::Participant.in(id:obj.participants).entries#.pluck(:name)
    data
  end
end

#a = Child::ActivityPlanner::Activity.last