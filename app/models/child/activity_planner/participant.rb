class Child::ActivityPlanner::Participant
  include Mongoid::Document
  field :name, type: String

  belongs_to :user
  
  index({user_id:1})

  def self.get_participant_list(user_id,activity_id=nil) 
    all_pariticipant =  Child::ActivityPlanner::Participant.where(user_id:user_id) 
    if activity_id.present?
      participants = Child::ActivityPlanner::Activity.find(activity_id).participants
    else
      participants = []
    end
    data = []
    all_pariticipant.each do |a|
      if participants.present? && participants.include?(a.id.to_s)
        a["added"] = true
      else
        a["added"] = false

      end
      data << a
    end
    data
  end 

end