class Child::AlexaQuiz::QuizDetail
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :quiz_attempt_no, type: Integer
  field :quiz_level, type: Integer
  field :question, type:String 
  field :answer, type:String 
  field :question_type, type:String 
  field :subject, type:String 
  field :answered, type:Integer   #0:incorrect 1:correct: 2:skipped
  field :is_answer_correct, type:Boolean
  field :member_id, type: String 
  field :user_id, type: String
  field :time_taken, type: Integer  #in seconds
  field :platform, type: String

  QUESTIONS_LIMIT = 50

  def self.get_attempt_no(user_id,child_id)
  	Child::AlexaQuiz::QuizDetail.where(user_id:user_id,member_id:child_id).order_by("quiz_attempt_no desc").first.quiz_attempt_no rescue 0
  end

  def self.show_mental_math_banner_status(current_user,options={})
    member_popup = PopupLaunch.where(member_id:current_user.id).last || PopupLaunch.new
    mental_math_free_popup_settings = ::System::Settings.where(name:"mentalMathFreePopupLunch").last
    popup_is_inactive =  ((mental_math_free_popup_settings.blank? || mental_math_free_popup_settings.status != "Active"))
    if !popup_is_inactive && ::Child::AlexaQuiz::QuizDetail.is_mental_math_banner_valid?(current_user,options)
      member_popup_info = member_popup.popup_info  || {}
      mental_math_free_popup = member_popup_info["mental_math_free"] || {}
      popup_setting = mental_math_free_popup_settings.setting_values.with_indifferent_access
      popup_expire_in_days = (popup_setting[:expire_in_days]).to_i
      # Not dismissed and valid to display from start start
      if mental_math_free_popup["skipped"] != true && ((mental_math_free_popup["start_date"] + popup_expire_in_days.to_i.days) >= Date.today rescue false)
        status = true
      else
        status = false
      end
    else
      status = false
    end
    status
  end

  def self.mental_math_banner_data(current_user,option={})
    mental_math_free_popup_settings = ::System::Settings.where(name:"mentalMathFreePopupLunch").last
    data = mental_math_free_popup_settings.setting_values.with_indifferent_access
    data
  end

  def self.is_mental_math_banner_valid?(current_user,options={})
    status = true 
    # Check family exist
    user_families = current_user.user_families
    return false if user_families.blank?
    
    # Not subscribe to premuim subscription
    family_ids = user_families.map(&:id).map(&:to_s)
    premium_subscription_id = SubscriptionManager.premium_subscriptions.pluck(:id).map(&:to_s)
    premuim_subscriptions = Subscription.where(:family_id.in=>family_ids,:subscription_manager.in=>premium_subscription_id).not_expired
    return false if premuim_subscriptions.present?

    # Check child 
    children_ids = current_user.children_ids(user_families).map(&:to_s)
    return false if children_ids.blank?
    
    # Check age for children greater than 3.5 years
    children_with_age_greater_than_3_5_years = ChildMember.where(:id.in=>children_ids, :birth_date => {"$lte"=> (Date.today - (3.years + 5.month) ) }).map(&:id)
    
    # Not expected child 
    expected_member = Pregnancy.where(:expected_member_id.in=>children_with_age_greater_than_3_5_years)
    return false if expected_member.present?
    
    status
    
  end

  def self.save_quiz_detail(current_user,params,api_version=4)
  	child_id = params[:child_id]
  	quiz_level = params[:current_quiz_level]
  	attempt_no = Child::AlexaQuiz::QuizDetail.get_attempt_no(current_user.user_id,child_id) + 1
  	if params[:questions].present?
      params[:questions].each do |question_detail|
        question_detail[:member_id] = child_id
        question_detail[:quiz_attempt_no] = attempt_no
        question_detail[:user_id] = current_user.user_id
        question_detail[:quiz_level] = quiz_level
        Child::AlexaQuiz::QuizDetail.new(question_detail).save
    	end
    end
    child = Member.find(child_id)
    child.update_attribute(:quiz_level,params[:next_quiz_level])
  end

  def create_quiz(params,options={})
    quiz = Child::AlexaQuiz::QuizDetail.create(params[:quiz_details])
    
    family_id = FamilyMember.where(member_id:quiz.member_id).last.family_id
    options[:member_id] = quiz.member_id
    current_user = options[:current_user]
    @user_id = current_user.user_id rescue nil
    ::Report::ApiLog.log_event(@user_id,family_id,31,options) rescue nil

  end

  class << self

    # def get_complete_summary_month_wise(params,current_user,start,_date,end_date, api_version=4,options={})
    #   @child_data = Member.find(params[:member_id])
    #   quiz_data = Child::AlexaQuiz::QuizDetail.where(member_id: @child_data.id, updated_)


    # end

    def get_complete_summary(params,current_user,api_version=4,options={})
      @child_data = Member.find(params[:member_id])
       
      quiz_json_response(current_user,api_version,options)
    end
    
    def get_subscription_detail(current_user,child_id,api_version=4,options={})
      begin
	# Disable subscription box for Mental Math for now
        show_subscription_box_status = false
        subsciption_status = (!show_subscription_box_status)
        return subsciption_status
	family_id = FamilyMember.where(member_id:child_id).last.family_id
        # identifier = 'Mental maths box'
        identifier = 'Mental Maths'
        tool = Nutrient.where(:identifier=>/^#{identifier}$/i).last 
        show_subscription_box_status,free_trial_available_status,free_trial_msg = tool.show_subscription_box_status(current_user,family_id,api_version,options) 
        # subsciption_status,msg,required_level = tool.get_subscription_status(current_user,family_id,api_version,options)
        subsciption_status = (!show_subscription_box_status)
      rescue Exception => e 
        Rails.logger.info "Error inside response_with_quiz_subscription check subscription"
        Rails.logger.info e.message
        subsciption_status = false
      end
      

      subsciption_status
    end
    
    def available_question_count_to_attempt(current_user,subscription_status,child_id,api_version,options={})
      allowed_count = Child::AlexaQuiz::QuizDetail::QUESTIONS_LIMIT
      if subscription_status == true
        available_question_count = -1
      else
        attempted_question_count = Child::AlexaQuiz::QuizDetail.where(member_id:child_id).count
        if allowed_count > attempted_question_count
          available_question_count  = allowed_count - attempted_question_count
        else
          available_question_count  = 0
        end
      end
      available_question_count 
    end

    def valid_to_take_quiz(available_question_count)
      status = false
      if available_question_count == -1
        status = true
      elsif available_question_count > 0
        status = true
      elsif available_question_count == 0
        status = false
      end
      status
    end

    def child_quiz_info(current_user,child_id,api_version,options={})
      subscription_status = Child::AlexaQuiz::QuizDetail.get_subscription_detail(current_user,child_id,api_version,options)
      available_question_count_to_attempt = Child::AlexaQuiz::QuizDetail.available_question_count_to_attempt(current_user,subscription_status,child_id,api_version,options)
      valid_to_take_quiz = Child::AlexaQuiz::QuizDetail.valid_to_take_quiz(available_question_count_to_attempt)
      attempted_question_count = Child::AlexaQuiz::QuizDetail.where(member_id:child_id).count
      {:attempted_question_count=> attempted_question_count, :subscription_status=>subscription_status,:valid_to_take_quiz=>valid_to_take_quiz }
    end

    def quiz_json_response(current_user,api_version=4,options={})
      if options[:start_date] && options[:end_date].present?
        quiz_data = Child::AlexaQuiz::QuizDetail.where(member_id: @child_data.id,:created_at=>{"$gte"=>options[:start_date],"$lte"=>options[:end_date]})
      else
        quiz_data = Child::AlexaQuiz::QuizDetail.where(member_id: @child_data.id)
      end
      # Get child family subscription detail
      child_id = @child_data.id
      subsciption_status = get_subscription_detail(current_user,child_id,api_version,options)
      subscribed = subsciption_status
      args = {
              quiz_data: quiz_data, 
              status: 'complete',
              parent_key: 'mental_math_summary',
              subscribed: subscribed
            }
      quiz_response(current_user,api_version,args)
    end

    def get_daily_summary(params)
     @child_data = Member.find(params[:member_id])
     quiz_data = Child::AlexaQuiz::QuizDetail.
                                          where(member_id: params[:member_id]).
                                          order_by("created_at DESC").
                                          group_by { |d| d.created_at.to_date.to_date3}
      quiz_data_keys = quiz_data.keys.paginate(page: params[:page])
      paginate_date = {}
      quiz_data_keys.each{|x| paginate_date[x]= quiz_data[x]}
      data_hash, data_hash[:mental_math_summary] ={}, {}
      paginate_date.each { |data| 
        data_hash[:mental_math_summary][data[0]] = subject_summary(data[1])
      }
      current_page = params[:page]
      {  current_page: params[:page], total_records: quiz_data.count,
         next_page: quiz_data_keys.next_page,
         total_pages: quiz_data_keys.total_pages, month_order: quiz_data_keys
       }.merge(data_hash)
    end

    private

    def ques_total_answered(data)
      data.count{|x| [0, 1, true, false].include?(x[:answered] || x[:is_answer_correct])} rescue 0
    end

    def correct_answers(data)
      data.count{|x| [1, true].include?(x[:answered] || x[:is_answer_correct]) } rescue 0
    end
    
    def skipped_questions(data)
      data.count{|x| 2 == x[:answered] } rescue 0
    end
    
    def incorrect_answers(data)
      data.count{|x| [0, false].include?(x[:answered] || x[:is_answer_correct]) } rescue 0
    end

    def total_skipped_question_percentage(data)
      skipped_ans = skipped_questions(data)
      percent_of(skipped_ans, data.count)
    end

    def total_incorrect_answer_percentage(data)
      incorrect_ans = incorrect_answers(data)
      percent_of(incorrect_ans, data.count)
    end

    def correct_answers_percentage(data)
      correct_ans = correct_answers(data)
      percent_of(correct_ans, data.count)
    end

    def avg_time_taken_in_secs(data)
      time_sum = data.map(&:time_taken).sum rescue 0
      (time_sum / data.count).round(1) rescue 0
    end

    def questions_answered_percentage(data)
      answered = ques_total_answered(data)
      percent_of(answered, data.count)
    end

    def percent_of(i, n)
      if n > 0
        (i.to_f / n.to_f * 100.0).round(1) rescue 0
      else
        0
      end
    end

    def most_practiced_topic(data)
      data_hash = {}
      data_by_topic = data.group_by(&:question_type)
      data_by_topic.each do |topic_data|
        data_hash[topic_data[0]] = correct_answers(topic_data[1])
      end
      max_answered_topic = max_value_of(data_hash)
      total_questions = data_by_topic[max_answered_topic].count rescue 0
      answered_questions = data_hash[max_answered_topic] rescue 0
      "#{max_answered_topic} (#{answered_questions.to_s}/#{total_questions.to_s})"
    end

    def max_value_of(data_hash)
      max_value = data_hash.max_by{|k,v| v}
      # keys = []
      # data_hash.map{|k, v|  keys << k if v == max_value[1]}
      max_value[0]
    end

    def best_performed_topic(data)
      data_hash = {}
      data_by_topic = data.group_by(&:question_type)
      data_by_topic.each do |topic_data|
        data_hash[topic_data[0]] = correct_answers(topic_data[1])
      end
      max_value_of(data_hash)
    end

    def question_answered_details(data)
      "#{@child_data.name} answered #{ques_total_answered(data)} out of #{data.count} questions\nasked"
    end

    def answer_status(data)
      child_performed(data)
    end

    def child_performed(data)
      json = {}
      member_ids = Child::AlexaQuiz::QuizDetail.pluck(:member_id).uniq
      members_in_same_age = members_in_same_age(member_ids)
      quiz_data_collection = Child::AlexaQuiz::QuizDetail.
                              where(:member_id.in => members_in_same_age).
                              group_by(&:member_id)


      quiz_data_collection.each do |quiz_data|
        json[quiz_data[0]] = correct_answers(quiz_data[1])
      end
      child_marks = json[@child_data.id.to_s]
      average_count = (json.values.sum/json.values.count).to_i rescue 0
      diff_in_marks = percent_of((average_count - child_marks), average_count)
      if diff_in_marks > 0
        "#{@child_data.name}’s was able to answer #{diff_in_marks.to_i}% more than average children of his age"
      elsif diff_in_marks < 0
        "#{@child_data.name}’s was able to answer #{-(diff_in_marks).to_i}% lower than average children of his age"
      else
        "#{@child_data.name}’s was able to answer equals to average children of his age"
      end
    end

    def members_in_same_age(member_ids)
      current_member_age = @child_data.age_in_years
      data = []
      member_ids.each do |member_id|
        member_age = Member.find(member_id).age_in_years rescue 0
        data << member_id if current_member_age == member_age
      end
      data
    end

    def subject_summary(data)
      all_subjects = []
      data_by_date  = data.group_by{|a| a.created_at.to_date.to_s}
      data_by_date.each do |practiced_date,practiced_data|
      data_group_by_subjects = data.where(:id.in=>practiced_data.map(&:id)).group_by(&:subject)
        data_group_by_subjects.each do |subject,subject_data|
           all_subjects << subject_summary_json(subject, subject_data, data)
        end
      end
      all_subjects
    end

    def dummy_subject_summary
      [{
        "total_correct_answer_percentage": 80,
        "total_questions": 32,
        "practiced_date": "23 Jul 2018",
        "practiced_date_formatted": "1 day ago"
      },{
        "total_correct_answer_percentage": 70,
        "total_questions": 12,
        "practiced_date": "23 Jul 2018",
        "practiced_date_formatted": "3 days ago"
      },{
        "total_correct_answer_percentage": 60,
        "total_questions": 60,
        "practiced_date": "23 Jul 2018",
        "practiced_date_formatted": "12 days ago"
      },{
        "total_correct_answer_percentage": 70,
        "total_questions": 32,
        "practiced_date": "23 Jul 2018",
        "practiced_date_formatted": "13 days ago"
      }]
    end

    def mental_maths_feature_type(id)
      id.blank? ? '' : Nutrient.find(id).feature_type
    end

    def show_badges(data)
      true
    end

    def show_summary(data)
      true
    end

    def quiz_response(current_user,api_version=4,*args)
      json, json[args[0][:parent_key]] = {}, {}
      parent_key_json = json[args[0][:parent_key]]
      data = args[0][:quiz_data]
      parent_key_json['current_level'] = @child_data.quiz_level
      parent_key_json['badge_title'], parent_key_json['badge_color'] =
                                    badges_level(data)
      parent_key_json['show_badges'] = show_badges(data)
      parent_key_json['total_questions'] = data.count

      subscription_status = is_enough_subscription(data, args[0][:subscribed])
      linked_devices_list = linked_devices(@child_data, current_user)
      user_deviced_linked = is_user_deviced_linked(current_user)

      parent_key_json['practice_status'], parent_key_json['practice_status_color'] =
                                  practice_status(data, user_deviced_linked, subscription_status)

      if data.count > 0
        parent_key_json['total_practiced_days'] = total_practiced_days(data)
        parent_key_json['last_session_held'] = last_session_held(data)
        parent_key_json['quiz_frequency'], parent_key_json['quiz_frequency_text'], parent_key_json['quiz_frequency_status'] =    quiz_frequency(data)
        parent_key_json['avg_questions_per_day'] = parent_key_json['total_questions'].to_i/parent_key_json['total_practiced_days'].to_i rescue 0
      end

      mental_maths_box_tool_details = mental_maths_tool_details(current_user,api_version,subscription_status)

      parent_key_json['show_summary'] = show_summary(data)
      parent_key_json['is_user_deviced_linked'] = user_deviced_linked
      parent_key_json['is_sufficient_subscription'] = subscription_status
      # parent_key_json["box_tool_id"] = mental_maths_box_tool_details[:tool_id]
      # parent_key_json["box_feature_type"] = mental_maths_box_tool_details[:feature_type]
      # parent_key_json["show_box"] = mental_maths_box_tool_details[:show_box_status]
      parent_key_json["max_level"] = max_level()
      parent_key_json['practiced_topics'] = practiced_topics(data) if data.count > 0
      parent_key_json['linked_devices'] = linked_devices_list

      unless mental_maths_box_tool_details[:show_box_status]
        parent_key_json['answered_stats'] = answered_stats(data)
        parent_key_json['daily_records'] = subject_summary(data) 
      else
        parent_key_json['answered_stats'] = dummy_answered_stats()
        parent_key_json['daily_records'] = dummy_subject_summary()

      end

      json
    end

    def max_level
      10
    end

    def mental_maths_tool_details(current_user,api_version=4,subscription_status=false)
      tool_id = Nutrient.get_tool_id('Mental maths') rescue nil
      options = {:show_feature_type=>"box"}
      tool = Nutrient.critriea_for_valid_to_launch(current_user,api_version,options).where(id:tool_id).last
      # if tool.blank?
        # show_box_status = false
      # else
        show_box_status = !subscription_status
      # end
          
      {:tool_id=>tool_id,:feature_type=>"box",:show_box_status=>show_box_status}
    end

    def badges_level(data)
      ['Coming Soon', '#9B9B9B']
      # total_questions = data.count
      # if total_questions >= 100
      #   ['Explorer', 'Blue']
      # elsif total_questions > 100 && total_questions <= 200
      #   ['Rising Star', 'Blue']
      # elsif total_questions > 200 && total_questions <= 300
      #   ['Star', 'Blue']
      # elsif total_questions < 100
      #   ['No Badge', 'Blue']
      # end
    end

    def practice_status(data, user_deviced_linked, subscribed)
      all_users = {}
      questions_practiced = data.count
      Child::AlexaQuiz::QuizDetail.all.group_by(&:member_id)
                                .map{|i, x| all_users[i] = x.count } 

      more_ques_attempted = 0
      all_users.values.each do |val|
        more_ques_attempted += 1  if data.count < val
      end

      more_percentage = percent_of(more_ques_attempted, all_users.count) rescue 0

      if !user_deviced_linked
        ["#{more_percentage.to_i}% of the kids are practising more questions than #{@child_data.name}", "#9B9B9B"]
      elsif user_deviced_linked && !subscribed && questions_practiced < QUESTIONS_LIMIT
        ["#{@child_data.name} can practise #{QUESTIONS_LIMIT - questions_practiced} more questions in the Basic subscription", "#9B9B9B"]
      elsif user_deviced_linked && !subscribed && questions_practiced >= QUESTIONS_LIMIT
        ["Subscription is required to practise more questions. Please download the app to enable it.", "#DF5F48"]
      else
        ["#{more_percentage.to_i}% of the kids are practising more questions than #{@child_data.name}", "#9B9B9B"]
      end
    end

    def total_practiced_days(data)
      data.group_by{|x| x.created_at.end_of_day}.count rescue 0
    end

    def last_session_held(data)
      last_session_date = data.sort("created_at desc").first.created_at.to_date.to_date4.to_s rescue ''

      "Last session held on #{last_session_date}"
    end

    def quiz_frequency(data)
      ['frequently', "#{@child_data.name} has been stimulating his brain frequently", "green"]
      [nil, nil, nil]
    end

    def is_user_deviced_linked(current_user)
       Devices::HomeDevice.where(:member_id=>current_user.id.to_s).present?
    end

    def is_enough_subscription(data, subscribed)
      subscribed
    end

    def practiced_topics(data)
      Child::AlexaQuiz::QuizDetail.where(member_id: @child_data.id).group_by(&:question_type).keys.uniq rescue []
    end

    def linked_devices(child,current_user)
      member_email_ids = {}
      linked_devices_list = []
      family_ids = child.user_family_ids
      member_ids = FamilyMember.where(:family_id.in=>family_ids).pluck(:member_id)
      members = Member.where(:id.in=>member_ids)
      members.each { |member|  member_email_ids[member.id.to_s] = member.email}
      member_linked_devices = Devices::HomeDevice.where(:member_id.in=>member_ids)
      member_linked_devices.each do |device|
        linked_devices_list << { :platform=> device.platform, :email=> member_email_ids[device.member_id.to_s] } 
      end
      linked_devices_list
    end

    def answered_days_ago_stats(daysago, data)
      answered_stats = {}
      days_ago_data = []
      data.map {|da| days_ago_data << da if (da[:created_at].to_date > daysago.days.ago.to_date) }
      answered_stats['title'] = "Last #{daysago} #{'day'.pluralize(daysago)}"
      answered_stats['avg_question_per_day'] = avg_question_per_day(days_ago_data, daysago)
      answered_stats['practiced_days'] = practiced_days(days_ago_data).to_i rescue 0
      answered_stats['practiced_percentage_change'] =
                                        practiced_percentage_change(daysago, data).to_i
      answered_stats
    end

    def practiced_percentage_change(daysago, data)
      days_ago_data, prev_days_ago_data = [], []
      data.map {|da| days_ago_data << da if (da[:created_at].to_date > daysago.days.ago.to_date) }

      data.map {|da|
        if (da[:created_at].to_date < daysago.days.ago.to_date) && 
            (da[:created_at].to_date > (daysago*2).days.ago.to_date)
          prev_days_ago_data << da
        end
      }
      
      diff_percentage = 0
      if (prev_days_ago_data.count > days_ago_data.count)
        diff_percentage = -(percent_of(days_ago_data.count, prev_days_ago_data.count))
      elsif (prev_days_ago_data.count > days_ago_data.count)
        diff_percentage = percent_of(prev_days_ago_data.count, days_ago_data.count)
      end
      
      diff_percentage
    end

    def avg_question_per_day(data, dayago)
      ((data.count.to_f/dayago.to_f)).round(1) rescue 0
    end

    def practiced_days(data)
      total_practiced_days(data)
    end

    def subject_summary_json(key, data_by_subject, org_data)
      json =  {}
      # json['subject_name'] = key
      json['total_correct_answer_percentage'] = 
                                    correct_answers_percentage(data_by_subject)
      json['total_questions'] = data_by_subject.count
      json['total_skipped_question_percentage'] = 
                                    total_skipped_question_percentage(data_by_subject)
      json['total_incorrect_answer_percentage'] = 
                                    total_incorrect_answer_percentage(data_by_subject)
      json["total_correct_answer"] = correct_answers(data_by_subject)
      json["total_skipped_question"] = skipped_questions(data_by_subject)
      json["total_incorrect_answer"] = incorrect_answers(data_by_subject)
      json['practiced_date'] =                   
                            subject_practiced_date(data_by_subject)
      json['practiced_date_formatted'] =
                          subject_practiced_date_formatted(json['practiced_date'])
      json['most_practiced_topic'] = 
                                    most_practiced_topic(data_by_subject)
      json['best_performed_topic'] = 
                                    best_performed_topic(data_by_subject)
      json['topics_summary'] =
                                topics_summary(data_by_subject)

      json
    end
    
    def subject_practiced_date(data)
      data.sort_by(&:created_at).last.created_at.to_date.to_date4.to_s
    end
    
    def subject_practiced_date_formatted(data_date)
      data_date.to_time.convert_to_ago2.humanize
    end

    def topics_summary(data)
      topic_summary = []
      data = data.group_by(&:question_type)
      data.each do |d|
        json = {}
        json['topic_name'] = d[0]
        json['total_question'] = d[1].count
        json['total_answered'] = ques_total_answered(d[1])
        json['total_correct_answer'] = correct_answers(d[1])
        json['score_percentage'] = correct_answers_percentage(d[1])
        topic_summary << json
      end
      topic_summary
    end

    def answered_stats(data)
      answered_arr = []
      [7, 28].each do |time|
        answered_arr << answered_days_ago_stats(time, data)
      end
      answered_arr
    end

    def dummy_answered_stats
      [{
        'title': 'Last 7 days',
        'avg_question_practiced': 12,
        'practiced_days': 4,
        'practiced_percentage_change': +3
      },
      {
        'title': 'Last 28 days',
        'avg_question_practiced': 8,
        'practiced_days': 20,
        'practiced_percentage_change': -1
      }]
    end

  end
end
