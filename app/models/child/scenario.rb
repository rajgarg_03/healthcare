class Child::Scenario
  include Mongoid::Document
  include Mongoid::Timestamps
  WillPaginate.per_page = 5
  
  field :scenario_title,                :type => String # scenario name/title
  field :topic     ,                    :type => String # topic/description/title ..etc for tool 
  field :start_date,                    :type => Time
  field :peak_relevence_date,           :type => Time
  field :expire_date,                   :type => Time
  field :effective_score,               :type => Float
  field :reminder_factor,               :type => String
  field :N_factor,                      :type => String
  field :score ,                        :type => String
  field :category,                      :type => String
  field :record_id,                     :type => String #to debug, record for measurement/milestone based on scenario category
  field :scenario_custom_id,            :type => Integer
  field :s_id,                          :type => String # special id. combination of System::Scenario::ScenarioLinkedToTool id and member id
  field :scenario_linked_to_tool_id,    :type => String # System::Scenario::ScenarioLinkedToTool ID
  field :linked_tool_class,             :type => String
  field :linked_tool_id,                :type => String  # linked tool id with linked tool class
  field :shortlisted,                   :type => Boolean, :default => false 
  field :notification_expiry_date,      :type => Time#,  :default => DateTime.now
  field :checked,                       :type => Boolean, :default => false
  field :remind_me_on,                  :type => Date 
  field :reminder_count,                :type => Integer, :default => 0
  field :created_at,                    :type => Time,    :default => DateTime.now
  field :updated_at,                    :type => Time,    :default => DateTime.now

  belongs_to :member

  index ({scenario_custom_id:1})
  index ({member_id:1})
  index ({category:1})
  index ({s_id:1})
  index ({effective_score:1})
  index({linked_tool_id:1})
  index({notification_expiry_date:1})
  validates :member_id, uniqueness: {scope: [ :scenario_linked_to_tool_id]}

    def calculate_effective_score(auto_save=true)
    begin
      dp = self.peak_relevence_date.to_date || (Date.today + 1.days)
      dc = Date.today
      ds = self.start_date.to_date || Date.today
      d_exp = (self.expire_date || Date.today).to_date  # if no expiry date set today date
      reminder_value = 1 + (self.reminder_factor.to_i * 0.5)
      relavence_factor = ( ((dp-dc).to_i.abs)/((dp-ds).to_i.abs) ).to_f  *  self.N_factor.to_f
      time_adjustment_factor = (dc < d_exp) ?  1/(1 + relavence_factor) : 0
      calculated_effective_score = self.score.to_f *  time_adjustment_factor * reminder_value.to_f
     rescue Exception=>e
       calculated_effective_score = 0.001
    end
    self.effective_score  = calculated_effective_score
    save if auto_save
    self
  end
 
  # Child::Scenario.assign_scenario("5baf2c1bf9a2f3b9b6000718")
  def self.assign_scenario(child_member_id=nil,options={:system_scenario_list=>nil,:member_obj=>nil,:category=>"all"})
     begin
      # Fetch all active system scenario list 
      system_scenario_detail_list = options[:system_scenario_list] || ::System::Scenario::ScenarioDetails.where(:status=>"Active")
      member = options[:member_obj] || Member.find(child_member_id.to_s)
      pregnancy_record = member.is_expected? ? Pregnancy.where(expected_member_id:member.id).last : nil
      # Fetch all linked tool scenario
      system_scenario_group_by_custom_id = options[:system_scenario_group_by_custom_id] || System::Scenario::ScenarioLinkedToTool.group_by_system_scenario_detail_custom_id
      # Get child country code to assign link as per country 
      country_code = member.get_country_code rescue nil
      country_code = (country_code || "other").downcase
      faq_notification_enabled = System::Settings.faqNotificationEnabled?(nil,country_code,options)
      
      if !faq_notification_enabled# || Member.sanitize_country_code(country_code).include?("uk")
        # system_scenario_group_by_custom_id =  system_scenario_group_by_custom_id.reject{|k,v| v.first["tool_type"] == "faq"}
        # Remove faq scenarios if not enabled for member
        system_scenario_group_by_custom_id.each{|k,v|  v.delete_if {|a| a["tool_type"] == "faq" rescue false} }
      end
        
      # Save Scenario which are eligible for the member
      system_scenario_detail_list.each do |system_scenario_detail|
        begin
          scenario_detail = system_scenario_detail
          # get all linked tool scenario
          system_scenarios = system_scenario_group_by_custom_id[system_scenario_detail.custome_id]
          # If tool linked with scenario check for assignment to member
          if system_scenarios.present?
            data = ::Child::Scenario.save_for_member(system_scenarios,member,country_code,pregnancy_record,scenario_detail,options)
          else
            # puts ".....No scenario linked with #{scenario_detail.custome_id}"
          end
        rescue Exception=>e
        end
      end
    rescue Exception=>e
      Rails.logger.info "Error--member_id=>#{member.id}-- ---error: #{e.message}"
    end
  end
  
  # Get the jab having highest score for Scenario
  def self.jab_with_high_score(jabs,system_scenario_detail,options={})
    all_scenario_for_jab = []
    # scenario_detail = system_scenario
    jabs.each do |jab|
      begin
        jab_date = (jab.due_on || jab.est_due_time)  
        administered_date = jab_date 
        if eval(system_scenario_detail.expire_date).to_date > Date.today && eval(system_scenario_detail.start_date).to_date <= Date.today 
          data_for_scenario = 
            { 
              :start_date=>  eval(system_scenario_detail.start_date)  ,
              :peak_relevence_date => eval(system_scenario_detail.peak_rel_date) ,
              :expire_date => eval(system_scenario_detail.expire_date) ,
              :scenario_custom_id=> system_scenario_detail.custome_id,
              :N_factor => (system_scenario_detail.N_factor rescue 1),
              :score=> (system_scenario_detail.score rescue 1),
              :record_id => jab.id
            }
          scenario_object = ::Child::Scenario.new(data_for_scenario)
          all_scenario_for_jab << scenario_object.calculate_effective_score(false)
        end
      rescue 
      end
    end
    jab_scenario_with_high_score = (all_scenario_for_jab.sort_by {|c| c[:effective_score]}).last
    
    jab = jab_scenario_with_high_score.present? ? Jab.where(id:jab_scenario_with_high_score[:record_id]).last : nil
  end
 

 def self.get_system_category(system_scenarios,scenario_detail)
  scenario_category = System::Scenario::ScenarioDetails.get_scenario_category(scenario_detail)
  scenario_category 
 end 


 # find record to check scenario validity
 # check start_date and expiry date for record to assign Scenario
 # check critriea if present in system Scenario and validate that critriea
 # return hash value of scenario data to save in DB
  def self.scenario_data_after_validate_condition(system_scenarios,member,country_code,pregnancy_record,scenario_detail,options={})
    # scenario_detail = scenario_detail || system_scenario.scenario_detail
    scenario_category = ::Child::Scenario.get_system_category(system_scenarios,scenario_detail)
    pregnancy = pregnancy_record
    
    # check scenario has link to display
    tool_links =  []
     
    scenario_data = nil
    scenario_data = System::Scenario::ScenarioDetails.get_data_after_validate(scenario_detail,scenario_category,member,country_code,{:linked_tool=>scenario_detail,:pregnancy=>pregnancy_record,:linked_tools_for_topic=>system_scenarios})
    # Evaluate scenario name and Desciption
    if scenario_data.present?
      system_scenarios_group_by_id = {}
      # Assign evaluated data to linked tool scenarios
      system_scenarios.each do |scenario|
        system_scenarios_group_by_id[scenario["_id"]] = scenario
      end
      data = []

      scenario_data[:topic_with_linked_tool_id].each do |scenario_linked_to_tool_id,topic|
        system_scenario = system_scenarios_group_by_id[scenario_linked_to_tool_id]
        system_scenario_id = system_scenario["_id"]
        tool_active_status = (eval(system_scenario["tool_class"]).find(system_scenario["linked_tool_id"]).status == "Active") rescue false
        if tool_active_status
          notification_expiry_date = system_scenario["notification_expiry"].present? ? (Date.today + eval(system_scenario["notification_expiry"].to_s)  rescue nil) : nil
          data << { 
            :scenario_linked_to_tool_id => scenario_linked_to_tool_id.to_s,
            :linked_tool_id=> system_scenario["linked_tool_id"].to_s,
            :scenario_custom_id => scenario_detail.custome_id,
            :status => scenario_detail.status,
            :member_id => member.id.to_s,
            :new_added=> true,
            :valid_for_role=> (scenario_detail.valid_for_role rescue nil),
            :notification_expiry_date => notification_expiry_date,
            :s_id=> ((system_scenario_id.to_s + "_" + member.id.to_s) rescue nil) ,
            :linked_tool_class=> system_scenario["tool_class"],
            :scenario_title=>scenario_data[:scenario_title], 
            :topic=>scenario_data[:topic_with_linked_tool_id][system_scenario_id], 
            :start_date=>scenario_data[:scenario_start_date], 
            :peak_relevence_date=>scenario_data[:scenario_peak_relevence_date], 
            :expire_date=>scenario_data[:scenario_expire_date], 
            :N_factor=>scenario_data[:N_factor], 
            :category=>scenario_data[:category], 
            :score=>scenario_data[:score],
            :record_id => (scenario_data[:record_id] rescue nil)
          }#.merge(data_for_scenario)
        end
      end
    end
  
    data
  end

  def self.save_for_member(system_scenarios,member,country_code,pregnancy_record,scenario_detail,options={})
    country_code = "india" if country_code == "in"
    scenario_data = ::Child::Scenario.scenario_data_after_validate_condition(system_scenarios,member,country_code,pregnancy_record,scenario_detail,options)
    if scenario_data.present?
      scenario_data.each do |data|
        scenario_obj = ::Child::Scenario.new(data)
        scenario_obj.calculate_effective_score(false)
        existing_scenario = ::Child::Scenario.where(s_id:scenario_obj.s_id).last
        save_scenario_status = true
        # Save scanrio data into Pointer Table to keep api working for now
        # TODO: don't duplicate data into Pointer table
        if scenario_obj[:linked_tool_class] == "SysArticleRef"
          pointer_data = ArticleRef.save_pointer_data_after_scenarion_assign(scenario_obj,member,scenario_detail,options)
          save_scenario_status = pointer_data.present?
        end
        next unless save_scenario_status
        if existing_scenario.blank?
          scenario_obj.upsert
        else
          saved_scenario = existing_scenario #::Child::Scenario.where(scenario_linked_to_tool_id:scenario_obj.scenario_linked_to_tool_id,member_id:member.id.to_s).last
          saved_scenario.notification_expiry_date = nil if scenario_obj.notification_expiry_date.blank?
          data.merge!({topic:scenario_obj.topic, scenario_title:scenario_obj.scenario_title, :notification_expiry_date=>saved_scenario.notification_expiry_date ,:updated_at=>Time.now,:new_added=>false, :effective_score=>scenario_obj.effective_score})
          saved_scenario.update_attributes(data)
        end
       
      end
    end
  end
  



end