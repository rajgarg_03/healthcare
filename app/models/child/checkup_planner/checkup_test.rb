class Child::CheckupPlanner::CheckupTest
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :system_checkup_planner_test_id, type: String
  field :checkup_id,type:String
  belongs_to :system_checkup_planner_test, class_name: 'System::Child::CheckupPlanner::Test', foreign_key: :system_checkup_planner_test_id

  index({checkup_id:1})

  belongs_to :checkup, class_name: 'Child::CheckupPlanner::Checkup', foreign_key: :checkup_id
  
end