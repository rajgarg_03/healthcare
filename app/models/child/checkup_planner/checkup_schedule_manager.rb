# class Child::CheckupPlanner::CheckupManager
#   include Mongoid::Document  
#   include Mongoid::Timestamps
#   field :title, type: String
#   field :description, type:String 
#   # field :recommended, type:String
#   field :checkup_date, type:String
#   field :country, type:String
#   field :member_id, type:String
  
#   index({member_id:1})
#   index({checkup_date:1})
  
#   def member
#     Member.where(id:self.member_id).last
#   end
  
#   def self.get_schedule_list(current_user,member_id=nil)
#     added_checkup_schedule = Child::CheckupPlanner::Checkup.where(member_id:member_id).order_by("created_at desc")
#     added_checkup_schedule_manager_ids = added_checkup_schedule.pluck(:checkup_schedule_manager_id).map(&:to_s)
#     checkup_scheduler_manager_list = Child::CheckupPlanner::CheckupManager.in(member_id:[nil,current_user.id.to_s]).order_by("created_at desc").entries
#     data = {}
#     member_birth_date = member_id.present? ? Member.find(member_id).birth_date : Date.today
#     added_checkup_schedule.each do |checkup_schedule|
#       custom_status_val = checkup_schedule.status + " on " + (checkup_schedule.checkup_date || Date.today).to_date3
#       data[checkup_schedule.checkup_schedule_manager_id.to_s] = custom_status_val
#     end
#     if checkup_scheduler_manager_list.present?
#       checkup_scheduler_manager_list.map do |checkup_scheduler_manager|
#         checkup_manager_id = checkup_scheduler_manager.id.to_s
#         checkup_scheduler_manager["added"] = added_checkup_schedule_manager_ids.include?(checkup_manager_id)
#         estimated_date = checkup_scheduler_manager.member_id.present? ? Date.today.to_date3 : (member_birth_date + (eval(checkup_scheduler_manager.checkup_date) rescue 0.days) ).to_date.to_date3
#         checkup_scheduler_manager["custom_status"] = data[checkup_manager_id] || "Estimated on #{estimated_date}"
#       end
#     end
#     checkup_scheduler_manager_list
#   end

#   def self.add_schedule(current_user,params)
#     params[:checkup_schedule_manager][:member_id] = current_user.id.to_s
#     record = Child::CheckupPlanner::CheckupManager.create(params[:checkup_schedule_manager])
#     record
#   end
# end