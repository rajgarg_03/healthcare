class Child::CheckupPlanner::Checkup
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :title, type: String
  field :description, type:String 
  field :status, type: String,:default=>"Estimated" # Estimated/Planned/Completed
  field :checkup_date, type:Date
  field :time_frame, type:String  # time frame text 
  field :checkup_start_time, type:String
  field :checkup_end_time, type:String
  field :checkup_datetime, type:String
  field :doctor_name, type:String
  field :addded_by, type:String #user/system
  field :facility_name, type:String
  field :notes, type:String
  field :data_source, type:String, :default=>"nurturey"
  
  index({member_id:1})
  index({checkup_date:1})
  has_many :checkup_tests, class_name: "Child::CheckupPlanner::CheckupTest"
  
  belongs_to :member
  StatusColorCode = {"Estimated"=>"#e9401c","Planned"=>"#d9401c","Completed"=>"#c9401c"}

  def self.checkup_summary(current_user,member_id,api_version=1)
    child = ChildMember.find(member_id)
    measurement = Health.get_latest_height_weight(current_user,child).get_unit_converted_record(current_user) rescue nil
    
    measurement_updated_date = Health.where(member_id:member_id).order_by("updated_at desc").first.updated_at.convert_to_ago2 rescue nil
    measurement_updated_date_text = measurement_updated_date.present? ?  "Updated: #{measurement_updated_date}": "Not updated yet"
      
    next_checkup_on = Child::CheckupPlanner::Checkup.next_checkup_on(current_user,member_id)
    last_checkup_on = Child::CheckupPlanner::Checkup.last_checkup_on(current_user,member_id)

    last_checkup_on = last_checkup_on ? ("Last on " + last_checkup_on) : nil
    
    upcoming_vaccination = Vaccination.next_jab_date(member_id)

    last_vaccination = Vaccination.last_jab_date(member_id, 'to_date3')
    last_vaccination = last_vaccination ? ("Last on " + last_vaccination) : nil
    
    summary =  {:last_vaccination=> last_vaccination, :last_checkup_on=> last_checkup_on,:measurement_updated_date=>measurement_updated_date_text, :height=>measurement["height"] ,:weight=> measurement["weight"],:bmi=> measurement["bmi"],:head_circum=> measurement["head_circum"],:next_checkup_on=>next_checkup_on, :upcoming_vaccination=>upcoming_vaccination }
  end
  
  # type = upcoming/past/all
  def self.get_list_with_test(current_user,member_id,type,api_version)
    member  = Member.find(member_id)
    data = []
    if type == "past" || type == "upcoming"
      data = Child::CheckupPlanner::Checkup.data_group_by_date(current_user,member,api_version,type)
    else
      checkups = Child::CheckupPlanner::Checkup.where(:member_id=>member_id.to_s).sort_by! {|s| s.title[/\d+/].to_i}
      data = checkups.map{|obj| obj.in_json(current_user,member,api_version,true)}
    end  
    data
  end

  def in_json(current_user,member,api_version=1,include_test=false)
    checkup = self 
    text,color = checkup.status_text(current_user,member,api_version)
    checkup["status_color_code"] = color
    checkup["status_text"] = text
    if include_test == true
      test_ids = Child::CheckupPlanner::CheckupTest.where(checkup_id:checkup.id).pluck(:system_checkup_planner_test_id)
      checkup["tests"] = System::Child::CheckupPlanner::Test.where(:id.in=>test_ids).entries
    end
    checkup
  end
  
  def self.data_group_by_date(current_user,member,api_version,type)
    hash_data  = {"Overdue"=>[],'This Month'=>[],'Next Month'=>[]}
    member_id = member.id.to_s
    today_date = DateTime.now.in_time_zone.beginning_of_day.to_date
    month_end_date = DateTime.now.in_time_zone.end_of_month.to_date
    if type == "past"
      data =  Child::CheckupPlanner::Checkup.where(:member_id=>member_id,:checkup_date.lte=>Date.today,status:"Completed").order_by("checkup_date desc")
    elsif type == "upcoming"
      data = Child::CheckupPlanner::Checkup.where(:member_id=>member_id.to_s,:status.ne=>"Completed").order_by("checkup_date asc")
    else
      data = {}
    end
    data = data.group_by  {|a| a.checkup_date.strftime("%Y-%m-%d")}
     
    data.each do |checkup_date,value|
      checkup_date = checkup_date.to_date
      value = value.map{|a| a.in_json(current_user,member,api_version)}
      if (checkup_date < today_date) && type == "upcoming"
        hash_data["Overdue"] << value
        hash_data["Overdue"].flatten!
      elsif (checkup_date >= today_date) && checkup_date <= month_end_date
        hash_data['This Month'] << value
        hash_data['This Month'].flatten!
      elsif (checkup_date >= (today_date + 1.month).at_beginning_of_month)  && (checkup_date <= (today_date + 1.month).at_end_of_month)
        hash_data['Next Month'] << value 
        hash_data['Next Month'].flatten!
      elsif (checkup_date > (today_date + 1.month))
        hash_data = month_wise_hash_data(hash_data, checkup_date, value)
        hash_data[checkup_date.strftime('%B %Y')].flatten!
      elsif (checkup_date <= today_date) && type == "past"
        hash_data = month_wise_hash_data(hash_data, checkup_date, value)
        hash_data[checkup_date.strftime('%B %Y')].flatten!
      end
    end
    hash_data.delete_if { |k, v| v.blank? }
    hash_data
  end

  def self.month_wise_hash_data(hash_data, checkup_date, value)
    if hash_data[checkup_date.strftime('%B %Y')].nil?
      hash_data[checkup_date.strftime('%B %Y')] = []
      hash_data[checkup_date.strftime('%B %Y')] << value
    else
      hash_data[checkup_date.strftime('%B %Y')] << value
    end
    hash_data
  end

  # def status_text(current_user,member,api_version)
  #   checkup = self
  #   if checkup.checkup_date < Date.today
  #     "Late by #{checkup.checkup_date.to_time.convert_to_ago2}".gsub(" ago","")
  #   elsif checkup.checkup_date == Date.today
  #     "Due on today"
  #   else
  #     "Due on after #{checkup.checkup_date.to_time.convert_to_ago2}".gsub(" ago","")
  #   end
  # end



  def status_text(current_user,member,api_version)
    checkup = self
    status = checkup.status 
    due_on = checkup.checkup_date

    est_days = DateTime.now + 10.days
    est_late_days = DateTime.now - 6.days
    due_on = due_on.to_date rescue nil
    yellow_color, red_color, grey_color = '#f5c423', '#d9401c', '#9b9b9b'
    return ['Due on ', grey_color] if due_on.blank?
    
    if status == 'Completed'
      # ['Completed on ' + due_on.strftime("%d %b %Y"), grey_color]
      ['', '']
    elsif due_on < Date.today #est_late_days
      timedifference = TimeDifference.between(due_on.to_date, DateTime.now.to_date).humanize
      text = 'Late by ' + timedifference.to_s
      [text, red_color]
    elsif due_on >= Date.today
      if due_on <= est_days
         day_count = (due_on.to_date - Time.now.to_date).to_i
         text = day_count == 0 ? "Today" : "Upcoming in #{day_count} #{'day'.pluralize(day_count)}"
        color = yellow_color
        [text,color]
      else
        # text = (status == "Estimated") ? "Estimated due date on" : "Planned On"
        # text = text + " " + due_on.strftime("%d %b %Y")
        # color = grey_color
        ['', '']
      end
 
    else
      # ["#{status} on " + due_on.strftime("%d %b %Y"), grey_color]
      ['', '']
    end
  end



  def add_test_to_checkup(test_ids)
    checkup = self
    test_ids = [test_ids].flatten
    System::Child::CheckupPlanner::Test.where(:id.in=>test_ids).each do |checkup_test|
      Child::CheckupPlanner::CheckupTest.new(checkup_id:checkup.id,system_checkup_planner_test_id:checkup_test.id).save
    end
  end

  def self.data_for_google_calender(checkup_schedule,action,option)
    custom_status_val = checkup_schedule.status + " on " + (checkup_schedule.checkup_date || Date.today).to_date3
    schedule_date = checkup_schedule.checkup_date.to_time.in_time_zone(option["time_zone"]).to_date
    start_time  = schedule_date.to_time1(checkup_schedule.checkup_start_time)
    end_time  =   schedule_date.to_time1(checkup_schedule.checkup_end_time)
    if start_time.blank?
    data = {
        'summary'  =>   custom_status_val,
        'id' => "Checkup" + checkup_schedule.id.to_s,  
        'start' => {
          'date' => schedule_date.strftime("%Y-%m-%d"),
          'timeZone' => ActiveSupport::TimeZone.find_tzinfo(option["time_zone"])
          },  
        'end' => {         
          'date' => schedule_date.strftime("%Y-%m-%d"),
          'timeZone' => ActiveSupport::TimeZone.find_tzinfo(option["time_zone"])
          },
      }
    else
      data = {
        'summary'  =>   custom_status_val,
        'id' => "Checkup" + checkup_schedule.id.to_s,  
        'start' => {
          'dateTime' =>  start_time.to_datetime.rfc3339},  
        'end' => {         
          'dateTime' => end_time.to_datetime.rfc3339},
      }
    end
  end


  def self.sync_with_google_data(member_id)
    if member_id.present?
    else
    end
  end

  def self.add_checkup_schedule(current_user,params)
  	member = ChildMember.find(params[:checkup_plan][:member_id])
    param_data = params[:checkup_plan]
    param_data[:checkup_date] = param_data["checkup_date"] || param_data["date"] 
    data = validate_schedule_time(params)
    raise "invalid time" if data.blank?
    title = param_data["title"]
    description = param_data["description"]
    checkup_date = param_data["checkup_date"].to_date

    checkup_start_time = (checkup_date|| Date.today).to_time1(param_data["start_time"])
    checkup_end_time = checkup_start_time + 1.hours
    schedule_title = params[:title] || title
    schedule_description = param_data[:description] || description
    schedule_checkup_date = param_data[:checkup_date].present? ? checkup_start_time.in_time_zone.to_date : checkup_date
    schedule_checkup_start_time = param_data[:checkup_start_time] || checkup_start_time.to_time.to_formatted_s(:time) rescue nil
    schedule_checkup_end_time = param_data[:checkup_end_time] || checkup_end_time.to_time.to_formatted_s(:time)
    record = Child::CheckupPlanner::Checkup.create(
      notes:params[:checkup_plan][:notes], 
      added_by:"user", 
      title:schedule_title,
      status: (param_data[:status] || "Estimated"), 
      description:schedule_description,
      checkup_date:schedule_checkup_date, 
      member_id:params[:checkup_plan][:member_id],
      checkup_start_time:schedule_checkup_start_time,
      checkup_end_time:schedule_checkup_end_time,
      facility_name: params[:checkup_plan][:facility_name],
      doctor_name: params[:checkup_plan][:doctor_name]
      )
    # record = Child::CheckupPlanner::Checkup.create(facility_name: params[:checkup_schedule][:facility_name], doctor_name:params[:checkup_schedule][:doctor_name], checkup_schedule_manager_id: params[:checkup_schedule][:checkup_schedule_manager_id], title:schedule_title, description:schedule_description,checkup_date:schedule_checkup_date, member_id:params[:checkup_schedule][:member_id],checkup_start_time:schedule_checkup_start_time,checkup_end_time:schedule_checkup_end_time)
      
  end
  
  def update_checkup_schedule(current_user,params)
    data = Child::CheckupPlanner::Checkup.validate_schedule_time(params)
    if data.present?
      checkup = self
      param_data = params[:checkup_plan]
      param_data[:checkup_date] = param_data.delete(:date)
      param_data[:checkup_start_time] = param_data.delete(:start_time)
      param_data[:checkup_end_time] = param_data.delete(:end_time)
      raise "Error" if !checkup.update_attributes(param_data)
    else
      raise "Start time is greater than End time"
    end
  end

  def self.validate_schedule_time(params)
    if params[:checkup_plan][:checkup_date].present? && params[:checkup_plan][:checkup_start_time].present?
      schedule_date = (params[:checkup_plan][:checkup_date].to_date)
      start_time  = schedule_date.to_time1(params[:checkup_plan][:checkup_start_time])
      end_time  = schedule_date.to_time1(params[:checkup_plan][:checkup_end_time])
      if start_time < end_time
        params
      else
        nil
      end  
    else
      params
    end
  end

  # def self.get_schedules(current_user,member_id)
  #   schedules = where(member_id:member_id).order_by("checkup_date desc").entries
  #   schedules.map do |schedule|
  #     custom_status_val =  (schedule.status + " on " + (schedule.checkup_date|| Date.today).to_date3)
  #     schedule["custom_status"] = custom_status_val
  #   end
  #   schedules
  # end

  def self.next_checkup_on(current_user,member_id)
    next_checkup_date = Child::CheckupPlanner::Checkup.where(:status.ne=>"Completed", :checkup_date.gte=>Date.today,member_id:member_id).order_by("checkup_date asc").first.checkup_date rescue nil
    next_checkup_date.to_time.strftime("%d %b %y") rescue nil
  end

  def self.last_checkup_on(current_user,member_id)
    last_checkup_date = Child::CheckupPlanner::Checkup.where(:status=>"Completed", :checkup_date.lte=>Date.today,member_id:member_id).order_by("checkup_date asc").first.checkup_date rescue nil
    last_checkup_date.to_date.to_date3 rescue nil
  end

  def delete_schedule(current_user)
    self.delete
  end
  
end