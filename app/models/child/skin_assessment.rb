class Child::SkinAssessment
include Mongoid::Document
  include Mongoid::Timestamps  
  
  field :skin_health, type:Float # %skin health value from face++
  field :acne, type:Float        # % acne  health value from face++
  field :dark_circle, type:Float #%  dark circle value from face++
  field :stain, type:Float       #% stain value from face++
  field :assessment_date, type:Date

  has_many :pictures, :as => :imageable, :dependent => :destroy
  belongs_to :member  
  SkinAttributes = ["skin_health", "acne", "dark_circle", "stain"]
  def self.get_tool_id
    begin
      tool_name = "Skin assessment"
      tool_id = Nutrient.where(:identifier=>/#{tool_name}$/i).last.id
    rescue Exception=>e 
      nil
    end
  end
  
  def self.latest_assessment(member_id)
    where(member_id:member_id).order_by("created_at desc").limit(1).first
  end
  
  def data_for_graph
    skin_assessment = self
    {:skin_health=>skin_assessment[:skin_health], :stain=>skin_assessment[:stain], :dark_circle=>skin_assessment[:dark_circle] , :acne=>skin_assessment[:acne]}
  end

  def self.get_list(member_id,current_user,round_off_val=2)
    data = Child::SkinAssessment.where(member_id:member_id).order_by("created_at desc")
    member = Member.find(member_id)
    data = data.map {|assessment| assessment.in_json(member,round_off_val) rescue nil}
    data.compact!
    data
  end

  def in_json(member,round_off_val=2)
   skin_assessment = self
   skin_assessment_attributes = skin_assessment.attributes
   skin_assessment_picture = skin_assessment.pictures.last
   skin_assessment_attributes["photo_aws_url"] = skin_assessment_picture.aws_url(:small)
   skin_assessment_attributes["photo_main_url"] = skin_assessment_picture.photo_url_str
   child_age = ApplicationController.helpers.calculate_age2(member.birth_date.to_date,skin_assessment.assessment_date ).gsub("and", "").gsub("  ", " ")
   age = ApplicationController.helpers.calculate_age3(member.birth_date.to_date,skin_assessment.assessment_date ).gsub("and", "").gsub("  ", " ")
   skin_assessment_attributes["child_age"] = child_age
   skin_assessment_attributes["assessment_date"] = skin_assessment["assessment_date"].to_date.to_date4
   skin_assessment_attributes["age"] = age
   Child::SkinAssessment::SkinAttributes.each do |i|
    skin_assessment_attributes[i] = skin_assessment[i].round_to(round_off_val)
   end
   skin_assessment_attributes
  end
  
  def self.create_record(child_member, current_user,params,api_version=1)
    begin
      skin_assessment_record = ::Child::SkinAssessment.new(member_id:child_member.id, assessment_date:Date.today)
      picture =  Picture.create(:imageable_type=>"Child::SkinAssessment",:imageable_id=>skin_assessment_record.id,local_image: params[:media_files],upload_class: "child_skin_assessment")
      # picture.save!
      skin_assessment_record.pictures << picture
      # picture =  skin_assessment_record.pictures.last
      picture.upload_to_s3
      picture.local_image.destroy
      picture.reload
      face_plus_error = nil
      begin
        skin_data = FacePlus.get_skin_detail(picture)
      rescue Exception=> e 
        face_plus_error = e.message
        raise e.message
      end
      skin_assessment_record.update_attributes(skin_data)
    rescue Exception=> e 
      skin_assessment_record.delete_record
      skin_assessment_record = nil
    end

    [skin_assessment_record,face_plus_error]
  end

  def delete_record
    skin_assessment = self
    skin_assessment.pictures.delete_all
    skin_assessment.destroy
  end

  def self.get_skin_assessment_score(member_id,skin_data_for_sample=false,options={})
    return skin_assessment = {"skin_health"=>0,"acne"=>0,"dark_circle"=>0,"stain"=>0} if skin_data_for_sample
    begin
      assessment = {}
      member_id = member_id.to_s
      member = Member.find(member_id)
      last_skin_assessment = Child::SkinAssessment.where(member_id:member_id).order_by("created_at desc").first
      ["skin_health","acne","dark_circle","stain"].each do |i|
        assessment[i] = last_skin_assessment[i]
      end
    rescue Exception=> e 
      Rails.logger.info e.message
      assessment = {}
    end
    assessment 
  end

  def self.skin_assessment_score_to_text(skin_assessment,round_off_val=2)
    data = {}
    skin_assessment.each do |type,score| 
      data[type] = score.present? ? score.to_f.round_to(round_off_val).to_s : nil
    end
    data
  end
  
  def self.skin_assessment_analysis_for_skin_health(member,skin_assessment_for_skin_health)
    score = skin_assessment_for_skin_health
    if score > 3
      {:current_status =>{:status=>"Green", :message=>"#{member.first_name} is very tall"}, :special_msg_for_box=> "Tallness is rarely a problem, especially if parents are also tall." }
    elsif score <= 3 && score >2
      {:current_status =>{:status=>"Green", :message=>"#{member.first_name} seems to be quite tall among the peer group."}, :special_msg_for_box=>nil }
    else
      {:current_status =>{:status=>"Green", :message=>"#{member.first_name} seems to be quite tall among the peer group."}, :special_msg_for_box=>nil }
    end
  end

  def self.skin_assessment_analysis_for_acne(member,skin_assessment_for_acne)
    score = skin_assessment_for_acne
    if score > 3
      {:current_status =>{:status=>"Green", :message=>"#{member.first_name} is very tall"}, :special_msg_for_box=> "Tallness is rarely a problem, especially if parents are also tall." }
    elsif score <= 3 && score >2
      {:current_status =>{:status=>"Green", :message=>"#{member.first_name} seems to be quite tall among the peer group."}, :special_msg_for_box=>nil }
    else
      {:current_status =>{:status=>"Green", :message=>"#{member.first_name} seems to be quite tall among the peer group."}, :special_msg_for_box=>nil }

    end
  end

  def self.skin_assessment_analysis_for_dark_circle(member,skin_assessment_for_dark_circle)
    score = skin_assessment_for_dark_circle
    if score > 3
      {:current_status =>{:status=>"Green", :message=>"#{member.first_name} is very tall"}, :special_msg_for_box=> "Tallness is rarely a problem, especially if parents are also tall." }
    elsif score <= 3 && score >2
      {:current_status =>{:status=>"Green", :message=>"#{member.first_name} seems to be quite tall among the peer group."}, :special_msg_for_box=>nil }
    
    else  
      {:current_status =>{:status=>"Green", :message=>"#{member.first_name} seems to be quite tall among the peer group."}, :special_msg_for_box=>nil }
    
    end
  end

  def self.skin_assessment_analysis_for_stain(member,skin_assessment_for_stain)
    score = skin_assessment_for_stain
    if score > 3
      {:current_status =>{:status=>"Green", :message=>"#{member.first_name} is very tall"}, :special_msg_for_box=> "Tallness is rarely a problem, especially if parents are also tall." }
    elsif score <= 3 && score >2
      {:current_status =>{:status=>"Green", :message=>"#{member.first_name} seems to be quite tall among the peer group."}, :special_msg_for_box=>nil }
    else
      {:current_status =>{:status=>"Green", :message=>"#{member.first_name} seems to be quite tall among the peer group."}, :special_msg_for_box=>nil }

    end
  end


  def self.skin_assessment_score_analysis(skin_assessment,member)
    skin_assessment_analysis = {}
    skin_assessment_analysis["skin_health"] = skin_assessment_analysis_for_skin_health(member,skin_assessment["skin_health"]) if skin_assessment["skin_health"].present?
    skin_assessment_analysis["acne"] = skin_assessment_analysis_for_acne(member,skin_assessment["acne"]) if skin_assessment["acne"].present?
    skin_assessment_analysis["dark_circle"] =    skin_assessment_analysis_for_dark_circle(member,skin_assessment["dark_circle"]) if skin_assessment["dark_circle"].present?
    skin_assessment_analysis["stain"] =    skin_assessment_analysis_for_stain(member,skin_assessment["stain"]) if skin_assessment["stain"].present?
    data = []
    skin_assessment_analysis.each do |k,v|
      special_msg_for_box = v.delete(:special_msg_for_box)
      data << skin_assessment_analysis[k].merge(:trend_line=>{:message=>nil,:status=>nil},:type=>k.titlecase,:suggestions=>nil, :suggested_actions=>special_msg_for_box).delete_blank
    end
    data
  end
  
  #who_zscore_data = [{who_height_chart_data=>{"zscore_-3":}..month:},{who_weight_chart_data=>{"zscore_-3":}..month:}]
  #member_data = {:height=>[ [xaxis_data,y_axis_data]] },:weight=>[]}
  def self.chart_min_max_data(who_zscore_data,member_data)
    data = {}
    key_map = {"who_height_chart_data"=>"height", "who_weight_chart_data"=>"weight","who_bmi_chart_data"=>"bmi"}
    ["height","weight","bmi"].each do |a|
       x =  member_data[a].to_h rescue {}
       who_month_data = who_zscore_data[a].delete(:month)
       sorted_y_data = (who_zscore_data[a].values + x.values).flatten.sort
       sorted_x_data = (who_month_data +   x.keys).flatten.sort
       who_zscore_data[a][:month] = who_month_data
       data["#{a}_min_max_axis"] = {:min_x=>sorted_x_data[0],:max_x=>sorted_x_data[-1],:min_y=>sorted_y_data[0],:max_y=>sorted_y_data[-1] }
    end
   data
  end
   

end