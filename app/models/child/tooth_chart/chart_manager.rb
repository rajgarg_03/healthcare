class Child::ToothChart::ChartManager

  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String #Central incisor/Lateral incisor/Canine/First Molar/Second Molar
  field :position, type: String #Upper left/lower left/ Upper right/Lower right
  field :erupt_age_lower, type: Integer #8  (in month)
  field :erupt_age_upper, type: Integer #10 (in month)
  field :shed_age_lower, type: Integer  #12 (in years)
  field :shed_age_upper, type: Integer  #14 (in years)
  # for formula calculation
  field :erupted_min_range_in_day, :type =>Integer #m
  field :erupted_max_range_in_day, :type =>Integer #n
  field :erupted_n2_m2, :type =>Float #C
  field :erupted_n2_2m2, :type =>Float #D
  field :data_source, :type=> String, :default=> "nurturey"

  index({name:1})
  
  has_many :tooth_details, class_name: 'Child::ToothChart::ToothDetails', foreign_key: 'chart_manager_id'
  Description = {"Central incisor"=>{:title=>"Central incisor",:description=>"Cognitive development is child's development in terms of information processing, conceptual resources, perceptual skill, language learning and other aspects of brain development. Its the emergence of the ability to think and understand"}, "Lateral incisor"=>{:title=>"Lateral incisor",:description=>"Language development supports your child's ability to communicate, and express and understand feelings. It also supports thinking and problem-solving, and developing and maintaining relationships"}, "First molar"=>{:title=>"Social & Emotional",:description=>"Social-emotional tions and the ability to establish positive and rewarding relationships with others."}, "Canine"=>{:title=>"Canine",:description=>"Physical Development involves developing control over the body, particularly muscles and physical coordination. It involves learning activities such as grasping, writing, crawling, and walking."}}  
  #::Child::ToothChart::ChartManager.tooth_chart_due_range_for_child(mmeber)
  def self.tooth_chart_due_range_for_child(member,options={})
    shed_range = ::Child::ToothChart::ChartManager.order_by("shed_age_lower asc, shed_age_upper asc").entries.map{|a| (a.shed_age_lower..a.shed_age_upper)}.uniq
    erupt_range = ::Child::ToothChart::ChartManager.order_by("erupt_age_lower asc, erupt_age_upper asc").entries.map{|a| (a.erupt_age_lower..a.erupt_age_upper)}.uniq
    system_shed_range = shed_range
    system_erupt_range = erupt_range
    
    if member.present?
      shed_range_min = shed_range.first.entries.first
      shed_range_max = shed_range.last.entries.last
      erupt_range_min = erupt_range.first.entries.first
      erupt_range_max = erupt_range.last.entries.last
      age_in_month = member.age_in_months
      age_in_year = member.age_in_years
      
      # under age for erupt tooth development
      if age_in_month < erupt_range_min
        erupt_range = [erupt_range.first]
       return {:shed_range=>[],:erupt_range=>erupt_range}
      
      # over age for erupt tooth development
      elsif age_in_month > erupt_range_max
        erupt_range = [erupt_range.last]
      else
        erupt_range = erupt_range.select{|erupt| erupt.include?(age_in_month)}
      end
      # under age for shed tooth development
      if age_in_year < shed_range_min
        shed_range = [shed_range.first]
      # over age for shed tooth development
      elsif age_in_year > shed_range_max
        shed_range = [shed_range.last]
        return {:shed_range=>shed_range,:erupt_range=>[]}
      else
        shed_range = shed_range.select{|shed| shed.include?(age_in_year)}
      end          
      # For upcoming tooth include next age range
      next_shed_range_band =  system_shed_range[system_shed_range.index(shed_range.last).to_i + 1]
      shed_range << next_shed_range_band  if next_shed_range_band.present?
      next_erupt_range_band =  system_erupt_range[system_erupt_range.index(erupt_range.last).to_i + 1]
      shed_range << next_shed_range_band  if next_erupt_range_band.present?
    
    end
    {:shed_range=>shed_range,:erupt_range=>erupt_range}
  end

  # ::Child::ToothChart::ChartManager.due_upcoming_tooth_manager(current_user,member)
  def self.due_upcoming_tooth_manager(current_user,member,options={})
    due_range_values = ::Child::ToothChart::ChartManager.tooth_chart_due_range_for_child(member,options)
    or_condition = []

    shed_age_ranges = due_range_values[:shed_range]
    erupt_age_ranges = due_range_values[:erupt_range]
    # shed in below range
    shed_age_ranges.each do |shed_age|
      or_condition << {:shed_age_lower.gte=>shed_age.first,:shed_age_upper.lte=>shed_age.last}
    end

    # erupt in below range
    erupt_age_ranges.each do |erupt_age|
      or_condition << {:erupt_age_lower.gte=>erupt_age.first,:erupt_age_upper.lte=>erupt_age.last}
    end
    if or_condition.present?
      ::Child::ToothChart::ChartManager.or(or_condition)
    else
      # return nil set record
      ::Child::ToothChart::ChartManager.where(:id=>nil)
    end
  end

  # ::Child::ToothChart::ChartManager.overdue_tooth_manager(current_user,member)
  def self.overdue_tooth_manager(current_user,member,options={})
    system_shed_range = ::Child::ToothChart::ChartManager.order_by("shed_age_lower asc, shed_age_upper asc").entries.map{|a| (a.shed_age_lower..a.shed_age_upper)}.uniq
    system_erupt_range = ::Child::ToothChart::ChartManager.order_by("erupt_age_lower asc, erupt_age_upper asc").entries.map{|a| (a.erupt_age_lower..a.erupt_age_upper)}.uniq
    
    due_range_values = ::Child::ToothChart::ChartManager.tooth_chart_due_range_for_child(member,options)
        
    or_condition = []
    shed_due_age_range_index =  system_shed_range.index(due_range_values[:shed_range].first).to_i
    erupt_due_age_range_index =  system_erupt_range.index(due_range_values[:erupt_range].first).to_i
    
    shed_age_ranges = shed_due_age_range_index > 0 ? system_shed_range[0..(shed_due_age_range_index-1)] : []
    erupt_age_ranges = erupt_due_age_range_index > 0 ? system_erupt_range[0..(erupt_due_age_range_index-1)] : []
    
    # shed in below range
    shed_age_ranges.each do |shed_age|
      or_condition << {:shed_age_lower.gte=>shed_age.first,:shed_age_upper.lte=>shed_age.last}
    end
    overdue_shedded_tooth = []
    if shed_age_ranges.present?
      ::Child::ToothChart::ChartManager.or(or_condition).each do |tooth_manager|
       tooth_manager[:estimated_type]="erupted"
       overdue_shedded_tooth << tooth_manager
      end
      overdue_shedded_tooth = overdue_shedded_tooth.flatten.uniq
    end
    or_condition = []
    
    # erupt in below range
    erupt_age_ranges.each do |erupt_age|
      or_condition << {:erupt_age_lower.gte=>erupt_age.first,:erupt_age_upper.lte=>erupt_age.last}
    end
    overdue_erupt_tooth = []
    if erupt_age_ranges.present?
      ::Child::ToothChart::ChartManager.or(or_condition).each do |tooth_manager|
        tooth_manager[:estimated_type]="erupted"
        overdue_erupt_tooth << tooth_manager
      end
    end
     overdue_erupt_tooth = overdue_erupt_tooth.flatten.uniq
    [overdue_shedded_tooth + overdue_erupt_tooth].flatten
  end

  def self.seed_data
    create!([
      {position: 'Upper left', name:  'Central incisor', erupt_age_lower: 8, erupt_age_upper: 12, shed_age_lower: 6, shed_age_upper: 7},
      {position: 'Upper left', name:  'Lateral incisor', erupt_age_lower: 9, erupt_age_upper: 13, shed_age_lower: 7, shed_age_upper: 8},
      {position: 'Upper left', name:  'Canine', erupt_age_lower: 16, erupt_age_upper: 22, shed_age_lower: 10, shed_age_upper: 12},
      {position: 'Upper left', name:  'First molar', erupt_age_lower: 13, erupt_age_upper: 19, shed_age_lower: 9, shed_age_upper: 11},
      {position: 'Upper left', name:  'Second molar', erupt_age_lower: 25, erupt_age_upper: 33, shed_age_lower: 10, shed_age_upper: 12},

      {position: 'Upper right', name:  'Central incisor', erupt_age_lower: 8, erupt_age_upper: 12, shed_age_lower: 6, shed_age_upper: 7},
      {position: 'Upper right', name:  'Lateral incisor', erupt_age_lower: 9, erupt_age_upper: 13, shed_age_lower: 7, shed_age_upper: 8},
      {position: 'Upper right', name:  'Canine', erupt_age_lower: 16, erupt_age_upper: 22, shed_age_lower: 10, shed_age_upper: 12},
      {position: 'Upper right', name:  'First molar', erupt_age_lower: 13, erupt_age_upper: 19, shed_age_lower: 9, shed_age_upper: 11},
      {position: 'Upper right', name:  'Second molar', erupt_age_lower: 25, erupt_age_upper: 33, shed_age_lower: 10, shed_age_upper: 12},

      {position: 'Lower right', name:  'Central incisor', erupt_age_lower: 6, erupt_age_upper: 10, shed_age_lower: 6, shed_age_upper: 7},
      {position: 'Lower right', name:  'Lateral incisor', erupt_age_lower: 10, erupt_age_upper: 16, shed_age_lower: 7, shed_age_upper: 8},
      {position: 'Lower right', name:  'Canine', erupt_age_lower: 17, erupt_age_upper: 23, shed_age_lower: 9, shed_age_upper: 12},
      {position: 'Lower right', name:  'First molar', erupt_age_lower: 14, erupt_age_upper: 18, shed_age_lower: 9, shed_age_upper: 11},
      {position: 'Lower right', name:  'Second molar', erupt_age_lower: 23, erupt_age_upper: 31, shed_age_lower: 10, shed_age_upper: 12},

      {position: 'Lower left', name:  'Central incisor', erupt_age_lower: 6, erupt_age_upper: 10, shed_age_lower: 6, shed_age_upper: 7},
      {position: 'Lower left', name:  'Lateral incisor', erupt_age_lower: 10, erupt_age_upper: 16, shed_age_lower: 7, shed_age_upper: 8},
      {position: 'Lower left', name:  'Canine', erupt_age_lower: 17, erupt_age_upper: 23, shed_age_lower: 9, shed_age_upper: 12},
      {position: 'Lower left', name:  'First Molar', erupt_age_lower: 14, erupt_age_upper: 18, shed_age_lower: 9, shed_age_upper: 11},
      {position: 'Lower left', name:  'Second Molar', erupt_age_lower: 23, erupt_age_upper: 31, shed_age_lower: 10, shed_age_upper: 12}
    ])

  end

  def title
    (position + ' ' + name rescue '').capitalize
  end
  
  def erupts_age_range
    erupt_age_lower.to_s + '-' + erupt_age_upper.to_s + ' ' + 'months' rescue ""
  end


  def sheds_age_range
    shed_age_lower.to_s + '-' + shed_age_upper.to_s + ' ' + 'years' rescue ""
  end

  # def erupts_desc
  #   'Erupts: ' + erupts_age_range rescue ''
  # end

  # def sheds_desc
  #   'Sheds: ' + sheds_age_range rescue ''
  # end

  def title_as_key
    position.downcase.gsub(/\s+/, "_") + '_' + name.downcase.gsub(/\s+/, "_") rescue ''
  end

  def due_by_text(is_erupted= true)
    begin
      if is_erupted
        # 'Due by the age of ' + (erupt_age_upper).to_s + ' months'
        'Usually erupts around the age of ' + (erupt_age_lower).to_s + ' months'
      else
        'Usually sheds around the age of ' + (shed_age_lower).to_s + ' years'
      end
    rescue Exception=>e
    end
  end
  
  def info_text
    str = 'Usually erupts around the age of ' + erupts_age_range
    str += ' and '
    str += 'sheds around the age of ' + sheds_age_range
    str
  end

  def range
    data = {}
    chart_manager = self
    erupt = Child::ToothChart::ToothDetails::Erupt
    shed = Child::ToothChart::ToothDetails::Shed
    
    data[erupt] = chart_manager.erupt_age_lower.to_s + "-" + chart_manager.erupt_age_upper.to_s
    data[shed] = chart_manager.shed_age_lower.to_s + "-" + chart_manager.shed_age_upper.to_s
    data.with_indifferent_access
  end

  def get_expected_date(child,type)
    expected_date = calculate_expected_date_with_upper_limit_in_range(child,type).to_date 
    expected_date = Date.today if expected_date > Date.today
    expected_date.to_s
  end

  def get_upper_limit(type)
    if type == Child::ToothChart::ToothDetails::Erupt
      upper_limit_month = erupt_age_upper
    elsif type == Child::ToothChart::ToothDetails::Shed
      upper_limit_month = shed_age_upper * 12
    else
      lower_limit_month = nil
    end
    upper_limit_month
  end

  def get_lower_limit(type)
    if type == Child::ToothChart::ToothDetails::Erupt
      lower_limit_month = erupt_age_lower
    elsif type == Child::ToothChart::ToothDetails::Shed
      lower_limit_month = shed_age_lower * 12
    else
      lower_limit_month = nil
    end
    lower_limit_month
  end

  def calculate_expected_date_with_upper_limit_in_range(child,type)
    upper_limit = get_upper_limit(type)
    expected_date = child.birth_date + upper_limit.months
    expected_date
  end

  def calculate_expected_date_with_lower_limit_in_range(child,type)
    lower_limit = get_lower_limit(type)
    expected_date = child.birth_date + lower_limit.month
    expected_date
  end


  def get_estimated_erupt_shed_description(child_id,type=nil)
    data = {}
    erupt = Child::ToothChart::ToothDetails::Erupt
    shed = Child::ToothChart::ToothDetails::Shed
    tooth_manager = self
    child = ChildMember.find(child_id.to_s)
    if type == erupt || type.blank?
      estimated_date = tooth_manager.calculate_expected_date_with_upper_limit_in_range(child,erupt)
      data[erupt] = "Estimate eruption at #{ApplicationController.helpers.calculate_age2(child.birth_date,estimated_date)}"
    end
    if type == shed || type.blank?
      estimated_date = tooth_manager.calculate_expected_date_with_upper_limit_in_range(child,shed)
      data[shed] = "Estimate shedding at #{ApplicationController.helpers.calculate_age2(child.birth_date,estimated_date)}"
    end
    data
  end

  def get_detail_in_hash(child_id=nil,tooth_detail=nil,data_type=nil,options={})
    begin
      data = {}
      member = options[:member] || ChildMember.where(:id=>child_id).last
      shed = Child::ToothChart::ToothDetails::Shed
      erupt = Child::ToothChart::ToothDetails::Erupt
      chart_manager = self
      data[:tooth_manager_id] = chart_manager.id
      data[:title] = chart_manager.title
      data[:name] = chart_manager.title_as_key
      data[:child_tooth_id] =  nil
      if data_type != "upcoming"
        data[:added] = false
        data[:info_text] = chart_manager.info_text

      end
      data[:erupted_text] = "Usually at #{chart_manager.erupt_age_lower}-#{chart_manager.erupt_age_upper} months"
      data[:shedded_text] =  "Usually at #{chart_manager.shed_age_lower}-#{chart_manager.shed_age_upper} years"
      expected_erupted_date = member.birth_date + (chart_manager.erupt_age_upper).month
      expected_sheded_date = member.birth_date + (chart_manager.shed_age_upper).year
      is_erupted = (chart_manager[:estimated_type] == "erupted" || expected_erupted_date >= Date.today)
      data[:expected_erupted_date] = expected_erupted_date
      data[:expected_shedded_date] = expected_sheded_date
      if tooth_detail.present?
        type = tooth_detail.type
        data[:child_tooth_id] = tooth_detail.id
        current_state =  type
        date_on = tooth_detail.date_on(type) rescue nil
        previous_state = Child::ToothChart::ToothDetails::ToothStatus.invert[tooth_detail.tooth_state.to_i - 1] rescue nil
        data[:type] = type
        is_erupted = data[:type] == "erupted"
        
        data[:updated_date] = date_on
        data[:added] = tooth_detail.chart_manager_id.to_s == chart_manager.id.to_s
        data[:current_status_description] = tooth_detail.erupt_shed_description[current_state]
        data[:current_type] = type
        data[:current_type_date] = date_on
        data[:previous_status_description] = "(" + tooth_detail.erupt_shed_description[previous_state] + ")" rescue nil
        data[:tooth_status] = tooth_detail["#{type}_status"] || "unaccomplished"
        if data[:updated_date].present?
          data[:updated_at_age] = ApplicationController.helpers.calculate_age4(member.birth_date,date_on) rescue nil
          date_on = date_on.to_date
          data[:added_at_age_text] = ApplicationController.helpers.calculate_age5(member.birth_date,date_on) + " • " +  date_on.to_date5
          age_in_year = ((member.birth_date - date_on.to_date).abs.to_i/365) + 1 rescue 1
        else
          date_on = data[:type] == "erupted" ? expected_erupted_date : expected_sheded_date
          # date_on = is_erupted ? expected_erupted_date : expected_sheded_date
          age_in_year = ((member.birth_date - date_on.to_date).abs.to_i/365) + 1 rescue 1
        end
      else
        status = is_erupted ? ::Child::ToothChart::ToothDetails::Erupt : ::Child::ToothChart::ToothDetails::Shed
        date_on = is_erupted ? expected_erupted_date : expected_sheded_date
        age_in_year = ((member.birth_date - date_on.to_date).abs.to_i/365) + 1 rescue 1
          
        data[:estimated_tooth_status] = status
        data[:estimated_type] = status
      end
      data[:date_on] = date_on
      data[:due_by_text] = chart_manager.due_by_text(is_erupted)
      data["updated_at_age_text"] = "In the #{age_in_year.ordinalize} year"

      data
    rescue Exception => e
    end
    end

  
  class << self

    def get_progress_report_by_name(member,type,chart_manager_ids=[])
    data = {}
    score = ((1/3.to_f) * GlobalSettings::ToothChartProgressCalculationFactors[:scale]).round(2)
    scale = GlobalSettings::ToothChartProgressCalculationFactors[:scale]
    all_data = Child::ToothChart::ChartManager.get_tooth_manager_type_wise(chart_manager_ids)
    all_data.each do |name,values|
       
      tooth_chart_manager_ids = values[:tooth_manager_ids]
      stats = {"cannot_tell"=>0,"delayed"=>0}
      teeth = Child::ToothChart::ToothDetails.where(child_id:member.id).in(chart_manager_id:tooth_chart_manager_ids)
      teeth.each do |tooth|
        tooth_chart_manager = tooth.tooth_chart_manager
        if tooth.status(type) == "unaccomplished" && tooth.progress_score(tooth_chart_manager,member,type) < score
          stats["delayed"] = stats["delayed"] + 1
        end
      end
      data[name] = stats
    end
    data
  end


    def all_ids
      Child::ToothChart::ChartManager.all.pluck(:id)
    end

    def get_tooth_manager_type_wise(tooth_manager_ids=nil)
      tooth_type_ids = {}
      if !tooth_manager_ids.nil?
        tooth_manager_ids = tooth_manager_ids.map{|a| Moped::BSON::ObjectId(a.to_s)} 
        selection_critriea = { "_id" => {"$in"=>tooth_manager_ids }}
      else
        selection_critriea = {}
      end
      chart_manager_data =  Child::ToothChart::ChartManager.collection.aggregate([      
                               { "$match" => selection_critriea },
                                 { "$group" => {
                                    "_id" => "$name" ,
                                    "count" => { "$sum" => 1 },
                                    "uniqueIds": { "$addToSet": "$_id" }
                                  }
                                }
                              ])
       
      data = {}
      chart_manager_data.each do |result|
       data[result["_id"] ] = {:tooth_manager_ids =>result["uniqueIds"],:count=>result["count"],:type=>result["_id"] }
      end
      data
    end

    
    def get_tooth_type_ids
      tooth_manager_type_wise_data = get_tooth_manager_type_wise
      {
        first_molar_ids: tooth_manager_type_wise_data['First Molar'][:tooth_manager_ids],
        second_molar_ids: tooth_manager_type_wise_data['Second Molar'][:tooth_manager_ids],
        canine_ids: tooth_manager_type_wise_data['Canine'][:tooth_manager_ids],
        incisor_ids: tooth_manager_type_wise_data['Central incisor'][:tooth_manager_ids] + tooth_manager_type_wise_data['Lateral incisor'][:tooth_manager_ids]
      }
    end

    def get_tooth_type_ids_count
      tooth_manager_type_wise_data = get_tooth_manager_type_wise
       
      {
        first_molars: tooth_manager_type_wise_data['First Molar'][:count],
        second_molars: tooth_manager_type_wise_data['Second Molar'][:count],
        total_canine: tooth_manager_type_wise_data['Canine'][:count],
        total_incisor: tooth_manager_type_wise_data['Central incisor'][:count] + tooth_manager_type_wise_data['Lateral incisor'][:count]
      }
    end
  end

end
