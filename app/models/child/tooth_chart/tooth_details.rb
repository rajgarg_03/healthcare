class Child::ToothChart::ToothDetails

  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::Validations
  include Child::ToothChart::ToothChartTrack
  
  field :child_id, type: String
  field :chart_manager_id, type: String
  field :tooth_state, type:Integer # 0=> not yet/unaccomplished, 1=> erupted, 2=> shedded
  # field :type, type: String # erupt/shed
  field :erupted_on_date, type: Time # date/nil
  field :shedded_on_date, type: Time # date/nil
  field :erupted_status, :type=>String, :default=>"accomplished" #eg [accomplished, unknown, unaccomplished, removed]   
  field :shedded_status, :type=>String, :default=>"accomplished" #eg [accomplished, unknown, unaccomplished, removed]   
  field :status_by, :type=>String, :default=>"system" #eg system/nil/user    
  

  # field :chart_manager_name, type: String
  
  index({child_id:1})
  # index({type:1})
  index({chart_manager_id:1})
  # belongs_to :member, foreign_key: 'child_id', class_name: 'Member'
  # belongs_to :chart_manager, foreign_key: 'chart_manager_id', class_name: 'Child::ToothChart::ChartManager'

  validates :child_id, :type,  presence: true
   
   Erupt = "erupted"
   Shed = "shedded"
   ToothStatus = {"unaccomplished"=> 0,Erupt=>1,Shed=>2}.with_indifferent_access

  def member
    Member.where(id:self.child_id).last
  end
  
  def tooth_chart_manager
    Child::ToothChart::ChartManager.where(id:self.chart_manager_id).last
  end

  # params=>{:teeth => [{:chart_manager_id=>"",:tooth_status=>"",:type=>,date=>""}], :member_id=>"",type=>"erupted/shedded"}
  def self.add_teeth(current_user,params,options={:status_by=>"user"})
    status_by = options[:status_by]
    current_date = nil #Time.now.in_time_zone
    # raise "Please provide tooth type #{Erupt}/#{Shed}/Not yet" if params[:type].blank? || !["unaccomplished",Erupt,Shed].include?(params[:type])
    params[:teeth].each do |data|
      type =  data[:type].present? ? (data[:type].downcase rescue Erupt) : Erupt
      chart_manager_id = data[:chart_manager_id]
      tooth_status = data[:tooth_status].downcase
      current_date = data[:date]
      if tooth_status == "unaccomplished"
        tooth_state = Child::ToothChart::ToothDetails::ToothStatus[tooth_status]
      else
        tooth_state = Child::ToothChart::ToothDetails::ToothStatus[type]
      end
      tooth = Child::ToothChart::ToothDetails.where(child_id: params[:member_id], chart_manager_id: chart_manager_id).last  
      # return if entry already exist
      if tooth.present? 
        #if ![Erupt,Shed].include?(type) || tooth["#{type}_on_date"].present? && tooth.status_by == "user" && tooth.tooth_state <= tooth_state
        #  raise Message::API[:error][:add_tooth]
        #else
        if tooth_status == "unaccomplished"
          current_date = nil
          tooth.update_attributes("#{Erupt}_on_date"=>current_date,"#{Shed}_on_date"=>current_date,:status_by=>status_by,"#{Erupt}_status"=>tooth_status,"#{Shed}_status"=>tooth_status,:tooth_state=>tooth_state)
        else
          tooth.update_attributes("#{type}_on_date"=>current_date,:status_by=>status_by,"#{type}_status"=>tooth_status,:tooth_state=>tooth_state)
        end
        #end
      else
        if tooth_status == "unaccomplished"
          current_date = nil
          tooth = Child::ToothChart::ToothDetails.create!(child_id: params[:member_id], chart_manager_id: chart_manager_id,  "#{Erupt}_on_date"=> current_date, :status_by=>status_by,"#{Shed}_status"=>tooth_status,"#{Erupt}_status"=>tooth_status,:tooth_state=>tooth_state)
        else
          tooth = Child::ToothChart::ToothDetails.create!(child_id: params[:member_id], chart_manager_id: chart_manager_id,  "#{type}_on_date"=> current_date, :status_by=>status_by,"#{type}_status"=>tooth_status,:tooth_state=>tooth_state)
        end
      end 
    end
  end

  # params = {:date=>"",:type=>"erupted/shedded",:tooth_status=>"accomplished/unaccomplished",:member_id=>"",chart_manager_id=>""}
  def self.update_tooth(current_user,params,options={:status_by=>"user"})
    status_by = options[:status_by] 
    tooth_date = params[:date]
    tooth_status = (params[:tooth_status] || "accomplished").downcase
    tooth = Child::ToothChart::ToothDetails.where(id:params[:id]).last   
    child = ChildMember.find(tooth.child_id)
    params[:type] = "unaccomplished" if params[:type].blank? && tooth_status == "unaccomplished"
    raise "Please provide tooth type #{Erupt}/#{Shed}/Not yet" if params[:type].blank? || !["unaccomplished",Erupt,Shed].include?(params[:type].downcase) 
    type = params[:type].downcase
    if tooth_status == "unaccomplished"
      tooth_state = Child::ToothChart::ToothDetails::ToothStatus[tooth_status]
    else
      tooth_state = Child::ToothChart::ToothDetails::ToothStatus[type]
    end
    
    if tooth.present? 
      tooth_type_value = (type == "unaccomplished") ? "Not yet" : type
      raise "You can not change '#{tooth.type.capitalize}' to '#{tooth_type_value.capitalize}', please delete and add it again." if tooth.tooth_state > tooth_state
      if type == "unaccomplished"
        tooth_date = nil
      else
        raise "Please provide a valid date for #{type} tooth" if tooth_date.blank? 
        raise "#{type.capitalize} date can't be less than child birth date" if tooth_date.to_date < child.birth_date
        raise "#{Shed.capitalize} date can't be less than #{Erupt.capitalize} date." if tooth["#{Erupt}_on_date"].present? && tooth["#{Erupt}_on_date"].to_date > tooth_date.to_date && type == Shed
        tooth_date = tooth_date.to_date.to_time1(Time.current.strftime("%R"))
      end
      if [Erupt,Shed].include?(type)
        tooth.update_attributes("#{type}_on_date"=>tooth_date,:status_by=>status_by,"#{type}_status"=>tooth_status,:tooth_state=>tooth_state)
      elsif tooth_status == "unaccomplished" 
        tooth_date = nil
        tooth.update_attributes(:tooth_state=> tooth_state,"#{Erupt}_on_date"=>tooth_date,"#{Shed}_on_date"=>tooth_date,:status_by=>status_by,"#{Erupt}_status"=>tooth_status,"#{Shed}_status"=>tooth_status)
      end
    else
      # return if entry not exist
      raise Message::API[:error][:update_tooth]
    end

  end

  def delete_tooth(current_user,params,options={})
    tooth = self
    tooth_type = params[:type].downcase if params[:type].present?
    if tooth_type == Erupt
      tooth.update_attributes({"#{Erupt}_status":nil,"#{Erupt}_on_date":nil})
    elsif tooth_type == Shed
      tooth.update_attributes({"#{Shed}_status":nil,"#{Shed}_on_date":nil})
    elsif tooth_type == "all" || tooth_type.blank?
      tooth.destroy
    else
      raise "Type #{Erupt}/#{Shed} not found"
    end
  end

  def self.tooth_status(type,status_value)
    status_value = [status_value].flatten rescue []
    where({"#{type}_status" => { "$in" => status_value} })
  end

  def self.not_in_unaccomplished_status(status=nil)
     self.or({"#{Erupt}_status".to_sym.in => ["accomplished"]},{"#{Shed}_status".to_sym.in => ["accomplished"]})
  end
  
  def date_on(tooth_type=nil)
    tooth = self
    type = tooth_type || tooth.type
    (tooth["#{type}_on_date"]).to_date.to_date4 rescue nil
  end

  def type
    tooth = self
    current_state = Child::ToothChart::ToothDetails::ToothStatus.invert[tooth.tooth_state]
    return current_state if current_state.present?
    if tooth["#{Shed}_on_date"].present? 
      Shed
    elsif tooth["#{Erupt}_on_date"].present?
      Shed
    else
      "unaccomplished"
    end
    # if !teeth["#{Shed}_on_date"].blank? || teeth["#{Shed}_status"] == "unaccomplished"
    #   Shed
    # elsif teeth["#{Erupt}_on_date"].present?
    #   Erupt
    # else
    #   Erupt
    # end
        
  end

  def erupt_shed_description
    tooth = self
    if tooth.status_by == "system"
      tooth_manager = tooth.tooth_chart_manager
      estimated_data = tooth_manager.get_estimated_erupt_shed_description(tooth.child_id)
    end
    child = tooth.member
    tooth_manager = tooth.tooth_chart_manager
    erupted_date = tooth["#{Erupt}_on_date"]  
    shedded_date = tooth["#{Shed}_on_date"]
    data = {}
    if erupted_date.present?
      data[Erupt] = "Erupted at #{ApplicationController.helpers.calculate_age(child.birth_date,erupted_date)}"
    else
      if tooth.status_by == "user"
        data[Erupt] = "Erupted date not known" #{}"Estimated eruption at the age #{ApplicationController.helpers.calculate_age(child.birth_date,estimated_date)}"
      else
        data[Erupt] = estimated_data[Erupt] 
      end
    end
    if shedded_date.present?
      data[Shed] = "Shedded at #{ApplicationController.helpers.calculate_age(child.birth_date,shedded_date)}"
    else
      if tooth.status_by == "user"
        data[Shed] = "Shedded date not known" #{}"Estimated eruption at the age #{ApplicationController.helpers.calculate_age(child.birth_date,estimated_date)}"
      else
        data[Shed] = estimated_data[Shed] # "Estimated shedding at the age #{ApplicationController.helpers.calculate_age(child.birth_date,estimated_date)}"
      end
    end
    if tooth.tooth_state == 0 || tooth.tooth_state.nil?
      data[Erupt] = "Not erupted yet" if tooth["#{Erupt}_status"] == "unaccomplished"
      data[Shed] = "Not shedded yet" if tooth["#{Shed}_status"] == "unaccomplished"
      data["unaccomplished"] = "Not erupted yet"
    end
    data
  end

  def date_on_text
    begin
      tooth =  self
      if tooth.type ==  Erupt
        'erupt on ' + (tooth["#{Erupt}_on_date"]).to_date.to_date3 rescue nil
      elsif tooth.type == Shed
        'shed on ' + (tooth["#{Shed}_on_date"]).to_date.to_date3
      else
        'erupt on ' + (tooth["#{Erupt}_on_date"]).to_date.to_date3 rescue nil
      end
    rescue Exception=> e 
      Rails.logger.info e.message
      ""
    end
  end

  def self.estimated_tooth(member,type)
    where(child_id:member.id).in(status_by:"system").tooth_status(type,"accomplished")
  end

  def self.update_all_tooth_status(type,status,status_by=nil)
    condition = {}
    if type == Erupt
      condition ={"#{Erupt}_status"=>status}
    elsif type == Shed
      condition = {"#{Shed}_status"=>status}
    end
    condition["status_by"] = status_by if status_by.present?
    update_all(condition)
  end

  def self.achieved_tooth(member,type)
    where(child_id:member.id).in(status_by:["user",nil]).tooth_status(type,["accomplished",nil])
  end

  class << self
    def child_tooths_data(molar_ids, child_id)
      Child::ToothChart::ToothDetails.where(:chart_manager_id.in => molar_ids, child_id: child_id).not_in_unaccomplished_status
    end
  
    def erupt_data
     self.or({"#{Erupt}_on_date".to_sym.nin => [nil]},{"#{Erupt}_status".to_sym.in => ["accomplished",nil]})#.or({"#{Shed}_on_date".to_sym.in => [nil]},{"#{type}_status".to_sym.nin=>["unaccomplished"]})
    end

    def shed_data
      self.or({"#{Shed}_on_date".to_sym.nin => [nil]},{"#{Shed}_status"=>"unaccomplished"})
    end

    def child_data(child_id,type=nil)
      data = Child::ToothChart::ToothDetails.where(child_id: child_id)#.not_in_unaccomplished_status
      if type.present? 
        if type == Erupt
          data = data.erupt_data
        else
          data = data.shed_data
        end
      end
      data = data.order_by_type(type)
      data
    end

    def order_by_type(type=[])
      type = [type].flatten
      if type.present?
        order = (type.sort - ["unaccomplished"]).map{|a| "#{a}_on_date desc"}
        order = (order + ["tooth_state desc"]).join(",")
      else
        order = "#{Erupt}_on_date desc, tooth_state desc"
      end
      self.order_by(order)
    end

    def child_type_wise_count(child_id)
       tooth_chart_manager_ids = Child::ToothChart::ChartManager.get_tooth_type_ids
   
      count_array = []
      data = {}
      tooth_chart_manager_ids.each do |key, ids|
        data[key] =  Child::ToothChart::ToothDetails.child_tooths_data(ids, child_id).count
        #count_array << Child::ToothChart::ToothDetails.child_tooths_data(ids, child_id).count
      end
     # count_array
      data
    end

    def get_data_group_by_age(member,current_user,tooth_data,options={})
      tooth_data = tooth_data.sort_by{|teeth| teeth["updated_date"].to_date rescue nil}
      tooth_data = tooth_data.group_by{|teeth| teeth["updated_at_age_text"]}
      data = {}
      tooth_data.each do |age,tooth_list|
       erupted_shedded_count = get_erupted_sheeded_count_from_list(member,current_user,tooth_list,options)
       data[age] = {:tooth_list=>tooth_list}.merge(erupted_shedded_count)
      end
      data
    end

    def get_erupted_sheeded_count_from_list(member,current_user,teeth,options={})
      erupted_count = 0 
      shedded_count = 0
      teeth.each do |tooth|
        if tooth[:type] == ::Child::ToothChart::ToothDetails::Erupt
          erupted_count = erupted_count + 1 
        elsif tooth[:type] == ::Child::ToothChart::ToothDetails::Shed
          shedded_count = shedded_count + 1 
        end
      end
      {:erupted=>erupted_count,:shedded=>shedded_count}
    end

    def oblique_tooths_stats(child_id)
      tooth_manager_ids_count = Child::ToothChart::ChartManager.get_tooth_type_ids_count
      child_tooth_count = child_type_wise_count(child_id)
      tooth_stats = []
      updated_at_data = last_tooth_updated_at(child_id) 
      first_molars_stat = child_tooth_count[:first_molar_ids].to_s + '/' + tooth_manager_ids_count[:first_molars].to_s
      second_molars_stat = child_tooth_count[:second_molar_ids].to_s + '/' + tooth_manager_ids_count[:second_molars].to_s
      canine_stat = child_tooth_count[:canine_ids].to_s + '/' + tooth_manager_ids_count[:total_canine].to_s
      incisor_stat =  child_tooth_count[:incisor_ids].to_s + '/' + tooth_manager_ids_count[:total_incisor].to_s
      tooth_stats << {"name"=>"First molar","stat"=>first_molars_stat,"updated_at"=>updated_at_data[0]}
      tooth_stats << {"name"=>"Second molar","stat"=>second_molars_stat,"updated_at"=>updated_at_data[1]}
      tooth_stats << {"name"=>"Canine","stat"=>canine_stat,"updated_at"=>updated_at_data[2]}
      tooth_stats << {"name"=>"Incisors","stat"=>incisor_stat,"updated_at"=>updated_at_data[3]}
      tooth_stats
    end

    def last_tooth_updated_at(child_id)
      updated_at_array = []
      tooth_chart_manager_ids = Child::ToothChart::ChartManager.get_tooth_type_ids
      tooth_chart_manager_ids.each do |key, ids|
        tooth_data = Child::ToothChart::ToothDetails.where(child_id: child_id).in(chart_manager_id:ids).not_in_unaccomplished_status
        if tooth_data.blank?
          updated_at_array << ''
        else
          updated_at_array << 'Updated: ' + tooth_data.first.updated_at.convert_to_ago2
        end
      end
      updated_at_array
    end

    def tooth_stats_data_hash(child_id)
      tooth_stats_hash =  oblique_tooths_stats(child_id)
      tooth_stats_hash
    end

    def upcoming_tooth_data_hash(child_id,options={})
      data = []
      options[:member] = Member.find(child_id) rescue nil
      child_data_ids = Child::ToothChart::ToothDetails.where(child_id: child_id).not_in_unaccomplished_status.pluck(:chart_manager_id)
      tooth_chart_manager_list = Child::ToothChart::ChartManager.not_in(:id => child_data_ids).order_by("erupt_age_upper asc")
      if options[:age_in_month].present?
         age_in_month = options[:age_in_month]
        if options[:age_in_month] >  33
          age_in_month = 33
        end
        # tooth_chart_manager_list = tooth_chart_manager_list.where(:erupt_age_upper.gte=>age_in_month)
      end
      data = tooth_chart_manager_list.map{|tooth_chart_manager| tooth_chart_manager.get_detail_in_hash(child_id,nil,data_type="upcoming",options) }
      data
    end

    def child_data_hash(params, type=nil,options={})
      child_id = params[:member_id]
      member = Member.find child_id
      options[:member] = member
      if params[:type].present? && params[:type] != "all"
        type = params[:type].split(",").map(&:downcase) 
      else
        type = ["unaccomplished",Erupt,Shed]
      end
      if options[:type].present?
        type = options[:type]
      end
      child_data_array = []
       
      child_data = Child::ToothChart::ToothDetails.where(child_id:child_id).order_by_type(type)
      or_condition_with_and = []
      type_condition = []
      tooth_state_condition = []
      type.each do |tooth_state|

        if tooth_state == "unaccomplished"
           # child_data = child_data.or(:tooth_state=>ToothStatus[tooth_state])
          type_condition << {:tooth_state=>ToothStatus[tooth_state]}
          tooth_state_condition << {"tooth_state"=>Child::ToothChart::ToothDetails::ToothStatus[tooth_state]}

        else
          type_condition << {"#{tooth_state}_status"=>"accomplished"}
          tooth_state_condition << {"tooth_state"=>Child::ToothChart::ToothDetails::ToothStatus[tooth_state]}
        end
      end
      or_condition_with_and << {"$or"=>type_condition} if type_condition.present?
      or_condition_with_and << {"$or"=>tooth_state_condition} if tooth_state_condition.present?

      time_range_condition = []
      if params[:start_date].present? && params[:end_date].present?
        params[:start_date] = "01/01/" + params[:start_date]
        params[:end_date] = "31/12/" + params[:end_date]
        if type.include?(Erupt)
          time_range_condition <<{"#{Erupt}_on_date"=>{"$gte"=>params[:start_date], "$lte"=>params[:end_date]}}
        end
        if type.include?(Shed)
          time_range_condition <<{"#{Shed}_on_date"=>{"$gte"=>params[:start_date], "$lte"=>params[:end_date]}}
        end
        if type.include?("unaccomplished")
          time_range_condition << {:tooth_state=>ToothStatus["unaccomplished"]}
        end
      end
      or_condition_with_and << {"$or"=>time_range_condition} if time_range_condition.present?
      child_data = child_data.where("$and"=>or_condition_with_and) if or_condition_with_and.present?
      if params[:tooth_type].present?
        condition = []
        params[:tooth_type].each do |teeth_type|
          condition << ({:name=>/#{teeth_type}$/i})
        end
        tooth_chart_manager_ids = Child::ToothChart::ChartManager.or(condition).pluck(:id).map(&:to_s)
        child_data = child_data.where(:chart_manager_id.in=>tooth_chart_manager_ids)
      end
      member = options[:member] || Member.find(child_id)
      child_data.each do |tooth_detail|
        begin
          data_hash = {}
          tooth_chart_manager = Child::ToothChart::ChartManager.find(tooth_detail.chart_manager_id)
          data_hash = tooth_chart_manager.get_detail_in_hash(child_id,tooth_detail,data_type=nil,options)
          child_data_array << data_hash
          if options[:type].present?
            tooth_date_on = nil
            if data_hash[:type] == "shedded"
              data_hash_for_erupt = data_hash.dup
              data_hash_for_erupt[:type] = "erupted"
              tooth_date_on = tooth_detail.date_on("erupted") || data_hash_for_erupt[:expected_erupted_date]
              data_hash_for_erupt[:date_on] = tooth_date_on.to_date
              data_hash_for_erupt[:updated_date] = data_hash_for_erupt[:date_on]
              if tooth_detail.date_on("erupted").present?
                data_hash_for_erupt[:added_at_age_text] = ApplicationController.helpers.calculate_age5(member.birth_date,tooth_date_on) + " • " +  tooth_date_on.to_date.to_date5
              else
                data_hash_for_erupt[:added_at_age_text] = nil
              end
              age_in_year = ((member.birth_date - data_hash_for_erupt[:date_on].to_date).abs.to_i/365) + 1 #rescue 1
              data_hash_for_erupt["updated_at_age_text"] = "In the #{age_in_year.ordinalize} year"
              
              child_data_array << data_hash_for_erupt
            end
          end
        rescue Exception => e
          Rails.logger.info e.message
        end
      end
      child_data_array = child_data_array.sort_by{|tooth| tooth[:date_on].to_date}.reverse
      child_data_array
    end
    
    # Due and upcoming tooth
    def due_tooth(current_user,member,options={})
      data = []
      options[:member] = member
      
      # Tooth shuold be in list as per current age
      teeth_managers_ids_should_be_in_list = Child::ToothChart::ChartManager.due_upcoming_tooth_manager(current_user,member,options).pluck(:id).map(&:to_s) rescue []
      
      added_teeth = Child::ToothChart::ToothDetails.where(child_id:member.id,status_by:"user")#.or({:erupted_on_date.ne=>nil,:tooth_state=>1},{:shedded_on_date.ne=>nil,:tooth_state=>2})
      added_teeth_manager_ids = added_teeth.map(&:chart_manager_id)
      
      due_teeth_manager_ids = teeth_managers_ids_should_be_in_list - added_teeth_manager_ids
      
      tooth_group_by_manager_id = Child::ToothChart::ToothDetails.where(:id.in=>added_teeth_manager_ids).group_by{|tooth| tooth.chart_manager_id}
      due_teeth_manager = Child::ToothChart::ChartManager.where(:id.in=>due_teeth_manager_ids)
      due_teeth_manager.each do |tooth_chart_manager|
        tooth_detail = {}
        tooth = tooth_group_by_manager_id[tooth_chart_manager.id.to_s].first rescue nil
        tooth_detail = tooth_chart_manager.get_detail_in_hash(member.id,tooth,data_type=nil,options)
        data << tooth_detail
      
      end
      data = data.sort_by{|tooth| tooth[:date_on].to_date}

      data[0..11]
    end


    # overdue tooth
    def overdue_tooth(current_user,member,options={})
      data = []
      options[:member] = member
      
      # Tooth shuold be in list as per current age except due and upcoming
      # teeth_managers_ids_should_be_in_list = Child::ToothChart::ChartManager.overdue_tooth_manager(current_user,member,options).pluck(:id).map(&:to_s)
      
      # Erupt/shed added tooth
      added_teeth = Child::ToothChart::ToothDetails.where(child_id:member.id,status_by:"user")#.or({:tooth_state=>1},{:tooth_state=>2})
      added_teeth_manager_ids = added_teeth.map(&:chart_manager_id)
      
      due_teeth_manager_ids = [] #teeth_managers_ids_should_be_in_list - added_teeth_manager_ids
      
      tooth_group_by_manager_id = Child::ToothChart::ToothDetails.where(:id.in=>added_teeth_manager_ids).group_by{|tooth| tooth.chart_manager_id}
      # due_teeth_manager = Child::ToothChart::ChartManager.where(:id.in=>due_teeth_manager_ids)
      due_teeth_manager = Child::ToothChart::ChartManager.overdue_tooth_manager(current_user,member,options)#.pluck(:id).map(&:to_s)
      # byebug
      due_teeth_manager.each do |tooth_chart_manager|
        tooth_detail = {}
        tooth = tooth_group_by_manager_id[tooth_chart_manager.id.to_s].first rescue nil
        if !tooth.present?
          tooth_detail = tooth_chart_manager.get_detail_in_hash(member.id,tooth,data_type=nil,options)
          tooth_detail["estimated_tooth_status"] = tooth_chart_manager[:estimated_type] || tooth_detail[:estimated_type]
          tooth_detail["estimated_type"] = tooth_chart_manager[:estimated_type] || tooth_detail[:estimated_type]
          data << tooth_detail
        end
      end
      data = data.sort_by{|tooth| tooth[:date_on].to_date}
      data
    end

    def get_tooth_manager_data_hash(child_id)
      chart_data_hash = {}
      member = Member.find child_id
      chart_manager_data = Child::ToothChart::ChartManager.all
      child_tooth = {}
      options = {:member=>member}
      Child::ToothChart::ToothDetails.child_data(child_id).map{|data| child_tooth[data.chart_manager_id.to_s] = data }
      chart_manager_data.each do |chart_manager|
        chart_manager_hash = Hash.new { |h,k| h[k] = {} }
        key = chart_manager.title_as_key
        teeth = child_tooth[chart_manager.id.to_s]
        
        chart_manager_hash[key] = chart_manager.get_detail_in_hash(child_id,teeth,nil,options)
        chart_data_hash.merge!(chart_manager_hash)
      end
      chart_data_hash.values
    end
  end

end
