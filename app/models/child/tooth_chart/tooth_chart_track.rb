module Child::ToothChart::ToothChartTrack
  def self.included(base)
    base.extend ClassMethods
  end

  def progress_score(tooth_chart_manager,child,type)
    data = {}
    if type.present? && (type == Erupt || type == Shed)
      data[type] = calculate_score(tooth_chart_manager,child,type)
    else 
      data[Erupt] = calculate_score(tooth_chart_manager,child,Erupt)
      data[Shed] = calculate_score(tooth_chart_manager,child,Shed)
    end
    data        
  end

# get_tooth_added_date
  def calculate_score(tooth_chart_manager,child,type)
  	 scale =  GlobalSettings::ToothChartProgressCalculationFactors[:scale]
     tooth = self
     # type = Erupt
     tooth_added_date = get_tooth_added_date(type)
     tooth_status = tooth.status(type)
     return (1/(2.to_f) * scale).round(1) if tooth_status == "accomplished" && (tooth_added_date.blank? || tooth.status_by == "system")
     tooth_chart_manager = tooth_chart_manager || tooth.tooth_chart_manager
     child_age_in_day = (tooth_status == "accomplished" && tooth.status_by == "user") ? tooth.tooth_added_at_child_age_in_days(child,type)  : (Date.today - child.birth_date).to_i
     x2 = (child_age_in_day) ** GlobalSettings::ToothChartProgressCalculationFactors[:power]
     cf2 = GlobalSettings::ToothChartProgressCalculationFactors[:cf2] 
     cf1 = GlobalSettings::ToothChartProgressCalculationFactors[:cf1]  
      
     c = 2 * (tooth_chart_manager["#{type}_n2_m2"])
     d = 3 * (tooth_chart_manager["#{type}_n2_2m2"] + x2).to_f
     
     begin
      calculated_value =  (c/d) * scale
     rescue Exception=>e 
       calculated_value = cf2.to_f
     end
     calculated_value = 10 if calculated_value < 0
    [calculated_value,cf2].min.to_d.truncate(2).to_f
  end

  
  def is_progress_bar_enabled?(type)
    tooth = self
    added_tooth_date = get_tooth_added_date(type)
    tooth_status = tooth.status(type)
    tooth.status_by == "user" && added_tooth_date.present? && tooth_status == "accomplished"
  end

  def tooth_accomplished_status(tooth_chart_manager,child,type)
    tooth = self
    tooth_chart_manager = tooth.tooth_chart_manager
    x = GlobalSettings::ToothProgressDay[tooth.range[type]]
    calculated_expected_date_with_upper_limit = (tooth_chart_manager.calculate_expected_date_with_upper_limit_in_range(child) || Time.now).to_date
    calculated_expected_date_with_lower_limit = (tooth_chart_manager.calculate_expected_date_with_lower_limit_in_range(child) || Time.now).to_date
    added_tooth_date = get_tooth_added_date(type)
    tooth_status = tooth.status(type)
     
    if tooth.status_by == "user"
      if tooth_status == "accomplished"
        if added_tooth_date.blank?
          "can't tell" 
        elsif added_tooth_date <= (calculated_expected_date_with_lower_limit_range - x.days)
          "Ahead"
        elsif added_tooth_date < (calculated_expected_date_with_upper_limit + x.days) && added_tooth_date > (calculated_expected_date_with_lower_limit_range - x.days)
          "On Track"
        elsif added_tooth_date >= (calculated_expected_date_with_upper_limit + x.days)
          "Behind"
        end
      elsif  tooth_status == "unaccomplished"
        if Date.today < (calculated_expected_date_with_upper_limit + x.days)
          "yet to achieve"
        else
          "Behind"  
        end
      end
    elsif tooth.status_by == "system"
      if tooth_status == "accomplished"
        "can't tell"
      elsif tooth_status == "unaccomplished"
        "Behind"
      end
    end
  end


  def progress_text(score,type)
    begin
      milestone = self
      text_value = ""
      scale = GlobalSettings::ToothChartProgressCalculationFactors[:scale] 
      two_third_of_score = (2/(3.to_f) * scale).to_d.truncate(2).to_f
      one_third_of_score = (1/(3.to_f) * scale).to_d.truncate(2).to_f
      # get_tooth_info(type)
      added_tooth_date = get_tooth_added_date(type)
      tooth_status = tooth.status(type)
      if tooth_status == "accomplished"
        return  "Provide accomplished date"  if added_tooth_date.blank? || tooth.status_by == "system"
        text_value =  case   
          when score >  two_third_of_score  
            "Achieved earlier than usual age"
          when score >= one_third_of_score && score <= two_third_of_score
            "Achieved within usual age range"
          when score <  one_third_of_score 
            "tooth has been caught up"
         end
      else tooth_status == "unaccomplished"
        text_value = case  
          when score > (2/(3.to_f) * scale).round(2)   
            "not due yet"
          when score >= one_third_of_score && score <= two_third_of_score
            "due soon"
          when score <  one_third_of_score 
             "due"
         end
      end
    rescue Exception=>e 
      Rails.logger.info e.message
      text_value = ""
    end
    text_value
  end
  
  def tooth_accomplished_at_text(child,type)
    tooth = self
    tooth_added_date = tooth.get_tooth_added_date(type)
    tooth_status = tooth.status(type)
    if tooth_status == "accomplished"
      if tooth_added_date.present? && tooth.status_by == "user"
        age = ApplicationController.helpers.calculate_age(child.birth_date,tooth_added_date) rescue ""
        "Accomplished at #{age}"
      else
         "Accomplished date unknown"
      end
    else
      "Not accomplished yet"
    end
  end

  def tooth_added_at_child_age_in_days(child,type)
    tooth_added_at = get_tooth_added_date(type)
    (tooth_added_at - child.birth_date ).to_i rescue 0
  end

  def tooth_added_at_child_age(child,type)
    tooth_added_at = get_tooth_added_date(type)
    ApplicationController.helpers.calculate_age(child.birth_date,tooth_added_at) rescue ""
  end
  
  def get_tooth_added_date(type)
    tooth = self
    if type == Erupt
      tooth_added_at = tooth["#{Erupt}_on_date"]
    elsif type == Shed
      tooth_added_at = tooth["#{Shed}_on_date"]
    else
      Date.today
    end
    tooth_added_at
  end

  
  def status(type)
    tooth = self
    tooth["#{type}_status"]
  end

  # def get_tooth_info(type)
  #   tooth =  self
  #   @tooth_status = tooth["#{type}_status"]
  #   @tooth_date = tooth["#{type}_on_date"]
  # end

  module ClassMethods

    def mark_teeth_status_unknown_or_unaccomplished(member,type=nil,status="unknown")
      if Nutrient.is_tool_activated?(member.id, "ToothChart")
        required_tooth_chart_manager_ids = eligible_tooth_chart_manager_for_child_age(member,type,status)
        
        if status == "unaccomplished"
          # mark unknown teeth to unaccomplished 
          unknown_tooth_ids = Child::ToothChart::ToothDetail.where(child_id:member.id).tooth_status(type,"unknown").pluck(:id).map(&:to_s)
          if type.blank? || type == "all"
            Child::ToothChart::ToothDetail.where(child_id:member.id).in(id:unknown_tooth_ids).update_all_tooth_status(Child::ToothChart::ToothDetails::Erupt,status,status_by="system")
            Child::ToothChart::ToothDetail.where(child_id:member.id).in(id:unknown_tooth_ids).update_all_tooth_status(Child::ToothChart::ToothDetails::Shed,status,status_by="system")
          else
            Child::ToothChart::ToothDetail.where(child_id:member.id).in(id:unknown_tooth_ids).update_all_tooth_status(Child::ToothChart::ToothDetails::Shed,status,status_by="system")
            Child::ToothChart::ToothDetail.where(child_id:member.id).in(id:unknown_tooth_ids).update_all_tooth_status(Child::ToothChart::ToothDetails::Erupt,status,status_by="system")
          end
        end
        
        added_tooth_chart_manager_ids = Child::ToothChart::ToothDetail.where(child_id:member.id).pluck(:chart_manager_id).map(&:to_s)
        Child::ToothChart::ChartManager.in(id:(required_tooth_chart_manager_ids - added_system_milestone_ids)).each do |tooth_chart_manager|
          # add new teeth and mark as unknown if not added by user in age range
          if type == Child::ToothChart::ToothDetails::Erupt
            Child::ToothChart::ToothDetail.new( member_id: member.id,  chart_manager_id:tooth_chart_manager.id, status_by:"system", "#{Child::ToothChart::ToothDetails::Erupt}_status"=>status).save
          elsif type == Shed
            Child::ToothChart::ToothDetail.new( member_id: member.id,  chart_manager_id:tooth_chart_manager.id, status_by:"system", "#{Child::ToothChart::ToothDetails::Shed}_status"=>status).save
          elsif type.blank? || type == "all"
            Child::ToothChart::ToothDetail.new( member_id: member.id,  chart_manager_id:tooth_chart_manager.id, status_by:"system", "#{Child::ToothChart::ToothDetails::Erupt}_status"=>status, "#{Shed}_status"=>status).save
          end
        end
      end
    end

    def unaccomplished_teeth_having_no_info_from_user(member,type,status=nil)
      required_tooth_chart_manager_ids = eligible_tooth_chart_manager_for_child_age(member,type,status)
      added_tooth_chart_manager_ids = Child::ToothChart::ToothDetail.where(child_id:member.id,status_by:"user").pluck(:chart_manager_id).map(&:to_s)
      Child::ToothChart::ChartManager.in(id:(required_tooth_chart_manager_ids - added_tooth_chart_manager_ids)) 
    end  
  
    def eligible_tooth_chart_manager_for_child_age(member,type,status)
      member_age = member.age_in_months
      if type == Child::ToothChart::ToothDetails::Erupt
        tooth_chart_manager_list = Child::ToothChart::ChartManager.where(:erupt_age_upper.lte=>member_age)
      else
        member_age =  member_age/12 # in years
        tooth_chart_manager_list = Child::ToothChart::ChartManager.where(:shed_age_upper.lte=>member_age)
      end
      data = tooth_chart_manager_list.select  do |tooth_chart_manager| 
        if status == "unknown"
          true 
        else
          upper_limit_month = tooth_chart_manager.upper_limit_range(type)
          estimated_date = member.birth_date + upper_limit_month.months + GlobalSettings::ToothProgressDay[k].days
          estimated_date < Date.today
        end
         
      end
    end
  end

end