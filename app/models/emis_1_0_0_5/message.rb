class Emis_1_0_0_5::Message
  # Emis::Message.get_messages(organization_uid,member_id,options)
  def self.get_messages(organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      session_id,client_sequence_hash = ::Emis::Client.get_session_detail(member_id,options)
      user_clinic_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
      return true if client_sequence_hash.blank?
      
      @message_guid =  SecureRandom.uuid
      @conversation_guid = @message_guid
      request_data = {:action=> "get_messages",:message_id=>@message_guid,:patient_id=>nil}    
      request_data[:sequence_number] = user_clinic_session.sequence_number rescue nil
          
      msg = {"GetMessagesRequest"=>
        {
          "PatientSessionRequestHeader"=>{
            "ns1:ApplicationId" => APP_CONFIG["emis_application_id"], 
            "ns1:SessionId" => session_id,
            "ns1:ClientSequenceNumberHash"=> client_sequence_hash,
            "ns1:ConversationInformation"=>{
              "ns1:MessageGuid"=> @message_guid,
              "ns1:ConversationGuid"=>@conversation_guid
            } 
          }
          
        }
      }
      # msg["GetMessagesRequest"].merge!({"ns:HeadersOnly"=>true})
      @client =Emis::Client.new_client(Emis::Client.service_wsdl_document, Emis::Client.service_namespaces)
      response = @client.call(:get_messages, message: msg)
      response = response.body[:get_messages_response][:get_messages_result]
      data = []
      if (response[:patient_session_response_header][:session_valid] == false rescue true)
        raise 'invalid session' if @retry_count.to_i <= 2
        @retry_count = 0
        Emis::Client.log_request_data(current_user,member_id,request_data,false,options)
        return {error:response[:response_information][:response_detail][:message], status:100}
      elsif (response[:response_information][:success] == false rescue true)
        error_msg = response[:response_information][:response_detail][:message]
        Emis::Client.log_request_data(current_user,member_id,request_data,false,options)
        return {status:100,error:error_msg}
      else
        Emis::Client.log_request_data(current_user,member_id,request_data,true,options)
        # message list
        messages = response[:messages][:message]
        messages = messages.sort_by{|message| message[:message_id]}.reverse #rescue messages
        @retry_count = 0
        return {status:200,message_list:messages}
      end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      Emis::Client.log_request_data(current_user,member_id,request_data,false,options)
      return {status:100, message:e.message}
    end
  end
  # Emis::Message.get_message_detail(message_id, organization_uid,member_id,options)
  def self.get_message_detail(message_id,organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
     ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
       
      user_clinic_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
      
      session_id,client_sequence_hash = ::Emis::Client.get_session_detail(member_id,options)
      return true if client_sequence_hash.blank?
      
      @message_guid =  SecureRandom.uuid
      @conversation_guid = @message_guid
      request_data = {:action=> "get_message",:message_id=>@message_guid,:patient_id=>nil}    
      request_data[:sequence_number] = user_clinic_session.sequence_number rescue nil
          
      msg = {"GetMessageRequest"=>
        {
          "PatientSessionRequestHeader"=>{
            "ns1:ApplicationId" => APP_CONFIG["emis_application_id"], 
            "ns1:SessionId" => session_id,
            "ns1:ClientSequenceNumberHash"=> client_sequence_hash,
            "ns1:ConversationInformation"=>{
              "ns1:MessageGuid"=> @message_guid,
              "ns1:ConversationGuid"=>@conversation_guid
            } 
          } ,
          "ns:MessageID"=>message_id
        }
      }
      @client =Emis::Client.new_client(Emis::Client.service_wsdl_document, Emis::Client.service_namespaces)
      response = @client.call(:get_message, message: msg)
      response = response.body[:get_message_response][:get_message_result]
      data = []
      if (response[:patient_session_response_header][:session_valid] == false rescue true)
        raise 'invalid session' if @retry_count.to_i <= 2
        Emis::Client.log_request_data(current_user,member_id,request_data,false,options)
        return {error:response[:response_information][:response_detail][:message], status:100}

      elsif (response[:response_information][:success] == false rescue true)
        Emis::Client.log_request_data(current_user,member_id,request_data,false,options)
        return {error:response[:response_information][:response_detail][:message], status:100}
      else
        Emis::Client.log_request_data(current_user,member_id,request_data,true,options)
        message = response[:message]
        @retry_count = 0
        return {message_detail:message, status:200}
      end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      Emis::Client.log_request_data(current_user,member_id,request_data,false,options)
      return {error:response[:response_information][:response_detail][:message], status:100}
    end
  end

   # Emis::Message.delete_message(message_id, organization_uid,member_id,options)
  def self.delete_message(message_id,organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      user_clinic_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
      
      session_id,client_sequence_hash = ::Emis::Client.get_session_detail(member_id,options)
      return true if client_sequence_hash.blank?
      
      @message_guid =  SecureRandom.uuid
      @conversation_guid = @message_guid
      request_data = {:action=> "delete_message",:message_id=>@message_guid,:patient_id=>nil}    
      request_data[:sequence_number] = user_clinic_session.sequence_number rescue nil
          
      msg = {"DeleteMessageRequest"=>
        {
          "PatientSessionRequestHeader"=>{
            "ns1:ApplicationId" => APP_CONFIG["emis_application_id"], 
            "ns1:SessionId" => session_id,
            "ns1:ClientSequenceNumberHash"=> client_sequence_hash,
            "ns1:ConversationInformation"=>{
              "ns1:MessageGuid"=> @message_guid,
              "ns1:ConversationGuid"=>@conversation_guid
            } 
          } ,
          "ns:MessageID"=>message_id
        }
      }
      @client =Emis::Client.new_client(Emis::Client.service_wsdl_document, Emis::Client.service_namespaces)
      response = @client.call(:delete_message, message: msg)
      response = response.body[:delete_message_response][:delete_message_result]
      data = []
      if (response[:patient_session_response_header][:session_valid] == false rescue true)
        raise 'invalid session' if @retry_count.to_i <= 2
        Emis::Client.log_request_data(current_user,member_id,request_data,false,options)
        return {error:response[:response_information][:response_detail][:message], status:100}

      elsif (response[:response_information][:success] == false rescue true)
        Emis::Client.log_request_data(current_user,member_id,request_data,false,options)
        return {error:response[:response_information][:response_detail][:message], status:100}
      else
        Emis::Client.log_request_data(current_user,member_id,request_data,true,options)
        message = response[:messages]
        @retry_count = 0
        return {:message=>"Message deleted successfully", status:200}
      end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      Emis::Client.log_request_data(current_user,member_id,request_data,false,options)
      return {error:response[:response_information][:response_detail][:message], status:100}
    end
  end

  
  def self.send_message(subject,msg,organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      # ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      session_id,client_sequence_hash = ::Emis::Client.get_session_detail(member_id,options)
      user_clinic_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
      return true if client_sequence_hash.blank?
      
      @message_guid =  SecureRandom.uuid
      @conversation_guid = @message_guid
      request_data = {:action=> "send_message",:message_id=>@message_guid,:patient_id=>nil}    
      request_data[:sequence_number] = user_clinic_session.sequence_number rescue nil
          
      msg = {"SendMessageRequest"=>
        {
          "PatientSessionRequestHeader"=>{
            "ns1:ApplicationId" => APP_CONFIG["emis_application_id"], 
            "ns1:SessionId" => session_id,
            "ns1:ClientSequenceNumberHash"=> client_sequence_hash,
            "ns1:ConversationInformation"=>{
              "ns1:MessageGuid"=> @message_guid,
              "ns1:ConversationGuid"=>@conversation_guid
            } 
          } ,
          "ns:Subject"=>subject,
          "ns:MessageBody"=>msg
        }
      }
      @client =Emis::Client.new_client(Emis::Client.service_wsdl_document, Emis::Client.service_namespaces)
      response = @client.call(:send_message, message: msg)
      response = response.body[:send_message_response][:send_message_result]
      data = []
      if (response[:patient_session_response_header][:session_valid] == false rescue true)
        raise 'invalid session' if @retry_count.to_i <= 2
        Emis::Client.log_request_data(current_user,member_id,request_data,false,options)
        return {error:response[:response_information][:response_detail][:message], status:100}

      elsif (response[:response_information][:success] == false rescue true)
        Emis::Client.log_request_data(current_user,member_id,request_data,false,options)
        return {error:response[:response_information][:response_detail][:message], status:100}

      else
        Emis::Client.log_request_data(current_user,member_id,request_data,true,options)
        @retry_count = 0
        return {:message=>"Message sent successfully", status:200}
      end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      Emis::Client.log_request_data(current_user,member_id,request_data,false,options)
      return {error:response[:response_information][:response_detail][:message], status:100}
    end
  end

end