class Emis_1_0_0_5::Client

  def self.new_client(wsdl,namespaces)
    client = Savon.client(wsdl: wsdl, env_namespace: :soap,namespaces: namespaces, use_wsa_headers: true, soap_version: 2,log:true,element_form_default: :qualified,pretty_print_xml:true,namespace_identifier: :ns,logger: GpsocLogger)
  end


  def self.service_wsdl_document
     wsdl_document = APP_CONFIG["emis_service_wsdl"]
  end

  def self.service_namespaces
  {
    "xmlns:soap"=> "http://www.w3.org/2003/05/soap-envelope" ,
    "xmlns:ns1"=>"http://patientservices.e-mis.co.uk/commonmodel/20150224",
    "xmlns:ns"=>"http://patientservices.e-mis.co.uk/model/20160229"
  }
end

  def self.log_request_data(current_user,member_id,request_data,status,options={})
    current_user = current_user || options[:current_user]
    user_clinic_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
    sequence_number = user_clinic_session.sequence_number rescue nil
    request_data[:sequence_number] = request_data[:sequence_number] || sequence_number
    request_data[:request_status] = status 
    begin
      initial_sequence_no = sequence_number.to_i + 2
      user_clinic_session.update_attributes({sequence_number:initial_sequence_no})
    rescue Exception=>e 
      Rails.logger.info e.message
    end
    current_user.log_emis_request(member_id,request_data,options)
  end

  def self.get_session_detail(member_id,options)
    begin
      user_clinic_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
      if user_clinic_session.blank?
        begin
          user_details = ::Clinic::LinkDetail.where(member_id:member_id).last.user_info.with_indifferent_access
        rescue Exception=>e 
          raise "User's clinic link details missing"
        end
        #user_details = user_details["data"] || user_details
        current_user = options[:current_user] 
        user_details[:current_user] = current_user
        emis_session = ::Emis::Login.new(member_id,user_details)
        emis_session.authenticateSessionKey(current_user)
        user_clinic_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
        puts "Inside get seession"
      end
      sequence_number = user_clinic_session.sequence_number
      session_key1 = user_clinic_session.session_key
      client_sequence_hash1 = Pbk.session_hash(session_key1,sequence_number)
      session_id1 = user_clinic_session.session_id
    rescue Exception=>e 
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside get_session_detail")
      session_id1 = nil
      client_sequence_hash1 = nil
    end
      
    [session_id1,client_sequence_hash1]
  end


  def self.make_request(current_user,member_id,options={})
    begin
      data = {}
      status = 200
      clinic_link_detail = ::Clinic::LinkDetail.where(member_id:member_id).last
      link_info = clinic_link_detail.link_info rescue nil
      begin
        user_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
        if user_session.blank?
          end_user_session_id = ::Emis::Login.end_user_session(current_user,member_id,options)
        else
          end_user_session_id = user_session.session_key
          raise 'No Appliation session id' if end_user_session_id.blank?
        end
      rescue Exception => e 
        ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
        retry
      end
      
      header = { 'X-API-EndUserSessionId'=> end_user_session_id, 'Content-Type'=> 'application/json','X-API-Version'=>'2.0.0.0','X-API-ApplicationId'=>APP_CONFIG["emis_application_id"]}
      header.merge!(options[:request_header]) if options[:request_header].present?
      uri = URI.parse("http://185.13.72.92/pfs" + options[:request_uri].to_s)

      http = Net::HTTP.new(uri.host, uri.port)
      if uri.scheme == 'https'
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_PEER
      end
      if options[:request_type] == "Get"
        request = Net::HTTP::Get.new(uri.request_uri, header)
      else 
        request = Net::HTTP::Post.new(uri.request_uri, header)
      end
      request_body = options[:request_body]
      request.body = request_body.to_json if request_body.present?
      response = http.request(request)
      if response.code.to_i >= 200 && response.code.to_i < 300 
        data =  JSON.parse(response.body)
        status = 200
      else
        data =  JSON.parse(response.body)
        status = response.code
      end
    rescue Exception=>e 
      status = 100
      
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside emis make_request : #{options[:request_uri]} ")
    end
      # log event 
    # request_data = {:action=> action, :patient_id=>pinDocument["account_id"],:sequence_number=>nil}    
    # request_data[:request_status] = result[:response_information][:success] 
    # current_user.log_emis_request(member_id,request_data,options)

    [data,status]
  end

end