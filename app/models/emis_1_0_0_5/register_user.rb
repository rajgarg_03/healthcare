class Emis_1_0_0_5::RegisterUser
  # Api version 1.0.0.5
  # user_details = {"practice_ods_code"=>"A28826", "surname"=>"KUSHWAHA", "date_of_birth"=>"1993-04-08", "gender"=>"M", "house_name_no"=>"9", "postcode"=>"BR5 1QB", "email"=>"ronit1.garg@gmail.com", "mobile_no"=>""}
  # options = {member_id:"",:current_user=>""}
  def self.register_with_patient_detail1(user_details,options={})
    pinDocument = user_details.with_indifferent_access
    current_user = options[:current_user]
    member_id = options[:member_id]
    pinDocument[:date_of_birth] = pinDocument[:date_of_birth].to_date.strftime("%Y-%m-%d") rescue pinDocument[:date_of_birth]
    @application_id = APP_CONFIG["emis_application_id"]
    wsdl_document = APP_CONFIG["emis_service_wsdl"]
    namespaces = {
      "xmlns:soap"=> "http://www.w3.org/2003/05/soap-envelope" ,
      "xmlns:ns1"=>APP_CONFIG["emis_patient_wsdl_namespace"],
      "xmlns:ns"=>APP_CONFIG["emis_patient_wsdl_namespace1"]
    }
    action = :register_with_patient_details
    @client = Savon.client(wsdl: wsdl_document, env_namespace: :soap, use_wsa_headers: true, soap_version: 2,log:true,namespaces: namespaces,element_form_default: :qualified,pretty_print_xml:true,namespace_identifier: :ns,logger: GpsocLogger)
    message_uid = SecureRandom.uuid
    msg = {
            "RegisterWithPatientDetails"=>
            {
              "NonPatientContextRequestHeader"=>{
              "ns1:ApplicationId" => @application_id,
              "ns1:ConversationInformation"=>{ 
                "ns1:MessageGuid" => message_uid,
                "ns1:ConversationGuid"=>message_uid
              }
            },
          "PracticeODSCode"=>pinDocument["practice_ods_code"],
          "Surname"=>pinDocument["surname"],
          "DateOfBirth"=> pinDocument["date_of_birth"],
          "Gender"=>pinDocument["gender"],
          "HouseNameNumber"=>pinDocument["house_name_no"],
          "Postcode"=>pinDocument["postcode"]
          
        }
      }
    
    response = @client.call(action, message: msg)
    result =  response.body[:register_with_patient_details_response][:register_with_patient_details_result]
    
    request_data = {:action=> action, :message_id=>message_uid,:patient_id=>nil,:sequence_number=>nil}    
    request_data[:request_status] = result[:response_information][:success]
    current_user.log_emis_request(member_id,request_data,options)
    
    if result[:response_information][:success] == false
      {:message=>result[:response_information][:response_detail][:message],:status=>100}.with_indifferent_access
    else
       data = {:patient_relationship_id=>result[:patient_relationship_id],:patient_relationship_key=>result[:patient_relationship_key]}
      {:data=>data,:message=>nil,:status=>200}.with_indifferent_access
    end 
  end

  # Api version 1.0.0.5
  # pinDocument = {"account_id"=>"2325028826","account_linkage_key"=>"TcdbmCgdUANr7","practice_ods_code"=>"A28826","surname"=>"","date_of_birth"=>"1993-04-08"}
  def self.register_with_pin_document1(pinDocument,options={})
    return {:error=>'Invalid pin document',:status=>100} if pinDocument.blank?
    member_id = options[:member_id]
    current_user = options[:current_user]
    pinDocument = pinDocument.with_indifferent_access
    pinDocument[:date_of_birth] = pinDocument[:date_of_birth].to_date.strftime("%Y-%m-%d") rescue pinDocument[:date_of_birth]
    @application_id = APP_CONFIG["emis_application_id"]
    wsdl_document = APP_CONFIG["emis_service_wsdl"]
    namespaces = {
      "xmlns:soap"=> "http://www.w3.org/2003/05/soap-envelope" ,
      "xmlns:ns1"=>APP_CONFIG["emis_patient_wsdl_namespace"],
      "xmlns:ns"=>APP_CONFIG["emis_patient_wsdl_namespace1"]
    }
    action = :register_with_pin_document
    @client = Savon.client(wsdl: wsdl_document, env_namespace: :soap, use_wsa_headers: true, soap_version: 2,log:true,namespaces: namespaces,element_form_default: :qualified,pretty_print_xml:true,namespace_identifier: :ns,logger: GpsocLogger)
    message_uid = SecureRandom.uuid
    
    msg = {"RegisterWithPinDocumentRequest"=>
      {
        "NonPatientContextRequestHeader"=>{
          "ns1:ApplicationId" => @application_id,
          "ns1:ConversationInformation"=>{ 
            "ns1:MessageGuid" => message_uid,
            "ns1:ConversationGuid"=>message_uid
          }
        },
        "PinDocument"=>{
          "AccountID"=>pinDocument["account_id"],
          "AccountLinkageKey"=>pinDocument["account_linkage_key"],
          "PracticeODSCode"=>pinDocument["practice_ods_code"],
          
        },
        "Surname"=>pinDocument["surname"],
        "DateOfBirth"=> pinDocument["date_of_birth"]
      }
    }
    response = @client.call(action, message: msg)
    result = response.body[:register_with_pin_document_response][:register_with_pin_document_result]
    # log event 
    request_data = {:action=> action,:message_id=>message_uid,:patient_id=>pinDocument["account_id"],:sequence_number=>nil}    
    request_data[:request_status] = result[:response_information][:success] 
    current_user.log_emis_request(member_id,request_data,options)

    if result[:response_information][:success] == false
       # data = {:patient_relationship_key=>'dQXvbEG/sUb/HzGbc8XuNSyAsdMplr8Cc/jR6SJCr/xec=',:patient_relationship_id=>'300d2fa2-6a71-4785-b78b-d5dda94fa779'}
      # {:data=>data,:error=>nil,:status=>200}.with_indifferent_access
      
    
      {:message=>result[:response_information][:response_detail][:message],:status=>100}.with_indifferent_access
    else
       data = {:patient_relationship_id=>result[:patient_relationship_id],:patient_relationship_key=>result[:patient_relationship_key]}
      {:data=>data,:message=>nil,:status=>200}.with_indifferent_access
    end 
  end



  def self.get_proxyUser(organization_uid,member_id,options={})
    begin
      # request_data = {}
      current_user = options[:current_user]
      # # user_clinic_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
      data = []
      member_ids = FamilyMember.where(:family_id.in=>current_user.user_family_ids).map(&:member_id).map(&:to_s)
      member_ids = member_ids - [member_id.to_s]
      # proxy_users = Member.where(:id.in=>member_ids,:clinic_link_state.gte=>2) 
      # current_user = Member.where(email:"ronit.garg@gmail.com").last
      proxy_users = Member.where(:id.in=>member_ids,:clinic_link_state.gte=>0) 
      proxy_users.each do |member|
        data << {:full_name=> "#{member.first_name} #{member.last_name}",
                 :dob=> (member.birth_date.to_date1 rescue nil),
                 :age=> member.member_age.to_s,
                 :gender => "Male"
                }
      end
      data = nil
    rescue Exception=>e 
      puts e.message
      Rails.logger.info e.message
    end
    data
  end

 
  
  # registeration_detail = {:title=>'Mr',:first_name=>"HHHH",:surname=>'Garg',:date_of_birth=> "1991-01-01", :gender=>"Male", :house_name_no=>"10 Rillington Place",:street=>"Smart Street",:town=>"ontario", :postcode=>"BR5 1QB", :practice_ods_code=>"A28826", :telephone_number=>"",:mobile_number=>"", :email=>"hh@ff.com", :village=>"",:country=>"" }.with_indifferent_access
  def self.register_with_patient_detail(registeration_detail, options={})
    begin
      pinDocument = registeration_detail.with_indifferent_access
      current_user = options[:current_user]
      member_id = options[:member_id]
      pinDocument[:date_of_birth] = pinDocument[:date_of_birth].to_date.strftime("%Y-%m-%d") rescue pinDocument[:date_of_birth]

      # registeration_detail = {:sex=>"NotKnown", :postcode=>"", :title=>"Mr", :surname=>"Garg", :first_name=>"Raj",  :date_of_birth=> "1991-01-01", :village=>"", :practice_ods_code=>"A28826", :telephone_number=>"",:mobile_number=>"918826936913", :email=>"ronit.garg@gmail.com",:house_name_no=>"10 Rillington Place",:street=>"Smart Street", :village=>"" }.with_indifferent_access
      request_body = 
      {
        "NationalPracticeCode": pinDocument["practice_ods_code"],
        "RegistrationDetails": {
          "ContactDetails": {
            "TelephoneNumber": pinDocument["telephone_number"],
            "MobileNumber": pinDocument["mobile_number"],
            "EmailAddress": pinDocument["email"]
          },
          "Address": {
            "HouseNameFlatNumber": pinDocument["house_name_no"],
            "NumberStreet": pinDocument["street"],
            "Village": pinDocument["village"],
            "Town": pinDocument["town"],
            "County": pinDocument["country"],
            "Postcode": pinDocument["postcode"]
          }
        },
        "SearchCriteria": {
          "AssociationType": "None",
          "DateOfBirth": pinDocument["date_of_birth"].to_time,
          "FirstName": pinDocument["first_name"],
          "Postcode": pinDocument["postcode"],
          "Sex": pinDocument["gender"],
          "Surname": pinDocument["surname"],
          "Title": pinDocument["title"]
        }
      }
      options = {:request_type=>"Post", :request_uri=> "/users", :request_body=>request_body}

          
      response,status = ::Emis::Client.make_request(current_user,member_id,options)
      if status == 200
        response_data = {:access_identity_guid=>response["AccessIdentityGuid"]}
        {:data=>response_data,:message=>nil,:status=>200}.with_indifferent_access

      else
        data = {:message=>result[:response_information][:response_detail][:message],:status=>100}.with_indifferent_access
      end
    rescue Exception=>e 
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside get_session_id")
    end
  end
  
  # pinDocument = {"account_id"=>"2689028826","account_linkage_key"=>"tJ32RCgdUAAm1","practice_ods_code"=>"A28826","surname"=>"Garg","date_of_birth"=>"1995-11-30"}
  def self.register_with_pin_document(pinDocument,options={})
    begin
      return {:error=>'Invalid pin document',:status=>100} if pinDocument.blank?
      member_id = options[:member_id]
      current_user = options[:current_user]
      pinDocument = pinDocument.with_indifferent_access
      pinDocument[:date_of_birth] = pinDocument[:date_of_birth].to_date.strftime("%Y-%m-%d") rescue pinDocument[:date_of_birth]
      request_body = {
        "Surname": pinDocument[:surname],
        "DateOfBirth": pinDocument[:date_of_birth].to_time,
        "LinkageDetails": {
          "AccountId":pinDocument[:account_id],
          "LinkageKey": pinDocument[:account_linkage_key],
          "NationalPracticeCode": pinDocument[:practice_ods_code]
        }
      }
      options.merge!({:request_type=>"Post", :request_uri=> "/me/applications", :request_body=>request_body})
      # response,status = ::Emis::Client.make_request(current_user,member_id,options)
      # Mock for testing
      response,status = [{"AccessIdentityGuid"=>"5358df2b-4659-48ea-9630-99303dc54ebe"}, 200] 
      if status == 200
        data = {:data=> {:access_identity_guid=>response["AccessIdentityGuid"]},:status=>200}
        # data = {:data=>data,:message=>nil,:status=>200}.with_indifferent_access
        # {:data=>data,:error=>nil,:status=>200}.with_indifferent_access
      else
        data = {:message=>response["Message"],:status=>100}.with_indifferent_access
      end
    rescue Exception =>e 
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside emis make_request : #{options[:request_uri]} ")
      data  = {:message=>e.message,:status=>100}.with_indifferent_access
    end
    data
  end
    
end