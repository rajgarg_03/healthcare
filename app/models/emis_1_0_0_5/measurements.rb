class Emis_1_0_0_5::Measurements
#   Get Patient Measurements
#   Description – This method can be used to get the patient details for the given user. Patient details
#                 will be given as json string. This will also have the measurements for each visit.
  
  def self.wsdl_document
    @wsdl_document = APP_CONFIG["emis_service_wsdl"]
  end
  
   
   def self.namespaces
   {
      "xmlns:soap"=> "http://www.w3.org/2003/05/soap-envelope" ,
      "xmlns:ns1"=>"http://patientservices.e-mis.co.uk/commonmodel/20150224",
      "xmlns:ns"=>"http://patientservices.e-mis.co.uk/model/20160229"
    }
  end
  
  def self.get_patient_measurements(organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      
      user_clinic_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
      
      session_id,client_sequence_hash = ::Emis::BookAppointment.get_session_detail(member_id,options)
      return true if client_sequence_hash.blank?
      
      @message_guid =  SecureRandom.uuid
      @conversation_guid = @message_guid
      request_data = {:action=> "get_medical_record_data",:message_id=>@message_guid,:patient_id=>nil}    
      request_data[:sequence_number] = user_clinic_session.sequence_number rescue nil
          
      msg = {"GetMedicalRecordDataRequest"=>
        {
          "PatientSessionRequestHeader"=>{
            "ns1:ApplicationId" => APP_CONFIG["emis_application_id"], 
            "ns1:SessionId" => session_id,
            "ns1:ClientSequenceNumberHash"=> client_sequence_hash,
            "ns1:ConversationInformation"=>{
              "ns1:MessageGuid"=> @message_guid,
              "ns1:ConversationGuid"=>@conversation_guid
            } 
          } ,
          "ns:ContentType"=>"Investigations"
        }
      }
      @client =Emis::Client.new_client(Emis::Measurements.wsdl_document, Emis::Measurements.namespaces)
      response = @client.call(:get_medical_record_data, message: msg)
      response = response.body[:get_medical_record_data_response][:get_medical_record_data_result]
      if (response[:patient_session_response_header][:session_valid] == false rescue true)
        raise 'invalid session' if @retry_count.to_i <= 2
      elsif (response[:response_information][:success] == false rescue true)
        Emis::BookAppointment.log_request_data(current_user,member_id,request_data,false,options)
      else
        Emis::BookAppointment.log_request_data(current_user,member_id,request_data,true,options)
        measurement_records = Emis::Measurements.get_formatted_measurement_data(member_id,response[:patient_record][:investigations],options)
        Health.where(member_id:member_id,:data_source=>"gpsoc").delete_all
        measurement_records.each do |updated_at,data|
          begin
            data[:health].delete(nil)
            data.delete(nil)
            data = data.with_indifferent_access
            data[:health] = data[:health].with_indifferent_access
            if data[:health].present?
              Health.create_measurement_record(current_user,data)
            end
          rescue Exception =>e 
            Rails.logger.info e.message
            puts e.message
          end
        end
        @retry_count = 0
      end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      Emis::BookAppointment.log_request_data(current_user,member_id,request_data,false,options)
    end
  end


   def self.get_formatted_measurement_data(member_id,records,options={})
    measurement_unit = {"height"=>"length_unit","weight"=>"weight_unit"}
    health_data = {}
    # group_by_date = records.group_by{|a| a[:value][:availability_date_time].to_s}
    group_by_date = records.group_by{|a| a[:value][:effective_date][:Value].to_s}

    group_by_date.each do |date,health_records|
      measurement_data = {:health=>{}}
      health_records.each do |record|
        data = record[:value]
        record_date = date.to_date.to_s
        measurement_parameter = Emis::Measurements.get_measurement_parameter(data[:term])
        if measurement_parameter.present?
          metric_unit = measurement_unit[measurement_parameter]
          measurement_data[metric_unit]= (data[:numeric_units].pluralize rescue nil )
          measurement_data[:health].merge!({:member_id=>member_id,measurement_parameter=> data[:numeric_value],:data_source=>"gpsoc",:added_by=>"system",:updated_at=>record_date })
        end
      end
      health_data[date] = measurement_data
    end
    health_data
  end



   

  def get_measurement_parameter(term)
    if term.include?("height")
      "height"
    elsif term.include?("weight")
        "weight"
    end
  end

  def self.get_measurement_parameter(term)
    if term.include?("height")
      "height"
    elsif term.include?("weight")
        "weight"
    end
  end
  def self.get_patient_measurements1(organization_uid,member_id,options={})
    retry_count = 0
    begin
      data = []
      current_user = options[:current_user]
      api_version = options[:api_version]
    rescue Exception=>e
      Rails.logger.info "Error inside get_patient_measurements-- #{e.message}"
      puts "Error inside get_patient_measurements-- #{e.message}"
    end
    data
  end
  
end
 