class Emis_1_0_0_5::Prescribing
  # Emis::Prescribing.get_medication_courses(organization_uid,member_id,options)
  def self.get_medication_courses(organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      # ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      session_id,client_sequence_hash = ::Emis::Client.get_session_destail(member_id,options)
      user_clinic_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
      return true if client_sequence_hash.blank?
      
      @message_guid =  SecureRandom.uuid
      @conversation_guid = @message_guid
      request_data = {:action=> "get_medication_courses",:message_id=>@message_guid,:patient_id=>nil}    
      request_data[:sequence_number] = user_clinic_session.sequence_number rescue nil
          
      msg = {"GetMedicationCoursesRequest"=>
        {
          "PatientSessionRequestHeader"=>{
            "ns1:ApplicationId" => APP_CONFIG["emis_application_id"], 
            "ns1:SessionId" => session_id,
            "ns1:ClientSequenceNumberHash"=> client_sequence_hash,
            "ns1:ConversationInformation"=>{
              "ns1:MessageGuid"=> @message_guid,
              "ns1:ConversationGuid"=>@conversation_guid
            } 
          }  
        }
      }
      @client =Emis::Client.new_client(Emis::Client.service_wsdl_document, Emis::Client.service_namespaces)
      response = @client.call(:get_medication_courses, message: msg)
      response = response.body[:get_medication_courses_response][:get_medication_courses_result]
      data = []
      if (response[:patient_session_response_header][:session_valid] == false rescue true)
        raise 'invalid session' if @retry_count.to_i <= 2
         Emis::Client.log_request_data(current_user,member_id,request_data,false,options)
        return {error:response[:response_information][:response_detail][:message], status:100}
    
      elsif (response[:response_information][:success] == false rescue true)
        Emis::Client.log_request_data(current_user,member_id,request_data,false,options)
        return {error:response[:response_information][:response_detail][:message], status:100}
      else
        Emis::Client.log_request_data(current_user,member_id,request_data,true,options)
        medication_courses = response[:medication_courses][:medication_course]
        return {status:200,medication_courses:medication_courses}
        @retry_count = 0
      end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      Emis::Client.log_request_data(current_user,member_id,request_data,false,options)
      return {error:response[:response_information][:response_detail][:message], status:100}
    end
  end
  # medication_course_ids = ["b2ab7f85-b1ef-4ec3-9ecc-33eb690482e1","9ce0766e-b51f-4f68-8426-841e178af08c"]
  #Emis::Prescribing.request_prescription(medication_course_ids,organization_uid,member_id,options)
  def self.request_prescription(medication_course_ids,organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      session_id,client_sequence_hash = ::Emis::Client.get_session_detail(member_id,options)
      user_clinic_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
      
      return true if client_sequence_hash.blank?
      
      @message_guid =  SecureRandom.uuid
      @conversation_guid = @message_guid
      request_data = {:action=> "request_prescription",:message_id=>@message_guid,:patient_id=>nil}    
      request_data[:sequence_number] = user_clinic_session.sequence_number rescue nil
      msg = {"RequestPrescriptionRequest"=>
        {
          "PatientSessionRequestHeader"=>{
            "ns1:ApplicationId" => APP_CONFIG["emis_application_id"], 
            "ns1:SessionId" => session_id,
            "ns1:ClientSequenceNumberHash"=> client_sequence_hash,
            "ns1:ConversationInformation"=>{
              "ns1:MessageGuid"=> @message_guid,
              "ns1:ConversationGuid"=>@conversation_guid
            } 
          } ,
          "MedicationCourseIDs"=>{"MedicationCourseID"=>medication_course_ids } 
        }
      }
      @client =Emis::Client.new_client(Emis::Client.service_wsdl_document, Emis::Client.service_namespaces)
      response = @client.call(:request_prescription, message: msg)
      response = response.body[:request_prescription_response][:request_prescription_result]
      data = []
      if (response[:patient_session_response_header][:session_valid] == false rescue true)
        raise 'invalid session' if @retry_count.to_i <= 2
        return {error:response[:response_information][:response_detail][:message], status:100}
      elsif (response[:response_information][:success] == false rescue true)
        Emis::Client.log_request_data(current_user,member_id,request_data,false,options)
        return {error:response[:response_information][:response_detail][:message], status:100}
      else
        Emis::Client.log_request_data(current_user,member_id,request_data,true,options)
        # message = response[:messages]
        return {status:200}
        @retry_count = 0
      end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      Emis::Client.log_request_data(current_user,member_id,request_data,false,options)
      return {error:response[:response_information][:response_detail][:message], status:100}
    end
  end

  # Emis::Prescribing.get_prescription_requests((Date.today - 1.month),organization_uid,member_id,options)
  def self.get_prescription_requests(from_date, organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      
      # ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      session_id,client_sequence_hash = ::Emis::Client.get_session_detail(member_id,options)
      user_clinic_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
      
      return true if client_sequence_hash.blank?
      
      @message_guid =  SecureRandom.uuid
      @conversation_guid = @message_guid
      request_data = {:action=> "get_prescription_requests",:message_id=>@message_guid,:patient_id=>nil}    
      request_data[:sequence_number] = user_clinic_session.sequence_number rescue nil
          
      msg = {"GetPrescriptionRequestsRequest"=>
        {
          "PatientSessionRequestHeader"=>{
            "ns1:ApplicationId" => APP_CONFIG["emis_application_id"], 
            "ns1:SessionId" => session_id,
            "ns1:ClientSequenceNumberHash"=> client_sequence_hash,
            "ns1:ConversationInformation"=>{
              "ns1:MessageGuid"=> @message_guid,
              "ns1:ConversationGuid"=>@conversation_guid
            } 
          } ,
          "ns:FromDate"=>(from_date.to_date)
        }
      }
      @client =Emis::Client.new_client(Emis::Client.service_wsdl_document, Emis::Client.service_namespaces)
      response = @client.call(:get_prescription_requests, message: msg)
      response = response.body[:get_prescription_requests_response][:get_prescription_requests_result]
      data = []

      
      if (response[:patient_session_response_header][:session_valid] == false rescue true)
        raise 'invalid session' if @retry_count.to_i <= 2
      elsif (response[:response_information][:success] == false rescue true)
        Emis::Client.log_request_data(current_user,member_id,request_data,false,options)
      else
        Emis::Client.log_request_data(current_user,member_id,request_data,true,options)
        medication_course_list =  response[:medication_courses][:medication_course].group_by{|a| a[:medication_course_id] }
        prescription_list = response[:prescription_requests][:prescription_request]
        # {:date_requested=>Sat, 23 Feb 2019 10:06:00 +0000, :requested_medication_courses=>{:requested_medication_course=>{:medication_course_id=>"b2ab7f85-b1ef-4ec3-9ecc-33eb690482e1", :status=>"Rejected"}}, :patient_comment=>"Lost my medication , need replacement urgently", :clinician_comment=>nil, :requested_by=>nil, :request_id=>"5c450d8f-2a2a-4c62-a12d-71d7e6962eca"}
        data = []
        prescription_list.each do |prescription|
          requested_medication_courses = {}
          requested_medication_courses = prescription[:requested_medication_courses][:requested_medication_course]
          puts  ".................................."
          puts requested_medication_courses.inspect
          requested_medication_courses_data = []
          [requested_medication_courses].flatten.each do |req_medi_course|
            medication_course_details =  medication_course_list[req_medi_course[:medication_course_id]][0] || {}
            requested_medication_courses_data << req_medi_course.merge(medication_course_details)
          end
          # requested_medication_courses.merge!(medication_course_details)
          prescription_data = prescription
          prescription_data[:date_requested] = prescription[:date_requested].to_date
          prescription_data[:requested_medication_courses] =  requested_medication_courses_data
          data << prescription_data 
        end
        @retry_count = 0
        return {requested_prescription_list:data, status:200}
      
      end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      Emis::Client.log_request_data(current_user,member_id,request_data,false,options)
      return {requested_prescription_list:data, status:100}
    end
  end


  # medication_course_ids = ["b2ab7f85-b1ef-4ec3-9ecc-33eb690482e1","9ce0766e-b51f-4f68-8426-841e178af08c"]
  #Emis::Prescribing.cancel_prescription(medication_course_ids,organization_uid,member_id,options)
  def self.cancel_prescription(request_id,organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      # ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      session_id,client_sequence_hash = ::Emis::Client.get_session_detail(member_id,options)
      user_clinic_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
      
      return true if client_sequence_hash.blank?
      
      @message_guid =  SecureRandom.uuid
      @conversation_guid = @message_guid
      request_data = {:action=> "request_prescription",:message_id=>@message_guid,:patient_id=>nil}    
      request_data[:sequence_number] = user_clinic_session.sequence_number rescue nil
      msg = {"CancelPrescriptionRequest"=>
        {
          "PatientSessionRequestHeader"=>{
            "ns1:ApplicationId" => APP_CONFIG["emis_application_id"], 
            "ns1:SessionId" => session_id,
            "ns1:ClientSequenceNumberHash"=> client_sequence_hash,
            "ns1:ConversationInformation"=>{
              "ns1:MessageGuid"=> @message_guid,
              "ns1:ConversationGuid"=>@conversation_guid
            } 
          } ,
          "RequestID"=>request_id
        }
      }
      @client =Emis::Client.new_client(Emis::Client.service_wsdl_document, Emis::Client.service_namespaces)
      response = @client.call(:cancel_prescription_request, message: msg)

      response = response.body[:cancel_prescription_request_response][:cancel_prescription_request_result]
      data = []
      if (response[:patient_session_response_header][:session_valid] == false rescue true)
        raise 'invalid session' if @retry_count.to_i <= 2
        return {error:response[:response_information][:response_detail][:message], status:100}
      elsif (response[:response_information][:success] == false rescue true)
        Emis::Client.log_request_data(current_user,member_id,request_data,false,options)
        return {error:response[:response_information][:response_detail][:message], status:100}
      else
        Emis::Client.log_request_data(current_user,member_id,request_data,true,options)
        
        return {status:200}
        @retry_count = 0
      end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      Emis::Client.log_request_data(current_user,member_id,request_data,false,options)
      return {error:response[:response_information][:response_detail][:message], status:100}
    end
  end

  
   

end