class Emis_1_0_0_5::Login
  include Savon
  attr_accessor :patient_relationship_key, :patient_relationship_id,:member_id,:application_id,:client_token, :server_token,:initial_sequence_no,:handshake_token,:session_hash,:session_id
  def initialize(member_id,user_details={})
    user_details = user_details.with_indifferent_access
    self.application_id = Emis::Login.application_id
    self.initial_sequence_no = nil
    self.server_token = nil
    self.session_id = nil 
    self.session_hash = nil
    self.client_token = nil
    self.member_id = member_id
    self.patient_relationship_key = user_details[:patient_relationship_key]
    self.patient_relationship_id = user_details[:patient_relationship_id]
     
    @wsdl_document = Emis::Login.wsdl_document
    @cdbNumber = Emis::Login.cdbNumber
    @server_validation_key = nil 
  end  

  def self.application_id
     APP_CONFIG["emis_application_id"]
  end

  def self.cdbNumber
     APP_CONFIG["emis_cdb_number"]
  end
  
  def self.wsdl_document
    @wsdl_document = APP_CONFIG["emis_session_wsdl"]
  end
  def self.namespaces
   {
      "xmlns:soap"=> "http://www.w3.org/2003/05/soap-envelope" ,
      "xmlns:ns1"=>"http://www.w3.org/2003/05/soap-envelope",
      "xmlns:ns"=>APP_CONFIG["emis_session_wsdl_namespace"]
    }
  end

  def get_session_key(user_details={})
    application_id
    wsdl_document
    patient_relationship_guid = user_details['patient_relationship_guid']
    handshake_token,server_token = handshakePatientPublicKeys(nil,user_details)
  end


  def createPublicKeyAgreement
    msg = {"CreatePublicKeyAgreementRequest"=>
      {
        "ApplicationId" => @application_id, 
        "NotDotNet" => true
      }
    }
    # @client = @client || Savon.client(wsdl: Emis::Login.wsdl_document, env_namespace: :soap, use_wsa_headers: true, soap_version: 2,log:true,namespaces: Emis::Login.namespaces,element_form_default: :qualified,pretty_print_xml:true,namespace_identifier: :ns)
    @client = @client || Emis::Client.new_client(Emis::Login.wsdl_document, Emis::Login.namespaces)
    response = @client.call(:create_public_key_agreement, message: msg)
    result = response.body[:create_public_key_agreement_response][:create_public_key_agreement_result]
    
    self.initial_sequence_no = result[:initial_sequence_number]
    
    self.handshake_token = result[:handshake_token]
  end
  
  def handshakePatientPublicKeys
    createPublicKeyAgreement
    client_token = SecureRandom.base64(32)
    patient_relationship_guid = self.patient_relationship_id
    msg = {"HandshakePatientPublicKeysRequest"=>
     {
        "HandshakeToken" => self.handshake_token, 
        "ClientToken" => client_token,
        "PatientRelationshipGuid" =>patient_relationship_guid,
        "ApplicationId" => @application_id, 
        "CdbNumber" => @cdbNumber,
      }
    }
    # @client = @client || Savon.client(wsdl: Emis::Login.wsdl_document, env_namespace: :soap, use_wsa_headers: true, soap_version: 2,log:true,namespaces: Emis::Login.namespaces,element_form_default: :qualified,pretty_print_xml:true,namespace_identifier: :ns)
    @client = @client || Emis::Client.new_client(Emis::Login.wsdl_document,Emis::Login.namespaces)
    response = @client.call(:handshake_patient_public_keys, message: msg)
    result = response.body[:handshake_patient_public_keys_response][:handshake_patient_public_keys_result]
    @server_token = result[:server_token]
    self.handshake_token = result[:handshake_token]
    self.client_token = client_token
    self.server_token = result[:server_token]
    self.initial_sequence_no = self.initial_sequence_no
     
    [result[:handshake_token], result[:server_token],client_token,self.initial_sequence_no]
  end

  def authenticateSessionKey(current_user=nil,options={})
    handshakePatientPublicKeys
    patient_relationship_key = self.patient_relationship_key
    session_key = Pbk.encrypt(self.patient_relationship_key,self.client_token,self.server_token)
    msg = {"AuthenticateSessionKeyRequest"=>
     {
        "HandshakeToken" => self.handshake_token, 
        "SessionKey" => session_key,
        "ApplicationId" => @application_id  
        
      }
    }
    @client =  Emis::Client.new_client(Emis::Login.wsdl_document,Emis::Login.namespaces)
    response = @client.call(:authenticate_session_key, message: msg)
    result = response.body[:authenticate_session_key_response][:authenticate_session_key_result]
    request_data = {:action=> "authenticate_session_key",:message_id=>nil,:patient_id=>nil,:sequence_number=>self.initial_sequence_no}    
    if result[:response_information].present? && result[:response_information][:success] == false
      request_data[:request_status] = false
      
    else
      self.session_id =  result[:session_id]
      self.session_hash = Pbk.session_hash(session_key,self.initial_sequence_no)
      @server_validation_key =   result[:server_validation_key]
      ::Clinic::LinkDetail.save_user_session_detail(self.member_id,session_key,session_id,{:sequence_number=>self.initial_sequence_no})
      # log event 
      request_data[:request_status] = true 
      if current_user.present?
        current_user.log_emis_request(self.member_id,request_data,options)
      end
    end
    
    result
end
  # Api 1.0.0.5
  def self.authenticateUser1(member_id,user_details,options={})
    options[:member_id] = member_id
    member = Member.find(member_id)
    current_user = options[:current_user]
    if options[:register_with_pindocument] == true
     # pinDocument = {"account_id"=>"2325028826","account_linkage_key"=>"TcdbmCgdUANr7","practice_ods_code"=>"A28826","surname"=>"","date_of_birth"=>"1993-04-08"}
      pin_document = user_details
      response = Emis::RegisterUser.register_with_pin_document(pin_document,options)
    else
      # user_details = {"practice_ods_code"=>"A28826", "surname"=>"KUSHWAHA", "date_of_birth"=>"1993-04-08", "gender"=>"M", "house_name_no"=>"9", "postcode"=>"BR5 1QB", "email"=>"ronit1.garg@gmail.com", "mobile_no"=>""}
      response = Emis::RegisterUser.register_with_patient_detail(user_details,options)
    end
    response = response.with_indifferent_access
    return response if response[:status] == 100
    is_child_member = member.user_id.blank?
    user_detail_data = {}
    if options[:is_proxy_selected] == true
     # data = {:patient_relationship_key=>'dQXvbEG/sUb/HzGbc8XuNSyAsdMplr8Cc/jR6SJCr/xec=',:patient_relationship_id=>'300d2fa2-6a71-4785-b78b-d5dda94fa779'}

      user_details = response[:data]
      user_detail_data =  Emis::Login.getPatienDetail(current_user,member_id,user_details,options)
    elsif member.is_child? && member.age_in_months < (13*12) # 13 years
      user_details = response[:data]
      user_detail_data =  Emis::Login.getPatienDetail(current_user,member_id,user_details,options)
    elsif !member.is_child? && member.age_in_months > (16*12)
      user_details = response[:data]
      user_detail_data = Emis::Login.getPatienDetail(current_user,member_id,user_details,options)
    else
      response =  {:message=>'User is not authorized to access records',:status=>100}.with_indifferent_access
    end    

    if user_detail_data.present? #&& options[:is_proxy_selected] != true
      if user_detail_data[:first_name].downcase != member.first_name.downcase
        response =  {:message=>'User is not authorized to access records',:status=>100}.with_indifferent_access
      end
    end
    response[:data].merge!(user_detail_data) rescue nil
    response
  end
  
  # Api 1.0.0.5
  def self.getPatienDetail1(current_user,member_id,user_details,options={})
    begin
      data = {}
      session_id,client_sequence_hash = Emis::Login.get_session_detail(current_user,member_id,user_details,options)
      user_clinic_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
      @message_guid =  SecureRandom.uuid
      @conversation_guid = @message_guid
      request_data = {:action=> "get_patient_details",:message_id=>@message_guid,:patient_id=>nil}    
      msg = {"GetPatientDetailsRequest"=>
        {
          "PatientSessionRequestHeader"=>{
            "ns1:ApplicationId" => APP_CONFIG["emis_application_id"], 
            "ns1:SessionId" => session_id,
            "ns1:ClientSequenceNumberHash"=> client_sequence_hash,
            "ns1:ConversationInformation"=>{
              "ns1:MessageGuid"=> @message_guid,
              "ns1:ConversationGuid"=>@conversation_guid
            } 
          }  
        }
      }
      @client =Emis::Client.new_client(Emis::Measurements.wsdl_document, Emis::Measurements.namespaces)
      response = @client.call(:get_patient_details, message: msg)
      response = response.body[:get_patient_details_response][:get_patient_details_result]
       # log event 
      request_data = {:action=> "get_patient_details",:message_id=>@message_guid,:patient_id=>nil}    
      request_data[:sequence_number] = user_clinic_session.sequence_number rescue nil
      
      if (response[:patient_session_response_header][:session_valid] == false rescue true)
        raise 'invalid session' if @retry_count.to_i <= 2
         @retry_count = 0
      end
      # {:title=>"Mr", :first_names=>"Ronit", :surname=>"Garg", :calling_name=>"Emis", :patient_identifiers=>{:value=>nil, :type=>"NHS Number"}, :date_of_birth=>Wed, 22 Nov 2000, :contact_details=>{:email_address=>nil, :mobile_number=>nil, :telephone_number=>nil, :address=>{:house_name_flat_number=>"123", :number_street=>"Dcf", :village=>nil, :town=>"Brq 4Hw", :county=>nil, :postcode=>nil}, :status=>"Current"}, :gender=>"M"}
      response_result = response[:patient_details]
      contact_details = response_result[:contact_details] ||{}
      data = {
        :first_name=>response_result[:first_names],
        :last_name=> response_result[:surname],
        :nhs_number => (response_result[:patient_identifiers][:value] rescue nil),
        :patient_identifier => (response_result[:patient_identifiers][:value] rescue nil),
        :date_of_birth=> response_result[:date_of_birth].to_date,
        :mobile_number => contact_details[:mobile_number],
        :email => contact_details[:email_address],
        :address=> contact_details[:address],
        :gender=> (response_result[:gender] == "M" ? "Male" : "Female")
      }
       
    rescue Exception=>e 
      GpsocLogger.info  "..............................................................."
      GpsocLogger.info  "error msg is :#{e.message}"
      puts ".............Retry count --  #{@retry_count.to_i}"
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1
      retry if @retry_count.to_i <= 2
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside  getPatienDetail")
      data = {}
    end
    @retry_count = 0
    data
  end

  

  
  # Api 1.0.0.5
  # user_details = {:patient_relationship_key=>'dQXvbEG/sUb/HzGbc8XuNSyAsdMplr8Cc/jR6SJCr/xec=',:patient_relationship_id=>'300d2fa2-6a71-4785-b78b-d5dda94fa779'}
  def self.getPatienDetail(current_user,member_id,user_details,options={})
    begin
      data = {}
      session_id,client_sequence_hash = Emis::Login.get_session_detail(current_user,member_id,user_details,options)
      user_clinic_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
      @message_guid =  SecureRandom.uuid
      @conversation_guid = @message_guid
      request_data = {:action=> "get_patient_details",:message_id=>@message_guid,:patient_id=>nil}    
      msg = {"GetPatientDetailsRequest"=>
        {
          "PatientSessionRequestHeader"=>{
            "ns1:ApplicationId" => APP_CONFIG["emis_application_id"], 
            "ns1:SessionId" => session_id,
            "ns1:ClientSequenceNumberHash"=> client_sequence_hash,
            "ns1:ConversationInformation"=>{
              "ns1:MessageGuid"=> @message_guid,
              "ns1:ConversationGuid"=>@conversation_guid
            } 
          }  
        }
      }
      @client =Emis::Client.new_client(Emis::Measurements.wsdl_document, Emis::Measurements.namespaces)
      response = @client.call(:get_patient_details, message: msg)
      response = response.body[:get_patient_details_response][:get_patient_details_result]
       # log event 
      request_data = {:action=> "get_patient_details",:message_id=>@message_guid,:patient_id=>nil}    
      request_data[:sequence_number] = user_clinic_session.sequence_number rescue nil
      
      if (response[:patient_session_response_header][:session_valid] == false rescue true)
        raise 'invalid session' if @retry_count.to_i <= 2
         @retry_count = 0
      end
      # {:title=>"Mr", :first_names=>"Ronit", :surname=>"Garg", :calling_name=>"Emis", :patient_identifiers=>{:value=>nil, :type=>"NHS Number"}, :date_of_birth=>Wed, 22 Nov 2000, :contact_details=>{:email_address=>nil, :mobile_number=>nil, :telephone_number=>nil, :address=>{:house_name_flat_number=>"123", :number_street=>"Dcf", :village=>nil, :town=>"Brq 4Hw", :county=>nil, :postcode=>nil}, :status=>"Current"}, :gender=>"M"}
      response_result = response[:patient_details]
      contact_details = response_result[:contact_details] ||{}
      data = {
        :first_name=>response_result[:first_names],
        :last_name=> response_result[:surname],
        :nhs_number => (response_result[:patient_identifiers][:value] rescue nil),
        :patient_identifier => (response_result[:patient_identifiers][:value] rescue nil),
        :date_of_birth=> response_result[:date_of_birth].to_date,
        :mobile_number => contact_details[:mobile_number],
        :email => contact_details[:email_address],
        :address=> contact_details[:address],
        :gender=> (response_result[:gender] == "M" ? "Male" : "Female")
      }
       
    rescue Exception=>e 
      GpsocLogger.info  "..............................................................."
      GpsocLogger.info  "error msg is :#{e.message}"
      puts ".............Retry count --  #{@retry_count.to_i}"
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1
      retry if @retry_count.to_i <= 2
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside  getPatienDetail")
      data = {}
    end
    @retry_count = 0
    data
  end

   
  def self.get_session_detail(current_user,member_id,user_details,options={})
    begin
      # user_details = {patient_relationship_key:option[:patient_relationship_key,}
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      # user_details[:current_user] = current_user
      emis_session = ::Emis::Login.new(member_id,user_details)
      emis_session.authenticateSessionKey(current_user)
      user_clinic_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
      puts "Inside get seession"
      sequence_number = user_clinic_session.sequence_number
      session_key1 = user_clinic_session.session_key
      client_sequence_hash1 = Pbk.session_hash(session_key1,sequence_number)
      session_id1 = user_clinic_session.session_id
    rescue Exception=>e 
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside login::get_session_detail")
      session_id1 = nil
      client_sequence_hash1 = nil
    end
    [session_id1,client_sequence_hash1]
  end        
  
  def get_user_patient_key(user_details)
  end


  def get_clinic_linkable_status(user_details,options={})
    true
  end

  # v2

  # Api 2.0.0.0
   def self.authenticateUser(member_id,user_details,options={})
    options[:member_id] = member_id
    member = Member.find(member_id)
    ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
    current_user = options[:current_user]
    if options[:register_with_pindocument] == true
     # pinDocument = {"account_id"=>"2325028826","account_linkage_key"=>"TcdbmCgdUANr7","practice_ods_code"=>"A28826","surname"=>"","date_of_birth"=>"1993-04-08"}
      pin_document = user_details.with_indifferent_access
      response = Emis::RegisterUser.register_with_pin_document(pin_document,options)
    else
      # user_details = {:title=>'Mr',:first_name=>"HHHH",:surname=>'Garg',:date_of_birth=> "1991-01-01", :gender=>"Male", :house_name_no=>"10 Rillington Place",:street=>"Smart Street",:town=>"ontario", :postcode=>"BR5 1QB", :practice_ods_code=>"A28826", :telephone_number=>"",:mobile_number=>"", :email=>"hh@ff.com", :village=>"",:country=>"" }.with_indifferent_access
     response = Emis::RegisterUser.register_with_patient_detail(user_details,options)
    end
    response = response.with_indifferent_access
    return response if response[:status] == 100
    is_child_member = member.user_id.blank?
    user_detail_data = {}
    if options[:is_proxy_selected] == true
     # data = {"access_identity_guid"=>"5358df2b-4659-48ea-9630-99303dc54ebe"}
     ods_code = pin_document[:practice_ods_code]
      user_details = response[:data]
      user_details[:practice_code] =  ods_code
     
      user_detail_data =  Emis::Login.getPatientInfo(current_user,member_id,user_details,options)
    elsif member.is_child? && member.age_in_months < (13*12) # 13 years
      user_details = response[:data]
      user_detail_data =  Emis::Login.getPatientInfo(current_user,member_id,user_details,options)
    elsif !member.is_child? && member.age_in_months > (16*12)
      user_details = response[:data]
      user_detail_data = Emis::Login.getPatientInfo(current_user,member_id,user_details,options)
    else
      response =  {:message=>'User is not authorized to access records',:status=>100}.with_indifferent_access
    end    

    if user_detail_data.present? #&& options[:is_proxy_selected] != true
      if user_detail_data[:first_name].downcase != member.first_name.downcase
        response =  {:message=>'User is not authorized to access records',:status=>100}.with_indifferent_access
      end
    end
    response[:data].merge!(user_detail_data) rescue nil
    response
  end

   # user_details = {:access_identity_guid=>'',:practice_code=>'A28826'}
  def self.getPatientInfo(current_user,member_id,user_details,options={})
    begin
      data = {}
      user_clinic_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
      if user_clinic_session.blank?
        ::Emis::Login.get_session_id(current_user,member_id,user_details,options)
        user_clinic_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
      end
      user_session_id = user_clinic_session.session_id
      member_id = options[:member_id]
      current_user = options[:current_user]
      user_details = user_details.with_indifferent_access
       
      options.merge!({:request_type=>"Get", :request_uri=> "/me", :request_header=>{"X-API-SessionId"=>user_session_id} })
      response,status = ::Emis::Client.make_request(current_user,member_id,options)
   
      # {:title=>"Mr", :first_names=>"Ronit", :surname=>"Garg", :calling_name=>"Emis", :patient_identifiers=>{:value=>nil, :type=>"NHS Number"}, :date_of_birth=>Wed, 22 Nov 2000, :contact_details=>{:email_address=>nil, :mobile_number=>nil, :telephone_number=>nil, :address=>{:house_name_flat_number=>"123", :number_street=>"Dcf", :village=>nil, :town=>"Brq 4Hw", :county=>nil, :postcode=>nil}, :status=>"Current"}, :gender=>"M"}
      address_detail = response["Address"]   
      contact_details = response["ContactDetails"] ||{}
      data = {
        :first_name=>response["FirstName"],
        :last_name=> response["Surname"],
        :nhs_number => ( nil),
        :patient_identifier => ( nil),
        :date_of_birth=> response["DateOfBirth"].to_date,
        :mobile_number => contact_details["MobileNumber"],
        :email => (contact_details["EmailAddress"] rescue nil),
        :address=> address_detail ,
        :gender=> (response["Sex"])
      }
    rescue Exception=>e 
      GpsocLogger.info  "..............................................................."
      GpsocLogger.info  "error msg is :#{e.message}"
      puts ".............Retry count --  #{@retry_count.to_i}"
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1
      retry if @retry_count.to_i <= 2
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside  getPatienDetail")
      data = {}
    end
    @retry_count = 0
    data
  end



  def self.get_session_id(current_user,member_id,user_details,options={})
    begin
      @retry_count = @retry_count.to_i || 0
      user_details = user_details.with_indifferent_access
      member_id = options[:member_id]
      current_user = options[:current_user]
      request_body = {
        "AccessIdentityGuid": user_details[:access_identity_guid],
        "NationalPracticeCode": user_details[:practice_code]
      }
      options.merge!({:request_type=>"Post", :request_uri=> "/sessions", :request_body=>request_body})
      response,status = ::Emis::Client.make_request(current_user,member_id,options)
      # response
      # {
      #   "SessionId": "string",
      #   "ApplicationLinkLevel": "Unknown",
      #   "FirstName": "string",
      #   "Surname": "string",
      #   "Title": "string",
      #   "LastAccessTimestamp": "2019-12-31T09:09:15.320Z",
      #   "UserPatientLinks": [
      #     {
      #       "UserPatientLinkToken": "string",
      #       "PatientActivityContextGuid": "string",
      #       "NationalPracticeCode": "string",
      #       "Title": "string",
      #       "Forenames": "string",
      #       "Surname": "string",
      #       "Age": 0,
      #       "AssociationType": "None"
      #     }
      #   ]
      # }
      if status == 200
        session_id = response["SessionId"]
        token = response["UserPatientLinks"][0]["UserPatientLinkToken"] rescue nil
        user_clinic_session =  ::Clinic::UserClinicSession.where(member_id:member_id).last
        user_clinic_session.session_id = session_id
        user_clinic_session.link_token = token
        user_clinic_session.save
        data = {:session_id=>session_id,:token=>token,  :user_info=>response, :status=>200}.with_indifferent_access
      else
        data = {:message=>response["Message"],:status=>100}.with_indifferent_access
      end
    rescue Exception =>e 
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside emis make_request : #{options[:request_uri]} ")
      @retry_count = @retry_count.to_i + 1
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      retry if @retry_count < 2 
      @retry_count = 0
      data  = {:message=>e.message,:status=>100}.with_indifferent_access
    end
    data
  end


  # Get application session ID
  def self.end_user_session(current_user,member_id,options={})
    begin
      app_session_id = nil
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      header = {'Content-Type'=> 'application/json','X-API-Version'=>'2.0.0.0','X-API-ApplicationId'=>APP_CONFIG["emis_application_id"]}
      uri = URI.parse("http://185.13.72.92/pfs/sessions/endusersession")
      http = Net::HTTP.new(uri.host, uri.port)
      if uri.scheme == 'https'
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_PEER
      end
      
      request = Net::HTTP::Post.new(uri.request_uri, header)
      response = http.request(request)
      if response.code == "201"
        app_session_id = JSON.parse(response.body)["EndUserSessionId"]
        user_clinic_session = ::Clinic::UserClinicSession.new( session_key:app_session_id,member_id:member_id)
        user_clinic_session.save
      end
    rescue Exception=>e 
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside emis login end_user_session")
    end
    app_session_id
  end

end

