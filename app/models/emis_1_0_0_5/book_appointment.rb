class Emis_1_0_0_5::BookAppointment
  def self.application_id
     APP_CONFIG["emis_application_id"]
  end
  
# Emis::BookAppointment.log_request_data(current_user,member_id,request_data,status,options)
  def self.log_request_data(current_user,member_id,request_data,status,options={})
    ::Emis::Client.log_request_data(current_user,member_id,request_data,status,options)
    # user_clinic_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
    # sequence_number = user_clinic_session.sequence_number rescue nil
    # request_data[:sequence_number] = request_data[:sequence_number] || sequence_number
    # request_data[:request_status] = status 
    # begin
    #   initial_sequence_no = sequence_number.to_i + 2
    #   user_clinic_session.update_attributes({sequence_number:initial_sequence_no})
    # rescue Exception=>e 
    #   Rails.logger.info e.message
    # end
    # current_user.log_emis_request(member_id,request_data,options)
  end

  def self.wsdl_document
    @wsdl_document = APP_CONFIG["emis_service_wsdl"]
  end
  def self.namespaces
   {
      "xmlns:soap"=> "http://www.w3.org/2003/05/soap-envelope" ,
      "xmlns:ns1"=>"http://patientservices.e-mis.co.uk/commonmodel/20150224",
      "xmlns:ns"=>"http://patientservices.e-mis.co.uk/model/20160229"
    }
  end
  
  def self.get_session_detail(member_id,options)
    ::Emis::Client.get_session_detail(member_id,options)
    # begin
    #   user_clinic_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
    #   if user_clinic_session.blank?
    #     begin
    #       user_details = ::Clinic::LinkDetail.where(member_id:member_id).last.user_info.with_indifferent_access
    #     rescue Exception=>e 
    #       raise "User's clinic link details missing"
    #     end
    #     #user_details = user_details["data"] || user_details
    #     current_user = options[:current_user] 
    #     user_details[:current_user] = current_user
    #     emis_session = ::Emis::Login.new(member_id,user_details)
    #     emis_session.authenticateSessionKey(current_user)
    #     user_clinic_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
    #     puts "Inside get seession"
    #   end
    #   sequence_number = user_clinic_session.sequence_number
    #   session_key1 = user_clinic_session.session_key
    #   client_sequence_hash1 = Pbk.session_hash(session_key1,sequence_number)
    #   session_id1 = user_clinic_session.session_id
    # rescue Exception=>e 
    #   UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside get_session_detail")
    #   session_id1 = nil
    #   client_sequence_hash1 = nil
    # end
      
    # [session_id1,client_sequence_hash1]
  end

  def self.get_available_appointments(organization_uid,member_id,options={})
    begin
      request_data = {}
      current_user = options[:current_user]
      session_id,client_sequence_hash = ::Emis::BookAppointment.get_session_detail(member_id,options)
      user_clinic_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
      if client_sequence_hash.blank?
        return [100,{message:"Unable to access account"}] 
      end
      # clinic_detail = ::Clinic::ClinicDetail.where(organization_uid:organization_uid).last
      ::Clinic::UserClinicSession.where(member_id:member_id).last 
      @message_guid =  SecureRandom.uuid
      @conversation_guid = SecureRandom.uuid
      msg = {"GetAvailableAppointmentsRequest"=>
        {
            "PatientSessionRequestHeader"=>{
            "ns1:ApplicationId" => APP_CONFIG["emis_application_id"], 
            "ns1:SessionId" => session_id,
            "ns1:ClientSequenceNumberHash"=> client_sequence_hash,
            "ns1:ConversationInformation"=>{
              "ns1:MessageGuid"=> @message_guid,
              "ns1:ConversationGuid"=>@conversation_guid
            }
          }
        }
      }
      # @client = Savon.client(wsdl: Emis::BookAppointment.wsdl_document, env_namespace: :soap, use_wsa_headers: true, soap_version: 2,log:true,namespaces: Emis::BookAppointment.namespaces,element_form_default: :qualified,pretty_print_xml:true,namespace_identifier: :ns)
      @client =  Emis::Client.new_client(Emis::BookAppointment.wsdl_document, Emis::BookAppointment.namespaces)
      response = @client.call(:get_available_appointments, message: msg)
      result = response.body[:get_available_appointments_response][:get_available_appointments_result]
      # log event 
      request_data = {:action=> "get_available_appointments",:message_id=>@message_guid,:patient_id=>nil}    
      request_data[:sequence_number] = user_clinic_session.sequence_number rescue nil
      if (result[:patient_session_response_header][:session_valid] == false rescue true)
        raise 'invalid session' if @retry_count.to_i <= 2
      elsif result[:response_information].present? && result[:response_information][:success] == false
        error_msg = result[:response_information][:response_detail][:message]
        @retry_count = 0
        Emis::BookAppointment.log_request_data(current_user,member_id,request_data,false,options)
        return [100,{message:error_msg}]
      end
      @retry_count = 0
    rescue Exception => e
      GpsocLogger.info  "..............................................................."
      GpsocLogger.info  "error msg is :#{e.message}"
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      GpsocLogger.info "Inside exception retry count:  #{@retry_count} .................." 
      @retry_count = @retry_count.to_i + 1
      retry if @retry_count.to_i <= 2
       @retry_count = 0
    end
    
    Emis::BookAppointment.log_request_data(current_user,member_id,request_data,true,options)
    
    doctor_list = []
    result[:clinicians][:clinician].each do |doctor_detail|
      name = (doctor_detail[:name].strip)
        specialization = doctor_detail[:type]
        doctor_list << doctor_detail.merge({"specialization"=>specialization})
      end

    slot_list = result[:appointment_sessions][:appointment_session]
    data = {}
    slot_list.each do |slots|
      clinicianId = slots[:clinician_ids][:clinician_id]
      slot_data = {}
        slots[:slots][:slot].each do |slot|
          begin
          slot_data[slot[:start_time].to_date.to_s] = slot_data[slot[:start_time].to_date.to_s] || []
          slot_data[slot[:start_time].to_date.to_s] << {:start_time=>slot[:start_time].to_time.strftime("%I:%M"),:end_time=>slot[:end_time].to_time.strftime("%I:%M"), :id=>slot[:appointment_id],:appointment_id=>slot[:appointment_id]}
          rescue
            puts "clinicianId #{clinicianId} slot #{slot.inspect}"
          end
        end
        data[clinicianId] = data[clinicianId] || {:slots=>{}}
        data[clinicianId][:slots].merge!(slot_data)
        
    end
    appointment_data = {:slots_data=>data,:doctor_list=>doctor_list}
    [200,appointment_data]
  end

  def self.cancel_appointment(organization_uid,member_id,options={})
    begin
      current_user = options[:current_user]
      session_id,client_sequence_hash = ::Emis::BookAppointment.get_session_detail(member_id,options)
      if client_sequence_hash.blank?
        return {id:nil,status:100} 
      end
      appointment_details = options[:appointment_details]
      appointment_id =  appointment_details[:appointment_id]
      cancellation_reason = appointment_details[:cancellation_reason]
      @message_guid =  SecureRandom.uuid
      @conversation_guid = @message_guid

     
      msg = {"CancelAppointmentRequest"=>
        {
          "PatientSessionRequestHeader"=>{
            "ns1:ApplicationId" => APP_CONFIG["emis_application_id"], 
            "ns1:SessionId" => session_id,
            "ns1:ClientSequenceNumberHash"=> client_sequence_hash,
            "ns1:ConversationInformation"=>{
              "ns1:MessageGuid"=> @message_guid,
              "ns1:ConversationGuid"=>@conversation_guid
            } 
          },
          "AppointmentID"=>appointment_id,
          "CancellationReason"=>cancellation_reason
        }
      }
      # @client = @client || Savon.client(wsdl: Emis::BookAppointment.wsdl_document, env_namespace: :soap, use_wsa_headers: true, soap_version: 2,log:true,namespaces: Emis::BookAppointment.namespaces,element_form_default: :qualified,pretty_print_xml:true,namespace_identifier: :ns)
      @client = @client || Emis::Client.new_client(Emis::BookAppointment.wsdl_document, Emis::BookAppointment.namespaces)
      response = @client.call(:cancel_appointment, message: msg)
      response = response.body[:cancel_appointment_response][:cancel_appointment_result]
      request_data = {:action=> "cancel_appointment",:message_id=>@message_guid,:patient_id=>nil}    
      if (response[:response_information][:success]== true rescue false) 
        Emis::BookAppointment.log_request_data(current_user,member_id,request_data,true,options)
        {id:appointment_id,status:200}
      else
        raise 'invalid session' if @retry_count <= 2
         @retry_count = 0
         Emis::BookAppointment.log_request_data(current_user,member_id,request_data,false,options)

        {error:response[:response_information][:response_detail][:message], status:100}
      end
    rescue Exception=>e 
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      Emis::BookAppointment.log_request_data(current_user,member_id,request_data,false,options)
      {id:appointment_id,status:100}
    end
  end
  
  # options = {current_user=>}
  def self.book_appointment(organization_uid,member_id,appointment_details,options={})
    begin
      current_user = options[:current_user]
      session_id,client_sequence_hash = ::Emis::BookAppointment.get_session_detail(member_id,options)
      if client_sequence_hash.blank?
        return {message:"Unable to access account",status:100} 
      end
      user_clinic_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
    
      clinic_detail = ::Clinic::ClinicDetail.where(organization_uid:organization_uid).last
      appointment_id =  appointment_details[:appointment_id]
      purpose = appointment_details[:appointment_purpose]
      @message_guid =  SecureRandom.uuid
      @conversation_guid = @message_guid
      msg = {"BookAppointmentRequest"=>
        {
          "PatientSessionRequestHeader"=>{
            "ns1:ApplicationId" => APP_CONFIG["emis_application_id"], 
            "ns1:SessionId" => session_id,
            "ns1:ClientSequenceNumberHash"=> client_sequence_hash,
            "ns1:ConversationInformation"=>{
              "ns1:MessageGuid"=> @message_guid,
              "ns1:ConversationGuid"=>@conversation_guid
            } 
          },
          "AppointmentID"=>appointment_id,
          "BookingParameters"=>{
            "BookingReason"=>purpose,
            "TelephoneNumber"=>appointment_details[:telephone_no],
            "TelephoneContactType" => appointment_details[:telephone_contact_type]
          }
        }
      }
    # @client = Savon.client(wsdl: Emis::BookAppointment.wsdl_document, env_namespace: :soap, use_wsa_headers: true, soap_version: 2,log:true,namespaces: Emis::BookAppointment.namespaces,element_form_default: :qualified,pretty_print_xml:true,namespace_identifier: :ns)
    @client = Emis::Client.new_client(Emis::BookAppointment.wsdl_document, Emis::BookAppointment.namespaces)
    response = @client.call(:book_appointment, message: msg)
    result = response.body[:book_appointment_response][:book_appointment_result]
    request_data = {:action=> "book_appointment",:message_id=>@message_guid,:patient_id=>nil}    
    request_data[:sequence_number] = user_clinic_session.sequence_number rescue nil
    
    if (result[:patient_session_response_header][:session_valid] == false rescue true)
      GpsocLogger.info "retry count is #{@retry_count.to_i}"
      user_clinic_session = ::Clinic::UserClinicSession.where(member_id:member_id).last
      request_data[:sequence_number] = user_clinic_session.sequence_number rescue nil
      raise 'invalid session' if @retry_count.to_i <= 2

    elsif (result[:response_information][:success]== true rescue false)
      @retry_count = 0
      Emis::BookAppointment.log_request_data(current_user,member_id,request_data,true,options)
      {id:appointment_id,:source=>"emis",status:200,message:"Appointment booked successfully"}
    else
       @retry_count = 0
      Emis::BookAppointment.log_request_data(current_user,member_id,request_data,false,options)
      {message:result[:response_information][:response_detail][:message],status:100}
    end
    rescue Exception => e
      if @retry_count.to_i <= 2
        ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
        @retry_count = @retry_count.to_i + 1 
        retry 
      end
       @retry_count = 0
      Emis::BookAppointment.log_request_data(current_user,member_id,request_data,false,options)
      {message:result[:response_information][:response_detail][:message],status:100}
    end
  end

  def self.update_appointment(organization_uid,member_id,appointment_details,options={})
    {id:Random.rand(99999999999),status:200}
  end
  
  def self.get_doctor_list(organization_uid,member_id,options)
    current_user = options[:current_user]
    status, response_data = ::Emis::BookAppointment.get_available_appointments(organization_uid,member_id,options)
    data  = []

    response_data[:doctor_list].each do |doctor_detail|
      available_appointments = (response_data[:slots_data][doctor_detail[:clinician_id]][:slots] rescue {})
      sorted_available_appointments = available_appointments.keys rescue []
      slots_count_with_appointment_date = (available_appointments.map {|appointment_date,slots| {appointment_date=>slots.count} }.inject(:merge) rescue {})
      doctor_detail[:slots] = {:slots_count_with_appointment_date=>slots_count_with_appointment_date,:available_appointments=>available_appointments,:sorted_available_appointments=>sorted_available_appointments }
     data <<  doctor_detail 
    end
    data
  end

  def self.getPatienAppointment(member_id,options = {})
    
    begin
      current_user = options[:current_user]
      session_id,client_sequence_hash = ::Emis::BookAppointment.get_session_detail(member_id,options)
      @message_guid =  SecureRandom.uuid
      member = Member.find(member_id)
      @conversation_guid = @message_guid
     
      msg = {"GetPatientAppointmentsRequest"=>
        {
        "PatientSessionRequestHeader"=>{
            "ns1:ApplicationId" => APP_CONFIG["emis_application_id"], 
            "ns1:SessionId" => session_id,
            "ns1:ClientSequenceNumberHash"=> client_sequence_hash,
            "ns1:ConversationInformation"=>{
              "ns1:MessageGuid"=> @message_guid,
              "ns1:ConversationGuid"=>@conversation_guid
            } 
          }
        }
      }
      # @client = Savon.client(wsdl: Emis::BookAppointment.wsdl_document, env_namespace: :soap, use_wsa_headers: true, soap_version: 2,log:true,namespaces: Emis::BookAppointment.namespaces,element_form_default: :qualified,pretty_print_xml:true,namespace_identifier: :ns,logger: Rails.logger)
      @client = Emis::Client.new_client(Emis::BookAppointment.wsdl_document, Emis::BookAppointment.namespaces,)
      response = @client.call(:get_patient_appointments, message: msg)
      response = response.body[:get_patient_appointments_response][:get_patient_appointments_result]
      request_data = {:action=> "get_patient_appointments",:message_id=>@message_guid,:patient_id=>nil}    
      
      if (response[:response_information][:success] == false)
        raise 'invalid session' if @retry_count < 2
        @retry_count = 0
        Emis::BookAppointment.log_request_data(current_user,member_id,request_data,false,options)
        {:appointments=>[],:error=>response[:response_information][:response_detail][:message] , :message=>"Unable to get appointment for now. Try again later",:status=>100}
      else
        data = []
        doctor_list = {}
        Emis::BookAppointment.log_request_data(current_user,member_id,request_data,true,options)

        [(response[:clinicians][:clinician] || [])].flatten.each do |doctor_detail|
          begin
            info = doctor_detail
            name = info[:name].strip
            doctor_list[info[:clinician_id].to_s] = {:name=>name,:clinic_id=>info[:clinician_id]} 
          rescue Exception =>e 
            
          end
        end
        appointments = response[:appointment_sessions]  || {}
        appointment_sessions = [] 
        response[:appointment_sessions].each do |key,appointment|
          
          appointment_sessions << appointment
        end
        appointment_sessions = appointment_sessions.flatten
        appointment_sessions.each do |appointment|
          clinician_id = appointment[:clinician_ids][:clinician_id].to_s
          appointment[:slots].each do |slot,appointment_slots|
            [appointment_slots].flatten.each do |appointment_slot|
              data << {:id=>appointment_slot[:appointment_id],
                :purpose=>appointment_slot[:booking_reason], 
                :start_time=>appointment_slot[:start_time].to_time.utc,
                :end_time=>appointment_slot[:end_time].to_time.utc, 
                :appointment_id=>appointment_slot[:appointment_id],
                :doctor_name=>doctor_list[clinician_id][:name],
                :doctor_clinic_id =>clinician_id,
                :appointment_purpose=>appointment_slot[:booking_reason],
                :appointment_status=>"confirmed",
                :member_id=>member_id,
                :organization_uid=>member.organization_uid,
                :can_cancel=>true,
                :can_reschedule=> false,
                :doctor_specialization=>nil
              }
            end  
          end
        end
        @retry_count = 0
         data = data.sort_by{|a| a[:start_time]}.reverse rescue data
        {:appointments=>data,:status=>200}
      end
    rescue Exception=> e
      ::Clinic::UserClinicSession.where(member_id:member_id).delete_all
      @retry_count = @retry_count.to_i + 1 
      retry if @retry_count.to_i <= 2
      @retry_count = 0
      # Emis::BookAppointment.log_request_data(current_user,member_id,request_data,false,options)

      data = {appointments=>[],:error=> e.message,message:"Unable to get appointment for now. Try again later",status:100}
    end
  end

end