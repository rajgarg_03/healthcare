class Nutrient
  include Mongoid::Document
  include Mongoid::Timestamps  
  
  field  :description,        :type => String
  field  :title,              :type => String  
  field  :categories,           :type => Array, :default => []
  field  :recommended,         :type => String, :default => "false"
  field  :position,           :type => Integer 
  field  :pregnancy_description, :type => String
  field  :identifier, :type => String
  field  :tool_category, :type=> String, :default=>"basic" #basic , premium
  field  :tool_for, :type=> String, :default=>"child"  #[child, pregnancy, adult,CP,"general"]
  field  :feature_type, :type=> String, :default=>"tools"  #[tool, general, core]
  field  :launch_status , :type=>String #launched,upcoming,soft_launch
  field  :supported_api_version, :type=>Integer,:default=> nil
  field  :is_mandatory, :type=>Boolean, :default=> false #true/false
  field  :android_supported_app_version, :type=>Integer, :default=>0    
  field  :ios_supported_app_version, :type=>Integer,:default=>0
  
  field  :android_supported_app_version_str, :type=>String, :default=>0    
  field  :ios_supported_app_version_str, :type=>String,:default=>0
  field  :info_screen, :type=>String # used for info screen before subscription 
  field  :subscription_box_data, :type=>Hash # To show tool based subscription data
  
  # using multiple index beacause of limited records 
  index({supported_api_version:1})
  index({tool_for:1})
  index({launch_status:1})
  index({identifier:1})
  index({tool_category:1})
  index({position:1})
  index({android_supported_app_version:1})
  index({ios_supported_app_version:1})
  index({feature_type:1})
  index({title:1})
  index({description:1})


  validates :title, presence: true#, uniqueness: true
  validates :identifier, presence: true#, uniqueness: true

  ToolUser = {:child=>"child",:adult=>"adult"}
  ToolType = {:basic=>"basic", :premium=>"premium"}
  
  Category = ["All","Health", "Mind", "Education","Other"]
  ApiCategory = ["Health", "General","Psychological"]
  
  CardNutirent = {"api_childern_PregTimeline"=>"Timeline","api_childern_PrenatalTest"=>"Prenatal Tests", "api_childern_milestones" => "Milestone","api_childern_health"=> "Measurements", "api_childern_immunsation" =>"Immunisations","api_childern_document"=> "Documents", "api_childern_timelines"=>"Timeline"}
  
  ToolNotToLaunchFeatureType = ["general","box","button"]

  ICON_CLASS = {timeline: "icon-timeline",
    milestones: "icon-manage-check",
    measurements: "icon-tracker",
    immunisations: "icon-immunization",
    immunizations: "icon-immunization",
    documents: "icon-document-color",
    calendar: "icon-calender",
    medical_events: "icon-calender"

  }
  NUTRIENT_PATH = {timeline: "family_member_timeline_index_path(family.url_name,member.id)",
  	milestones: "family_member_milestones_path(family.url_name,member.id)",
  	measurements: "family_member_health_index_path(family.url_name,member.id)",
  	immunisations: "family_member_immunisation_index_path(family.url_name,member.id)",
    immunizations: "family_member_immunisation_index_path(family.url_name,member.id)",
  	documents: "family_member_documents_path(family.url_name,member.id)",
    calendar: "family_member_calendar_index_path(family.url_name,member.id)",
    medical_events: "family_member_calendar_index_path(family.url_name,member.id)"
  }

  def self.is_tool_available?(tool_id,current_user,api_version,tool_for,feature_type="tools")
    return false if tool_id.blank?
    tool = Nutrient.critriea_for_valid_to_launch(current_user,api_version,{:show_feature_type=> feature_type}).where(:id=>tool_id)
    tool = tool.where_tool_for(tool_for)
    tool.present?
  end

  def self.where_tool_for(tool_for=nil)
    if tool_for == "child"
      tool_for_values = ["child","CP"]
    elsif tool_for == "pregnancy"
      tool_for_values = ["pregnancy","CP"]
    elsif tool_for == "adult"
      tool_for_values = ["adult"]
    else
      tool_for_values =  Nutrient.distinct(:tool_for)
    end
    where(:tool_for.in=>tool_for_values)
  end
  
  def self.valid_to_launch(current_user,api_version,options={})
    data = Nutrient.critriea_for_valid_to_launch(current_user,api_version,options)
    data = data.not_in(:tool_for=>["adult"],:feature_type=> Nutrient::ToolNotToLaunchFeatureType)
    
    data
  end

  def self.critriea_for_valid_to_launch(current_user,api_version,options={})
    user = current_user.user
    user_type  = user["user_type"].downcase  rescue nil
    user_device = current_user.devices.last
    device_app_version = (user_device.app_version_in_number  rescue 0).to_i # if web user 
    platform = user_device.platform.downcase rescue "web"
    na_values_for_api_version = -1
    na_values_for_app_version = -1
    if user_type == "internal"
      data = all
    elsif platform == "android"
      data = where(:supported_api_version=>{"$lte" => api_version,"$ne"=>na_values_for_api_version},:android_supported_app_version=>{"$lte" => device_app_version,"$ne"=>na_values_for_app_version})
    elsif  platform == "ios"
     data =  where(:supported_api_version=>{"$lte" => api_version,"$ne"=>na_values_for_api_version},:ios_supported_app_version => {"$lte" => device_app_version,"$ne"=>na_values_for_app_version})
    else
      data = where(:supported_api_version=>{"$lte" => api_version,"$ne"=>na_values_for_api_version})
    end
    data = data.not_in(:launch_status=>"soft_launch")  if (user_type.blank? || user_type == "general" || user_type == "normal" )  
    if options[:add_default_nutrient] == true
      data = data.where(:launch_status=>"launched") if (user_type.blank? || user_type == "general" || user_type == "normal" )  
    end
    if options[:show_feature_type].blank? 
      data = data.in(:feature_type=>["tools",nil,"","core"])
    elsif options[:show_feature_type] == "tools"
      data = data.in(:feature_type=>["tools",nil,""])
    
    elsif options[:show_feature_type] == "all"
      data = data
    else
      data = data.where(:feature_type=>options[:show_feature_type])
    end
    identifier_list_remove = []
    identifier_list_remove << "Manage Tools" if api_version < 5
    user_sanitized_country_list = Member.sanitize_country_code(user.country_code)
    faq_notification_enabled = System::Settings.faqNotificationEnabled?(current_user,user.country_code,options)

    if !faq_notification_enabled
      identifier_list_remove << "FAQ"
    end

    if !user_sanitized_country_list.include?("uk")
      identifier_list_remove << "NHS Library" 
    end
    data =  data.not_in(identifier:identifier_list_remove) if identifier_list_remove.present?
    data
  end

   
  
  def self.PA_tool_name(current_user,api_version)
    tool_names = ["Menu"]
    valid_tools_for_adult = adult_tool_valid_for_launch(current_user,api_version).order_by("position asc").pluck(:title)
    tool_names << valid_tools_for_adult
    tool_names.flatten.uniq
  end


  def self.adult_tool_valid_for_launch(current_user,api_version)
    data = Nutrient.critriea_for_valid_to_launch(current_user,api_version)
    data = data.where_tool_for("adult")
    data  
  end

  def self.for_pregnancy(api_version,current_user,options={})
      api_version = api_version.present? ? api_version : 1
      where(:supported_api_version.lte =>api_version).where_tool_for("pregnancy").valid_to_launch(current_user,api_version,options).order_by("position ASC")
  end

  def self.for_born_child(api_version,current_user=nil,options={})
      api_version = api_version.present? ? api_version : 1
      where(:supported_api_version.lte =>api_version).where_tool_for("child").valid_to_launch(current_user,api_version,options).order_by("position ASC")
  end

  def self.get_member_tools(child,current_user,api_version,options={})
    valid_tool_for = child.is_expected? ? "pregnancy" : "child"
    child_tools = child.member_nutrients(api_version,current_user,options).pluck(:nutrient_id)
    if options[:system_tool_ids].present?
      tool_ids = child_tools & options[:system_tool_ids]
      Nutrient.where(:id.in=>tool_ids).where_tool_for(valid_tool_for).valid_to_launch(current_user,api_version,options).order_by("position ASC")
    else
      Nutrient.valid_to_launch(current_user,api_version,options).in(:id => child_tools).where_tool_for(valid_tool_for).order_by("position ASC")
    end
  end
  # list all active tool in family group by category(basic/premium) 
  def self.active_tool_list_for_family(current_user,family_id)
    child_ids = FamilyMember.where(family_id:family_id).in(role:["Son","Daughter"]).pluck(:member_id)
    basic_tool_ids = Nutrient.where(tool_category:"basic").pluck(:id)
    premium_tool_ids = Nutrient.where(tool_category:"premium").pluck(:id)
    data = {}
    data["basic"] = MemberNutrient.in(member_id:child_ids).in(nutrient_id:basic_tool_ids).entries
    data["premium"] = MemberNutrient.in(member_id:child_ids).in(nutrient_id:premium_tool_ids).entries
    data
  end

  # check if tool is subscibed in subscription plan and it is valid
  def get_subscription_status(current_user,family_id,api_version,options={})
    platform = current_user.device_platform
    tool = self
    # find family subscription plan or create if not exist
    subscription = options[:subscription] || Subscription.find_create_subscription(current_user,family_id,platform,api_version)
    free_access_to_premium_tool_status = System::Settings.has_free_access_to_premium_tool_status?(current_user,options)
    if free_access_to_premium_tool_status
      subscription_manager = SubscriptionManager.premium_subscriptions.first
    else
      subscription_manager = subscription.subscription_manager
    end
    subscription_listing_order = subscription_manager.subscription_listing_order
     # check user did not cross max limit to add tool 
    status =  !subscription.is_add_tool_count_limit_exceed?(tool.tool_category,subscription_manager,options)
    
    # Don't check price exempt for now
    #free_trial_available_status,free_trial_msg = Subscription.free_trial_end_date_message(current_user,family_id,subscription,subscription,options)
    #status = status && free_trial_available_status
    
    if status
      message = Message::Subscription[:sufficient_level] 
      tool_subscription_level_required = subscription_listing_order
    else
      next_higher_plan = SubscriptionManager.valid_subscription_manager(current_user,api_version).where(:subscription_listing_order.gt=>subscription_listing_order).order_by("subscription_listing_order asc").first
      tool_subscription_level_required = next_higher_plan.subscription_listing_order rescue subscription_listing_order + 1
      message = Message::Subscription[:insufficient_level]
    end
  
     [status,message,tool_subscription_level_required]
  end
  
  def show_subscription_box_status(current_user,family_id,api_version=4,options={})
    tool = self
    subscription_box_show_status = true
    subscription = Subscription.family_free_or_premium_subscription(family_id) || Subscription.find_create_subscription(current_user,family_id,current_user.device_platform,api_version)
    options = options.merge({:checking_for_show_subscription_status=>true})

    is_sufficient_subscription, msg, required_subscription_level = tool.get_subscription_status(current_user,family_id,api_version,{:subscription=>subscription})
    free_access_to_premium_tool_status = false #System::Settings.has_free_access_to_premium_tool_status?(current_user,options)
    # Check if user eligible for free access to premium tool or Tool is basic
    if free_access_to_premium_tool_status || tool.tool_category == "basic"
      free_trial_available_status,free_trial_msg = [true,nil]
    else
      if api_version > 4
        free_trial_available_status,free_trial_msg = Subscription.free_trial_end_date_message(current_user,family_id,subscription,subscription,options)
      else
        free_trial_available_status = true
      end
    end
    if is_sufficient_subscription && free_trial_available_status
      subscription_box_show_status = false
    end
    [subscription_box_show_status,free_trial_available_status,free_trial_msg]
  end

  def activate_tool(api_version,current_user,params)
    tool = self
    available_for_user = true
    is_valid_to_launch = Nutrient.valid_to_launch(current_user,api_version).where(id:tool.id.to_s).present?
    user_type  = current_user.user["user_type"].downcase rescue ""
    if user_type != "internal"
      available_for_user = tool.launch_status == "launched"
    end
    raise "Tool is not available" if !is_valid_to_launch || !available_for_user
     # create member tool in member_nutrients
    member = Member.find(params[:member_id])
    family_id = member.member_families.last.family_id
    # Raise error if tool limit exceed or not sufficient for current subscription
    subscription = Subscription.family_free_or_premium_subscription(family_id) || Subscription.find_create_subscription(current_user,family_id,current_user.device_platform,api_version)
    if api_version < 5
      subscription_status, msg, required_subscription_level = tool.get_subscription_status(current_user,family_id,api_version,{:subscription=>subscription})
    else
      subscription_status, msg, required_subscription_level = [true,"sufficient level",0]
    end
    raise msg if !subscription_status
  
    member_nutrient_obj = MemberNutrient.where(member_id:params[:member_id],nutrient_id:tool.id, position: tool.position).first_or_create
    member_nutrient = member_nutrient_obj.attributes.to_h
    
    member_nutrient['message'] = Message::API[:success][:acitvate_nutrient]
    member_nutrient['status'] = StatusCode::Status200
    #Update timeline filter to show in list
    Timeline.update_timeline_state_for_tool(member, tool.title,true)
    # update subscription tool added count 
    subscription.update_subscription_tool_count(tool,family_id,"activated")
    member_nutrient
  end
  
  def deactivate_tool(api_version,current_user,params)
    tool = self
    @member_nutrient = MemberNutrient.where(member_id:params[:member_id],nutrient_id:params[:id]).first
    member = @member_nutrient.member
    @member_nutrient.destroy
    #Update timeline filter to show in list
    Timeline.update_timeline_state_for_tool(member, tool.title,false)
    # Remove tool from subscription subscribed_tool_list
    family_id = member.member_families.last.family_id
    # update subscription tool added count 
    subscription = Subscription.family_free_or_premium_subscription(family_id) || Subscription.find_create_subscription(current_user,family_id,current_user.device_platform,api_version)
    subscription.update_subscription_tool_count(tool,family_id,"deactivated")
    @member_nutrient
  end
  
  def self.is_tool_activated?(child_id,tool_name)
    return true if tool_name.blank? || ["home","pointer","pointers","manage tools","faq","FAQ",'premium_subscription'].include?(tool_name.downcase)
    active_tools = MemberNutrient.where(member_id:child_id).pluck(:nutrient_id)
    active_tool_name_list = Nutrient.in(id:active_tools).pluck(:title).map(&:downcase)
    active_tool_name_list.include?(tool_name.downcase) rescue false
  end

  def self.premium_tools
    where(tool_category:Nutrient::ToolType[:premium])
  end
 
  def self.convert_device_app_version_number(number,device)
    if number.blank? || number.downcase == "na"
      return -1
    end
    if number.present? && device.present?
      data = number.to_s.split(".")
      if device == "ios"
        (data[0].to_i * (10**6)) + (data[1].to_i * (10**3)) + (data[2].to_i)
      elsif device == "android"
         (data[0].to_i * (10**6)) + (data[1].to_i * (10**3)) + (data[2].to_i)
      else
        number
      end
    else
      raise "Invalid number and device"
    end
  end
 
  def get_identifier(member,options={})
    self.identifier
  end
  
  def self.get_tool_id(identifier)
    tool =  get_tool(identifier)
    tool.id.to_s rescue nil
  end
  
  def self.get_tool(identifier)
    tool_id = Nutrient.where(:identifier=>/^#{identifier}$/i).last rescue nil
  end

  def clean_gpsoc_records(member_id,options={})
    tool = self 
    data_source = options[:data_source]
    if data_source.present?
      if tool.identifier == "Measurements"
        Health.where(member_id:member_id,data_source:data_source).delete_all
      elsif tool.identifier == "Documents"
        Document.where(member_id:member_id,data_source:data_source).delete_all
      end
      Timeline.where(data_source:data_source).delete_all
    end
  end

  def get_clinic_class_for_member_tool(current_user,api_version,organization_uid,member,options={})
    tool = self
    clinic_class = ::Clinic::ClinicDetail.get_clinic_linkable_class(current_user,member,tool.identifier,options)
    clinic_class
  end

  def get_json(options={})
    tool = self
    # "tool_activated"=> true
    is_adult_tools = false
    is_child_tools = false
    is_pregnancy_tools = false
    if tool.tool_for == "adult"
      is_adult_tools = true
    else 
      if tool.tool_for=='cp'
        is_child_tools =  true
        is_pregnancy_tools =  true
      elsif tool.tool_for =='child'
        is_child_tools =  true
      
      elsif tool.tool_for =='pregnancy' 
        is_pregnancy_tools =  true
      end
    end

    data = {  
        "launch_status"=> tool.launch_status, 
        "name"=> tool.title,
        "image"=> tool.identifier, 
        "tool_category"=> tool.tool_category, 
        "is_child_tools"=> is_child_tools ,
        "is_pregnancy_tools"=>is_pregnancy_tools, 
        "is_adult_tools"=> is_adult_tools, 
        "is_premium"=> (tool.tool_category == "premium") 
      }
    data
  end
end
