class WhoData::Pregnancy::Chart

  def self.blood_pressure
    # data = [[0,90],[42,120]]
    # min_max_x_axis = [0,42]
    # [data,min_max_x_axis]

    data_for_systolic = [[0,120],[42,120]]
    systolic_min_max_x_axis = [0,42]
    data_for_diastolic = [[0,60],[42,60]]
    diastolic_min_max_x_axis = [0,42]
    {:systolic=> [data_for_systolic,systolic_min_max_x_axis],:diastolic=>[data_for_diastolic, diastolic_min_max_x_axis]}

  end
 

  def self.hemoglobin
    # data_for_low = [[0,8],[42,10]]
    # low_min_max_x_axis = [0,42]
    # data_for_high = [[0,18],[42,22]]
    # high_min_max_x_axis = [0,42]
    # {:low=> [data_for_low,low_min_max_x_axis],:high=>[data_for_high, high_min_max_x_axis]}

    data_for_low = [[0,12],[42,10]]
    low_min_max_x_axis = [0,42]
    data_for_high = [[0,16],[42,14]]
    high_min_max_x_axis = [0,42]
    {:low=> [data_for_low,low_min_max_x_axis],:high=>[data_for_high, high_min_max_x_axis]}
  end

 
  # def self.blood_pressure
  #   data_for_low = [[0,70],[42,80]]
  #   data_for_high = [[0,110],[42,130]]

  #   min_max_x_axis_for_low = [0,42]
  #   min_max_x_axis_for_high = [0,42]

  #   {:low=>[data_for_low,min_max_x_axis_for_low],:high=>[data_for_high,min_max_x_axis_for_high]}
  # end

  def self.bmi
    # data = [[0,18.5],[42,25]]
    # min_max_x_axis = [0,42]
    # [data,min_max_x_axis]
    data_for_min = [[0,18.5],[42,22]]
    min_min_max_x_axis = [0,42]
    data_for_max = [[0,24.9],[42,28]]
    max_min_max_x_axis = [0,42]
    {:min=> [data_for_min,min_min_max_x_axis],:max=>[data_for_max, max_min_max_x_axis]}
  end

end