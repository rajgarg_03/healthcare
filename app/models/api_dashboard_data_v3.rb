module ApiDashboardData1

# Start of ios api for Home page 
 def set_priority1(api_version=1)
      {"2"=>["AddChild1"],  "3"=>["AddSpouse","AddTimeline","AddImmunization","AddMilestone","AddMeasurement","AddPrenatalTest","AddPregnancyTimeline","AddChildPic"] ,"5"=>["FamilyChildren"] }
  end



  def set_priority(api_version=1)
    data = {}
   ActionCardList.where(:supported_api_version.lte=>api_version).order_by("priority asc").each do |card|
     
    k = data[card[:priority].to_s] || []
    k << card[:name]
    data.merge!({card[:priority].to_s => k})
   end
  data
  end

  def AddChildPic(ids)
    tmp_child_ids = ids - Picture.where(upload_class:"pic").in(imageable_id:ids).pluck("imageable_id")#.map(&:to_s)
    if tmp_child_ids.blank?
      [false,[]]
    else
      [true,childerns(tmp_child_ids)]
    end
  end

  def AddChild1(ids)
    if ids.present?
      [false,[]]
    else
      [true, user_families]
    end
  end
  
   
  def AddPregnancyTimeline(ids)
    # tmp_child_ids = ids - Timeline.where({'member_id' => { "$in" => ids} }).group_by(&:member_id).keys
    ids = Pregnancy.in(expected_member_id:ids).where(status:"expected").pluck("expected_member_id").map(&:to_s)
    tmp_child_ids = ids - Timeline.where({'member_id' => { "$in" => ids} }).where("'updated_at' > " + "'#{(Time.now - 1.month)}'").group_by(&:member_id).keys.map(&:to_s)
    if tmp_child_ids.blank?
      [false,[]]
    else
      temp_childern = childerns(ids)
      [true, temp_childern.entries.select{|x| tmp_child_ids.include?(x.id.to_s)}]
    end
  end

  def AddPrenatalTest(ids)
    ids = Pregnancy.in(expected_member_id:ids).where(status:"expected").pluck("expected_member_id").map(&:to_s)
    tmp_child_ids = ids - MedicalEvent.where({'member_id' => { "$in" => ids} }).group_by(&:member_id).keys.map(&:to_s)
    if tmp_child_ids.blank?
      [false,[]]
    else
      temp_childern = childerns(ids)
      [true, temp_childern.entries.select{|x| tmp_child_ids.include?(x.id.to_s)}]
    end
  end


  
  
  def AddChild2(ids)
     p_family_childern = Family.in(id:famlies_without_child).entries
     if p_family_childern.present?
       return [true,p_family_childern]
     else
      return [false, []]
     end
  end

  def AddImmunization(ids)
    vacciantions = Vaccination.where({'member_id' => { "$in" => ids} }).where(opted:"true")
    # vaccination_ids = vacciantions.map(&:id)
    # jabs = Jab.in(:vaccination_id=>vaccination_ids).where(:status=>"Planned").or(:due_on.lte => Time.now).or(:due_on => nil,:est_due_time.lte=>Time.now)
    member_without_vaccination = ids - vacciantions.group_by(&:member_id).keys
    tmp_child_ids =   member_without_vaccination #+ Vaccination.in(id:jabs.map(&:vaccination_id).uniq).group_by(&:member_id).keys
    if tmp_child_ids.blank?
      [false,[]]
    else
      temp_childern = childerns(ids)
      [true, temp_childern.entries.select{|x| tmp_child_ids.include?(x.id)}]
    end
  end
  
  def AddMilestone(ids)
     tmp_child_ids = (ids) - (Milestone.where({'member_id' => { "$in" => ids} }).where("'created_at' > " + "'#{(Time.now - 1.month)}'").group_by(&:member_id).keys)
     if tmp_child_ids.blank?
      return [false,[]]
    else
      temp_childern = childerns(ids)
     [true, temp_childern.entries.select{|x| tmp_child_ids.include?(x.id)}]
   end
  end
  
  def AddMeasurement(ids)
     child_ids = ids - Health.where({'member_id' => { "$in" => ids} }).where("'updated_at' > " + "'#{(Time.now - 1.month)}'").group_by(&:member_id).keys
    # @childerns_health = childern.entries.select{|x| child_ids.include?(x.id)}
    
    if child_ids.blank?
      [false,[]]
    else
      [true,childerns(ids).entries.select{|x| child_ids.include?(x.id)}]
    end
  end

  def AddTimeline(ids)
    child_ids = ids - Timeline.where({'member_id' => { "$in" => ids} }).where("'updated_at' > " + "'#{(Time.now - 1.month)}'").group_by(&:member_id).keys
    if child_ids.blank?
      return [false, []]
    else
      temp_childern = childerns(ids)
      @childern_timelines = temp_childern.entries.select{|x| child_ids.include?(x.id)}
      return [true, @childern_timelines]
    end

  end


    def AddSpouse(ids)
       current_user =  self
      all_families_list = current_user.user_families.map{|a| a.id.to_s}
      invitation_for_father_from_family = Invitation.in(family_id:all_families_list).in(role:["Father","father"]).where(status: "invited").pluck(:family_id).map(&:to_s).uniq
      invitation_for_mother_from_family = Invitation.in(family_id:all_families_list).in(role:["Mother","mother"]).where(status: "invited").pluck(:family_id).map(&:to_s).uniq
      
      families_list = FamilyMember.in(family_id:all_families_list,role:["Father","Mother"]).map{|a| a.family_id.to_s}
      families_list = families_list + invitation_for_mother_from_family + invitation_for_mother_from_family

      family_with_father = FamilyMember.in(family_id:families_list).where(role:"Father").pluck(:family_id).map(&:to_s).uniq
      
      family_with_mother = FamilyMember.in(family_id:families_list).where(role:"Mother").pluck(:family_id).map(&:to_s).uniq
      
      family_without_mother = families_list - ( family_with_mother + invitation_for_mother_from_family)
      family_without_father = families_list - ( family_with_father + invitation_for_father_from_family)
      family_without_spouse = []#FamilyMember.in(family_id:families_list).not_in(role:[FamilyMember::ROLES[:father],FamilyMember::ROLES[:father]]).pluck(:family_id).map(&:to_s).uniq
      family_ids = (family_without_mother + family_without_father + family_without_spouse).uniq
      families = Family.in(id:family_ids)
      if families.blank?
        [false,[]]
      else
        [true,families]
     end
  end


  #value is childernIds/FamilyIds based on condiotion in action Logic
  def res(name,value,msg_flag=false)
    result = []
    tool_list_mapping_with_name = {"AddPregnancyTimeline"=>"Timeline","AddPrenatalTest"=>"Prenatal Tests", "AddMilestone" => "Milestones","AddMeasurement"=> "Measurements", "AddImmunization" =>"Immunisations", "AddTimeline"=>"Timeline"}

    all_child_ids = value.map{|v| v.id.to_s}
    nutrient =  Nutrient.where(title:tool_list_mapping_with_name[name]).last
    if nutrient.present?
    # only activated tools 
      child_ids = MemberNutrient.in(member_id:all_child_ids).where(status:"active",nutrient_id:nutrient.id).map{|a| a.member_id.to_s}
    else
      child_ids = all_child_ids
    end
    # Get dismissed tool list
    nutrient_name = name.gsub("Add","")
    dismiss_nutrient_ids =  DismissNutrient.where(nutrient_name: nutrient_name , parent_id: self.id).where({'child_id' => { "$in" => child_ids } }).or({:dismiss_up_to.gte=> Date.today},{:skipped_forever=>true}).pluck("child_id")
    
    # helper data to make line data dynmic
    pronoun1 = {"Son"=> "boy", "Daughter" => "girl"}
    pronoun2 = {"Son"=> "his", "Daughter" => "her"}
    
    if name=="AddChild1" || name == "AddChild12"
      nutrient_members =  famlies_without_child
      dismissable = (self.user_families.count > 1)
    elsif name == "AddSpouse"
      nutrient_members =  child_ids -  dismiss_nutrient_ids #families
    else
      if !["AddSpouse","AddPrenatalTest","AddPregnancyTimeline"].include?(name)
        # Remove expected member from all member list 
        pregnancy_childern = Pregnancy.in(expected_member_id:child_ids).where(status:"expected").map{|a| a.expected_member_id.to_s }
        nutrient_members = ChildMember.in(:id => (child_ids - (dismiss_nutrient_ids + pregnancy_childern) ) ) 
      else
        pregnancy_childern = Pregnancy.in(expected_member_id:child_ids).where(status:"expected").map{|a| a.expected_member_id.to_s }
        nutrient_members = ChildMember.in(:id => (pregnancy_childern - dismiss_nutrient_ids ) ) 
      end
    end
    nutrient_members.each do |child_member_obj|
      data = {}
      data[:role] = pronoun1[child_member_obj.family_members.first.role] rescue ""
      role = data[:role]
      role_pronoun = pronoun2[child_member_obj.family_members.first.role] rescue ""
      him_her = pronoun2[child_member_obj.family_members.first.role] rescue ""
      data[:pic_url] =  child_member_obj.profile_image.complete_image_url(:small,api_version) rescue ""
      card_data = @action_card_data[name][0]
      age = (ApplicationController.helpers.calculate_age(child_member_obj.birth_date|| Date.today) ) rescue nil
      user_pic = (self.profile_image.photo_url_str("small",api_version) rescue "")
      child_pic = (child_member_obj.profile_image.photo_url_str("small",api_version) rescue "")
      child_name = child_member_obj.first_name.strip rescue ""
    case name
    when "AddSpouse"
      family_id = child_member_obj #id of family
      family = Family.find(family_id)
      familymember = FamilyMember.where(family_id:family_id).pluck(:role)
      familymember = familymember +  Invitation.where(family_id:family_id,status: "invited").pluck(:role)

      if familymember.include?("Mother")
        role = "father"
        pronoun = "him"
      elsif familymember.include?("Father")
        role = "mother"
        pronoun = "her"
      else
        role = "mother"
        pronoun = "him"
      end
      family_name = family.name
      data[:header] = family_name rescue ""# "#{child_member_obj.first_name} is #{ApplicationController.helpers.calculate_age(child_member_obj.birth_date|| Date.today)} old"
      data[:image] = card_data["image"]
      data[:line1] = card_data["line1"]#(self.profile_image.photo_url_str("small") rescue "")
      data[:line2] = eval(card_data["line2"]) 
      data[:line3] = eval(card_data["line3"]) # not used by IOS (Discussed with Dharmendra)
      data[:line4] = eval(card_data["line4"])
      data[:button] = eval(card_data["button"])
      data[:type] =  card_data["card_type"] 
      data[:family_id] = family_id
      data[:member_id] = self.id
      data[:card_id] = family_id.to_s + "_" + card_data["card_type"]
      data[:dismissable] = card_data["dismissable"]
      data[:role] = role rescue ""
      data[:notific_msg] = "" if msg_flag
      data[:pic_url] = user_image
    when "AddChild1","AddChild2"
      record = child_member_obj # child_member is family record (family obj)
      family = Family.find(record)
      data = { :family_id =>record,
                :identifier=>rand(999999999999999),
                :header=> (family.name rescue ""),
                :line1=>  eval("\"" + card_data["tech_line1"]  + "\""),
                :dismissable => dismissable,
                :line2=>  eval("\"" + card_data["tech_line2"]  + "\""),
                :image=>  card_data["image"],
                :type=>   card_data["card_type"],
                :line3=>  eval("\"" + card_data["tech_line3"]  + "\""),
                :line4=>  eval("\"" + card_data["tech_line4"]  + "\""),
                :button=> eval(card_data["button"]),
                :card_id=> record.to_s + card_data["card_type"] 
              }
              data.merge!({:notific_msg=> "Add the little one we need to take care of in #{family.name}"}) if msg_flag
    when "AddPregnancyTimeline"
      role = nil
      data[:header] =  eval("\"" + card_data["header"]  + "\"")
      data[:image] =   eval("\"" + card_data["image"] + "\"")
      data[:line1] = eval("\"" + card_data["tech_line1"]  + "\"")
      data[:line2] = eval("\"" + card_data["tech_line2"]  + "\"")
      data[:line3] = eval("\"" + card_data["tech_line3"]  + "\"")
      data[:line4] = eval("\"" + card_data["tech_line4"]  + "\"")
      data[:button] = card_data["button"]
      data[:type] = card_data["card_type"]
      data[:member_id] = child_member_obj.id
      data[:card_id] = child_member_obj.id.to_s + "_" + nutrient_name
      data[:dismissable] = true
      data[:role] = nil rescue ""
      data[:notific_msg] = eval("\"" + card_data["tech_notification_message"]  + "\"") if msg_flag
    when "AddPrenatalTest"
      role = nil
      data[:header] =  eval("\"" + card_data["header"]  + "\"")
      data[:image] =   eval("\"" + card_data["image"] + "\"")
      data[:line1] = eval("\"" + card_data["tech_line1"]  + "\"")
      data[:line2] = eval("\"" + card_data["tech_line2"]  + "\"")
      data[:line3] = eval("\"" + card_data["tech_line3"]  + "\"")
      data[:line4] = eval("\"" + card_data["tech_line4"]  + "\"")
      data[:button] = card_data["button"]
      data[:type] = card_data["card_type"]
      data[:member_id] = child_member_obj.id
      data[:card_id] = child_member_obj.id.to_s + "_" + nutrient_name
      data[:dismissable] = card_data["dismissable"] || true
      data[:role] = role rescue ""
      data[:notific_msg] = eval("\"" + card_data["tech_notification_message"]  + "\"") if msg_flag
    when "AddTimeline" 
      data[:header] =  eval("\"" + card_data["header"]  + "\"")
      data[:image] =   eval("\"" + card_data["image"] + "\"")
      data[:line1] = eval("\"" + card_data["tech_line1"]  + "\"")
      data[:line2] = eval("\"" + card_data["tech_line2"]  + "\"")
      data[:line3] = eval("\"" + card_data["tech_line3"]  + "\"")
      data[:line4] = eval("\"" + card_data["tech_line4"]  + "\"")
      data[:button] = card_data["button"]
      data[:type] = card_data["card_type"]
      data[:member_id] = child_member_obj.id
      data[:card_id] = child_member_obj.id.to_s + "_" + nutrient_name
      data[:dismissable] = card_data["dismissable"] || true
      data[:notific_msg] = eval("\"" + card_data["tech_notification_message"]  + "\"") if msg_flag
    when "AddImmunization"
      data[:header] =  eval("\"" + card_data["header"]  + "\"")
      data[:image] =   eval("\"" + card_data["image"] + "\"")
      data[:member_id] = child_member_obj.id
      country_code = self.user.country_code rescue "uk"
      vaccin_count = calculate_vaccin(child_member_obj,ApplicationController.helpers.calculate_age(child_member_obj.birth_date|| Date.today))
      data[:line1] = eval("\"" + card_data["tech_line1"]  + "\"")
      data[:line2] = eval("\"" + card_data["tech_line2"]  + "\"")
      data[:line3] = eval("\"" + card_data["tech_line3"]  + "\"")
      data[:line4] = eval("\"" + card_data["tech_line4"]  + "\"")
      data[:type] =  card_data["card_type"]
      data[:button] = card_data["button"]
      data[:card_id] = child_member_obj.id.to_s + "_" + nutrient_name
      data[:dismissable] = card_data["dismissable"] || true
      data[:notific_msg] = eval("\"" + card_data["tech_notification_message"]  + "\"") if msg_flag
    when "AddMilestone"
      age = ApplicationController.helpers.calculate_age(child_member_obj.birth_date|| Date.today)
      data[:header] =   eval("\"" + card_data["header"]  + "\"")
      data[:image] =    eval("\"" + card_data["image"] + "\"")
      data[:line1] = eval("\"" + card_data["tech_line1"]  + "\"")
      data[:line2] = eval("\"" + card_data["tech_line2"]  + "\"")
      data[:line3] = eval("\"" + card_data["tech_line3"]  + "\"")
      data[:line4] = eval("\"" + card_data["tech_line4"]  + "\"")
      data[:type] =  card_data["card_type"]
      data[:button] = card_data["button"]
      data[:member_id] = child_member_obj.id
      data[:card_id] = child_member_obj.id.to_s + "_" + nutrient_name
      data[:dismissable] = card_data["dismissable"] || true
      data[:notific_msg] =  eval("\"" + card_data["tech_notification_message"]  + "\"") if msg_flag
    when "AddMeasurement"
      data[:header] =   eval("\"" + card_data["header"]  + "\"")
      data[:image] =    eval("\"" + card_data["image"] + "\"")
      data[:line1] = eval("\"" + card_data["tech_line1"]  + "\"")
      data[:line2] = eval("\"" + card_data["tech_line2"]  + "\"") # #{child_member_obj.first_name.strip} is growing quickly. Let me record the current measurements"
      data[:line3] = eval("\"" + card_data["tech_line3"]  + "\"")
      data[:line4] = eval("\"" + card_data["tech_line4"]  + "\"")
      data[:button]= card_data["button"]
      data[:member_id] = child_member_obj.id
      data[:type] = card_data["card_type"]
      data[:card_id] = (child_member_obj.id.to_s + "_" + card_data["card_type"])
      data[:dismissable] = card_data["dismissable"] || true
      data[:notific_msg] =  eval("\"" + card_data["tech_notification_message"]  + "\"") if msg_flag
    
    when "AddChildPic"
      data[:header] =   eval("\"" + card_data["header"]  + "\"")
      data[:image] = eval("\"" + card_data["image"] + "\"") || data[:role] + "-image" rescue nil
      data[:line1] = eval("\"" + card_data["tech_line1"]  + "\"")
      data[:line2] = eval("\"" + card_data["tech_line2"]  + "\"") # {child_member_obj.first_name.strip} doesn't have a profile pic"
      data[:line3] = eval("\"" + card_data["tech_line3"]  + "\"")
      data[:line4] = eval("\"" + card_data["tech_line4"]  + "\"")
      data[:button] = card_data["button"]
      data[:member_id] = child_member_obj.id
      data[:type] = card_data["card_type"]
      data[:card_id] = child_member_obj.id.to_s + "_" + nutrient_name
      data[:dismissable] =  card_data["dismissable"]
      data[:notific_msg] = eval("\"" + card_data["tech_notification_message"]  + "\"") if msg_flag

      
    else
      data[:member_id] = child_member_obj.id
    end
    
    result << data
     end
    result
  end

  def calculate_vaccin(child_member, child_member_age)
      total_jabs = 0
    if child_member.vaccinations.count == 0 
      age_in_days = SystemJab.convert_to_days(child_member_age)
      country_code = Vaccination::Country[(self.user.country_code || "uk").downcase]||"UK"
      if  child_member.family_members.first.role == FamilyMember::ROLES[:daughter]
        sys_vaccin_ids = SystemVaccin.where(vaccin_type: "Recommended").where(country:country_code).map(&:id)
      else
        sys_vaccin_ids = SystemVaccin.where(vaccin_type: "Recommended").where(country:country_code).not_in(:only_for=>[FamilyMember::ROLES[:daughter]]).map(&:id)
      end
      sys_jabs = SystemJab.in(system_vaccin_id:sys_vaccin_ids).where(:due_on_in_day_int.lte  => age_in_days)#.where(:due_on_in_day.gt=>0)
      total_jabs = sys_jabs.count
    else
      vacciantions = Vaccination.where(:member_id => child_member.id).where(opted:"true")
      vaccination_ids = vacciantions.map(&:id)
      jabs = Jab.in(:vaccination_id=>vaccination_ids).where(:est_due_time.lte=>Time.now)
      total_jabs = jabs.count
    end
    total_jabs
  end
  def api_dashboard(notification_msg=false,api_version=1)
    @api_version = api_version
    @families_list = self.user_families
    @child_ids = childern_ids
    @childern_list = childerns(@child_ids)
   
    if @families_list.blank?
      hash_data = {:identifier=>rand(999999999999999), :dismissable => false, :header=>"",:line1=>"",:line2=>"I need to create a family to facilitate coordination among all member",:image=>"manage-family-card-add-familly-image",:type=>"Family",:line3=>"", :line4=>"Don't wait, immerse in your parental journey",:button=>"Create Family"}
      hash_data.merge!({notific_msg:"Add family to easily coordinate among all the members"}) if notification_msg
      return  data = [hash_data]
    end
    name = {}
    pri = nil
    out = true
    aa = []
    @action_card_data = ActionCardList.where(:supported_api_version.lte => api_version).group_by { |card| card.name }
    set_priority(api_version).each do |k,v|
      if out
        v.each do |va|
           puts va
          status, data = eval(va+ " @child_ids")
          if status 
            out = false 
            name[va] =  data
            pri = k
          end
        end
      end
    end
    name.each do |k,v|
      aa << res(k,v,notification_msg)
    end
    aa.flatten
  end

  def api_recent_timeline(member_ids)
    recent_entries =[]
    data = {}
    recent_list = Timeline.where({'member_id' => { "$in" => member_ids} }).where(entry_from:"timeline").order_by("created_at DESC").limit(10).group_by{|p| p.member_id }.entries rescue []
    recent_list.each do |k,v|
      v.each do |timeline|
        data[:line1] = "Timeline"
        data[:line2] = timeline.name
        # data[:line3] = timeline.created_at.to_api_date1
        data[:line3] = timeline.created_at.to_date
        data[:line4] = ""# timeline.member.profile_image.photo_url_str("small") rescue ""
        data[:member_id] = timeline[:member_id]
        data[:title] = timeline.name
        recent_entries << data
        data ={}
      end
    end
      recent_entries
  end

  def api_recent_milestones(member_ids)
    recent_entries =[]
    data ={}
    recent_list = Milestone.where({'member_id' => { "$in" => member_ids} }).includes(:system_milestone).order_by("created_at DESC").limit(10).group_by{|p| p.member_id }.entries rescue []
    # recent_list = Milestone.order_by("created_at DESC").group_by{|p| p.member_id }.entries rescue []
    recent_list.each do |k,v|
      v.each do |milestone|
        system_milestone = milestone.system_milestone
        data[:line1] = "Milestones"
        data[:line2] = system_milestone.title rescue "" #v[0].description.blank? ?  (system_milestone).description : v[0].description
        data[:line3] = milestone.created_at.to_api_date1
        data[:line4] = "" #milestone.member.profile_image.photo_url_str("small") rescue ""
        data[:member_id] = milestone[:member_id]
        data[:title] = system_milestone.title rescue ""
        recent_entries << data
        data = {}
      end
    end
      recent_entries
  end


  def api_recent_health(member_ids=nil)
    recent_entries =[]
    data ={}
    convert_unit_val_list = self.metric_system
    units = Health.health_unit(convert_unit_val_list)
    units["bmi"] = ""
    recent_list = Health.where({'member_id' => { "$in" => member_ids} }).order_by("created_at DESC").limit(10)#.group_by{|p| p.member_id }.entries rescue []
    # recent_list.each do |k,v|
      recent_list.each do |health|
        health_attributes = health.attributes
        health_attributes["_id"] = nil #delete("_id")
        temp = Health.new(health_attributes)
        health.api_converted_data(convert_unit_val_list)
        data[:line1] = "Measurements"
        # change_parameter = Health::HEALTH_ENTRY_PARAM.keys.select{|health_key| health[health_key].present?}
        data[:line3] = health.created_at.to_api_date1
        data[:line4] = ""#health.member.profile_image.photo_url_str("small") rescue ""
        data[:member_id] = health["member_id"]
        data[:title] = ""
        str = []
        # health_attributes["_id"] = nil
        Health::HEALTH_ENTRY_PARAM.keys.each do |k1|
          str << Health::API_HEALTH_ENTRY_TITLE[k1] + ": " + temp[k1].to_s + " " +  units[k1] if temp[k1].present?
        end 

        data[:line2] = str.to_sentence.gsub(", and", " and").strip
        recent_entries << data
        data = {}
      end
    # end
      recent_entries
  end

  def api_recent_picture(member_ids)
    recent_entries =[]

    recent_list = Picture.where({'member_id' => { "$in" => member_ids} }).includes(:member).order_by("created_at DESC").limit(10).group_by{|p| p.member_id }.entries rescue []
    recent_list.each do |k,v|
      v[0][:entry_from] = "Pictures"
      recent_entries << v[0]
    end
      recent_entries
  end

  def api_recent_immunisation1(member_ids)
    recent_entries =[]
    data ={}
    count = 0
    recent_list = Vaccination.where({'member_id' => { "$in" => member_ids} }).where(opted: "true").order_by("created_at DESC").includes(:jabs)#.limit(10) #flatten.group_by{|p| p.member_id }.entries rescue []
     
    recent_list.each do | v|
    if count < 10
        all_vaccin_jabs= v.jabs.map(&:id)
         
        v.jabs.or(:due_on.ne => nil).or(:est_due_time => nil).order_by("id DESC").each do |jab|
        data[:line1] = "Immunisations"
        data[:line2] = "Jab"+ (all_vaccin_jabs.index(jab.id)+1).to_s + ": " + v.name + " - " + (jab.status == "Planned" ? "due on" : "administered on") + " " + jab.due_on.to_date1 rescue "Jab1: #{v.name}"
        data[:line3] =  (v.updated_at.to_api_date1)
        data[:line4] =  ""# v.member.profile_image.photo_url_str("small") rescue ""
        data[:member_id] = v[:member_id]
        data[:title] = ""
        recent_entries << data
        data = {}
        count = count+1

      end
    end

    end

      recent_entries[0..9]
  end
# refactored for IOS api response 
  def api_recent_immunisation(member_ids)
    recent_entries =[]
    data ={}
    recent_vaccin_ids = Vaccination.where({'member_id' => { "$in" => member_ids} }).where(opted: "true").order_by("created_at DESC").map(&:id)#.limit(10) #flatten.group_by{|p| p.member_id }.entries rescue []
    vaccin_jabs = Jab.in(vaccination_id:recent_vaccin_ids).or(:due_on.ne => nil).or(:est_due_time => nil).order_by("id DESC").limit(10) 
    vaccin_jabs.each do |jab|
      v = jab.vaccination
      all_vaccin_jabs = v.jab_ids
      data[:line1] = "Immunisations"
      data[:line2] = "Jab"+ (all_vaccin_jabs.index(jab.id)+1).to_s + ": " + v.name + " - " + (jab.status == "Planned" ? "due on" : "administered on") + " " + jab.due_on.to_date1 rescue "Jab1: #{v.name}"
      data[:line3] =  (v.updated_at.to_api_date1)
      data[:line4] =  ""# v.member.profile_image.photo_url_str("small") rescue ""
      data[:member_id] = v[:member_id]
      data[:title] = ""
      recent_entries << data
      data = {}
    end
    recent_entries
  end

# End of Home api



# for API
  def sent_invitations_data
    invitation = invitations.where(:sender_type => 'Member', :status => 'invited')
    data = []
    detail = {}
    invitation.each do |invit|
     detail["invitation_id"] = invit.id
     detail["role"] = invit.role
     detail["msg_to_sender"] = invit.message
     detail["family_name"] = invit.family.name rescue ""
     detail["family_uid"] = invit.family.family_uid rescue ""
     detail["family_id"] = invit.family_id
     sender = invit.sender_id.present? ? invit.sender : invit.member
     detail["sender_member_id"] = sender.id rescue ""
     detail["sender_email"] = sender.email rescue ""
     # detail["status"] = "Request sent to join"
     detail["status"] = "Join request sent"     
     detail["action"] ="You will be notified when your request is accepted"
     detail["sender_first_name"] = sender.first_name rescue ""
     detail["receiver_first_name"] = invit.member.first_name rescue ""
     detail["receiver_profile_image"] = (invit.member.profile_image.photo_url_str("small",api_version) rescue "")
     data << detail
     detail ={}
    end
    data
  end  

  def received_invitations_data(invitation_data=nil)
    data = []
    detail = {}
    (invitation_data || received_join_family_invitations).each do |invit|
      detail["invitation_id"] = invit.id
      detail["role"] = invit.role
      # detail["msg_to_sender"] = invit.message
      detail["family_name"] = invit.family.name rescue ""
      detail["family_uid"] = invit.family.family_uid rescue ""
      detail["family_id"] = invit.family_id
      receiver = invit.sender_id.present? ? invit.member : invit.family.owner
      sender = invit.sender_id.present? ? invit.sender : invit.member
      detail["receiver_member_id"] = receiver.id rescue ""
      detail["receiver_email"] = receiver.email rescue ""
      detail["sender_member_id"] = sender.id rescue ""
      detail["sender_profile_image"] = (sender.profile_image.photo_url_str("small") rescue "")
      detail["sender_email"] = sender.email rescue ""
      # detail["status"] = "Request sent to join"
      detail["status"] = "Join request received"
      # detail["action"] ="You will be notified when your request is accepted"
      detail["receiver_first_name"] = receiver.first_name rescue ""
      detail["sender_first_name"] = sender.first_name rescue ""
      data << detail
      detail ={}
    end
    data
  end  
 


end