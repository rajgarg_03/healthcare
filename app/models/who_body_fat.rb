class WhoBodyFat
 include Mongoid::Document
 include Mongoid::Timestamps

 field :month,               type: Integer
  field :L,      				     type: String
  field :M,          			   type: String
  field :S,            			 type: String
  field :P01,             	 type: Float
  field :P1,               	 type: Float
  field :P3,                 type: Float 
  field :P5,    				     type: Float
  field :P10,                type: Float
  field :P15,                type: Float
  field :P25,              	 type: Float
  field :P50,            		 type: Float
  field :P75,                type: Float
  field :P85,                type: Float
  field :P90,    				     type: Float
  field :P95,       			   type: Float
  field :P97,       			   type: Float
  field :P99,       			   type: Float
  field :P999,       			   type: Float
  field :week, type: Integer
  field :gender, type: Integer, default: 2
  
  index ({gender:1})
  index ({week:1})
  index ({month:1})
   

  GENDER = {"Boy" => 1, "Girl" => 2}


  def self.update_file(file_path_str,gender_str)

	File.open(file_path_str, "r").each_with_index do |line, index|
      next if index == 0

		  @obj=WhoBodyFat.new
      @obj.gender = GENDER[gender_str]
  		data = line.split(/\t/);
  		@obj.month=data[0]
  		@obj.L=data[1]
  		@obj.M=data[2]
  		@obj.S=data[3]
  		@obj.P01=data[4]
  		@obj.P1=data[5]
  		@obj.P3=data[6]
  		@obj.P5=data[7]
  		@obj.P10=data[8]
  		@obj.P15=data[9]
  		@obj.P25=data[10]
  		@obj.P50=data[11]
  		@obj.P75=data[12]
  		@obj.P85=data[13]
  		@obj.P90=data[14]
  		@obj.P95=data[15]
  		@obj.P97=data[16]
  		@obj.P99=data[17]
  		@obj.P999=data[18]
  		@obj.save
	end
 end

end