class PregnancyProgressBarManager
  include Mongoid::Document
  include Mongoid::Timestamps

  field :pregnancy_week_name, type: String
  field :description, type: String
  field :pregnancy_week, type: Integer
  field :pregnancy_max_duration, type: String
  field :week_left, type: String
  field :text_to_highlight, type: String
  field :action, type: String
  
  def title(pregnancy,pregnancy_week, current_user)
    if pregnancy_week >= 43
     self.week_left = "" # "0 week left"
     pregnancy.name + " is now overdue"
    elsif pregnancy.member_id.to_s == current_user.id.to_s
      "You are in #{pregnancy_week.ordinalize} week of your pregnancy"
    elsif pregnancy.member_id.present? && pregnancy.member_id.to_s != current_user.id.to_s
      mother_name = pregnancy.member.first_name rescue nil 
      if mother_name.blank?
        pregnancy.name + " is in #{pregnancy_week.ordinalize} week"
      else
      "#{mother_name} is in #{pregnancy_week.ordinalize} week of her pregnancy"
      end
    else
      pregnancy.name + " is in #{pregnancy_week.ordinalize} week"
    end
  end

  def self.populate_sheet_data
    PregnancyProgressBarManager.delete_all
    q1 = Roo::Excelx.new(Rails.root.to_path + "/lib/pregnancy_progress_bar.xlsx")
    q1.default_sheet = q1.sheets[0]
    q1.entries[1..-1].each do |entry|
      PregnancyProgressBarManager.create(
        pregnancy_week_name:entry[0],
        description: entry[1],
        pregnancy_week: entry[2],
        pregnancy_max_duration: 43,
        week_left: entry[3],
        text_to_highlight:(entry[5].strip rescue nil), 
        action: (entry[4].strip rescue nil)
      )
    end
  end
end

