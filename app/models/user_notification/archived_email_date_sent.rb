class UserNotification::ArchivedEmailDateSent
  include Mongoid::Document
  include Mongoid::Timestamps
   
  field :subject ,type:String
  field :message ,type:String   
  field :recipient_email , type:String
  field :member_id ,type:String
  field :notification_type ,type:String
  field :email_data_to_send_id ,type:String
  field :user_notification_id ,type:String
  belongs_to :user
  index({user_id:1})
  index({user_notification_id:1})
  

  def self.copy_data_from_email_data_to_send
    begin
      email_data = UserNotification::EmailDataToSend.where(:created_at.lte=>Date.today - 1.month) 
      num_docs = email_data.count
      group_size = 2
      num_groups = (Float(num_docs) / group_size).ceil
      puts "#{num_docs} documents found. #{num_groups} groups calculated."
      archived_ids = []
      1.upto(num_groups) do |group|
        new_notification_list = []
        email_data_group = email_data.paginate(:page => group, :per_page => group_size)
        email_data_group.each do |notification_email_data|
          user_notification = UserNotification.where(id:notification_email_data.user_notification_id.to_s).last
          if user_notification.present? && user_notification.email_status == "sent"
            archived_ids.push(notification_email_data.id.to_s)
            new_notitification = notification_email_data.attributes.except("_id")
            new_notitification["email_data_to_send_id"] = notification_email_data.id
            new_notification_list << new_notitification
          end
        end

        UserNotification::ArchivedEmailDateSent.collection.insert(new_notification_list)
         
      end
       email_data = UserNotification::EmailDataToSend.where(:id.in=> archived_ids).delete_all
    rescue Exception => e 
      Rails.logger.info "Error in copy_data_from_email_data_to_send"
      Rails.logger.info e.message
    end
  end

end