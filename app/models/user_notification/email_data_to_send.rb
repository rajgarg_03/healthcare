class UserNotification::EmailDataToSend
  include Mongoid::Document
  include Mongoid::Timestamps
   
  field :subject ,type:String
  field :message ,type:String   
  field :recipient_email , type:String
  field :user_notification_id ,type:String
  field :member_id ,type:String
  field :notification_type ,type:String
  belongs_to :user
  index({user_id:1})
  index({user_notification_id:1})
  
  def self.create_data_for_user_notification(user_notification,current_user,member_id,options={})
    begin
      UserNotification::EmailDataToSend.where(user_notification_id:user_notification.id,user_id:current_user.user_id,member_id:member_id)
      obj = UserNotification::EmailDataToSend.new

      email = BatchMailer.send("notification_email_for_identifier_#{user_notification.type}".to_sym,user_notification,current_user,member_id,options) rescue nil
      email = email || check_exist_email_for_identifier(user_notification,current_user,options)
      #raise "No email added for identifier-#{user_notification.type}" if email.blank?
      if email.present?
        if user_notification.type == "pointers"
          msg = user_notification.message.gsub("\\n","") rescue ""
          notification = Notification.where(title:msg, member_id:current_user.id, type:"pointers").last
          if notification.present?
           # raise "Email already existing with same subject for same member and user" if UserNotification::EmailDataToSend.where(subject:email.subject,user_id:current_user.user_id,member_id:member_id,notification_type:"pointers").present?
          end
        end
        obj.subject = email.subject
        obj.message = email.body.to_s
        obj.member_id = member_id
        obj.user_id = user_notification.user_id
        obj.user_notification_id = user_notification.id
        obj.notification_type = user_notification.type
        obj.recipient_email = current_user.email
        obj.save
      end
    rescue Exception => e 
      if options[:score] == 11
        UserMailer.notify_dev_for_error(e.message,e.backtrace,options,"Error inside create_data_for_user_notification").deliver
      end
      NotificationTrackingLogger.info "Error inside create_data_for_user_notification for notification #{user_notification.id rescue nil}....  #{e.message}"
      #Rails.logger.info "Error in create_data_for_user_notification  ......  #{e.message}"
    end
  end
  
  def self.check_exist_email_for_identifier(user_notification,current_user,options={})
    begin
      identifier =  user_notification.type
      if identifier.to_s == "17"
        subscription = options[:subscription]
        UserMailer.subscription_taken(current_user,user_notification.family_id,subscription)
      elsif identifier.to_s == "pointers"
        BatchMailer.pointer_email(current_user,user_notification,options)
      elsif identifier.to_s == "18"
        subscription = options[:subscription]
        UserMailer.subscription_taken(current_user,user_notification.family_id,subscription)
      elsif identifier.to_s == "19"
        system_timeline = options[:system_timeline]
        birthday_email_for = options[:birthday_email_for]
        age_in_month = options[:age_in_month]
        if system_timeline.present? || birthday_email_for == "monthly"
          if birthday_email_for == "monthly" && age_in_month < 12 && age_in_month > 0
              BatchMailer.month_birthday_notification_email_for_identifier_19(current_user.user,user_notification,options.merge({:user_notification=>user_notification}))
          elsif birthday_email_for == "annually" 
            if age_in_month > 11 && age_in_month/12 < 5
              birthday_mailer = system_timeline.name.downcase.to_s.split(" ").join("_") + "_email"
              UserMailer.send(birthday_mailer.to_sym,current_user.user,user_notification.member,options.merge({:user_notification=>user_notification})) rescue nil
            elsif age_in_month/12 > 5
              UserMailer.generic_birthday_email(current_user.user,user_notification.member,options.merge({:user_notification=>user_notification})) rescue nil
            end
          end
        else
          UserMailer.generic_birthday_email(current_user.user,user_notification.member,options) rescue nil
        end
      else
        nil
      end
    rescue Exception=> e 
      if options[:score] == 11
        UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside check_exist_email_for_identifier")
      end
      Rails.logger.info "Error inside check_exist_email_for_identifier"
      Rails.logger.info e.message
      nil
    end
  end

  def self.from_email(mail)
    obj = UserNotification::EmailDataToSend.new
    obj.subject = mail.subject
    obj.message = mail.body.to_s
    obj.recipient_email = mail.to
    obj.save
  end
end