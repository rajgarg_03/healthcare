class UserNotification::ClientResponse
  # include Mongoid::Document
  # include Mongoid::Timestamps
   
  # field :user_id ,           type:String
  # field :notification_type , type:String # 1,2,pointers..etc
  # field :notification_id ,   type:String
  # field :notification_destination ,   type:String
  # field :platform ,          type:String  #email/device
  # field :deeplink_data ,     type:Hash  #email/device
  
  def self.save_client_response(current_user,data,options={})
    begin
      user_notification = UserNotification.find(data[:notification_id]) rescue nil
      return true if user_notification.blank?
      user = User.where(id:user_notification.user_id).last
      user.update_subscription_popup_launch_count(0,options) rescue nil
      user_notification.client_response_status = true
      user_notification.client_response_platform << data[:platform] 
      user_notification.client_response_platform = user_notification.client_response_platform.uniq
      user_notification.client_response_type << data[:response_type] 
      user_notification.client_response_type = user_notification.client_response_type.uniq
      user_notification.save
      # deeplink_data = data[:deeplink_data] #|| user_notification.get_deeplink_data(current_user) rescue nil
      # ressponse_data = {user_id:current_user.user_id,
      #     notification_type:data[:notification_type],
      #     notification_id:data[:notification_id],
      #     notification_category:user_notification.category,
      #     platform:data[:platform],
      #     deeplink_data:deeplink_data}
      # UserNotification::ClientResponse.new(ressponse_data).save!  
    rescue Exception => e
      UserMailer.notify_dev_for_error(e.message,e.backtrace,data,"Error inside save_client_response").deliver
    end
    
  end

end