class SecondaryEmail
  include Mongoid::Document
  include Mongoid::Paranoia
  include Mongoid::Timestamps

  belongs_to :user

  field :email, :type => String
  field :status, :type => Integer
  field :user_id, :type => String
  field :confirmation_token, :type => String
  #embedded_in :user, :inverse_of => :secondary_emails

  EMAIL_STATUS = {
  	1 => "Verified",
  	0 => "UnVerifed"
  }
end
