#This should be renamed as Pointer
class ArticleRef
  include Mongoid::Document
  include Mongoid::Timestamps
  WillPaginate.per_page = 5
  field :name,             :type => String
  field :description,      :type => String
  field :reminder_count,   :type => Integer, :default => 0
  field :remind_me_on, :type => Date 
  field :shortlisted,     :type => Boolean, :default => false 
  field :effective_score,      :type => Float
  field :ref_start_date,       :type => Time
  field :ref_peak_relevence_date,       :type => Time
  field :ref_expire_date,       :type => Time
  field :reminder_factor,       :type => String
  field :N_factor,       :type => String
  field :ref_info,       :type => Array
  field :score , :type=>String
  field :checked, type: Boolean, :default => false
  field :created_at,       :type => Time, :default => DateTime.now
  field :updated_at,       :type => Time, :default => DateTime.now
  field :category, :type=> String
  field :record_id, :type=> String
  field :custome_id, :type=> Integer
  field :status, :type=> String, :default=>"Active"
  field :valid_for_role, :type=>String
  field :s_id, :type=>String # special id. combination of pointer custome id and member id
  field :notification_expiry_date, :type=> Time#,  :default => DateTime.now

  belongs_to :member
  belongs_to :sys_article_ref

  index ({custome_id:1})
  index ({sys_article_ref_id:1})
  index ({member_id:1})
  index ({category:1})
  index ({s_id:1})
  index ({effective_score:1})
  index ({status:1})
  index ({valid_for_role:1})
  
  validates :member_id, uniqueness: {scope: [ :custome_id]}
  scope :unread, where(:checked => false)
  
   def update_user_activity_status_v2_for_pointer(current_user=nil,options={})
      begin
        pointer = self
        member = pointer.member
        member.update_activity_status_v2(current_user.user_id,"Active")
        user = User.find(pointer.user_id)
        user.mark_user_activity_status_v2("Active")
      rescue Execption => e 
      end
   end

  def calculate_effective_score(auto_save=true)
    begin
      dp = self.ref_peak_relevence_date.to_date || (Date.today + 1.days)
      dc = Date.today
      ds = self.ref_start_date.to_date || Date.today
      d_exp = (self.ref_expire_date || Date.today).to_date  # if no expiry date set today date
      reminder_value = 1 + (self.reminder_factor.to_i * 0.5)
      relavence_factor = ( ((dp-dc).to_i.abs)/((dp-ds).to_i.abs) ).to_f  *  self.N_factor.to_f
      time_adjustment_factor = (dc < d_exp) ?  1/(1 + relavence_factor) : 0
      calculated_effective_score = self.score.to_f *  time_adjustment_factor * reminder_value.to_f
     rescue Exception=>e
       calculated_effective_score = 0.001
    end
    self.effective_score  = calculated_effective_score
    save if auto_save
    self
  end
  

  def self.assign_article_ref(child_member_id=nil,option={:sys_ref_list=>nil,:member_obj=>nil,:category=>"all"})
     begin
      member = option[:member_obj] || Member.find(child_member_id.to_s)
      pregnancy_record = Pregnancy.where(expected_member_id:member.id).last #|| Pregnancy.new
      # find Active System  pointers
      sys_atricle_refs = option[:sys_ref_list] || SysArticleRef.where(status:"Active")
      # Get child country code to assign link as per country 
      country_code = member.get_country_code rescue "other"
      if (SysArticleRefLink::CountryList & Member.sanitize_country_code(country_code)).present?
        country_code = country_code
      else
        country_code =  "other"
      end
      country_code = country_code || "other"
      # Save pointer which are eligible for the member
      system_scenario_detail_maping = {}
      if option[:sys_ref_list].present?
        System::Scenario::ScenarioDetails.where(status:"Active",:custome_id.in=>sys_atricle_refs.map(&:system_scenario_detail_custome_id)).map{|system_scenario| system_scenario_detail_maping[system_scenario.custome_id] = system_scenario }
      else
        System::Scenario::ScenarioDetails.where(status:"Active", :custome_id.in=>sys_atricle_refs.pluck(:system_scenario_detail_custome_id)).map{|system_scenario| system_scenario_detail_maping[system_scenario.custome_id] = system_scenario }
      end
      system_pointer_link_maping = SysArticleRefLink.group_by_system_pointer_custom_id(country_code)
      
      (sys_atricle_refs.no_timeout rescue sys_atricle_refs).each do |system_article|
        begin
          scenario_detail = system_scenario_detail_maping[system_article.system_scenario_detail_custome_id] || system_article.scenario_detail
          pointer_links = system_pointer_link_maping[system_article.custome_id]
          if pointer_links.present? && scenario_detail.present?
            if (system_article.pointer_type.downcase == "affiliate" rescue false)
              pointer_links.each do |link|
                link["article_provide"] = link["article_provide"].split(" | ").last 
              end
            end
            data = ArticleRef.save_for_member(system_article,member,country_code.downcase,pregnancy_record,scenario_detail,{:pointer_links=>pointer_links})
          end
        rescue Exception=>e
          puts e.message + " id :" + system_article.id + "member id:" + member.id.to_s + " custome_id: "+ system_article.custome_id.to_s
        end
      end
    rescue Exception=>e
      Rails.logger.info "Error--member_id=>#{member.id}---#{e.message}"
    end
  end
  
  # Get the jab having highest score for pointer
  def self.jab_with_high_score(jabs,system_article,options={})
    all_pointer_for_jab = []
    scenario_detail = options[:scenario_detail] || system_article.scenario_detail
    jabs.each do |jab|
      begin
        jab_date = (jab.due_on || jab.est_due_time)  
        administered_date = jab_date 
        if eval(scenario_detail.expire_date).to_date > Date.today && eval(scenario_detail.start_date).to_date <= Date.today 
          data_for_pointer = 
            { 
              :ref_start_date=>  eval(scenario_detail.start_date)  ,
              :ref_peak_relevence_date => eval(scenario_detail.peak_rel_date) ,
              :ref_expire_date => eval(scenario_detail.expire_date) ,
              :sys_article_ref => system_article.id.to_s,
              :N_factor => (scenario_detail.N_factor rescue 1),
              :score=> (scenario_detail.score rescue 1),
              :record_id => jab.id
            }
          pointer_obj = ArticleRef.new(data_for_pointer)
          all_pointer_for_jab << pointer_obj.calculate_effective_score(false)
        end
      rescue 
      end
    end
    jab_pointer_with_high_score = (all_pointer_for_jab.sort_by {|c| c[:effective_score]}).last
    
    jab = jab_pointer_with_high_score.present? ? Jab.where(id:jab_pointer_with_high_score[:record_id]).last : nil
  end
 

 def self.get_system_category(sys_article_category,scenario_detail)
  pointer_category = System::Scenario::ScenarioDetails.get_scenario_category(scenario_detail)
  pointer_category 
 end 


 # find record to check pointer validity
 # check start_date and expiry date for record to assign pointer
 # check critriea if present in system pointer and validate that critriea
 # if system pointer is valid after all check get pointer link and check it is not blank
 # return hash value of pointer data to save in DB
  def self.pointer_data_after_validate_condition(system_article,member,country_code,pregnancy_record,scenario_detail=nil,options={})
    scenario_detail = scenario_detail || system_article.scenario_detail
    pointer_category = ArticleRef.get_system_category(system_article,scenario_detail)
    scenario_category =  pointer_category
    pointer_data = {}
    pregnancy = pregnancy_record #|| Pregnancy.where(expected_member_id:member.id.to_s).first
    
    # check pointer has link to display
    pointer_links =  options[:pointer_links] || ArticleRef.ref_link_for_refrence(system_article,country_code)
     
    if pointer_links.present?
      scenario_data = nil
      scenario_data =  System::Scenario::ScenarioDetails.get_data_after_validate(scenario_detail,scenario_category,member,country_code,{:linked_tool=>system_article,:pregnancy=>pregnancy,:linked_tool_topic=>system_article.description})
      # Evaluate pointer name and Desciption
      if scenario_data.present?
        data_for_pointer =   {
          name:scenario_data[:scenario_title], 
          description:scenario_data[:topic], 
          ref_start_date:scenario_data[:scenario_start_date], 
          ref_peak_relevence_date:scenario_data[:scenario_peak_relevence_date], 
          ref_expire_date:scenario_data[:scenario_expire_date], 
          N_factor:scenario_data[:N_factor], 
          category:scenario_data[:category], 
          score:scenario_data[:score]
        }
        pointer_data = { 
          :sys_article_ref_id => system_article.id.to_s,
          :record_id => (eval(pointer_category).id rescue nil),
          :custome_id => system_article.custome_id,
          :status => system_article.status,
          :member_id => member.id.to_s,
          :ref_info => pointer_links,
          :new_added=> true,
          :valid_for_role=> system_article.valid_for_role,
          :notification_expiry_date => (Date.today + eval(system_article["notification_expiry_tech"].to_s) rescue nil),
          :s_id=> ((system_article.custome_id.to_s + "_" + member.id.to_s) rescue nil) 
        }.merge(data_for_pointer)
      end
    end
    pointer_data
  end


  def self.save_pointer_data_after_scenarion_assign(scenario_data,member,scenario_detail=nil,options={})
    begin
      country_code = member.get_country_code
      system_article = SysArticleRef.find(scenario_data[:linked_tool_id])
      pointer_data = {}
      # check pointer has link to display
      pointer_links =  options[:pointer_links] || ArticleRef.ref_link_for_refrence(system_article,country_code)
      if pointer_links.present?
          # Evaluate pointer name and Desciption
          notification_expiry_date = system_article["notification_expiry_tech"].to_s.present? ? (Date.today + eval(system_article["notification_expiry_tech"].to_s) rescue nil) : nil
        if scenario_data.present?
          data_for_pointer =   {
            name:scenario_data[:scenario_title], 
            description:scenario_data[:topic], 
            ref_start_date:scenario_data[:start_date], 
            ref_peak_relevence_date:scenario_data[:peak_relevence_date], 
            ref_expire_date:scenario_data[:expire_date], 
            N_factor:scenario_data[:N_factor], 
            category:scenario_data[:category], 
            score:scenario_data[:score]
          }
          pointer_data = { 
            :sys_article_ref_id => system_article.id.to_s,
            :record_id => (eval(pointer_category).id rescue nil),
            :custome_id => system_article.custome_id,
            :status => system_article.status,
            :member_id => member.id.to_s,
            :ref_info => pointer_links,
            :new_added=> true,
            :valid_for_role=> system_article.valid_for_role,
            :notification_expiry_date => notification_expiry_date,
            :s_id=> ((system_article.custome_id.to_s + "_" + member.id.to_s) rescue nil) 
          }.merge(data_for_pointer)
        end
        options[:validated_pointer_data] = pointer_data
        ArticleRef.save_for_member(system_article,member,country_code,pregnancy_record=nil,scenario_detail,options)
      end
      pointer_data
    rescue Exception=>e 
      Rails.logger.info "Error inside save_pointer_data_after_scenarion_assign ........... #{e.message}"
    end
  end


  def self.save_for_member(system_article,member,country_code,pregnancy_record,scenario_detail=nil,options={})
    # return true if system_article.criteria.nil?
    country_code = "india" if country_code == "in"
    # validated_pointer_data  from scenario ro from other validation method skip validation again
    pointer_data = options[:validated_pointer_data] || ArticleRef.pointer_data_after_validate_condition(system_article,member,country_code,pregnancy_record,scenario_detail,options)
    if pointer_data.present?
      pointer_obj = ArticleRef.new(pointer_data)
      pointer_obj.calculate_effective_score(false)
      existing_pointer = ArticleRef.where(s_id:pointer_obj.s_id).last
      if !existing_pointer
        pointer_obj.upsert
      else
        saved_pointer = existing_pointer #ArticleRef.where(custome_id:system_article.custome_id,member_id:pointer_obj.member_id.to_s).last
        pointer_data.merge!({:notification_expiry_date=> saved_pointer.notification_expiry_date, :updated_at=>Time.now,:new_added=>false, :effective_score=>pointer_obj.effective_score})
        saved_pointer.update_attributes(pointer_data)
        saved_pointer.calculate_effective_score
      end
    end
  end
  

  def self.ref_link_for_refrence(sys_ref_article,country_code=nil)
    begin
      data = []
      country_code = country_code.downcase rescue ""
      country_code = "india" if country_code == "in"
      country_code = "uk" if ((country_code.downcase == "gb") rescue false)
      code = SysArticleRefLink::CountryList.include?(country_code) ? country_code : "other"
      links = SysArticleRefLink.where(status:"Active", country:code,custome_id: sys_ref_article.custome_id.to_i).order_by("link_order asc")
      links.each do |link|
        data <<  SysArticleRefLink.filter_data(link,sys_ref_article)
      end
    rescue Exception => e
      data = []  
    end
    data
  end

  def self.update_jab_ref(jab)
    begin
      jab_date = (jab.due_on || jab.est_due_time) rescue nil
      administered_date = jab_date #+ 45.days rescue nil
      art_ref = ArticleRef.where(record_id:jab.id.to_s).first
      system_article = SysArticleRef.where(custome_id:art_ref.custome_id).first
      scenario_detail = system_article.scenario_detail
      art_ref.update_attributes({:ref_start_date=>  (eval(scenario_detail.start_date) ) ,
                                 :ref_peak_relevence_date => (eval(scenario_detail.peak_rel_date) ),
                              :ref_expire_date => (eval(scenario_detail.expire_date) ),
                              :notification_expiry_date => (eval(system_article["notification_expiry_tech"].to_s) )
                             })
    rescue Exception => e
     puts e.message 
    end
  end


  def self.update_prenatal_ref(prenatal_test)
    begin
    art_ref = ArticleRef.where(record_id:prenatal_test.id.to_s).first
    system_article = SysArticleRef.where(custome_id:art_ref.custome_id).first
    scenario_detail = system_article.scenario_detail
    art_ref.update_attributes({:ref_start_date=>  (eval(scenario_detail.start_date) ) ,
                               :ref_peak_relevence_date => (eval(scenario_detail.peak_rel_date) ),
                               :ref_expire_date => (eval(scenario_detail.expire_date) ),
                               :notification_expiry_date => (eval(system_article["notification_expiry_tech"].to_s) )
                             })
    rescue Exception => e
     puts e.message 
    end
  end
  
  #Used as utility for rake job
  def self.update_links(custome_id,country_code=nil)
    begin
      members = ArticleRef.where(custome_id:custome_id).map(&:member).uniq
      system_article = SysArticleRef.where(custome_id:custome_id).last
      members.each do |member|
        member_country_code = User.where(member_id:member.family_mother_father.first.to_s).first.country_code rescue "other"
        member_country_code = member_country_code || "other"
        if ((country_code.downcase == member_country_code.downcase) rescue false)
          ref_link_data = ArticleRef.ref_link_for_refrence(system_article,country_code)
          article_ref = ArticleRef.where(member_id:member.id.to_s,custome_id:custome_id).first
          article_ref.ref_info =  ref_link_data
          article_ref.save
        end
      end
    rescue Exception=>e
      puts e.message
      Rails.logger.info e.message
    end
  end
  def self.valid_pointers
    self.not_in(effective_score:[nil,0,"0"]).where(status:"Active")
  end
  
  def self.get_recommended_pointer(current_user,member_id,tool="",options={:list_all=>false})
    begin
    data = get_pointers_list(current_user,[member_id])
    data = data.where(category:tool) if tool.present?
    data = data.order_by("effective_score desc")
    if options[:list_all].blank? || options[:list_all] != true
      data = data.limit(1).first
    end
    rescue Exception=>e 
      Rails.logger.info e.message
      data = nil
    end
    data
  end
  
  # fetch all pointers valid for member
  def self.get_pointers_list(current_user,childern_ids)
    pointer_list = ArticleRef.valid_pointers.in(member_id:childern_ids).or({:shortlisted=>true,:remind_me_on.lte=>Time.zone.now.to_date},{:remind_me_on=> nil,:ref_expire_date.gte=>Time.zone.now.to_date},{:remind_me_on.lte=>Time.zone.now.to_date})
    if current_user.has_mother_role?
      all_article_refs = pointer_list 
    else
      all_article_refs = pointer_list.not_in(valid_for_role:"mother")
    end
    all_article_refs = all_article_refs
  end

  # fetch all pointers valid for member
  def self.get_pointers_link_list(current_user,childern_ids,link_limit=nil,options={})
    pointers_widget = []
    pointers = ArticleRef.get_pointers_list(current_user,childern_ids).order_by("effective_score desc")
    if link_limit.present?
      pointers = pointers.limit(6)
    end
    if options[:only_affiliated_pointer] == true
      affiliate_pointers_custom_ids = SysArticleRef.where(:custome_id.in=>pointers.map(&:custome_id)).where(pointer_type:"Affiliate").pluck(:custome_id)
      pointers = pointers.where(:custome_id.in=>affiliate_pointers_custom_ids)
    end
    system_pointer_group_by_id = SysArticleRef.where(:id.in=>pointers.pluck(:sys_article_ref_id)).group_by{|system_pointer| system_pointer.id.to_s}
    
    pointers.each do |pointer|
      system_pointer = system_pointer_group_by_id[pointer.sys_article_ref_id.to_s].last rescue nil
      # SysArticleRef.where(id:pointer.sys_article_ref_id).last
      pointer[:ref_info].each do |pointer_link|
        pointer_link["pointer_id"] = pointer.id
        if (system_pointer.pointer_type.downcase == "affiliate" rescue false)
          pointer_link["article_provide"] = pointer_link["article_provide"].split(" | ").last
        end
        pointers_widget << pointer_link
      end
    end
    # Remove duplicated url links
    pointers_widget.uniq!{|a| a["url"]}
    pointers_widget[0..(link_limit.to_i - 1)]
  end

  def get_pointer_history(day_count=30,group_by="created_at")
    pointer = ArticleRef.last
    conditions = { group_by => {"$gte"=>selected_date.to_time, "$lte"=>selected_date.to_time + 1.day}  }
    pointer_manager_id = pointer.sys_article_ref.id
    hash_data =  ArticleRef.collection.aggregate([
        { "$match" => { :sys_article_ref_id =>pointer_manager_id}, :created_at => {"$gte"=>Time.now - day_count.days } },
         {"$group" => {
          "_id" =>  {"date":{"$dateToString":{ format: "%Y-%m-%d", "date": "$#{group_by}" }}, "type"=>"$type" }   ,
           "count" => {"$sum" => 1} , 
           "sent": {
                "$sum": {
                    "$cond": [ { "$eq": [ "$status",  "sent" ] }, 1, 0]
                }
            },

            "pending": {
                "$sum": {
                    "$cond": [ { "$eq": [ "$status",  "pending" ] }, 1, 0]
                }
            }
          }}   
       ])
    data = []
    hash_data.each do |fetched_data|
      date = fetched_data["_id"]["date"]
      info = {"total_count"=>fetched_data["count"],"sent"=>fetched_data["sent"],"pending"=>fetched_data["pending"]}
      data << {date=>info}
    end
   data
  end

  # get all pointer which are not being sent and have valid to send notification after assign
  def self.pointer_list_for_notification(sent_pointer_s_ids=[],childern_ids=[])
    ArticleRef.in(member_id:childern_ids).not_in(s_id:sent_pointer_s_ids).order_by("effective_score DESC")
  end


  
   # ArticleRef.assigned_pointer_group_by_system_pointer_id
   def self.assigned_pointer_group_by_system_pointer_id(pointer_manager_id=nil,options={})
    if options[:group_by].present?
      group_by_val = "$#{options[:group_by]}"
    else
      group_by_val = "$sys_article_ref_id"
    end
    if pointer_manager_id.present? 
      hash_data =  ArticleRef.collection.aggregate([
        { "$match" => { :sys_article_ref_id =>pointer_manager_id,  :created_at => {"$gte"=>Time.now - 30.days }} },
      {"$group" => {
          "_id" => group_by_val,
           "count" => {"$sum" => 1} 
        }}  
       ])

     else
      hash_data =  ArticleRef.collection.aggregate([
        { "$match" => { :created_at => {"$gte"=>Time.now - 30.days }} },
      {"$group" => {
          "_id" => group_by_val,
           "count" => {"$sum" => 1} 
        }}  
       ])

     end
    data = {}
    hash_data.each{|result| data[result["_id"] ] = result["count"] }
    data
  end


  # ArticleRef.pointer_links_group_by_system_pointer_custom_id
   def self.pointer_links_group_by_system_pointer_custom_id(pointer_manager_id=nil)
      hash_data =  SysArticleRefLink.collection.aggregate([
      {"$group" => {
          "_id" => "$custome_id",
           "count" => {"$sum" => 1},
           "active": {
                "$sum": {
                    "$cond": [ { "$eq": [ "$status",  "Active" ] }, 1, 0]
                }
            }
 
        }}  
       ])

    data = {}
    hash_data.each{|result| data[result["_id"] ] = {"total"=>result["count"],"active"=>result["active"]} }
    data
  end  

end