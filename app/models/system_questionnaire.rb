class SystemQuestionnaire
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :title, type: String
  field :description, type: String
  has_many :system_questions, :dependent => :destroy
  belongs_to  :member
end
