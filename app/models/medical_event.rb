class MedicalEvent 
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :name, type: String
  field :desc, type: String
  field :opted, type: String 
  field :category, type: String
  field :status, type: String , :default => "Planned"
  field :est_due_time, type: Time
  field :start_time, type: Time
  field :end_time, type: Time
  field :note, type: String
  field :fullday, type: Boolean, :default => true
  field :component, type: Array
  field :event_index, type: Integer
  has_many :timelines, :as=> :timelined,:dependent => :destroy
  
  has_many :pictures, :as=> :imageable,:dependent => :destroy
  belongs_to :member
  validates :category, presence: true
  validates :member_id, presence: true
  
  index ({member_id:1})
  index ({name:1})
  
  def to_api_json(api_version=1)
    data = self.attributes
    data["start_time"] = start_time.utc.to_formatted_s(:time) rescue nil
    data["end_time"] = end_time.utc.to_formatted_s(:time) rescue nil
    data[:component] = component.sort
    data["pictures"] = medical_event_pictures(api_version)
    data
  end
  
  def save_medical_event_timeline(current_user)
  end


  def delete_medical_event
    medical_event = self
    medical_event.timelines.delete_all
    medical_event.pictures.delete_all
    ArticleRef.where(record_id:medical_event.id.to_s).delete_all
    medical_event.delete
  end  

  def mother_name
    begin
      pregnancy = Pregnancy.where(expected_member_id:member_id).first
      asso_mother = Member.where(id:pregnancy.member_id).first
      asso_mother.first_name.present? ? asso_mother.first_name : pregnancy.name
    rescue
      Member.find(self.member_id).first_name rescue ""
    end
  end
  def medical_event_pictures(api_version=1)
    data = []
    
    self.pictures.each do |picture|
      data << {"picture"=> picture.attributes.merge(url_small: picture.complete_image_url(:small,api_version),url_large: picture.complete_image_url(:medium,api_version), url_original: picture.complete_image_url(:original,api_version), type: 'non_default',aws_small_url: picture.aws_url(:small,api_version),aws_original_url: picture.aws_url(:original,api_version))}
    end
    data
  end

end