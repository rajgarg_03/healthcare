class System::GpsocImmunisationJab
  include Mongoid::Document 
  include Mongoid::Timestamps 
  field :custom_id,                type: Integer
  field :term,                     type: String
  field :tpp_term,                 type: String
  field :country_code,             type: String, default:"uk"
  field :snomed_code,              type: String
  field :read_v2_code,             type: String
  field :ctv3_code,                type: String
  field :snomed_ct_description_id, type: String
  field :snomed_ct_term, type: String
  field :status,                   type: String # Active/Draft/Inactive
  field :system_jab_id,            type: String # Nurturey System Jab ID for mapping 
  field :combined_vaccine,         type: Boolean ,:default=>false# to mark combine vaccine

  validates_presence_of :custom_id
  
  index({custom_id:1}) 
  index({snomed_ct_description_id:1})  
  index({system_jab_id:1})  

  def assign_custom_id
    jab  = self
    jab[:custom_id] = ::System::GpsocImmunisationJab.max(:custom_id).to_i + 1
    jab
  end

  def system_jab
    gpsoc_jab = self
    SystemJab.where(id:gpsoc_jab.system_jab_id).last.get_jab_detail rescue nil
  end

  # Used for admin panel
   def update_record(params)
    begin
      gpsoc_immunisation_jab = self
      jab_params = params["SystemGpsocImmunisationJab".underscore]
       
      if gpsoc_immunisation_jab.update_attributes(jab_params)
        status = true
      else
        status = false
      end
    rescue Exception => e
      Rails.logger.info e.message
      status = false 
    end
    [gpsoc_immunisation_jab,status]
  end

  # data = {:system_gpsoc_immunisation_jab=>{term:"",snomed_code: "",read_v2_code:"",ctv3_code:"",status:"Active"}}
  # System::GpsocImmunisationJab.create_record(data)
  def self.create_record(data)
    begin
      jab_params = data["SystemGpsocImmunisationJab".underscore]
      gpsoc_immunisation_jab = ::System::GpsocImmunisationJab.new(jab_params)
      gpsoc_immunisation_jab = gpsoc_immunisation_jab.assign_custom_id
      gpsoc_immunisation_jab.save!
      status = true
    rescue Exception => e
      Rails.logger.info e.message
      status = false 
    end
    [gpsoc_immunisation_jab,status]
  end

  def self.duplicate_record(data)
    record_to_be_duplicated = System::GpsocImmunisationJab.find(data[:record_id]).attributes
    record_to_be_duplicated.delete("_id")
    record_to_be_duplicated.delete("created_at")
    record = System::GpsocImmunisationJab.new(record_to_be_duplicated)
    record.assign_custom_id

    record.save
    [record,nil]
  end


  def self.duplicate_all_records(params)
    begin
      raise "Already added data for selected country" if System::GpsocImmunisationJab.where(:country_code.in=> Member.sanitize_country_code(params[:destination_country_code])).present?
      sanitize_country_code = Member.sanitize_country_code(params[:source_country_code])
      system_vaccine_list = System::GpsocImmunisationJab.where(:country_code.in=>sanitize_country_code )
      system_vaccine_list.each do |system_vaccine|
        vaccine_params = {:record_id=>system_vaccine.id}
        # vaccine_params["country_code"] = params[:destination_country_code]
        system_vaccine_obj = System::GpsocImmunisationJab.duplicate_record(vaccine_params).first
        system_vaccine_obj.country_code = params[:destination_country_code]
        system_vaccine_obj.save
      end
      status = true
      # update_member_system_vaccine_for_added_country(params[:destination_country_code])
      message = ""
    rescue Exception=> e 

      Rails.logger.info "Error in System::GpsocImmunisationJab.duplicate_all_records"
      Rails.logger.info e.message
      status = false
      message = e.message
    end
    [status,message]
  end

  def self.delete_record(record_id)
    begin
      gpsoc_immunisation_jab = System::GpsocImmunisationJab.find(record_id)
      gpsoc_immunisation_jab.delete
    rescue Exception => e
      Rails.logger.info e.message
    end
  end

end