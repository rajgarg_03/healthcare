module NookSetting
  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    def isNookEnabled?(current_user,api_version=6,options={})
      begin
        status = true
        user = current_user.user
        return true if (user.is_internal? || user.is_beta_user?)
        system_setting = ::System::Settings.where(name:'nook').last
        return false if system_setting.blank? || system_setting.status != "Active"
        settings = system_setting.setting_values.with_indifferent_access
        user_device = current_user.devices.last
        device_platform = user_device.platform

        return false if settings["#{device_platform}_app_version"].downcase == "na" 
        return false unless user_device.check_device_is_upto_date?(device_platform,user_device.app_version, settings["ios_app_version"], settings["android_app_version"],options)
        
        if settings["country_list"].present?
          return false if (settings["country_list"] & Member.sanitize_country_code(user.country_code)).blank?
        end
      rescue Exception => e
        Rails.logger.info "Error inside show_nook_widget .............#{e.message}"
        status = false
      end
      status
    end
  end
end