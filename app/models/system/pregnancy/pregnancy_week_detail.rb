class System::Pregnancy::PregnancyWeekDetail
  include Mongoid::Document
  include Mongoid::Timestamps

  field :pregnancy_week,                                type: Integer
  field :description,                                   type: String
  field :your_baby_description,                         type: String
  field :your_body_description,                         type: String
  field :your_body_symptom,                             type: Array
  field :your_and_your_partner,                         type: String
  field :remember_info,                                 type: String
  field :test_of_the_week,                              type: Hash
  field :check_list,                                    type: String
  field :scan_image,                                    type: Boolean
  field :status,                                        type: String
  field :country_code,                                  type: String
  # field :nhs_syndicate_content_id,                      type:String
  
  index({status:1})
  index({pregnancy_week:1})
  index({description:1})
  index({your_baby_description:1})
  index({your_body_symptom:1})
  # index({nhs_syndicate_content_id:1})
  index({check_list:1})
  index({country_code:1})
  
  def nhs_syndicate_content
    pregnancy_week_detail = self 
    ::Nhs::SyndicatedContentLinkTool.nhs_content_list_linked_with_tool(pregnancy_week_detail).last#.syndicated_format_data(nil,nil)
  end
  def formatted_data(options={})
    pregnancy_week_detail = self
    data = {}
   
    data["description"] =  pregnancy_week_detail.description
    data["pregnancy_week"] = pregnancy_week_detail.pregnancy_week

    data["your_body"] = {:title=> "Your body",:description=>pregnancy_week_detail.your_body_description,
                         :symptoms=>eval(pregnancy_week_detail.your_body_symptom)}
    data["save_scan_image"] = pregnancy_week_detail.scan_image
    data["test_of_the_week"] = pregnancy_week_detail.test_of_the_week
    data["your_baby"] = {:title=>"Your baby",:description=>pregnancy_week_detail.your_baby_description}
    data["nurturey_nuggets"] = {:title=>"Nuturey nuggets",:description=>"",:nuggets=>[{:title=>'You and your partner',:description=>pregnancy_week_detail.your_and_your_partner},
                              {:title=>'Remember this!',:description=>pregnancy_week_detail.remember_info},
                              {:title=>'Check list for this week',:description=>pregnancy_week_detail.check_list}
                             ]}
    data["section_order"] = ['your_baby','save_scan_image','your_body','test_of_the_week','nurturey_nuggets']
    if options[:show_nhs_syndicated_content] != false &&  pregnancy_week_detail.nhs_syndicate_content_id.present?
      syndicated_content = pregnancy_week_detail.nhs_syndicate_content
      nhs_syndicate_content_attr = pregnancy_week_detail.nhs_syndicate_content.syndicated_format_data(nil,nil,options)
      content = nhs_syndicate_content_attr.delete("content")
      options[:api_link] = syndicated_content.api_link
      options[:web_url] = syndicated_content.web_url
      formated_content = ::Nhs::SyndicatedContent.format_content(content,options) 
      nhs_syndicate_content_attr["content"] = formated_content
      data["nhs_syndicate_content"] = nhs_syndicate_content_attr
    end
  data
  end

  def update_record(data)
    pregnancy_week_detail = self
    request_data = data["system_pregnancy_pregnancy_week_detail"]
    request_data["test_of_the_week"] = eval(request_data["test_of_the_week"])
    status  = pregnancy_week_detail.update_attributes(request_data)
    [pregnancy_week_detail,status]
  end

  def self.create_record(data)
    request_data = data["system_pregnancy_pregnancy_week_detail"]
    request_data["test_of_the_week"] = eval(request_data["test_of_the_week"])
    pregnancy_week_detail = System::Pregnancy::PregnancyWeekDetail.new(request_data)
    status = pregnancy_week_detail.save
    [pregnancy_week_detail,status]

  end
  # clone for country
  def self.duplicate_all_records(params)
    begin
      raise "Already added data for selected country" if SystemVaccin.where(:country_code.in=> Member.sanitize_country_code(params[:destination_country_code])).present?
      sanitize_country_code = Member.sanitize_country_code(params[:source_country_code])
      pregnancy_week_details = ::System::Pregnancy::PregnancyWeekDetail.where(:country_code.in=>sanitize_country_code)
      pregnancy_week_details.each do |pregnancy_week_detail|
      pregnancy_params = {:record_id=>pregnancy_week_detail.id}
        pregnancy_week_detail_obj,msg = ::System::Pregnancy::PregnancyWeekDetail.duplicate_record(pregnancy_params)
        pregnancy_week_detail_obj.country_code = params[:destination_country_code]
        pregnancy_week_detail_obj.save
      end
      status = true
      # update_member_system_milestone_for_added_country(params[:destination_country_code])
      message = ""
    rescue Exception=> e 

      Rails.logger.info "Error in SystemVaccin.duplicate_all_records"
      Rails.logger.info e.message
      status = false
      message = e.message
    end
    [status,message]
  end
  


  def self.duplicate_record(params)
    record_to_be_duplicated = System::Pregnancy::PregnancyWeekDetail.find(params[:record_id]).attributes
    record_to_be_duplicated.delete("_id")
    record_to_be_duplicated.delete("created_at")
    record = System::Pregnancy::PregnancyWeekDetail.new(record_to_be_duplicated)
    record.save
    [record,nil]
  end

  def self.delete_record(record_id)
    begin
      pregnancy_week_detail = System::Pregnancy::PregnancyWeekDetail.find(record_id)
      pregnancy_week_detail.delete
    rescue Exception => e
      Rails.logger.info e.message
    end
  end

  def self.update_member_pregnancy_for_added_country(country_code)
    country_code = country_code.upcase
    Milestone.distinct(:member_id).each do |child_id|
      begin
        child = ChildMember.find(child_id)
        member = child
        member_country_code = User.where(member_id:child.family_mother_father.first.to_s).first.country_code.upcase #rescue "GB"
        if member_country_code == country_code
          Milstone.where(member_id:child_id.to_s).each do |milestone|
            system_milestone = SystemMilestone.find(milestone.system_milestone_id.to_s)
            existing_system_milestone_for_cloned_country = SystemMilestone.where(title:system_milestone.title).in_country_code({:country_code=>member_country_code}).last
            if existing_system_milestone_for_cloned_country.present?
              milestone.system_milestone_id = existing_system_milestone_for_cloned_country.id
              milestone.save
            end
          end
        end
      rescue Exception => e 
        puts e.message
        puts "Inside update_member_system_milestone_for_added_country"
        Rails.logger.info "Inside update_member_system_milestone_for_added_country"
        Rails.logger.info e.message
      end
    end
  end

end
  