class System::Faq::AnswerFeedback
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :helpful, type: String  #nill, yes, no
  belongs_to :question, class_name: "System::Faq::Question"
  belongs_to :member, class_name: "Member"

  class << self
    def create_feedback(params, current_user)
      validate_helful(params[:helpful_to_user])
      if params[:question_id].present?
        answer_feedback = System::Faq::AnswerFeedback.where(member_id: current_user.id, question_id: params[:question_id])
        answer_feedback.delete_all if answer_feedback.count > 0
        create!(helpful: params[:helpful_to_user].downcase, question_id: params[:question_id], member_id: current_user.id)
      else
        raise 'Please enter question id'
      end
    end

    def validate_helful(helpful_to_user)
      raise 'Please enter valid value for helpul' unless ['', 'yes', 'no'].include?(helpful_to_user.downcase)
    end

  end

end
