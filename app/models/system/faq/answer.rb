class System::Faq::Answer
  include Mongoid::Document  
  include Mongoid::Timestamps
  
  field :title, type: String
  # field :category, type: String
  
  belongs_to :question, :class_name=>"System::Faq::Question"
   
  validates :title, presence: true
  index({question_id:1})
end