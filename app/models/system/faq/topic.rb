class System::Faq::Topic
  include Mongoid::Document  
  include Mongoid::Timestamps
  include System::Faq::Utility
  field :title, type: String
  field :description, type:String 
  field :topic_for, type:String #child/pregnancy/all
  field :cross_references, type:Array #other topic ids
  field :category, type:String #milestone,tooth_chart..etc
  field :status, type:String ,:default=>"Draft"#"Active/Draft"
  field :sort_order, type:Integer #use to keep record in sort order. eg. 1,2,3.."
    
  has_many :sub_topics, :class_name=>"System::Faq::SubTopic", :dependent => :destroy
  validates :title, presence: true

  index({sort_order:1})
  index({status:1})
  index({category:1})
  index({topic_for:1})
  
  
  def self.filtered_record(record_id)
    self
  end

  def self.sort_by_sort_order
    order_by("sort_order asc")
  end

  def self.topic_list_response(current_user,params,options={})
    validate_params(params)
    subtopic = System::Faq::SubTopic.where(id: params[:subtopic_id]).includes(:questions, :topic).first
    questions = subtopic.questions.includes(:answers, :answer_feedback).sort_by_sort_order rescue []
    data = {}
    if subtopic
      topic = subtopic.topic
      data[:topic] = topic.title
      data[:topic_id] = topic.id
      data[:subtopic] = subtopic.title
      data[:subtopic_id] = subtopic.id
      data[:questions] = questions.map{|question| question.get_json(current_user)}
    end
    data
  end

  def self.validate_params(params)
    raise 'Please check params' unless (params[:subtopic_id].present?)
  end

end
