class System::Faq::SubTopic
  include Mongoid::Document  
  include Mongoid::Timestamps
  include System::Faq::Utility
  
  field :title, type: String
  field :status, type:String ,:default=>"Draft"#"Active/Draft"
  field :sort_order, type:Integer
  
  belongs_to :topic ,:class_name=>"System::Faq::Topic"
  has_many :questions , :class_name =>"System::Faq::Question",:dependent => :destroy
  validates :title, presence: true
  
  index({sort_order:1})
  index({status:1})
  index({topic_id_id:1})

  #  Used in admin panel
  def self.filtered_record(record_id)
    klass = self
    sub_topic = klass.find(record_id)
    where(:topic_id=>sub_topic.topic_id).sort_by_sort_order
  end

  def self.sort_by_sort_order
    order_by("sort_order asc")
  end

  
end