class System::Faq::Question
  include Mongoid::Document  
  include Mongoid::Timestamps
  include System::Faq::Utility
  
  field :title, type: String
  field :category, type: String # radio,list,label
  field :status, type:String ,:default=>"Draft" #"Active/Draft"
  field :sort_order, type:Integer
  field :custom_id, type:Integer
  field :notification_expiry, type:String

  index({custom_id:1})
  index({sub_topic_id_id:1})


  after_create :assign_custom_id
 
  belongs_to :sub_topic ,:class_name=> "System::Faq::SubTopic"
  has_one :answer ,:class_name=> "System::Faq::Answer",:dependent => :destroy
  has_many :answer_feedbacks, class_name: 'System::Faq::AnswerFeedback', dependent: :destroy
  validates :title, presence: true
  
  def topic 
    self.title
  end
  def scenario_detail
    faq_question = self
    System::Scenario::ScenarioLinkedToTool.where(tool_class:faq_question.class.to_s,linked_tool_id:faq_question.id.to_s).last.scenario_detail rescue nil

  end

  def update_scenario(system_scenario_custom_id)
    faq_question = self
    System::Scenario::ScenarioLinkedToTool.add_to_list(faq_question,system_scenario_custom_id)
  end

  def assign_custom_id
    question  = self
    question[:custom_id] = System::Faq::Question.max(:custom_id).to_i + 1
    question.save
  end

  def get_json(current_user,options={})
    question = self
    data = {}
    data[:id] = question.id
    data[:question] = question.title
    data[:answers] = question.answer.title.split(";").map(&:strip) rescue []
    user_feedback = System::Faq::AnswerFeedback.where(:question_id=>question.id, :member_id=> current_user.id).last.helpful rescue nil
    data[:helpful_to_user] = user_feedback
    data
  end
  
  def self.sort_by_sort_order
    order_by("sort_order asc")
  end
 # to be removed 
  def self.create_test_data(sub_topic_id=nil)
    sub_topic_id = sub_topic_id || "5bc4365e8cd03d8d6600001e"
    sort_order = 3
    SysArticleRef.all.each do |system_article|
      title =  system_article.description
      faq_question = System::Faq::Question.new(sub_topic_id:sub_topic_id,sort_order:sort_order,title:title)
      faq_question.save
      System::Scenario::ScenarioLinkedToTool.add_to_list(faq_question,system_article.system_scenario_detail_custome_id)
    end

  end

  # Used for Rake task
  def self.update_existig_answer_for_question
    System::Faq::Question.sort_by_sort_order.each do |question|
      begin
        @question = question
        answer_titles = System::Faq::Answer.where(question_id:question.id).pluck(:title).map(&:to_s)
        answer_title = answer_titles.join(";").gsub(";;",";").split(";").map(&:strip).join(";")
        new_answer = System::Faq::Answer.new(question_id:question.id,title:answer_title)
        if new_answer.valid?
          System::Faq::Answer.where(question_id:question.id).delete_all
        end
        new_answer.save!

      rescue Exception=> e 
        options = {question:@question}
        UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside update_existig_answer_for_question")
      end
    end 
  end

  def self.assign_custom_id_to_question
    System::Faq::Question.each_with_index do |question,index|
      question[:custom_id] = index + 1
      question.save
    end
  end
  
  #   Used in admin panel
  def self.filtered_record(record_id)
    klass = self
    question = klass.find(record_id)
    where(:sub_topic_id=>question.sub_topic_id)
  end
  
  def self.get_assigned_question_to_member(member_ids,current_user,api_version=5,options={})
    member_ids = member_ids || []
    country_code = current_user.user.country_code.to_s
    # faq_tool = Nutrient.adult_tool_valid_for_launch(current_user,api_version).where(identifier:"FAQ").last
    # return [] if (faq_tool.blank? || Member.sanitize_country_code(country_code).include?("uk"))
    faq_notification_enabled = System::Settings.faqNotificationEnabled?(current_user,country_code,options)
    return [] if !faq_notification_enabled

    system_faq_questions = ::Child::Scenario.where( :status=> "Active",:member_id.in=>member_ids).where(:linked_tool_class=>"System::Faq::Question").order_by("effective_score desc").limit(10)
    system_faq_question_ids = system_faq_questions.pluck(:linked_tool_id).uniq.compact.take(2)

    if system_faq_question_ids.blank?
      system_faq_linked_to_tool_ids = system_faq_questions.pluck(:scenario_linked_to_tool_id)
      system_faq_question_ids = System::Scenario::ScenarioLinkedToTool.in(:id=>system_faq_linked_to_tool_ids).pluck(:linked_tool_id).compact
    end

    data = []
    questions = System::Faq::Question.where(:id.in=>system_faq_question_ids.map(&:to_s))
    
    subtopics = System::Faq::SubTopic.where(:id.in=>questions.map(&:sub_topic_id))
    topic_ids = []
    map_subtopic_with_id = {}
    subtopics.each do |subtopic| 
      map_subtopic_with_id[subtopic.id]=subtopic
      topic_ids << subtopic.topic_id
    end
    topics = System::Faq::Topic.where(:id.in=>topic_ids)
    map_topic_with_id = {}
    topics.map{|topic| map_topic_with_id[topic.id] = topic}
     
    questions.each do |question|
      begin
        question_detail = question.get_json(current_user)
        subtopic = map_subtopic_with_id[question.sub_topic_id]
        topic = map_topic_with_id[subtopic["topic_id"]]
        data << {topic:topic, sub_topic:subtopic,question_detail:question_detail}
      rescue Exception=>e 
        Rails.logger.info e.message
        puts e.message
      end
    end 
    data
  end 
end