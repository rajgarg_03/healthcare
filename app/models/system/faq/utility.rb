module System::Faq::Utility

  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods
    def update_sorting_order(action_type,current_sorting_order,current_user,api_version=5,options={})
      record_id = options[:id]
      klass = self
      if action_type == "delete"
        current_sorting_order = current_sorting_order
        klass.filtered_record(record_id).where(:id.nin=>[record_id],:sort_order.gt=>current_sorting_order).each do |record|
          record.update_attribute(:sort_order,(record.sort_order - 1))
        end
       
      elsif action_type == "add"
        if klass.filtered_record(record_id).where(:sort_order=>current_sorting_order).count > 1
          klass.filtered_record(record_id).where(:id.nin=>[record_id], :sort_order.gte=>current_sorting_order).each do |record|
            record.update_attribute(:sort_order,(record.sort_order + 1))
          end
        end
      elsif action_type == "update"
        previous_sort_order = options[:previous_sort_order]
        if current_sorting_order != previous_sort_order
          if previous_sort_order < current_sorting_order
            klass.filtered_record(record_id).where(:id.nin=>[record_id],:sort_order=>{"$gte"=>previous_sort_order,"$lte"=>current_sorting_order}).each do |record|
              record.update_attribute(:sort_order,(record.sort_order - 1))
            end
          else
            klass.filtered_record(record_id).where(:id.nin=>[record_id],:sort_order=>{"$lte"=>previous_sort_order,"$gte"=>current_sorting_order}).each do |record|
              record.update_attribute(:sort_order,(record.sort_order + 1))
            end
          end
        end
      end
    end
  end

end