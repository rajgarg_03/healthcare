class System::UnregisterFromEmail
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :user_email,                    :type => String #Deleted user email
  field :user_id,                       :type => String  
  field :unregister_on_date,            :type => Time
  field :unregister_by,         :type => String # user/system
  field :unregister_by_member_id,            :type => String # user id who delete account
  field :unregister_by_user_name,          :type => String # user id who delete account
  
  def self.unregister_from_email(current_user,file,options={})
    q1 = (file)
    sheets = q1.sheets
    (0..0).each do |index| 
      entries = q1.sheet(index).entries[0..-1]
       entries.each do |entry|
        begin
          user = User.where(email:entry[0].strip.to_s.downcase).first 
          if user.present? && user.unregister_from_mail != true
            user.unregister_from_mail = true
            user.save(validate:false)
            ::System::UnregisterFromEmail.save_unregister_from_email_detail(user,current_user,options)
          end
        rescue Exception => e 
          Rails.logger.info "Error inside unregister_from_email........... #{e.message} "
        end
      end
    end
  end

  def self.save_unregister_from_email_detail(user,unregister_by_user,options={})
    # begin
      if unregister_by_user.present?
        unregister_by = "user"
        unregister_by_user_email = unregister_by_user.email
        unregister_by_user_name = "#{unregister_by_user.first_name} #{unregister_by_user.last_name}"
      else
         unregister_by = "system"
         unregister_by_user_email = nil
         unregister_by_user_name = "system"
      end
      data = {
      :user_email => user.email,
      :user_id=> user.id,
      :unregister_on_date => Time.now,
      :unregister_by_user_name=>unregister_by_user_name,
      :unregister_by_email_id=>unregister_by_user_email,
      :unregister_by_member_id => (unregister_by_user.id rescue nil),
      :unregister_by => unregister_by}
      System::UnregisterFromEmail.new(data).save
     
  end
end
