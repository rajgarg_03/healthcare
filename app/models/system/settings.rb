require "system/settings/nook_setting"
class System::Settings
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :name, type: String
  field :description, type: String
  field :status, type:String 
  field :setting_values, type:Hash
  validates_uniqueness_of :name

  include NookSetting

  def self.has_free_access_to_premium_tool_status?(current_user,options={})
    system_setting = System::Settings.where(name:'accessToPremiumTool').last
    return false if system_setting.blank? || system_setting.status != "Active"
    user_country_code = current_user.user.country_code.to_s.downcase
    access_status  = [system_setting.setting_values["countries"]].flatten.include?(user_country_code)
    access_status
  end
   
  def self.allowed_login_attempt
    system_setting = System::Settings.where(name:'loginAttemptAllowed').last
    return -1 if system_setting.blank? || system_setting.status != "Active"
    system_setting.setting_values["allowed_attempt"].to_i
  end

  def self.is_beta_user?(email,options={})
    system_setting = System::Settings.where(name:'betaUserList').last
    return false if system_setting.blank? || system_setting.status != "Active"
    system_setting.setting_values["email_list"].include?(email)
  end

  def self.nhs_login_button(current_user,api_version=5)
    user = current_user.user
    return true if (user.is_internal? || user.is_beta_user?)

    system_setting = System::Settings.where(name:'nhsLoginEnabled').last
    return false if system_setting.blank? || system_setting.status != "Active"
    system_setting.setting_values["show_button"]
  end


  def self.gpsoc_api_limit_count_per_day(current_user,api_version=5)
    user = current_user.user
    return -1 if (user.is_internal? || user.is_beta_user?)

    system_setting = System::Settings.where(name:'gpsocApiLimitCount').last
    return -1 if system_setting.blank? || system_setting.status != "Active"
    system_setting.setting_values["limit_per_day"].to_i
  end
  
  def self.pointer_access_to_update?(current_user,options={})
    # system_setting = System::Settings.where(name:'pointerAccessToAdminPanel').last
    # return false if system_setting.blank? || system_setting.status != "Active"
    # settings = system_setting.setting_values.with_indifferent_access
    access_to_users =  GlobalSettings::PointerAccessToAdminPanel
    access_to_users.include?(current_user.email)
  end
  
  # Request for Delink account with GP 
  def self.im1DelinkingEnabled?(current_user,options={})
    user = current_user.user
    return false if (user.is_internal?)
    system_setting = System::Settings.where(name:'im1DelinkingStatus').last
    return true if system_setting.blank? || system_setting.status != "Active"
    status = false
    system_setting.setting_values["delinking_enable_status"].to_s == "true"
  end

  #  Check if Nhs gp search is working or not/ 
  #  Use NHS Gp search for GP connect or Use other Api eg EMIS api
  def self.nhsGPSearchApiActive?(current_user,options={})
    system_setting = System::Settings.where(name:'nhsGPSearchApi').last
    return true if system_setting.blank? || system_setting.status != "Active"
    system_setting.setting_values["nhs_search_by_postcode_api_status"].to_s == "true" || system_setting.setting_values["nhs_search_by_place_or_postcode_status"].to_s == "true"
  end


  def self.show_health_care_widget(current_user,api_version=5,options={})
    begin
      user = current_user.user
      return true if (user.is_internal? || user.is_beta_user?)
      system_setting = System::Settings.where(name:'healthCareWidget').last
      return false if system_setting.blank? || system_setting.status != "Active"
      status = false
      settings = system_setting.setting_values.with_indifferent_access
      user_device = current_user.devices.last
      device_platform = user_device.platform
      return false if settings["#{device_platform}_app_version"].downcase == "na" 
      if user_device.check_device_is_upto_date?(device_platform,user_device.app_version, settings["ios_app_version"], settings["android_app_version"],options)
         user_country = Member.member_country_code(user.country_code)
        if settings["country_list"].present? && settings["country_list"].include?(user_country)
          status = true
        end
      end
    rescue Exception => e
      Rails.logger.info "Error inside show_health_care_widget .............#{e.message}"
      status = false
    end
    status
  end


   # FAQ notification for users
   # eg. {setting_values:{"faq_enable"=>true,"country_list"=>["uk","us"]}}
  def self.faqNotificationEnabled?(current_user,country_code,options={})
    # user = current_user.user
    # return true if (user.is_internal?)
    country_code = GlobalSettings::CountryListForPushNotification.include?(country_code.downcase) ? country_code.downcase : "other"  rescue country_code
    system_setting = System::Settings.where(name:'faqStatus').last
    return true if system_setting.blank? || system_setting.status != "Active"
    notification_status = system_setting.setting_values["faq_enable"].to_s == "true"
    notification_for_country = system_setting.setting_values["country_list"]
     
    if notification_for_country.blank? || notification_for_country.map(&:downcase).include?("all")
      status = notification_status
    else
      country_list = notification_for_country.map{|a| Member.sanitize_country_code(a)}.flatten + notification_for_country
      status = notification_status && country_list.include?(country_code)
    end
    status
  end

  def self.im1ClinicRecordSavingEnabled?(current_user,record_type,options={})
    record_type = record_type.to_s.downcase
    #setting_values = {"appointment"=>true,"health_records"=>true,"prescribing"=>true,"allergy"=>true,"immunisations"=>true,"consultations"=>true,"medication"=>true,"problems"=>true,"testresults"=>true}
    system_setting = System::Settings.where(name:'iM1ClinicRecordSaving').last
    return false if system_setting.blank? || system_setting.status != "Active"
    system_setting.setting_values[record_type].to_s == "true"
  end

  def self.tppEnabled?(current_user,options={})
    system_setting = System::Settings.where(name:'tppEnabled').last
    user_device = current_user.devices.last rescue nil 
    return false if system_setting.blank? || system_setting.status != "Active"
    is_active  = system_setting.status == "Active"
    if user_device.blank?
      enabled_for_device = false
    else
      user_device_app_version = user_device.app_version_conversion_from_str_to_int(user_device.app_version)
      required_app_version = Device.new.app_version_conversion_from_str_to_int(system_setting.setting_values[user_device.platform]["app_version"].to_s) rescue 0
      enabled_for_device = user_device_app_version >=required_app_version

    end
    is_active && enabled_for_device
  end

end