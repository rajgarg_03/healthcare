class System::AffiliateMarketingProgram::AmazonAffiliatedLink
  include Mongoid::Document  
  include Mongoid::Timestamps
  
  field :product_name,       type: String
  field :product_price,      type: String
  field :product_link_url,   type: String
  field :product_image_url,  type: String
  field :product_provider,   type: String
  field :country_code,       type: String
  field :product_keywords,   type: Array
  field :asin,               type:String

  validates_uniqueness_of :asin
  
  
  def self.search_items(current_user,search_keywords,search_index,options={})
    begin
      request,country_code = self.set_request_config(current_user,options)
      # search index values:
      # 'Beauty','Grocery','Industrial','PetSupplies','OfficeProducts','Electronics','Watches','Jewelry','Luggage','Shoes','Furniture','KindleStore','Automotive','Pantry','MusicalInstruments','GiftCards','Toys','SportingGoods','PCHardware','Books','LuxuryBeauty','Baby','HomeGarden','VideoGames','Apparel','Marketplace','DVD','Appliances','Music','LawnAndGarden','HealthPersonalCare','Software'
      search_index = search_index || "all"
      search_index = search_index.split(",").map(&:titleize).join(",")
      search_keywords = search_keywords.split(",").map(&:titleize).join(",")
      response = request.item_search(
      query: {
        'Keywords' => search_keywords,
        'SearchIndex' => (search_index || "All"),
        "ResponseGroup" => "Images,ItemAttributes"
      })
      response = response.parse
      response  = response["ItemSearchResponse"]["Items"]["Item"]
      self.save_response_for_searchitems(current_user,country_code,response,search_keywords,options)    
    rescue Exception=>e 
      response = nil     
    end
    response
  end
  

  def self.save_response_for_searchitems(current_user,country_code,response,search_keywords,options={})
    response.each do |item|
      begin
        obj = System::AffiliateMarketingProgram::AmazonAffiliatedLink.where(asin:item["ASIN"]).last
        if obj.blank?
          obj = System::AffiliateMarketingProgram::AmazonAffiliatedLink.new
        end
        obj.product_name = item["ItemAttributes"]["Title"]
        obj.product_link_url = item["DetailPageURL"]
        obj.product_price = item["ItemAttributes"]["ListPrice"]["FormattedPrice"] 
        obj.product_image_url = item["LargeImage"]["URL"]
        obj.country_code = country_code
        obj.product_provider=  item["ItemAttributes"]["Publisher"] 
        obj.asin  = item["ASIN"]
        obj.product_keywords = obj.product_keywords || []
        obj.product_keywords << search_keywords.split(",")
        obj.product_keywords = obj.product_keywords.flatten.uniq
        obj.save
      rescue Exception=>e 
        puts "error inside save :- #{e.message}"
      end
    end
    nil
  end

  def self.save_product_for_keywords
    current_user = nil
    options = {}
    scenario_keywords = ["baby","walker"]#System::Scenario::ScenarioDetails.get_keywords_for_product
    # search_keywords = scenario_keywords.join(",")
    scenario_keywords.each do |search_keywords|
      options[:country_code] = "IN"
      System::AffiliateMarketingProgram::AmazonAffiliatedLink.search_items(current_user,search_keywords,search_index="All",options)
      options[:country_code] = "UK"
      System::AffiliateMarketingProgram::AmazonAffiliatedLink.search_items(current_user,search_keywords,search_index="All",options)
    end
  end

  def self.set_request_config(current_user,options={})
    country_code = (options[:country_code] || current_user.get_country_code).upcase
    request = Vacuum.new(country_code.upcase)
    aws_access_key_id = AppConfig["amazon_affiliate_access_key"]
    aws_secret_access_key = AppConfig["amazon_affiliate_secret_key"]
    associate_tag = AppConfig["amazon_affiliate_associate_tag"]
    request.configure(
      aws_access_key_id: aws_access_key_id,
      aws_secret_access_key: aws_secret_access_key,
      associate_tag: associate_tag
    )   

    [request,country_code]
  end
 

end