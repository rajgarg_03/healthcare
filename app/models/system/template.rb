class System::Template
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :title, type: String
  field :description, type:String 
  field :template_for, type:String #child/pregnancy/all
  field :mapped_table, type:String #child/pregnancy/all

  def self.available_template
   [["SystemTimeline","SystemTimeline"],["SystemVaccin","SystemVaccin"],["SystemMilestone","SystemMilestone"], ["SystemTimeline","SystemTimeline"],["System::Child::CheckupPlanner::Checkup","System::Child::CheckupPlanner::Checkup"],["SysMedicalEvent","SysMedicalEvent"],["SysMedicalEventTime","SysMedicalEventTime"],["AllergyManager","AllergyManager"],["AllergySymptomManager","AllergySymptomManager"],["System::Pregnancy::PregnancyWeekDetail","System::Pregnancy::PregnancyWeekDetail"],["System::GpsocImmunisationJab","System::GpsocImmunisationJab"],["Nhs::GuideTiles","Nhs::GuideTiles"]]
  end

  def self.available_system_table_for_scenario
   [["System::Faq","System::Faq"],["System::Faq::Topic","System::Faq::Topic"],["System::Faq::SubTopic","System::Faq::SubTopic"], ["System::Faq::Question","System::Faq::Question"]]
  end
  
  ClassForContentMapping = {"WhoBmi"=>"BMI(Measurement)",
                            "WhoHeight"=>"Measurment-Height",
                            "WhoWeight"=>"Weight(Measurement)",
                            "Pregnancy::HealthCard::BloodPressure"=>"BloodPressure(Pregnancy::HealthCard)",
                            "Pregnancy::HealthCard::Hemoglobin"=>"Hemoglobin(Pregnancy::HealthCard)",
                            "Pregnancy::HealthCard::Measurement"=>"Measurement(Pregnancy::HealthCard)",
                            "Pregnancy::KickCounter::KickCountDetail"=>"KickCounter(Pregnancy)",
                            "Child::HealthCard::EyeChart"=>"EyeChart(Child::HealthCard)" ,
                            "Child::ToothChart::ChartManager"=>"ChartManager(Child::ToothChart)",
                            "Child::VisionHealth::CvtTest"=>"CvtTest(Child::VisionHealth)",
                            "Child::Zscore" => "Zscore(Child)",
                            
                          }   
                          # "Nhs::GuideTiles"=> "NhsGuideTiles"
  def self.get_country_column_for_mapped_table(table_name)
    table_name = table_name.to_s
    column_name = "country_code"
    if table_name == "SystemVaccin"
      column_name = "country"
    elsif table_name == "System::Child::CheckupPlanner::Checkup"
      column_name = "country_code"
    elsif table_name == "SysMedicalEventTime"
      column_name = "country_code"
    elsif table_name == "SysMedicalEventTime"
      column_name = nil
    elsif table_name == "SysMedicalEvent"
      column_name = "country"
    elsif table_name == "System::Pregnancy::PregnancyWeekDetail"
      column_name = "country_code"
    elsif table_name == "System::Faq::Question"
      column_name = nil
    elsif table_name ==  "System::Faq::SubTopic"
      column_name = nil
    elsif table_name == "System::Faq::Topic"
      column_name = nil
    elsif table_name == "Nhs::GuideTiles"
      column_name = "country_code"
    elsif System::Template::ClassForContentMapping.keys.include?(table_name)
      column_name = "NA"
    end 
    column_name  
  end


  def self.get_serach_column_for_mapped_table(table_name)
    table_name = table_name.to_s
    case table_name
    when "SystemVaccin"
       ["name", "vaccin_type","trade_name"]
     when  "SystemMilestone"
      ["title","description","milestone_question"]

    when "System::Child::CheckupPlanner::Checkup"
       ["title","description"]
    when "SysMedicalEventTime"
      ["category_title"]
    when  "SysMedicalEvent"
      ["name","desc","category","category_title"]
    when "AllergySymptomManager"
    	["name","category"]
    when "AllergyManager"
      ["name","category"]
    when "System::Pregnancy::PregnancyWeekDetail"
      ["pregnancy_week", "description","your_baby_description","your_body_description"]
    when "SystemTimeline"
      ["name", "desc"]
    when "System::Faq::Question"
      ["title","category"]
    when "System::Faq::SubTopic"
      ["title","status"]
    when "Nhs::GuideTiles"
      ["title","description","category"]

    else 
      nil
    end 
      
  end

end