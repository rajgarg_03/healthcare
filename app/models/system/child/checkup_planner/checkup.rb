class System::Child::CheckupPlanner::Checkup
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :title, type: String
  field :time_frame, type:String
  field :description, type:String
  field :time_frame_text, type:String 
  field :country_code, type: String

  belongs_to :member
  has_many :checkup_tests,class_name:"System::Child::CheckupPlanner::CheckupTest"
  
  index({member_id:1})
  index({country_code:1})

  def tests
    checkup_tests.map(&:test)
  end

  def self.get_list_with_test
    data = []
    System::Child::CheckupPlanner::Checkup.all.each do |checkup|
       test_ids = System::Child::CheckupPlanner::CheckupTest.where(checkup_id:checkup.id).pluck(:test_id)
       checkup["tests"] = System::Child::CheckupPlanner::Test.where(:id.in=>test_ids).entries
      data << checkup
    end
    data
  end

  def self.add_system_checkup_to_member(current_user,member,api_version=1,options={})
    country_code = Member.member_country_code(current_user.user.country_code).upcase rescue nil
    country_code = ["GB", "IN", "US", "AE", "NZ", "CA", "ZA" "SG"].include?(country_code)? country_code : "Other"
    system_checkup_planner = System::Child::CheckupPlanner::Checkup.where(:country_code=>country_code)
    added_checkup_planner = []
    if options[:save_checkup_planner] == true
      system_checkup_planner.each do |system_checkup|
        # Add checkup
        child_checkup = ::Child::CheckupPlanner::Checkup.new(
          title:system_checkup.title,
          description:system_checkup.description,
          status: "Estimated",
          member_id: member.id,
          time_frame: system_checkup.time_frame_text,
          checkup_date: (member.birth_date + eval(system_checkup.time_frame)  rescue nil),
          checkup_start_time: "09:00",
          checkup_end_time: "10:00",
          addded_by: "system"
          )
        
        child_checkup.save
        added_checkup_planner << child_checkup
        # Add tests to checkup 
        test_ids = System::Child::CheckupPlanner::CheckupTest.where(checkup_id:system_checkup.id).pluck(:test_id)
        test_ids.each do |test_id|
          ::Child::CheckupPlanner::CheckupTest.new(system_checkup_planner_test_id:test_id,checkup_id:child_checkup.id).save
        end
      end
    end
    [system_checkup_planner,added_checkup_planner]
  end

  # For admin panel
  def update_record(params)
    begin
      checkup_params = params["system_child_checkup_planner_checkup"]
      checkup_params["time_frame"] = (params[:duration]||0).to_s + "." + params[:frequency] 

      checkup = self
      if checkup.update_attributes(checkup_params)
        status = true
      else
        status = false
      end
    rescue Exception => e
      Rails.logger.info e.message
      status = false 
    end
    [checkup,status]
  end


  def self.create_record(params)
    begin
      checkup_params = params["system_child_checkup_planner_checkup"]
      checkup_params["time_frame"] = (params[:duration]||0).to_s + "." + params[:frequency] 
      
      checkup = self.new(checkup_params)
      checkup.save!
      status = true
    rescue Exception => e
      Rails.logger.info e.message
      status = false 
    end
    [checkup,status]
  end

  def self.delete_record(record_id)
    begin
      checkup = System::Child::CheckupPlanner::Checkup.find(record_id)
      checkup.delete
    rescue Exception => e
      Rails.logger.info e.message
    end
  end
  
  def self.get_checkup_test_data(system_checkup)
    data = system_checkup.tests
    fields = System::Child::CheckupPlanner::Test.attribute_names
    fields.delete("_id")
    fields.delete("created_at")
    fields.delete("member_id")
    fields.delete("updated_at")
    [data,fields]
  end

end

