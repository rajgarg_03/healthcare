class System::Child::CheckupPlanner::CheckupTest
  include Mongoid::Document  
  include Mongoid::Timestamps

  belongs_to :test, class_name:"System::Child::CheckupPlanner::Test"
  belongs_to :checkup, class_name:"System::Child::CheckupPlanner::Checkup"

  index({member_id:1})

  
end