class System::Child::CheckupPlanner::Test
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :title, type: String
  field :description, type:String 
  field :added_by , type:String # "user/system"
  index({member_id:1})

  belongs_to :member
  
  def title_with_description
    "#{title} - #{description}" 
  end
  
  def self.get_list(current_user,api_version=1,checkup_plan_id=nil)
    added_test = []
    added_test  = ::Child::CheckupPlanner::CheckupTest.where(:checkup_id=> checkup_plan_id).pluck(:system_checkup_planner_test_id).map(&:to_s) if checkup_plan_id.present?
    all_system_tests = System::Child::CheckupPlanner::Test.in(member_id:[nil,current_user.id.to_s]).order_by("added_by desc,title asc")
    data = []
    all_system_tests.each do |test|
      test["added"] = added_test.include?(test.id.to_s)
      puts test["added"]
      data << test
    end
    data
  end
  
  def get_system_checkup
    checkup_test = self
    System::Child::CheckupPlanner::CheckupTest.where(test_id:checkup_test.id.to_s).last.checkup rescue nil
  end

  def self.add_custom_test(current_user,params,api_version=1)
    params[:system_test][:member_id] = current_user.id
    checkup_test = System::Child::CheckupPlanner::Test.new(params[:system_test])
    checkup_test.save!
    checkup_test
  end
  

  # Used for admin panel
   def update_record(params)
    begin
      system_test = self
      system_test_params = params["system_child_checkup_planner_test"]
      
      if system_test.update_attributes(system_test_params)
        status = true
      else
        status = false
      end
    rescue Exception => e
      Rails.logger.info e.message
      status = false 
    end
    [system_test.get_system_checkup,status]
  end


  def self.create_record(params)
    begin
      system_test_params = params["system_child_checkup_planner_test"]
      # system_test_params[:due_on] = params[:duration] + "." + params[:frequency]
      system_test = self.new(system_test_params)
      system_test.save!
      system_checkup_test =  System::Child::CheckupPlanner::CheckupTest.new(checkup_id: params[:checkup_id],test_id:system_test.id)
      system_checkup_test.save
      system_checkup = system_test.get_system_checkup
       
      status = true
    rescue Exception => e
      Rails.logger.info e.message
      status = false 
    end
    [system_checkup,status]
  end

  def self.duplicate_record(params)
    record_to_be_duplicated_obj = System::Child::CheckupPlanner::Test.find(params[:record_id])
    record_to_be_duplicated = record_to_be_duplicated_obj.clone.attributes
    record_to_be_duplicated.delete("_id")
    record_to_be_duplicated.delete("created_at")
    record = System::Child::CheckupPlanner::Test.new(record_to_be_duplicated)
    record.save
    system_checkup = record_to_be_duplicated_obj.get_system_checkup
    system_checkup_test =  System::Child::CheckupPlanner::CheckupTest.new(checkup_id: system_checkup.id,test_id:record.id)
    system_checkup_test.save
    [record,system_checkup]
  end

  def self.add_existing_test(params)
    begin
      test_record = System::Child::CheckupPlanner::Test.find(params[:test_id])
      recost_exist = System::Child::CheckupPlanner::CheckupTest.where(checkup_id: params[:checkup_id],test_id:params[:test_id]).last
      if recost_exist.blank?
        system_checkup_test =  System::Child::CheckupPlanner::CheckupTest.new(checkup_id: params[:checkup_id],test_id:params[:test_id])
        system_checkup_test.save!
        checkup_record = System::Child::CheckupPlanner::Checkup.find(params[:checkup_id])
      end
      status = true
    rescue Exception=> e 
      status = false
    end
    [checkup_record,status]
  end

  def self.delete_record(record_id)
    begin
      system_test = System::Child::CheckupPlanner::Test.find(record_id)
      System::Child::CheckupPlanner::CheckupTest.where(test_id:system_test.id).delete_all
      # system_test.delete
    rescue Exception => e
      Rails.logger.info e.message
    end
  end



end

