class System::MeasurementWho

def self.get_who_data(current_user,health_parameter, gender=1)
    gender_str = gender
    if health_parameter == "weight" && current_user.metric_system["weight_unit"] =="kgs"
      WhoWeight.where(:gender=> gender_str).in(measure_unit: nil).order_by('month ASC')
    elsif  health_parameter == "weight" && current_user.metric_system["weight_unit"] =="lbs"
      WhoWeight.where(:gender=> gender_str).in(measure_unit: "lbs").order_by('month ASC')

    elsif  health_parameter == "height" && current_user.metric_system["length_unit"] =="cms"
      WhoHeight.where(:gender=> gender_str).in(measure_unit: nil).order_by('month ASC')
    elsif  health_parameter == "height" && current_user.metric_system["length_unit"] =="inches"
      WhoHeight.where(:gender=> gender_str).in(measure_unit: "inches").order_by('month ASC')
    
    elsif  health_parameter == "bmi"
          WhoBmi.where(:gender=> gender_str).order_by('month ASC')

    elsif  health_parameter == "waist_circum" && current_user.metric_system["length_unit"] =="cms"
      WhoWaist.where(:gender=> gender_str).in(measure_unit: nil).order_by('month ASC')
    elsif  health_parameter == "waist_circum" && current_user.metric_system["length_unit"] =="inches"
      WhoWaist.where(:gender=> gender_str).in(measure_unit: "inches").order_by('month ASC')
    
    elsif  health_parameter == "head_circum" && current_user.metric_system["length_unit"] =="cms"
      WhoHcf.where(:gender=> gender_str).in(measure_unit: nil).order_by('month ASC')
    elsif  health_parameter == "head_circum" && current_user.metric_system["length_unit"] =="inches"
      WhoHcf.where(:gender=> gender_str).in(measure_unit: "inches").order_by('month ASC')
    else
     (0..24).map{|a| a}
    end

  end
 end