class System::DeletedUserDetail
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :user_email,                    :type => String #Deleted user email
  field :provider,                      :type =>String  # Facebook ..etc
  field :last_sign_in_at,               :type =>String 
  field :account_status,                 :type => String #confirmed/unconfirmed/unapproved/approved
  field :activity_status,               :type=>String, :default=>"Active" # eg. Inactive/Active/Defunct
  field :activity_status_v2,            :type=>String#, :default=>"Active" # eg. Inactive/Active/Defunct
  field :user_id,                       :type => String # Deleted user id
  field :member_id,                     :type => String # Deleted user memebr id
  field :sign_in_count,                 :type => Integer # total signin count
  field :last_sign_in_at,               :type => Time
  field :segment,                       :type=>String, :default=>"trial" # eg. trial/starter/adoptor/ambassador
  field :deleted_on_date,               :type => Time
  field :deleted_by,                    :type => String # user/system
  field :deleted_by_user_id,            :type => String # user id who delete account
  field :deleted_by_user_name,          :type => String # user id who delete account
  field :deleted_by_email_id,           :type => String # user id who delete account
  field :country_code,                  :type => String # user id who delete account
  def self.save_delete_user_detail(deleted_user,deleted_by_user,options={})
    # begin
      if deleted_by_user.present?
        deleted_by = "user"
        deleted_by_user_email = deleted_by_user.email
        deleted_by_user_name = "#{deleted_by_user.first_name} #{deleted_by_user.last_name}"
      else
         deleted_by = "system"
         deleted_by_user_email = nil
         deleted_by_user_name = "system"
      end
      data = {
      :user_email => deleted_user.email,
      :country_code=>deleted_user.country_code,
      :provider=> deleted_user.provider,
      :account_status=> deleted_user.status,
      :activity_status=> deleted_user.activity_status,
      :activity_status_v2 => deleted_user.activity_status_v2,
      :user_id=> deleted_user.id,
      :member_id=> deleted_user.member_id,
      :sign_in_count=> deleted_user.sign_in_count,
      :last_sign_in_at=> deleted_user.last_sign_in_at,
      :segment=> deleted_user.segment,
      :deleted_on_date=> Time.now,
      :deleted_by_user_name=>deleted_by_user_name,
      :deleted_by_email_id=>deleted_by_user_email,
      :deleted_by_user_id => (deleted_by_user.id rescue nil),
      :deleted_by => deleted_by}
      System::DeletedUserDetail.new(data).save
    # rescue Exception=>e
    #   options[:deleted_user] = deleted_user.inspect rescue nil
    #   options[:deleted_by_user_or_system] = deleted_by
    #   options[:deleted_by] =  deleted_by_user.inspect rescue nil
    #   UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside save_delete_user_detail")#.deliver!
    # end
  end
end
