# Log security related issues identified
class System::Security
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :level,                         :type => Integer,:default=>1 #1,2,3 (1=>high,2 medium, 3 low)
  field :member_id,                     :type => String
  field :description,                   :type => String # detail for issue
  field :mail_msg,                      :type => String # detail for issue
  field :admin_notify_status ,          :type => Boolean,:default=>false # is admin notified for issue
  field :category ,                     :type=>  String # eg "emis/nurturey... 
  field :identifier_id,                  :type => Integer # 1=> emis access outside uk,2,3

  def self.save_security_issue(member_id,message,category,level,identifier_id,mail_msg,options={})
    begin
      system_secuirty = System::Security.create(mail_msg:mail_msg,member_id:member_id,identifier_id:identifier_id, description:message,category:category,level:(level||1)  )
      # check if admin notified 
      admin_notified = System::Security.where(member_id:member_id, identifier_id:identifier_id,admin_notify_status:true, :created_at.gte=>Date.today).last
      if admin_notified.blank?
        begin
        # send  mail to admin
          UserMailer.notify_admin_for_system_security_alert(system_secuirty,options).deliver!
          system_secuirty.admin_notify_status = true
          system_secuirty.save
        rescue Exception => e 
          Rails.logger.info "Error inside notify admin ... #{e.message}"
        end
      end
    rescue Exception=>e 
      Rails.logger.info "Error inside save_security_issue ... #{e.message}"
    end
  end

end