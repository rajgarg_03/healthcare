# system scenario 
class System::Scenario::ScenarioLinkedToTool
  include Mongoid::Document  
  include Mongoid::Timestamps

  field :tool_type ,                                :type=>  String # eg "question/pointer/faq/" 
  field :tool_subtype ,                             :type=>  String # eg "question/pointer/topic/" 
  field :linked_tool_id,                            :type=>  String  
  field :tool_class,                                :type=>  String  
  field :topic,                                     :type=>  String  
  field :system_scenario_detail_custom_id,          :type=>  Integer 
  field :notification_expiry,                       :type=>  String
  
  index({tool_class:1})
  index({linked_tool_id:1})
  index({system_scenario_detail_custom_id:1})
  
  def scenario_detail
  	return nil if self.system_scenario_detail_custom_id.blank?
    ::System::Scenario::ScenarioDetails.where(:custome_id=>self.system_scenario_detail_custom_id).last    
  end

  def self.add_to_list(tool_object,system_scenario_custom_id)
    tool = tool_object
    tool_class = tool.class.to_s

    tool_type,tool_subtype = get_tool_type_and_subtype(tool_class)
    if system_scenario_custom_id.blank?
      linked_scenarios_custom_id = System::Scenario::ScenarioLinkedToTool.where(linked_tool_id:tool.id,tool_class:tool_class).delete_all
    else
    
      linked_scenarios = System::Scenario::ScenarioLinkedToTool.where(linked_tool_id:tool.id,tool_class:tool_class).last
      topic = tool.title
       if linked_scenarios.present?
         linked_scenarios.update_attributes({topic:topic,:system_scenario_detail_custom_id=>system_scenario_custom_id,:notification_expiry=>tool.notification_expiry})
       else
        linked_scenarios = System::Scenario::ScenarioLinkedToTool.create(topic:topic,:system_scenario_detail_custom_id=>system_scenario_custom_id,tool_subtype:tool_subtype,tool_type:tool_type,linked_tool_id:tool.id,tool_class:tool_class,:notification_expiry=>tool.notification_expiry)
       end
    end
  end

  def self.get_tool_type_and_subtype(tool_class)
    
    if tool_class== "System::Faq::Question"
      ["faq","question"]  
    elsif tool_class== "SysArticleRef"
      ["pointer",nil]
    else
      [nil,nil]  
    end
  end

#  System::Scenario::ScenarioLinkedToTool.group_by_system_scenario_detail_custom_id
  def self.group_by_system_scenario_detail_custom_id(tool_class=nil,custom_ids=nil)
    group_by = {
      "_id" => "$system_scenario_detail_custom_id",
      "data" => {"$push"=> "$$ROOT"}
    }
    sort_by = {"system_scenario_detail_custom_id"=>1}
    if tool_class.present?
      match_values = {"tool_class"=>tool_class } 
      hash_data =  System::Scenario::ScenarioLinkedToTool.collection.aggregate([
        {"$match"=>match_values},
        {"$group" =>group_by },
        {"$sort"=>sort_by}
      ])
    else
      hash_data =  System::Scenario::ScenarioLinkedToTool.collection.aggregate([
        {"$group" =>group_by },
        {"$sort"=>sort_by}
      ])
    end

    data = {}
    hash_data.each{|result| data[result["_id"] ] = result["data"] }
    data
  end

end