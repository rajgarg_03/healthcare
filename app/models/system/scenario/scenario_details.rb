class System::Scenario::ScenarioDetails
  include Mongoid::Document  
  include Mongoid::Timestamps
  # field :title,                         :type => String
  # field :stage,                         :type => String # eg, infant,pregnancy
  # field :criteria,                      :type => String # eg. "All/any specific milestone acheived etc." 
  # field :criteria_in_tech,              :type => String # eg. "All/any specific milestone acheived etc." 
  # field :score,                         :type => String # 1,2...etc
  # field :N_factor,                      :type => String # 1,2,3..etc
  # field :status,                        :type => String, :default=>"Active"# eg.Active/Draft
  # field :ds,                            :type => String # start date eg. Day of milestone achieve
  # field :dp,                            :type => String # peak date .eg.  Day of milestone achieve afer 2 days
  # field :dexp,                          :type => String # expiry date eg. Day of milestone achieve after 5 days
  # field :start_date,                    :type => String # start date in techinical eg. "milestone.acheive + 0.days"
  # field :peak_rel_date,                 :type => String # peak date in techinical eg. "milestone.acheive + 2.days"
  # field :expire_date,                   :type => String # expiry date in techinical eg. "milestone.acheive + 5.days"
  # field :tool_for_criteria,             :type => String #eg. nil/milestones/timeline/measurements/..etc

  # field :name,                          :type => String# scenario in tech
  field :custome_id,                    :type => Integer #auto increamented value for  ScenarioDetails.eg 1,2,3 etc will used for backtracking
  field :title,                         :type=> String # name
  field :title_in_tech,                 :type=> String
  field :score,                         :type => String
  field :N_factor,                      :type => String 
  field :start_date,                    :type => String
  field :peak_rel_date,                 :type => String
  field :expire_date,                   :type => String
  field :category,                      :type=> String # pregnancy,milestones,measurements
  field :status,                        :type=> String, :default=>"Active"
  field :stage,                         :type=> String
  field :condition,                     :type=> String
  field :condition_in_tech,             :type=> String #condition in tech
  field :ds,                            :type=> String
  field :dp,                            :type=> String
  field :dexp,                          :type=> String
  field :keywords,                      :type=> Array, :default=>[] #preganacy,walk,
  
  after_create :assign_custome_id
  
  index ({custome_id: 1})
  index ({status: 1})
  index ({category: 1})
  index ({title_in_tech: 1})
  
  validates_presence_of   :title 
  DATATABLE_COLUMNS = [:custome_id,:N_factor, :category, :condition, :condition_in_tech,  :dexp, :dp, :ds, :expire_date, :peak_rel_date, :score, :stage, :start_date, :status, :title, :title_in_tech]
  def assign_custome_id
    scenario_detail  = self
    scenario_detail[:custome_id] = scenario_detail[:custome_id] || System::Scenario::ScenarioDetails.max(:custome_id).to_i + 1
    scenario_detail.save
  end 
  
  def self.datatable_filter(search_value, search_columns)
    return all if search_value.blank?
    result = self
    filter = []
    search_columns.each do |key, value|
      if value['data'] == "custome_id"
       filter << {"#{value['data']}"=>search_value} if value['searchable']
      else
       filter << {"#{value['data']}"=>/#{Regexp.escape(search_value)}/i} if value['searchable']
      end
    end
    result = result.where("$or"=>filter) 
    result
  end  
  
  def self.datatable_order(order_column_index, order_dir)
    order_by("#{System::Scenario::ScenarioDetails::DATATABLE_COLUMNS[order_column_index]} #{order_dir}")
  end
  
  def self.valid(options={})
    where(status:"Active")
  end

  def self.get_scenario_category(scenario_detail,options={})
    scenario_category = scenario_detail.category.downcase rescue "member"
    if ["member","infants","child","infant","all"].include?(scenario_category)
      scenario_category = "member"
    elsif ["milestone","milestones"].include?(scenario_category)
      scenario_category = "milestone"
    elsif ["health","measurement","measurements"].include?(scenario_category)
      scenario_category = "health"
    elsif ["prenatal_test","prenatal test","prenatal tests"].include?(scenario_category)
      scenario_category = "prenatal_test"
    elsif ["pregnancy"].include?(scenario_category)
      scenario_category = "pregnancy"
    elsif ["vaccination","immunisation","immunisations"].include?(scenario_category)
      scenario_category = "vaccination"
    elsif ['allergies'].include?(scenario_category)
      scenario_category = "allergies"
    else 
       scenario_category = "member"
    end
    scenario_category 
  end

  def self.data_validation_for_allergy(scenario_detail,member,country_code,options={})
    validation_step_1 = false
    criteria_status = false
    allergy = nil
    begin 
      scenario_criteria_for_allergy =  eval(scenario_detail.condition_in_tech)
      if scenario_criteria_for_allergy.blank?
        validation_step_1 = true
      end
    rescue Exception=> e
     return [nil,validation_step_1,criteria_status]
    end  
    allergy_name = scenario_criteria_for_allergy[:allergy]
    country_code = scenario_criteria_for_allergy[:country]
    if country_code.blank? || country_code == "all"
      allergy_manager = AllergyManager.where(:name=>/^#{Regexp.escape(allergy_name.strip)}$/i)
    else
      allergy_manager = AllergyManager.where(:name=>/^#{Regexp.escape(allergy_name.strip)}$/i).in_country(nil,country_code)
    end
    allergy_manager_ids = allergy_manager.pluck(:id)
    allergy = Child::HealthCard::Allergy.where(member_id:member.id,:allergy_manager.in=>allergy_manager_ids).last
    if allergy.present?
      validation_step_1 = true
    end
    [allergy,validation_step_1,criteria_status]
  end

  # System::Scenario::ScenarioDetails.get_data_after_validate
  def self.get_data_after_validate(scenario_detail,scenario_category,member,country_code,options={})
# puts [scenario_detail,scenario_category,member,country_code,options].inspect
    validation_step_1 = false
    criteria_status =  false
    linked_tool = options[:linked_tool]
    data = {}
    pregnancy = options[:pregnancy]#.present? ? options[:pregnancy] : Pregnancy.where(expected_member_id:member.id.to_s).first
    pregnancy_status = pregnancy.present?
    scenario_condition_in_tech = scenario_detail.condition_in_tech
    case scenario_category
    when "allergies"
      return validation_step_1 if pregnancy_status
      allergies,validation_step_1,criteria_status =  System::Scenario::ScenarioDetails.data_validation_for_allergy(scenario_detail,member,country_code,options)
    when "health"
      return validation_step_1 if pregnancy_status
      health = member.health_records.order_by("updated_at desc").first
      if health.present? && !pregnancy_status
        health.height = health.height || (Health.where(:height.ne=>nil).where(member_id:member.id).order_by("updated_at desc").first.height || 0 rescue 1)
        health.weight = (health.weight.blank? || health.weight.to_i == 0) ? (Health.where(:weight.ne=>nil).where(member_id:member.id).order_by("updated_at desc").first.weight rescue 1) : health.weight
        validation_step_1 = true
        criteria_status = !scenario_condition_in_tech.nil?
      end
    when "pregnancy"
      validation_step_1 = (pregnancy.present? && pregnancy.status == "expected") rescue false
      criteria_status = validation_step_1 && !scenario_condition_in_tech.nil?
    when "vaccination"
      # get name, country and condition from article for vaccination (eg vaccination_name:no-of_days:country_list_filter#critriea)
      return validation_step_1 if pregnancy_status
      
      # vaccine_conditions = scenario_condition_in_tech.split("#")
      # vaccine_name, jab_due_on_before,country = vaccine_conditions[0].split(":")
      # country_filter_list = eval(country)
      scenario_condition_in_tech = eval(scenario_condition_in_tech)
      vaccine_name = scenario_condition_in_tech[:vaccin] || scenario_condition_in_tech[:vaccine]
      country_filter_list = [scenario_condition_in_tech[:country]].flatten.compact
      jab_due_on_before = scenario_condition_in_tech[:due_on_before_day]
      jab_due_on_before = jab_due_on_before || "45.days"
      system_vaccine_ids = SystemVaccin.where(:name=> /^#{Regexp.escape(vaccine_name.strip)}$/i).pluck(:id).map(&:to_s)
      if system_vaccine_ids.present?
        vaccination = Vaccination.where(:member_id=>member.id,:opted=> "true", :sys_vaccin_id.in => system_vaccine_ids ).first rescue nil
      else
        vaccination = nil
      end
      if vaccination.present? && !pregnancy_status
        all_jabs = vaccination.jabs
        jab_due_on_before_time = Time.now.end_of_day + eval(jab_due_on_before)
        jabs = Jab.where(:vaccination_id=>vaccination.id).or({:status=>"Planned",:due_on.lte => jab_due_on_before_time},{:due_on => nil,:est_due_time.lte =>jab_due_on_before_time}) rescue []
        if jabs.present? 
          validate_country_check = true 
          if !country_filter_list.blank? && !(country_filter_list.first.downcase == "all")
            # get vaccin country if pointer is available for specific country
            system_vaccin = SystemVaccin.find(vaccination.sys_vaccin_id)
            vaccine_country = system_vaccin.country
            sanitize_country_codes = []
            country_filter_list.each do |country|
              sanitize_country_codes << Member.sanitize_country_code(country)
            end
            sanitize_country_codes.flatten!
            validate_country_check = sanitize_country_codes.include?(vaccine_country)
          end
          if validate_country_check
            if linked_tool.class.to_s == "SysArticleRef"
              jab = ArticleRef.jab_with_high_score(jabs,linked_tool,{:scenario_detail=>scenario_detail})
            else
              jab = ::Child::Scenario.jab_with_high_score(jabs,scenario_detail,{:scenario_detail=>scenario_detail})
            end
            if jab.present?
              jab_date = (jab.due_on || jab.est_due_time)  
              administered_date = jab_date 
              validation_step_1 = true
              criteria_status = false
            end
          end
        end
      end
    when "milestone"
      return validation_step_1 if pregnancy_status
      
      # eg  Milestone_name:#milestone.present?
      begin
        scenario_condition_in_tech = eval(scenario_condition_in_tech).with_indifferent_access 
        milestone_name = scenario_condition_in_tech[:milestone_name].strip
        milestone_country_filter = [scenario_condition_in_tech[:country]].flatten.compact
      rescue Exception=>e 
        milestone_conditions = scenario_condition_in_tech.split("#")
        milestone_name, milestone_country_filter = milestone_conditions[0].split(":").map(&:strip)
        milestone_country_filter = milestone_country_filter.nil? ? nil : eval(milestone_country_filter)
      
      end
      validate_country_check = milestone_country_filter.present? ? (milestone_country_filter & Member.sanitize_country_code(country_code)).present? : true
      if validate_country_check
        sys_milestone = SystemMilestone.where(:title=>/^#{Regexp.escape(milestone_name.strip)}$/i).first
        milestone = Milestone.where(system_milestone_id:sys_milestone.id.to_s,member_id:member.id.to_s).first rescue nil
        criteria_status = false

        validation_step_1 = milestone.present? rescue false
      end
    when "prenatal_test"
      test_name, component_name = scenario_condition_in_tech.split("#")
      if test_name.present?
        prenatal_test = MedicalEvent.where(:name=>/^#{Regexp.escape(test_name)}$/i,:member_id=>member.id).first 
      elsif component_name.present?
        prenatal_test = MedicalEvent.where(:component=>/^#{Regexp.escape(component_name)}$/i,:member_id=>member.id).first
      end
      criteria_status = false
      validation_step_1 = prenatal_test.present? rescue false
    when "member"
      # pregnancy = Pregnancy.where(expected_member_id:member.id.to_s).first
      validation_step_1 = pregnancy.blank? 
      criteria_status = !scenario_condition_in_tech.nil?
    end
    if validation_step_1
      scenario_start_date = eval(scenario_detail.start_date).to_date
      current_date =  Time.now.in_time_zone.to_date
      scenario_expire_date = eval(scenario_detail.expire_date).to_date
    
      # check start date and expiry date is valid
      if scenario_start_date <= current_date && scenario_expire_date >= current_date
         
        # check pointer has other check and check is valid
        if criteria_status && scenario_condition_in_tech.present?
          valid_to_assign  = eval(scenario_condition_in_tech)  rescue false
        else
          valid_to_assign = true
        end
      else
        valid_to_assign = false
      end
    else
      valid_to_assign = false
    end
    if valid_to_assign
      # Evaluate scenario title  and  tool topic 
      # Pointer name in case of pointer
      begin
        scenario_title = scenario_detail.title_in_tech.gsub("sleep","sileep")
        if scenario_category == "pregnancy"
          scenario_title = scenario_title.gsub(scenario_category , (scenario_category + "obj"))
        end
        scenario_title =  eval(scenario_title) 
        scenario_title.gsub!((scenario_category + "obj"),scenario_category )
        scenario_title.gsub!("sileep","sleep") rescue nil
      rescue Exception=>e
        scenario_title = scenario_detail.title_in_tech
      end
      tool_topic = nil
      if options[:linked_tool_topic].present?
        # Description in case of pointer
        begin
          linked_tool_topic = options[:linked_tool_topic].gsub("sleep","sileep")
          if scenario_category == "pregnancy"
            tool_topic = linked_tool_topic.gsub(scenario_category, (scenario_category + "obj"))
          end
          tool_topic =  eval(tool_topic) 
          tool_topic.gsub!((scenario_category + "obj"),scenario_category )
          tool_topic.gsub!("sileep","sleep") rescue nil
        rescue Exception=>e
          tool_topic = options[:linked_tool_topic]
        end
      end
      topic_with_linked_tool_id = {}
      if options[:linked_tools_for_topic].present?
        # Description in case of scenario
        options[:linked_tools_for_topic].each do |scenario_linked_to_tool|
          begin
            linked_tool_topic = scenario_linked_to_tool["topic"]
            if linked_tool_topic.blank?
              linked_tool_topic = eval(scenario_linked_to_tool["tool_class"]).find(scenario_linked_to_tool["linked_tool_id"]).title
              linked_to_tool_obj = System::Scenario::ScenarioLinkedToTool.find(scenario_linked_to_tool["_id"])
              linked_to_tool_obj.update_attribute(:topic,linked_tool_topic)
            end
            linked_tool_topic = linked_tool_topic.gsub("sleep","sileep")
            if scenario_category == "pregnancy"
              tool_topic = linked_tool_topic.gsub(scenario_category, (scenario_category + "obj"))
            end
            tool_topic =  eval(tool_topic) 
            tool_topic.gsub!((scenario_category + "obj"),scenario_category )
            tool_topic.gsub!("sileep","sleep") rescue nil
          rescue Exception=>e
            tool_topic = linked_tool_topic || "No data"
          end
          topic_with_linked_tool_id[scenario_linked_to_tool["_id"]] = tool_topic
          tool_topic = nil
        end
      end
      data = {
        :scenario_title=>scenario_title,
        :topic=>  tool_topic,
        :scenario_start_date=>  scenario_start_date ,
        :scenario_peak_relevence_date => eval(scenario_detail.peak_rel_date),
        :scenario_expire_date => scenario_expire_date ,
        :N_factor => (scenario_detail.N_factor rescue 1),
        :category => scenario_detail.category,
        :score=> (scenario_detail.score rescue 0.01),
        :topic_with_linked_tool_id=>topic_with_linked_tool_id,
        :record_id=> (eval(scenario_category).id )
      }
    end
    data
  end   
  

  def linked_items
    data = []
    system_scenario_detail = self
    linked_tools = ::System::Scenario::ScenarioLinkedToTool.where(system_scenario_detail_custom_id:system_scenario_detail.custome_id) 
    linked_tools.each do |linked_tool|
      data << {"topic"=>linked_tool.topic, "tool"=> (linked_tool.tool_type || linked_tool.tool_class),"tool_class"=>linked_tool.tool_class,"tool_id"=>linked_tool.linked_tool_id}
    end
    # linked_pointers = SysArticleRef.where(system_scenario_detail_custome_id:system_scenario_detail.custome_id)
    # linked_pointers.each do |linked_tool|
    #   data << {"topic"=>linked_tool.topic, "tool"=> "pointer","tool_class"=>linked_tool.class.to_s,"tool_id"=>linked_tool.id}
    # end
   data
  end
   
  def self.update_scenario_condition_for_vaccination_category
    sceanrios = System::Scenario::ScenarioDetails.or({:category=>/vaccination/i},{:category=>/immunisation/i},{:category=>/immunisations/i})
    System::Scenario::ScenarioDetails.or({:category=>/vaccination/i},{:category=>/immunisation/i},{:category=>/immunisations/i}).update_all(:category=>"immunisations")
    sceanrios.each do |scenario|
      begin
        status = false
         begin 
          status  = eval(scenario.condition_in_tech).class.to_s != "Hash"
        rescue Exception => e 
          status = true 
        end
        if status
          vaccine_conditions = scenario.condition_in_tech.split("#")
          vaccine_name, jab_due_on_before,country = vaccine_conditions[0].split(":")
          country_filter_list = eval(country)
          jab_due_on_before =  jab_due_on_before || 45.days
          scenario.condition_in_tech = {:vaccin=>vaccine_name,:country=>country_filter_list,:jab_due_on_before=>jab_due_on_before}.to_s
          scenario.save
        end
      rescue Exception=>e 
        Rails.logger.info e.message
        puts e.message
      end
    end  
  end
 
 def self.get_keywords_for_product(options={})
   System::Scenario::ScenarioDetails.all.pluck(:keywords).flatten.uniq
 end

 # System::Scenario::ScenarioDetails.add_existing_pointer_scenario
 def self.add_existing_pointer_scenario
  SysArticleRef.order_by("custome_id asc").all.each do |system_pointer|
    data = {
      :title=> system_pointer["scenarios"],
      :title_in_tech=> system_pointer["name"],
      :score=> system_pointer["score"],
      :N_factor=> system_pointer["N_factor"],
      :start_date=> system_pointer["ref_start_date"],
      :peak_rel_date=> system_pointer["ref_peak_rel_date"],
      :expire_date=> system_pointer["ref_expire_date"],
      :category=> system_pointer["category"],
      :status=> system_pointer["status"],
      :stage=> system_pointer["stage"],
      :condition=> system_pointer["condition"],
      :condition_in_tech=> system_pointer["criteria"],
      :ds=> system_pointer["ds"],
      :dp=> system_pointer["dp"],
      :dexp=> system_pointer["dexp"]
    }
    scenario_detail = System::Scenario::ScenarioDetails.new(data)
    scenario_detail.save
    system_pointer.update_attribute(:system_scenario_detail_custome_id, scenario_detail.custome_id.to_s)
  end
 end

  def self.get_all_keywords 
    all_keywords = []
    System::Scenario::ScenarioDetails.all.entries.each do |scenerio_detail|
      if scenerio_detail.keywords != nil 
        all_keywords.concat(scenerio_detail.keywords)

      end
    end
    all_keywords = all_keywords.map(&:downcase).uniq
    return all_keywords
  end

end