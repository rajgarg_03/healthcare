class System::ScenarioDetails
   include Mongoid::Document  
  include Mongoid::Timestamps
  field :custome_id,                    :type => Integer #auto increamented value for  ScenarioDetails.eg 1,2,3 etc will used for backtracking
  field :title,                         :type => String
  field :description,                   :type => String
  field :tool_type ,                    :type=>  String # eg "question/pointer/topic/" 
  field :tool_subtype ,                 :type=>  String # eg "question/pointer/topic/" 
  field :linked_tool_custome_id,        :type=>  String # custome_id of linked tool with scenario. eg. 1,2,3,etc
  field :stage,                         :type => String # eg, infant,pregnancy
  field :criteria,                      :type => String # eg. "All/any specific milestone acheived etc." 
  field :criteria_in_tech,                      :type => String # eg. "All/any specific milestone acheived etc." 
  field :score,                         :type => String # 1,2...etc
  field :N_factor,                      :type => String # 1,2,3..etc
  field :status,                        :type => String, :default=>"Active"# eg.Active/Draft
  field :ds,                            :type => String # start date eg. Day of milestone achieve
  field :dp,                            :type => String # peak date .eg.  Day of milestone achieve afer 2 days
  field :dexp,                          :type => String # expiry date eg. Day of milestone achieve after 5 days
  field :start_date,                :type => String # start date in techinical eg. "milestone.acheive + 0.days"
  field :peak_rel_date,             :type => String # peak date in techinical eg. "milestone.acheive + 2.days"
  field :expire_date,               :type => String # expiry date in techinical eg. "milestone.acheive + 5.days"
  field :tool_for_criteria,             :type => String #eg. nil/milestones/timeline/measurements/..etc

  index({custome_id:1})
  index({score:1})
  index({status:1})
  index({tool_type:1})
  index({tool_subtype:1})
  index({linked_tool_custome_id:1})
  index({tool_for_criteria:1})

  after_create :assign_custome_id

  def assign_custome_id
    scenario_detail  = self
    scenario_detail[:custome_id] = System::ScenarioDetails.max(:custome_id).to_i + 1
    scenario_detail.save
  end
end