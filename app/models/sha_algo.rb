class ShaAlgo #Encrypt
  require "openssl"
  ALGORITHM = APP_CONFIG["algorithm"]
  def self.encryption(msg)
    begin
      #cipher = OpenSSL::Cipher::AES128.new(ALGORITHM)
      cipher = OpenSSL::Cipher.new 'AES-128-CBC'
      cipher.encrypt 
      cipher.key = APP_CONFIG["rsa_key"]
      cipher.iv = APP_CONFIG["rsa_key"]
      crypt = cipher.update(msg) + cipher.final()
      crypt_string = (Base64.strict_encode64(crypt))
      return crypt_string
    rescue Exception => exc
      puts ("Message for the encryption log file for message #{msg} = #{exc.message}")
      nil
    end
  end

   def self.decryption(msg)
    begin
      # cipher = OpenSSL::Cipher::AES128.new(ALGORITHM)
      cipher = OpenSSL::Cipher.new 'AES-128-CBC'
      cipher.decrypt 
      cipher.key = APP_CONFIG["rsa_key"]
      cipher.iv = APP_CONFIG["rsa_key"]
      tempkey = Base64.decode64(msg)
      crypt = cipher.update(tempkey)
      crypt << cipher.final()
      return crypt
    rescue Exception => exc
      puts ("Message for the decryption log file for message #{msg} = #{exc.message}")
    end
  end


end