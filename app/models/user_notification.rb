class UserNotification
  include Mongoid::Document
  include Mongoid::Timestamps

  # field :child_id, type: String
  field :type, type: String # eg:Pointer/Action/UserAction/birthday_wish/m003 its a identifier for push type
  field :message, type: String 
  field :status, type: String, default: "pending" #push notificatoin  status eg: pending/sent
  field :email_status, type: String, default: "pending" #email notificatoin  status eg: pending/sent
  field :priority, type: Integer #0 for mandatory in batch, 1 pointer and real time,  other are logicwise
  field :identifier, type:String # eg: for pointer s_id: for action card_id
  field :user_id, type:String # user to whom notification sent
  field :score, type: Float # used for pointer to sort,test batch notification with score 11
  field :push_error_message , type:String # error message if notification push fails
  field :notification_expiry_date , type:Date # 
  field :country_code, type:String  
  field :batch_no, type:Integer  
   
  # Deeplink fields (used in mobile app)
  field :content_category, type: String # to show image or video link type "image/video"
  field :content_url, type:String   #link for eg "http://nxyt.tiny.url/images/"
  field :destination , type:String #eg. "pointers", "manage_family","measurment..." this is destination when notification is clicked eg timeline, milestone etc
  field :destination_type , type:String # Eg, "User", Child,Pregnancy
  field :action_name , type:String #Eg Add,Update
  field :family_id , type:String
  field :role , type:String # Son, Daughter,User
  field :feed_id, type:String

  field :category, :type=>String # to identifiy which type of notification.eg measuremnts/manage families, timeline etc 
  field :notify_by, :type => Array # eg. ["email"/ "push"]
  field :schedule_push_date, type:Date # Date on which push has to be sent
  field :client_response_status, type:Boolean # 1/true= yes, 0/false=No
  field :client_response_type, type:Array,default:[] # ["email","push"]
  field :client_response_platform, type:Array,default:[] # ["web","ios","android"] 
  belongs_to :member 
  

  index({user_id:1})
  index({status:1})
  index({type:1})
  index({notification_expiry_date:1})
  index({batch_no:1})
  index({country_code:1})
  index({destination:1})
  index({category:1})


  ToolDestinationNameMappingForHomeAction =  {"AddSpouse"=>"addspouse", "Family" => "family", "picture"=>"add_profile_picture", "measurement"=>"Measurements", "milestone"=>"Milestones", "immunisation" =>"Immunisations", "nutrient"=> "Manage Tools", "timeline"=>"Timeline", "document"=>"Documents","prenatal test"=>"Prenatal Tests"}

  def get_deeplink_data(current_user,api_version=1)
    begin
      user_notification = self
      title = nil
      if user_notification["destination_type"].present?
        destination_in_downcase = user_notification.destination.downcase
        user = current_user.user
        destination_name = destination_in_downcase == "pointer" ? "pointers" : user_notification.destination
        if destination_in_downcase == "immunisations"
          title = Jab.find(user_notification.feed_id).vaccination.get_name rescue nil
        end
        get_tool_id = Nutrient.get_tool_id(destination_name)
        tool = Nutrient.find(get_tool_id) rescue Nutrient.new
        user_notification["destination_type"] = "User" if destination_name.downcase == "pointers"
        if destination_in_downcase == "faq"
          {notification_type:user_notification.type,notification_id:user_notification.id.to_s, tool_id:get_tool_id.to_s,tool_feature_type:tool.feature_type, title:title, email:user.email,member_id: user_notification.member_id.to_s, destination_name:destination_name, authentication_token:user.deeplink_api_token,user_id:user_notification.user_id,role:user_notification.role,feed_id:user_notification.feed_id.to_s,family_id:user_notification.family_id.to_s,action_name:user_notification.action_name,destination_type:user_notification["destination_type"].to_s.downcase}.merge(deeplink_data_for_faq(user_notification.feed_id.to_s))
        elsif destination_in_downcase == "nhs library"
          user_notification["destination_type"] = "User"
          syndicated_record =  user_notification.get_scenario_linked_record(current_user)
          feed_id = syndicated_record.id rescue nil
          # nhs_library_category = syndicated_record.category rescue []

          {notification_type:user_notification.type,notification_id:user_notification.id.to_s,tool_id:get_tool_id.to_s,tool_feature_type:tool.feature_type, title:title, email:user.email,member_id: user_notification.member_id.to_s, destination_name:destination_name, authentication_token:user.deeplink_api_token,user_id:user_notification.user_id,role:user_notification.role,feed_id:feed_id.to_s,family_id:user_notification.family_id.to_s,action_name:user_notification.action_name,destination_type:user_notification["destination_type"].to_s.downcase}
        else
          {notification_type:user_notification.type,notification_id:user_notification.id.to_s,tool_id:get_tool_id.to_s,tool_feature_type:tool.feature_type, title:title, email:user.email,member_id: user_notification.member_id.to_s, destination_name:destination_name, authentication_token:user.deeplink_api_token,user_id:user_notification.user_id,role:user_notification.role,feed_id:user_notification.feed_id.to_s,family_id:user_notification.family_id.to_s,action_name:user_notification.action_name,destination_type:user_notification["destination_type"].to_s.downcase}
        end
      else
        {}
      end
    rescue Exception => e 
      Rails.logger.info "Error in deeplink data"
      Rails.logger.info e.message
      {}
    end
  end

  def deeplink_data_for_faq(child_scenario_id)
    data = {}
    begin
      child_scenario = ::Child::Scenario.where(id:child_scenario_id).last
      if child_scenario.linked_tool_id.blank?
        system_scenario = System::Scenario::ScenarioLinkedToTool.find(child_scenario.scenario_linked_to_tool_id)
        child_scenario.linked_tool_id = child_scenario.linked_tool_id
        child_scenario.save
      end
      tool_class = child_scenario.linked_tool_class
      tool_obj = eval(tool_class).find(child_scenario.linked_tool_id)
      topic_id = nil
      question_id = nil
      sub_topic_id = nil
      faq_screen = nil
      if tool_class == "System::Faq::Question"
        faq_screen = "question"
        question_id = tool_obj.id.to_s
        sub_topic_id = tool_obj.sub_topic_id.to_s
        topic_id = System::Faq::SubTopic.find(sub_topic_id).topic_id.to_s
      elsif tool_class =="System::Faq::SubTopic"
        faq_screen = "sub_topic"
        sub_topic_id = tool_obj.id.to_s
        topic_id = tool_obj.topic_id.to_s
      elsif tool_class == "System::Faq::Topic"
        faq_screen = "topic"
        topic_id = tool_obj.id.to_s
      end
      data = {:faq_question_id=>question_id,:faq_sub_topic_id=>sub_topic_id,:faq_topic_id=>topic_id,:faq_screen=>faq_screen}
    rescue Exception=> e 
      Rails.logger.info "error inside deeplink_data_for_faq"
      Rails.logger.info e.message
      data = {}
    end
    data
  end

  
  def self.notify_by_email_and_push?(notify_by,options={})
    return true if notify_by == "both"
    notify_by.include?("email") && notify_by.include?("push")
  end
  def self.notify_by_push?(notify_by,options={})
    return true if notify_by == "push" || notify_by.blank?
    notify_by.include?("push") && !notify_by.include?("email")
  end
  
  def self.notify_by_email?(notify_by,options={})
    return false if notify_by.blank?
    return true if notify_by == "email"
    notify_by.include?("email") && !notify_by.include?("push") 
  end
  
  def self.prepare_data_for_deeplink(current_user,member_id,destination_name,destination_type,feed_id,action=nil,options={})
    member = Member.find(member_id) rescue nil
    family_id = nil
    family_member = member.family_members.first rescue nil
     
    if family_member.present?
      family_id = family_member.family_id
      
      if member.user_id.present? 
        role =  FamilyMember.where(family_id:family_id,member_id:member.id).last.role
      else
        role = member.is_expected? ? "Pregnancy" : "Child"
      end
    else
      role = "User"
    end
    if family_id.blank?
      family_id = options[:family_id] if (options.present? && options[:family_id].present?)
    end

    destination_type = destination_type || role
    if options[:push_type] == "pointers"
      feed_id = ArticleRef.where(s_id: options[:unique_id]).last.id rescue feed_id
    end
    { role:role,feed_id:feed_id,family_id:family_id,action_name:action,destination:destination_name, destination_type:destination_type.downcase}
  end

  def self.add_pointer_notification(current_user,batch_notification=nil, pointer_limit=2,options={})
    begin
      user_id = current_user.user.id
      batch_notification = batch_notification.present? ? batch_notification : BatchNotification::BatchNotificationManager.where(identifier:/^pointer/i,status:"Active").last
      if batch_notification.blank? #|| !batch_notification.is_valid_notification?
        Rails.logger.info "Pointer batch notification is not in active status"
        return false
      end
      type = batch_notification.identifier
      category = batch_notification.category # pointers
      destination_type = "user"
      destination_name = batch_notification.destination # pointers
      batch_no = batch_notification.batch_no
      pointer_limit = pointer_limit # 2
      priority = batch_notification.priority
      count = 0
      childern_ids = current_user.unordered_children_ids
     
      # Delete all pending  assign pointer notification 
      UserNotification.where(:type=>type,user_id:user_id).where(:status.in=>["pending",nil],:email_status.in=>["pending",nil]).delete_all
     
      # Find all sent pointer notification to user
      sent_notification_pointer_ids = UserNotification.where(user_id:user_id,type:type).or({status:"sent"},{email_status:"sent"}).pluck(:identifier).map(&:to_s)
     
      # Get top score pointer 
      top_pointers = ArticleRef.in(member_id:childern_ids).or({notification_expiry_date:nil},{:notification_expiry_date.gte=>Date.today}).order_by("effective_score DESC").no_timeout.limit(100).pluck(:s_id).map(&:to_s)
      
      NotificationTrackingLogger.info "......No top pointer for #{user_id} on #{Date.today}  ......" if top_pointers.blank?

      final_top_pointer_list  =  (top_pointers - sent_notification_pointer_ids)
      country_code =  (current_user.user.country_code rescue "uk")
      title_prefix =  batch_notification.title_prefix.to_s
      
      if final_top_pointer_list.count < 1
        NotificationTrackingLogger.info "......No final pointer for #{user_id}  #{Date.today}  ......"
      end
      
      final_top_pointer_list.each do |pointer_sid|
        begin
          pointer = ArticleRef.where(s_id:pointer_sid).last
          message = (pointer.name + "\n" + pointer.description)
          notification_expiry_date = pointer.notification_expiry_date
          feed_id = pointer.id
          tool_id = feed_id
          identifier = pointer.s_id
          action_type = batch_notification.action
          child_id = pointer.member_id.to_s
          obj = UserNotification.add_push(batch_notification,current_user,type,batch_no,priority,message,destination_name,child_id,action_type="add",content_url=nil,tool_id,notification_expiry_date,score=pointer.effective_score,identifier,options)

          if obj.present?
            count = count + 1
          else
            # Log user to whom notification not being assign
            gap_in_notification = batch_notification.check_sent_in_x_day.to_i
            if gap_in_notification > 0
              not_assinged_before = UserNotification.where(user_id:user_id,:notification_expiry_date.gte=>(Date.today - gap_in_notification.day)).blank?
              if not_assinged_before
                NotificationTrackingLogger.info "Pointer not assign #{Date.today} and #{Date.today - gap_in_notification.day} for user id #{user_id}"
              end
            end
          end
        rescue Exception=>e
          NotificationTrackingLogger.info "......Error inside add_pointer_notification #{user_id} on  #{Date.today}  is #{e.message}......"
          # Rails.logger.info e.message
        end
         break if count >= pointer_limit
      end
    rescue Exception=>e
      NotificationTrackingLogger.info "....Error inside add_pointer_notification getting down  on  #{Date.today} die to error  #{e.message}....for user id #{current_user.user_id}.."
      # Rails.logger.info e.message
    end
  end

def get_scenario_linked_record(current_user,options={})
  user_notification = self
  scenario = ::Child::Scenario.find user_notification.feed_id
  if scenario.linked_tool_id.blank?
    record_detail = System::Scenario::ScenarioLinkedToTool.find scenario.scenario_linked_to_tool_id
    scenario.linked_tool_id = record_detail.linked_tool_id
    scenario.save
  end
  eval(scenario.linked_tool_class).find(scenario.linked_tool_id) rescue nil
end

#  Add push for FAQ, NHS-SyndicatedContent etc
def self.add_scenario_notification(current_user,batch_notification=nil, scenario_limit=2,options={})
    begin
      user = current_user.user
      user_id = user.id
        batch_notification = batch_notification.present? ? batch_notification : BatchNotification::BatchNotificationManager.where(identifier:/^pointer/i,status:"Active").last
        if batch_notification.blank? #|| !batch_notification.is_valid_notification?
          Rails.logger.info "Scenario notification is not in active status"
          return false
        end
      scenario_for_tool = options[:scenario_tool_class]
      type = batch_notification.identifier
      category = batch_notification.category # pointers
      destination_type = "user"
      destination_name = batch_notification.destination # pointers
      batch_no = batch_notification.batch_no
      scenario_limit = scenario_limit # 2
      priority = batch_notification.priority
      count = 0
      childern_ids = current_user.unordered_children_ids
      UserNotification.where(user_id:user_id,type:type,:status.in=>["pending",nil],:email_status.in=>["pending",nil]).delete_all
      sent_notification_scenario_ids = UserNotification.where(user_id:user_id,type:type).or({:status=>"sent"},{:email_status=>"sent"}).pluck(:identifier)
      if scenario_for_tool.present?
        #TO be fixed at assign scenario 
        if scenario_for_tool == "SysArticleRef"
          system_pointer_custome_ids = options[:system_pointer_custome_ids]
          top_scenario = ::Child::Scenario.where(:linked_tool_class=>scenario_for_tool).in(member_id:childern_ids).where(:expire_date.gte=>Date.today).or({notification_expiry_date:nil},{:notification_expiry_date.gte=>(Date.today)}).order_by("effective_score DESC").no_timeout.limit(40).map{|a| system_pointer_custome_ids[a["linked_tool_id"]].to_s + "_" +  a.member_id}.uniq
        else
          top_scenario = ::Child::Scenario.where(:linked_tool_class=>scenario_for_tool).in(member_id:childern_ids).where(:expire_date.gte=>Date.today).or({notification_expiry_date:nil},{:notification_expiry_date.gte=>(Date.today)}).order_by("effective_score DESC").no_timeout.limit(40).pluck(:s_id).uniq
        end
      else
        top_scenario = ::Child::Scenario.in(member_id:childern_ids).where(:expire_date.gte=>Date.today).or({notification_expiry_date:nil},{:notification_expiry_date.gte=>(Date.today)}).order_by("effective_score DESC").no_timeout.limit(40).pluck(:s_id).map(&:to_s)
      end
      if top_scenario.blank?
        NotificationTrackingLogger.info  "No #{scenario_for_tool} top scenario found for user id: #{user_id} on #{Time.now}"
        return false
      end
      title_prefix = batch_notification.title_prefix.to_s
      final_top_scenario_list  =  (top_scenario - sent_notification_scenario_ids)
      if final_top_scenario_list.blank?
        NotificationTrackingLogger.info  "No #{scenario_for_tool} final top scenario found for user id: #{user_id}  on #{Time.now}"
        return false
      end
      country_code =  (user.country_code rescue "uk")
      final_top_scenario_list.each do |scenario_sid|
        begin
          if scenario_for_tool == "SysArticleRef"
            pointer = ArticleRef.where(s_id:scenario_sid).last
            scenario = ::Child::Scenario.where(linked_tool_id:pointer.sys_article_ref_id, member_id:pointer.member_id,linked_tool_class: "SysArticleRef" ).last
          else
            scenario = ::Child::Scenario.where(s_id:scenario_sid).last
          end
          message = (scenario.scenario_title + "\n" + scenario.topic)
          child_id = scenario.member_id.to_s
          notification_expiry_date = Date.today#scenario.notification_expiry_date rescue Date.today
          feed_id = scenario.id
          tool_id = feed_id
          if scenario[:linked_tool_class] == "SysArticleRef"
            pointer_custome_id = system_pointer_custome_ids[scenario[:linked_tool_id].to_s] || SysArticleRef.find(scenario[:linked_tool_id]).custome_id
            identifier = pointer_custome_id.to_s + "_" + child_id.to_s
          else
            identifier = scenario.s_id
          end
          action_type = batch_notification.action
          obj = UserNotification.add_push(batch_notification,current_user,type,batch_no,priority,message,destination_name,child_id,action_type="add",content_url=nil,tool_id,notification_expiry_date,score=scenario.effective_score,identifier,options)

          if obj.present?
            count = count + 1
          else
            NotificationTrackingLogger.info  "No #{scenario[:linked_tool_class]} notification assign for user id: #{user_id} on #{Time.now}"
          end
          # Exit if user cretieria is not valid to assgin notification
          break if !obj.present?

        rescue Exception=>e
          NotificationTrackingLogger.info  "Error inside final_top_scenario_list for user id: #{user_id} on #{Time.now} : #{e.message}"
        end
         break if count >= scenario_limit
      end
    rescue Exception=>e
      NotificationTrackingLogger.info  "Error inside add_scenario_notification for user id: #{user_id} on #{Time.now} : #{e.message}"
    end
  end


def check_expiry_date_before_push
  user_notification = self
  if user_notification['notification_expiry_date'].blank? || (user_notification["notification_expiry_date"] >= Date.today)
    return true
  else
    return false
  end
end

def validate_push_count_before_sent?(options={})
 status = false
 user_notification = self
 tool_name = user_notification.tool_name
 if UserNotification.notify_by_email?(options[:user_notification_type])
    push_sent_data_record = UserNotification::EmailDataForValidation.where(user_id:user_notification.user_id,created_at:Date.today).last || UserNotification::EmailDataForValidation.update_data(user_notification.user_id,tool_name,true)
    allowed_notification_count = GlobalSettings::MaxAllowedEmailCountTypeWise[tool_name] || GlobalSettings::DefaultMaxAllowedEmailCountTypeWise
 else
    push_sent_data_record = UserNotification::PushDataForValidation.where(user_id:user_notification.user_id,created_at:Date.today).last || UserNotification::PushDataForValidation.update_data(user_notification.user_id,tool_name,true)
    allowed_notification_count = GlobalSettings::MaxAllowedPushCountTypeWise[tool_name] || GlobalSettings::DefaultMaxAllowedPushCountTypeWise
 end
 # check for total push notification sent to user
 if push_sent_data_record.remaining_push_count_to_sent > 0
    # check for tool_type["measuremnet","timeline"..etc] push notification sent to user
   if user_notification.priority == 0
     status = true
   else
     status =  push_sent_data_record[:sent_push_count_by_type][tool_name].to_i <  allowed_notification_count
   end
 end
 status
end


def tool_name
  self.category.downcase  rescue nil
end




def valid_to_push?(options={})
   status = false
   user_notification = self
   user_notification_type = options[:user_notification_type].present? ? options[:user_notification_type] : "push" 
   user = User.find(user_notification.user_id.to_s)
   current_user = user.member
   # check max allowed push count
   begin 
      batch_notification_manager = BatchNotification::BatchNotificationManager.find_active_notification(user_notification.type,options) || BatchNotification::BatchNotificationManager.new
      return false if !batch_notification_manager.valid_total_allowed_count?(current_user,{:user_notification_type=>user_notification_type, :check_before_sent=> true}.merge(options))
    rescue Exception => e 
      Rails.logger.info "unable to validate max push count for notification id -#{user_notification.id}"
      Rails.logger.info  e.message
    end
   # user is sigin with enabled push notification setting/ email subscription setting
   if UserNotification.notify_by_email?(user_notification_type)
     is_valid_user_to_send  =  (user_notification.email_status == "pending" && user.is_valid_user_to_send_email?)
    else
     is_valid_user_to_send  = (user_notification.status == "pending" && user.is_valid_user_to_send_push?)
   end
   NotificationTrackingLogger.info "User id  #{user_notification.user_id} not valid for notification....." if !is_valid_user_to_send
   if is_valid_user_to_send
     # check push notification expiry date validity 
     if check_expiry_date_before_push
        all_tool_name_list = Nutrient.in(tool_for:["child","CP","pregnancy"]).pluck(:identifier).map(&:downcase)
        all_tool_name_list = all_tool_name_list + ["pointers", "manage tools","home","manage family"]
        tool_name = user_notification.tool_name
        # check if user has activated tool for which push notification to be send 
        if (options[:notification_type].present? && options[:notification_type].downcase == "instant") 
          # don't check max limit for real time/user action notitification
          status = true
        elsif all_tool_name_list.include?(tool_name)
          # check user did not receive push notification more then allowed push notification  
            status  = user_notification.validate_push_count_before_sent?(options)
            if status
              status = current_user.has_valid_tool_and_subscription(batch_notification_manager=nil,nil,nil,{:user_notification_obj=>user_notification})
            end          
        else
          # if no tool limit define for selected tool then check max allowed push count 
          if UserNotification.notify_by_email?(user_notification_type)
            push_sent_data_record = UserNotification::EmailDataForValidation.where(user_id:user_notification.user_id,created_at:Date.today).last
          else
            push_sent_data_record = UserNotification::PushDataForValidation.where(user_id:user_notification.user_id,created_at:Date.today).last
          end
          status = push_sent_data_record.blank? ? true : push_sent_data_record.remaining_push_count_to_sent > 0
          if status
            status = current_user.has_valid_tool_and_subscription(batch_notification_manager=nil,nil,nil,{:user_notification_obj=>user_notification})
          end 
        end
     end
   end
   status
  end



def self.not_expired 
  self.or({:notification_expiry_date.gte=>Date.today},{:notification_expiry_date=>nil})
end

def self.user_notifiy_by(category="push")
  if category == "push"
    self.where(:notify_by.in=>["both",category,nil])
  else
    self.where(:notify_by.in=>["both",category])
  end
end

def self.send_push_notification_by_batch(batch_no,country_code=nil,options={})
  instant_push_identifier = BatchNotification::BatchNotificationManager.where(push_type:"Instant").pluck(:identifier)
  instant_push_identifier = instant_push_identifier + ["pointers","user_action"]
  if country_code.present?
    if country_code == "other" 
      country_code = GlobalSettings::CountryListForPushNotification
      user_ids = UserNotification.where(batch_no:batch_no).or({:status=>"pending"},{:email_status=>"pending"}).not_expired.not_in(:country_code=>country_code).pluck(:user_id).uniq
    else
      country_code = Member.sanitize_country_code(country_code) 
      user_ids = UserNotification.where(batch_no:batch_no ).or({:status=>"pending"},{:email_status=>"pending"}).not_expired.in(:country_code=> country_code).pluck(:user_id).uniq
    end
  else
    user_ids = UserNotification.where(batch_no:batch_no).or({:status=>"pending"},{:email_status=>"pending"}).not_expired.pluck(:user_id).uniq
  end
  if options.present? && options[:user_ids].present?
    user_ids = options[:user_ids]
  end
  User.in(id:user_ids).batch_size(100).no_timeout.each do |user|
    begin
      if user.is_valid_user_to_send_push?
        UserNotification.push_notification_by_priority(user,batch_no,instant_push_identifier,options)
      end
      if user.is_valid_user_to_send_email?

          UserNotification.email_notification_by_priority(user,batch_no,instant_push_identifier,options)
      end
    rescue Exception=>e
      if options[:score] == 11
        UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside send_push_notification_by_batch")
      end
      Rails.logger.info e.message
    end
  end
   
end

  def self.push_notification_by_priority(user,batch_no,instant_push_identifier,options={})
    current_user = user.member
    # get all push notification to be send- pending push in selected batch no
    pending_user_notifications = UserNotification.where(batch_no:batch_no,user_id:current_user.user_id.to_s,status:"pending").user_notifiy_by("push").not_expired.order_by("priority asc").no_timeout
    return nil if pending_user_notifications.blank?
    # list all push notification assigned to user- pending + sent. Exclude pointer type having priority 1
    all_assigned_user_notification = UserNotification.where(user_id:current_user.user_id.to_s).user_notifiy_by("push").not_expired.not_in(type:instant_push_identifier).order_by("priority asc")
    #all_assigned_user_notification = UserNotification.where(user_id:current_user.user_id.to_s).not_expired.not_in(type:instant_push_identifier).order_by("priority asc").entries
     
    detail_data = {}
    # get ordered ids of push notification to push according to priority 
    all_assigned_user_notification.no_timeout.each do |a|
      detail_data[a.tool_name]||= []
      detail_data[a.tool_name] << a.id.to_s
    end
    pointer_pushed_count = 0
    action_for_action_pushed_count = 0
    tool_list = Nutrient.in(tool_for:["child","CP","pregnancy"]).pluck(:identifier).map(&:downcase) + ["manage family", "manage tools", "home","nhs library"]
    pending_user_notifications.each do |user_notification|
    active_batch_notification = BatchNotification::BatchNotificationManager.find_active_notification(user_notification.type,options)
      # active and valid to push on week day
      if active_batch_notification.present? && active_batch_notification.is_valid_notification?
        tool_name = user_notification.tool_name
          # Push notification for pointer type
        if pointer_pushed_count < GlobalSettings::MaxAllowedPushCountTypeWise["pointers"] && (user_notification.type == "pointer" || user_notification.type == "pointers")
          UserNotification.push_notification(user_notification,current_user,pusher=nil,devices=nil,fcm_pusher=nil,options) rescue Rails.logger.info "notification id #{user_notification.id} not sent"
          pointer_pushed_count = pointer_pushed_count + 1
          # Push notification for action for home  type
        elsif action_for_action_pushed_count < GlobalSettings::MaxAllowedPushCountTypeWise["action_for_home"] && user_notification.type == "action_for_home" 
          UserNotification.push_notification(user_notification,current_user,pusher=nil,devices=nil,fcm_pusher=nil,options) rescue Rails.logger.info "notification id #{user_notification.id} not sent"
          action_for_action_pushed_count = action_for_action_pushed_count + 1
        # send push notification without check type count if push notification is mandatory or not in tool list
        elsif user_notification.priority == 0  || (!tool_list.include?(tool_name) && user_notification.tool_name != "pointers")
          UserNotification.push_notification(user_notification,current_user,pusher=nil,devices=nil,fcm_pusher=nil,options) rescue Rails.logger.info "notification id #{user_notification.id} not sent"
        else 
           # check if there is no other push notification with high priority in allowed push count
          if detail_data[tool_name].present? && detail_data[tool_name].index(user_notification.id.to_s) <= (GlobalSettings::MaxAllowedPushCountTypeWise[tool_name].to_i - 1)
            UserNotification.push_notification(user_notification,current_user,pusher=nil,devices=nil,fcm_pusher=nil,options) rescue Rails.logger.info "notification id #{user_notification.id} not sent"
          end
        end
      end
    end
  end

  def self.email_notification_by_priority(user,batch_no,instant_push_identifier,options)
    current_user = user.member
    # get all push notification to be send- pending push in selected batch no
    pending_user_notifications = UserNotification.where(batch_no:batch_no,user_id:current_user.user_id.to_s,email_status:"pending").user_notifiy_by("email").not_expired.order_by("priority asc").no_timeout
    return nil if pending_user_notifications.blank?
    # list all push notification assigned to user- pending + sent. Exclude pointer type having priority 1
    all_assigned_user_notification = UserNotification.where(user_id:current_user.user_id.to_s).user_notifiy_by("email").not_expired.not_in(type:instant_push_identifier).order_by("priority asc").no_timeout

    #all_assigned_user_notification = UserNotification.where(user_id:current_user.user_id.to_s).not_expired.not_in(type:instant_push_identifier).order_by("priority asc").entries
    detail_data = {}
    # get ordered ids of push notification to push according to priority 
    all_assigned_user_notification.each do |a|
      detail_data[a.tool_name]||= []
      detail_data[a.tool_name] << a.id.to_s
    end
    pointer_pushed_count = 0
    action_for_action_pushed_count = 0
    tool_list = Nutrient.in(tool_for:["child","CP","pregnancy"]).pluck(:identifier).map(&:downcase) + ["manage family","manage tools","home"]
    pointer_pushed_count = 0 
    action_for_action_pushed_count = 0
    pending_user_notifications.each do |user_notification|
      if BatchNotification::BatchNotificationManager.find_active_notification(user_notification.type,options).present?
        tool_name = user_notification.tool_name
        if pointer_pushed_count < GlobalSettings::MaxAllowedEmailCountTypeWise["pointers"] && (user_notification.type == "pointer" || user_notification.type == "pointers")
          # Push email notification for pointers
          UserNotification.send_email_notification(user_notification,current_user,options) rescue Rails.logger.info "notification id #{user_notification.id} email not sent"
          pointer_pushed_count = pointer_pushed_count + 1
          # Push email notification for action for home  type
        elsif action_for_action_pushed_count < GlobalSettings::MaxAllowedEmailCountTypeWise["action_for_home"] && user_notification.type == "action_for_home" 
          UserNotification.send_email_notification(user_notification,current_user,options) rescue Rails.logger.info "notification id #{user_notification.id} email not sent"
          action_for_action_pushed_count = action_for_action_pushed_count + 1
        
        elsif user_notification.priority == 0  || (!tool_list.include?(tool_name) && user_notification.tool_name != "pointers")
          UserNotification.send_email_notification(user_notification,current_user,options) rescue Rails.logger.info "notification id #{user_notification.id} not sent"
        else 
           # check if there is no other push notification with high priority in allowed push count
          if detail_data[tool_name].present? && detail_data[tool_name].index(user_notification.id.to_s) <= (GlobalSettings::MaxAllowedEmailCountTypeWise[tool_name].to_i - 1)
            UserNotification.send_email_notification(user_notification,current_user, options) rescue Rails.logger.info "notification id #{user_notification.id} not sent"
          end
        end
      end
    end
  end

  def self.send_email_notification(user_notification,current_user,options={})
    begin
      # current_sign_in = current_user.user.last_sign_in_at
      return nil if(UserNotification.notify_by_push?(user_notification.notify_by)|| user_notification.notify_by.blank?)
      type = user_notification.type
      push_category = user_notification["content_url"].present? && user_notification.type == "birthday_wish" ? "image_content" : type
      options[:user_notification_type] = "email"
      if user_notification.valid_to_push?(options)
        # deep_link_data = user_notification.get_deeplink_data(current_user) rescue {}
        email_data = UserNotification::EmailDataToSend.where(user_notification_id:user_notification.id.to_s,user_id:user_notification.user_id,member_id:user_notification.member_id).last
        if email_data.present?
          BatchMailer.trigger_batch_mail(email_data,current_user).deliver
          user_notification.email_status = "sent"
          user_notification["notification_expiry_date"] = Time.now# if user_notification["notification_expiry_date"].blank?
          user_notification.save
          
          tool_name_in_downcase = user_notification.tool_name
          tool_name = GlobalSettings::MaxAllowedPushCountTypeWise[tool_name_in_downcase].present? ? tool_name_in_downcase : ""
          # don't update sent push count if notification push type is instant/pointer/realtime 
          if (options[:notification_type].blank? ||  options[:notification_type].downcase != "instant") 
            UserNotification::EmailDataForValidation.update_data(user_notification.user_id,tool_name)
          end
          msg = user_notification.message
          notification_msg = msg.gsub("\\n","") rescue ""
          
          notification = Notification.where(member_id:current_user.id,user_notification_id:user_notification.id, type:type).last
          if notification.blank?  
            if type == "action_for_home"
              tools = {"family"=>"manage family", "PrenatalTest"=> "medical_event","measurement"=>"health","vaccination"=>"vaccination", "timeline"=>"timeline","timelines"=>"timeline","milestone"=>"milestones","document"=>"documents","picture"=>"picture","manage family"=>"family", "Measurements"=>"health", "Milestones"=>"milestone", "Immunisations"=>"vaccination", "Manage Tools"=>"nutrient", "Timeline"=>"timeline", "Documents"=>"documents", "Prenatal Tests"=>"medical_event"}
              destination_name = tools[user_notification.destination] || user_notification.destination 
              link = "/families/family/members/#{user_notification.member_id}/#{destination_name}"
            else
              link = "/"
            end

            current_user.notifications.create(user_notification_id:user_notification.id, type:type,:title => notification_msg, :link => link, sender: current_user.id,child_member_id:user_notification.member_id)
          else
            notification
          end
        end
      end
    rescue Exception => e 
      if options[:score] == 11
        UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside push email notification")
      end
      Rails.logger.info "Error inside push email notification"
      Rails.logger.info e.message
    end 
  end

def self.push_notification(user_notification,current_user,pusher=nil,devices=nil,fcm_pusher=nil,options={})
    return nil if UserNotification.notify_by_email?(user_notification.notify_by)
    tools = {"family"=>"manage family", "PrenatalTest"=> "medical_event","measurement"=>"health","vaccination"=>"vaccination", "timeline"=>"timeline","timelines"=>"timeline","milestone"=>"milestones","document"=>"documents","picture"=>"picture","manage family"=>"family", "Measurements"=>"health", "Milestones"=>"milestone", "Immunisations"=>"vaccination", "Manage Tools"=>"nutrient", "Timeline"=>"timeline", "Documents"=>"documents", "Prenatal Tests"=>"medical_event"}
    pusher = pusher || UserNotification.notification_pusher
    current_sign_in = current_user.user.last_sign_in_at
    unread_notification =  Notification.where(:created_at.gte=>current_sign_in,member_id:current_user.id, checked:false).count rescue 0
    unread_notification = unread_notification + 1
    msg = user_notification.message
    type = user_notification.type
    devices = devices || current_user.devices.order_by("id desc")
    push_category = user_notification["content_url"].present? && user_notification.type == "birthday_wish" ? "image_content" : type
    if devices.present? && user_notification.valid_to_push?(options)
      deep_link_data = user_notification.get_deeplink_data(current_user) rescue {}
      if devices.last.platform == "android"
        fcm = fcm_pusher || Fcm.new(APP_CONFIG["fcm_key"])
        registration_ids = [devices.last.token] # an array of one or more client registration tokens
        if deep_link_data.present?
          data_options = { data: {title: nil, body: msg , notification_type:type,deeplink_params:deep_link_data,content_url:user_notification["content_url"] }}
        else
          data_options = { data: {title: nil, body: msg , notification_type:type,content_url:user_notification["content_url"] }}
        end
        response = fcm.send(registration_ids, data_options)
      else
        device = devices.first
        if deep_link_data.present?
          custome_msg = {notification_type: type,deeplink_params:deep_link_data, content_url:user_notification["content_url"]}
        else
          custome_msg = {notification_type: type,content_url:user_notification["content_url"]}
        end
        notification = Grocer::Notification.new(
          device_token: device.token,
          alert: msg,
          custom:custome_msg,
          badge: unread_notification,
          category: push_category, # optional; used for custom notification actions
          sound: "siren.aiff", # optional
          expiry: Time.now + 60*60, # optional; 0 is default, meaning the message is not stored
          identifier: 1234, # optional; must be an integer
          content_available: true # optional; any truthy value will set 'content-available' to 1
        )
         pusher.push(notification)
      end
      user_notification.status = "sent"
      user_notification["notification_expiry_date"] = Time.now# if user_notification["notification_expiry_date"].blank?
      user_notification.save
      
      tool_name_in_downcase = user_notification.tool_name
      tool_name = GlobalSettings::MaxAllowedPushCountTypeWise[tool_name_in_downcase].present? ? tool_name_in_downcase : ""
      # don't update sent push count if notification push type is instant/pointer/realtime 
      if (options[:notification_type].blank? ||  options[:notification_type].downcase != "instant") 
        UserNotification::PushDataForValidation.update_data(user_notification.user_id,tool_name)
      end
     
      notification_msg = msg.gsub("\\n","")
      notification = Notification.where(member_id:current_user.id,user_notification_id:user_notification.id, type:type).last
      if notification.blank?
        if type == "action_for_home"
          destination_name = tools[user_notification.destination] || user_notification.destination 
          link = "/families/family/members/#{user_notification.member_id}/#{destination_name}"
        else
          link = "/"
        end

        current_user.notifications.create(user_notification_id:user_notification.id, type:type,:title => notification_msg, :link => link, sender: current_user.id,child_member_id:user_notification.member_id)
      else
        notification
      end
    end 
  end


  def self.notification_pusher
     Grocer.pusher(certificate: APP_CONFIG["grocer_certificate_file"],passphrase:APP_CONFIG["grocer_passphrase"],gateway: APP_CONFIG["grocer_gateway"],port: 2195,retries: 3)
  end
  
  
  # Add Action card in user notification
  def self.add_home_action(current_user,notification=nil, api_version=1)
    destination_name_mapping = UserNotification::ToolDestinationNameMappingForHomeAction #{"picture"=>"Profil Pic", "measurement"=>"Measurements", "milestone"=>"Milestones", "immunisation" =>"Immunisations", "nutrient"=> "Manage Tools", "timeline"=>"Timeline", "document=>Documents","prenatal test"=>"Prenatal Tests"}
    user = current_user.user
    identifier = "action_for_home"
    batch_notification = notification.present? ? notification  : BatchNotification::BatchNotificationManager.find_active_notification(identifier)
    if batch_notification.blank? || !batch_notification.is_valid_notification?
      Rails.logger.info "Action for home batch notification is not in active state"
      return false
    end 
    type = identifier
    batch_no = batch_notification.batch_no
    api_version = api_version || (member.devices.last.app_version || 1) rescue 1
    dashboard_data = current_user.api_dashboard(true,api_version)#.first
    count = 0
    UserNotification.where(user_id:user.id,type:type,status:"pending").delete_all
    dashboard_data.each do |data|
      identifier = (data[:family_id] || data[:member_id]|| data[:identifier]) rescue nil
      destination = destination_name_mapping[data[:type]] rescue nil
      feed_id = nil
 
      if destination.downcase == "family" || destination.downcase == "addspouse" || destination.downcase == "add_profile_picture"
        identifier =  identifier.to_s.match(/^(\d)+$/).present? ? nil : identifier
        role = FamilyMember.where(family_id:data[:family_id],member_id:current_user.id).last.role rescue "User"
        role = "User" if role == "Creator"
        if destination.downcase == "addspouse"
          role = (["Mother", "Father"] - [role]).first 
          data[:notific_msg] = data[:line2]  
        end
        if destination.downcase == "add_profile_picture"
          destination = "Manage Family"
          data[:button] = "add_profile_picture"
          @member_id = data[:member_id]
          identifier =  FamilyMember.where(member_id:@member_id).pluck(:family_id).last
        end
        deep_link_data = {role:role,member_id: @member_id, family_id:identifier,action_name:data[:button].downcase,destination:"Manage Family",destination_type:"User"}
      else
        member_id = identifier
        family_id = data[:family_id]
        options = {:family_id=>family_id}
        deep_link_data = UserNotification.prepare_data_for_deeplink(current_user,member_id,destination,@destination_type=nil,feed_id,"add",options)
      end
      priority = batch_notification.priority
      batch_no = batch_notification.batch_no
      score = 1
      tool_id = feed_id
      notification_expiry_date = Date.today
      push_message = data[:notific_msg]
      child_id = data[:member_id]
      action_type ="add"
      obj = UserNotification.add_push(batch_notification,current_user,type,batch_no,priority,push_message,destination,child_id,action_type,content_url=nil,tool_id,notification_expiry_date,score,identifier)

      if obj.present?
         obj.update_attributes(deep_link_data.merge(category:destination))
        count = count+1
      end
      break if count == (GlobalSettings::MaxAllowedPushCountTypeWise["action_for_home"] || 1)
    end
  end

  # instant Push on user action eg join/accept family request
  def self.add_user_action_in_notification(current_user,user_action_object,category=nil,push_identifier=nil,options={})
    begin
      push_type = push_identifier.present? ? push_identifier : "user_action"
      batch_notification_manager = BatchNotification::BatchNotificationManager.find_active_notification(push_type,options) 
       
      feed_id = user_action_object.unique_id.split("_")[0]
      batch_no = batch_notification_manager.batch_no rescue  nil 
      priority = batch_notification_manager.priority rescue 1
      push_message = user_action_object.message
      destination_name = user_action_object.action_for_tool
      category = batch_notification_manager.destination rescue (category || destination_name)
      action_type =user_action_object.action_type
      content_url = nil
      child_id = user_action_object.child_id
      notification_expiry_date = Date.today
      batch_notification_manager =  batch_notification_manager.present? ? batch_notification_manager : BatchNotification::BatchNotificationManager.new(category:category, check_sent_in_x_day:"NA",check_sent_before:false)
      options[:family_id] = user_action_object.family_id
      obj = UserNotification.add_push(batch_notification_manager,current_user,push_type,batch_no,priority,push_message,destination_name,child_id,action_type,content_url,feed_id,notification_expiry_date,score=1,identifier=nil,options)
      if obj.present? 
        notify_by = (batch_notification_manager.notify_by || "push") rescue "push"
        # send realtime notification 
        if UserNotification.notify_by_push?(notify_by) || UserNotification.notify_by_email_and_push?(notify_by)
          UserNotification.push_notification(obj,current_user,nil,nil,nil,{notification_type:"instant"}.merge(options)) rescue Rails.logger.info "Push not sent for id #{obj.id}"
        end
        
        if UserNotification.notify_by_email?(notify_by) || UserNotification.notify_by_email_and_push?(notify_by)
          UserNotification.delay.send_email_notification(obj,current_user, {notification_type:"instant"}.merge(options)) rescue Rails.logger.info "notification id #{obj.id} not sent"
        end
 
      end

    rescue Exception => e 
      Rails.logger.info e.message
    end
  end

  # TODO: add to admin panel
  def self.add_birthday_wish_in_notification(current_user,birthday_wish_object,push_msg_for_birthday_next_day=false)
    push_type = "birthday_wish"
    batch_notification_manager = BatchNotification::BatchNotificationManager.where(identifier:push_type).last
    batch_no = batch_notification_manager.batch_no rescue nil
    priority = batch_notification_manager.priority rescue 4
    destination_name = batch_notification_manager.destination  rescue "Timeline"
    if birthday_wish_object.system_timeline_id.present?
      system_timeline = SystemTimeline.find(birthday_wish_object.system_timeline_id)
      @birthday_pic =  system_timeline.get_birthday_image
      @feed_id = Timeline.where(timelined_id:birthday_wish_object.system_timeline_id,member_id:birthday_wish_object.member_id).last.id rescue nil
    end
    # tool_id  = @feed_id
    @birthday_pic = nil if birthday_wish_object.system_timeline_id.blank?
    child_id  = birthday_wish_object.member_id
    push_message = push_msg_for_birthday_next_day ? birthday_wish_object.notification_message_for_schedule_push : birthday_wish_object.notification_message_for_instantly_push
    notification_expiry_date = Date.today
    action_type = batch_notification_manager.action rescue nil
    content_url = @birthday_pic
    batch_notification_manager =  batch_notification_manager.present? ? batch_notification_manager : BatchNotification::BatchNotificationManager.new(category:destination_name, check_sent_in_x_day:"NA",check_sent_before:false)
     UserNotification.add_push(batch_notification_manager, current_user,push_type,batch_no,priority,push_message,destination_name,child_id,action_type,content_url,@feed_id,notification_expiry_date)
  end
  
    def self.is_valid_push_to_add(batch_notification_manager,current_user,push_type,priority,destination_name,child_id,destination_type,feed_id=nil,options={})  
      tool_name = destination_name
      user_id = current_user.user_id
      user = current_user.user
      type = push_type
      # feed_id = tool_id
      identifier = options[:unique_id]
      check_sent_before_status = false
      sent_in_x_day_status = false
      notification_exist_to_push = false
      user_has_valid_signup_tenure =  true
      user_notification_type = options[:user_notification_type].blank? ? "push" : options[:user_notification_type]
      # check if same User Notification is pending to push
      if UserNotification.notify_by_email?(user_notification_type)
        notification_exist_to_push =  UserNotification.where(email_status:"pending",feed_id:feed_id,type:type,member_id:child_id,user_id:user_id,:notification_expiry_date.gte=>Date.today).first.present?
      else
        notification_exist_to_push =  UserNotification.where(status:"pending",feed_id:feed_id,type:type,member_id:child_id,user_id:user_id,:notification_expiry_date.gte=>Date.today).first.present?
      end
      return false if notification_exist_to_push

      if batch_notification_manager.check_signup_tenure.present? && batch_notification_manager.check_signup_tenure != "NA"
        # check user signup tenure
        user_has_valid_signup_tenure = user.created_at.to_date <  (Date.today - batch_notification_manager.check_signup_tenure.to_i.days) 
      end
      
      #  assign if notification not sent before and tool is activeated for child and user is valid to receive push
      if batch_notification_manager.check_sent_before == true
        # Never sent type of notification to user if ever sent in past
        if UserNotification.notify_by_email?(user_notification_type)
            sent_in_x_day_status =  UserNotification.where(email_status:"sent",type:type,user_id:user_id ).last.present?
        else
          sent_in_x_day_status =  UserNotification.where(status:"sent",type:type,user_id:user_id ).last.present?
        end
      else
        # Gap in send Notification again for same user
        if batch_notification_manager.check_sent_in_x_day != "NA" &&  batch_notification_manager.check_sent_in_x_day.to_i > 0
          calculated_gap_date = (Date.today - batch_notification_manager.check_sent_in_x_day.to_i.day)
          # Check if notification is sent in gap day or already assigned
          if UserNotification.notify_by_email?(user_notification_type)
            sent_in_x_day_status =  UserNotification.where(email_status:"sent",type:type,user_id:user_id).or({:schedule_push_date.gte=> calculated_gap_date},{:notification_expiry_date.gte=> calculated_gap_date} ).last.present? 
          else
            sent_in_x_day_status =  UserNotification.where(status:"sent",type:type,user_id:user_id).or({:schedule_push_date.gte=> calculated_gap_date},{:notification_expiry_date.gte=> calculated_gap_date} ).last.present? 
          end
        end
      end

      # check if notification sent ever.find in splited data
      if batch_notification_manager.check_sent_before
        if sent_in_x_day_status == false
          if UserNotification.notify_by_email?(user_notification_type)
            check_sent_before_status = PushNotificationSplitedData.where(email_status:"sent",identifier:identifier,type:type,user_id:user_id).last.present? 
          else
            check_sent_before_status = PushNotificationSplitedData.where(status:"sent",identifier:identifier,type:type,user_id:user_id).last.present? 
          end
        else
          check_sent_before_status = true
        end
      end
       
      is_valid_user_to_send = true
      tool_is_activated = (destination_type.present? &&  destination_type.downcase != "user") ? Nutrient.is_tool_activated?(child_id, tool_name) : true
      user_has_valid_signup_tenure && !sent_in_x_day_status && !check_sent_before_status && tool_is_activated && is_valid_user_to_send
    end   

  def self.add_push(batch_notification_manager,current_user,push_type,batch_no,priority,push_message,destination_name,child_id,action_type="add",content_url=nil,feed_id=nil,notification_expiry_date=nil,score=1,identifier=nil,options={})
    begin
      notification_obj = nil
      batch_notification_manager = batch_notification_manager || BatchNotification::BatchNotificationManager.where(identifier:push_type).last || BatchNotification::BatchNotificationManager.new(category:destination_name,  check_sent_in_x_day:"NA",check_sent_before:false)
      user = current_user.user

      # Check User segment
      return nil if !user.has_valid_segment?(batch_notification_manager,options)

      return nil if !batch_notification_manager.valid_total_allowed_count?(current_user,options)

      # Check added child member tenure
      return nil if !current_user.has_valid_added_child_member_tenure?(batch_notification_manager,options)

      # Check activity status
      return nil if !current_user.has_valid_activity_status?(batch_notification_manager,options)

      # already_added =  UserNotification.where(member_id:child_id,user_id:current_user.user_id,type:push_type,:created_at.gte=>Date.today).present?
      # return nil if already_added
      tool_name = destination_name
      user_id = current_user.user_id

      # check user has valid tool active/deactivated and subscription free/paid as per batch manager
      valid_tool_and_subscription_status = current_user.has_valid_tool_and_subscription(batch_notification_manager,child_id,destination_name,options)
      return nil if !valid_tool_and_subscription_status

      category = batch_notification_manager.category || tool_name
      type = push_type
      # feed_id = tool_id
      unique_id = identifier.blank? ? (tool_name + "_" + feed_id.to_s) : identifier
      options[:unique_id] = unique_id

      @destination_type = nil
      @destination_type = "user" if (push_type.downcase == "pointers" || tool_name.downcase == "manage family") rescue nil
      @destination_type = "user" if (tool_name.downcase == "faq" || tool_name.downcase == "premium_subscription") rescue nil
      @destination_type = "user" if (tool_name.downcase == "home") rescue nil
      @destination_type = "user" if (tool_name.downcase == "pregnancy_week" && type== "38") rescue nil
      @destination_type = "user" if (tool_name.downcase == "Manage GP") rescue nil
      count = 0
      notify_by = batch_notification_manager.notify_by
      if options[:trigger_only].present?
        notify_by = options[:trigger_only]
      end
      # check valid to add push and email to trigger
      push_status = nil
      email_status = nil
      if UserNotification.notify_by_email_and_push?(notify_by)
        valid_push =  UserNotification.is_valid_push_to_add(batch_notification_manager,current_user,push_type,priority,destination_name,child_id,@destination_type,feed_id,{:unique_id=>unique_id, :user_notification_type=>"push"}.merge(options)) 
        valid_email =  UserNotification.is_valid_push_to_add(batch_notification_manager,current_user,push_type,priority,destination_name,child_id,@destination_type,feed_id,{:unique_id=>unique_id, :user_notification_type=>"email"}.merge(options)) 
        push_status = "pending"
        email_status = "pending"
      elsif UserNotification.notify_by_email?(notify_by)
        valid_email =  UserNotification.is_valid_push_to_add(batch_notification_manager,current_user,push_type,priority,destination_name,child_id,@destination_type,feed_id,{:unique_id=>unique_id,:user_notification_type=>"email"}.merge(options)) 
        valid_push = false
        email_status = "pending"
      elsif UserNotification.notify_by_push?(notify_by)
        valid_push =  UserNotification.is_valid_push_to_add(batch_notification_manager,current_user,push_type,priority,destination_name,child_id,@destination_type,feed_id,{:unique_id=>unique_id,:user_notification_type=>"push"}.merge(options)) 
        valid_email = false
        push_status = "pending"
      else
        valid_push =  UserNotification.is_valid_push_to_add(batch_notification_manager,current_user,push_type,priority,destination_name,child_id,@destination_type,feed_id,{:unique_id=>unique_id,:user_notification_type=>"push"}.merge(options)) 
        valid_email = false
        push_status = "pending"
      end
      if options[:test_without_validation]
        priority = 0 # set mendatory if testing push 
      end
      if (valid_push || valid_email)
        options[:push_type] = push_type 
        deep_link_data = UserNotification.prepare_data_for_deeplink(current_user, child_id,tool_name,@destination_type,feed_id,action_type,options)
        if deep_link_data[:family_id].blank? && options[:family_id].present?
          deep_link_data[:family_id] = options[:family_id]
        end
        if options[:deeplink_role].present?
          deep_link_data[:role] = options[:deeplink_role]
        end
        if options[:deeplink_destination_type].present?
          deep_link_data[:destination_type] = options[:deeplink_destination_type]
        end
        if options[:score].present?
          score = options[:score]
        end
        deep_link_data[:pregnancy_week] = options[:pregnancy_week] if options[:pregnancy_week].present?
        deep_link_data[:pregnancy_id] = options[:pregnancy_id] if options[:pregnnacy_id].present?
        push_message =  batch_notification_manager.title_prefix.to_s + push_message
        notification_obj = UserNotification.create({schedule_push_date:Date.today, notification_expiry_date:notification_expiry_date,created_at:Time.now,
          priority:priority,
          user_id: user_id,
          member_id: child_id, 
          message: push_message, 
          batch_no: batch_no,
          status: push_status,
          email_status: email_status,
          category: category,
          identifier: unique_id,
          content_url: content_url,
          notify_by: notify_by,
          country_code: (user.country_code rescue nil),
          type:type,
          score:score}.merge(deep_link_data))
      end
      if valid_email
        UserNotification::EmailDataToSend.create_data_for_user_notification(notification_obj,current_user,child_id,options)
      end
    rescue Exception => e 
      Rails.logger.info e.message
      NotificationTrackingLogger.info "Error inside add_push for user id #{current_user.user_id}.............. #{e.message}"
    end
    notification_obj   
  end


  def self.assign_push_to_all_elder_member(child,push_message,batch_notification,feed_id=nil,user_ids=nil,content_url=nil,options={})
    if user_ids.blank?
      family_elder_member_ids =  child.family_elder_members.map(&:to_s) 
      batch_notification_check_signup_tenure_value = batch_notification.check_signup_tenure.to_i
      if batch_notification.check_signup_tenure.present? && batch_notification.check_signup_tenure != "NA"
        users = User.where(:created_at.lt =>(Time.now - batch_notification_check_signup_tenure_value.days)).in(member_id:family_elder_member_ids,status:["confirmed","approved"])
      else
        users = User.in(member_id:family_elder_member_ids,status:["confirmed","approved"])
      end
      if UserNotification.notify_by_push?(batch_notification.notify_by) && !UserNotification.notify_by_email?(batch_notification.notify_by)
        users = users.where(:app_login_status=>true)
      end
      user_ids = users.pluck(:id).map(&:to_s)
    end
    
    batch_notification_manager = batch_notification
    return false if !batch_notification_manager.valid_total_allowed_count?(current_user=nil,options)
    
    push_type = batch_notification.identifier #push identifier
    priority = batch_notification.priority.to_i
    child_id = child.id.to_s rescue nil
    identifier  = batch_notification.identifier
    destination_name = batch_notification.destination
    action_type = batch_notification.action.downcase rescue ""
    batch_no = batch_notification.batch_no.to_i
    family_id = options[:family_id]
    if child_id.present?
      family_id = FamilyMember.where(member_id:child_id).first.family_id
    end
    notification_expiry_date = Date.today + batch_notification.expire_in_no_of_day.to_i.days
    status = false
    Member.in(user_id:user_ids).no_timeout.each do |member|
      begin
        # To support 34 identifier for ios only check user device platform
        next if options[:check_platform] && !options[:valid_device_list_for_notification].include?(member.device_platform.to_s.downcase)
        notification_obj = UserNotification.add_push(batch_notification,member,push_type,batch_no,priority,push_message,destination_name,child_id,action_type,content_url,feed_id,notification_expiry_date,score=priority,identifier,options)
        if notification_obj.present?
          status = true 
          # record_object =  BatchNotification::PushNotification.create(:action_for_tool=>destination_name,
          #   :message=> push_message,
          #   :action_type => action_type,
          #   :unique_id=> feed_id, 
          #   :object_id=>member.id,
          #   :family_id => family_id,
          #   :child_id => child_id,
          #   :schedule_push_date=> Date.today
          # )
        end
      rescue Exception=>e 
        Rails.logger.info e.message
      end
      status
    end
  end
  
   
end



