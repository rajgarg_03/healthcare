class Timeline
  include Mongoid::Document
  require 'active_support_encoding'
  WillPaginate.per_page = 5
   include Mongoid::Timestamps
  field :name,            :type => String
  field :desc,            :type => String, :default => ""
  field :member_id,       :type => String
  field :timeline_at,     :type => Time,   :default => DateTime.now
  field :data_type,       :type => String
  field :data_id,         :type => String
  field :added_by,        :type => String
  field :entry_from,      :type => String
  field :state,           :type=>Boolean, :default => true

  field :data_source, type:String,:default=>"nurturey" # eg smrthi,emis,nurturey

  validates_length_of :name, :maximum=>50
  # validates_presence_of :name
  validates_presence_of :name
  belongs_to :timelined, :polymorphic=>true
  has_many :pictures, :as=> :imageable, :dependent => :destroy
  
  belongs_to :member
  
  index ({id:1})
  index ({member_id:1})
  index ({timelined_id:1})
  index({system_entry:1})
  index({entry_from:1})

  validates_associated :pictures
   PostTitle1 = {"prenatal_test"=> "Prenatal tests", "health"=> "Measurements", "timeline"=>"Timeline", "preg"=>"Timeline", "document"=> "Documents","milestone"=>"Milestones", "jab"=>"Immunisations"}
   PostTitle = {
      "MedicalEvent"=> "Prenatal tests",
      "Health"=> "Measurements",
      "timeline"=>"Timeline",
      "Pregnancy"=>"Timeline", 
      "Document"=> "Documents",
      "Milestone"=>"Milestones", 
      "Jab"=>"Immunisations"}
  
  def timeleine_added_by
    begin
      member = Member.find(self.added_by)
      member.first_name + " " + member.last_name
    rescue
      ""
    end
  end

  def timeline_attributes
    timeline =  self
    data = timeline.attributes 
    data["desc"] = ActiveSupportEncoding.escape(timeline.desc)
    data["name"] = ActiveSupportEncoding.escape(timeline.name)
    data
  end

  def self.update_timeline_state_for_tool(member, tool_name, state_val = true)
    tool_name_mapping = {"Prenatal tests"=>"MedicalEvent", "Measurements"=>"Health",  "Documents"=>"Document", "Milestones"=>"Milestone","Milestone"=>"Milestone", "Immunisations"=>"Jab"}
    Timeline.where(member_id:member.id, timelined_type:tool_name_mapping[tool_name]).update_all(state:state_val)
  end

  def display_default_image?
    return false unless (self.system_entry == "true" rescue true)
    SystemTimeline.where(id:self.timelined_id).first || false
  end

  def self.save_system_timeline_to_member(member_id,added_by,system_timeline_ids = [])
    member = Member.find(member_id)
    system_timeline_ids = system_timeline_ids.map(&:to_s)
    added_timelines = Timeline.where(member_id:member.id.to_s,system_entry:"true").pluck(:timelined_id).map(&:to_s)
   
    system_timelines = system_timeline_ids.present? ? SystemTimeline.for_category('child').where(:id.in => (system_timeline_ids - added_timelines) ) : SystemTimeline.not_in(id:added_timelines).for_category('child')
    system_timelines.each do |system_timeline|
      begin
        timelined_at = member.birth_date + eval(system_timeline.duration)
      rescue
      end
      timeline = Timeline.create({timelined_id:system_timeline.id, timeline_at:timelined_at ,name:system_timeline.name}.merge({system_entry: "true", member_id: member_id,added_by: added_by}))
    end
    note = Timeline.where(system_entry:"true",member_id: member_id).map(&:id).join(",")
    member.member_settings.where(name:"timeline_setting").first_or_create(name:"timeline_setting", status:"pending",note: note)
    member.member_settings.where(name:"timeline_display").first_or_create(name:"timeline_display", status:"true", note: 1.days.ago)
  end

  def self.save_future_system_timeline_to_member(member_id,added_by,system_timeline_ids = [])
    member = Member.find(member_id)
    system_timeline_ids = system_timeline_ids.map(&:to_s)
    added_timelines = Timeline.where(member_id:member.id.to_s,system_entry:"true").pluck(:timelined_id).map(&:to_s)
    system_timelines = system_timeline_ids.present? ? SystemTimeline.for_category('child').where(:id.in => (system_timeline_ids - added_timelines) ) : SystemTimeline.not_in(id:added_timelines).for_category('child')
    system_timelines.each do |system_timeline|
      begin
        timelined_at = member.birth_date + eval(system_timeline.duration)
      rescue
      end
      if timelined_at > Date.today 
        timeline = Timeline.create({timelined_id:system_timeline.id, timeline_at:timelined_at ,name:system_timeline.name}.merge({system_entry: "true", member_id: member_id,added_by: added_by}))
      end
    end
  end

  def get_timeline_at(date=nil)
    if date.present?
      date = date.is_a?(String) ? date : date.strftime("%d-%m-%Y")
      get_time = new_record? ? Time.zone.now : (timeline_at || Time.zone.now)
      (date + ' ' + get_time.strftime("%H:%M:%S %z"))
    else
      Time.now
    end
  end

end