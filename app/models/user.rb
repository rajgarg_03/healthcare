class User
  include Mongoid::Document
  include Geocoder::Model::Mongoid
  #extend Geocoder::ModelMethods
  # geocoded_by :address               # can also be an IP address
  # after_validation :geocode    

  include Mongoid::Paranoia
  include Mongoid::Timestamps
  WillPaginate.per_page = 5
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, 
  :recoverable, :rememberable, :trackable, :validatable, :confirmable, 
  :omniauthable, :omniauth_providers => [:facebook,:google_oauth2]

  ## Database authenticatable
  field :first_name, :type => String
  field :last_name, :type => String
  field :country_code, :type => String
  field :status, :type => Integer

  field :coordinates, :type => Array
  field :address

  field :email,							     :type => String, :default => ""
  field :encrypted_password,     :type => String, :default => ""
  field :uid, :type => String    # used in FaceBook account. FaceBook has uniq Uid
  field :provider, :type => String # eg facebookb/nhs/nil .facebook if Facebook login else nil
  field :app_login_status, :type => Boolean #check user login status in app. if login then only send notification #true/false
  field :failed_login_attempt_count, :type => Integer, :default=>0  #keeps record for successive failed login attempt
  
  validates_presence_of :email
  validates_uniqueness_of :email
  validates_presence_of :encrypted_password
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i }
  
  ## Recoverable
  field :reset_password_token,	    :type => String
  field :reset_password_sent_at,    :type => Time
  ## Rememberable
  field :remember_created_at, :type => Time
  
  ## Trackable
  field :sign_in_count,			              :type => Integer, :default => 0 # count of user signin 
  field :current_sign_in_at,              :type => Time
  field :last_sign_in_at,		              :type => Time
  field :current_sign_in_ip,              :type => String
  field :last_sign_in_ip,		              :type => String
  # Confirmable
  field :confirmation_token,	            :type => String #used in App for security 
  field :confirmed_at,				            :type => Time # time when user confirm his email
  field :confirmation_sent_at,            :type => Time
  field :unconfirmed_email,		            :type => String # Only if using reconfirmable
  field :status,                          :type=> String, :default=>"approved" #Email verification status . eg. confirmed/unconfirmed/approved/unapproved
  field :member_id,                       :type => String # Only if using reconfirmable
  field :welcome_email_sent,              :type => Boolean
  field :unregister_from_mail,            :type=> Boolean  , :default => false #keep user subscription status for email from nurturey marketing/other mails 
  field :user_type,                       :type=>String, :default=>"General" #eg Internal and General. distinguish end user and developer account
  field :account_verification_token,      :type => String # secure hex string used for email verification email link 
  field :basic_setup_and_handhold_counter,:type => Integer ,:default=>0 #eg 1,2,3,4 . used to avoid run handhold and basic setup every time. will run on a particular no.
  field :signup_source,                   :type=>String #"alexa
  field :signup_source_reference ,        :type=>String # Mathmatic skill,nurtuey_skill
  field :stage,                           :type=>String, :default=>"prospect"  # eg. prospect/customer
  field :activity_status,                 :type=>String, :default=>"Active" # eg. Inactive/Active/Defunct
  field :activity_status_v2,                 :type=>String, :default=>"Active" # eg. Inactive/Active/Defunct
  field :segment,                         :type=>String, :default=>"trial" # eg. trial/starter/adoptor/ambassador
  field :trial_days,                      :type=>Integer,:default=>7 
  field :pricing_exemption,               :type=>String, :default=>"none" #eg basic/none
  field :role_for_admin_panel,            :type=>String, :default=>"none" # admin/superadmin/none 
  field :email_domain_status,             :type=>String # "valid/invalid" 
  field :subscription_popup_launch_count, :type=>Integer # eg. 1,2 3 
  # field :patient_access_linked_status,    :type=>String # eg. "true/false"
  # field :patient_access_type,             :type=>String # eg. "emis/smiriti" 
  field :api_access_token,                :type => String #used in App for security refresh after signin
  field :token_generated_date,            :type => Time # date on which token(auth_token) generated 
  field :alexa_access_token,              :type => String # Used for Alexa/Google device login support
  field :nhs_login_for_im1,               :type => Boolean, :default => false  # keep record for nhs login purpose(login for clinic link only) session based
  field :nhs_login_count,                 :type => Integer, :default => 0  # keep record for nhs login count
  
  field :allowed_signin_provider, :type=>Array # to support multiple provider sign in eg, Nhs/Apple/Nurturey/Orion/Facebook
  has_one :member
  has_one :secondary_email
  has_one :google_cal
   
  has_many :families, foreign_key: :member_id
  has_many :notifications, :class_name=>"Invitation", foreign_key: :member_id
  has_many :families, foreign_key: :member_id
  
  has_one :cover_image, :as=>:imageable, :class_name=>"Picture"#, foreign_key: :member_id
  has_many :user_login_details
  has_many :user_notifications

  validate :password_complexity
  

  accepts_nested_attributes_for :cover_image,  :autosave => true
  
  index({id:1}) 
  index({confirmation_token:1})
  index({api_access_token:1})
  index({country_code:1})
  index({member_id:1})
  index({status:1})
  index({app_login_status:1})
  index({signup_source:1})
  index({user_type:1})
  index({email:1})
  
  cattr_accessor :password_validation_type
  SiginProviderList = [ "Nurturey","Apple","Nhs","Facebook","Orion"]
  
  # Show screen on first time login via nhs
  def nhs_welcome_screen_status(options={})
    user = self
    user.nhs_login_count == 1
  end

  def confirmation_required?
    !confirmed?
  end

  def is_internal?
    begin
      user = self
      user.user_type.to_s.downcase == "internal"
    rescue Exception=>e 
      false
    end
  end
  
  def account_type_allowed?(provider,options={})
    user = self
    allowed_signin_provider_list = user.allowed_signin_provider || []
    if allowed_signin_provider_list.blank?
      if user.provider.present?
        allowed_signin_provider_list << user.provider.titleize
      else
        allowed_signin_provider_list << "Nurturey"
        # allowed_signin_provider_list << "Nhs" # allow nhs login with Email/pwd signup
      end
      user.allowed_signin_provider = allowed_signin_provider_list
      user.save
    end
    status = false
    if allowed_signin_provider_list.include?(provider)
      status = true
    elsif provider == "Nhs" && allowed_signin_provider_list.include?("Nurturey")
      status = true
    end
    status
  end

  def can_access_clinic_services_with_allowed_account_type?(options={})
    user = self
    allowed_signin_provider_list = user.allowed_signin_provider
    allowed_signin_provider_list.include?("Nurturey") || allowed_signin_provider_list.include?("Nhs")
  end

  def user_account_type
    begin   
      user = self
      account_type = "Nurturey"
      case user.provider.to_s.downcase
      when "nhs"
        account_type =  "NhsLogin"
      when "facebook"
        account_type = "Facebook"
      when ""
        account_type = "Nurturey"
      when "nurturey"
        account_type = "Nurturey"
      when "apple"
        account_type = "Apple"
      else
        account_type =  "Nurturey"
      end
    rescue Exception => e 
      account_type = "Nurturey"
    end
    account_type
  end
  
  def is_beta_user?
    user = self
    ::System::Settings.is_beta_user?(user.email)
  end


  def can_access_tpp_service?(options={})
    user = self
    current_user = user.member rescue nil
    if options[:im1_account_id].present?
      # if non tpp account if account id length != 9 
      if options[:im1_account_id].to_s.length != ::Clinic::ClinicDetail::TppAccountLength
        Rails.logger.info "Invalid im account id for TPP member_id #{user.member_id}"
        return false 
      end
    end
    return true if ::System::Settings.tppEnabled?(current_user,options)
    user.is_internal? || user.is_beta_user?
  end

  def error_message_for_signup(error_messages,validation_type="type1")
    error_messages.reject{|k,v| v.blank?}
    message = ""
    if error_messages[:email].present?
      message =  "Please fill Email ID with correct format"
    elsif error_messages[:password].present?
      message =  error_messages[:password].last
    end
    message
  end
  
  def password_complexity
    if password_validation_type.present?
      return errors.add :password, "Please provide a password with 8 characters or more." if password.blank? || password.length < 8
      return errors.add :password, "Confirm password does not matched" if password_confirmation.present? && password != password_confirmation rescue false
    end
    if password_validation_type == "type2"
      account_name = self.email.split("@").first[0..2]
      checker = StrongPassword::StrengthChecker.new(min_word_length:8,use_dictionary:true)
      score_strength = checker.calculate_entropy(password)
      if !((password =~ /#{account_name}/i).nil? rescue true)
        return errors.add :password, "Password can not be part of username"
      elsif score_strength < 10
        return errors.add :password, Message::DEVISE[:m25]

      end
      # return if password.blank? || password =~ /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,70}$/
      return if password.blank? || password =~ /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,70}$/
      errors.add :password, Message::DEVISE[:m19]
    end
  end


  def used_password?(current_pwd)
    user = self
    pwd_status = false
    password_list = PasswordHistory.where(user_id:user.id).order_by("id desc").limit(4).entries
    password_list.each do |pwd|
      decrypt_pwd = ShaAlgo.decryption(pwd.encrypt_password)
      
      if current_pwd ==  decrypt_pwd
        pwd_status = true 
        break
      end
    end
    pwd_status
  end
  

  def log_signin_event(event_name,params, status="success", options={})
    user = self
    options[:platform] = params[:platform]
    if options[:platform].blank?
      options[:platform] = Device.where(member_id:user.member_id).last.platform rescue nil
    end
    if event_name == "user logged in successfully"
      options[:failed_attempt] = 0
      user.failed_login_attempt_count = 0
      user.save
    elsif event_name == "user attempt to login failed"
      options[:failed_attempt] = user.failed_login_attempt_count.to_i + 1
      user.failed_login_attempt_count = options[:failed_attempt]
      user.save
    end
    case event_name
      when 'user logged in successfully'
        custom_id = 240
      when 'user attempt to login failed'
        custom_id = 240
        status = "failed"
      when 'user password changed successfully'
        custom_id = 241
      when 'user password not changed'
        custom_id = 241
        status = "failed"
      when 'user signout successfully'
        custom_id = 242
        
      when 'user attempt to signout failed'
        custom_id = 242
        status = "failed"
      when 'user signup successfully'
        custom_id = 243
      when 'user attempt to signup failed'
        status = "failed"
        custom_id = 243
      else
       custom_id = Report::Event.where(name:event_name).last.custom_id rescue nil
    end
    if custom_id == 243 && (params[:user][:provider].to_s.downcase == "nhs" rescue false)
      #Event : 'Signup via NHS'
      ::Report::ApiLog.log_event(user.id,nil,268,options.merge(status:status)) rescue nil
    end
    options[:status] = status
    ::Report::ApiLog.log_event(user.id,nil,custom_id,options) rescue nil
  end

  def valid_to_run_handhold_and_basic_setup?(platform=nil)
    return true if User.is_alexa_platform?(platform)
    user = self.reload
    status = false
    basic_setup_and_handhold_count = user.basic_setup_and_handhold_counter.to_i rescue 0
    # if user.provider == "nhs" && user.member.clinic_link_state < 3
    #   basic_setup_and_handhold_count = 0
    # end
   
    case  
      when basic_setup_and_handhold_count == 0
        user["basic_setup_and_handhold_counter"] = basic_setup_and_handhold_count + 1
        status = true
      when basic_setup_and_handhold_count >= APP_CONFIG[:handhold_basic_setup_run_after_count]
        user["basic_setup_and_handhold_counter"] = 0
      else
        user["basic_setup_and_handhold_counter"] = basic_setup_and_handhold_count + 1
    end
    user.save
    status
  end
  
  def update_subscription_popup_launch_count(count=nil,options={})
    user = self
    if count.present?
      launch_count = count
    else
      launch_count = user.subscription_popup_launch_count
      if launch_count.blank? || launch_count < GlobalSettings::SubscriptionPopupLunchFrequency
        launch_count = launch_count.to_i + 1
      else
        launch_count = 0
      end
    end
    if launch_count == 0 
      report_event = Report::Event.where(name:"show subscription popup").last
      ::Report::ApiLog.log_event(user.id,family_id=nil,report_event.custom_id,options) rescue nil
    end
    user.update_attribute(:subscription_popup_launch_count,launch_count)
  end

  
  def self.find_by_verification_token(token)
    where(account_verification_token:token).last
  end

  # Find by api_token /auth token
  def self.find_by_token(api_token,auth_token=nil,api_version=5)
    User.find_user_with_token(api_token,auth_token,api_version).first
  end
  def user_refresh_token
    self.confirmation_token
  end

  def refresh_api_token
    user = self
    existing_access_token = user.api_access_token
    existing_auth_token = user.confirmation_token
    user.api_access_token = generate_access_token

    if ((user.token_generated_date + GlobalSettings[:AuthTokenExpireIn]) < Time.now rescue false)
      Rails.logger.info "User Auth token is changing"
      user.confirmation_token = generate_auth_token
      user.token_generated_date = Time.now
    end
    user.save(validate:false)
    user.reload
    Rails.logger.info "Api access token changed status #{existing_access_token ==  user.api_access_token}"
    Rails.logger.info "Auth token changed status #{existing_auth_token ==  user.confirmation_token}"
    user.api_access_token
  end

  def generate_auth_token
    Rails.logger.info "Generating auth token"
    token = nil
    loop do
      token = Devise.friendly_token
      break token unless User.where(confirmation_token: token).first
    end
    Rails.logger.info "Generated auth token"
   token
  end

  def generate_access_token
    Rails.logger.info "Generating access token"
    token = nil
    loop do
      token = Devise.friendly_token
      break token unless User.where(api_access_token: token).first
    end
    Rails.logger.info "Generated access token"
     token
  end
  
  # devise gem status - confirmed/unconfirmed
  # unapproved/approve - if admin unapproved/approve any user. to keep record which user unapproved/approved by admin 
  STATUS = {
    :confirmed =>"confirmed",
    :unconfirmed =>"unconfirmed",
    :approved => "approved",
    :unapproved => "unapproved"
  }
  # return true/false . User email verification status is confirmed/approved or not
  def is_approved?
    self.status == STATUS[:approved] || self.status == STATUS[:confirmed]
  end

#  For admin Panel
  def get_account_status
    user = self 
    if user.is_approved?
      "Active"
    elsif user.status == User::STATUS[:unapproved]
      "Blocked"
    else
      user.status
    end
  end
  ######## For social services ########
  
  def can_access_admin_panel?
    user = self
    if ["admin","superadmin"].include?(user.role_for_admin_panel)
      true
    else
      false
    end
  end
  def self.prepare_fb_data(params)
    info_data = params[:fb_data][:info]
    info_data[:country_code] = info_data[:country_code] || params[:country_code]
    info_data[:time_zone] = info_data[:time_zone] || params[:time_zone]
    params[:fb_data]
  end

  def self.from_omniauth(auth,options={})
    user = User.where(email: auth.info.email).first
    if user.blank?
      user = User.new(provider: auth.provider, uid: auth.uid)
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,8] + SecureRandom.urlsafe_base64(nil, true)
      user.status = User::STATUS[:confirmed]
      user.first_name = (auth.info.first_name || auth.info.name) rescue nil
      user.last_name = auth.info.last_name
      country_code = auth.info.country_code  
      time_zone = (auth.info.time_zone || auth.time_zone ) rescue nil
      user.country_code = country_code.present? ? country_code.downcase : 'uk'
      # don't send verification mail
      user.skip_confirmation!
      user.confirmation_token || user.send(:generate_confirmation_token)
      user.save
      if user.save
        member = ParentMember.where(:email => user.email).first || ParentMember.create(time_zone: time_zone,user_id: user.id, first_name: auth.info.first_name, last_name: auth.info.last_name, image: auth.info.image, email: user.email)
        if member.user_id.blank? # For invitation
          member.update_attributes(time_zone: time_zone, user_id: user.id, first_name: auth.info.first_name, last_name: auth.info.last_name, image: auth.info.image)
        end
        Score.new(activity_type: "User", activity_id: user.id, activity: "Sign up", point: Score::Points["Signup"], member_id: member.id).save
        user.update_attributes(member_id:member.id )
        user.save(validate:false)
        user.save_detail(options)
        user.reload
      end
     
    end
    # Update allowed provider for signin
    provider = auth.provider.to_s.downcase.titleize
    User::SsoLoginDetail.save_login_detail(provider,auth.uid,user.id,options)
    # if user
    #   user.skip_confirmation!
    #   user.status = User::STATUS[:confirmed]
    #   user.provider = auth.provider
    #   user.save
    # end
    user
  end

  def update_allowed_provider_for_signin(provider,options={})
    begin
      user = self
      provider_list = user.allowed_signin_provider.blank? ?  [] : user.allowed_signin_provider
      user.allowed_signin_provider = (provider_list + [provider.to_s.downcase.titleize]).uniq
      user.save(validate:false)
    rescue Exception =>e 
      Rails.logger.info "Error inside update_allowed_provider_for_signin ............ #{e.message}"
    end
  end
  # Add extra detail if needed
  def save_detail(data)
    if data.present?
      user = self
      # save user signup source and reference
      user.signup_source = data[:signup_source]
      user.signup_source_reference = data[:signup_source_reference]
      user.save(validate:false)
      if (["alexa","googleassistance"].include?(user.signup_source.downcase) rescue false)
        BatchNotification::Tools::MentalMaths.assign_batch_notification_for_identifier_31(31,{:user_ids=>[user.id]})
      end
      # log event for signup 
      report_event = Report::Event.where(name:"user signup").last
      options = {:ip_address=>user.current_sign_in_ip}
      ::Report::ApiLog.log_event(user.id,family_id=nil,report_event.custom_id,options) rescue nil
    end
  end

  def self.find_user_with_token(api_token,auth_token=nil,api_version=5)
    return User.new if api_token.blank?
    if api_version >= 6
      user = User.where(api_access_token:api_token)
      if  auth_token.present? && (api_token.blank? || user.blank? )
        user = User.where(confirmation_token:auth_token)
      end 
    else
      if api_token.present? && auth_token.present?
        user = User.where(api_access_token:api_token,confirmation_token:auth_token)
      else
        # check if user token is api access token or auth token
        user = User.or({api_access_token:api_token},{confirmation_token:api_token})
      end
      if user.blank? && auth_token.present?
        user = User.where(confirmation_token:auth_token)
      end
    end
    user 
  end
  


  # check user is valid or not to send push notification
  def is_valid_user_to_send_push?
    user = self
    status = false
    if user["app_login_status"] == true && is_approved? 
      notification_setting = MemberSetting.where(member_id:user.member_id, name:"subscribe_notification").last.status rescue true
      if notification_setting.to_s == "true"
        status = Device.where(member_id:user.member_id.to_s).pluck(:token).present? rescue false
      end
    end
    status
  end

  def is_valid_user_to_send_email?
    subscribed_to_mailer?  && is_approved? 
  end
 
  # return token for api 
  def api_token(is_alexa_signin=false)
    user = self
    if is_alexa_signin
      user.alexa_access_token || user.confirmation_token
    else
      user.api_access_token || user.confirmation_token
    end
  end
  def deeplink_api_token
    user = self
    user.confirmation_token
  end

  def is_token_valid?(token,is_alexa_signin=false,options={})
    user = self
    status = false
    if is_alexa_signin
      status = user.alexa_access_token == token || user.confirmation_token == token
    else
      status = user.api_access_token == token || user.confirmation_token == token
    end
    status
  end

   def auth_token
    self.confirmation_token
  end
  
  def get_user_country_code
    user = self
    return 'GB' if user.country_code.blank? || ["uk","united kingdom"].include?(user.country_code.downcase)
    return "IN" if ["in","india"].include?(user.country_code.downcase)
    user.country_code.upcase
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end
   
  # List all family member of User
  def family_members(type)
    members=[]
    user = self
    family_ids = Family.where(member_id:user.member_id).pluck(:id).map(&:to_s)
    family_member_ids = FamilyMember.in(:family_id=>family_ids).pluck(:member_id).uniq
    members = Member.in(:id=> family_member_ids).entries
    return members
  end
  
  # check user authentication status
  # Return String 
  def self.user_authentication_status(user_for_check,current_user)
    begin
      data = "unauthenticated"
      data = "authenticated" if (current_user.present? && user_for_check == current_user.user)
      data = "blocked" if user_for_check.status == User::STATUS[:unapproved]
      data = "email_unconfirmed" if (user_for_check.status == User::STATUS[:unconfirmed] || !user_for_check.is_approved?)
    rescue
      data = "unauthenticated"
    end
    data
  end

  def email_verification_status
    unconfirmed_email  = self.status == User::STATUS[:unconfirmed] || !self.is_approved?
    !unconfirmed_email  
  end
  # Generate hex string used in email verification link
  def generate_verification_token
    user = self
    verification_token = Devise.friendly_token[0,20]
    Rails.logger.info "Code before updating ................#{user.account_verification_token}"
    user.account_verification_token = verification_token
    verification_code_status = user.save(validate:false)
    user.reload
    Rails.logger.info ".....................Generated verification code : #{verification_token} ................"
    Rails.logger.info "Update verification code status: #{verification_code_status} "
    Rails.logger.info "Code after updated ................#{user.account_verification_token}"
    user.account_verification_token
  end

 # check if user can access dashboard at signin without confirm his email for n hour/minutes (eg 2 day)
  def self.can_access_dashboard_after_signup?(user,api_version=1,check_unapprove_status=true)
    if User.is_alexa_platform?(user.signup_source)
      allowed_time = GlobalSettings::EmailVerificationAfterSignupForAlexa[:allowed_time]
    else
      allowed_time = GlobalSettings::EmailVerificationAfterSignup[:allowed_time]
    end
    if user.status == User::STATUS[:unconfirmed]
      if user.present? &&  (user.created_at + allowed_time) >= Time.now.in_time_zone && api_version > 3
        GlobalSettings::EmailVerificationAfterSignup[:enable]
      else
        false
      end
    else
      # user can login if user is not in unapproved/unconfirmed status
      if check_unapprove_status
        user.status != User::STATUS[:unapproved]
      else
        return true
      end
    end
  end
  
  # Delete user account and dependent record from DB
  def delete_account(deleted_by)
    begin
      user = self
      current_user = self.member
      # families created by user
      families = current_user.families
      families.each  do |family|
        begin
          family.delete_family(current_user)
        rescue Exception=>e 
          UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,{:family_id=>family.id, member_id:current_user.id},"Error inside delete_account")
        end
      end
      organization_uid = current_user.organization_uid
      options = {:current_user=>current_user}
      member_id = current_user.id
      ::Clinic::Organization.delink_clinic(member_id,organization_uid,options)
      Member.where(:id.in=>[current_user.id]).delete_all
      Pregnancy.where(member_id:member_id).update_all(member_id:nil)
      System::DeletedUserDetail.save_delete_user_detail(user,(deleted_by.user rescue nil))
      User.in(member_id:[current_user.id]).delete_all
    rescue Exception=>e 
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,{member_id:current_user.id},"Error inside delete_account")
    end
    # All son/Daughter members in user created families
    # member_ids = FamilyMember.where({'family_id' => { "$in" => families.map(&:id)} }).in(:role =>["Son","Daughter"]).map(&:member_id).uniq
    # member_ids = member_ids.map(&:to_s)
    # member_ids << current_user.id.to_s
    # #Delete all dependent record of user from DB
    # Timeline.in(member_id:member_ids).delete_all
    # Milestone.in(member_id:member_ids).delete_all
    # Picture.in(member_id:member_ids).delete_all
    # Document.in(member_id:member_ids).delete_all
    # Health.in(member_id:member_ids).delete_all
    # ArticleRef.in(member_id:member_ids).delete_all
    # SystemMilestone.in(member_id:member_ids).delete_all
    # SystemTimeline.in(member_id:member_ids).delete_all
    # Vaccination.in(member_id:member_ids).delete_all
    # Pregnancy.in(member_id:member_ids).delete_all
    # MemberNutrient.in(member_id:member_ids).delete_all
    # Score.in(member_id:member_ids).delete_all
    # MedicalEvent.in(member_id:member_ids).delete_all
    # Invitation.in(member_id:member_ids).delete_all
    # Invitation.in(sender_id:member_ids).delete_all
    # Invitation.in(family_id:families.map{|a| a.id.to_s}).delete_all
    # Pregnancy::HealthCard::BloodPressure.in(member_id:member_ids).delete_all
    # DismissNutrient.in(child_id:member_ids).delete_all
    # Pregnancy::HealthCard::Measurement.in(member_id:member_ids).delete_all
    # Pregnancy::HealthCard::Hemoglobin.in(member_id:member_ids).delete_all
    # Device.in(member_id:member_ids).delete_all
    # UserNotification.in(member_id:member_ids).delete_all
    # UserNotification.where(user_id:self.id.to_s).delete_all
    # Notification.in(member_id:member_ids).delete_all
    # Notification.in(sender_id:member_ids).delete_all
    # Event.in(member_id:member_ids).delete_all
    # FamilyMember.in(member_id:member_ids).delete_all
    # Family.where(member_id:current_user.id.to_s).delete_all
    # Family.in(id:families.map(&:id)).delete_all
    # UserCommunication::UserActions.in(child_id:member_ids).delete_all
    # UserCommunication::BirthdayWish.in(child_id:member_ids).delete_all
    # Subscription.where(member_id:current_user.id.to_s).delete_all #.update_all({member_id:nil,})
  end

  def active_families
    families.where(:status.ne => 1)
  end

  # used in batch job
  def send_welcome_mail
    UserMailer.welcome_email(user).deliver
  end
 
  # Method check user subscription status for emails sends from nurturey . Return true/false 
  def subscribed_to_mailer?
    user = self
    return false if user.unregister_from_mail == true
    setting = MemberSetting.where(member_id:user.member.id,name: "mail_subscribe").first
    if setting && !setting.note.split(",").include?("0")
      false
    else
      true
    end
  end
  
  # Devise gem hookup code 
  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if email = conditions.delete(:email)
      User.includes(:secondary_email).any_of({ :email =>  /^#{Regexp.escape(email)}$/i }, { :email =>  /^#{Regexp.escape(email)}$/i }).first
    else
      super(warden_conditions)
    end
  end  
  
  # update user email verification status to confirmed
  def verify
    self.update_attribute(:status,'confirmed')
    self.update_attribute(:account_verification_token,nil)
    self.update_attribute(:failed_login_attempt_count,0)
  end

  def families_count
    current_families = member.families.includes(:family_members).entries
    asso_families =  Family.where({'_id' => { "$in" => FamilyMember.where(member_id:member.id).map(&:family_id).uniq - current_families.map(&:id)}}).includes(:family_members).entries
    families = current_families + asso_families        
    families.count
  end


  def has_active_family?(options={})
    user = self
    family_ids = FamilyMember.where(:member_id=>user.member_id).pluck(:family_id)
    active_family = Family.where(:id.in=>family_ids,:activity_status_v2=>"Active").last
    active_family.present?
  end
  
  #API-42
  # Find user subscribed plan and parent tool list
  def self.user_subscription(current_user,api_version=1)
    subscription = Subscription.user_premium_or_free_subscription(current_user.id.to_s) 
    subscription_manager = subscription.subscription_manager
    pa_tool_list = Nutrient.PA_tool_name(current_user,api_version)
    subscription_listing_order = subscription_manager.subscription_listing_order rescue Subscription::Level["free"] # Not subscribed to any plan 
    all_tools =   pa_tool_list
    subscription_type = subscription_manager.title rescue SubscriptionManager.free_subscription.title
    [all_tools,subscription_type,subscription_listing_order ]
  end
  
  # list all active tools of user childrens
  def self.member_active_tools(member_ids,current_user,api_version=1)
    tool_ids = MemberNutrient.in(member_id:member_ids).where(status: "active").pluck("nutrient_id").uniq
    member_tools = Nutrient.in(:id=>tool_ids).where(:identifier.ne=>"Manage Tools").valid_to_launch(current_user,api_version).order_by("position ASC").entries
    member_tools
  end
  
  def mark_user_activity_status(activity_status, options={})
    begin
      user = self
      current_user = user.member
      if user.activity_status != activity_status
        # update user acitivity status
        user.update_attribute(:activity_status,activity_status)
        current_user.update_attribute(:activity_status,activity_status) rescue nil
      end

      # update family acitivity status
        user_family_ids = current_user.user_family_ids
        options[:user_id] = user.id
        Family.where(:id.in=>user_family_ids,:activity_status.ne=>activity_status).each do |family|
          family.mark_family_activity_status(activity_status,options)
        end
    rescue Exception=>e 
      Rails.logger.info e.message
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside mark_user_activity_status")
    end
  end

  def mark_user_activity_status_v2(activity_status, options={})
    begin
      user = self
      current_user = user.member
      return nil if user.activity_status_v2 == activity_status
      # update user acitivity status
      user.update_attribute(:activity_status_v2,activity_status)
      current_user.update_attribute(:activity_status_v2,activity_status) rescue nil
      report_event = options[:user_report_event] || Report::Event.where(name:"mark user #{activity_status.downcase}").last
      ::Report::ApiLog.log_event(user.id,nil,report_event.custom_id,options) rescue nil
     
    rescue Exception=>e 
      Rails.logger.info e.message
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside mark_user_activity_status_v2")
    end
  end

  def find_user_stage(stage_value=nil,options={})
    user = self
    return "customer" if options[:case] == "member_added"
    if stage_value.blank?
      stage_value = "prospect"
      if user.is_approved?
        current_user = user.member
        family_ids = current_user.user_family_ids
        child_member_ids =  FamilyMember.where(:family_id.in => family_ids,:role.in => ["Son","Daughter"]).pluck("member_id")
        if family_ids.present? && child_member_ids.present?
          stage_value = "customer"
        end
      end
    end
    stage_value
  end

  def update_user_stage(stage_value=nil,options={})
    begin
      user = self
      user_stage = user.find_user_stage(stage_value,options)
      if user.stage != user_stage
        user.update_attribute(:stage,user_stage)
        report_event = Report::Event.where(name:"mark user stage #{user_stage}").last
        ::Report::ApiLog.log_event(user.id,family_id=nil,report_event.custom_id,options) rescue nil
      end
    rescue Exception=>e 
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,{user_id:user.id},"Error inside update_user_stage") rescue nil
    end 
  end
  
  def subscription_popup_status(family_list=nil,options={})
    user = self
    status =  false
    if options[:show_subscription_popup_status] == false 
      # Dont show subscription popup for now
      return false 
    end
    return false if user.subscription_popup_launch_count.nil?
    return false if options[:deeplink_destination_name] == "premium_subscription"
    last_signin_date  = user.last_sign_in_at rescue Date.today
    if !has_premium_subscription_for_family?(family_list,options) # no premium subscription 
      if options[:deeplink_destination_name].present? || user.subscription_popup_launch_count == 0 || last_signin_date < (Date.today - 10.days)
        status = true
      end
    end
    status
  end

  
 
 def valid_family_to_subscribe(family_list=[])
   data = nil
   family_list.each do |family|
     data = family
     break if FamilyMember.where(family_id:family.id,:role.in=>FamilyMember::CHILD_ROLES).present?
   end
   data
 end
  def latest_subsription_popup_event
    report_event = ::Report::Event.where(name:'show subscription popup').last
    Report::ApiLog.get_log_event(report_event.custom_id,user.id.to_s).last
  end

  # linked user in clinical system eg EMIS
  def get_proxy_user_list(options={})
    member = self.member
    current_user = member
    options[:current_user] = current_user
    data = nil
    begin
      clinic_link_detail =  ::Clinic::LinkDetail.get_clinic_link_detail(member.id,member,options)
      if clinic_link_detail.present? ||  member.ods_code.present?
        clinic_detail = member.get_clinic_detail(current_user,options)
        ods_code = (clinic_link_detail.link_info["practice_ods_code"] || member.ods_code) rescue  member.ods_code 
        if clinic_detail.blank? && ods_code.present?
          clinic_data = Nhs::NhsApi.get_clinic_by_ods_code(current_user,current_user.id,ods_code,options)
          if clinic_data.present?
            ::Clinic::ClinicDetail.save_clinic_detail(clinic_data,{:organization_type=>"emis"})
          end
        end
      end
      if clinic_link_detail.present?  && !clinic_link_detail.two_factor_auth_required?
        class_name = ::Clinic::ClinicDetail.get_clinic_linkable_class(current_user,member,"RegisterUser",options)
        data = class_name.get_proxyUser(member.organization_uid,member.id,{:current_user=>member})
        options[:proxy_user_data] = data
      end
      
      if member.clinic_account_status == "Active" &&  !member.is_clinic_api_request_excced?(options)
        ::Clinic::LinkDetail.delink_proxy_user(current_user,linked_proxy_member_ids=nil,options)
      end
    rescue Exception => e 
      Rails.logger.info "Error inside get_proxy_user_list .... #{e.message}"
    end
    data
  end

  def nhs_login_button_status(current_user,api_version=5)
    user = self
    user_country = Member.member_country_code(user.country_code)
    return false if user_country != "gb"
    System::Settings.nhs_login_button(current_user,api_version).to_s == "true"
  end
  
  # return true if user has primium subscription  or don't have any child in family
  def has_premium_subscription_for_family?(family_list=nil,options={})
    user = self
    current_user = user.member
    family_list = family_list || current_user.user_families
    # check family has pricing exemption all 

    return true if !(family_list.map(&:pricing_exemption) - ["none","basic"]).blank?
    return true if current_user.children_ids(family_list).blank?
    family_ids = family_list.map(&:id)
    family_subscription_manager = Subscription.where(:family_id.in=>family_ids).not_expired.pluck(:subscription_manager)
    family_subscription_listing_order = SubscriptionManager.where(:id.in=>family_subscription_manager).pluck(:subscription_listing_order).compact
    family_subscription_listing_order.max.to_i > 0 # has premium subscription
  end

  def self.user_basic_info(current_user,user_families_data=nil,api_version=1)
    families = user_families_data || current_user.user_family_ids
    family_ids = families.map(&:to_s)
    child_member_ids = FamilyMember.in(family_id:family_ids,role:FamilyMember::CHILD_ROLES).pluck(:member_id).uniq.map(&:to_s)

    score = Score.where(:family_id.in=>family_ids).sum(:point) + current_user.scores.where(:activity_type.in => ['User','Invitation']).sum(:point)
    score_badge = Score::badge(score)
    info ={}
    options = {}
    user = current_user.user
    user_device = current_user.devices.last
    user.first_name = current_user.first_name
    user.last_name = current_user.last_name
    clinic_detail = current_user.get_clinic_detail(current_user,options) || Clinic::ClinicDetail.new
    current_user_attributes = current_user.attributes
    current_user_attributes[:clinic_details] = clinic_detail.format_data(current_user,current_user,options) rescue nil
    
    info[:location] = user.country_code
    user[:proxy_users] = user.get_proxy_user_list({:fetch_date_of_birth=>false})
    user[:push_token] = user_device.token rescue nil
    user[:push_skip_count] = user_device.screen_skip_count rescue 0
    user[:show_push_notification_screen] = user_device.screen_skip_up_to.in_time_zone < Time.now.in_time_zone rescue true
    user[:member_active_tools] = self.member_active_tools(child_member_ids,current_user,api_version)
    user[:tools_list], user[:subscription_status],user[:subscription_level] = user_subscription(current_user,api_version) #rescue []
    user[:has_premium_subscription] = user[:subscription_level].to_i > 0
    user[:nhs_gp_search_active] = ::System::Settings.nhsGPSearchApiActive?(current_user,options)
    info[:metric_system] = current_user.metric_system
    info[:measurement_min_max_value] = Health.health_min_max_value(info[:metric_system])
    info[:eye_chart_min_max_value] = GlobalSettings::EyeChartMinMax
    info[:pregnancy_health_min_max_value] = Pregnancy::HealthCard::Measurement.health_min_max_value(info[:metric_system])
    info[:time_zone] = ActiveSupport::TimeZone.zones_map(&:name).include?(current_user.time_zone) ? current_user.time_zone : "UTC"
    info[:time_zone] = "Kolkata" if current_user.time_zone == "Asia/Kolkata"
    Time.zone = "London"
    info[:day_light_saving] = Time.zone.now.dst? rescue false
    Time.zone = info[:time_zone]
    info[:mail_subscribe] = MemberSetting.mailer_subsciber?(current_user.id)
    info[:notification] = current_user.notification_subscribed?
    info[:score] = score
    info[:score_badge] = score_badge
    user[:reauthentication_required] = current_user.reauthentication_required?(current_user,nil)
    user[:re_authentication_timeout_time] = GlobalSettings.timeout_authentication
    user[:show_nhs_login] = user.nhs_login_button_status(current_user,api_version)
    user[:health_care_widget_status] = ::System::Settings.show_health_care_widget(current_user,api_version)

    user[:is_email_verified] = user.email_verification_status
    user[:email_verification_opt_out_time] = GlobalSettings::EmailVerificationAfterSignup[:allowed_time_toString]
    picture =  user.cover_image
    current_user_profile_image = current_user.profile_image
    user_profile_image = (current_user.current_profile_image_url("small").blank? ? nil : current_user.current_profile_image_url("small",api_version) )  rescue ""
    cover_image =  picture.blank? ? nil : picture.attributes.merge(url_small: picture.complete_image_url(:small,api_version), url_large: picture.complete_image_url(:medium,api_version), url_original: picture.complete_image_url(:original,api_version), aws_small_url: picture.aws_url(:small,api_version), aws_original_url: picture.aws_url(:original,api_version))
    {:user=> user,:sign_in_count=>user.sign_in_count,:token=>user.api_token,:refresh_token=>user.auth_token, :setting_info=> info ,:cover_image=> cover_image, :member=>current_user_attributes,:profile_image=> user_profile_image , :aws_profile_image_url => (current_user_profile_image.aws_url("small",api_version) rescue "") } 
  end
  
  def self.is_alexa_platform?(platform) 
    (platform == "alexa" || platform == "googleassistance")
  end  
  
  def get_user_segment
    user = self
    user.segment
  end  

  def has_valid_segment?(batch_notification_manager,options={})
    begin
      user = self
      return true if options[:test_without_validation] == true
      if batch_notification_manager.user_segment == "NA" || batch_notification_manager.user_segment.blank?
        return true
      end
      user_segment = user.get_user_segment
      status  = [batch_notification_manager.user_segment].flatten.include?(user_segment)
    rescue Exception=> e
      Rails.logger.info e.message
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside has_valid_segment")
      status = false
    end
    return status
  end
  
  def update_user_segment(signin_count=nil,options={})
    user = self
    signin_count = signin_count || user.sign_in_count
    if signin_count <= GlobalSettings::UserSegment[:trial]
      segment_value = "trial"
    elsif signin_count <= GlobalSettings::UserSegment[:starter]
      segment_value = "starter"
    elsif signin_count <= GlobalSettings::UserSegment[:adoptor]
      segment_value = "adoptor"
    else
      segment_value = "ambassador"
    end
    if user.segment != segment_value
      report_event = Report::Event.where(name:"mark user segment #{segment_value}").last
      ::Report::ApiLog.log_event(user.id,options[:family_id],report_event.custom_id,options) rescue nil

      user.segment = segment_value
      user.save
    end
  end
  def update_family_children_country_code(country_code)
    begin
      user = self 
      member = user.member
      Family.where(member_id:member_id).update_all(country_code:country_code) rescue nil
      mother_father_role_family_ids =  FamilyMember.where(member_id:member.id).in(:role=>[FamilyMember::ROLES[:mother],FamilyMember::ROLES[:father]]).order_by("role desc").pluck(:family_id)
      family_children_id = member.childern_ids(mother_father_role_family_ids)
      ChildMember.where(:id.in=>family_children_id).update_all(:country_code=>country_code)
    rescue Exception=> e 
      Rails.logger.info "Error inside update_family_children_country_code ..........#{e.message}"
    end
  end

  # changed pwd plain txt to encrypt for v2.this method will decrypt
  def has_valid_password?(data)
    begin
      pwd = ShaAlgo.decryption(data)
      self.valid_password?(pwd)
    rescue Exception=> e
      false
    end
  end
end
