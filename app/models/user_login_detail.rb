class UserLoginDetail
  include Mongoid::Document  
  include Mongoid::Timestamps

  field:login_time , type:Time
  field:platform , type:String
  
  belongs_to :user
  # index({user_id:1})
  # index({login_time:1})

  before_create :update_user_last_signin_at
  after_create  :update_user_subscription_popup_launch
  
  def update_user_subscription_popup_launch
    user.update_subscription_popup_launch_count
    PopupLaunch.update_app_review_popup_launch_count(user.member_id) rescue nil
    # PopupLaunch.update_app_referral_popup_launch_count(user.member_id) rescue nil
    member = user.member
    options = {}
    member.delay.update_family_and_user_segment(member,options)
    options[:user_id] = user.id
    options[:update_family_activity_status] = false
       
    activity_status = "Active"
    user.mark_user_activity_status(activity_status,options)
    if user.activity_status_v2 != activity_status && user.has_active_family?
      user.mark_user_activity_status_v2(activity_status,options)
    end
  end

  def self.user_login_activity(in_last_days=30,counts=2)
    begin
    logged_user = []
     UserLoginDetail.where(:login_time.gte=> (Time.now - in_last_days.days),:login_time.lte=>Time.now).group_by(&:user_id).map do |k,v|
        logged_user << k if v.count >= counts
      end
      logged_user.count
    rescue Exception=>e
      Rails.logger.info e.message
    end
  end

  private

  def update_user_last_signin_at
    user.update_attribute(:last_sign_in_at,Time.zone.now) rescue nil
    
  end
    
end