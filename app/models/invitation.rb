class Invitation 
  include Mongoid::Document
  include Mongoid::Timestamps
  field :family_id,      :type => String
  field :member_id,    :type => String
  field :sender_id,      :type => String
  field :sender_type,      :type => String
  field :status,      :type => String
  field :role,    :type => String
  field :message,      :type => String
  field :invitation_type,    :type => String

  belongs_to :member, inverse_of: :member
  belongs_to :family
  belongs_to :sender, inverse_of: :member, foreign_key: "sender_id", class_name: "Member"

  STATUS = {:accepted=>'accepted', :invited => 'invited', :rejected => 'rejected', :deleted => 'deleted', :joined => 'joined'}

  TYPE = {:family => 'Family', :member => 'Member'}

  index ({member_id:1})
  index ({family_id:1})
  index ({sender_type:1})
  index ({status:1})

  def accept
    update_attribute(:status,'accepted')
    UserMailer.notify_sender_for_invitation(self).deliver if family_id.present? #sender_type == "Family"
    # self.delete
  end  

  def reject
    update_attribute(:status,'rejected')
    UserMailer.notify_sender_for_invitation(self).deliver if family_id.present? #sender_type == "Family"
    # self.delete
  end  

  def accepted?
    status == 'accepted'
  end

  def rejected?
    status == 'rejected'
  end  

end