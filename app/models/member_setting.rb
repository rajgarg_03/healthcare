class MemberSetting
 
  include Mongoid::Document
  include Mongoid::Timestamps  
  field :name, type: String
  field :note, type: String
  field :status, type: String
  field :data, type: Hash
  belongs_to :member 

  
  index ({member_id:1})
  index ({name:1})
  # MailerType = {0=>"BirthdayMailer", 1=>"ReminderMailer", 2=>"InvitatonMailer", 3=>"PromotionMailer"} not for now
  MailerType = {0=>"Receive Emails"}
  
  def self.mailer_subsciber?(member_id)
    begin
     data =  where(member_id:member_id, name:"mail_subscribe").first
     data.blank? || data.note == MemberSetting::MailerType.keys.join(",")
    rescue Exception=> e
     false
    end
  end
end
=begin

  MemberSetting.all.map(&:name).uniq
 "timeline_setting"  => "auto timeline entries",
 "google_syn" = set google event calendar sync setting"
 "length_metric_system" => "set measurement length unit", 
 "weight_metric_system" => "set measurement length unit"
 "health_setting" => "enabled parameter for health to edit set by user"
 "timeline_display" => remind me later for automated entries
 "milestone_display" => "remind me later for automated entries"
 "display_immunisation_info" => "first time popup if no entry in immunisation"
 "display_calendar_info" => "first time popup if no entry in calendar"
 "display_timeline_info" => "first time popup if no entry in timeline"
 "display_milestones_info" => first time popup if no entry in Milestone"
 "display_health_info" => first time popup if no entry in Health"
 "display_documents_info" =>  "first time popup if no entry in Document"
 "mail_subscribe" => 'Setting for User unsubscribe for mailer '
 "preg_info" => 'Setting for pregent child info'
 "subscribe_notification" => "use for mobile push notification"
=end
