class Subscription::History
  include Mongoid::Document
  include Mongoid::Timestamps
  # field :secret_key,     :type => String
  field :start_date, type:Date # subscription purchase date
  field :end_date, :type=> Date
  field :subscription_manager_id, :type=>String
  field :last_renewal_date, type:Date # subscription last renewal date
  field :cancellation_date, type:Date      # subscription expiry date / cancel date 
  field :expiry_date, type:Date   # subscription expiry date 
  field :free_trial_end_date, type:Date # free trial end date
  field :subscription_valid_certificate, type:String # true/false/grace
  field :platform, type:String  # android,ios
  field :product_id, type:String
  field :buyer_name, type:String
  field :transaction_id, type:String
  field :assigned_by, :type=>String, :default=>"user"
  belongs_to :family
  belongs_to :member
  belongs_to :subscription

  def self.create_record(subscription,receipt_response)
    last_subscription_histroy = Subscription::History.where(subscription_id:subscription.id).last
    new_start_date = (subscription.start_date || Date.today) rescue Date.today
    last_subscription_end_date = subscription.expiry_date
    if subscription.platform == "ios"
      transaction_id = receipt_response[:transaction_id] || last_subscription_histroy.transaction_id rescue nil
    else
      transaction_id = receipt_response[:order_id] || receipt_response[:transaction_id] || last_subscription_histroy.transaction_id rescue nil
    end
    transaction_id =  transaction_id.split("..")[0] rescue transaction_id
    subscription_manager = subscription.subscription_manager
    if Subscription.is_free?(subscription_manager.subscription_listing_order.to_i)
        subscription.subscription_valid_certificate = "true"
        subscription.free_trial_end_date = nil
        subscription.last_renewal_date = nil
        subscription.expiry_date = nil
        subscription.cancellation_date = nil
    end
    new_record_data = {
      :subscription_manager_id=>subscription.subscription_manager_id,
      :last_renewal_date => subscription.last_renewal_date,
      :cancellation_date=> subscription.cancellation_date,
      :expiry_date => subscription.expiry_date,
      :free_trial_end_date => subscription.free_trial_end_date,
      :subscription_valid_certificate=> subscription.subscription_valid_certificate,
      :platform => subscription.platform,
      :product_id => subscription.product_id,
      :buyer_name => subscription.buyer_name,
      :assigned_by=> subscription.assigned_by,
      :family_id => subscription.family_id,
      :member_id => subscription.member_id,
      :subscription_id => subscription.id,
      :transaction_id => transaction_id,
      :start_date => new_start_date
    }
    if !(check_already_existing(new_record_data,last_subscription_histroy))
      subscription_histroy_record = Subscription::History.create(new_record_data)
      if last_subscription_histroy.present?
        last_subscription_histroy.end_date = last_subscription_end_date
        last_subscription_histroy.save
      end
    end
     
    subscription_histroy_record
  end

  def self.check_already_existing(new_record_data, last_subscription_histroy )
    return false if last_subscription_histroy.blank?
    last_transaction_id = last_subscription_histroy[:transaction_id].to_s.split("..")[0] rescue nil 
    new_record_data[:transaction_id]  = new_record_data[:transaction_id].split("..")[0] rescue new_record_data[:transaction_id]
    return true if new_record_data[:transaction_id].to_s == last_transaction_id
    
   (new_record_data[:subscription_manager_id].to_s == last_subscription_histroy[:subscription_manager_id] &&
    new_record_data[:last_renewal_date] ==  last_subscription_histroy[:last_renewal_date]  &&
    new_record_data[:cancellation_date] ==  last_subscription_histroy[:cancellation_date] &&  
    new_record_data[:expiry_date]       ==       last_subscription_histroy[:expiry_date].to_date &&
    new_record_data[:free_trial_end_date] ==    last_subscription_histroy[:free_trial_end_date]  &&
    new_record_data[:subscription_valid_certificate] == last_subscription_histroy[:subscription_valid_certificate]  &&
    new_record_data[:platform]    == last_subscription_histroy[:platform] &&
    new_record_data[:product_id]  == last_subscription_histroy[:product_id] && 
    new_record_data[:buyer_name]  == last_subscription_histroy[:buyer_name] && 
    new_record_data[:assigned_by] == last_subscription_histroy[:assigned_by] &&
    new_record_data[:family_id].to_s   == last_subscription_histroy[:family_id].to_s &&
    new_record_data[:member_id].to_s   == last_subscription_histroy[:member_id].to_s &&
    new_record_data[:subscription_id].to_s == last_subscription_histroy[:subscription_id].to_s
    )
  end


  def self.get_data(current_user,options={})
    subscription_history = Subscription::History.where(member_id:current_user.id)
    response = []
    subscription_history.each do |history|
         data = {}
         data[:family_name]= history.family.name rescue nil
         data[:subscription]= SubscriptionManager.find(history.subscription_manager_id).title rescue ''
         data[:assigned_by]= history.assigned_by rescue ''
         data[:start_date]= history.start_date rescue ''
         data[:end_date] =  history.end_date.to_date.strftime("%Y-%m-%d") rescue ''  
         data[:cancellation_date] =  history.cancellation_date.to_date.strftime("%Y-%m-%d") rescue '' 
         data[:expiry_date] =  history.expiry_date.to_date.strftime("%Y-%m-%d") rescue ''  
         data[:subscription_valid_certificate] =  history.subscription_valid_certificate
          response << data  
    end
    sort_detail =  options[:order]["0"]
    if sort_detail.present?
      sort_values = ['',:family_name,:subscription,:assigned_by,:start_date,:end_date,:cancellation_date,:expiry_date,:subscription_valid_certificate]
      if sort_detail["dir"] == "asc"
        response = response.sort_by{ |t| t[sort_values[sort_detail["column"].to_i]] } 
      else
        response = response.sort_by{ |t| t[sort_values[sort_detail["column"].to_i]] } .reverse

      end
    end
    response
  end
end