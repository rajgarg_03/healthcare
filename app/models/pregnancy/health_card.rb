class Pregnancy::HealthCard

  def self.chart_x_y(data)
    begin
      x_axis = data.map{ |row| row[0].to_i }
      y_axis = data.map{ |row| row[1].to_i }
      min_x_axis = x_axis.min
      max_x_axis = x_axis.max 
      min_y_axis = y_axis.min > 40 ? 40 : y_axis.min
      max_y_axis = y_axis.max + 1
      max_x_axis = max_x_axis.to_i > 40 ? max_x_axis : 40
       {min_x:0, max_x: max_x_axis, min_y:min_y_axis, max_y: max_y_axis}
    rescue
      {min_x:0, max_x: 40, min_y:0, max_y: 0}
    end
  end
end