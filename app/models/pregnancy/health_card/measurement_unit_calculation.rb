module Pregnancy::HealthCard::MeasurementUnitCalculation
  UNIT_VAL = {"kgs"=>1, "lbs"=>2.204, "cms"=>1 , "inc"=>0.394, "inches"=>0.394}

   def calulate_bmi(weight, height)
     Health.new.calulate_bmi(weight, height)
  end
  # for Api 
  def api_convert_to_unit(option={"length_unit"=>"to_inches","weight_unit"=>"to_lbs","data"=>nil})
    params = option["data"] || self.attributes
    length_unit = option["length_unit"]
    weight_unit = option["weight_unit"]
    #if length_unit == "inches" 
      params["height"] = (params["height"].to_f * UNIT_VAL[length_unit]).round(1) if params["height"].present?
      params["head_circum"] = (params["head_circum"].to_f * UNIT_VAL[length_unit]).round(1) if params["head_circum"].present?
      params["waist_circum"] = (params["waist_circum"].to_f * UNIT_VAL[length_unit]).round(1) if params["waist_circum"].present?
    # elsif length_unit == "cms" 
    #   params["height"] = (params["height"].to_f / UNIT_VAL["inches"]).round(1) if params["height"].present?
    #   params["head_circum"] = (params["head_circum"].to_f / UNIT_VAL["inches"]).round(1) if params["head_circum"].present?
    #   params["waist_circum"] = (params["waist_circum"].to_f / UNIT_VAL["inches"]).round(1) if params["waist_circum"].present?
    # end

    if weight_unit == "lbs" 
      params["weight"] = (params["weight"].to_f * UNIT_VAL["lbs"]).round(1) if  params["weight"].present?
    elsif weight_unit == "kgs"
      params["weight"] = (params["weight"].to_f / UNIT_VAL["lbs"]).round(1) if params["weight"].present?
    end
    params["height"] = params["height"].to_f.round(1)
    params["weight"] = params["weight"].to_f.round(1)
    params["head_circum"] = params["head_circum"].to_f.round(1)
    params["waist_circum"] = params["waist_circum"].to_f.round(1)
    params["bmi"] = params["bmi"].to_f.round(1)
    params["updated_at"] = params["updated_at"].in_time_zone rescue nil 
    return params
  end


  def convert_to_cms_kgs(metric_system,params={},round_off=1)
      metric_system = metric_system.with_indifferent_access
      health_unit_to_convert = {}
      health_unit_to_convert["length_unit"] = "cms" if metric_system["length_unit"] == "inches"
      health_unit_to_convert["weight_unit"] = "kgs" if metric_system["weight_unit"] == "lbs"
      health_unit_to_convert["data"] = params
      Health.new.api_convert_to_unit(health_unit_to_convert,round_off)
    end

  def unit_convert(metric_system,round_off=1)
    begin
      metric_system = metric_system.with_indifferent_access
      health = Health.new(height:self.height,weight:self.weight)
      convert_unit_val_list = {}
      convert_unit_val_list["length_unit"] =  metric_system["length_unit"] == "inches" ? "inches" : ""      
      convert_unit_val_list["weight_unit"] =  metric_system["weight_unit"] == "lbs"  ? "lbs" : ""
      health.api_converted_data(convert_unit_val_list,round_off=2)
    rescue
      health = {}
    end
   end
end