class Pregnancy::HealthCard::Measurement
 include Pregnancy::HealthCard::MeasurementUnitCalculation
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :height, type: String
  field :weight, type: String
  # field :head_circum, type: String 
  field :bmi, type: String
  field :pregnancy_week, type:String
  field :test_date, type:Time
  belongs_to :pregnancy
  validates_presence_of :test_date
  
  has_many :timelines, :as=> :timelined, :dependent => :destroy
  index ({pregnancy_id:1})
  index ({test_date:1})
  
  def self.nhs_syndicate_content(pregnancy,options={})
    begin
      country_code = pregnancy.get_country_code(options)
      options[:country_code] = country_code
      options[:tool_class] = "Pregnancy::HealthCard::Measurement"
      options[:without_content] = true
      ::Nhs::SyndicatedContentLinkTool.nhs_content_list_linked_with_tool(nil,options).map{|a| a.syndicated_format_data(nil,nil,options)}
    rescue Exception=>e 
      nil
    end
  end

  def to_api_json(metric_system)
    data = self.attributes
    converted_data = unit_convert(metric_system,round_off=2).with_indifferent_access
    data["height"] = converted_data[:height].round2
    data["weight"] = converted_data[:weight].round2
    week_val = pregnancy_week.to_s rescue "0"
    data["week"] = week_val + " Week"
    data["date"] = (data["week"] + ", " + self.test_date.to_date.to_date3 ) rescue nil
    data
  end
# Pregnancy::HealthCard::Measurement.health_min_max_value
def self.health_min_max_value(option={"length_unit"=>"cms","weight_unit"=>"kgs"},&block)
  if option["length_unit"] == "cms" || option["length_unit"].blank?
   minimum_range = GlobalSettings::HealthCardMin[:cms]
   maxmimum_range = GlobalSettings::HealthCardMax[:cms]
   length_param =  {"height_min"=> minimum_range["height"], "height_max"=> maxmimum_range["height"],}
  else
    minimum_range = GlobalSettings::HealthCardMin[:inches]
    maxmimum_range = GlobalSettings::HealthCardMax[:inches]
   length_param =  {"height_min"=> minimum_range["height"], "height_max"=> maxmimum_range["height"]}
 end
  if option["weight_unit"]=="kgs" || option["weight_unit"].blank?
    minimum_range = GlobalSettings::HealthCardMin[:kgs]
    maxmimum_range = GlobalSettings::HealthCardMax[:kgs]
    weight_param = {"weight_min"=> minimum_range["weight"],"weight_max"=>maxmimum_range["weight"]}
  else
    minimum_range = GlobalSettings::HealthCardMin[:lbs]
    maxmimum_range = GlobalSettings::HealthCardMax[:lbs]
    weight_param = {"weight_min"=> minimum_range["weight"],"weight_max"=>maxmimum_range["weight"]}
  end
  return length_param.merge!(weight_param).merge!(GlobalSettings::PregnacyBloodPressure).merge!(GlobalSettings::Hemoglobin)

  end

  def self.chart_info(pregnancy)
    blank_values = ["0", "0.0", "", nil]
    health_records = pregnancy.pregnancy_measurements.order_by("test_date desc")
    bmi_data = health_records.not_in(:bmi => blank_values ).collect{|rec| [rec["pregnancy_week"],rec["bmi"].to_f.round(1)]}
    weight_data = [[]]
    who_data = WhoData::Pregnancy::Chart.bmi
    [bmi_data,weight_data,who_data]
  end
 
end