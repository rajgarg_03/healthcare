class Pregnancy::HealthCard::Hemoglobin
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :hemoglobin_count, type: String
  field :test_date, type: Time
  field :pregnancy_week, type: String
  belongs_to :pregnancy
  validates_presence_of :test_date
  validates_presence_of :hemoglobin_count
  has_many :timelines, :as=> :timelined, :dependent => :destroy
  index ({pregnancy_id:1})
  
  High = 16
  Low = 10

  def to_api_json
    data = self.attributes
    data["unit"] = "g/dl"
    data["hemoglobin_count"] = data["hemoglobin_count"].to_f.round(2)
    week_val = pregnancy_week.to_s
    data["week"] = week_val + " Week"
    data["date"] = (data["week"] + ", " + test_date.to_date.to_date3 ) rescue nil
    data["result"] =  self.hemoglob_result
    data
  end
  
   def self.nhs_syndicate_content(pregnancy, options={})
    begin
      country_code = pregnancy.get_country_code(options)
      options[:country_code] = country_code
      
      options[:tool_class] = "Pregnancy::HealthCard::Hemoglobin"
      options[:without_content] = true
      ::Nhs::SyndicatedContentLinkTool.nhs_content_list_linked_with_tool(nil,options).map{|a| a.syndicated_format_data(nil,nil,options)}
    rescue Exception=>e 
      nil
    end
  end
  
  def last_updated_record
    return nil if hemoglobin_count.blank?
    begin
      hemoglobin_count.to_f.round(1).to_s + " g/dl"
    rescue
       nil
    end
  end
  def self.chart_x_y(data)
    begin
      x_axis = data.map{ |row| row[0].to_i }
      y_axis = data.map{ |row| row[1].to_i }
      min_x_axis = x_axis.min
      max_x_axis = x_axis.max
      min_y_axis = y_axis.min
      max_y_axis = y_axis.max
      max_x_axis = max_x_axis.to_i > 40 ? max_x_axis : 40
       {min_x:0, max_x: max_x_axis, min_y:min_y_axis, max_y: max_y_axis}
    rescue
      {min_x:0, max_x: 40, min_y:0, max_y: 0}
    end
  end

  def hemoglob_result
    return nil if self.hemoglobin_count.blank?
    count  = self.hemoglobin_count.to_f.round(1) rescue 0
    status = case 
      when count > High  then "High"
      # when count < High && count > Low then "Normal"
      when count < Low then "Low"
      else
        "Normal"
      end

      return  status
  end
end
