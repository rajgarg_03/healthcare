class Pregnancy::HealthCard::BloodPressure
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :systolic, type: String
  field :diastolic, type: String
  field :pulses, type: String
  field :test_date, type:Time
  field :pregnancy_week, type:String  
  validates_presence_of :test_date
  
  belongs_to :pregnancy
  PLUSES_UNIT = "BPM"
  SYSTOLIC_UNIT = "mmHg"
  
  def to_api_json
    data = self.attributes
    data["systolic_unit"] = SYSTOLIC_UNIT
    data["pulses_unit"] = PLUSES_UNIT
    data["diastolic_unit"] = SYSTOLIC_UNIT
    data["systolic"] = systolic
    data["diastolic"] = diastolic
    data["pulses"] = pulses
    week_val =  pregnancy_week.to_s rescue "0"
    data["week"] = week_val + " Week"
    data["date"] = (data["week"] + ", " + self.test_date.to_date.to_date3) rescue nil
    data["result"] = self.bp_result
    data
  end

   
  def self.nhs_syndicate_content(pregnancy,options={})
    begin
      country_code = pregnancy.get_country_code(options)
      options[:country_code] = country_code
      options[:tool_class] = "Pregnancy::HealthCard::BloodPressure"
      options[:without_content] = true
      ::Nhs::SyndicatedContentLinkTool.nhs_content_list_linked_with_tool(nil,options).map{|a| a.syndicated_format_data(nil,nil,options)}
    rescue Exception=>e 
      nil
    end
  end

  def self.who_chart_data
    WhoData::Pregnancy::Chart.blood_pressure
  end
  
  def updated_bp
    last_bp_value = last_updated_record
    if last_bp_value.present?
      [last_bp_value, (pulses.to_s + PLUSES_UNIT)].compact.join(", ")
    else
      nil
    end
    # if last_updated_record.present?
    #   last_updated_record + 
    # systolic.to_s + "/" + diastolic.to_s + SYSTOLIC_UNIT + ", " + pulses.to_s + PLUSES_UNIT
  end

  def last_updated_record
    begin
      if systolic.present? && diastolic.present?
        systolic.to_s + "/" + diastolic.to_s + " " + SYSTOLIC_UNIT
      else
        nil
      end
    rescue
       nil
    end
  end


  def last_updated_values(pregnancy_id)
    # pregnancy_id = params[:preg_blood_press][:pregnancy_id]
    systolic_val = where(pregnancy_id:pregnancy_id).not_in(:systolic=>nil).order_by("test_date desc").pluck(:systolic).last
    diastolic_val = where(pregnancy_id:pregnancy_id).not_in(:diastolic=>nil).order_by("test_date desc").pluck(:diastolic).last
    pulses_val = where(pregnancy_id:pregnancy_id).not_in(:pulses=>nil).order_by("test_date desc").pluck(:pulses).last
    [systolic_val,diastolic_val,pulses_val]
  end

  def self.prepare_data_to_save(params,pregnancy)
    

    test_date_val = params[:preg_blood_press][:test_date].to_date
    pregnancy_week = pregnancy.pregnancy_week(test_date_val)
    params[:preg_blood_press][:pregnancy_week] = pregnancy_week
    params
  end
  
  def self.create_record(current_user, params,pregnancy)
    params = Pregnancy::HealthCard::BloodPressure.prepare_data_to_save(params,pregnancy)
    record = create(params[:preg_blood_press])
    record
  end

  def update_record(current_user,params,pregnancy)
    params = Pregnancy::HealthCard::BloodPressure.prepare_data_to_save(params,pregnancy)
    update_attributes(params[:preg_blood_press])
    self
  end

  def self.chart_x_y(data)
    axis_data  = {}
    if data.present?
      data.each do |a|
        key = a.keys.first
        axis_data[key] = cal_chart_x_y(a[key])
      end
    end
    axis_data
  end
  def self.cal_chart_x_y(data)
    begin

      x_axis = data.map{ |row| row[0].to_i }
      y_axis = data.map{ |row| row[1].to_i }
      min_x_axis = x_axis.min
      max_x_axis = x_axis.max 
      min_y_axis = y_axis.min
      max_y_axis = y_axis.max + 1
      max_x_axis = max_x_axis.to_i > 40 ? max_x_axis : 40
      {min_x:0, max_x: max_x_axis, min_y:min_y_axis, max_y: max_y_axis}
    rescue
      {min_x:0, max_x: 0, min_y:0, max_y: 0}
    end
  end
  #https://www.cdc.gov/bloodpressure/measure.htm
  def bp_result
    return nil if self.systolic.blank? && self.diastolic.blank?
    systolic_val = self.systolic.to_i rescue 0
    diastolic_val  = self.diastolic.to_i rescue 0
    status = case
      when systolic_val > 120  then "High"
      when diastolic_val < 60  then "Low"
      else
        "Normal"
      end
    return  status
  end



def self.chart_data_value(pregnancy,data)
    data = []
    Pregnancy::HealthCard::BloodPressure.where(pregnancy_id:pregnancy.id).order_by("test_date asc").each do |record|
      systolic = (record.systolic || systolic) rescue nil
      pulses = (record.pulses || pulses) rescue nil
      diastolic = (record.diastolic) rescue nil
      data << {:pregnancy_week =>record.pregnancy_week, :systolic=>systolic,:pulses=>pulses,:diastolic=>diastolic}
    end 
    
    systolic_chart_data = data.collect{|rec| [rec[:pregnancy_week],rec[:systolic].to_f]}
    pulses_chart_data = data.collect{|rec| [rec[:pregnancy_week],rec[:pulses].to_f]}
    diastolic_chart_data = data.collect{|rec| [rec[:pregnancy_week],rec[:diastolic].to_f]}
    max_pregnancy_week = data.map{|a| a[:pregnancy_week].to_i}.max || 0
    max_pregnancy_week = max_pregnancy_week > 40 ? max_pregnancy_week : 40
    week_age = (0..max_pregnancy_week).map(&:to_i)
    chart_data = {:systolic_chart_data=>systolic_chart_data,:pulses_chart_data=>pulses_chart_data,:diastolic_chart_data=>diastolic_chart_data}
    combine_data = systolic_chart_data + pulses_chart_data + diastolic_chart_data
    return chart_data, week_age, combine_data
  end
  
end
