class Pregnancy::KickCounter::KickCountDetail
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :kick_count, type: Integer
  field :pregnancy_id, type: String
  field :added_by, type: String #system,user
  field :start_time, type:Time
  field :end_time, type:Time
  field :duration, type:String
  
  validates_presence_of :pregnancy_id, :start_time,:added_by
  
  def self.chart_data(pregnancy)
    data = []
    records = Pregnancy::KickCounter::KickCountDetail.where(:pregnancy_id=>pregnancy.id,:start_time.lte=>Time.now.in_time_zone).order_by("start_time asc")
    records.each do |kick_count|
      pregnancy_week,pregnancy_days = pregnancy.pregnancy_week(kick_count.start_time.to_date,pregnancy.expected_on,add_days=true)
      data << [eval("#{pregnancy_week}.#{pregnancy_days}"),kick_count.kick_count ]
    end
    data
  end

  def self.avg_chart_data
    [[28, 10], [40, 10]]
  end

  def self.get_all_list(pregnancy,upcoming_records=false)
    data = []
    pregnancy_id = pregnancy.id
    if upcoming_records == true
      kick_records = Pregnancy::KickCounter::ScheduledKick.upcoming(pregnancy_id).order_by("start_time asc")
    else
      kick_records = Pregnancy::KickCounter::KickCountDetail.where(:pregnancy_id=>pregnancy_id,:start_time.lte=>Time.now.end_of_day,added_by:"user").order_by("start_time desc")
    end

    kick_records.each do |kick_count|
      json_data = kick_count.in_json(pregnancy)
      json_data["kick_date"] = kick_count.start_time.in_time_zone.to_date.to_date3
      data << json_data
    end
    data
  end

  def in_json(pregnancy=nil,pregnancy_expected_on=nil)
    kick_counter = self
    pregnancy = pregnancy || Pregnancy.new({:expected_on=>pregnancy_expected_on})
    kick_counter["pregnancy_week"], kick_counter["pregnancy_days"] = pregnancy.pregnancy_week(kick_counter.start_time.to_date,pregnancy.expected_on,true)

    kick_counter["pregnancy_week_day"] = show_preg_days_format(kick_counter["pregnancy_week"], kick_counter["pregnancy_days"])
    kick_counter["kick_date"] = kick_counter.start_time.in_time_zone.to_date.to_date4
    kick_counter["formatted_start_time"] = kick_counter.start_time.in_time_zone.strftime("%I:%M %p")
    kick_counter["formatted_end_time"] = kick_counter.end_time.in_time_zone.strftime("%I:%M %p") rescue ''
    kick_counter
  end

  def show_preg_days_format(weeks, days)
    str = ''
    if weeks.to_i == 0 && days.to_i == 0
      str = '0 day'
    elsif weeks.to_i == 0
      str = days.to_s + ' day'.pluralize(days)
    elsif days.to_i == 0
      str = weeks.to_s + ' week'.pluralize(weeks)
    else
      str = weeks.to_s + ' week'.pluralize(weeks) + ' ' + days.to_s + ' day'.pluralize(days)
    end
    str
  end
  
  def validate_time
    kick_count = self
     if  kick_count.start_time.present? && kick_count.end_time.present? && kick_count.start_time > kick_count.end_time 
      raise Message::API[:error][:kick_count_time_comp]
    end
  end


  def self.create_kick_detail_count(current_user, params)
    params["kick_counter_data"]["added_by"] = "system" if params["kick_counter_data"]["added_by"].blank? 
    kick_count = Pregnancy::KickCounter::KickCountDetail.new(params["kick_counter_data"])
    kick_count.validate_time
    kick_count.duration = ((kick_count.end_time - kick_count.start_time)/1.minute).to_i.to_s + " min" if params["kick_counter_data"]["added_by"] == "user"
    kick_count.save
    kick_count
  end

  def update_kick_count(current_user,params)
    kick_count = self
    kick_count.start_time = params["kick_counter_data"]["start_time"] || self.start_time
    kick_count.end_time = params["kick_counter_data"]["end_time"] || self.end_time
    params["kick_counter_data"]["added_by"] = "system" if params["kick_counter_data"]["added_by"].blank? 
    kick_count.validate_time
    params["kick_counter_data"]["duration"] = ((kick_count.end_time - kick_count.start_time)/1.minute).to_i.to_s + " min"
    kick_count.update_attributes(params["kick_counter_data"]) 
  end

  def delete_kick_count
    self.delete
  end

  def self.get_avg_kick_count(pregnancy, time_in_hours=2,options={})
    if options[:start_time].present? && options[:end_time].present?
      filter_query = { :pregnancy_id => pregnancy.id.to_s, :end_time => {"$lte" => options[:end_time].to_time.end_of_day, "$gte" => options[:end_time].to_time.beginning_of_day } }
    else
      filter_query = { :pregnancy_id => pregnancy.id.to_s}
    end
    kick_records = Pregnancy::KickCounter::KickCountDetail.collection.aggregate([
        { "$match" => filter_query  },
          { "$group":
              { "_id": { "$subtract": [
                { "$subtract": [ "$end_time",  Time.iso8601("2017-09-30T16:00:00.000Z") ] },
                { "$mod": [ { "$subtract": [ "$end_time",  Time.iso8601("2017-09-30T16:00:00.000Z") ] }, 1000 * 60 * time_in_hours * 60]}]},
                "count": { "$sum": "$kick_count" }
              }
          }
    ])
    kick_records_per_hour = kick_records.collect{|x| x['count']}
    avg_kick = (kick_records_per_hour.sum/kick_records_per_hour.count).to_i rescue 0
    return "Avg #{avg_kick} kicks in #{time_in_hours} hours per day"
  end

end
