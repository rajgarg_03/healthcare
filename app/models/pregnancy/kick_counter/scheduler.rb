class Pregnancy::KickCounter::Scheduler
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :kick_counter_start_date, type: String
  field :reminder_time, type: String
  field :pregnancy_id, type:String
  field :kick_counter_start_time, type:Time
  validates_presence_of :pregnancy_id,:kick_counter_start_date

  def in_json
    data = self
    data["formatted_start_date"] = kick_counter_start_time.in_time_zone.to_date.to_date4 
    data["formatted_start_time"] = kick_counter_start_time.in_time_zone.strftime("%I:%M %p")
    data.as_json.except(:kick_counter_start_date, :kick_counter_start_time)
  end

  #{:scheduler_data=>{:kick_counter_start_date=>"2018-01-01", :reminder_time=>"14:44",:pregnancy_id=>58df9c48f9a2f347fd000015}}
  def self.create_update_kick_schedule(current_user,params,options={})
    validate_kick_counter_start_date(params[:scheduler_data][:kick_counter_start_date])

    if options[:kick_schedule_object].present?
      kick_schedule = options[:kick_schedule_object]
      pregnancy_id = kick_schedule.pregnancy_id
    else
      pregnancy_id = params[:scheduler_data][:pregnancy_id]
    end
     
    pregnancy = Pregnancy.find(pregnancy_id)
    kick_start_time = "#{params[:scheduler_data][:kick_counter_start_date]} #{params[:scheduler_data][:reminder_time]}"
    params[:scheduler_data][:kick_counter_start_time] = kick_start_time
    if kick_schedule.present?
      status = kick_schedule.update_attributes(params[:scheduler_data])
    else
      status = Pregnancy::KickCounter::Scheduler.new(params[:scheduler_data]).save
    end
    if status
      added_by = params[:scheduler_data][:added_by] ? params[:scheduler_data][:added_by] 
                                                    : 'system'
      Pregnancy::KickCounter::ScheduledKick.delete_all_system_upcoming_data_for_pregnancy(current_user,pregnancy.id)
      start_range_date = kick_start_time.to_date rescue Date.today
      (start_range_date..pregnancy.expected_on).each do |start_date|
        upcoming_kick_start_time = start_date.to_date1 + ' ' + params[:scheduler_data][:reminder_time]
        request_data = {:kick_counter_data=>{:start_time=>upcoming_kick_start_time,:pregnancy_id=>pregnancy.id, added_by: added_by}}.with_indifferent_access
        kick_data = Pregnancy::KickCounter::ScheduledKick.create_kick_count(current_user,request_data)
      end
    end
  end

  def self.validate_kick_counter_start_date(start_date)
    raise Message::API[:error][:kick_count_start_date_null] if ValidateParams.check_date(start_date)
  end
  
  def get_schedular_staus
    kick_schedular = self
    pregnancy = Pregnancy.find(kick_schedular.pregnancy_id)
    pregnancy_week,pregnancy_days = pregnancy.pregnancy_week(kick_schedular.kick_counter_start_date.to_date,pregnancy.expected_on,add_days=true)
    status = nil

    if pregnancy_week < 28
      status = "due"
    elsif pregnancy_week > 28 && pregnancy_week < 30
      status = "on time"
    else
      status = "overdue"
    end
    status  
  end


end