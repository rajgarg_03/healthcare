class Pregnancy::KickCounter::ScheduledKick
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :pregnancy_id, type: String
  field :start_time, type:Time
  field :added_by, type: String, default: 'system' #system,user

  def in_json(pregnancy=nil,pregnancy_expected_on=nil)
    kick_counter = self
    kick_counter["formatted_start_time"] = kick_counter.start_time.in_time_zone.strftime("%I:%M %p")
    kick_counter
  end

  def self.delete_all_system_upcoming_data_for_pregnancy(current_user,pregnancy_id,options={})
    where(:end_time=>nil,:pregnancy_id=>pregnancy_id).delete_all
  end

  def self.create_kick_count(current_user,params)
    kick_count = Pregnancy::KickCounter::ScheduledKick.new(params["kick_counter_data"])
    kick_count.save
    kick_count
  end

  def self.upcoming(pregnancy_id)
    where(:pregnancy_id=>pregnancy_id, :start_time.gte=>Time.now.end_of_day)
  end

  def update_kick_schedule(current_user, params)
    kick_start_time = "#{params[:scheduler_data][:kick_counter_start_date]} #{params[:scheduler_data][:reminder_time]}"
    request_data = {:start_time=>kick_start_time, added_by: params[:scheduler_data][:added_by]}
    update_attributes(request_data)
  end

end
