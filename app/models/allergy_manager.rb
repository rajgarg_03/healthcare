class AllergyManager
  include Mongoid::Document 
  include Mongoid::Timestamps 
  field :name, type: String
  field :member_id, type: String
  field :category, type: String
  #field :allergy_symptoms, type: Array
  field :country_code, type: String
  field :status, type: String, default:"active" # eg. active/deleted
  
  index ({country_code:1})
  index ({member_id:1})
  index ({category:1})
  index ({status:1})
  
  def self.valid(current_user,allergy_added_in_member_list=[])
    if allergy_added_in_member_list.present?
      where({"$or"=>[{:id.in=>allergy_added_in_member_list},{:status.in=>["active",nil]}]})
    else
      where(:status.in=>["active",nil])
    end 
  end
  
  def self.in_country(current_user=nil,country_code=nil)
    country_code = (country_code || current_user.user.country_code || "GB") rescue "GB"
    country_code = AllergyManager.distinct(:country_code).map(&:downcase).include?(country_code.downcase)? country_code : "GB"
    sanitize_country_code = Member.sanitize_country_code(country_code)
    where(:country_code.in=>sanitize_country_code)
  end

  def self.get_list(current_user,params)
    begin
      member_id = params[:member_id]
      member = Member.find member_id
      member_country_code = User.where(member_id:member.family_mother_father.first.to_s).first.country_code.upcase rescue nil
      allergy_added_in_member_list = Child::HealthCard::Allergy.where(member_id:member_id).pluck(:allergy_manager_id)
      if params[:category].blank? || params[:category].downcase == "all"
        user_added_list = AllergyManager.where(member_id:current_user.id).sort_by{|a| a.name.downcase}
        system_manager_list = AllergyManager.valid(current_user,allergy_added_in_member_list).where(member_id:nil).in_country(current_user,member_country_code).sort_by{|a| a.name.downcase}
      else
        user_added_list = AllergyManager.where(member_id: current_user.id).where(category:params[:category]).sort_by{|a| a.name.downcase}
        system_manager_list = AllergyManager.valid(current_user,allergy_added_in_member_list).where(member_id:nil).in_country(current_user,member_country_code).where(category:params[:category]).sort_by{|a| a.name.downcase}
      end
      allergy_manager_list = user_added_list + system_manager_list
      data = []
      allergy_manager_list.map do |allergy_manager|
       allergy_manager["added"] = allergy_added_in_member_list.include?(allergy_manager.id)
      end
      allergy_manager_list
    rescue Exception => e
      Rails.logger.info "Error in allergy manager get list#{current_user}.inspect ---#{params}"
      Rails.logger.info e.message
      allergy_manager_list = []
    end
  end

  # not in use
  def get_list1(current_user,member_id)
    allergy_manager_list = AllergyManager.in(member_id:[nil,current_user.id]).order_by("name asc").entries
    member_added_list = Child::HealthCard::Allergy.where(member_id:member_id).pluck(:allergy_manager)
    allergy_manager_list.each do |a|
    end
  end

  def self.add_new_allergy(current_user,params)
    country_code = current_user.user.country_code || "GB" rescue "GB"
    create(country: country_code, member_id:current_user.id,name:params[:name],category:params[:category])
  end
  # For admin Panel
  def update_record(params)
    begin
      allergy_params = params["AllergyManager".underscore]
      allergy_manager = self
      if allergy_manager.update_attributes(allergy_params)
        status = true
      else
        status = false
      end
    rescue Exception => e
      Rails.logger.info e.message
      status = false 
    end
    [allergy_manager,status]
  end


  def self.create_record(params)
    begin
      allergy_params = params["AllergyManager".underscore]
      system_allergy = self.new(allergy_params)
      system_allergy.save!
      status = true
    rescue Exception => e
      Rails.logger.info e.message
      status = false 
    end
    [system_allergy,status]
  end
  
  def self.duplicate_record(params)
    begin
    
    record_to_be_duplicated = AllergyManager.find(params[:record_id]).attributes
    record_to_be_duplicated.delete("_id")
    record_to_be_duplicated.delete("created_at")
    system_allergy = AllergyManager.new(record_to_be_duplicated)
    system_allergy.save!
    status = true
    rescue Exception => e
      Rails.logger.info e.message
      status = false 
    end
    [system_allergy,status]
  end
  def self.delete_record(record_id)
    begin
      system_allergy = AllergyManager.find(record_id)
      #SystemJab.where(system_vaccin_id:system_vaccine.id).delete_all
      system_allergy.delete
    rescue Exception => e
      Rails.logger.info e.message
    end
  end
  

   
  def self.duplicate_all_records(params)
    begin
      raise "Already added data for selected country" if AllergyManager.where(:country_code.in=> Member.sanitize_country_code(params[:destination_country_code])).present?
      sanitize_country_code = Member.sanitize_country_code(params[:source_country_code])
      system_allergy_list = AllergyManager.where(:country_code.in=>sanitize_country_code ).valid(current_user=nil)
      system_allergy_list.each do |system_allergy|
        allergy_params = {:record_id=>system_allergy.id}
        # allergy_params["country_code"] = params[:destination_country_code]
        system_allergy_obj,status = AllergyManager.duplicate_record(allergy_params)
        system_allergy_obj.country_code = params[:destination_country_code]
        system_allergy_obj.save
      end
      status = true
      update_member_allergy_manager_for_added_country(params[:destination_country_code])
      message = ""
    rescue Exception=> e 
      Rails.logger.info "Error in AllergyManager.duplicate_all_records"
      Rails.logger.info e.message
      status = false
      message = e.message
    end
    [status,message]
  end
  
  def self.update_member_allergy_manager_for_added_country(country_code)
    country_code = country_code.upcase
    Child::HealthCard::Allergy.distinct(:member_id).each do |child_id|
      begin
        puts "child_id"
        child = ChildMember.find(child_id)
        puts child.first_name
        puts child_id
        member = child
        member_country_code = User.where(member_id:child.family_mother_father.first.to_s).first.country_code.upcase #rescue "GB"
        if member_country_code == country_code
          Child::HealthCard::Allergy.where(member_id:child_id.to_s).each do |allergy|
            allergy_manager = AllergyManager.find(allergy.allergy_manager_id.to_s)
            existing_allergy_manager_for_cloned_country = AllergyManager.where(name:allergy.name).in_country(nil,member_country_code).last
            if existing_allergy_manager_for_cloned_country.present?
              allergy.allergy_manager_id = existing_allergy_manager_for_cloned_country.id
              allergy.save
            end
            allergy_symptom_manager = AllergySymptomManager.where(:allergy_symptoms.in=>allergy.allergy_symptoms)
            existing_symptom_manager = AllergySymptomManager.where(:name.in=>allergy_symptom_manager.map(&:tittle)).in_country(nil,member_country_code)
            if existing_symptom_manager.present?
              allergy.allergy_symptoms = existing_symptom_manager.map(&:id)
              allergy.save
            end
          end
        end
      rescue Exception => e 
        puts e.message
        puts "Inside update_member_allergy_manager_for_added_country"
        Rails.logger.info "Inside update_member_allergy_manager_for_added_country"
        Rails.logger.info e.message
      end
    end
  end
end

