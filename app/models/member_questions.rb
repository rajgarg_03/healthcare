class MemberQuestions

  include Mongoid::Document
  include Mongoid::Timestamps

  field :answer, type: String
  belongs_to :system_question
end