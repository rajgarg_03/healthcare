class Document
  include Mongoid::Document
    include Mongoid::Timestamps
    include Mongoid::Paperclip

  field :name, type: String
  field :document_type, type: String
  field :last_modified, type: Time
  field :member_id, type: String
  field :created_at, type: Time, default: DateTime.now
  field :data_source, type:String,:default=>"nurturey" # eg smrthi,emis,nurturey
  field :uid, type:String  # eg uniq id to identify doc in smrthi,emis etc
  field :html_content, type:String  # Data from GP only .eg smrthi,emis etc

  validates :name, presence: true

  paperclip_opts = Rails.env.development? ? {
    :path => ":rails_root/public/assets/documents/:id/:style/:basename.:extension",
    :url  => "/assets/documents/:id/:style/:basename.:extension"
  } : 
  {
    :storage => :s3,
      :s3_credentials => {
        :bucket => APP_CONFIG["s3_bucket"],
        :access_key_id => APP_CONFIG["s3_access_key_id"],
        :secret_access_key => APP_CONFIG["s3_secret_access_key"]
      },
      :url => ':s3_domain_url',
      :path => lambda{ |f| '/:class' + '/:id/:style/:basename.:extension'},
      :s3_protocol => 'https'
  }
  has_mongoid_attached_file :file,paperclip_opts

  # has_mongoid_attached_file :file,
  #   :path => ":rails_root/public/assets/documents/:id/:style/:basename.:extension",
  #   :url  => "/assets/documents/:id/:style/:basename.:extension"

  # validates_attachment_content_type :file, :content_type => ["image/jpg", "image/jpeg", "image/png"]

    belongs_to :member
    after_save :add_to_timeline 
    after_destroy :delete_from_timeline

  DOC_TYPES = {general: 'General', school_related: 'School Related', health: 'Health', award: 'Award & Honour'}
        
    def add_to_timeline
      timeline = Timeline.where(timelined_type: 'Document', timelined_id: self.id).first
      if timeline
        timeline.update_attributes(name: name, desc: document_type)
      else  
        Timeline.new(data_source:data_source ,entry_from: "document", name: name, desc: document_type, member_id: member_id, timeline_at: DateTime.now, timelined_type: 'Document', timelined_id: self.id).save
      end 
    end

    def delete_from_timeline
      Timeline.where(timelined_type: 'Document',timelined_id: self.id).destroy_all  
    end 

    def complete_file_url(size=:original,api_version=1)
      url = file.url
      if api_version >= 6
        url = ShaAlgo.encryption(url)
      end
      url
      # if file.try(:path) && FileTest.exist?(file.path)
      #   return (APP_CONFIG['default_host']+self.file.url)
      # end
    end

    def aws_url(size=:original,api_version=1)
      begin
        url = 'https://' + APP_CONFIG['s3_bucket'] + '.s3.amazonaws.com/documents/' + id + '/'  + size.to_s + '/' + file_file_name
        if api_version >= 6
          url = ShaAlgo.encryption(url)
        end
        url
      rescue Exception=>e
        ''
      end
    end

end