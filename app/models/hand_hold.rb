class HandHold
  
  # 5676ffea8cd03ddf81000009
  # For child - [{"hh_immunisations"=>{:id=>"5676f41a0a9a9927b6000001", :name=>"chotu", :role=>"\"Son\"", :is_expected=>false}}, {"hh_milestones"=>{:id=>"5676f41a0a9a9927b6000001", :name=>"chotu", :role=>"\"Son\"", :is_expected=>false}}, {"hh_timeline"=>{:id=>"5676f41a0a9a9927b6000001", :name=>"chotu", :role=>"\"Son\"", :is_expected=>false}}, {"hh_add_photo"=>{:id=>"5676f41a0a9a9927b6000001", :name=>"chotu", :role=>"\"Son\"", :is_expected=>false}}]
  # For pregnancy - {:handhold_controllers => [ { id =>xyz, name => xyz, is_expected => true, handhold_actions=> ['add_medical_event','add_timeline']  
  def self.get_handhold_data(current_user,ids,use_case=nil,type='member',api_version=1,options={})
    handhold_actions = HandHold.get_handhold_actions(current_user,ids,use_case,api_version, type,options)
    is_summary_screen = handhold_actions.present?
    {handhold_actions: handhold_actions, is_summary_screen: is_summary_screen}
  end

  def self.get_handhold_actions(current_user,ids,use_case,api_version=1,type='member',options={})
    all_members_handhold_controllers = []
    options = options.merge(:api_version=>api_version)
    ids.each do |id|
      begin
        if type == 'pregnancy'
          pregnancy = Pregnancy.find(id)
          member = Member.where(id:pregnancy.expected_member_id).last
        else
          member = ChildMember.find(id)
          role = member.family_members.first.role
        end
        case use_case
        when 'new_child_added', 'pregnancy_completed', 'child_screens'
         
          HandHold.child_screens(member,current_user,api_version,options).each do |handhold_action|
            summary_data = {}
             
            data = HandHold.prepare_handhold_data(current_user,handhold_action,type,member,role,api_version,options)
                                                  # current_user,handhold_action,type,record,role=nil,api_version=1,options={})
            if data.present?
              key = data.keys[0]
              summary_data = HandHold.save_data(current_user,member,handhold_action,data,"child",options) if options[:save_handhold_data].to_s == "true"
              data[key].merge!(summary_data)
              all_members_handhold_controllers <<  data 
            end
          end
        when 'pregnancy_created', 'pregnancy_screens'
          HandHold.preg_screens(pregnancy,options).each do |handhold_action|
            summary_data = {}
            data = prepare_handhold_data(current_user,handhold_action,type,pregnancy,api_version,options)
            if data.present?
              key = data.keys[0]
              summary_data = HandHold.save_data(current_user,member,handhold_action,data,"pregnancy",options) if options[:save_handhold_data].to_s == "true"
              data[key].merge!(summary_data)
              all_members_handhold_controllers << data
            end
          end
        end
      rescue Exception=> e
        Rails.logger.info "HandHold error message: #{e.message}"
        Rails.logger.info "HandHold error: #{e.backtrace}"
      end

    end
    all_members_handhold_controllers
  end

  def self.preg_screens(pregnancy,options={})
    child = pregnancy.expected_member
    screens = []
    if child.present?
      add_medical_event_is_skipped = SkipHandHoldAction.where(skipped: true,screen_id:pregnancy.id,screen_name:"add_medical_event",:skipped_up_to.gt=>Time.zone.now).present? 
      add_timeline_is_skipped = SkipHandHoldAction.where(skipped: true,screen_id:pregnancy.id,screen_name:"add_timeline",:skipped_up_to.gt=>Time.zone.now).present? 
      
      screens << "add_medical_event"  if !add_medical_event_is_skipped && child.medical_events.blank?
      screens << "add_timeline"       if !add_timeline_is_skipped && !child_tools_updated?(child,"timeline","pregnancy")
      if options[:api_version] >= 6  
        hh_pregnancy_health_card_is_skipped = SkipHandHoldAction.where(skipped: true,screen_id:pregnancy.id,screen_name:"hh_pregnancy_health_card",:skipped_up_to.gt=>Time.zone.now).present? 
        hh_kick_counter =  SkipHandHoldAction.where(skipped: true,screen_id:pregnancy.id,screen_name:"hh_kick counter ",:skipped_up_to.gt=>Time.zone.now).present? 
        hh_documents_is_skipped =  SkipHandHoldAction.where(skipped: true,screen_id:pregnancy.id,screen_name:"hh_documents",:skipped_up_to.gt=>Time.zone.now).present? 
        screens << "hh_pregnancy health card" if !hh_pregnancy_health_card_is_skipped
        screens << "hh_kick counter" if !hh_kick_counter
        screens << "hh_documents"    if !hh_documents_is_skipped 
      end
    end
    screens
  end

  def self.child_screens(child,current_user=nil,api_version=1,options={})
    hh_immunisations_is_skipped = SkipHandHoldAction.where(skipped: true,member_id:child.id,screen_name:"hh_immunisations",:skipped_up_to.gt=>Time.zone.now).or({skipped_by:current_user.id},{skipped_by:nil}).present? 
    hh_milestones_is_skipped = SkipHandHoldAction.where(skipped: true,member_id:child.id,screen_name:"hh_milestones",:skipped_up_to.gt=>Time.zone.now).or({skipped_by:current_user.id},{skipped_by:nil}).present? 
    hh_timeline_is_skipped = SkipHandHoldAction.where(skipped: true,member_id:child.id,screen_name:"hh_timeline",:skipped_up_to.gt=>Time.zone.now).or({skipped_by:current_user.id},{skipped_by:nil}).present? 

    screens = []
 
    screens << "hh_immunisations" if !hh_immunisations_is_skipped && child.vaccinations.blank?
    screens << "hh_milestones"    if !hh_milestones_is_skipped && !child_tools_updated?(child,"milestones","child") 
    screens << "hh_timeline"      if !hh_timeline_is_skipped  && !child_tools_updated?(child,"timeline","child")
    if options[:save_handhold_data].blank? || options[:save_handhold_data].to_s == "false"
      hh_add_photo_is_skipped = SkipHandHoldAction.where(skipped: true,member_id:child.id,screen_name:"hh_add_photo",:skipped_up_to.gt=>Time.zone.now).or({skipped_by:current_user.id},{skipped_by:nil}).present? 
      screens << "hh_add_photo"     if !hh_add_photo_is_skipped && child.profile_image.blank?
    end
    if api_version > 4
      if !(options[:save_handhold_data].blank? || options[:save_handhold_data].to_s == "false")
        hh_checkup_planner_is_skipped = SkipHandHoldAction.where(skipped: true,member_id:child.id,screen_name:"hh_checkup planner",:skipped_up_to.gt=>Time.zone.now).or({skipped_by:current_user.id},{skipped_by:nil}).present? 
        screens << "hh_checkup planner" if !hh_checkup_planner_is_skipped  && Child::CheckupPlanner::Checkup.where(member_id:child.id.to_s).blank?
        hh_dental_health_is_skipped   = SkipHandHoldAction.where(skipped: true,member_id:child.id,screen_name:"hh_tooth chart",:skipped_up_to.gt=>Time.zone.now).or({skipped_by:current_user.id},{skipped_by:nil}).present? 
        screens << "hh_tooth chart" if !hh_dental_health_is_skipped 
      end
    end
    if api_version >=6
      if child.is_user_fully_linked_with_clinic?(current_user,options)
        # screens << "hh_documents" #if !hh_dental_health_is_skipped 
        screens << "hh_measurements" #if !hh_dental_health_is_skipped 
        screens << "hh_allergies" #if !hh_dental_health_is_skipped 
      end
    end
    screens
  end

  # record = child record
  # nil will be returned in case there is no data for a particular handhold_action
  def self.prepare_handhold_data(current_user,handhold_action,type,record,role=nil,api_version=1,options={})
    options = options.merge({:api_version=>api_version})
    if type == 'member'
      handhold_data = HandHold.get_handhold_data_for_action(current_user,record,handhold_action,options)
      handhold_action_hash = {handhold_action => {:id => record.id, :name => record.first_name, :role => role, :is_expected => false, :data => handhold_data}} if handhold_data.present?
    elsif type == 'pregnancy'
      preg_mother_name = record.member.first_name rescue nil
      handhold_data = get_handhold_data_for_action(current_user,record,handhold_action,options)
      handhold_action_hash = {handhold_action => {:mother_name=> preg_mother_name, :id => record.id, :name => record.name, :is_expected => true, :data => handhold_data}} if handhold_data.present?
    else
      Rails.logger.info "Error:.........Incorrect Type used in HandHold" 
    end
    handhold_action_hash
  end

  #TODO: Add cases for timelines and immunizations
  def self.get_handhold_data_for_action(current_user,child,handhold_action,options={})
    begin
      api_version = options[:api_version] || 1
      case handhold_action
        when 'hh_pregnancy health card', 'pregnancy health card'
          data = {:pregnancy_health_card=>["no data"]}
        when 'hh_kick counter', 'kick counter'
          data = {:kick_counter=>["no data"]}
        when 'hh_vision health', 'vision health'
          data = {:vision_health=>["no data"]}
        when 'hh_milestones', 'milestones'
          child_hh_milestones(child,options)
        when 'hh_timeline','timeline'
          child_hh_timelines(child,"child")
        when 'hh_immunisations','immunisations'
          user_hh_immunisations(current_user,child,false)
        when 'add_medical_event','hh_prenatal tests' , 'prenatal tests'
          child_member = Member.find(child.expected_member_id)
          SysMedicalEvent.recommended_test_name(child_member)
        when 'hh_add_photo'
          {:hh_add_photo=>true}
        when "hh_checkup planner",'checkup planner'
          if api_version > 4 
            options = options.merge({:save_checkup_planner => false})
            system_checkup_planner, added_checkup_planner = System::Child::CheckupPlanner::Checkup.add_system_checkup_to_member(current_user,child_member,api_version,options)
            system_checkup_planner.entries
          end
        when "hh_tooth chart",'tooth chart'
          if api_version > 4 
            data = Child::ToothChart::ChartManager.all.entries
          end
        when 'add_timeline'
          # {default_system_timeline:  SystemTimeline.where(category:"preg").entries}
          child_member = Member.find(child.expected_member_id)
          data = child_hh_timelines(child_member,"pregnancy")
          {:default_system_timeline=> data.present? ? data[:system_timelines]:[]}
        when 'hh_documents', 'documents'
          return {:documents=> ['no data']} if child.class.to_s == "Pregnancy"
          options[:current_user] = current_user
          clinic_detail = member.get_clinic_detail(current_user,options) rescue nil
          begin
            data = []
            if child.is_user_fully_linked_with_clinic?(current_user,options)
              if clinic_detail.present?
                if clinic_detail.is_tpp?(options)
                  options[:item_type] = "TestResultsView"
                  response = ::Clinic::Tpp::HealthCard.get_patient_health_record(child.id,options)
                  
                elsif  clinic_detail.is_emis?(options)
                  options[:item_type] = "Documents"
                  response = ::Emis::HealthCard.get_patient_health_record(child.id,options)
                end  
              end
              response[:data].each{|k,v| data << v}
              data = data.flatten
            end
          rescue Exception =>e 
            Rails.logger.info "Error inside hh_documents ....#{e.message}"
            data = []
          end
          {:documents=> data.present? ? data:[]}
        when 'hh_measurements','measurements'
          options[:current_user] = current_user
          data = []
          begin
            clinic_detail = member.get_clinic_detail(current_user,options) rescue nil
            if child.is_user_fully_linked_with_clinic?(current_user,options)
              tool = Nutrient.where(identifier:"Measurements").last
              clinic = tool.get_clinic_class_for_member_tool(current_user,api_version,child.organization_uid,child,options)
              clinic.get_patient_measurements(child.organization_uid,child.id,options)
            end
            
            child.reload
          rescue Exception =>e 
            Rails.logger.info "Error inside hh_measurements ....#{e.message}"
            data = []
          end 
          {:measurements=> Health.member_health_records(child)}
        when 'hh_allergies', "child health card"
          options[:item_type] =  "Allergy"
          options[:current_user] = current_user
          data = []
          begin
            if child.is_user_fully_linked_with_clinic?(current_user,options)
              response = Emis::Allergy.get_patient_allergies(child.id,options)
              response[:data].each{|k,v| data << v}
              data = data.flatten
            end
          rescue Exception =>e 
            Rails.logger.info "Error inside hh_allergies ....#{e.message}"
            data = []
          end
          {:alleries=> data.present? ? data:[]}
          
      end
    rescue Exception=>e 
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,options,"Error inside get_handhold_data_for_action")
    end
  end
  
  def self.user_hh_immunisations(current_user,child,user_added_vaccine)
    {sys_vaccination_list:SystemVaccin.system_vaccination_list_for_user(current_user,child,user_added_vaccine,{:non_recommended=>false,:order_by=>"due_date"})}
  end


  def self.child_hh_milestones(child,options={})
    if options[:save_handhold_data].to_s == "true" || options[:api_version] > 4
      options = options.merge({:source=>"handhold"})
    end
    system_milestones =  SystemMilestone.system_milestone_for_member(child,options).only(:id,:category,:dynamic_title,:default_image_exists,:title,:sequence,:description,:milestone_type)
    system_milestone_ids = system_milestones.pluck(:id)
    # member_milestones = Milestone.in(member_id:child.id, system_milestone_id:system_milestone_ids).group_by{|p| p.system_milestone_id.to_s } #group_by("system_milestone_id")
    member_milestones = Milestone.milestone_group_by_id(child.id,system_milestone_ids)

    data = []
    child_male = child.male?
    system_milestones.each do |sys_milestone|
      sys_milestone_attributes = sys_milestone.attributes.except('title','dynamic_title')
      sys_milestone_attributes[:title] = sys_milestone.get_dynamic_title(child,child_male) || sys_milestone.title
      milestone_attr = sys_milestone_attributes
      member_milestone_info = member_milestones[sys_milestone.id]
      milestone_attr["sequence"] =  sys_milestone.sequence
      milestone_attr["default_image_exists"] = sys_milestone.default_image_exists
      if member_milestone_info.present? #["id"] == sys_milestone.id.to_s
        milestone_attr["added"] = true
       milestone_attr["milestone_date"] = member_milestone_info[0].date if member_milestone_info[0].date.present?
      else
        milestone_attr["expected_date"] =  sys_milestone.get_expected_date(child)
      end
      data << milestone_attr
    end
    data.present? ? {system_milestones: data} : {}

  end

  # Return nil if data is not present
  def self.child_hh_timelines(child,category=nil)

    timeline_category = category || (child.is_expected? ? "pregnancy" : "child")
    timeline_handhold_data = SystemTimeline.get_child_system_timlines(child,timeline_category).entries
    timeline_handhold_data.present? ? {system_timelines: timeline_handhold_data} : nil
  end

  def self.user_complete_handhold(parent_member,api_version=1,options={})
    all_members_handhold_actions = []
    family_ids = parent_member.user_family_ids
    pregnancy_ids = Pregnancy.in(family_id:family_ids).where(status:"expected").pluck(:id)
    
    family_child_member_ids = FamilyMember.where(:family_id.in=>family_ids,:role.in=>FamilyMember::CHILD_ROLES).pluck(:member_id).map(&:to_s)
    expected_child_ids = Pregnancy.in(:expected_member_id=>family_child_member_ids).pluck(:expected_member_id).map(&:to_s)
    child_member_ids = (family_child_member_ids - expected_child_ids)
    child_member_ids = ChildMember.where(:id.in=>child_member_ids).order_by("birth_date des").pluck(:id).map(&:to_s)
    
    (all_members_handhold_actions << HandHold.get_handhold_actions(parent_member,pregnancy_ids,'pregnancy_screens',api_version,'pregnancy',options)).flatten!
    (all_members_handhold_actions << HandHold.get_handhold_actions(parent_member,child_member_ids, 'child_screens',api_version,'member',options)).flatten! if all_members_handhold_actions.count < 6
    handhold_actions = all_members_handhold_actions.first(6)
    is_summary_screen = handhold_actions.present?
    {handhold_actions: handhold_actions, is_summary_screen: is_summary_screen}
  end

  def self.child_tools_updated?(child,tool,category="child")
    status = false
    case tool
    when "timeline"
      #sys_timeline_ids = []
      sys_timeline_ids = SystemTimeline.get_child_system_timlines(child,category).last["_id"] rescue nil
        
      status = Timeline.where(member_id:child.id,system_entry: "true",timelined_id:sys_timeline_ids.to_s).present?
    when "milestones"
      sys_category = child.find_category
      milestone_sequence = SystemMilestone.where(category: sys_category).first.sequence
      milestone_sequence = milestone_sequence == 1 ? 1 : ((milestone_sequence -1) rescue 1)
      system_milestones = SystemMilestone.where(:sequence=> milestone_sequence).map{|a| a.id.to_s}
      status = Milestone.where(member_id: child.id.to_s).in(system_milestone_id:system_milestones).present?
    end
    status
  end

  def self.save_data(current_user,member,handhold_screen,user_data,member_type,options={})
    data_list = []
    data_source = "nurturey"
    api_version = options[:api_version] || 4
    begin
      data = user_data[handhold_screen][:data]
    rescue Exception=>e 
      data = []
      Rails.logger.info e.message
      Rails.logger.info "Error inside Handhold save_data\n details:\n handhold_screen: #{handhold_screen} \n User: #{current_user.inspect}\n Member: #{member.inspect}\n member_type:#{member_type}\n"
      return nil
    end
    if options[:activated_tools_controller] != true
      return nil if data.blank?
    end
      # {:total_added_text=>nil,:status_text=>nil}
    if  handhold_screen == "add_timeline"||  handhold_screen == "hh_timeline"
      options = options.merge({:category=>member_type}) 
      if member_type == "pregnancy"
        highligted_text = [{:title=> "We can share the selected events to Facebook or Whatsapp directly from Nurturey", :textsToBold=> ["Facebook or Whatsapp"]}]
        system_timeline_ids = []
        data_list = []
        data[:default_system_timeline].each do |timeline| 
          system_timeline_ids << timeline["_id"] 
          # data_list = {:title=>timeline[""]}
          data_list << {:id=>timeline["_id"], :title=>timeline["name"],:subtitle=>timeline[:est_date],:added=> true, :data_source=>(timeline[:data_source] || data_source)}
        end
        status_text = ""
        event_pluralize = "event".pluralize(system_timeline_ids.count) rescue "events"
     
        subtitle1 = "I have created following #{system_timeline_ids.count} Timeline #{event_pluralize} thus far for your pregnancy"
        subtitle1TextsToBold = [" #{system_timeline_ids.count} Timeline #{event_pluralize}"]
        subtitle2 = "We can modify and add images later"
        data_key = "default_system_timeline"
      else
        # highligted_text = [{:title=>"Share selected events to Facebook or Whatsapp directly from Nurturey, keeping rest of the Timeline personal", :textsToBold=> ["Share", "Facebook or Whatsapp"]},{:title=>"Build comprehensive childhood story, as we will post automatic entries from other tools, such as Measurements.", :textsToBold=> ["comprehensive childhood story"]}]
        system_timeline_ids = []
        data[:system_timelines].each do |timeline| 
          system_timeline_ids << timeline["_id"] 
          # data_list << {:id=>timeline["_id"], :title=>timeline["name"],:subtitle=>timeline[:est_date],:added=> true,:data_source=>(timeline[:data_source] || data_source)}
        end
        # status_text = "#{system_timeline_ids.count} birthdays"
        # event_pluralize = "event".pluralize(system_timeline_ids.count) rescue "events"
        # subtitle1 = "I have created following #{system_timeline_ids.count} Timeline #{event_pluralize} for him"
        # subtitle1TextsToBold = [" #{system_timeline_ids.count} Timeline #{event_pluralize}"]
        # subtitle2 = "We can modify and add images later"
        # data_key = "system_timelines"
      end
      SystemTimeline.save_system_timeline(current_user,member,system_timeline_ids,options)
      return nil
      # total_added_text = "#{system_timeline_ids.count} timeline #{event_pluralize} added"
      # saved_data_summary = {:data_list=>data_list,:data_key=>data_key,:subtitle2=>subtitle2, :subtitle1=>subtitle1,:subtitle1TextsToBold=>subtitle1TextsToBold, :summary_text => highligted_text, :total_added_text=>total_added_text,:status_text=>status_text}

    elsif handhold_screen == "hh_milestones"
       data_list = []
      system_milestones_data = []
      (data[:system_milestones] || []).map do |system_milestone| 
        system_milestones_data << {:system_milestone_id=>system_milestone["_id"],:date=>system_milestone["expected_date"]}
        data_list << {:id=>system_milestone["_id"], :title=>system_milestone[:title],:subtitle=> ("Usually achieved in #{system_milestone['category']} months" + "\n" + "Expected date: #{system_milestone['expected_date']}"), :added=> true, :data_source=> (system_milestone["data_source"] || data_source)}
      end
      today_date = Date.today
      overdue_system_milestones_count = 0
      due_system_milestones_count = 0
      SystemMilestone.save_handhold_data(member,system_milestones_data,options)
      system_milestones_data.each do |data|
        if data[:date].to_date < today_date
          overdue_system_milestones_count = overdue_system_milestones_count + 1
        elsif data[:date].to_date >= today_date
          due_system_milestones_count = due_system_milestones_count + 1
        end
      end
      total_added_text = "#{overdue_system_milestones_count + due_system_milestones_count} milestones added"
      
      status_text = "0 completed • #{overdue_system_milestones_count} overdue •  #{due_system_milestones_count} due"
      highligted_text=[{:title=> "You can add images of these milestones to cherish"},{:title=>"Build meaningful memories, as these milestones will also appear in #{member.first_name}'s Timeline"}]

      subtitle1 = "I have marked the following #{system_milestones_data.count} growth milestones that #{member.first_name.titleize} may have accomplished by this age"
      subtitle1TextsToBold = [" marked"," #{system_milestones_data.count} growth milestones"]
      subtitle2 = "We can modify and add images later"
      data_key = "system_milestones"
      saved_data_summary = {:data_list=> data_list, :data_key=>data_key,:subtitle2=> subtitle2,:subtitle1=>subtitle1,:subtitle1TextsToBold=>subtitle1TextsToBold,:summary_text=>highligted_text, :total_added_text=>total_added_text,:status_text=>status_text}
      
    elsif handhold_screen == "hh_immunisations"
      options[:current_user] = current_user
      system_vaccine_ids = []
      (data[:sys_vaccination_list]||[]).map do |system_vaccine| 
        system_vaccine_ids << system_vaccine.id
        jab_status = system_vaccine["data_source"] != "nurturey" ? "Administered" : "Estimated due date"
        data_list << {:id=>system_vaccine["_id"], :title=>system_vaccine["name"],:subtitle=> "#{jab_status}: #{system_vaccine['estimated_due_date']}", :added=> true, :data_source=> (system_vaccine["data_source"] || data_source)}
      end
      options[:sync_with_gpsoc] = true
      SystemVaccin.add_system_vaccine_to_member(member,system_vaccine_ids,options)
      member_vaccination_ids = Vaccination.where(:member_id=>member.id.to_s,:sys_vaccin_id.in=>system_vaccine_ids.map(&:to_s)).pluck(:id).map(&:to_s)
      upcoming_immunisation_count = Jab.where(due_on_from_data_source:nil, :start_time.gte=>Date.today,:vaccination_id.in=>member_vaccination_ids).pluck(:vaccination_id).uniq.count
      overdue_immunisation_count  = Jab.where(due_on_from_data_source:nil, :start_time.lt=>Date.today,:vaccination_id.in=>member_vaccination_ids).pluck(:vaccination_id).uniq.count
      administered_immunisation_count  = Jab.where(:due_on_from_data_source.ne=>nil,:vaccination_id.in=>member_vaccination_ids).pluck(:vaccination_id).uniq.count
      immunisation_added_count = system_vaccine_ids.uniq.count
      highligted_text = [{:title=>"Identified all jabs for all immunisations"}, {:title=>"Marked estimated due dates in the family calendar"},{:title=>"Set 1 month in advance reminders"}]
      total_added_text = "#{immunisation_added_count} immunisations added to the schedule"
      status_text = "#{administered_immunisation_count} Administered • #{upcoming_immunisation_count} upcoming • #{overdue_immunisation_count} overdue"
      
      subtitle1 = "I have scheduled the following #{immunisation_added_count} immunisations common in %{child_country}"
      subtitle1TextsToBold = [" scheduled"," #{immunisation_added_count} immunisations"]
      subtitle2 = "We can correct the list later based on doctors recommendation"
      data_key = "sys_vaccination_list"

      saved_data_summary = {:data_list=>data_list,:data_key=>data_key,:subtitle2=> subtitle2,:subtitle1=>subtitle1,:subtitle1TextsToBold=>subtitle1TextsToBold,:summary_text=>highligted_text, :total_added_text=>total_added_text,:status_text=>status_text}
      
    elsif handhold_screen == "add_medical_event" || handhold_screen == "prenatal tests" || handhold_screen == "hh_prenatal tests"
      system_prenatal_test_name_list = []
      data.map do |prenatal_test| 
        system_prenatal_test_name_list << prenatal_test[:name]
        data_list << {:id=>prenatal_test["_id"], :title=>prenatal_test[:name],:subtitle=> (prenatal_test[:category].to_s + "\n" + "Estimated due date on #{prenatal_test[:est_due_date]}"), :added=> true,  :data_source=>(prenatal_test[:data_source] || data_source)}
      end
      SysMedicalEvent.save_system_prenatal_test(member,system_prenatal_test_name_list,options)
      total_added_text = "#{system_prenatal_test_name_list.count} prenatal tests added"
      status_text = ""
      highligted_text = [{:title=>"I've marked tests estimated due dates in the family calendar"}, {:title=>"Set 1 month in advance reminders "}]
     
      subtitle1 = "I have scheduled the following #{system_prenatal_test_name_list.count} pregnancy related tests common in %{child_country}"
      subtitle1TextsToBold = [" scheduled", " tests"]
      subtitle2 = "we can correct this list later, based on doctor's recommendation"
      data_key = nil
      saved_data_summary = {:data_list=>data_list,:data_key=>data_key,:subtitle2=> subtitle2,:subtitle1=>subtitle1,:subtitle1TextsToBold=>subtitle1TextsToBold,:summary_text=>highligted_text, :total_added_text=>total_added_text,:status_text=>status_text}
    
    elsif handhold_screen == "hh_checkup planner" || handhold_screen == "checkup planner"
      data_list = []
      system_checkup_plans, added_checkup_planner_list = System::Child::CheckupPlanner::Checkup.add_system_checkup_to_member(current_user,member,api_version,options.merge(:save_checkup_planner=> true))
      added_checkup_planner_list.map do |checkup_plan| 
        data_list << {:id=>checkup_plan["_id"], :title=>checkup_plan[:title],:subtitle=> ("Estimated due date on #{checkup_plan[:checkup_date].to_date.to_date4}"), :added=> true, :data_source=>(checkup_plan[:data_source] || data_source)}
      end
      total_added_text = "#{data_list.count} checkups added"
      status_text = ""
      highligted_text = [{:title=>"All checkup plans have been synced with calendar"}]
     
      subtitle1 = "I have added following checkups relevant in %{child_country}"
      subtitle1TextsToBold = []
      subtitle2 = "We can modify and add to the list later"
      data_key = nil
      saved_data_summary = {:data_list=>data_list,:data_key=>data_key,:subtitle2=> subtitle2,:subtitle1=>subtitle1,:subtitle1TextsToBold=>subtitle1TextsToBold,:summary_text=>highligted_text, :total_added_text=>total_added_text,:status_text=>status_text}
     
    elsif handhold_screen == "hh_tooth chart" || handhold_screen == "tooth chart"
      data_list = []
      total_added_text = "0 Teeth added"
      status_text = "0 Erupted • 0 Shedded"
      highligted_text = [{:title=>"Easily update eruption and shedding of teeth"}]
      
      subtitle1TextsToBold = []
      data_key = nil
      if data_list.present?
        subtitle1 = "I have added following checkups relevant in %{child_country}"
        subtitle2 = "We can modify and add to the list later"
      else
        subtitle1 = ""
        subtitle2 = ""
      end
      saved_data_summary = {:data_list=>data_list,:data_key=>data_key,:subtitle2=> subtitle2,:subtitle1=>subtitle1,:subtitle1TextsToBold=>subtitle1TextsToBold,:summary_text=>highligted_text, :total_added_text=>total_added_text,:status_text=>status_text}
    elsif handhold_screen == "hh_kick counter" || handhold_screen == "kick counter"
      data_list = []
      total_added_text = "Easily track your baby's kick pattern"
      status_text = ""
      subtitle1TextsToBold = []
      data_key = nil
      saved_data_summary = {:data_list=>data_list,:data_key=>data_key,:subtitle1TextsToBold=>subtitle1TextsToBold, :total_added_text=>total_added_text,:status_text=>status_text}
    elsif handhold_screen == "hh_pregnancy health card" || handhold_screen == "pregnancy health card"
      data_list = []
      total_added_text = "View and manage snapshot of overall indicators of mothers during pregnancy"
      status_text = ""
      subtitle1TextsToBold = []
      data_key = nil
      saved_data_summary = {:data_list=>data_list,:data_key=>data_key,:subtitle1TextsToBold=>subtitle1TextsToBold, :total_added_text=>total_added_text,:status_text=>status_text}
     
     elsif handhold_screen == "hh_vision health" || handhold_screen == "vision health"
      data_list = []
      total_added_text = "0 records added"
      status_text = ""
      subtitle1TextsToBold = []
      data_key = nil
      saved_data_summary = {:data_list=>data_list,:data_key=>data_key,:subtitle1TextsToBold=>subtitle1TextsToBold, :total_added_text=>total_added_text,:status_text=>status_text}
  
    elsif handhold_screen == "hh_documents" || handhold_screen == "documents"
      data_list = []
      data[:documents] = data[:documents]
      begin
        (data[:documents]||[]).map do |document| 
          document = document.with_indifferent_access
          data_list << {:id=>(document["value"] || document[:uid]), :title=>document["name"],:subtitle=> "", :added=> true, :data_source=> (document["data_source"] || data_source)}
        end
      rescue Exception=>e 
        Rails.logger.info "Error inside Handhold save_data ....#{e.message}" 
      end
      total_added_text = "#{data_list.count} documents added"
      # highligted_text = [{:title=>""}]
      subtitle1TextsToBold = []
      
      status_text = "" # "#{data_list.count} record was updated"
      data_key = data_list.present? ?  "documents" : nil
     
      saved_data_summary = {:data_list=>data_list,:data_key=>data_key,:subtitle1TextsToBold=>subtitle1TextsToBold,:total_added_text=>total_added_text,:status_text=>status_text}
    # Measurements
    elsif handhold_screen == "hh_measurements" || handhold_screen == "measurements"
      data_list = []
      data[:measurements] = data[:measurements]
      data[:measurements].map do |measurement| 
        update_at_text = "At " + ApplicationController.helpers.calculate_age6(member.birth_date, measurement.updated_at)
        title = "Height, Weight, BMI"
        data_list << {:id=>measurement.id, :title=>title,:subtitle=>update_at_text, :added=> true, :data_source=> (measurement["data_source"] || data_source)}
      end
      total_added_text = "#{data_list.count} measurements records added"
      status_text = "" #"#{data_list.count} measurements added"
      
      subtitle1TextsToBold = []
      data_key = "measurements"
      saved_data_summary = {:data_list=>data_list,:data_key=>data_key,:subtitle1TextsToBold=>subtitle1TextsToBold,:total_added_text=>total_added_text,:status_text=>status_text}
    
    elsif handhold_screen == "hh_allergies" || handhold_screen == "allergies" || handhold_screen == "child health card" ||  handhold_screen== "hh_child health card"
      data_list = []
      data[:alleries] = data[:alleries]
      data[:alleries].map do |allergy| 
        allergy = allergy.with_indifferent_access
        data_list << {:id=>"", :title=>allergy["name"],:subtitle=> "", :added=> true, :data_source=> (allergy["data_source"] || data_source)}
      end
      total_added_text = "#{data_list.count} alleries added"
      status_text = ""
      # highligted_text = [{:title=>""}]
      
      subtitle1TextsToBold = []
      data_key = "alleries"
      
      saved_data_summary = {:data_list=>data_list,:data_key=>data_key,:subtitle1TextsToBold=>subtitle1TextsToBold, :total_added_text=>total_added_text,:status_text=>status_text}
    end
    saved_data_summary
  end

end