module ApiDashboardData

# Start of ios api for Home page 
 def set_priority(api_version=1,options={})
  case api_version
   when 3
      {"2"=>["api_childern_family"],  "3"=>["api_childern_AddSpouse","api_childern_timelines","api_childern_immunisation","api_childern_milestones","api_childern_health","api_childern_PrenatalTest","api_childern_PregTimeline","api_childern_ChildPic"] ,"5"=>["api_family_children"] }
   when 4
      {"2"=>["api_childern_family"],  "3"=>["api_childern_AddSpouse","api_childern_timelines","api_childern_immunisation","api_childern_milestones","api_childern_health","api_childern_PrenatalTest","api_childern_PregTimeline","api_childern_ChildPic"] ,"5"=>["api_family_children"] }
   when 5
      if options[:show_subscription_card].to_s == "true"
        {"2"=>["subscription_action_card", "api_childern_family"],  "3"=>["api_childern_AddSpouse","api_childern_timelines","api_childern_immunisation","api_childern_milestones","api_childern_health","api_childern_PrenatalTest","api_childern_PregTimeline","api_childern_ChildPic"] ,"5"=>["api_family_children"] }
      else
        {"2"=>["api_childern_family"],  "3"=>["api_childern_AddSpouse","api_childern_timelines","api_childern_immunisation","api_childern_milestones","api_childern_health","api_childern_PrenatalTest","api_childern_PregTimeline","api_childern_ChildPic"] ,"5"=>["api_family_children"] }
      end
    when 6
      if options[:show_subscription_card].to_s == "true"
        if self.device_platform == "ios"
          {"2"=>["subscription_action_card","nook_action_card", "api_childern_family", "app_refferal_action_card"],  "3"=>["api_childern_AddSpouse","api_childern_timelines","api_childern_immunisation","api_childern_milestones","api_childern_health","api_childern_PrenatalTest","api_childern_PregTimeline","api_childern_ChildPic"] ,"5"=>["api_family_children"] }
        else
          {"2"=>["subscription_action_card", "nook_action_card", "api_childern_family"],  "3"=>["api_childern_AddSpouse","api_childern_timelines","api_childern_immunisation","api_childern_milestones","api_childern_health","api_childern_PrenatalTest","api_childern_PregTimeline","api_childern_ChildPic"] ,"5"=>["api_family_children"] }
        end
      else
        if self.device_platform == "ios"
          {"2"=>["api_childern_family","nook_action_card", "app_refferal_action_card"],  "3"=>["api_childern_AddSpouse","api_childern_timelines","api_childern_immunisation","api_childern_milestones","api_childern_health","api_childern_PrenatalTest","api_childern_PregTimeline","api_childern_ChildPic"] ,"5"=>["api_family_children"] }
        else
          {"2"=>["api_childern_family","nook_action_card"],  "3"=>["api_childern_AddSpouse","api_childern_timelines","api_childern_immunisation","api_childern_milestones","api_childern_health","api_childern_PrenatalTest","api_childern_PregTimeline","api_childern_ChildPic"] ,"5"=>["api_family_children"] }
        end
      end
  
    else
      {"2"=>["api_childern_family"],  "3"=>["api_childern_timelines","api_childern_immunisation","api_childern_milestones","api_childern_health","api_childern_PrenatalTest","api_childern_PregTimeline","api_childern_ChildPic"] ,"5"=>["api_family_children"] }
    end
  end

  def api_childern_ChildPic(ids)
    tmp_child_ids = ids - Picture.where(upload_class:"pic").in(imageable_id:ids).pluck("imageable_id")#.map(&:to_s)
    if tmp_child_ids.blank?
      [false,[]]
    else
      [true,childerns(tmp_child_ids)]
    end
  end

  def api_childern_family(ids)
    if ids.present?
      [false,[]]
    else
      [true, user_families]
    end
  end


  def subscription_action_card(ids)
    status  = !ids.blank? && !user.has_premium_subscription_for_family?(user_families)
     
    if !status 
      [false,[]]
    else
      [true,user_families]
    end
  end

  def app_refferal_action_card(ids)
    family_created  = user_families.present?
    member_app_referral_popup = PopupLaunch.where(member_id:user.member_id).last || PopupLaunch.new
     
    popup_info = (member_app_referral_popup.popup_info  || {}).with_indifferent_access
    last_refferal_date = popup_info[:app_referral][:refferal_date] rescue nil
    if last_refferal_date.present?
      refferal_date_expired = last_refferal_date + 30.days < Date.today
    else
      refferal_date_expired = true
    end
    if family_created &&  refferal_date_expired
      [true,user_families]
    else
      [false,[]]
    end
  end

  def nook_action_card(ids)
    is_nook_enable = ::System::Settings.isNookEnabled?(self)
    if is_nook_enable
      [true,user_families]
    else
      [false,[]]
    end
  end
  
  def api_childern_document(ids)
    tmp_child_ids = ids - Document.where({'member_id' => { "$in" => ids} }).group_by(&:member_id).keys
    if tmp_child_ids.blank?
      [false,[]]
    else
      temp_childern = childerns(ids)
      [true, temp_childern.entries.select{|x| tmp_child_ids.include?(x.id.to_s)}]
    end
  end

  def api_childern_PregTimeline(ids)
    # tmp_child_ids = ids - Timeline.where({'member_id' => { "$in" => ids} }).group_by(&:member_id).keys
    ids = Pregnancy.in(expected_member_id:ids).where(status:"expected").pluck("expected_member_id").map(&:to_s)
    tmp_child_ids = ids - Timeline.where({'member_id' => { "$in" => ids} }).where(:updated_at.gt => (Time.now - 1.month)).group_by(&:member_id).keys.map(&:to_s)
    if tmp_child_ids.blank?
      [false,[]]
    else
      temp_childern = childerns(ids)
      [true, temp_childern.entries.select{|x| tmp_child_ids.include?(x.id.to_s)}]
    end
  end

  def api_childern_PrenatalTest(ids)
    ids = Pregnancy.in(expected_member_id:ids).where(status:"expected").pluck("expected_member_id").map(&:to_s)
    tmp_child_ids = ids - MedicalEvent.where({'member_id' => { "$in" => ids} }).group_by(&:member_id).keys.map(&:to_s)
    if tmp_child_ids.blank?
      [false,[]]
    else
      temp_childern = childerns(ids)
      [true, temp_childern.entries.select{|x| tmp_child_ids.include?(x.id.to_s)}]
    end
  end

  def api_childern_OverduePrenatalTest(ids)
    # tmp_child_ids = ids - MedicalEvent.where({'member_id' => { "$in" => ids} }).where("'est_due_time' > " + "'#{Time.now}'").group_by(&:member_id).keys
    ids = Pregnancy.in(expected_member_id:ids).where(status:"expected").pluck("expected_member_id").map(&:to_s)
    
    tmp_child_ids = ids - MedicalEvent.in(:member_id => ids).where(:est_due_time.gt => Time.now).pluck(:member_id).map(&:to_s).uniq
    if tmp_child_ids.blank?
      [false,[]]
    else
      temp_childern = childerns(ids)
      [true, temp_childern.entries.select{|x| tmp_child_ids.include?(x.id.to_s)}]
    end
  end
  
  def api_family_children(ids)
     p_family_childern = Family.in(id:famlies_without_child).entries
     if p_family_childern.present?
       return [true,p_family_childern]
     else
      return [false, []]
     end
  end
  
  def tool_child_list(all_ids,tool_child_ids)
    temp_childern = childerns(all_ids)
    [true, temp_childern.entries.select{|x| tool_child_ids.include?(x.id)}]
  end

  def api_childern_immunisation(ids)
    vacciantions = Vaccination.where({'member_id' => { "$in" => ids} }).where(opted:"true")
    # vaccination_ids = vacciantions.map(&:id)
    # jabs = Jab.in(:vaccination_id=>vaccination_ids).where(:status=>"Planned").or(:due_on.lte => Time.now).or(:due_on => nil,:est_due_time.lte=>Time.now)
    member_without_vaccination = ids - vacciantions.pluck(:member_id).uniq
    # member_without_vaccination = ids - vacciantions.group_by(&:member_id).keys
    tmp_child_ids =   member_without_vaccination #+ Vaccination.in(id:jabs.map(&:vaccination_id).uniq).group_by(&:member_id).keys
    if tmp_child_ids.blank?
      [false,[]]
    else
      tool_child_list(ids,tmp_child_ids)
      # temp_childern = childerns(ids)
      # [true, temp_childern.entries.select{|x| tmp_child_ids.include?(x.id)}]
    end
  end
  
  def api_childern_milestones(ids)
     tmp_child_ids = (ids) - (Milestone.where({'member_id' => { "$in" => ids} }).where(:created_at.gt => (Time.now - 1.month)).pluck(:member_id).uniq)
     # tmp_child_ids = (ids) - (Milestone.where({'member_id' => { "$in" => ids} }).where("'created_at' > " + "'#{(Time.now - 1.month)}'").group_by(&:member_id).keys)
     if tmp_child_ids.blank?
      return [false,[]]
    else
      tool_child_list(ids,tmp_child_ids)
      # temp_childern = childerns(ids)
      # [true, temp_childern.entries.select{|x| tmp_child_ids.include?(x.id)}]
   end
  end
  
  def api_childern_health(ids)
     child_ids = ids - Health.where({'member_id' => { "$in" => ids} }).where(:updated_at.gt => (Time.now - 1.month)).group_by(&:member_id).keys
    # @childerns_health = childern.entries.select{|x| child_ids.include?(x.id)}
    if child_ids.blank?
      [false,[]]
    else
      tool_child_list(ids,child_ids)
      # [true,childerns(ids).entries.select{|x| child_ids.include?(x.id)}]
    end
  end

  def api_childern_timelines(ids)
    # child_ids = ids - Timeline.where({'member_id' => { "$in" => ids} }).where("'updated_at' > " + "'#{(Time.now - 1.month)}'").group_by(&:member_id).keys
    child_ids = ids - Timeline.where({'member_id' => { "$in" => ids} }).where(:updated_at.gt =>(Time.now - 1.month)).pluck(:member_id).uniq
    if child_ids.blank?
      return [false, []]
    else
      tool_child_list(ids,child_ids)
      # temp_childern = childerns(ids)
      # @childern_timelines = temp_childern.entries.select{|x| child_ids.include?(x.id)}
      # return [true, @childern_timelines]
    end

  end

  def api_childern_nutrients(ids)
    if @api_version < 3
      exclude_tool = Nutrient.where(title:"Health Card").last.id.to_s
      child_ids = (ids)  - MemberNutrient.where({'member_id' => { "$in" => ids } }).not_in(nutrient_id:[exclude_tool]).where(status: "active").pluck(:member_id).uniq
    else
      child_ids = (ids)  - MemberNutrient.where({'member_id' => { "$in" => ids } }).where(status: "active").pluck(:member_id).uniq
    end
    if child_ids.blank?
      [false,[]]
    else
      tool_child_list(ids,child_ids)
      # temp_childern = childerns(ids)
      # [true,temp_childern.entries.select{|x| child_ids.include?(x.id)}]
    end
  end

    def api_childern_AddSpouse(ids)
       current_user =  self
      all_families_list = current_user.user_families.map{|a| a.id.to_s}
      invitation_for_father_from_family = Invitation.in(family_id:all_families_list).in(role:["Father","father"]).where(status: "invited").pluck(:family_id).map(&:to_s).uniq
      invitation_for_mother_from_family = Invitation.in(family_id:all_families_list).in(role:["Mother","mother"]).where(status: "invited").pluck(:family_id).map(&:to_s).uniq
      
      families_list = FamilyMember.in(family_id:all_families_list,role:["Father","Mother"]).pluck(:family_id).map(&:to_s)
      families_list = families_list + invitation_for_mother_from_family + invitation_for_mother_from_family

      family_with_father = FamilyMember.in(family_id:families_list).where(role:"Father").pluck(:family_id).map(&:to_s).uniq
      
      family_with_mother = FamilyMember.in(family_id:families_list).where(role:"Mother").pluck(:family_id).map(&:to_s).uniq
      
      family_without_mother = families_list - ( family_with_mother + invitation_for_mother_from_family)
      family_without_father = families_list - ( family_with_father + invitation_for_father_from_family)
      family_without_spouse = []#FamilyMember.in(family_id:families_list).not_in(role:[FamilyMember::ROLES[:father],FamilyMember::ROLES[:father]]).pluck(:family_id).map(&:to_s).uniq
      family_ids = (family_without_mother + family_without_father + family_without_spouse).uniq
      families = Family.in(id:family_ids)
      if families.blank?
        [false,[]]
      else
        [true,families]
     end
  end

  def res(name,value,msg_flag=false,dashboard_version=nil,api_version=1)
    result = []
   
    all_child_ids = value.map{|v| v.id.to_s}
    nutrient = Nutrient.where(title:Nutrient::CardNutirent[name]).first
    if nutrient.present?
    # only activated tools 
      child_ids = MemberNutrient.in(member_id:all_child_ids).where(status:"active",nutrient_id:nutrient.id).map{|a| a.member_id.to_s}
    else
      child_ids = all_child_ids
    end
    nutrient_name = name.gsub("api_childern_","")
    dismiss_nutrient_ids =  DismissNutrient.where(nutrient_name: nutrient_name , parent_id: self.id).where({'child_id' => { "$in" => child_ids } }).or({:dismiss_up_to.gte=> Date.today},{:skipped_forever=>true}).pluck("child_id")
    pronoun1 = {"Son"=> "boy", "Daughter" => "girl"}
    pronoun2 = {"Son"=> "his", "Daughter" => "her"}
    not_preg_tool_card = ["api_childern_timelines", "api_childern_nutrients","api_childern_immunisation","api_childern_milestones","api_childern_health","api_childern_ChildPic"].include?(name)
    if name=="api_family_children" || name == "api_childern_family"
      nutrient_members =  famlies_without_child
      dismissable = (self.user_families.count > 1)
    elsif name == "api_childern_AddSpouse"
      nutrient_members =  child_ids -  dismiss_nutrient_ids #families
    elsif name == "subscription_action_card"
      card_id  =  "subscriptionActionCard"
      if DismissNutrient.where(parent_id:id.to_s,nutrient_name: card_id).or({:dismiss_up_to.gte=> Date.today},{:skipped_forever=>true}).present?
        nutrient_members = []
      else
        nutrient_members = [self.user_families.first] # user first family
      end
    elsif name == "app_refferal_action_card"
      card_id  =  "appRefferalActionCard"
      if DismissNutrient.where(parent_id:id.to_s,nutrient_name: card_id).or({:dismiss_up_to.gte=> Date.today},{:skipped_forever=>true}).present?
        nutrient_members = []
      else
        nutrient_members = [self.user_families.first] # user first family
      end
    elsif name == "nook_action_card"
      card_id  =  "nookActionCard"
      if DismissNutrient.where(parent_id:id.to_s,nutrient_name: card_id).or({:dismiss_up_to.gte=> Date.today},{:skipped_forever=>true}).present?
        nutrient_members = []
      else
        nutrient_members = [self.user_families.first] # user first family
      end
    else
      if not_preg_tool_card
        pregnancy_childern = Pregnancy.in(expected_member_id:child_ids).pluck(:expected_member_id).map{|a| a.to_s }
        nutrient_members = ChildMember.in(:id => (child_ids - (dismiss_nutrient_ids + pregnancy_childern) ) ) 
      else
        pregnancy_childern = Pregnancy.in(expected_member_id:child_ids).pluck(:expected_member_id).map{|a| a.to_s }
        nutrient_members = ChildMember.in(:id => (pregnancy_childern - dismiss_nutrient_ids ) ) 
      end
    end
    nutrient_members.each do |child_member1|
        child_first_name = child_member1.first_name.strip rescue nil
        action_card_title = {
          :api_childern_AddSpouse=>"Invite the %{role} to join your family",
          :api_family_children=>"Let's add a little one or pregnancy",
          :api_childern_family=>"Let's add a little one or pregnancy",
          :api_childern_PregTimeline=>"Any new Timeline memory for #{child_first_name}?",
          :api_childern_PrenatalTest=>"Update prenatal tests for #{child_first_name}",
          :api_childern_OverduePrenatalTest=>"Update prenatal tests for #{child_first_name}",
          :api_childern_timelines=>"Any new Timeline memory for #{child_first_name}?",
          :api_childern_nutrients=>"Tool helps you in your parental journey",
          :api_childern_immunisation=>"Time to review #{child_first_name}'s immunisations",
          :api_childern_milestones=>"Any recent Milestones for #{child_first_name}?",
          :api_childern_health=>"Time to update Measurements for #{child_first_name}",
          :api_childern_ChildPic=>"#{child_first_name.titleize rescue nil} needs a profile pic"
        }

      data = {}
      role = child_member1.family_members.first.role rescue nil
      role_pronoun = pronoun2[role] rescue ""
      if dashboard_version.to_i < 1
        data[:role] = pronoun1[role] rescue ""
        data[:pic_url] =  child_member1.profile_image.complete_image_url(:small,api_version) rescue ""
      end
    case name
    when "api_childern_AddSpouse"
      family_id = child_member1
      family = Family.find(family_id)
      data[:member_id] = self.id
      data[:family_id] = family_id
      data[:card_id] = family_id.to_s + "_" + "AddSpouse"
      data[:dismissable] = true
      data[:notific_msg] = "" if msg_flag
      data[:type] = "AddSpouse"
      data[:image] = "AddSpouse"
      data[:tool_identifier] = "AddSpouse"

        familymember = FamilyMember.where(family_id:family_id).pluck(:role)
        familymember = familymember +  Invitation.where(family_id:family_id,status: "invited").pluck(:role)

        if familymember.include?("Mother")
          role = "father"
          pronoun = "him"
        elsif familymember.include?("Father")
          role = "mother"
          pronoun = "her"
        else
          role = "mother"
          pronoun = "him"
        end
        family_name = family.name
        data[:role] = role rescue ""
        data[:title] = action_card_title[:api_childern_AddSpouse] %{:role=>role} 
         # = spous_title
      if (dashboard_version.nil? || dashboard_version.to_i < 1 )
        profile_image = (self.profile_image.photo_url_str("small",api_version) rescue "")
        data[:header] = family_name rescue ""
        data[:pic_url] = profile_image
        data[:line1] = profile_image
        data[:line2] = "We haven't added the #{role} to #{family_name}" 
        data[:line3] = "and keep everyone on the same page" # not used by IOS (Discussed with Dharmendra)
        data[:line4] = "Let's send an email invite to #{pronoun}"  
        data[:button] = "Add spouse"
        data[:image] = "add-spouce-card-image"
      end
      
    when "api_family_children","api_childern_family"
      record = child_member1 # child_member is family record here
      family = Family.find(record)
      if (dashboard_version.nil? || dashboard_version.to_i < 1 )
        data = { :family_id =>record,
                :identifier=>(name == "api_family_children" ? record : rand(999999999999999)),
                :header=> (family.name rescue ""),
                :line1=>"",
                :dismissable => dismissable,
                :line2=>  "Who is the little one we need to take care of in " + (family.name rescue ""),
                :image=>"manage-family-card-add-child-image",
                :type=>"Family",
                :line3=>"",
                :line4=>"See your child's potential bloom with Nurturey",
                :button=>"Add Child",
                :card_id=> record.to_s + "_family" 
              }
      else
        data = { :family_id =>record,
                :identifier=>(name == "api_family_children" ? record : rand(999999999999999)),
                :dismissable => dismissable,
                :image=>"Child",
                :type=>"Family",
                :tool_identifier=>"Family",
                :member_id=>"",
                :title=>action_card_title[:api_family_children],
                :card_id=> record.to_s + "_family" 
              }
      end
              data.merge!({:notific_msg=> "Add the little one we need to take care of in #{family.name}"}) if msg_flag
    when "api_childern_PregTimeline"
      data[:member_id] = child_member1.id
      data[:card_id] = child_member1.id.to_s + "_" + nutrient_name
      data[:dismissable] = true
      data[:notific_msg] = "Add an event to your pregnancy's timeline and keep building sweet memories" if msg_flag
      data[:title] = action_card_title[:api_childern_PregTimeline]
      data[:image] = "Timeline"
      data[:tool_identifier] = "Timeline"
      data[:type] = "Timeline"
      if (dashboard_version.nil? || dashboard_version.to_i < 1 )
        data[:role] = nil rescue ""
        data[:header] =   "#{child_member1.first_name} is #{ApplicationController.helpers.calculate_age(child_member1.birth_date|| Date.today)} old"
        data[:image] = "pregnancy-user-icon"
        data[:line1] = (child_member1.profile_image.photo_url_str("small",api_version) rescue "")
        data[:line2] = "It's been a while since we added an event to your pregnancy's Timeline" #{}"Add a timeline entry"
        data[:line3] = ""
        data[:line4] = "Let's keep building the memories"
        data[:button] = "Add Timeline event"
        data[:type] = "timeline"
      end
    when "api_childern_PrenatalTest"
      data[:title] = action_card_title[:api_childern_PrenatalTest]
      data[:type] = "Prenatal Tests"
      data[:image] = "Prenatal Tests"
      data[:tool_identifier] = "Prenatal Tests"
      data[:member_id] = child_member1.id
      data[:card_id] = child_member1.id.to_s + "_" + nutrient_name
      data[:dismissable] = true
      data[:notific_msg] = "Update the prenatal test details now and keep it up-to-date" if msg_flag
      if (dashboard_version.nil? || dashboard_version.to_i < 1 )
        data[:header] =   "#{child_member1.first_name} is #{ApplicationController.helpers.calculate_age(child_member1.birth_date|| Date.today)} old"
        data[:role] = nil rescue ""
        data[:image] = "pregnancy-user-icon"
        data[:line1] = (child_member1.profile_image.photo_url_str("small",api_version) rescue "")
        data[:line2] = "Let me update the prenatal test details for #{child_member1.first_name} "
        data[:line3] = ""
        data[:line4] = "And keep the records up-to-date"
        data[:button] = "Update Prenatal tests"
        data[:type] = "prenatal test"
      end
    when "api_childern_OverduePrenatalTest"
      data[:member_id] = child_member1.id
      data[:card_id] = child_member1.id.to_s + "_" + nutrient_name
      data[:dismissable] = true
      data[:notific_msg] = "Update the prenatal test details now and keep it up-to-date" if msg_flag
      data[:title] = action_card_title[:api_childern_OverduePrenatalTest]
      data[:type] = "Prenatal Tests"
      data[:image] = "Prenatal Tests"
      data[:tool_identifier] = "Prenatal Tests"
      if (dashboard_version.nil? || dashboard_version.to_i < 1 )
        data[:header] =   "#{child_member1.first_name} is #{ApplicationController.helpers.calculate_age(child_member1.birth_date|| Date.today)} old"
        data[:image] = "pregnancy-user-icon"
        data[:line1] = (child_member1.profile_image.photo_url_str("small",api_version) rescue "")
        data[:line2] = "Lets update the prenatal test details for #{child_member1.first_name}"
        data[:line3] = ""
        data[:line4] = "and keep the records up-to-date"
        data[:button] = "Update Prenatal tests"
        data[:type] = "prenatal test"
        data[:role] = nil rescue ""
      end
    when "subscription_action_card"
      data[:title] = 'Go Premium and help us build the smartest assistant'
      data[:member_id] = self.id
      data[:card_id] = self.id.to_s + "_" + "subscriptionActionCard"
      data[:dismissable] = true
      data[:type] = "premium_subscription"
      data[:image] = "premium_subscription"
      data[:tool_identifier] = "premium_subscription"
      data[:notific_msg] = "" if msg_flag
    when "app_refferal_action_card"
      data[:title] = 'Introduce Nurturey with your friends'
      data[:member_id] = self.id
      data[:card_id] = self.id.to_s + "_" + "appRefferalActionCard"
      data[:dismissable] = true
      data[:type] = "app_referral"
      data[:image] = "app_referral"
      data[:tool_identifier] = "app_referral"
      data[:notific_msg] = "" if msg_flag
    when "nook_action_card"
      data[:title] = 'Check out relevant products on Nook'
      data[:member_id] = self.id
      data[:card_id] = self.id.to_s + "_" + "nookActionCard"
      data[:dismissable] = true
      data[:type] = "nook"
      data[:image] = "nook"
      data[:tool_identifier] = "nook"
      data[:notific_msg] = "" if msg_flag
    when "api_childern_timelines" 
      data[:title] = action_card_title[:api_childern_timelines]
      data[:member_id] = child_member1.id
      data[:card_id] = child_member1.id.to_s + "_" + nutrient_name
      data[:dismissable] = true
      data[:title] = action_card_title[:api_childern_timelines]
      data[:type] = "Timeline"
      data[:image] = "Timeline"
      data[:tool_identifier] = "Timeline"
      data[:notific_msg] = "Add an event to #{child_member1.first_name.strip}'s timeline and keep building sweet memories of #{role_pronoun} childhood" if msg_flag
      if (dashboard_version.nil? || dashboard_version.to_i < 1 )
        data[:header] =   "#{child_member1.first_name} is #{ApplicationController.helpers.calculate_age(child_member1.birth_date|| Date.today)} old"
        data[:image] = "timeline-card-entry-image"
        data[:line1] = (child_member1.profile_image.photo_url_str("small",api_version) rescue "")
        data[:line2] = "It's been a while since we added an event to #{child_member1.first_name}'s Timeline" #{}"Add a timeline entry"
        data[:line3] = ""
        data[:line4] = "Let's keep building the memories"
        data[:button] = "Add Timeline event"
        data[:type] = "timeline"
      end
    when "api_childern_nutrients" 
      data[:title] = action_card_title[:api_childern_nutrients]
      data[:card_id] = child_member1.id.to_s + "_" + nutrient_name
      data[:dismissable] = true
      data[:notific_msg] = "Add an event to #{child_member1.first_name.strip}'s timeline and keep building sweet memories of #{role_pronoun} childhood" if msg_flag
      data[:type] = "Nutrient"
      data[:tool_identifier] = "Nutrient"
      data[:image] = "nutrient"
      if (dashboard_version.nil? || dashboard_version.to_i < 1 )
        data[:header] =   "#{child_member1.first_name} is #{ApplicationController.helpers.calculate_age(child_member1.birth_date|| Date.today)} old"
        data[:image] = "nutrient-card-entry-image"
        data[:line1] = (child_member1.profile_image.photo_url_str("small",api_version) rescue "")
        data[:line3] = "Tool helps you in your parental journey"
        data[:line4] = "and tracking #{child_member1.first_name}'s development"
        data[:line2] = "Activate tool"
        data[:member_id] = child_member1.id
        data[:button] = ""
        data[:type] = "nutrient"
      end
    when "api_childern_immunisation"
      data[:card_id] = child_member1.id.to_s + "_" + nutrient_name
      data[:dismissable] = true
      data[:notific_msg] = "Update the #{child_member1.first_name.strip}'s immunisations record now to help us easily manage it for you" if msg_flag
      data[:title] = action_card_title[:api_childern_immunisation]
      data[:type] = "Immunisations"
      data[:image] = "Immunisations"
      data[:tool_identifier] = "Immunisations"
      data[:member_id] = child_member1.id
      if (dashboard_version.nil? || dashboard_version.to_i < 1 )
        data[:header] =   "#{child_member1.first_name} is #{ApplicationController.helpers.calculate_age(child_member1.birth_date|| Date.today)} old"
        data[:image] = "immunisation-card-syringe-image"
        country_code = self.user.country_code rescue "uk"
        vaccin_count = calculate_vaccin(child_member1,ApplicationController.helpers.calculate_age(child_member1.birth_date|| Date.today))
        data[:line1] = (child_member1.profile_image.photo_url_str("small",api_version) rescue "")
        data[:line2] =  "We haven't kept a record of #{child_member1.first_name}'s immunisations"
        data[:line3] = "#{vaccin_count} immunisations"
        data[:line4] =  "Let's keep immunisation details up to date"
        data[:type] = "immunisation"
        data[:button] = "Update Immunisations"
      end
    when "api_childern_milestones"
      data[:member_id] = child_member1.id
      data[:card_id] = child_member1.id.to_s + "_" + nutrient_name
      data[:dismissable] = true
      data[:notific_msg] = "Update #{child_member1.first_name.strip}'s milestones record now and help us easily track #{role_pronoun} social, cognitive and emotional development" if msg_flag
      data[:title] = action_card_title[:api_childern_milestones]
      data[:type] =  "Milestones"
      data[:image] =  "Milestones"
      data[:tool_identifier] =  "Milestones"
      if (dashboard_version.nil? || dashboard_version.to_i < 1 )
        age = ApplicationController.helpers.calculate_age(child_member1.birth_date|| Date.today)
        data[:header] =   "#{child_member1.first_name} is #{age} old"
        data[:image] = "milestone-card-graph-image"
        data[:line1] = (child_member1.profile_image.photo_url_str("small",api_version) rescue "")
        data[:line2] = "It seems #{child_member1.first_name} would have achieved some milestones that we haven't marked"
        data[:line3] = ""
        data[:line4] = "Let's track social, cognitive and emotional development"
        data[:type] =  "Milestone"
        data[:button] = "Update Milestones"
      end
    when "api_childern_health"
      data[:member_id] = child_member1.id
      data[:card_id] = child_member1.id.to_s + "_" + nutrient_name
      data[:dismissable] = true
      data[:notific_msg] = "Update #{child_member1.first_name.strip}'s measurement record now and compare growth with WHO charts" if msg_flag
      data[:title] = action_card_title[:api_childern_health]
      data[:type] =  "Measurements"
      data[:image] =  "Measurements"
      data[:tool_identifier] =  "Measurements"

      if (dashboard_version.nil? || dashboard_version.to_i < 1 )
        data[:header] =   "#{child_member1.first_name} is #{ApplicationController.helpers.calculate_age(child_member1.birth_date|| Date.today)} old"
        data[:image] = "measurement-card-growth-chart-image.png"
        data[:line1] = (child_member1.profile_image.photo_url_str("small",api_version) rescue "")
        data[:line2] = "#{child_member1.first_name.strip} is growing quickly. Let me record the current measurements"
        data[:line3] = ""
        data[:line4] = "And compare growth with WHO charts"
        data[:button]= "Update Measurements"
        data[:type] = "measurement"
      end
    when "api_childern_document"
      data[:type] = "Documents"
      data[:title] = action_card_title[:api_childern_document]
      data[:image] = "Documents"
      data[:tool_identifier] = "Documents"
      data[:notific_msg] = " " if msg_flag
      data[:member_id] = child_member1.id
      data[:dismissable] = true
      data[:card_id] = child_member1.id.to_s + "_" + nutrient_name
      if (dashboard_version.nil? || dashboard_version.to_i < 1 )
        data[:header] =   "#{child_member1.first_name.strip} is #{ApplicationController.helpers.calculate_age(child_member1.birth_date|| Date.today)} old"
        data[:image] = "measurement-card-entry-image"
        data[:line1] = ""
        data[:type] = "document"
        data[:line2] = "Add document"
      end
    when "api_childern_ChildPic"
      data[:member_id] = child_member1.id
      data[:card_id] = child_member1.id.to_s + "_" + nutrient_name
      data[:dismissable] = true
      data[:notific_msg] = "Add #{child_member1.first_name.strip}'s profile picture and personalise #{role_pronoun} profile" if msg_flag
      data[:title] = action_card_title[:api_childern_ChildPic]
      data[:tool_identifier] = "picture"
      data[:image] = "picture"
      data[:type] = "picture"
      if (dashboard_version.nil? || dashboard_version.to_i < 1 )
        data[:header] =   "#{child_member1.first_name} is #{ApplicationController.helpers.calculate_age(child_member1.birth_date|| Date.today)} old"
        data[:image] = data[:role] + "-image" rescue nil
        data[:line1] = "" #"Add #{child_member1.first_name}'s' Profile pic"
        data[:line2] = "#{child_member1.first_name.strip} doesn't have a profile pic"
        data[:line3] = ""
        data[:line4] ="Let's personalise by uploading a picture"
        data[:button]= "Add Profile Image"
        data[:type] = "picture"
      end
     
      
    else
      data[:member_id] = child_member1.id
    end
    
    result << data
     end
    result
  end

  def calculate_vaccin(child_member, child_member_age)
      total_jabs = 0
    if child_member.vaccinations.count == 0 
      age_in_days = SystemJab.convert_to_days(child_member_age)
      country_code = Vaccination::Country[(self.user.country_code || "uk").downcase]||"UK"
      if  child_member.family_members.first.role == FamilyMember::ROLES[:daughter]
        sys_vaccin_ids = SystemVaccin.where(vaccin_type: "Recommended").where(country:country_code).pluck(:id)
      else
        sys_vaccin_ids = SystemVaccin.where(vaccin_type: "Recommended").where(country:country_code).not_in(:only_for=>[FamilyMember::ROLES[:daughter]]).pluck(:id)
      end
      sys_jabs = SystemJab.in(system_vaccin_id:sys_vaccin_ids).where(:due_on_in_day_int.lte  => age_in_days)#.where(:due_on_in_day.gt=>0)
      total_jabs = sys_jabs.count
    else
      vacciantions = Vaccination.where(:member_id => child_member.id).where(opted:"true")
      vaccination_ids = vacciantions.pluck(:id)
      jabs = Jab.in(:vaccination_id=>vaccination_ids).where(:est_due_time.lte=>Time.now)
      total_jabs = jabs.count
    end
    total_jabs
  end
  
   

  def api_dashboard(notification_msg=false,api_version=1,limit=nil,dashboard_version=nil,options={})
    @api_version = api_version
    current_user = self
    user = current_user.user
    @families_list = current_user.user_families
    @child_ids = current_user.children_ids(@families_list)
    @childern_list = current_user.childrens(@child_ids)
    
    if @families_list.blank?
      if dashboard_version.nil? || dashboard_version.to_i < 1
        hash_data = {:identifier=>rand(999999999999999), :dismissable => false, :header=>"",:line1=>"",:line2=>"I need to create a family to facilitate coordination among all member",:image=>"manage-family-card-add-familly-image",:type=>"Family",:line3=>"", :line4=>"Don't wait, immerse in your parental journey",:button=>"Create Family"}
      else
        hash_data = {:identifier=>rand(999999999999999), :dismissable => false, :image=>"Family",:type=>"Family",:title=>"Let's create your family first",:tool_identifier=>"Family"}

      end    
      hash_data.merge!({notific_msg:"Add family to easily coordinate among all the members"}) if notification_msg
      return  data = [hash_data]
    
    end
    action_card_data = {}
    card_priority = nil
    out = true
    @data = []
    current_user.set_priority(api_version,options).each do |k,action_cards|
      if out
        action_cards.each do |action_card|
          status, data = eval(action_card+ " @child_ids")
          if status 
            out = false
            out = true if  action_card == "subscription_action_card" 
            action_card_data[action_card] =  data

            card_priority = k
          end
        end
      end
    end
    limit_count = 0
    action_card_data.each do |action_card_name,card_data|
      @data << res(action_card_name,card_data,notification_msg,dashboard_version,api_version)
      if limit.present? 
        limit_count = limit_count + 1
        break  if limit_count >= limit.to_i
      end
    end
    @data.flatten
  end

  def api_recent_timeline(member_ids)
    recent_entries =[]
    data = {}
    recent_list = Timeline.where({'member_id' => { "$in" => member_ids} }).where(entry_from:"timeline").order_by("created_at DESC").limit(10).only(:member_id,:created_at,:name).group_by{|p| p.member_id }.entries rescue []
    recent_list.each do |k,v|
      v.each do |timeline|
        data[:line1] = "Timeline"
        data[:line2] = timeline.name
        # data[:line3] = timeline.created_at.to_api_date1
        data[:line3] = timeline.created_at.to_date
        data[:line4] = ""# timeline.member.profile_image.photo_url_str("small") rescue ""
        data[:member_id] = timeline[:member_id]
        data[:title] = timeline.name
        recent_entries << data
        data ={}
      end
    end
      recent_entries
  end

  def api_recent_milestones(member_ids)
    recent_entries =[]
    data ={}
    recent_list = Milestone.where({'member_id' => { "$in" => member_ids} }).includes(:system_milestone).order_by("created_at DESC").limit(10).only(:title,:member_id,:created_at).group_by{|p| p.member_id }.entries rescue []
    # recent_list = Milestone.order_by("created_at DESC").group_by{|p| p.member_id }.entries rescue []
    recent_list.each do |k,v|
      system_milestones = {}
      system_milestones_ids = v.map(&:system_milestone_id)
      SystemMilestone.in(:id=>system_milestones_ids).only(:id,:title).each{|a| system_milestones[a.id] = a}

      v.each do |milestone|
        system_milestone = system_milestones[milestone.system_milestone_id]
        data[:line1] = "Milestones"
        data[:line2] = system_milestone.title rescue "" #v[0].description.blank? ?  (system_milestone).description : v[0].description
        data[:line3] = milestone.created_at.to_api_date1
        data[:line4] = "" #milestone.member.profile_image.photo_url_str("small") rescue ""
        data[:member_id] = milestone[:member_id]
        data[:title] = system_milestone.title rescue ""
        recent_entries << data
        data = {}
      end
    end
      recent_entries
  end


  def api_recent_health(member_ids=nil)
    recent_entries =[]
    data ={}
    convert_unit_val_list = self.metric_system
    units = Health.health_unit(convert_unit_val_list)
    units["bmi"] = ""
    recent_list = Health.where({'member_id' => { "$in" => member_ids} }).order_by("created_at DESC").limit(10)#.group_by{|p| p.member_id }.entries rescue []
    # recent_list.each do |k,v|
      recent_list.each do |health|
        health_attributes = health.attributes
        health_attributes["_id"] = nil #delete("_id")
        temp = Health.new(health_attributes)
        health.api_converted_data(convert_unit_val_list)
        data[:line1] = "Measurements"
        # change_parameter = Health::HEALTH_ENTRY_PARAM.keys.select{|health_key| health[health_key].present?}
        data[:line3] = health.created_at.to_api_date1
        data[:line4] = ""#health.member.profile_image.photo_url_str("small") rescue ""
        data[:member_id] = health["member_id"]
        data[:title] = ""
        str = []
        # health_attributes["_id"] = nil
        Health::HEALTH_ENTRY_PARAM.keys.each do |k1|
          str << Health::API_HEALTH_ENTRY_TITLE[k1] + ": " + temp[k1].to_s + " " +  units[k1] if temp[k1].present?
        end 

        data[:line2] = str.to_sentence.gsub(", and", " and").strip
        recent_entries << data
        data = {}
      end
    # end
      recent_entries
  end

  def api_recent_picture(member_ids)
    recent_entries =[]

    recent_list = Picture.where({'member_id' => { "$in" => member_ids} }).includes(:member).order_by("created_at DESC").limit(10).group_by{|p| p.member_id }.entries rescue []
    recent_list.each do |k,v|
      v[0][:entry_from] = "Pictures"
      recent_entries << v[0]
    end
      recent_entries
  end

  def api_recent_immunisation1(member_ids)
    recent_entries =[]
    data ={}
    count = 0
    recent_list = Vaccination.where({'member_id' => { "$in" => member_ids} }).where(opted: "true").order_by("created_at DESC").includes(:jabs)#.limit(10) #flatten.group_by{|p| p.member_id }.entries rescue []
     
    recent_list.each do | v|
    if count < 10
        all_vaccin_jabs= v.jabs.pluck(:id)
        vaccine_name = v.get_name
        v.jabs.or(:due_on.ne => nil).or(:est_due_time => nil).order_by("id DESC").only(:id,:due_on,:status).each do |jab|
        data[:line1] = "Immunisations"
        data[:line2] = "Jab"+ (all_vaccin_jabs.index(jab.id)+1).to_s + ": " + vaccine_name + " - " + (jab.status == "Planned" ? "due on" : "administered on") + " " + jab.due_on.to_date1 rescue "Jab1: #{vaccine_name}"
        data[:line3] =  (v.updated_at.to_api_date1)
        data[:line4] =  ""# v.member.profile_image.photo_url_str("small") rescue ""
        data[:member_id] = v[:member_id]
        data[:title] = ""
        recent_entries << data
        data = {}
        count = count+1

      end
    end

    end

      recent_entries[0..9]
  end
# refactored for IOS api response 
  def api_recent_immunisation(member_ids)
    recent_entries =[]
    data ={}
    recent_vaccin_ids = Vaccination.where({'member_id' => { "$in" => member_ids} }).where(opted: "true").order_by("created_at DESC").pluck(:id)#.limit(10) #flatten.group_by{|p| p.member_id }.entries rescue []
    vaccin_jabs = Jab.in(vaccination_id:recent_vaccin_ids).or(:due_on.ne => nil).or(:est_due_time => nil).order_by("id DESC").limit(10).only(:id,:due_on,:status,:vaccination_id)
    vaccin_jabs.each do |jab|
      v = jab.vaccination
      vaccine_name = v.get_name
      all_vaccin_jabs = v.jab_ids
      data[:line1] = "Immunisations"
      data[:line2] = "Jab"+ (all_vaccin_jabs.index(jab.id)+1).to_s + ": " + vaccine_name + " - " + (jab.status == "Planned" ? "due on" : "administered on") + " " + jab.due_on.to_date1 rescue "Jab1: #{vaccine_name}"
      data[:line3] =  (v.updated_at.to_api_date1)
      data[:line4] =  ""# v.member.profile_image.photo_url_str("small") rescue ""
      data[:member_id] = v[:member_id]
      data[:title] = ""
      recent_entries << data
      data = {}
    end
    recent_entries
  end

# End of Home api



# for API
  def sent_invitations_data
    invitation = invitations.where(:sender_type => 'Member', :status => 'invited')
    data = []
    detail = {}
    invitation.each do |invit|
     invit_family = invit.family rescue Family.new
     detail["invitation_id"] = invit.id
     detail["role"] = invit.role
     detail["msg_to_sender"] = invit.message
     detail["family_name"] = invit_family.name rescue ""
     detail["family_uid"] = invit_family.family_uid rescue ""
     detail["family_id"] = invit.family_id
     sender = invit.sender_id.present? ? invit.sender : invit.member
     detail["sender_member_id"] = sender.id rescue ""
     detail["sender_email"] = sender.email rescue ""
     # detail["status"] = "Request sent to join"
     detail["status"] = "Join request sent"     
     detail["action"] ="You will be notified when your request is accepted"
     detail["sender_first_name"] = sender.first_name rescue ""
     detail["receiver_first_name"] = invit.member.first_name rescue ""
     detail["receiver_profile_image"] = (invit.member.profile_image.photo_url_str("small",api_version) rescue "")
     data << detail
     detail ={}
    end
    data
  end  

  def received_invitations_data(invitation_data=nil)
    data = []
    detail = {}
    (invitation_data || received_join_family_invitations).each do |invit|
      invit_family = invit.family rescue Family.new
      detail["invitation_id"] = invit.id
      detail["role"] = invit.role
      # detail["msg_to_sender"] = invit.message
      detail["family_name"] = invit_family.name rescue ""
      detail["family_uid"] = invit_family.family_uid rescue ""
      detail["family_id"] = invit.family_id
      receiver = invit.sender_id.present? ? invit.member : invit.family.owner
      sender = invit.sender_id.present? ? invit.sender : invit.member
      detail["receiver_member_id"] = receiver.id rescue ""
      detail["receiver_email"] = receiver.email rescue ""
      detail["sender_member_id"] = sender.id rescue ""
      detail["sender_profile_image"] = (sender.profile_image.photo_url_str("small") rescue "")
      detail["sender_email"] = sender.email rescue ""
      # detail["status"] = "Request sent to join"
      detail["status"] = "Join request received"
      # detail["action"] ="You will be notified when your request is accepted"
      detail["receiver_first_name"] = receiver.first_name rescue ""
      detail["sender_first_name"] = sender.first_name rescue ""
      data << detail
      detail ={}
    end
    data
  end  
 


end