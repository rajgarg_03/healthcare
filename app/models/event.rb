class Event
  include Mongoid::Document
  include Mongoid::Timestamps
  # include Mongoid::Commentable
  field :tool_identifier # if any tool linked with event
  field :content
#  acts_as_activity :user
#   validates_presence_of :name
 #  validates_presence_of :start_time
  # validates_presence_of :end_time
  # validates_presence_of :user
  validate :start_time_cannot_be_before_end_time
 
  field :name, :type => String
  field :description, :type => String 
  field :start_date,        :type => DateTime
  field :end_date,        :type => DateTime
  field :start_time,        :type => DateTime
  field :end_time,        :type => DateTime
  field :member_id, :type=>String
  field :all_day, :type=>Boolean, default: false
  validates_presence_of :name
  # validate :time_valid
  belongs_to :member

  index ({member_id:1})
  
  #Procs used to make sure time is calculated at runtime
  scope :upcoming, lambda {
    where('end_time > ?' , Time.now.to_date).order('start_time ASC')
  }
  scope :past, lambda {
    where('end_time <= ?' , Time.now.to_date).order('start_time DESC')
  }
  
  
 # acts_as_commentable

  # Used by acts_as_commentable

  def time_and_date
    if !start_time.nil? and !end_time.nil?
      if spans_days?
        string = "#{start_time.strftime("%B %d")} to #{end_time.strftime("%B %d %Y")}"
      else
        string = "#{start_time.strftime("%B %d, %Y")}, #{start_time.strftime("%I:%M %p")} - #{end_time.strftime("%I:%M %p")}"
      end
    end
  end

 #api data
  # parent_member is required
  def self.get_event_data(calendar_type="parent",event_type="upcoming",child_member_id=nil,parent_member_id=nil,limit_count=nil,options={})
    calendar_data = []
   # get current date in timezone . DB store date in UTC so date might be different  
    #date_in_time_zone = Time.zone.parse( Date.today.strftime("%Y-%m-%d 00:00:00")).utc.to_date
    date_in_time_zone = Time.now.in_time_zone.to_date
    medical_status = {"Estimated"=> "Estimated due date", "Planned"=>"Due"}
    # Add current user id to fetch data if calendar type parent
    if calendar_type == "parent"
      parent_member = Member.find(parent_member_id)
      child_ids = parent_member.childern_ids
      event_member_ids = [parent_member.id] + child_ids
      vaccin_member_ids = child_ids
       
    else
      event_member_ids = [child_member_id]
      vaccin_member_ids = [child_member_id]
      return [{:message=>"Child member id can't blank", :start=>date_in_time_zone}] if child_member_id.blank?
    end
    # Get all Event, Jab and Prenatal test record 
    events = Event.all.where({'member_id' => { "$in" => event_member_ids} }).limit(limit_count).limit(limit_count)
    vaccinations = Vaccination.all.where({'member_id' => { "$in" => vaccin_member_ids} }).where(opted:"true").includes(:jabs)
    prenatal_tests =  MedicalEvent.where({'member_id' => { "$in" => vaccin_member_ids} }).where(opted:"true").not_in(status:["Completed"]).limit(limit_count)
    activity_planner_schedules =  Child::ActivityPlanner::Activity.where({'member_id' => { "$in" => event_member_ids} }).limit(limit_count)
    checkup_planner_schedules = Child::CheckupPlanner::Checkup.in(:member_id=>event_member_ids).where(:status.ne=>"Completed").limit(limit_count)
    
    if event_type == "upcoming"
      events = events.gte(start_date: date_in_time_zone).order_by("start_date asc")
      prenatal_tests = prenatal_tests.gte(est_due_time:date_in_time_zone).order_by("est_due_time asc")
      activity_planner_schedules = activity_planner_schedules.gte(start_time:date_in_time_zone).order_by("start_time asc")
      checkup_planner_schedules = checkup_planner_schedules.gte(checkup_date:date_in_time_zone).order_by("checkup_date asc")
      if limit_count.present?
        vaccine_ids = Vaccination.where(:member_id.in=>vaccin_member_ids).where(opted:"true").pluck(:id).map(&:to_s)
        jab_vaccine_ids = Jab.where(status: "Planned").or(:due_on.gte => date_in_time_zone).or(:due_on => nil, :est_due_time.gte => date_in_time_zone).where(:vaccination_id.in=>vaccine_ids).order_by("due_on asc, est_due_time asc").pluck(:vaccination_id).reverse[0..limit_count]
        vaccinations = Vaccination.in(:id=>jab_vaccine_ids)
      end
    else
      events = events.lt(start_date: date_in_time_zone).order_by("start_date desc")
      prenatal_tests = prenatal_tests.lt(est_due_time:date_in_time_zone).order_by("est_due_time desc")
      activity_planner_schedules = activity_planner_schedules.lt(checkup_date:date_in_time_zone).order_by("start_time desc")
      checkup_planner_schedules = checkup_planner_schedules.lt(checkup_date:date_in_time_zone).order_by("checkup_date desc")
    end
    
    role_maping_with_member_id = {}
    FamilyMember.in(member_id:(event_member_ids + vaccin_member_ids).uniq).map{|a| role_maping_with_member_id[a.member_id.to_s] = a.role }
    if options[:data_for_dashboard] == true
      name_maping_with_member_id = {}
      Member.in(id:(event_member_ids + vaccin_member_ids).uniq).map{|member| name_maping_with_member_id[member.id.to_s] = member.first_name }
    end    
    events.each do |event|
      #family_member = FamilyMember.where(member_id:event.member_id.to_s).last #event.member.family_members
      family_member_role = role_maping_with_member_id[event.member_id.to_s]
        event_data = {}
        event_data =  {:id => event.id,
            :title => (event.name.slice(0..49) rescue ""),
            :description => event.description,
            :start => "#{(event.start_date|| Date.today).to_date}",
            :end => "#{(event.end_date || Date.today).to_date}",
            :allDay => (event.all_day rescue false) ,
            :recurring => false,
            :backgroundColor => "#C0D230",
            :member_id => event.member_id, 
            :profile_pic => "",
            :role => (family_member_role rescue ""  ) ,
            :textColor => "black",
            :type => "Event",
            :datail=> event.attributes
          } 
        if options[:data_for_dashboard] == true
          event_data[:event_name] = event_data[:title]
          event_data[:event_upcoming_text] = "In #{ApplicationController.helpers.calculate_age4(Date.today,event_data[:start],level=1)}"
        end
        calendar_data << event_data
    end
    activity_planner_schedules.each do |activity_planner|
      family_member_role = role_maping_with_member_id[activity_planner.member_id.to_s]
      data = activity_planner.to_json
      event_data = {}
       event_data  =  {:id => activity_planner.id,
            :title => (data["activity_name"] rescue ""),
            :description => "",
            :start => "#{(activity_planner.start_time|| Date.today).to_date}",
            :end => "#{(activity_planner.end_time || Date.today).to_date}",
            :allDay => (false) ,
            :recurring => false,
            :backgroundColor => "#C0D231",
            :member_id => activity_planner.member_id, 
            :profilr_pic => "",
            :profile_pic => "",
            :role => (family_member_role rescue ""  ) ,
            :textColor => "black",
            :type => "ActivityPlanner",
            :datail=> data
          } 
        if options[:data_for_dashboard] == true
          event_data[:event_name] = "#{event_data[:title]} due for #{name_maping_with_member_id[event_data[:member_id].to_s]}"
          event_data[:event_upcoming_text] = "In #{ApplicationController.helpers.calculate_age4(Date.today,event_data[:start],level=1)}"
        end
         calendar_data << event_data 
    end

    checkup_planner_schedules.each do |checkup_planner|
      family_member_role = role_maping_with_member_id[checkup_planner.member_id.to_s]
      data = checkup_planner.attributes
      event_data = {}
        event_data = {:id => checkup_planner.id,
            :title => (data["title"] rescue ""),
            :description => (data["description"] rescue ""),
            :start => "#{(checkup_planner.checkup_date|| Date.today).to_date}",
            :end => "#{(checkup_planner.checkup_date || Date.today).to_date}",
            :allDay => (false) ,
            :recurring => false,
            :backgroundColor => "#C0D233",
            :member_id => checkup_planner.member_id, 
            :profilr_pic => "",
            :profile_pic => "",
            :role => (family_member_role rescue ""  ) ,
            :textColor => "blue",
            :type => "CheckupPlanner",
            :datail=> data
          } 
          if options[:data_for_dashboard] == true
          event_data[:event_name] = "#{event_data[:title]} due for #{name_maping_with_member_id[event_data[:member_id].to_s]}"
          event_data[:event_upcoming_text] = "In #{ApplicationController.helpers.calculate_age4(Date.today,event_data[:start],level=1)}"
        end
         calendar_data << event_data
    end

    prenatal_tests.each do |medical_event|
      #family_member = FamilyMember.where(member_id:medical_event.member_id.to_s).last #event.member.family_members
      family_member_role = role_maping_with_member_id[medical_event.member_id.to_s]
      event_data = {}
        event_data = {:id => medical_event.id,
            :title => ((medical_event.name|| medical_event.category).slice(0..49)  rescue "") + " - #{medical_status[medical_event.status]}",
            :description => medical_event.desc,
            :start => "#{(medical_event.est_due_time rescue Date.today).to_date}",
            :end => "#{(medical_event.est_due_time rescue Date.today).to_date}",
            :allDay => (medical_event.fullday rescue true) ,
            :recurring => false,
            :backgroundColor => "#C0D233",
            :member_id => medical_event.member_id, 
            :profilr_pic => "",
            :profile_pic => "",
            :role => (family_member_role rescue ""  ) ,
            :textColor => "black",
            :type => "PrenatalTest",
            :datail=> medical_event.attributes
          } 
          if options[:data_for_dashboard] == true
          event_data[:event_name] = "#{event_data[:title]} due for #{name_maping_with_member_id[event_data[:member_id].to_s]}"
          event_data[:event_upcoming_text] = "In #{ApplicationController.helpers.calculate_age4(Date.today,event_data[:start],level=1)}"
        end
         calendar_data << event_data
    end
     vaccinations.each do |vaccin|
      # family_member = FamilyMember.where(member_id:vaccin.member_id.to_s).last #event.member.family_members
      family_member_role = role_maping_with_member_id[vaccin.member_id.to_s]

      if event_type == "upcoming"
        jabs = vaccin.jabs.where(status: "Planned").or(:due_on.gte => date_in_time_zone).or(:due_on => nil, :est_due_time.gte => date_in_time_zone)
      else
        jabs = vaccin.jabs.or(:due_on.lt => date_in_time_zone).or(:due_on => nil,:est_due_time.lt => date_in_time_zone)
      end
      jabs.each do |cpe|
        if cpe.due_on.blank? && !cpe.est_due_time.blank?
          status = "Estimated due date"
        else
          status = "Due"
        end
        index = (vaccin.jabs.map(&:id).index(cpe.id)+1).to_s
        if options["calendar_version"].to_i > 0
          jab_name =  "Jab-#{index.to_i.ordinalize}"
          jab_name_in_detail = index.to_i.ordinalize
        else
          jab_name =  "Jab#{index.to_i}"
          jab_name_in_detail =  "Jab#{index.to_i}"
        end
        vaccin_name = vaccin.get_name
        event_data = {}
        event_data = {:id => cpe.id,
          :title => ("#{vaccin_name} -" + jab_name  + "-" +  status),
          :description => cpe.desc,
          :start => "#{(cpe.due_on|| cpe.est_due_time || Date.today).to_date}",
          :end => "#{(cpe.due_on || cpe.est_due_time || Date.today).to_date}",
          :allDay => true,
          :recurring => false,
          :backgroundColor => "#C0D890",
          :textColor => "black",
          :member_id => vaccin.member_id, 
          :profilr_pic => "" ,
          :profile_pic => "" ,
          :role => (family_member_role rescue "") ,
          :type => "Jab",
          :status=> status,
          :datail=> cpe.attributes.merge({:vaccin_name=>vaccin_name, :name=>"#{jab_name_in_detail}"})
        }
        if options[:data_for_dashboard] == true
          event_data[:event_name] = "#{vaccin_name} due for #{name_maping_with_member_id[event_data[:member_id].to_s]}"
          event_data[:event_upcoming_text] = "In #{ApplicationController.helpers.calculate_age4(Date.today,event_data[:start],level=1)}"
        end
         calendar_data << event_data
     end
   end
      
  calendar_data
  end
  

 #end of Api

  def self.create_event(current_user,data,options)
    
  end

  def update_event(current_user,data)
    event = self
    if event.update_attributes(data)
      #UserCommunication::UserActions.add_user_action(event,"Calendar",current_user,"update")
      event.member.send_event_to_parent_google_cal(event,"event","update") rescue nil
     retune true
   else
    return false
     end
  end
  def spans_days?
    (end_time - start_time) >= 86400 if !start_time.nil? and !end_time.nil?
  end
  
  protected
    def start_time_cannot_be_before_end_time
      errors.add("start_date", " must be before end time") unless start_date && end_date && (end_date > start_date)
    end 
end
