class VersionController
  include Mongoid::Document
  include Mongoid::Timestamps

  field :member_id, :type => String 
  field :update_screen_count, :type => Integer 
  field :force_update_count, :type => Integer 
  field :notification_count, :type=> Integer
  field :skip_up_to, :type=> Time
  
  index({member_id:1})
  Screen = {1=>"version_checker_notification_screen",2=>"version_checker_force_update_screen", 3=> "version_checker_update_screen"}
	
  # check_for_controller
  def self.run(current_user,options={})
    version_control_obj = VersionController.where(member_id:current_user.id.to_s).last || VersionController.create(member_id:current_user.id)
   # return [nil,false] if (version_control.skip_up_to > Time.zone.now rescue false)
  	device = current_user.devices.last 
    if options.present?
      options[:os_version] ||= device.os_version
      options[:platform]||= device.platform 
      options[:app_version] = options[:app_version].present? ? options[:app_version] :  device.app_version
      device = Device.new(options) 
    end
    version_control = false
    data = []
    begin
      if device.platform.downcase == "ios"
        update_url = "https://itunes.apple.com/app/nurturey/id1023121839?ls=1&mt=8"
      elsif device.platform.downcase == "android"
        update_url = "https://play.google.com/store/apps/details?id=com.nurturey.app"
      else
        update_url = "https://itunes.apple.com/app/nurturey/id1023121839?ls=1&mt=8"
      end
    rescue Exception => e 
      update_url = "https://itunes.apple.com/app/nurturey/id1023121839?ls=1&mt=8"
    end
    version_checker_notification_screen = {:title=>"I have a message for you",:subtitle=>"here is link"}
    version_checker_force_update_screen = {:title=>"Your personal assistant is now ready for an update to iOS 10",:subtitle=>"It is necessary to update your Nurturey app to the latest release to avoid incompatibility issues",:url=>update_url}
    version_checker_update_screen =       {:title=>"Please update the app",:subtitle=>"I have got several new features to support your journey",:url=>update_url}
    
    # For testing 
    # if Rails.env != "production"
    #   device_info = "app_version-#{device.app_version},platform-#{device.platform},os_version-#{device.os_version}"
    #   version_checker_notification_screen = {:title=>"I have a message for you- #{device_info}",:subtitle=>"here is link"}
    #   version_checker_force_update_screen = {:title=>"Your personal assistant is now ready for an update to iOS 10 #{device_info}",:subtitle=>"It is necessary to update your Nurturey app to the latest release to avoid incompatibility issues",:url=>update_url}
    #   version_checker_update_screen =       {:title=>"Please update the app #{device_info}",:subtitle=>"I have got several new features to support your journey",:url=>update_url}
    # end
    

    logic_data  = VersionController.controller_logic(device)
    if device.present? && version_control_obj.notification_count.to_i == 0
      version_control = true
      logic_data.each do |a|
        data << {Screen[a]=>eval(Screen[a])}
      end
    elsif logic_data && logic_data.include?(2) && version_control_obj.notification_count.to_i != 0
      version_control = true
      data << {Screen[2]=> eval(Screen[2])}
    else
      # if Covid19.is_active?
      #   # Show Covid-19 message for a while
      #   data << Covid19.get_covid_message_data(current_user,options)
      #   version_control = true
      # else
        data,version_control =  [[],false]  
      # end
    end
    if Covid19.is_active? && Covid19.is_valid_for_user?(current_user,options)
      # Show Covid-19 message for a while
      data << Covid19.get_covid_message_data(current_user,options)
      version_control = true
    end
    if version_control
      version_control_obj.notification_count = version_control_obj.notification_count.to_i + 1 
      version_control_obj.save
     end
     [data,version_control]
  end


def self.controller_logic(device)
  screens = []
  return [] if device.blank?
  platform = device.platform.downcase rescue ""
  os_version = device.os_version.to_i
  
  device_app_version_in_number = Device.new.app_version_conversion_from_str_to_int(device.app_version) rescue 0
  #IOS 2.4.3
  app_version_2_4_3 = Device.new.app_version_conversion_from_str_to_int("2.4.3")
  
  if(device_app_version_in_number.to_i < app_version_2_4_3 && platform == "ios")
    screens << 3
  end

  # Android < 2.3.8
  app_version_2_3_8 = Device.new.app_version_conversion_from_str_to_int("2.3.8")
  
  if(device_app_version_in_number.to_i < app_version_2_3_8 && platform == "android")
    screens << 3
  end

    # if (os_version >= 10 && app_version == 1.6  && platform == "ios")
    #   screens << 2
    # end
    # if (os_version < 10  && os_version >= 9 && app_version == 1.5  && platform == "ios")
    #   screens << 1
    #   screens << 3

    # end
    # if ( app_version == 1.5  && platform == "ios")
    #   screens << 3
    # end
    
    # # For android
    # if (os_version >= 7 && app_version == 1.1  && platform == "android")
    #   screens << 2
    # end
    # if (os_version < 7  && os_version >= 6 && app_version == 1.0  && platform == "android")
    #   screens << 1
    #   screens << 3

    # end
    # if ( app_version == 1.0  && platform == "android")
    #   screens << 3
    # end
    screens.uniq
  end

end


#1.if OS is 10 and version number is 1.6 and platform is iOS send me force update 
#2. if OS is 9 and version number is 1.5 and platform is iOS send me notification  + update screen
#3. if version number is 1.5  and platform is iOS send me update screen


#1.if OS is 7 and version number is 1.1 and platform is android send me force update 
#2. if OS is 6 and version number is 1.0 and platform is android send me notification  + update screen
#3. if version number is 1.0  and platform is android send me update screen
# iOS: 2.0.8 ,Android:2.0.5 version_checker_update_screen