class Orion::LoginDetail
# Keep referesh token and other detail for orion login 
  include Mongoid::Document  
  include Mongoid::Timestamps
  field :member_id,                     :type=> String
  field :refresh_token,                 :type=> String # Expiry token use to generate new token
  field :access_token,                  :type=> String # Expiry access token use to get api response

  def self.save_access_token(member,access_token,refresh_token,options={})
    begin
      Rails.logger.info "Saving token for Orion login"
      Orion::LoginDetail.where(member_id:member.id).delete_all
      Orion::LoginDetail.new(member_id:member.id,access_token:access_token, refresh_token:refresh_token).save!
    rescue Exception=>e 
      Rails.logger.info "Error inside Orion LoginDetail save_access_token..#{member.inspect}.........#{refresh_token}......#{e.message}"
    end
  end
  
  # ::Orion::LoginDetail.get_token_with_refresh_token(refresh_token)
  def self.get_token_with_refresh_token(refresh_token,options={})
    auth_base_url = APP_CONFIG['orion_auth_base_url']
    client_id = APP_CONFIG['orion_client_id'] #"I3dsKsfD0q3c45OEu5HeNPTqyKsE7GJH"
    client_secret = APP_CONFIG['orion_client_secret'] #"x3opFxWfM6yp8oUl"
    redirect_uri = APP_CONFIG["default_host"]+ "/users/auth/orion"
    
    url = URI("#{auth_base_url}/oauth2/token")
    https = Net::HTTP.new(url.host, url.port);
    https.use_ssl = true
    https.verify_mode = OpenSSL::SSL::VERIFY_PEER
    request = Net::HTTP::Post.new(url)

    authtoken = 'Basic ' + Base64.strict_encode64(client_id + ":" + client_secret)
    request["Authorization"] =  authtoken
    login_detail = Orion::LoginDetail.where(refresh_token:refresh_token).last
    
    request["Content-Type"] = "application/x-www-form-urlencoded"
    request.body = {:grant_type=>"refresh_token",:refresh_token=>refresh_token }.to_query
    response = https.request(request)
    if response.code.to_i == 200
      response_data = JSON.parse(response.body).with_indifferent_access
      data = {:status=>200, access_token:response_data[:access_token],refresh_token:response_data[:refresh_token], token_type:response_data[:token_type]}
      # Update token detail
      if login_detail.present?
        login_detail.update_attributes(refresh_token:data[:refresh_token],:access_token=>data[:access_token])
      end
    else
      Rails.logger.info "Error inside orion get_token_with_refresh_token ...user_id .....#{login_detail.user_id rescue nil}...  .#{refresh_token}.. No access token created"
      data = {status:response.code.to_i, message:"Failed to authrize"}
    end  
    data
  end


  
  # Orion::LoginDetail.get_access_token_with_code(code,options)
  def self.get_access_token_with_code(code,options={})
    Rails.logger.info "Code is.................#{code}"
     
    auth_base_url = APP_CONFIG['orion_auth_base_url']
    client_id = APP_CONFIG['orion_client_id'] #"I3dsKsfD0q3c45OEu5HeNPTqyKsE7GJH"
    client_secret = APP_CONFIG['orion_client_secret'] #"x3opFxWfM6yp8oUl"
    redirect_uri = APP_CONFIG["default_host"]+ "/users/auth/orion"
    
    url = URI("#{auth_base_url}/oauth2/token")
    https = Net::HTTP.new(url.host, url.port);
    https.use_ssl = true
    https.verify_mode = OpenSSL::SSL::VERIFY_PEER
    request = Net::HTTP::Post.new(url)

    authtoken = 'Basic ' + Base64.strict_encode64(client_id + ":" + client_secret)
    request["Authorization"] =  authtoken
    
    request["Content-Type"] = "application/x-www-form-urlencoded"
    request.body = {:grant_type=>"authorization_code",:code=>code,:redirect_uri=>redirect_uri }.to_query
    response = https.request(request)
      
    if response.code.to_i == 200
      response_data = JSON.parse(response.body).with_indifferent_access
      data = {:status=>200, access_token:response_data[:access_token],refresh_token:response_data[:refresh_token], token_type:response_data[:token_type]}
    else
      data = {status:response.code.to_i, message:"Failed to authrize"}
    end
    data
  end

  # ::Orion::LoginDetail.fetch_user_info(access_token,token_type,refresh_token,options)
  def self.fetch_user_info(access_token,token_type,refresh_token,options={})
    begin

      Rails.logger.info "Fetching user info from Orion"
      if access_token.blank? && options[:auth_code].present?
        data = ::Orion::LoginDetail.get_access_token_with_code(options[:auth_code],options)
        return data if data[:status] != 200
        access_token,token_type,refresh_token = [data[:access_token],data[:token_type],data[:refresh_token]]
      end

      api_base_url = APP_CONFIG['orion_api_base_url']
      url = URI.parse("#{api_base_url}/userinfo/")
      https = Net::HTTP.new(url.host, url.port);
      if url.scheme == 'https'
        https.use_ssl = true
        https.verify_mode = OpenSSL::SSL::VERIFY_PEER
      end
      request = Net::HTTP::Get.new(url)
      request["Authorization"] =  token_type.titleize + ' ' + access_token
      request["Accept"] = "application/json"
      response = https.request(request)
      if response.code.to_i == 200
        Rails.logger.info "User info fetched from Orion........................"
        response_data = JSON.parse(response.body).with_indifferent_access
        response_data =  ::Orion::LoginDetail.format_orion_user_data(response_data,options)
        data = response_data.merge({status:200})
        email = data[:email]
        if options[:create_deeplink_data] == true
          data = ::Orion::LoginDetail.create_deeplink_data(data,access_token,refresh_token,options)
        end
        user = User.where(email:email).last
 
        if user.present?
          ::Orion::LoginDetail.save_access_token(user.member,access_token,refresh_token,options)
        elsif options[:member_id].present?
          member = Member.find(options[:member_id])
          ::Orion::LoginDetail.save_access_token(member,access_token,refresh_token,options)

        end
      else
        data = {status:response.code.to_i, message:"Failed to connect with api"}
      end
    rescue Exception => e 
      Rails.logger.info "Error inside ::Orion::LoginDetail fetch_user_info ....... #{e.message}"
      data = {status:100, message:"Failed to connect with api"}
    end
    data 
  end


  def self.format_orion_user_data(user_data,options={})
    data = user_data
    data.merge!({
      :patient_id=>nil,
      :date_of_birth=>nil,
      :user_email=>user_data[:email],
      :last_name=>user_data[:family_name],
      :first_name=>user_data[:given_name].to_s,
      :surname=> user_data[:family_name],
      :account_id=>nil,
      :ods_code=>"orion"
    }) 
    data
  end

  def self.create_deeplink_data(user_info,access_token,refresh_token,options={})
    begin
      Rails.logger.info "Creating deeplink for Orion"
      Rails.logger.info "User info for deeplink #{user_info.inspect}"
      token_info = token_info || {}
      user_info = (user_info).with_indifferent_access
      deep_link = APP_CONFIG['default_host']+"/orion_authentication_completed"
      email = user_info["email"]
      user = User.where(email:email).last
      current_user = user.member rescue Member.new
      access_token =  access_token 
      refresh_token = refresh_token #token_info[:refresh_token] ? token_info[:refresh_token] : ""
       
       
      already_account_exist = user.present?   
      orion_account = (user.account_type_allowed?("Orion") rescue false)
      clinic_detail = {"account_id"=>nil,"practice_ods_code"=>"orion","surname"=>"","date_of_birth"=>"",:register_with_pindocument=>true}
      api_access_token = (user.api_access_token ||  user.refresh_api_token )rescue nil
      orion_info = user_info
      query_data = orion_info.merge({
        :access_token=>access_token,
        :refresh_token=>refresh_token,
        :api_access_token=>api_access_token,
        :linkable_status=>true,
        :is_account_already_exist =>already_account_exist,
        :is_orion_account => orion_account
      }).merge(clinic_detail)
      
      final_deep_link = deep_link + "?#{query_data.to_query}"

      Rails.logger.info "................Deeplink:  #{final_deep_link} ......................."
      status = 200
    rescue Exception=>e
      Rails.logger.info e.message
      status = 100
    end
    [final_deep_link,status]
  end

end