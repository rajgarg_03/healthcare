class AlbumController < ApplicationController
  before_filter :authenticate_user!, :only => [:add_album,:photo_list,:add_photo,:destroy_album_photo,:album_list,:destroy_album]
  before_filter :check_current_user_member

  def check_current_user_member
    @user_family_member = current_user.family_members(FamilyMember::STATUS[:accepted]) if !current_user.blank?
  end


  def index
    @member  = current_user
    @albums = Album.all
  end
  

  def create
    @album = Album.new(params[:album])
    @album.save
    @member = @album.member
    (params[:upload_media] || [] ).each do |media_file|
      @album.pictures << Picture.new(local_image: media_file, upload_class: "album") 
    end
    render layout: false
  end

  def new
    @album = Album.new(member_id:params[:member_id])
    render :layout => false  
  end

  def get_album_detail
    @album = Album.find(params[:album_id])
    
  end

  def delete_photo
    render text: "success"
  end
  
  def destroy_album_photo
    @album_image = Picture.find(params[:id])
    @album = @album_image.imageable
    @album_image.destroy
    flash[:notice] = Message::GLOBAL[:success][:destroy_album_photo]
    redirect_to :action => :photo_list ,:test_report_id => @album.id and return
  end

  def photo_list
    @album = Album.find(params[:test_report_id]) if !params[:test_report_id].blank?
    @album_images = @album.pictures
  end
  
  def destroy_album
    @album = Album.find(params[:id])
    @album_images = @album.pictures
    @album_images.each do | single | 
      single.destroy
    end
    @album.destroy
    flash[:notice] = Message::GLOBAL[:success][:destroy_album] #{}"Album destroy successfully."
    redirect_to :action => :album_list  and return
  end

  def album_list    
    @albums = []
    current_user.families.each do |family|
      family.albums.each do |album|
        @albums << album
      end
    end    
  end
  
  def add_photo
    @extra_style = params[:style] if params[:style].present?    
    @album = Album.find(params[:test_report_id]) 
    if request.post? || request.put?
      params[:test_reports][:name] = params[:test_reports][:local_image].original_filename
      @album.pictures.create(params[:test_reports])
      if @album.save
        flash[:notice] = Message::GLOBAL[:success][:add_photo] #{}" Photo added successfully "
        redirect_to :action => :photo_list ,:test_report_id => @album.id and return
      else
        puts @album.errors.to_a
        flash[:notice] =  Message::GLOBAL[:error][:add_photo] #{}"There were some problems uploading the photo"
        redirect_to :action => :photo_list ,:test_report_id => @album.id and return
      end
      
    end
    render :layout => false and return
  end
end
