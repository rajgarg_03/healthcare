class DocumentsController < ApplicationController
	skip_before_filter  :verify_authenticity_token, only: [:destroy, :delete_attachment]
	before_filter :google_drive_login, :only => [:list_google_docs]

	GOOGLE_CLIENT_ID = APP_CONFIG['google_client_id']
	GOOGLE_CLIENT_SECRET = APP_CONFIG['google_client_secret']
	GOOGLE_CLIENT_REDIRECT_URI = "#{APP_CONFIG['default_host']}/oauth2callback"
	
	# better put constant like above in environments file, I have put it just for simplicity
	  def list_google_docs
	  	@member = Member.find(params[:member_id])
	    google_session = GoogleDrive.login_with_oauth(session[:google_token])
	    @google_docs = []
	    for file in google_session.files
	      @google_docs  << file.title     
	    end   
	   end

	  def download_google_docs
	    file_name = params[:doc_upload]
	    file_path = Rails.root.join('tmp',"doc_#{file_name.gsub('/','_')}")
	    google_session = GoogleDrive.login_with_oauth(session[:google_token])
	    file = google_session.file_by_title(file_name)
	    #TODO: Convert this to dynamic
	    @member = Member.find(params[:member_id])
	    file.download_to_file(file_path)	    
	    document = File.open(file_path)
	    @member.documents << Document.new(file: document)
	    @member.save	    
	    File.delete(file_path)
	    # redirect_to documents_path(member_id: @member.id)
	    # redirect_to list_google_doc_path
	  end

	  def set_google_drive_token
	  	client = Google::APIClient.new
		auth = client.authorization
		auth.client_id = GOOGLE_CLIENT_ID
		auth.client_secret = GOOGLE_CLIENT_SECRET
		auth.scope =
		    "https://www.googleapis.com/auth/drive " +
		    "https://docs.google.com/feeds/ " +
		    "https://docs.googleusercontent.com/ " +
		    "https://spreadsheets.google.com/feeds/"
		auth.redirect_uri = GOOGLE_CLIENT_REDIRECT_URI		
		if params[:code].blank?
			redirect_to "#{auth.authorization_uri}"
		else
			auth.code = params[:code]
			auth.fetch_access_token!
			access_token = auth.access_token
		    session[:google_token] = access_token if access_token
		    redirect_to list_google_doc_path
		end    
	  end

	  def google_drive_login
	  	# session[:google_token] = nil
	    unless session[:google_token].present?
	      google_drive = GoogleDrive::GoogleDocs.new(GOOGLE_CLIENT_ID,GOOGLE_CLIENT_SECRET,
	                     GOOGLE_CLIENT_REDIRECT_URI)
	      auth_url = google_drive.set_google_authorize_url
	      redirect_to auth_url
	    end
	  end


	def index
	  @member = Member.find(params[:member_id])
	  @documents = @member.documents
	  info_popup_status(@member,"documents")
	end

	def new
	  @member = Member.find(params[:member_id])
	  @document = @member.documents.new
	  render layout: false
	end		

	def create
	  @member = Member.find(params[:member_id])
	  @document = @member.documents.new(params[:document])
	  # @document.file = params[:upload_document].last
	  @message = Message::GLOBAL[:success][:doc_create] if @document.save #"Your document has been uploaded."
	  @documents = @member.documents
	end

	def edit
	  @document = Document.find(params[:id])
	  @member = @document.member
	  render layout: false
	end	

	def update
	  @member = Member.find(params[:member_id])
	  @document = @member.documents.find(params[:id])
	  # @document.file = params[:upload_document].last if params[:upload_document]
	  @document.update_attributes(params[:document])
	  @documents = @member.documents
	end	

	def destroy
	  @document = Document.find(params[:id])	  
	  @document.destroy
	  @member = @document.member.reload
	  @documents = @member.documents
	end	

	def search
	  @member = Member.find(params[:member_id])
	  if params[:Document][:name].present?
	  	@documents = @member.documents.any_of(name: /#{params[:Document][:name]}.*/)
	  else
	  	@documents = @member.documents
	  end	
	  # Document.any_of(name: /test.*/).entries
	end	

	def delete_attachment
	  @document = Document.find(params[:id])
	  @document.file = nil
	  @document.save
	end	

	def download
	  @document = Document.find(params[:id])
	  # send_file @document.file.path
	  redirect_to @document.file.url
	end	

	def documents_info
	  render layout: false
	end	
		
end	