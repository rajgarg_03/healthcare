class PasswordsController < Devise::PasswordsController
  prepend_before_filter :clear_session, :only=>[:edit]
  before_filter :authenticate_user!, :except => [:new,:create]
  layout "login"
  def edit
    self.resource = User.find(params[:id])
    if params[:alexa_flow_status].to_s == "true"
      session[:alexa_flow_status] = true
    end
    resource = User.find(params[:id])
    super
    @user = current_user
  end
  
  def update
    params[:platform] = "web" # used in log event
    if request.xhr? || params[:user][:current_password].present?
      @user = current_user.user
      @success = false
      password_blank = params[:user][:current_password].blank? #params are not present after next method i.e update_with_password
      user_error_msg = User.new(:email=>current_user.email,:password_validation_type=> GlobalSettings::PasswordValidationType, :password_confirmation=>params[:user][:password_confirmation],   :password=>params[:user][:password]).password_complexity
      if password_blank
        @error = Message::DEVISE[:m17]
        message = @error 
        status = StatusCode::Status100
      elsif  user_error_msg.present? 
        
        @error = user_error_msg.last
        message = @error 
        status = StatusCode::Status100
      elsif (params[:user][:password] == params[:user][:current_password]) || @user.used_password?(params[:user][:password])
        @user.used_password?(params[:user][:password])
        @error = Message::DEVISE[:m21]
        message = @error 
        status = StatusCode::Status100
      elsif params[:user].present? && params[:user][:password].present? && @user.update_with_password(params[:user])
        @user = @user.reload
        sign_out(@user)
       sign_in(:user, @user, bypass: true)
        @success = true
        session[:api_token] = @user.confirmation_token
        @message = Message::GLOBAL[:success][:password_update] #"Your password has been changed."
        message = @message
        status = StatusCode::Status200
      
      else
        @error = Message::GLOBAL[:success][:password_blank] if password_blank 
        @error = @error || select_error_message(@user.errors.messages)
        message = @user.errors.messages ||  @error
        status = StatusCode::Status100
      end
      if status == 200
        PasswordHistory.create_password_record(@user,params[:user][:password])
        event_name = "user password changed successfully"
      else
        event_name = "user password not changed"
      end
      @user.delay.log_signin_event(event_name,params) rescue nil
      
      respond_to do |format|
        format.js  
        format.json{render :json=> {:message => message , :status=> status }}
      end
    else
      @user =  User.where(reset_password_token:params[:user][:reset_password_token]).last
      @password_format_correct = true
      user_error_msg = User.new(:email=>@user.email,:password_validation_type=> GlobalSettings::PasswordValidationType, :password_confirmation=>params[:user][:password_confirmation],   :password=>params[:user][:password]).password_complexity rescue nil
      if @user.blank?
        @user = User.new
        @error = "Sorry, reset password link has expired. Please request for forgot Password again"
        message = @error 
        @user.errors.add :password, @error
        status = StatusCode::Status100
        @password_format_correct = false
      elsif user_error_msg.present?
        @error = user_error_msg.last
        @user.errors.add :password, @error
        message = @error 
        status = StatusCode::Status100
        @password_format_correct = false
      elsif @user.used_password?(params[:user][:password])
        @error = Message::DEVISE[:m21]
        message = @error 
        @user.errors.add :password, @error
        status = StatusCode::Status100
        @password_format_correct = false
        event_name = "user password not changed"
      else
        self.resource = resource_class.reset_password_by_token(resource_params)
      end
       
      if @password_format_correct && resource.errors.empty?  
        event_name = "user password changed successfully"
        @user.delay.log_signin_event(event_name,params) rescue nil
        PasswordHistory.create_password_record(resource,params[:user][:password]) 
        
        resource.unlock_access! if unlockable?(resource)
        flash_message = resource.active_for_authentication? ? :updated : :updated_not_active
        set_flash_message(:notice, flash_message) if is_navigational_format?
        sign_in(:user, resource, bypass: true)

        session[:api_token] = @user.confirmation_token
        redirect_url = session[:alexa_flow_status] == true ? "/alexa_account_link" : dashboard_url
        respond_to do |format|
          format.html { redirect_to redirect_url}
          format.json{render :json=> {:message => flash_message, :status=> StatusCode::Status100 }}
        end
        
      else
        @resource = @user
        @token = params[:user][:reset_password_token] rescue  nil
        if !@password_format_correct
          @error_message = @error
        else
          @error_message = select_error_message(resource.errors.messages)
        end
        event_name = "user password not changed"
        @user.delay.log_signin_event(event_name,params) rescue nil
       

        respond_to do |format|
          format.html {respond_with (@user)}
          format.json{render :json=> {:message => @error_message, :status=> StatusCode::Status100 }}
        end

      end
    end
  end

  def secure_update
      params[:user][:password] = ShaAlgo.decryption(params[:user][:password].gsub(" ","+")) if params[:user][:password].present?
      params[:user][:password_confirmation] = ShaAlgo.decryption(params[:user][:password_confirmation].gsub(" ","+"))
      password_validation_type = params[:password_validation_type] || params[:user][:password_validation_type] || "type1"
    if request.xhr? || params[:user][:current_password].present?
      @user = current_user.user
      @success = false
      password_blank = params[:user][:current_password].blank? #params are not present after next method i.e update_with_password
      user_error_msg = User.new(:email=>current_user.email,:password_validation_type=> GlobalSettings::PasswordValidationType, :password_confirmation=>params[:user][:password_confirmation],   :password=>params[:user][:password]).password_complexity
      if password_blank
        @error = Message::DEVISE[:m17]
        message = @error 
        status = StatusCode::Status100
      elsif user_error_msg.present?    
        @error = user_error_msg.last
        message = @error 
        status = StatusCode::Status100
      elsif (params[:user][:password] == params[:user][:current_password]) || @user.used_password?(params[:user][:password])
        @error = Message::DEVISE[:m21]
        message = @error 
        status = StatusCode::Status100
      elsif params[:user].present? && params[:user][:password].present? && @user.update_with_password(params[:user])
        @user = @user.reload
        sign_in(@user, :bypass => true)
        @success = true
        session[:api_token] = @user.confirmation_token
        @message = Message::GLOBAL[:success][:password_update] #"Your password has been changed."
        message = @message
        status = StatusCode::Status200
      else
        @error = Message::GLOBAL[:success][:password_blank] if password_blank 
        @error = @error || select_error_message(@user.errors.messages)
        message = @user.errors.messages ||  @error
        status = StatusCode::Status100

      end
      if status == 200
        event_name = "user password changed successfully"
      else
        event_name = "user password not changed"
      end
      @user.delay.log_signin_event(event_name,params) rescue nil
      respond_to do |format|
        format.js  
        format.json{render :json=> {:message => message , :status=> status }}
      end
    else
      resource_params[:password] = params[:user][:password]
      resource_params[:password_confirmation] = params[:user][:password_confirmation]

      self.resource = resource_class.reset_password_by_token(resource_params)
      event_name = "user password not changed"
      resource.delay.log_signin_event(event_name,params) rescue nil
      if resource.errors.empty?
        resource.unlock_access! if unlockable?(resource)
        flash_message = resource.active_for_authentication? ? :updated : :updated_not_active
        set_flash_message(:notice, flash_message) if is_navigational_format?
        sign_in(resource_name, resource)
        session[:api_token] = @user.confirmation_token
        respond_to do |format|
          format.html { redirect_to dashboard_url}
          format.json{render :json=> {:message => flash_message, :status=> StatusCode::Status100 }}
        end
        
      else
        @resource = resource
        @token = params[:user][:reset_password_token] rescue  nil
        @error_message = select_error_message(resource.errors.messages)
        respond_to do |format|
          format.html {respond_with resource}
          format.json{render :json=> {:message => @error_message, :status=> StatusCode::Status100 }}
        end

      end
    end
  end
  
  def new
    super
  end

  def create
    data  = resource_params.blank? ? params[:user] : resource_params
    user_obj = User.where(email:data[:email]).last 
    if user_obj.present? 
      user_obj[:alexa_flow_status] = params["alexa_flow_status"].to_s == "true" rescue nil
      user_obj.save rescue nil
    end
    # resource_class[:alexa_flow_status] = params[:alexa_flow_status].to_s == "true"
    if user_obj.present? && user_obj.get_account_status != "Blocked" &&   user_obj.account_type_allowed?("Nurturey")
      self.resource = resource_class.send_reset_password_instructions(data)
    end
    user_obj.unset('alexa_flow_status') rescue nil
    
    # if successfully_sent?(resource)
      respond_to do |format|
        format.html {redirect_to home_messages_path(forgot_password: true)}
        format.js {redirect_to home_messages_path(forgot_password: true)}
        #"You will receive an email with instructions about how to reset your password in a few minutes"
        format.json{render :json=> {:message =>Message::GLOBAL[:success][:password_create]  ,:screen_name=>["sign_in"], :status=> 200 }}
      end
    # else
    #   session[:email] = params[:user][:email]
    #   flash[:notice] = Message::GLOBAL[:error][:unregistered_email] #"The email you entered is not registered."
    #   respond_to do |format|
    #     format.html {redirect_to "/users/sign_in?pageName=reset"}
    #     format.js {redirect_to "/users/sign_in?pageName=reset"}
    #     format.json{render :json=> {:message => flash[:notice] ,:screen_name=>["forgot_password"], :status=> 100 }}
    #   end

    # end
  end

  protected
  # The path used after sending reset password instructions
  def after_sending_reset_password_instructions_path_for(resource_name)
    "/"
    # new_user_password_path if is_navigational_format?
  end

  def select_error_message(messages)
    error_message =  Message::DEVISE[:m17] if (messages[:current_password].present? rescue false)
    error_message = Message::GLOBAL[:success][:password_forgot_password] if messages[:reset_password_token]
    error_message = Message::DEVISE[:m13] if (messages[:password].include?("is too short (minimum is 8 characters)") rescue false)
    error_message = Message::DEVISE[:m14] if (messages[:password].include?("doesn't match confirmation") rescue false)
    error_message = Message::DEVISE[:m15] if (messages[:password].include?("can't be blank") rescue false)
    
    error_message
  end
  def clear_session
    session.clear
  end

end