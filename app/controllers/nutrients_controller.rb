class NutrientsController < ApplicationController
  skip_before_filter  :verify_authenticity_token, only: [:deactivate]
  
  def index
    api_version = 1    
    @member = Member.find(params[:member_id])
    @member_nutrients = Nutrient.get_member_tools(@member,current_user,api_version).entries
    valid_tool_for =   ["child","CP"]
    options = {:show_feature_type=>"tools"}
    @recommended_nutrient = Nutrient.valid_to_launch(current_user,api_version,options).in(tool_for:valid_tool_for).order_by("position ASC").entries
  end

  def acitvate   
    nutrient = Nutrient.find(params[:id])
    api_version = 1
    @member_nutrient = MemberNutrient.where(member_id:params[:member_id],nutrient_id:nutrient.id, position: nutrient.position).first_or_create
    @member = ChildMember.find(params[:member_id])
    @family_member = FamilyMember.where(member_id: @member.id).first
    @member_nutrients = Nutrient.get_member_tools(@member,current_user,api_version).entries
    Timeline.update_timeline_state_for_tool(@member, nutrient.title,true)
  end

  def deactivate
    @member_nutrient = MemberNutrient.where(member_id:params[:member_id],nutrient_id:params[:id]).first
    @member_nutrient.destroy
    nutrient = Nutrient.find(params[:id])
    @member = Member.find(params[:member_id])
    @family_member = FamilyMember.where(member_id: @member.id).first
    api_version = 1
    @member_nutrients = Nutrient.get_member_tools(@member,current_user,api_version).entries
    Timeline.update_timeline_state_for_tool(@member, nutrient.title,false)
  end

  def change_category
    api_version = 1
    @member = Member.find(params[:member_id])    
    @member_nutrients = Nutrient.get_member_tools(@member,current_user,api_version).entries.select{|n| n.categories.any?{|cat| cat==params[:category]}}
    @recommended_nutrient = @member.get_nutrients(api_version,current_user).select{|n| n.categories.any?{|cat| cat==params[:category]}}
  end  

end	