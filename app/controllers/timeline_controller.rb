class TimelineController < ApplicationController
  #layout "timeline" #TODO: confirm with Rajesh
  layout "application"

  def index
    @timeline_member = Member.where(id:params[:member_id]).first || current_user
    selected_date = (params[:selected_date] || Date.today).to_date + 1.day
    @timelines= @timeline_member.timelines.where(:state=>true, :timeline_at.lte => selected_date).order_by("timeline_at DESC").order_by("id DESC").paginate(:page => (params[:page]), :per_page => 10)
    @automated_entries = @timeline_member.member_settings.where(name:"timeline_setting").first
    @setting  = @timeline_member.member_settings.where(name:"timeline_setting", status:"pending").first.blank? ? "None" : "pending"
    @display_setting = @timeline_member.member_settings.where(name:"timeline_display").first || @timeline_member.member_settings.create(name:"timeline_display", status:"true",note: 1.days.ago )    
    @health_unit = Health.health_unit(current_user.metric_system)
    
    info_popup_status(@timeline_member,"timeline")
    respond_to do |format|
      format.js {render :layout=> false}
      format.html
    end
  end

  # TODO: Not using this action for now but can use in future.
  # def create
  #   params[:timeline].merge!({:added_by=> current_user.id})
  #   @timeline= Timeline.new(params[:timeline])
  #   @child_member = ChildMember.find(params[:member_id])
  #   pictures = []
  #   (params[:userprofile_picture]||[]).each do |pic|
  #     # @timeline.pictures << Picture.new(local_image:pic,upload_class: "timeline", member_id:params[:member_id])
  #     pictures << Picture.new(local_image:pic,upload_class: "timeline", member_id:params[:member_id])
  #   end
  #   if pictures.all?{|p| p.valid?}
  #     @child_member.timelines << @timeline
  #     @timeline.pictures << pictures
  #   else
  #     @pic_invalid = true  
  #   end
  #   @message = "Your post has been created."
  #   respond_to do |format|
  #     format.js {render :layout=> false}
  #   end
  # end

  def delete_timeline_pic
    @picture = Picture.find(params[:pic_id])
    @timeline = Timeline.find(@picture.imageable_id)
    @picture.delete
  end


  def remind_later
    setting = MemberSetting.find(params[:setting_id])
    if params[:no_thanks].present?
      setting.update_attributes({:status=>"Done", note:""})
    else
      setting.update_attributes({:status=>"false",note: (Date.today + 7.days)})
    end
    respond_to do |format|
      format.js {render :text=> "ok", :layout=> false}
    end
  end

  def new
    @timeline = Timeline.new
    @timeline_member = Member.where(id:params[:member_id]).first
    respond_to do |format|
      format.html {render :layout=> false}
    end
  end

  def edit
    @timeline = Timeline.find(params[:id])
    respond_to do |format|
      format.html {render :layout=> false}
    end
  end

  def create_timeline
    params[:timeline][:name] = nil if (params[:timeline][:name] == "Timeline title" || params[:timeline][:name].blank?)
    # params[:timeline][:timeline_at] = params[:timeline][:timeline_at].present? ? (params[:timeline][:timeline_at]+' '+Time.now.strftime("%H:%M:%S %z")) : Time.now
    @timeline = Timeline.new(params[:timeline])
    @timeline.timeline_at = @timeline.get_timeline_at(params[:timeline][:timeline_at])
    @timeline.timelined_type = "Timeline"
    @timeline.entry_from = "timeline"
    @member = @timeline.member
    # ids = params[:deleted_image].split(",").compact rescue []
    # Picture.where({'_id' => { "$in" => ids} }).delete_all
    if @timeline.save
      @message = Message::GLOBAL[:success][:create_timeline] #"Your post has been created."
      (params[:upload_media] || [] ).select{|p| p.present?}.each do |media_file|
        @timeline.pictures << Picture.new(local_image: media_file, upload_class: "timeline",member_id:@member.id)
      end
      if @timeline.pictures.all?{|p| p.valid?}        
        family_id = FamilyMember.where(member_id: @member.id).first.family_id
        Score.create(family_id: family_id,activity_type: "Timeline", activity_id: @timeline.id, activity: "Added post #{@timeline.name.truncate(15)}", point: Score::Points["Timeline"], member_id: @member.id)
      else
        @pic_invalid = true          
      end
   end
    respond_to do |format|
      format.js {render :layout=> false}
    end
  end  
  
  def update_timeline
    params[:timeline][:name] = nil if (params[:timeline][:name] == "Timeline title" || params[:timeline][:name].blank?)
    @timeline = Timeline.find(params[:id])
    @member = @timeline.member
    # params[:timeline][:timeline_at] = (params[:timeline][:timeline_at]+' '+@timeline.timeline_at.strftime("%H:%M:%S %z")) rescue Time.now
    params[:timeline][:timeline_at] = @timeline.get_timeline_at(params[:timeline][:timeline_at])
    ids = params[:deleted_image].split(",").compact rescue []
    Picture.where({'_id' => { "$in" => ids} }).delete_all

    (params[:upload_media] || [] ).select{|p| p.present?}.each do |media_file|
      @timeline.pictures << Picture.new(local_image: media_file, upload_class: "timeline",member_id:@member.id)
    end

    if @timeline.pictures.all?{|p| p.valid?}
      @status = @timeline.update_attributes(params[:timeline]) 
      @timeline.reload
    else
      @pic_invalid = true          
    end    
    respond_to do |format|
      format.js {render :layout=> false}
    end
  
  end

  def delete_timeline
    @timeline = Timeline.find(params[:id])
    @timeline.delete
    Score.where(activity_type: "Timeline", activity_id: @timeline.id, member_id: @timeline.member.id).first.destroy rescue nil
    respond_to do |format|
      format.js {render :layout=> false}
    end
  end

  def automated_timeline
    @member = Member.find(params[:member_id])
    settings = @member.member_settings.where(name:"timeline_setting", status:"pending").first
    skipped_timelines = settings.note.split(",") rescue []
    next_timeline = skipped_timelines.index(params[:id]) || 0
    @timeline = Timeline.find(skipped_timelines[next_timeline])
    
    @last = skipped_timelines.last(2).first.to_s == params[:id].to_s
    render :layout=> false
  end

  def skip_automated_timeline
    @member = Member.find(params[:member_id])
    settings = @member.member_settings.where(name:"timeline_setting").first
    skipped_timelines = settings.note.split(",") rescue []
    next_timeline = skipped_timelines.index(params[:id]) + 1 || -1 rescue 0
    @timeline = Timeline.find(skipped_timelines[next_timeline])
    @last = skipped_timelines.last.to_s == @timeline.id.to_s
    @skipped = true
    # settings.update_attributes(status:"done") if @last && skipped_timelines.count == 1
    render action: "save_auto_time"
  end

  def save_auto_time
    @current_timeline = Timeline.find(params[:timeline]["id"])
    @member = Member.find(params[:member_id]) 
    @current_timeline.update_attributes(params[:timeline])
    (params[:upload_media]||[]).each do |pic|
      @current_timeline.pictures << Picture.new(local_image:pic,upload_class: "timeline",member_id:@member.id)
    end
    setting = @member.member_settings.where(name:"timeline_setting").first
    skipped_timelines = setting.note.split(",") rescue []
    next_timeline = skipped_timelines.index(params[:timeline]["id"]) + 1 || -1 rescue 0
    skipped_timelines.delete(params[:timeline][:id])
    @timeline = Timeline.find(skipped_timelines[next_timeline])
    setting.update_attributes(note:skipped_timelines.join(","))
    @last = skipped_timelines.last.to_s == @timeline.id.to_s
    setting.update_attributes(status:"done") if  @last && skipped_timelines.blank?
  end

  def media
    render :layout=> false
  end

  def pics
    @timeline = Timeline.where(id:params[:timeline]).first
    render :layout=> false
  end

  def timeline_info
    render layout: false
  end

  def facebook_preview
    @timeline = Timeline.find(params[:id])
    render layout: false
  end

  def send_to_facebook    
    @timeline = Timeline.find(params[:id])
    redirect_to "/users/auth/facebook?timeline_id=#{@timeline.id}&message=#{params[:post_title]}"
  end
    
end
