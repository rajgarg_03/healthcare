class CampaignManager::EmailCampaignController < ApplicationController
  before_filter :authenticate_user!, :except => [:uninstall_callback_campaign,:uninstall_callback_campaign_browser_view]

  def uninstall_callback_campaign
    begin
      uninstall_callback_campaign = CampaignManager::UninstallCallbackCampaign.where(id:params[:campaign_id]).last
      uninstall_callback_campaign.user_respond_to_mail_status = true
      uninstall_callback_campaign.user_last_respond_date = Date.today
      uninstall_callback_campaign.save
      @url = "/"
    rescue Exception=> e 
       @url =  "/"
    end
    render layout:false
  end

  def uninstall_callback_campaign_browser_view
      begin
      @uninstall_callback_campaign = CampaignManager::UninstallCallbackCampaign.where(id:params[:campaign_id]).last
      @user =  Member.find(@uninstall_callback_campaign.member_id).user
      render :layout=> false
      rescue Exception=>e 
       redirect_to "/" and return true
      end
  end

end
