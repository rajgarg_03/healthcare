class HomeController < ApplicationController
  before_filter :authenticate_user!, only: [:alexa_account_link]
  layout 'home_page_uk_layout'

  def index
    @resource = User.new
    clear_alexa_session
    session[:deeplink] = params if params[:destination_name].present?
    session[:uae] = false
    user_locale = I18n.default_locale
    if params[:test_locale].blank? && !params[:user_locale].present?
      begin
        url = "https://ipapi.co/#{request.remote_ip}/country"
        country = (Net::HTTP.get_response(URI.parse(url)).body)
        get_locale =  (country == "UK" || country == "GB") ? :uk : I18n.default_locale
        params[:user_locale] = get_locale
      rescue Exception=>e
      end
    else
      params[:user_locale] = params[:user_locale] || params[:test_locale]
    end
    I18n.locale = (params[:user_locale].to_s.downcase == "uk" ? :uk : user_locale).to_sym
    @invitation = Invitation.find(session[:invitation_id]) if session[:invitation_id]
    if current_user
      redirect_to dashboard_path and return
    else
      render :layout => false
    end
  end

  def uk
    redirect_to "/" and return
    # @resource = User.new
    # clear_alexa_session
    # session[:deeplink] = params if params[:destination_name].present?
    # I18n.locale = :uk
    # @locale = "uk"
    # @invitation = Invitation.find(session[:invitation_id]) if session[:invitation_id]
    # if current_user
    #   redirect_to dashboard_path and return
    # else
    #   render :action=>:index, :layout => false
    # end
  end

  def pinkbook
    redirect_to "/" and return
    #  @resource = User.new
    # clear_alexa_session
    # session[:deeplink] = params if params[:destination_name].present?
    # I18n.locale = :uk
    # @locale = "uk"
    # @invitation = Invitation.find(session[:invitation_id]) if session[:invitation_id]
    # if current_user
    #   redirect_to dashboard_path and return
    # else
    #   render :action=>:index, :layout => false
    # end
  end


  def designasmile
    @designcontest = CampaignManager::DesignContest.new
    render :layout=>false
  end

  def save_designasmile
    designcontest =  CampaignManager::DesignContest.where(email:params[:email]).last
    form_data = {name:params[:name],email:params[:email],address:params[:country],no_of_children:params[:children],age_of_childrens:params[:age]}
    form_data[:file] = params[:upload]
    if designcontest.present?
      status = 100
      message = "Sorry, this email has already sent an entry. Kindly use a different email"
      response = {status:status,message:message,designcontest:designcontest}
    else
      response = CampaignManager::DesignContest.create_record(form_data)
    end
    if response[:status] != 200
      @designcontest = response[:designcontest]
    end
    render json:response
  end

  def uae
    @resource = User.new
    clear_alexa_session
    session[:deeplink] = params if params[:destination_name].present?
    @invitation = Invitation.find(session[:invitation_id]) if session[:invitation_id]
    if (current_user rescue false)
      redirect_to dashboard_path and return
    end
    session[:uae] = true
    render :layout => 'home_page_uae_layout'
  end

  def app_download
    render layout: false
  end

  def nhs_login_failed
    @message = session[:nhs_message]
    render layout: false
  end

  def alexa_account_link
    render layout: false
  end

  def login
    clear_alexa_session
    render layout: false
  end
  
  def features
    render layout: 'home_page_uae_layout'
  end

  def uae_faq
    render layout: 'home_page_uae_layout'
  end
  
  def app_invite_fb
    render layout:false
  end

  def mobile_experience_popup
    render layout:false
  end

  def messages
    if params[:forgot_password] == "true"
      @message = Message::GLOBAL[:success][:forgot_password].html_safe #"You will receive an email with instructions about how to reset your password in a few minutes.<br/><a href='/users/sign_in' class='btn btn-type2 login_btn'>Login</a>"
    elsif params[:status] == "unapproved"
      @message = "Kindly verify your email ID. Please check the email we sent to you.<br/><a href='/resend_confirmation_link?email=#{session[:resend_verify_email]}' class='btn btn-type2 login_btn'>Resend Email</a>".html_safe
    elsif params[:verify_email_sent] == "true"
      @message = "Please verify your email ID through the link we have emailed to you. Do check your spam folder, if you haven't received the email.<br/><a href='/users/sign_in' class='btn btn-type2 login_btn'>Login</a>&nbsp;&nbsp;&nbsp;<a href='/resend_confirmation_link?email=#{session[:resend_verify_email]}' class='btn resend-email-link' style='vertical-align:sub' >Resend Email</a>".html_safe
    elsif params[:email_resent] == "true"
      @message = "We have resent the verification email to you. Do check your spam folder, if you haven't received the email.<br/><a href='/users/sign_in' class='btn btn-type2 login_btn' >Login</a>&nbsp;&nbsp;&nbsp;<a href='/resend_confirmation_link?email=#{session[:resend_verify_email]}' class='resend-email-link btn' style='vertical-align:sub'>Resend Email</a>".html_safe
    elsif params[:verification_link_expired].to_s == "true"
      @message = "This link is not active, as it may have been clicked already.<br/>".html_safe
    elsif params[:nhs_login_status] == "failed"
      @message = session[:nhs_message] || ::Message::NhsLoginFailed.html_safe
    elsif params[:orion_login_status] == "failed"
      @message = ::Message::API[:error][:orion_login_message]
    end
    render layout: "login"  
    # layout "login"
  end  

  def unsubscribe
    @member = Member.where(id:params[:sub_id]).first || current_user
    mail_setting = MemberSetting.where(member_id:@member.id, name:"mail_subscribe").first
    mail_setting = mail_setting || MemberSetting.create(member_id:@member.id, name:"mail_subscribe",note:MemberSetting::MailerType.keys.join(","),status: "subscribe")
    @subscribe_mail_ids = mail_setting.note.split(",")
    if params[:resubscribe] == "true"
      subscription_id = (@subscribe_mail_ids + [params[:mtype]]).uniq
      setting_val = true
    else
      setting_value = false
      subscription_id = @subscribe_mail_ids - [params[:mtype]]
    end
    @member.subscribe_mail(subscription_id)
    respond_to do |format|
      format.html { render layout: "login"  }
      format.js
      format.json { render :json=>{:status=>status, mailer_setting: setting_val, :message=>Message::GLOBAL[:success][:unsubscribe]}}
    end
  end
  
  def terms 
  end

  def privacy 
  end

  def team
  end

  def copyright
  end

  def cookies
  end  
  
  def faq
    render layout: "home"
  end

  def youtube_video
    render :partial=>"youtube_video", :layout=>false
  end
  def vimeo_video
    @video_link = params[:link]
    render :partial=>"vimeo_video", :layout=>false

  end
  def about
    @resource = User.new
  end

  def contact
    # @resource = User.new
    redirect_to "/" and return true

  end

  def get_geoname
    begin
    gids = params[:gid].split(",")
    all_data = {"totalResultsCount" => 0, "geonames" => []}
    gids.each do |gid|
      url = "http://www.geonames.org/childrenJSON?geonameId=#{gid}&style=long"
      data = JSON.parse(Net::HTTP.get_response(URI.parse(url)).body)
      all_data["totalResultsCount"] += (data["totalResultsCount"] rescue 0)
      all_data["geonames"] += (data["geonames"] rescue [])
    end
  rescue
    all_data
  end
    render :json => all_data
  end  

  # def enquiry
  #   @success = false
  #   @enquiry = Enquiry.new(params[:enquiry])
  #   if @enquiry.save
  #     @success = true
  #   end
  #   puts @enquiry.errors.to_a
  # end

  def popup_url
    render :partial=> params[:url], params[:family_member_id] => params[:family_member_id]
  end

  def accept_join_family_request
    @invitation = Invitation.find(params[:invitation_id])
    @invitation.accept
    @family_member = FamilyMember.new(:member_id => @invitation.member.id, :family_id => @invitation.family_id, :role => @invitation.role)
    if @family_member.save
      options = {:ip_address=>request.remote_ip}
      @family_member.update_family_member_data(options)
      @user = @invitation.family.owner.user
      if @user && @user.status == "unconfirmed"
        @user.status = "confirmed"
        @user.save
      end
      session[:api_token] = @user.confirmation_token if @user.present? # == current_user.user
      session[:api_invitation] = @invitation.id.to_s
      session[:api_email] = @user.email rescue nil
      session[:api_invitation_status] = "active"
      add_notification(@invitation.member, "Your request to join #{@invitation.family.name} has been #{@invitation.status} by #{@invitation.family.owner.first_name}.", families_path, @invitation.family.owner, "family")
    end
    respond_to do |format|
      sign_out if user_signed_in? && @invitation.family.owner != current_user
      format.html {redirect_to (current_user.present? ? dashboard_path : "/users/sign_in"), notice: "Invitation #{@invitation.status}"}
    end
  end

  def decline_join_family_request
    @invitation = Invitation.find(params[:invitation_id])
    @invitation.reject
    @user = @invitation.family.owner.user
    if @user && @user.status == "unconfirmed"
      @user.status = "confirmed"
      @user.save
    end
    session[:api_token] = @user.confirmation_token if @user.present? # == current_user.user
    session[:api_invitation] = @invitation.id.to_s
    session[:api_email] = @user.email rescue nil
    session[:api_invitation_status] = "active"
    add_notification(@invitation.member, "Your request to join #{@invitation.family.name} has been #{@invitation.status} by #{@invitation.family.owner.first_name}.", families_path, @invitation.family.owner, "family")
    respond_to do |format|
      sign_out if user_signed_in? && @invitation.family.owner != current_user
      format.html {redirect_to (current_user.present? ? dashboard_path : "/users/sign_in"), notice: "Invitation #{@invitation.status}"}
    end
  end  


  def notification
    notification = Notification.find(params[:id])
    notification.update_attribute(:checked, true) unless notification.blank?
    redirect_to notification.blank? ? root_path : notification.link
  end
private 
  def clear_alexa_session
    session[:alexa_source_reference] = nil
    session[:alexa_redirect_uri] = nil
    session[:alexa_flow_status] = nil
    end
end
