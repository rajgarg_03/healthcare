class InvitationsController < ApplicationController

  # Accept request from web in manage family section
  def accept
  	@invitation = Invitation.find(params[:invitation_id])
  	#unless @invitation.accepted?
      @invitation.accept 
      find_sender_and_receiver_for_invitation
      count = @owner_requested_user ? 1 : 0
    	@family_member = FamilyMember.find_or_initialize_by(:family_user_count => count, :member_id => @invitation.member.id, :family_id => @invitation.family_id, :role => @invitation.role)
      if @family_member.save
        options = {:ip_address=>request.remote_ip}
        @family_member.update_family_member_data(options)
        message ="Your #{@owner_requested_user ? 'invitation' : 'request'} to join #{@invitation.family.name} has been #{@invitation.status} by #{@receiver.first_name || @receiver.email}."
        if @owner_requested_user
          member = @owner_requested_user ? current_user : @receiver
          UserCommunication::UserActions.new.invited_user_joins_family(member,@invitation)
        else
           UserCommunication::UserActions.new.admin_approves_family_join_request(@invitation.member,@invitation)
        end
        add_notification(@sender, message, families_path, current_user, "family")
        api_message = Message::API[:success][:accept_invitation]
      end
    #end
    respond_to do |format| 
      format.html
      format.js
      format.json { render :json=> {:member=> @family_member, :invitation=> @invitation, message: api_message, status: StatusCode::Status200}  }
    end    
  end	
  
  # Decline request from web in manage family section
  def decline
  	@invitation = Invitation.find(params[:invitation_id])
  	unless @invitation.rejected?
      @invitation.reject
      find_sender_and_receiver_for_invitation
      message = "Your #{@owner_requested_user ? 'invitation' : 'request'} to join #{@invitation.family.name} has been #{@invitation.status} by #{@receiver.first_name || @receiver.email}."
      add_notification(@sender,message , families_path, current_user,"family")
      api_message = Message::API[:success][:decline_invitation]
    end
    respond_to do |format| 
      format.html
      format.js
      format.json { render :json=> {:invitation=> @invitation , message: api_message,  status: StatusCode::Status200}  }
    end    
  end	

  def new
  	@past_invitations = current_user.sent_invitations.order_by("created_at DESC").paginate(:page => (params[:page]), :per_page => 4)
  end	

  def edit_email
  	@member = ParentMember.new(email: params[:email])
  	render layout: false
  end

  def change_email
  	@member = ParentMember.new(email: params[:Invitation][:email])
  end	

  def add_invitee
  	@member = ParentMember.new(email: params[:Invitation][:email])
  end

  def send_invites
  	@invitations = []
  	params[:emails].each do |email|
  	  @elder_member = ParentMember.where(:email => email).first || ParentMember.new(:email => email)
  	  if @elder_member.persisted? || @elder_member.save
  	  	invitation = Invitation.where(member_id: @elder_member.id, status: Invitation::STATUS[:invited]).first
  	  	if invitation.blank?
          invitation = @elder_member.invitations.new(sender_id: current_user.id, sender_type: 'Member')
          invitation.status =  Invitation::STATUS[:invited]
          invitation.save          
          @invitations << invitation
        end
        @message = Message::GLOBAL[:success][:send_invites] #"Thanks, email invitations have been sent to your friends."
        UserMailer.invite_friend_email(invitation,params[:invite_email_text]).deliver
  	  end	
  	end
    respond_to do |format| 
      format.html
      format.js
      format.json { render :json=> {:member=>  @elder_member,:invitations=>@invitations,:message=>@message  , status: StatusCode::Status200}  }
    end
  end	

  def remove
  end

  def view_invite
  	render layout: false
  end	

   # Logs event for referral 
  def invite_friend_to_nurturey
    format.json { render :json=> {status:200}  }
  end
  

  private

  def find_sender_and_receiver_for_invitation
    @sender = @invitation.sender_id.present? ? @invitation.sender : @invitation.member
    @receiver = @invitation.sender_id.present? ? @invitation.member : @invitation.family.owner
    @owner_requested_user = @invitation.sender_id.present?    
  end  

end	