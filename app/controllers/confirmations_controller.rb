class ConfirmationsController < Devise::ConfirmationsController

# confirmation link verification
  def show
    user = User.find_by_verification_token(params[:confirmation_token]) 
    options = {:ip_address=>request.remote_ip}
    if user
      session[:mobile_verification] = true
      user.verify # update user email status confirmed
      session[:tour_step] = 0 if user.sign_in_count <= 1
      sign_in(:user, user)
      # sign_in(user,:bypass => true)
      member.update_family_and_user_segment(nil,options) rescue nil
      if params[:alexa_flow_status].to_s == "true"
        redirect_to "/alexa_account_link" 
      else
        redirect_to "/" and return true
      end
    else
      (UserMailer.delay.notify_dev_for_verification_email(params[:confirmation_token],options) rescue nil)
      redirect_to "/messages?verification_link_expired=true" and return true
    end
  end
end