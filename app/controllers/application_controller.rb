class ApplicationController < ActionController::Base
  
  protect_from_forgery
  before_filter :authenticate_user!, except:[:routing_error], if: :api_request_with_token
  before_filter :log_request,except:[:routing_error]
  before_filter :check_verified_session, :check_parent,except:[:routing_error] #, :require_ssl
  # before_filter :check_notifications
  # http_basic_authenticate_with name: "admin", password: "w!nt3r(00l" if !Rails.env.production?
  rescue_from Exception, with: :show_exception 
  # rescue_from Mongoid::Errors::DocumentNotFound, with: :show_exception
  # alias_method :devise_current_user, :current_user
  before_filter :redirect_if_user_has_deeplink,except:[:routing_error]
  around_filter :set_time_zone,except:[:routing_error]
   
  def authorization_status(current_user)
    status = true

    if params[:member_id].present? || params[:family_id].present? || params[:pregnancy_id].present?
      family_ids = FamilyMember.where(member_id:current_user).pluck(:family_id).map(&:to_s)
      member_ids = FamilyMember.where(:family_id.in=>family_ids).pluck(:member_id).map(&:to_s)
      pregnnacy_ids = Pregnancy.where(:expected_member_id.in=>member_ids).pluck(:id).map(&:to_s)
       
      if (params[:family_id].present? && params[:family_id].to_s.length > 10 ) && !family_ids.include?(params[:family_id])
        status =  false 
      elsif params[:member_id].present? && !member_ids.include?(params[:member_id]) && current_user.id.to_s != params[:member_id]
        status =  false 
      elsif params[:pregnancy_id].present? && !pregnnacy_ids.include?(params[:pregnancy_id])
        status = false
      end
    end
    # check if user present? Fix for android
    if status == false && params[:member_id].present?
      status = (User.find(params[:member_id]).present?  rescue false)
    end
    status 
  end

  def api_request_with_token
    params[:token].blank? && request.headers["HTTP_TOKEN"].blank?
  end
  
  def devise_current_user
     @devise_current_user ||= warden.authenticate(:scope => :user)
  end

  def current_user
    api_controller_action = params[:controller].split("/")[1]
    @apiVersion =  api_controller_action == "v4" ? 4 : (api_controller_action.gsub("v","").to_i rescue 1)
    Rails.logger.info "Api version is(Application user)...............................................#{@apiVersion}"
    Rails.logger.info "Token is (Application user)............................................ #{request.headers["HTTP_TOKEN"]}"
    if @apiVersion >= 6
      token = request.headers["HTTP_TOKEN"]
      refresh_token = params[:refresh_token]||request.headers["HTTP_REFRESHTOKEN"]
      params[:refresh_token] = refresh_token
    else
      token = params[:token]
      refresh_token = params[:refresh_token]|| params[:authentication_token] || params[:auth_token] 
    end
    Rails.logger.info "Key is(Application user)............................................ #{refresh_token}"

    if token.present?

     user = User.find_user_with_token(token,refresh_token,@apiVersion).first
      if user.blank?
        if ["unread_count","user_info", "user_sign_out","enable_push_notification","update_subscription_receipt"].include?(params[:action])
          Rails.logger.info "Not supported token"
          raise "Session expired"
        elsif ["mark_user_notification_responded"].include?( params[:action])
          params[:refresh_token] = token

        else
          #Rails.logger.info "Unauthorized for action #{params.inspsct}"
          raise "Invalid token" 
        end
      end
      raise "Account blocked by admin" if user.get_account_status == "Blocked"
      raise "Unauthorized access" if !authorization_status(user.member)
      params[:token] = token
      member = user.member
    else
      member = devise_current_user.member if devise_current_user
    end
    Rails.logger.info "Member is(Application user)............................................ #{member.id}" rescue nil

    member
  end
  
  # log all incoming request in DB
  def log_request
    begin
      api_controller_action = params[:controller].split("/")[1]
      @apiVersion =  api_controller_action == "v4" ? 4 : (api_controller_action.gsub("v","").to_i rescue 1)
      if @apiVersion >= 6
        token = request.headers["HTTP_TOKEN"]
        refresh_token = request.headers["HTTP_REFRESHTOKEN"] || params[:refresh_token]
        params[:refresh_token] = refresh_token
      else
        token = params[:token]
        refresh_token = params[:refresh_token]|| params[:authentication_token] || params[:auth_token] 
        # params[:refresh_token] = refresh_token
      end
      Rails.logger.info "Key is(Log Request)........................ ....   #{refresh_token}"
      Rails.logger.info "token is(Log Request) ..........................   #{token}"
   
      options = {:ip_address=>request.remote_ip, :request_type=>request.method,:request_format=>(request.format.symbol.to_s rescue nil)}
      member = current_user || (User.or({confirmation_token: refresh_token}, {api_access_token:token},{email:params[:email]}).last.member rescue nil)
      #log event info
      report_event = Report::Event.get_event_data(params[:controller], params[:action]) rescue nil
      member_id = Report::ApiLog.get_key_value(params,"member_id")
      pregnancy_id = Report::ApiLog.get_key_value(params,"pregnancy_id") ||  Report::ApiLog.get_key_value(params,"preg_id")
      Rails.logger.info ".............................Checking for preganacy id"
      Rails.logger.info     params.inspect 
      Rails.logger.info     pregnancy_id 
      Rails.logger.info "............................."
      options[:member_id] = member_id
      options[:pregnancy_id] = pregnancy_id
      options[:family_id] = Report::ApiLog.get_key_value(params,"family_id")
      if member.blank?
        Report::ApiLog.create_record(email=nil,params,options) 
        Report::ApiLog.log_event(user_id=nil,family_id=nil,report_event.custom_id,options)  if report_event.present?
      else
        options[:current_user] = member
        member.delay.log_api(params,options) rescue nil
        member.delay.log_event(family_id=nil,report_event.custom_id,options) if report_event.present?
      end
    rescue Exception =>e 
      Rails.logger.info "Application Request log is failing....................."
      Rails.logger.info e.message
    end
    Mongoid.override_database(APP_CONFIG["database_name"])
  end

  def redirect_if_user_has_deeplink
    begin
      if params[:source] == "email_deeplink"
        options = params
        options[:platform] = "web"
        options[:response_type] = "email"
        user =  current_user.user
        sign_in(:user, user)
        params_data =  params.deep_dup
        member_id = params[:member_id]
        UserNotification::ClientResponse.save_client_response(current_user,params_data) rescue nil
        redirect_url = "/"

        if params[:destination_name] == "Manage Tools"
          family = Family.find(params[:family_id])
          if member_id.blank?
            # redirect to manage family if no child in family in web
            redirect_url = "/families"
            
            # For app check pregnancy manage tool
            member_ids =  FamilyMember.where(family_id:family.id.to_s,:role.in=>FamilyMember::CHILD_ROLES).pluck(:member_id).map(&:to_s)
            expected_member = Member.where(:id.in=>member_ids).order_by("birth_date desc").pluck(:id)
            member_id = expected_member[0] rescue nil
            # No child/preganancy deeplink to manage family
            params[:destination_name] = "Manage Family" if member_id.blank?
            params[:member_id] = member_id

          else
            redirect_url = Rails.application.routes.url_helpers.family_member_nutrients_path("family", member_id)
          end
        elsif params[:destination_name] == "alexa_skill"
          @app_store = true # don't open nurturey app
          redirect_url = "alexa://alexa?fragment=skills/dp/B078TMBZJV"
        elsif params[:destination_name] == "Pointers" 
          redirect_url = "/app_download"
        elsif params[:destination_name] == "Tooth Chart"
          redirect_url = "/app_download"
        elsif params[:destination_name] == "Z score"
          redirect_url = "/app_download"
        elsif params[:destination_name] == "Vision Health"
          redirect_url = "/app_download"
        elsif params[:destination_name] == "Nhs Library"
          redirect_url = "/app_download"
        elsif params[:destination_name] == "premium_subscription"
          redirect_url = "/app_download"
        elsif params[:destination_name] == "Manage GP"
          redirect_url = "/app_download"
        elsif params[:destination_name] == "Mental Maths"
          browser_agent = request.headers['User-Agent'] 
          if params[:notification_type] == "32"
            redirect_url = "/app_download"
          elsif browser_agent.match(/Android/i).present?
            redirect_url = "https://play.google.com/store/apps/details?id=com.nurturey.app"
            @app_store = true
          elsif browser_agent.match(/iPhone|iPad|iPod/i).present?
            redirect_url = "https://itunes.apple.com/app/nurturey/id1023121839?ls=1&mt=8"
            @app_store = true
          else
            redirect_url = "/app_download"
          end
         
        elsif params[:destination_name] == "Timeline"
          # To redirct to child Timeline
          tool = Nutrient.where(identifier:"Timeline").last
          family_member = FamilyMember.where(:member_id=>params[:member_id]).last
          family = Family.new(:name=>"MyFamily")
          member = family_member.member
          redirect_url = eval(Nutrient::NUTRIENT_PATH[tool.title.downcase.tr(" ","_").to_sym])# rescue "/"
        elsif params[:destination_name] == "Measurements"
          # To redirct to child Timeline
          tool = Nutrient.where(identifier:"Measurements").last
          family_member = FamilyMember.where(:member_id=>params[:member_id]).last
          family = Family.new(:name=>"MyFamily")
          member = family_member.member
          redirect_url = eval(Nutrient::NUTRIENT_PATH[tool.title.downcase.tr(" ","_").to_sym])# rescue "/"
        elsif params[:destination_name] == "Milestones"
          # To redirct to child Milestones
          tool = Nutrient.where(identifier:"Milestones").last
          family_member = FamilyMember.where(:member_id=>params[:member_id]).last
          family = Family.new(:name=>"MyFamily")
          member = family_member.member
          redirect_url = eval(Nutrient::NUTRIENT_PATH[tool.title.downcase.tr(" ","_").to_sym])# rescue "/"
         
          # nutrient_link(tool,family_member)
        elsif params[:destination_name] == "Immunisations"
          # To redirct to child Immunisations
          
          tool = Nutrient.where(identifier:"Immunisations").last
          family_member = FamilyMember.where(:member_id=>params[:member_id]).last
          family = Family.new(:name=>"MyFamily")
          member = family_member.member
          redirect_url = eval(Nutrient::NUTRIENT_PATH[tool.title.downcase.tr(" ","_").to_sym])# rescue "/"
           
        elsif params[:destination_name] == "Manage Family"
          redirect_url = "/families"
        end
        session[:deeplink] = params || session[:deeplink] 
        @redirect_to = (redirect_url)
        if @app_store == true
           redirect_to @redirect_to and return  
        else
          render :partial=>"layouts/deeplink_page", :layout=>false and return 
        end
      end
    rescue Exception => e
      params[:source] = nil
      session[:deeplink] = params

      Rails.logger.info e.message
    end
  end

  

  def check_parent
    if params[:member_id].present? && params[:action] == "index" && params[:controller] != "home"
      member_ids = Member.where(id:params[:member_id]).first.family_elder_members
      unless member_ids.include?(current_user.id) 
        redirect_to "/"
      end
    end
  end


  def after_sign_in_path_for(resource)
    return dashboard_path
  end

  def toDate(date=nil)
    begin
      date.strftime("%d-%m-%Y")
    rescue
     Date.today.strftime("%d-%m-%Y")
    end
  end

  def toDate1(date=nil)
    begin
      date.strftime("%d %b %Y")
    rescue
     ""
    end
  end
   
def routing_error(error = 'Routing error', status = :not_found, exception=nil)
    status = StatusCode::Status100
    message = ""
    screen_name = ""
    # render_exception(404, "Routing Error", exception)
    respond_to do |format| 
      format.json{ render :json=> {:status=> 404,:screen_name=>screen_name,:message=>message} and return true }
      format.html{ render file: "#{Rails.root}/public/404", layout:false}
    end
end
  def info_popup_status(member,tool_name)
    tool_info = member.member_settings.where(name:"display_#{tool_name}_info").first || member.member_settings.create(name:"display_#{tool_name}_info", status:"true")
    if tool_info.status != "false"
      eval("@display_#{tool_name}_info = true")
      tool_info.update_attribute(:status, "false")
    end
  end

  def add_notification(member, title, link, sender, type="family")
    member.notifications.create(:title => title, :link => link, sender: sender.id, type:type)
  end

  def decode_and_save_image(record_obj,image_data)
    member = record_obj.member    
    content = image_data
    content.gsub!('\\r', "\r")
    content.gsub!('\\n', "\n")
    decode_base64_content = Base64.decode64(content) 
    File.open("public/pictures/#{record_obj.class.name.downcase}_#{record_obj.id}.jpg", "wb") do |f|
      f.write(decode_base64_content)
    end
    file = File.open("public/pictures/#{record_obj.class.name.downcase}_#{record_obj.id}.jpg")
    picture = Picture.new(local_image:file,upload_class: record_obj.class.name.downcase,member_id:member.id)
    if picture.valid? && record_obj.pictures << picture
      @picture_saved = true        
    else
      @picture_saved = false
    end
    File.delete("public/pictures/#{record_obj.class.name.downcase}_#{record_obj.id}.jpg")
  end

  private

  def check_verified_session
    api_controller_action = params[:controller].split("/")[1]
    @apiVersion =  api_controller_action == "v4" ? 4 : (api_controller_action.gsub("v","").to_i rescue 1)
    if @apiVersion >= 2 && params[:allow_access_to_dashboard] != "true"
      if current_user && !User.can_access_dashboard_after_signup?(current_user.user,@apiVersion)
        session.clear
       respond_to do |format| 
         format.json{ render :json=> {:status=> 100,:screen_name=>"email_verification_screen",:message=>Message::DEVISE[:m1]} and return true }
       end
      end
       # redirect_to root_path
    end
  end 

  protected
  
  def require_ssl
    # redirect_to :protocol => "https://" unless (request.ssl? or local_request?)  
  end
  def basic_auth
    unless (current_user.email == "admin@nurturey.com")
      redirect_to root_path
    end
  end

  def admin_basic_auth
    unless current_user.user.can_access_admin_panel?
      redirect_to root_path
    end
  end

  protected
    def show_exception(exception)
      if exception.message == "Invalid token" || exception.message == "Session expired"
        status = StatusCode::Status401
        message = ::Message::DEVISE[:m22]
        screen_name = "login_screen"
        http_status = 401
      elsif exception.message == "Account blocked by admin"
        status = StatusCode::Status401
        message = ::Message::DEVISE[:m23]
        screen_name = "login_screen"
        http_status = 200
      else
        status = StatusCode::Status100
        message = ""
        screen_name = ""
        http_status = 200
      end
      respond_to do |format|
        format.json {render status:http_status, json: {status: 404, screen_name:screen_name ,message:message, error:exception.message}}
        format.html
        format.js
      end
    end
  private
   
    def render_error(status, exception)
      Rails.logger.error status.to_s + " " + exception.message.to_s
      Rails.logger.error exception.backtrace.join("\n") 
      respond_to do |format|
        format.html { render template: "errors/error_#{status}",status: status }
        format.all { render nothing: true, status: status }
      end
    end
  def validate_email_status
    # check if user email is confirmed or approved 
    if  !current_user.user.email_verification_status
      respond_to do |format| 
        format.json{ render :json=> {:status=> 100,:screen_name=>"email_verification_screen",:message=>Message::DEVISE[:m18]} and return true }
      end
    end
  end

  def set_time_zone
    begin
      if current_user
        if current_user.time_zone.present?
          @time_zone = current_user.time_zone
        elsif cookies && cookies[:browser_timezone].present?
          @time_zone = cookies[:browser_timezone]
          @time_zone = "Kolkata" if cookies[:browser_timezone] == "ASIA/KOLKATA"
        end
        begin
          Time.zone = @time_zone 
        rescue Exception=>e 
          @time_zone = "Kolkata"
        end
        current_user.update_attribute(:time_zone,@time_zone) 
        time_zone = current_user.time_zone
      else
        time_zone =  "Kolkata"
      end
    rescue
      time_zone =  "Kolkata"
    end
    Time.use_zone(time_zone) { yield } 
  
  end  
end
