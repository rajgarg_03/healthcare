 class Api::V6::ClinicController < ApiBaseController
  before_filter :set_api_version
  before_filter :check_clinic_account_active
  before_filter :validate_user_gpsoc_limit
  
  #/api/v5/clinic/setup_clinic
  #params = {:clinic_detail=> {:name=>"EMIS",:uid=>"JHUS3AX",:address=>"UK nsd",:postcode=>"BR5 QAA"},:organization_type=>"emis",:family_id=>'5cc9448bf9a2f32c330000c1', :organization_uid=>'UYT6HGYA',:member_id=>'5c3c988b8cd03d370a000028'}
  #params = {:clinic_detail=> {:ods_code=>'A28827', :name=>"EMIS",:uid=>"JHUS3AX",:address=>"UK nsd",:postcode=>"BR5 QAA"},:organization_type=>"emis/tpp",:family_id=>'5ccab494f9a2f3ab37000315', :organization_uid=>'JHUS3AX',:member_id=>'5ccab4aff9a2f3ab37000323'}
  def setup_clinic
    begin
      options = {}
      options[:type] = params[:organization_type]
      options[:current_user] = current_user
      options[:family_ids] = [params[:family_id]].flatten if params[:family_id].present?
      organization_uid = params[:organization_uid]
      options[:organization_type] = params[:organization_type]
      options[:ods_code] = params[:ods_code]
      options[:organization_id] = organization_uid
      options[:clinic_detail] = params[:clinic_detail] if params[:clinic_detail].present?
      member_id = params[:member_id]
      @message = Message::API[:success][:setup_clinic]
      status = StatusCode::Status200
      ::Clinic::Organization.set_clinic(member_id,organization_uid,options)
    rescue Exception => e
        status = StatusCode::Status100
        @error = e.message
        @message = Message::API[:error][:setup_clinic]

    end
    render :json=>{:error=>@error,:message=>@message,:status=>status}    
  end
  
  def get_clinic_list
    begin
      options = {}
      member_id = params[:member_id]
      clinic_list = ::Clinic::Organization.get_clinic_list(current_user,member_id,options)
      status = StatusCode::Status200
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
        status = StatusCode::Status100
        @error = e.message
    end
    render :json=>{:reauthentication_required=>@reauthentication_required,:error=>@error,:status=>status,:clinic_list=>clinic_list}  
  end

  #  Get clinic list for UAE
  #  Get /api/v5/clinic/get_uae_clinic_list.json?
  #  params = {:member_id=>""}
  def get_uae_clinic_list
    begin
      options = {}
      options[:clinic_with_drive_through] = params[:drive_through].to_s == "true"
      member_id = params[:member_id]
      clinic_list = ::Clinic::Seha::Organization.get_clinic_list(current_user,member_id,options)
      status = StatusCode::Status200
      @reauthentication_required = false #current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
        status = StatusCode::Status100
        @error = e.message
    end
    render :json=>{:reauthentication_required=>@reauthentication_required,:error=>@error,:status=>status,:clinic_list=>clinic_list}  
  end

  # post
  #/api/v6/clinic/authenticate_by_clinic.json
  # smirthi clinic_link_detail = {:organization_uid=>'WKI72JH', :login_id=>"2397", :password=>"6dFKQWWA+2rZzDm/0pwPFg=="}
  # EMIS - pin doc:
  # clinic_link_detail = {"first_name"=> "","account_id"=>"2325028826","account_linkage_key"=>"TcdbmCgdUANr7","practice_ods_code"=>"A28826","surname"=>"","date_of_birth"=>"1993-04-08",:register_with_pindocument=>true} # emis - register pin document
  # EMIS - without pin doc: 
  # clinic_link_detail = {"first_name"=> "" ,practice_ods_code"=>"A28826", "surname"=>"KUSHWAHA", "date_of_birth"=>"1993-04-08", "gender"=>"M", "house_name_no"=>"9", "postcode"=>"BR5 1QB", "email"=>"ronit1.garg@gmail.com", "mobile_no"=>"",:register_with_pindocument=>false} 
  # Orion - 
  # clinic_link_detail = {"first_name"=> "" ,"practice_ods_code"=>"orion", "surname"=>"KUSHWAHA", "date_of_birth"=>"1993-04-08", "email"=>"ronit.garg@gmail.com", "patient_id"=>"AAA-233-HUY12",:register_with_pindocument=>true} 
  # params = {member_id=>"",:clinic_link_detail=>clinic_link_detail,:is_proxy_selected=>false/true, :proxy_user_id=>nil}
  def authenticate_by_clinic
    begin
      options = {}
      if params[:clinic_link_detail][:register_with_pindocument].present?
        options[:register_with_pindocument] = params[:clinic_link_detail][:register_with_pindocument].to_s == "true" rescue false
      end
      clinic_link_detail = params[:clinic_link_detail] || {}
      if !clinic_link_detail["organization_uid"].nil? && clinic_link_detail["organization_uid"].to_i == 0 && clinic_link_detail["practice_ods_code"].blank? 
        clinic_link_detail["practice_ods_code"]  = clinic_link_detail["organization_uid"]
      end
      options[:current_user] = current_user
      options[:is_proxy_selected] = params[:is_proxy_selected].to_s == "true"
      options[:proxy_user_id] = params[:proxy_user_id]
      member_id = params[:member_id] || current_user.id
      member  = Member.find(member_id)
      if clinic_link_detail["practice_ods_code"] == "orion"
        member.update_attributes({:ods_code=>'orion',:organization_uid=>'orion'})
        member.reload
      end
      # authenticate_status = Clinic::Organization.get_authentication_from_clinic(current_user,member_id,clinic_link_detail,options)
      authenticate_status,msg = Clinic::ClinicDetail.get_authentication_from_clinic(current_user,member_id,clinic_link_detail,options)
      if  authenticate_status == "authenticated"
        member = Member.find(member_id)
        user_details = ::Clinic::User.get_user_detail(current_user,member_id,options)
      else
        user_details = {}
      end
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
      
      user_action = ::Clinic::LinkDetail.get_operation_list(member,options)
      clinic_linkdetail = ::Clinic::LinkDetail.get_clinic_link_detail(member_id,current_user,options) || ::Clinic::LinkDetail.new
      two_factor_auth_required = clinic_linkdetail.two_factor_auth_required? rescue false
      status = StatusCode::Status200
      if authenticate_status == "authenticated"
        @message = ::Message::Clinic[:user_authenticated]
      else
        @message = msg
        status = StatusCode::Status100
      end
    rescue Exception => e
        Rails.logger.info "............Error inside authenticate By Clinic .............."
        Rails.logger.info e.message
        status = StatusCode::Status100
        @message = ::Message::Clinic[:user_authentication_failed] 
        @error = e.message
    end
    render :json=>{user_details:user_details, :email=>clinic_linkdetail.email, :phone_number=>clinic_linkdetail.phone_number,:show_two_factor_auth=>two_factor_auth_required, :reauthentication_required=>@reauthentication_required,:error=>@error,:message=>@message,:status=>status,:authenticate_status=>authenticate_status,:user_action=>user_action}  
  end
  
  #Post
  #/api/v5/clinic/link_proxy_user.json?
  # params = {:member_id=>"5e2817538cd03d992800004b",:patient_uid=>"ddec6b3e-a4d3-4121-bb16-69fb4d88942e",practice_ods_code:''}
  def link_proxy_user
     begin
      options = {}
      member_id = params[:member_id]
      if params[:practice_ods_code] == "A28826"
        ods_code = ::Clinic::ClinicDetail.where(organization_uid:current_user.organization_uid).last.ods_code
      else
        ods_code = params[:practice_ods_code] #== "A28826"
      end
      patient_uid =  params[:patient_uid]
      member  = Member.find(member_id)
      authenticate_status,msg = ::Clinic::LinkDetail.link_proxy_user(current_user,member_id,ods_code,patient_uid, options)
     
      # begin
      #   Clinic::ClinicDetail.setup_member_clinic_for_proxy_linking(current_user,member_id,ods_code,options)
      # rescue Exception=>e 
      #   return {status:100, message:e.message}
      # end
      # # Get parent clinic link detail
      # user_clinic_link_detail = ::Clinic::LinkDetail.get_clinic_link_detail(current_user.id,current_user,options)
      # clinic_link_detail =  user_clinic_link_detail.link_info
      # clinic_link_detail[:patient_context_guid] = params[:patient_uid]
      # clinic_link_detail = clinic_link_detail.with_indifferent_access
      
      # options[:is_proxy_selected] = true
      # options[:register_with_pindocument] = true # only pin document registred user can link proxy user
      # options[:current_user] = current_user
      # options[:proxy_user_id] = current_user.id
      # options[:proxy_user_link_flow] = true
     
      # authenticate_status,msg = Clinic::ClinicDetail.get_authentication_from_clinic(current_user,member_id,clinic_link_detail,options)
      if  authenticate_status == "authenticated"
        member = Member.find(member_id)
        Rails.logger.info 'User get authenticated'
        user_details = ::Clinic::User.get_user_detail(current_user,member_id,options)
        Rails.logger.info "User details fetched #{user_details}"
      else
        user_details = {}
      end
      member = Member.find(member_id)

      # user_details = ::Clinic::User.get_user_detail(current_user,member_id,options)
      @reauthentication_required = current_user.reauthentication_required?(current_user,member_id,options)
      
      user_action = ::Clinic::LinkDetail.get_operation_list(member,options)
      link_detail = ::Clinic::LinkDetail.get_clinic_link_detail(member_id,current_user,options) || ::Clinic::LinkDetail.new
      Rails.logger.info "Clinic link details id #{link_detail.id}"
      
      two_factor_auth_required = link_detail.two_factor_auth_required? rescue false
      status = StatusCode::Status200
      if authenticate_status == "authenticated"
        @message = ::Message::Clinic[:user_authenticated]
      else
        @message = msg
        status = StatusCode::Status100
      end
    rescue Exception => e
        Rails.logger.info "............Error inside Link proxy user .............."
        Rails.logger.info e.message
        status = StatusCode::Status100
        @message = ::Message::Clinic[:user_authentication_failed]
        @error = e.message
    end

    render :json=>{user_details:user_details, :email=>link_detail.email, :phone_number=>link_detail.phone_number,:show_two_factor_auth=>two_factor_auth_required, :reauthentication_required=>@reauthentication_required,:error=>@error,:message=>@message,:status=>status,:authenticate_status=>authenticate_status,:user_action=>user_action}  
    
  end



  #Post
  #/api/v5/clinic/sent_otp_for_clinic_access.json?
  # params = {:member_id=>"",method=>"email/phone"}
  def sent_otp_for_clinic_access
    member = Member.find(params[:member_id])
    options = {:current_user=>current_user,:api_version=>@api_version,:otp_receiving_source=>(params[:method]||params[:verification_method])}
    status = Clinic::UserOtp.generate_otp(member.id,current_user,options)
    status_code = status == true ? StatusCode::Status200 :  StatusCode::Status100
    render :json=>{:status=> status_code}
  end


  #Post
  #/api/v6/clinic/verify_otp_for_clinic_access.json?
  # params = {:member_id=>"", :otp=>'826663',:verification_method=>"email/phone"}
  def verify_otp_for_clinic_access
    member = Member.find(params[:member_id])
    member_id = member.id
    options = {:current_user=>current_user,:api_version=>@api_version}
    otp = params[:otp]
    status = Clinic::UserOtp.verify_otp(member.id,current_user,otp,options)
    user_action = ::Clinic::LinkDetail.get_operation_list(member,options)
    full_access,access_level, msg,action_identifier,access_reason = ::Clinic::User.get_access_level(current_user, member_id, options)

    
    if status == true
      status = StatusCode::Status200
    else
      status =StatusCode::Status100
      @msg = ::Message::Clinic[:incorrect_otp] #  'OTP entered is incorrect. Please enter correct OTP to proceed.'
    end
    render :json=>{:access_reason=>access_reason, :status=> status,:message=>@msg,:user_action=>user_action,:action_identifier=>action_identifier}

  end

  def set_api_version
    @api_version = 6
  end
  
  # Get /api/v6/clinic/get_clinic_linkable_status.json?token=
  # params = {:organization_uid}
  def get_clinic_linkable_status
    begin
      linkable = false
      options = {}
      clinic_detail = ::Clinic::ClinicDetail.find_by_code_or_organization_id(current_user.ods_code,params[:organization_uid],options)
      linkable = clinic_detail.is_linkable?(current_user,options)
      status = StatusCode::Status200
    rescue Exception => e
      status = StatusCode::Status100
      @error = e.message
    end
    render :json=>{:error=>@error,:status=>status,:linkable=>linkable}  
  end

  # Get /api/v5/clinic/get_clinic_services.json?token=
  # params = {:organization_uid,member_id=>}
  def get_clinic_services
    begin
      linkable = false
      member_id = params[:member_id]
      organization_uid = params[:organization_uid]
      member = Member.find(member_id)
      options = {:current_user=>current_user,:member_id=>member_id}
      clinic_detail = member.get_clinic_detail(current_user,options) || Clinic::ClinicDetail.new
      user_clinic = ::Clinic::User.get_clinic_user(member_id,current_user,options)
      
      clinic_link_detail = ::Clinic::LinkDetail.get_clinic_link_detail(member_id,current_user,options) || ::Clinic::LinkDetail.new
      # TODO: Add logic to add clinic if user linked but Clinic not saved in system 
      # if clinic_detail.blank? 
      #   if clinic_link_detail.present?
          
      #   else
      #     clinic_detail = Clinic::ClinicDetail.new
      #   end
      # end

      can_delink = clinic_detail.is_delinakable?(options)

      if user_clinic.present?
        clinic_services_status = user_clinic.get_services_for_user(options)
        user_clinic = ::Clinic::User.get_clinic_user(member_id,current_user,options)
      else
        clinic_services_status = {}
      end
      
      # Tab title list in Healthcard section
      health_card_action_list = clinic_detail.get_title_for_clinic_service_available(current_user,member,clinic_services_status,options)
 
      full_access,access_level, msg,action_identifier,access_reason = ::Clinic::User.get_access_level(current_user, member_id, options)
      options[:is_full_access] = full_access
      options[:access_level] = access_level
      options[:clinic_services_status] = clinic_services_status
      clinic_service_list,gp_data = clinic_detail.clinic_services(member,options)
      if access_reason.blank?
        access_reason = ::Message::Clinic[:service_disabled]
      end
      if user_clinic.access_level == 2
        two_factor_auth_required = false
      else
        two_factor_auth_required = clinic_link_detail.two_factor_auth_required? rescue false
      end
      status = StatusCode::Status200
      user_details = ::Clinic::User.get_user_detail(current_user,member_id,options)
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      status = StatusCode::Status100
      @error = e.message
    end
    render :json=>{health_report_action_list:health_card_action_list, gp_data:gp_data, user_details:user_details, access_reason:access_reason, action_identifier:action_identifier, access_level:access_level, :email=>clinic_link_detail.email, :phone_number=>clinic_link_detail.phone_number, :show_two_factor_auth=>two_factor_auth_required, :reauthentication_required=>@reauthentication_required,:error=>@error,:delinkable=>can_delink, :status=>status,:full_access=>full_access,:clinic_services_status=>clinic_services_status,:clinic_service_list=>clinic_service_list}  
  end

  # Put /api/v5/clinic/delink_with_clinic.json?token=
  # params = {:organization_uid,member_id=>}
  def delink_with_clinic
    begin
      options = {:current_user=>current_user}
      member_id = params[:member_id]
      organization_uid = params[:organization_uid]
      Clinic::Organization.delink_clinic(member_id,organization_uid,options)
      status = StatusCode::Status200
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      status = StatusCode::Status100
      @error = e.message
    end
    render :json=>{:error=>@error,:status=>status,:reauthentication_required=>@reauthentication_required}  
  end


  # Get /api/v5/clinic/get_clinical_data.json?token=
  # params = {:type=>"allergy"/immunisations", member_id=>}
  def get_clinical_data
    begin
      member_id = params[:member_id]
      member = Member.find(member_id)
      options = {:current_user => current_user}
      clinic_detail = member.get_clinic_detail(current_user,options)
      status = StatusCode::Status200
      item_type =  params[:type].titleize
      item_type = "TestResults" if item_type == "Investigations"
      options[:item_type] = item_type
      if clinic_detail.is_tpp?(options)
        item_type =  params[:type]
        options[:item_type] = item_type
        response = ::Clinic::Tpp::HealthCard.get_patient_health_record(member_id,options)
        response[:data] = response[:data].values.flatten.sort_by{|a| a[:updated_at].to_date}.reverse rescue []
      elsif clinic_detail.is_emis?(options)
        if item_type == "Allergy"
          response = Emis::Allergy.get_patient_allergies(member_id,options)
          response[:data] = response[:data].values.flatten.sort_by{|a| ["updated_at"]}.reverse rescue []
        elsif item_type == "Immunisations"
          response = Emis::Immunisations.get_patient_immunisations(member_id,options)
          response[:data] = response[:data].values.flatten.sort_by{|a| ["updated_at"]}.reverse rescue []
        elsif  ["Consultations","Medication","Problems","TestResults","Documents"].include?(item_type)
          response = ::Emis::HealthCard.get_patient_health_record(member_id,options)
          response[:data] = response[:data].values.flatten.sort_by{|a| ["updated_at"]}.reverse rescue []
        else
          response = {status:100,:error=>"Not support for #{param[:type]}"}
        end
      elsif clinic_detail.is_orion?(options)
        if item_type == "Immunisations"
          response = ::Clinic::Orion::Immunisations.get_patient_immunisations(member_id,options)
          response[:data] = response[:data].values.flatten.sort_by{|a| ["updated_at"]}.reverse rescue []
        else
          response = {status:100,:error=>"Not support for #{param[:type]}"}
        end
      else
        response = {status:100,:error=>"Not support for #{param[:type]}"}
      end
      if response[:status] == 200
        health_card_records = response[:data]
        category = item_type
        ::Clinic::HealthRecord.save_records(member,health_card_records,category,options)
      end
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
      response[:reauthentication_required] = @reauthentication_required
    rescue Exception => e
      status = StatusCode::Status100
      @error = e.message
    end
    render :json=>response
  end

end  