class Api::V6::Pregnancy::KickCounter::KickCountController < ApiBaseController
  before_filter :set_api_version
  
  #Get
  # /api/v4/pregnancy/kick_counter/kick_count.json?token=AuSnoCzp2uceDGA1JGqb&pregnancy_id=58df9c48f9a2f347fd000015&upcoming=true/false
  def index
    begin
      pregnancy_id = params[:pregnancy_id]
      pregnancy = Pregnancy.find(pregnancy_id)
      member = Member.find(pregnancy.expected_member_id)
      upcoming_records = (params[:upcoming].present? && params[:upcoming] == "true") ? true : false 

      options = {}
      options = options.merge({:get_data_for_from_list=>"upcoming"}) if upcoming_records
      @tool, @show_subscription_box_status,@info_data,all_kick_count = member.get_subscription_show_box_data(current_user,"Kick Counter",@api_version,options)
      if @show_subscription_box_status == false
        all_kick_count = Pregnancy::KickCounter::KickCountDetail.get_all_list(pregnancy,upcoming_records)
      end
      pregnancy_member_id = pregnancy.status == "completed" ? pregnancy.added_child.last : pregnancy.expected_member_id
      recommended_pointer = nil #ArticleRef.get_recommended_pointer(current_user,pregnancy_member_id,"pregnancy")
      avg_kick_count_text = Pregnancy::KickCounter::KickCountDetail.get_avg_kick_count(pregnancy, 2) unless upcoming_records
      kick_count_scheduler = Pregnancy::KickCounter::Scheduler.where(pregnancy_id:pregnancy_id).last rescue nil
      kick_schedule_status = kick_count_scheduler.get_schedular_staus rescue nil
      is_kick_counter_scheduled = kick_count_scheduler.present?
      kick_count_scheduler = kick_count_scheduler.in_json rescue nil

      is_kick_counting_started = Pregnancy::KickCounter::KickCountDetail.where(pregnancy_id: pregnancy_id).count > 0

      kick_count_scheduler.merge!(is_kick_counting_started: is_kick_counting_started) if kick_count_scheduler

      show_kick_count_schedule_screen = pregnancy.show_kick_count_schedule_screen
      pregnancy.update_attributes(show_kick_count_schedule_screen: false)
      avg_chart_data = Pregnancy::KickCounter::KickCountDetail.avg_chart_data
      chart_data = Pregnancy::KickCounter::KickCountDetail.chart_data(pregnancy)
      status = StatusCode::Status200
      chart_min_max_axis = Pregnancy::HealthCard.chart_x_y(chart_data+avg_chart_data+ [avg_chart_data[0][0], 0])
      graph_data = { graph_data: {
                                  chart_data:         chart_data, 
                                  avg_chart_data:     avg_chart_data,
                                  chart_min_max_axis: chart_min_max_axis 
                                  } }
      if upcoming_records
        render :json=> {:free_trial_end_date_message=>@info_data[:free_trial_end_date_message],:subscription_box_data=>@info_data[:subscription_box_data],:show_subscription_box=> @show_subscription_box_status, :system_tool_id=> @tool.id,:is_kick_counter_scheduled=> is_kick_counter_scheduled, :kick_count_scheduler => kick_count_scheduler,:kick_schedule_status=> kick_schedule_status, :status=>status,:upcoming_schedules=>all_kick_count, show_kick_count_schedule_screen: show_kick_count_schedule_screen}
      else
        render :json=> {:free_trial_end_date_message=>@info_data[:free_trial_end_date_message],:subscription_box_data=>@info_data[:subscription_box_data],:show_subscription_box=> @show_subscription_box_status, :system_tool_id=> @tool.id,:is_kick_counter_scheduled=> is_kick_counter_scheduled, :kick_count_scheduler => kick_count_scheduler, :kick_schedule_status=> kick_schedule_status,:recommended_pointers=>recommended_pointer,:status=>status,:kick_counters=>all_kick_count, show_kick_count_schedule_screen: show_kick_count_schedule_screen, avg_kick_count_text: avg_kick_count_text}.merge(graph_data)
      end
    rescue Exception=>e 
      status = StatusCode::Status100
      @error = e.message
      render :json=> {:error =>@error,status: status, error_detail:e.message}
    end
    
  end

  # Post
  # /api/v4/pregnancy/kick_counter/kick_count/add_kick_count.json?token=AuSnoCzp2uceDGA1JGqb&pregnancy_id=58df9c48f9a2f347fd000015&upcoming=true/false
  # params = {:kick_counter_data=>{:start_time=>"2018-01-01 13:44", :end_time=>"2018-01-01 14:44", :kick_count=>3,:pregnancy_id=>58df9c48f9a2f347fd000015}}
  def add_kick_count
    begin
      validate_start_and_end_date
      validate_kick_count
      pregnancy = Pregnancy.find(params["kick_counter_data"]["pregnancy_id"])
      params["kick_counter_data"]["added_by"] = "user"
      Pregnancy::KickCounter::KickCountDetail.create_kick_detail_count(current_user,params)
      status = StatusCode::Status200
      message = Message::API[:success][:add_kick_count]
    rescue Exception => e 
      @error = Message::API[:error][:add_kick_count]
      message = e.message
      status = StatusCode::Status100
    end
    render :json=> {:error =>@error,status: status, message:message}

  end
  
  # Put
  # /api/v4/pregnancy/kick_counter/kick_count/update_kick_count.json?token=AuSnoCzp2uceDGA1JGqb
  # params = {kick_counter_id=>"5a4db86af9a2f3b261000001", :kick_counter_data=>{:start_time=>"2018-01-01 13:44", :end_time=>"2018-01-01 14:44", :kick_count=>3,:pregnancy_id=>58df9c48f9a2f347fd000015}}
  def update_kick_count
    begin
      validate_start_and_end_date
      validate_kick_count
      unless params["kick_counter_data"]["start_time"].to_date > Date.today
        kick_counter_object =  Pregnancy::KickCounter::KickCountDetail.find(params[:kick_counter_id])
        params["kick_counter_data"]["added_by"] = "user"
        kick_counter_object.update_kick_count(current_user,params)
        status = StatusCode::Status200
        message = Message::API[:success][:update_kick_count]
      else
        message = Message::API[:error][:update_kick_count]
        status = StatusCode::Status100
      end
    rescue Exception => e 
      @error = Message::API[:error][:update_kick_count]
      message = e.message
      status = StatusCode::Status100
    end
    render :json=> {:error =>@error,status: status, message:message}

  end

  # Delete
  # /api/v4/pregnancy/kick_counter/kick_count/delete_kick_count.json?token=AuSnoCzp2uceDGA1JGqb&kick_counter_id
  def delete_kick_count
    begin  
      kick_counter_object =  Pregnancy::KickCounter::KickCountDetail.find(params[:kick_counter_id])
      kick_counter_object.delete_kick_count#(current_user,params)
      status = StatusCode::Status200
      message = Message::API[:success][:delete_kick_count]
    rescue Exception => e 
      @error = e.message
      message = Message::API[:error][:delete_kick_count]
      status = StatusCode::Status100
    end
    render :json=> {:error =>@error,status: status, message:message}
    
  end
  
  # Post
  # /api/v4/pregnancy/kick_counter/kick_count/add_kick_counter_schedule.json?token=AuSnoCzp2uceDGA1JGqb
  # params = {:scheduler_data=>{:kick_counter_start_date=>"2018-01-01", :reminder_time=>"14:44",:pregnancy_id=>58df9c48f9a2f347fd000015}}
  def add_kick_counter_schedule
    begin
      Pregnancy::KickCounter::Scheduler.create_update_kick_schedule(current_user,params)
      status = StatusCode::Status200
      message = Message::API[:success][:add_kick_counter_schedule]
    rescue Exception => e 
      @error = e.message
      message = Message::API[:error][:add_kick_counter_schedule]
      status = StatusCode::Status100
    end
    render :json=> {:error =>@error,status: status, message:message}
  end
  
  #  Put
  # /api/v4/pregnancy/kick_counter/kick_count/update_kick_counter_schedule.json?token=AuSnoCzp2uceDGA1JGqb
  # params = {:kick_schedule_id => "5a606ba0f9a2f3611f000001", :scheduler_data=>{:kick_counter_start_date=>"2018-01-01", :reminder_time=>"14:44", :pregnancy_id=> "7as9dkuedmnc23d"}}
  def update_kick_counter_schedule
    begin
      unless params[:kick_schedule_id].blank?
        kick_schedule = Pregnancy::KickCounter::Scheduler.find(params[:kick_schedule_id])
        params[:scheduler_data][:added_by] = 'user'
        Pregnancy::KickCounter::Scheduler.create_update_kick_schedule(current_user,params,{:kick_schedule_object=>kick_schedule})
        status = StatusCode::Status200
        message = Message::API[:success][:update_kick_counter_schedule]
      else
        message = Message::API[:error][:update_kick_counter_schedule]
        status = StatusCode::Status100
      end
    rescue Exception => e 
      @error = Message::API[:error][:update_kick_counter_schedule]
      message = e.message
      status = StatusCode::Status100
    end
    render :json=> {:error =>@error,status: status, message:message}
  end

  #  Put
  # /api/v4/pregnancy/kick_counter/kick_count/update_upcoming_kick_counter_schedule.json?token=AuSnoCzp2uceDGA1JGqb
  # params = {:kick_schedule_id => "5a606ba0f9a2f3611f000001", :scheduler_data=>{:kick_counter_start_date=>"2018-01-01", :reminder_time=>"14:44"}}
  def update_upcoming_kick_counter_schedule
    begin
      validate_update_start_date
      unless params[:kick_schedule_id].blank?
        kick_scheduled = Pregnancy::KickCounter::ScheduledKick.find(params[:kick_schedule_id])
        params[:scheduler_data][:added_by] = 'user'
        kick_scheduled.update_kick_schedule(current_user, params)
        status = StatusCode::Status200
        message = Message::API[:success][:update_kick_counter_schedule]
      else
        message = Message::API[:error][:update_kick_counter_schedule]
        status = StatusCode::Status100
      end
    rescue Exception => e 
      @error = Message::API[:error][:update_kick_counter_schedule]
      message = e.message
      status = StatusCode::Status100
    end
    render :json=> {:error =>@error,status: status, message:message}
  end
  
  def set_api_version
    @api_version = 6
  end

  private

  def validate_start_and_end_date
    invalidate = ValidateParams.check_date(params[:kick_counter_data][:start_time],
      params[:kick_counter_data][:end_time])

    raise Message::API[:error][:start_and_end_date_empty] if invalidate
  end

  def validate_kick_count
    invalidate = ValidateParams.check_positive_number(params[:kick_counter_data][:kick_count])
    raise Message::API[:error][:kick_count_count_empty] if invalidate
  end

  def validate_update_start_date
    invalidate = ValidateParams.check_date(params[:scheduler_data][:kick_counter_start_date])
    raise Message::API[:error][:kick_count_start_date_null] if invalidate
  end

end
