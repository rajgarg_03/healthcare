class Api::V6::Pregnancy::HealthCard::BloodPressureController < ApiBaseController
  before_filter :set_api_version
  
  #http://localhost:3001/api/v3/pregbp.json?token=uSLz1sXRUgUyJXo2wZwB&pregnancy_id=57ea71b5f9a2f382a0000002
  #params={:pregnancy_id=>""}
  def index
    begin
      data = Pregnancy::HealthCard::BloodPressure.where(pregnancy_id:params[:pregnancy_id]).order_by("test_date desc")
      last_updated_record  = data.first.updated_bp rescue nil
      status =  StatusCode::Status200
      pregnancy  = Pregnancy.find(params[:pregnancy_id])
      chart_data,week_age,combine_data = chart_data_value(pregnancy,data)
      who_chart_data,who_min_max_chart_axis = Pregnancy::HealthCard::BloodPressure.who_chart_data
      last_record = data.last
      min_max_axis = Pregnancy::HealthCard.chart_x_y(combine_data + who_chart_data[:systolic][0] + who_chart_data[:diastolic][0])
      options = {:get_data_for_from_list=>"Blood Pressure"}
      member = Member.where(:id=>pregnancy.expected_member_id).last
      @tool, @show_subscription_box_status,@info_data,dummy_data = member.get_subscription_show_box_data(current_user,"Pregnancy Health Card",@api_version,options)
      if @show_subscription_box_status == true
        data = dummy_data
      else
        data = data.map(&:to_api_json)
      end
      nhs_syndicate_content = ::Pregnancy::HealthCard::BloodPressure.nhs_syndicate_content(pregnancy,options)
    rescue Exception=>e
      data = []
      status = StatusCode::Status100
      last_updated_record = nil
      @error = e.message
    end
    render :json=>{:syndicated_content=>nhs_syndicate_content , :free_trial_end_date_message=>@info_data[:free_trial_end_date_message],:subscription_box_data=>@info_data[:subscription_box_data],system_tool_id:@tool.id,show_subscription_box:@show_subscription_box_status, who_chart_data: {systolic: who_chart_data[:systolic][0], diastolic: who_chart_data[:diastolic][0]} ,last_record:last_record, last_updated_record:last_updated_record, :error=>@error,:week_age=>week_age, :systolic_chart_data=>chart_data[:systolic_chart_data],:pulses_chart_data=>chart_data[:pulses_chart_data],:diastolic_chart_data=>chart_data[:diastolic_chart_data],:chart_min_max_axis=>min_max_axis, :data=>data, :status=>status}
  end
  
  #http://localhost:3001/api/v3/pregbp.json?token=uSLz1sXRUgUyJXo2wZwB&preg_id=57ea71b5f9a2f382a0000002
  #post params={preg_blood_press=>{:test_date=> , pregnancy_id=>"", :systolic=>"",:diastolic=>"",:pulses=>""}}
  def create
    begin
      pregnancy  = Pregnancy.find(params[:preg_blood_press][:pregnancy_id])

      # record = Pregnancy::HealthCard::BloodPressure.create(params[:preg_blood_press])
      # record.update_attribute(:pregnancy_week,pregnancy.pregnancy_week(record.test_date.to_date))
      record = Pregnancy::HealthCard::BloodPressure.create_record(current_user,params,pregnancy)
      status =  StatusCode::Status200
      message = Message::API[:success][:preg_pregbp_create]
      data = Pregnancy::HealthCard::BloodPressure.where(pregnancy_id:params[:preg_blood_press][:pregnancy_id])
      chart_data,week_age,combine_data = chart_data_value(pregnancy,data)
      who_chart_data,who_min_max_chart_axis = Pregnancy::HealthCard::BloodPressure.who_chart_data
       min_max_axis = Pregnancy::HealthCard.chart_x_y(combine_data + who_chart_data[:systolic][0] + who_chart_data[:diastolic][0])
      last_record = data.last
    rescue Exception=>e
      @error = e.message
      message = Message::API[:error][:preg_pregbp_create]
      status =  StatusCode::Status100
      data = []
    end
    render :json=>{:systolic_chart_data=>chart_data[:systolic_chart_data],:pulses_chart_data=>chart_data[:pulses_chart_data],:diastolic_chart_data=>chart_data[:diastolic_chart_data], :chart_min_max_axis=>min_max_axis, who_chart_data: {systolic: who_chart_data[:systolic][0], diastolic: who_chart_data[:diastolic][0]}, :last_record=>last_record,:error=>@error,:record=> record,:message=>message,:week_age=>week_age, :chart_data=>chart_data, :data=>data.map(&:to_api_json), :status=>status}
  end

  #params={:id=>"asdasd",preg_blood_press=>{test_date=>, :systolic=>"",:diastolic=>"",:pulses=>""}}
  def update
    begin
      params[:preg_blood_press].delete(:pregnancy_id)
      bp = Pregnancy::HealthCard::BloodPressure.where(id:params[:id]).last
      pregnancy  = Pregnancy.find(bp.pregnancy_id)
      bp.update_record(current_user,params,pregnancy)
      # bp.update_attributes(params[:preg_blood_press])
      # bp.update_attribute(:pregnancy_week,pregnancy.pregnancy_week(bp.test_date.to_date))
      data = Pregnancy::HealthCard::BloodPressure.where(pregnancy_id:bp.pregnancy_id)
      status =  StatusCode::Status200
      chart_data,week_age,combine_data = chart_data_value(pregnancy,data)
      who_chart_data,who_min_max_chart_axis = Pregnancy::HealthCard::BloodPressure.who_chart_data
       min_max_axis = Pregnancy::HealthCard.chart_x_y(combine_data + who_chart_data[:systolic][0] + who_chart_data[:diastolic][0])
      last_record = data.last
      message = Message::API[:success][:preg_pregbp_update]
    rescue Exception=>e
      data = []
      status = StatusCode::Status100
      message = Message::API[:error][:preg_pregbp_update]
      @error = e.message
    end
    render :json=>{:chart_min_max_axis=>min_max_axis, who_chart_data: {systolic: who_chart_data[:systolic][0], diastolic: who_chart_data[:diastolic][0]},  :last_record=>last_record,:error=>@error,:record=>bp,:message=>message,:week_age=>week_age, :systolic_chart_data=>chart_data[:systolic_chart_data],:pulses_chart_data=>chart_data[:pulses_chart_data],:diastolic_chart_data=>chart_data[:diastolic_chart_data],   :data=>data.map(&:to_api_json), :status=>status}
  end

  def destroy
    begin
      bp = Pregnancy::HealthCard::BloodPressure.where(id:params[:id]).last
      preg_id = bp.pregnancy_id
      bp.delete
      pregnancy  = Pregnancy.find(bp.pregnancy_id)
      data = Pregnancy::HealthCard::BloodPressure.where(pregnancy_id:preg_id)
      last_record = data.last
      chart_data,week_age, combine_data = chart_data_value(pregnancy,data)
      who_chart_data,who_min_max_chart_axis = Pregnancy::HealthCard::BloodPressure.who_chart_data
       min_max_axis = Pregnancy::HealthCard.chart_x_y(combine_data + who_chart_data[:systolic][0] + who_chart_data[:diastolic][0])
      message = Message::API[:success][:preg_pregbp_delete]
      status =  StatusCode::Status200
    rescue Exception=>e
      data = []
      status = StatusCode::Status100
      message = Message::API[:error][:preg_pregbp_delete]
      @error = e.message
    end
    render :json=>{:chart_min_max_axis=>min_max_axis, who_chart_data: {systolic: who_chart_data[:systolic][0], diastolic: who_chart_data[:diastolic][0]} ,:last_record=>last_record,:error=>@error, :week_age=>week_age, :systolic_chart_data=>chart_data[:systolic_chart_data],:pulses_chart_data=>chart_data[:pulses_chart_data],:diastolic_chart_data=>chart_data[:diastolic_chart_data], :data=>data.map(&:to_api_json),:message=>message, :status=>status}
  end


  def chart_data_value(pregnancy,data)
    data = []
    Pregnancy::HealthCard::BloodPressure.where(pregnancy_id:pregnancy.id).order_by("test_date asc").each do |record|
      systolic = (record.systolic || systolic) rescue nil
      pulses = (record.pulses || pulses) rescue nil
      diastolic = (record.diastolic) rescue nil
      data << {:pregnancy_week =>record.pregnancy_week, :systolic=>systolic,:pulses=>pulses,:diastolic=>diastolic}
    end 
    
    systolic_chart_data = data.collect{|rec| [rec[:pregnancy_week],rec[:systolic].to_f]}
    pulses_chart_data = data.collect{|rec| [rec[:pregnancy_week],rec[:pulses].to_f]}
    diastolic_chart_data = data.collect{|rec| [rec[:pregnancy_week],rec[:diastolic].to_f]}
    
    # systolic_chart_data = data.collect{|rec| [rec["pregnancy_week"],rec[:systolic].to_f]}
    # pulses_chart_data = data.collect{|rec| [rec["pregnancy_week"],rec[:pulses].to_f]}
    # diastolic_chart_data = data.collect{|rec| [rec["pregnancy_week"],rec[:diastolic].to_f]}
    max_pregnancy_week = data.map{|a| a[:pregnancy_week].to_i}.max || 0
    max_pregnancy_week = max_pregnancy_week > 40 ? max_pregnancy_week : 40
    week_age = (0..max_pregnancy_week).map(&:to_i)
    chart_data = {:systolic_chart_data=>systolic_chart_data,:pulses_chart_data=>pulses_chart_data,:diastolic_chart_data=>diastolic_chart_data}
    combine_data = systolic_chart_data + pulses_chart_data + diastolic_chart_data
    return chart_data, week_age, combine_data
  end
  
  def set_api_version
    @api_version = 6
  end
end

