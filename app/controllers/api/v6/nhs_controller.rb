require 'net/http'
class Api::V6::NhsController < ApplicationController
before_filter :set_api_version
skip_before_filter :authenticate_user!
# layout 'home_page_layout', only: [:search,:news,:media,:organisations_search]

# GET "http://localhost:3000/api/v3/nutrients/546128110a9a999a09000005/acitvate.json?token=XrjzH88mP4syK1q4MTeb&member_id=546128200a9a99178300000d"
def search
  query = params[:query]
  # BatchMailer.premium_subscription_email.deliver!
  begin
    @data = Nhs::NhsApi.get_search_data(query)
    render json:@data.to_json
  rescue Exception=>e
    Rails.logger.info e.message
    status = StatusCode::Status100
    format.json {render json: {message: "NHS server not available", status: status}}
  end
end

def news
  start_date = params[:start_date]
  end_date = params[:end_date]
  order = params[:order]
  page = params[:page]
  
  begin
    @data = Nhs::NhsApi.get_news_data(start_date,end_date,page,order)
    render json: @data
  rescue Exception=>e
    Rails.logger.info e.message
  end
end


def media
  start_date = params[:start_date].blank?  ? (Date.today-7).to_s : params[:start_date]
    end_date = params[:end_date].blank? ? Date.today.to_s : params[:end_date]
  page = params[:page] ? params[:page] : 1
  
  begin
    @data = Nhs::NhsApi.get_media_data(start_date, end_date, page)
    render json: @data
  rescue Exception=>e
    Rails.logger.info e.message
  end
end




def organisations_search
  organisation_type = params[:organisation_type]? params[:organisation_type]: "GP"
  limit = params[:limit]? params[:limit]: "25"
  offset = params[:offset]? params[:offset] : 0
  service_code = params[:service_code] ? params[:service_code] : ""
  ods_code = params[:ods_code] ? params[:ods_code] : ""
  select_param = params[:select_param] ? params[:select_param] : ""
  order_by = params[:order_by] ? params[:order_by] : ""
  order = params[:order] ? params[:order_by] : "asc"
  lat = params[:lat] ? params[:lat] : ""
  long = params[:long] ? params[:long] : ""
  
  begin
    @data = Nhs::NhsApi.get_organisations(organisation_type, limit, offset, service_code, ods_code, select_param, order_by, order,lat,long)
    render json: @data
  rescue Exception=>e
    Rails.logger.info e.message
  end
end

def token
  begin
    result = {}
    if params[:refresh_token] != nil 
      result = Nhs::NhsApi.generate_token_via_refresh_token(params[:refresh_token])
    else 
      result = Nhs::NhsApi.generate_login_token(params[:refresh_token])
    end
    render json: result
    rescue Exception=>e
      Rails.logger.info e.message
      render :json=>{error:e.message,:message=>::Message::NhsLogin[:error_message]}
  end
end

def authenticate_user
  code = params[:code]
  state_data = eval(ShaAlgo.decryption(params[:state].gsub(' '+ '+'))).with_indifferent_access rescue {}
  options = {:user_email=>state_data[:email],:login_type=>state_data[:login_type]}

  nhs_login_failed_url = "/messages?nhs_login_status=failed"
  begin
    token_response = {}
      token_response = Nhs::NhsApi.generate_login_token(code)
      if token_response[:status] == 200
        Rails.logger.info "In Authorize redirect ===>>>  "
        @user_info = Nhs::NhsApi.fetch_user_info(token_response["access_token"])
        if @user_info[:status] == 200
          Rails.logger.info "token is fetching correctly ===>>>   "+token_response["access_token"]    
          @deeplink = Nhs::NhsApi.create_deeplink_data(@user_info,token_response,options)
          redirect_to @deeplink
        else
          @message = @user_info[:message]
          # render :json=> @user_info
          redirect_to nhs_login_failed_url
        end
      else
        @message = token_response[:message]
        redirect_to nhs_login_failed_url
        # render :json=>token_response
      end
  rescue Exception=>e
    Rails.logger.info e.message
    @message = ::Message::NhsLogin[:error_message]
    # render :json=>{error:e.message,status:100,:message=>::Message::NhsLogin[:error_message]}
    redirect_to @nhs_login_failed_url
  end
end

def initiate_user_authentication
  authorize_url = APP_CONFIG['nhs_login_authorize_url']
  user_email = params[:email] || ''
  state = ShaAlgo.encryption({email:user_email,login_type:params[:type]}.to_s)
  
  client_id = APP_CONFIG['nhs_login_client_id']
  scope = APP_CONFIG['nhs_login_scope']
  redirect_url = APP_CONFIG['nhs_login_redirect']
  vtr = APP_CONFIG['nhs_login_vtr']
  if params[:email].present?
    begin
      user = User.where(email:params[:email]).last 
      if params[:type] == "im1"
        user.nhs_login_for_im1 = true
      else
        user.nhs_login_for_im1 = false
      end
      user.save(validate:false)
    rescue Exception => e
      Rails.logger.info "Error inside initiate_user_authentication.......... #{e.message}"      
    end
  end
  final_redirect_url = authorize_url+"&state="+state+"&client_id="+client_id+"&redirect_uri="+redirect_url+"&scope="+scope+"&vtr="+vtr

  redirect_to final_redirect_url
end

#used for nhslogin redirect
def authorize_redirect
  authenticate_user
end

def user_info
  access_token = params[:access_token]
  begin
    user_info = Nhs::NhsApi.fetch_user_info(access_token)
    render json:user_info
  rescue Exception=>e
    Rails.logger.info e.message
    render :json=>{:error=>e.message,:message=>::Message::NhsLogin[:error_message],status:100}
  end
end

private
  def set_api_version
    @api_version = 6
  end

end