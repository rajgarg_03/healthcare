# art_ref AKA pointer
class Api::V6::ArticleRefController < ApplicationController
  before_filter :set_api_version
 
 # List all pointer having effective score greater than 0
 # Get /article_ref.json?token=3123&shortlisted=true/false
 # param {}
  def index
    begin
      params[:page] =  (params[:page] || 1).to_i rescue 1
      values_in_score = [nil,0,"0"] #values that pointer should not have for listing 
      if params[:member_id].present?
        childern_ids = [params[:member_id]]
      else
        childern_ids = current_user.childern_ids.map(&:to_s)
      end
      # if user has role Mother, add mother roles pointer additional
        all_article_refs = ArticleRef.get_pointers_list(current_user,childern_ids)
      if params[:shortlisted]== "true"
        all_article_refs = all_article_refs.where(shortlisted:true)
      end
      if params[:feed_id].present?
        per_page = 5
        begin
          pointer = ArticleRef.find params[:feed_id] #{}"596b40bff9a2f3ce52000387"
          position = all_article_refs.where(:effective_score.gte=>pointer.effective_score).order_by("effective_score desc").count + per_page
          @current_page = (position) /per_page.ceil
          article_refs =   all_article_refs.order_by("effective_score desc").paginate(:page => 1,:per_page=>position)
          pointer.update_user_activity_status_v2_for_pointer
        rescue Exception=> e 
          Rails.logger.info "...............Error inside pointers list- #{e.message} ..................."
          Rails.logger.info e.backtrace
          @current_page = params[:page]
          article_refs =  all_article_refs.order_by("effective_score desc").paginate(:page => params[:page] )
        end
      else
        @current_page = params[:page]
        article_refs =   all_article_refs.order_by("effective_score desc").paginate(:page => ((params[:page].to_i||1)rescue 1) )
      end
      @last_updated_on = ArticleRef.not_in(effective_score:values_in_score).in(member_id:childern_ids).order_by("updated_at desc").first.updated_at rescue nil
      
      #For pagination data
      @total_page,@next_page = Utility.page_info(all_article_refs,@current_page)
      
      all_article_refs.update_all(checked: true) #NOTE: Mark all referances read.
      status = StatusCode::Status200
    rescue Exception=>e
      Rails.logger.info "...............Error1 inside pointers list- #{e.message} ..................."
      status = StatusCode::Status100
      @error = e.message
    end
    if @last_updated_on.present?
       render :json=> { :last_updated_on=>@last_updated_on, :status=>status, :current_page=> @current_page,:total_page=>@total_page, :next_page=>@next_page || @total_page,  :error=>@error, :article_ref=> article_refs }
    else
      render :json=> { :status=>status, :current_page=> @current_page,:total_page=>@total_page, :next_page=>@next_page || @total_page,  :error=>@error, :article_ref=> article_refs }
    end
  end

  # Api art_ref_detail : Get the detail of particular ArticleRef
  # Details of a requested pointer id will be return in response 
  # Get /article_ref/art_ref_detail.json?token=
  # params = {:article_ref_id=>"" }
  def art_ref_detail
    begin
      article_ref = ArticleRef.find(params[:article_ref_id]) rescue {}
      article_ref.update_user_activity_status_v2_for_pointer rescue nil
      status = StatusCode::Status200
    rescue Exception=>e
      Rails.logger.info "...............Error1 inside pointers art_ref_detail- #{e.message} ..................."
      status = StatusCode::Status100
      @error = e.message
    end
    render :json=> { :status=>status, :error=>@error, :article_ref=> article_ref }
  end

  # Update pointer details
  # Put /art_ref_detail/update_art_ref.json?token=
  # params = {:article_ref_id=>"", :data=>{:name=>"", :description=>""} }
  def update_art_ref
    begin
      article_ref = ArticleRef.find(params[:article_ref_id])
      article_ref.update_attributes(params[:data])
      status = StatusCode::Status200
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
    end
      render :json=> { :status=>status, :error=>@error, :article_ref=> article_ref }
  end

  # Save  pointer reference link clicked by user
  # Post /api/v4/article_ref/save_pointer_reference_link_clicked_detail.json?member_id=5b76a47cf9a2f3a05b000003&token=AuSnoCzp2uceDGA1JGqb
  # params = {:pointer_id=>"", :pointer_reference_link_id=>, :platform=>"ios/android"}
  def save_pointer_reference_link_clicked_detail
    begin
      record = Report::PointerReferenceLinkClickedData.new(device_platform:params[:platform], pointer_reference_link_id:params[:pointer_reference_link_id], user_id:current_user.user_id,pointer_id:params[:pointer_id])
      record.save!
      status = StatusCode::Status200
    rescue Exception =>e 
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,params,"Error inside save_pointer_reference_link_clicked_detail")
      status = StatusCode::Status100
      @error = e.message
    end
    render :json=> { :status=>status, :error=>@error }
  end
  
  # Update pointers remind me value and counter.
  # Put /art_ref_detail/remind_me_later.json?token=
  # params = {:article_ref_id=>"", :data=>{:after_days=>"12"} }
  def remind_me_later
    begin
      article_ref = ArticleRef.find(params[:article_ref_id])
      article_ref.reminder_count = article_ref.reminder_count.to_i + 1
      article_ref.remind_me_on = Date.today + eval(params[:data][:after_days]+".days")
      article_ref.save
      status = StatusCode::Status200
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
    end
      render :json=> { :status=>status, :error=>@error, :article_ref=> article_ref }
  end

  # Shortlist a pointer  
  # Put /art_ref_detail/add_to_favorite.json?token=
  # params = {:article_ref_id=>"", :data=>{:shortlisted=>true/false} }
  def add_to_favorite
    begin
      article_ref = ArticleRef.find(params[:article_ref_id])
      article_ref.update_attributes(params[:data])
      status = StatusCode::Status200
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
    end
      render :json=> { :status=>status, :error=>@error, :article_ref=> article_ref }
  end


  # Delete pointer from list.set score 0, status deleted. This will delete in rake job
  # Delete /art_ref_detail/delete_article_ref.json?token=
  # params {:article_ref_id=>"", }
  def delete_article_ref
    begin
      article_ref = ArticleRef.find(params[:article_ref_id])
      article_ref.status = "deleted"
      article_ref.score = 0
      article_ref.effective_score = 0
      article_ref.save
      status = StatusCode::Status200
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
    end
      render :json=> { :status=>status, :error=>@error }
  end

  # Show pointer detail
  # Get /art_ref_detail/{article_ref_id}.json?token=
  # params {:article_ref_id=> }
  def show
    begin
      article_ref = ArticleRef.where(id:params[:id]).last
      if article_ref.blank?
        Rails.logger.info "Pointer does not find by ID"
        user_notification = UserNotification.where(feed_id:params[:id],type:"pointers").last
        s_id = user_notification.identifier
        article_ref = ArticleRef.where( s_id:s_id).last

        if article_ref.blank?
          Rails.logger.info "Pointer does not find by sid"
          # Reassign pointer
          member_id = user_notification.member_id
          member = Member.find(member_id)
          member.update_member_ref("add")
          article_ref = ArticleRef.where(member_id:member_id,s_id:s_id).last
          if article_ref.present?
            Rails.logger.info ".......Pointer Reassigned ID - #{article_ref.id}...."
          end
        end
      end     
      response = {status: StatusCode::Status200, article_ref: article_ref}
    rescue Exception=>e
      Rails.logger.info "Error inside show pointer #{e.message}"
      Rails.logger.info "RCA #{e.backtrace}"
      response = {status: StatusCode::Status100, error: e.message}
    end
    render :json=> response
  end

  # Get highest scored pointer of user. only 2 pointer will be recommended
  def recommended_art_ref
    begin
      member = current_user.childern_ids.map(&:to_s)
      article_ref = ArticleRef.get_pointers_list(current_user,member).order_by("effective_score desc").limit(2) || []
      last_updated_on = ArticleRef.valid_pointers.in(member_id:member).order_by("updated_at desc").first.updated_at rescue nil
      response = {status: StatusCode::Status200, article_ref: article_ref,last_updated_on:last_updated_on}
    rescue Exception=>e
      response = {status: StatusCode::Status100, error: e.message}
    end
    render :json=> { :status=>status, :article_ref=> article_ref }
  end  
  
  def set_api_version
    @api_version = 6
  end
end
