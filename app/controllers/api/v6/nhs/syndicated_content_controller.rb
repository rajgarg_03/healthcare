class Api::V6::Nhs::SyndicatedContentController < ApiBaseController
  before_filter :set_api_version
  # Get /api/v5/nhs/syndicated_content/get_filter_content.json?token=541ebcb88cd03ddfc4000002&search=ai
  # params = {:member_id=>'',:search=>""} 
  def get_filter_content
    begin
      data = []
      options = {}
      member_id = params[:member_id]
      if params[:search].present?
        topic_list = ::Nhs::SyndicatedContent.search_title(params[:search].strip)
        
      elsif member_id.present?
        topic_list = ::Nhs::SyndicatedContent.all
      else
        topic_list = ::Nhs::SyndicatedContent.all
      end
      if topic_list.present?
        topic_list = topic_list.only(:genre, :custom_id,:title,:category,:api_link,:content_topic_name)
        data = topic_list.all.map{|a| a.syndicated_format_data(current_user,member_id,options) }
      end
      status = ::StatusCode::Status200
    rescue Exception =>e 
      status = ::StatusCode::Status100
      @error = e.message
      @msg = ::Message::API[:error][:get_syndicated_content]
    end
    render :json=>{status:status, message:@msg, error:@error, data:data}
  end

  # Get /api/v5/nhs/syndicated_content/get_category_list.json?token=541ebcb88cd03ddfc4000002
  # params = {:member_id=>""}
  def get_category_list
    begin
      category_list = nil
      member_id = params[:member_id]
      options = {}
      status = ::StatusCode::Status200
      category_list = ::Nhs::SyndicatedContent.get_category_list(current_user,member_id,options)
    rescue Exception => e
      status = ::StatusCode::Status100
      @erro = e.message
      @msg = "Unable to get details. Please try again"
    end
    render :json=>{status:status, message:@msg, error:@error, category_list:category_list}
  end


  # Get /api/v5/nhs/syndicated_content/get_topic_list_for_category.json?token=541ebcb88cd03ddfc4000002
  # params = {:member_id=>"",:category=>""}
  def get_topic_list_for_category
    begin
      topic_list = nil
      member_id = params[:member_id]
      options = {}
      category = params[:category]
      raise "No category selected" if category.blank?
      status = ::StatusCode::Status200
      topic_list = ::Nhs::SyndicatedContent.get_category_topic_list(current_user,member_id,category,options)
    rescue Exception => e
      status = ::StatusCode::Status100
      @erro = e.message
      @msg = "Unable to get details. Please try again"
    end
    render :json=>{status:status, message:@msg, error:@error, topic_list:topic_list}
  end

  #Get /api/v5/nhs/syndicated_content/get_content.json?token=541ebcb88cd03ddfc4000002
  # params = {:id=>"",:api_link=>""}
  def get_content
    begin
      member_id = params[:member_id]
      options = {:current_user=>current_user,member_id:member_id}
      if params[:id].present?
        syndicated_content= ::Nhs::SyndicatedContent.find(params[:id])
        api_link = syndicated_content.api_link
        options[:web_url] = syndicated_content.web_url
      elsif params[:api_link].present?
        api_link = params[:api_link]
      end
      category = params[:category]
      raise "No reference link found" if api_link.blank?
      status = ::StatusCode::Status200
      content = ::Nhs::SyndicatedContent.get_content(api_link,options)
      formated_content = ::Nhs::SyndicatedContent.format_content(content,options)
    rescue Exception => e
      status = ::StatusCode::Status100
      @erro = e.message
      content = nil
      @msg = "Unable to get details. Please try again"
    end
    render :json=>{status:status, message:@msg, error:@error, content:formated_content}
  end

  def set_api_version
    @api_version = 6
  end
end