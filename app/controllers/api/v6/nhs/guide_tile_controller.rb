class Api::V6::Nhs::GuideTileController < ApiBaseController
  before_filter :set_api_version
  
  # Get
  # api/v6/nhs/guide_tile/list_article_for_tile?guide_tile_id=
  def list_article_for_tile
    begin
      guide_tile = ::Nhs::GuideTiles.find(params[:guide_tile_id])
      linked_tool_object_id = guide_tile.id
      linked_tool_class = guide_tile.class.to_s
      syndicated_custome_id  = ::Nhs::SyndicatedContentLinkTool.where(linked_tool_object_id:linked_tool_object_id,linked_tool_class:linked_tool_class).pluck(:nhs_syndicated_content_custom_id)
      options = {:without_content => true}
      member_id = nil
      data = Nhs::SyndicatedContent.where(:custom_id.in=>syndicated_custome_id).map{|a| a.syndicated_format_data(current_user,member_id,options) }
      status = 200
      message = nil
    rescue Exception => e
      data = []
      status = 100
      message = ::Message::API[:error][:get_syndicated_content]
    end
    render :json=>{status:status,:article_list=>data,:message=>message}
  end

  # Get
  # api/v6/nhs/guide_tile/get_nhs_guide_tiles
  def get_nhs_guide_tiles
    begin
      options =  {}
      
      options[:suggested_content_limit] = 6
      data = ::Nhs::GuideTiles.guidetile_widget(current_user,options)
      status = 200
      message = nil
    rescue Exception => e
      data = {}
      status = 100
      message = ::Message::API[:error][:get_syndicated_content]
    end
    render :json=>{status:status,:nhs_guide_tiles=>data[:tiles],:suggested_content=>data[:suggested_content], :message=>message}
  end

  def set_api_version
    @api_version = 6
  end
end