class Api::V6::User::FaqController < ApiBaseController
  before_filter :set_api_version
  
  # Get
  # /api/v4/user/faq/get_topic_list.json?token=AuSnoCzp2uceDGA1JGqb
  # params = {:token=>}  
  def get_topic_list
    topics = System::Faq::Topic.sort_by_sort_order.entries
    render json: {status: StatusCode::Status200, topic_list:topics} 
  end


  # Get
  # /api/v4/user/faq/get_subtopic_list.json?token=AuSnoCzp2uceDGA1JGqb
  # params = {:token=>,:topic_id=>}  
  def get_subtopic_list
    topic = System::Faq::Topic.find(params[:topic_id])
    sub_topics = topic.sub_topics.sort_by_sort_order
    render json: {status: StatusCode::Status200, :topic=>topic, subtopic_list:sub_topics} 
  end
  
  # Get
  # /api/v4/user/faq/get_question_detail.json?token=AuSnoCzp2uceDGA1JGqb&topic_id=5b6d2ce2f9a2f3610700000f
  # params = {:question_id=>,:token=>}
  def get_question_detail
    if params[:question_id].blank? 
      subtopic = System::Faq::SubTopic.find(params[:subtopic_id])
      questions = subtopic.questions.sort_by_sort_order
      question_detail = questions.map{|question| question.get_json(current_user)} 
    else
      question = System::Faq::Question.find(params[:question_id])
      subtopic = System::Faq::SubTopic.find(question.sub_topic_id)
      question_detail = question.get_json(current_user)
    end
    topic = System::Faq::Topic.find(subtopic.topic_id)
    render json: {status: StatusCode::Status200, topic:topic, sub_topic:subtopic,question_detail:question_detail} 
  end

  # Get
  # api/v4/user/faq.json?token=3PQP4v3XxT53zXxt5JBJ
  # params = {:subtopic_id=>}
  def index
    begin
      json_resposne = System::Faq::Topic.topic_list_response(current_user,params)
      status = StatusCode::Status200
      message = ''
    rescue Exception=>e
      message = e.message
      @error = e.message
      status = StatusCode::Status100
    end
    json = { status: status, message: message, error: @error }
    json.merge!(json_resposne) if json_resposne
    render json: json
  end

  # /api/v4/user/faq/user_feedback.json?token=3PQP4v3XxT53zXxt5JBJ&question_id=5b75827d7a1d5037b0000003&helpful_to_user=yes
  def user_feedback
    begin
      System::Faq::AnswerFeedback.create_feedback(params, current_user)
      status = StatusCode::Status200
      message = Message::API[:success][:update_faq_feedback]
    rescue Exception=>e
      message = e.message
      @error = e.message
      status = StatusCode::Status100
    end
    json = { status: status, message: message, error: @error }
    render json: json
  end
  
  def set_api_version
    @api_version = 6
  end

end
