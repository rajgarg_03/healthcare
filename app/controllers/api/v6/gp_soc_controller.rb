 class Api::V6::GpSocController < ApiBaseController
  before_filter :set_api_version
  #api/v5/gp_soc/search_gp_practices?postcode=
  def search_gp_practices
    begin
      @data = []
      if params[:postcode].present?
        options = {:current_user=>current_user}
        @data,@message,status = GpSoc::RegisteredUser.find_gp_practices_by_post_code(params[:postcode],nil,options)
      else
        status = StatusCode::Status100
        @message = "Please enter postcode"
      end
    rescue Exception => e
        status = StatusCode::Status100
        @error = e.message
    end
    status = 100 if status == 401
    
    render :json=>{:error=>@error,:message=>@message,:status=>status,:gp_practices=>@data}    
  end

  def set_api_version
    @api_version = 6
  end


end  