class Api::V6::Child::ToothChart::ChartManagerController < ApiBaseController
  before_filter :set_api_version

  # Not in use for now
  def member_tooth_list
    # @member = Member.find("5858e8e1f9a2f300cb000045")
    @member = Member.find(params[:member_id])

    @member["age"] = ApplicationController.helpers.calculate_age(@member.birth_date) rescue ""
    @child_male =  @member.male?
    type = param[:type]

    added_tooth_chart_manager_ids = Child::ToothChart::ToothDetails.where(child_id:@member.id).pluck(:chart_manager_id)
    estimated_tooth_chart_manager_id = Child::ToothChart::ToothDetails.estimated_tooth(@member,type).pluck(:chart_manager_id)
    tooth_chart_manager_ids_accomplished_by_user = Child::ToothChart::ToothDetails.achieved_tooth(@member,type).pluck(:chart_manager_id)
    achieved_tooth_manager_ids = tooth_chart_manager_ids_accomplished_by_user

    @all_chart_manager_type_count = Child::ToothChart::ChartManager.get_tooth_manager_type_wise
    
    @added_tooth_group_by_type_count = Child::ToothChart::ChartManager.get_tooth_manager_type_wise(added_tooth_chart_manager_ids)
    @accomplished_tooth_group_by_type_count = Child::ToothChart::ChartManager.get_tooth_manager_type_wise(tooth_chart_manager_ids_accomplished_by_user)
    @estimated_group_by_type_count = Child::ToothChart::ChartManager.get_tooth_manager_type_wise(estimated_tooth_chart_manager_id)
    
    @teeth_type_stats  = []
    tooth_progress_report = Child::ToothChart::ChartManager.get_progress_report_by_name(@member,type,added_tooth_chart_manager_ids)
    
    @added_tooth_group_by_type_count.each do |k,value|
      teeth_title_description = Child::ToothChart::ChartManager::Description[k.to_s]
     line2 = (@estimated_group_by_type_count[k][:count] rescue 0).to_s + " Estimated" 
     line3 = (tooth_progress_report[k.to_s]["delayed"] rescue 0).to_s + " Due"
     line1 = value[:count].to_s + "/" + (@all_chart_manager_type_count[k][:count]||0) .to_s
      @teeth_type_stats << {line1: line1, line2:line2, line3:line3, title:teeth_title_description[:title], description:teeth_title_description[:description] }
    end
    @estimated_percentage = calculate_percentage(@all_chart_manager_type_count,estimated_group_by_type_count)
    @achieved_percentage = calculate_percentage(@all_chart_manager_type_count,@achieved_tooth_manager_ids)
    render :json => {:estimated_per=>@estimated_percentage,:ach_per=>@achieved_percentage ,:state=>teeth_type_stats}
  end

  #  api :GET, "chart_manager", "Get list of tooth for a child"
  #  param :member_id_, String, desc: 'id of child member', required: true
  #  param :type, ["erupted", "shedded"], desc: 'detail for erupted/shedded/all teeth', required: true
  #  param :start_date, String, desc: 'date', required: optional
  #  param :end_date, String, desc: 'date', required: optional
  #  param :tooth_type, String, desc: "incisor,Canine,First Molar,Second Molar", required: optional
  #  description 'Controller: api/v4/child/tooth_chart/chart_manager Action: index'
  #  meta jira_id: "https://nurturey.atlassian.net/browse/API-215"
  #
  #  params={:member_id=>"",:type=>"erupted/shedded/all",:start_date=>,:end_date=>,:tooth_type=>"incisor,Canine,First Molar,Second Molar"}
  #Will deprecated in version 6
  def index
    begin
      type_param
      data = {}
      member =  Member.find(params[:member_id])
      params[:tooth_type] = params[:tooth_type].split(",") if params[:tooth_type].present?
      options = {}
      if params[:tooth_type].present?
        options = {:get_data_for_from_list=>"filter"}
      end
      @tool, @show_subscription_box_status,@info_data,@child_tooth_detail_hash = member.get_subscription_show_box_data(current_user,"Tooth Chart",@api_version,options)
      if @show_subscription_box_status == false
        @child_tooth_detail_hash = Child::ToothChart::ToothDetails.child_data_hash(params,params[:type])
      end

      if params[:type].blank? || params[:type] == 'all'
        @tooth_stats_hash = Child::ToothChart::ToothDetails.tooth_stats_data_hash(params[:member_id])
        @upcoming_tooth_hash = Child::ToothChart::ToothDetails.upcoming_tooth_data_hash(params[:member_id])
        @recommended_pointers = nil #ArticleRef.get_recommended_pointer(current_user, params[:member_id], "health")
        erupted_tooth_count = Child::ToothChart::ToothDetails.where(child_id:params[:member_id]).erupt_data.count
        updated_at =  Child::ToothChart::ToothDetails.where(child_id:params[:member_id]).not_in_unaccomplished_status.order_by("updated_at desc").first.updated_at.convert_to_ago2 rescue nil
        updated_at_text = updated_at.present? ? "Updated: #{updated_at}" : "Not updated yet"
        data = {:tooth_stats=> @tooth_stats_hash,
                :upcoming_tooth_development=> @upcoming_tooth_hash,
                :recommended_pointers=> @recommended_pointers,
                :tooth_chart_data=> @child_tooth_detail_hash,
                :title=>"#{(member.first_name.downcase.titleize rescue '')} has grown #{erupted_tooth_count} teeth",
                :updated_at=>updated_at_text
              }
      end
      status =  StatusCode::Status200
    rescue Exception=>e 
      status =  StatusCode::Status100
      @error = e.message
    end
    data.merge!({ :free_trial_end_date_message=>@info_data[:free_trial_end_date_message],:subscription_box_data=>@info_data[:subscription_box_data],system_tool_id:@tool.id,show_subscription_box:@show_subscription_box_status,  status: status, tooth_chart_data: @child_tooth_detail_hash,:error=>@error })
    render json: data
  end

  # show member tooth list 
  def toothList
    begin
      options = {}
      data = {}
      if params[:type].present? && params[:type].downcase != "all"
        params[:type] = params[:type].downcase 
        params[:type] = Child::ToothChart::ToothDetails::Shed if params[:type] == "shadded" || params[:type] =="shad"
        params[:type] = Child::ToothChart::ToothDetails::Erupt if params[:type] =="erupt"
      else
        options[:type] = [Child::ToothChart::ToothDetails::Shed,Child::ToothChart::ToothDetails::Erupt]
      end
      member =  Member.find(params[:member_id])
      params[:tooth_type] = params[:tooth_type].split(",") if params[:tooth_type].present?
      options[:get_data_for_from_list] = "version1"
      tool, show_subscription_box_status, info_data, child_tooth_detail_hash = member.get_subscription_show_box_data(current_user,"Tooth Chart",@api_version,options)
      if show_subscription_box_status == false
        child_tooth_detail_hash = Child::ToothChart::ToothDetails.child_data_hash(params,params[:type],options)
        child_tooth_detail_hash = Child::ToothChart::ToothDetails.get_data_group_by_age(member,current_user,child_tooth_detail_hash,options)
      end
      if params[:type].blank? || params[:type] == 'all'
        # enable overdue after verify
        overdue_tooth =nil # Child::ToothChart::ToothDetails.overdue_tooth(current_user,member,options)
        tooth_due = Child::ToothChart::ToothDetails.due_tooth(current_user,member,options)
        tooth_stats_hash = Child::ToothChart::ToothDetails.tooth_stats_data_hash(params[:member_id])
        upcoming_tooth_hash = tooth_due #Child::ToothChart::ToothDetails.upcoming_tooth_data_hash(params[:member_id])
        recommended_pointers = nil
        erupted_tooth_count = Child::ToothChart::ToothDetails.where(child_id:params[:member_id]).erupt_data.count
        updated_at =  Child::ToothChart::ToothDetails.where(child_id:params[:member_id]).not_in_unaccomplished_status.order_by("updated_at desc").first.updated_at.convert_to_ago2 rescue nil
        updated_at_text = updated_at.present? ? "Updated: #{updated_at}" : "Not updated yet"
        data = {:tooth_stats=> tooth_stats_hash,
                :tooth_overdue=>overdue_tooth,
                :upcoming_tooth_development=> upcoming_tooth_hash,
                :recommended_pointers=> recommended_pointers,
                :tooth_chart_data=> child_tooth_detail_hash,
                :title=>"#{(member.first_name.downcase.titleize rescue '')} has grown #{erupted_tooth_count} teeth",
                :updated_at=>updated_at_text
              }
      end
      status =  StatusCode::Status200
    rescue Exception=>e 
      status =  StatusCode::Status100
      @error = e.message
    end
    data.merge!({ :free_trial_end_date_message=>info_data[:free_trial_end_date_message],
      :subscription_box_data=>info_data[:subscription_box_data],
      system_tool_id:tool.id,
      show_subscription_box:show_subscription_box_status,  
      status: status, 
      tooth_chart_data: child_tooth_detail_hash,
      tooth_chart_data_key_order: child_tooth_detail_hash.keys,
      :error=>@error }
      )
    
    render json: data
  end

  def get_tooth_manager_data
    data = Child::ToothChart::ToothDetails.get_tooth_manager_data_hash(params[:member_id])
    render json: {status:  StatusCode::Status200, tooth_details: data}
  end
 
  #  api :Post, "chart_manager/add_child_tooth_detail", "Add tooth data of eurpted/shedded type for a child"
  #  param :member_id, String, desc: 'id of child member', required: true
  #  param :teeth, Array, desc: 'tooth chart manager ids with status to be added', required: true
  #  param :type, ["erupted", "shedded"], desc: 'add tooth detail for erupted/shedded tooth', required: true
  #  description 'Controller: api/v4/child/tooth_chart/chart_manager Action: update_child_tooth_detail'
  #  meta jira_id: "https://nurturey.atlassian.net/browse/API-215"
  #
  # {"token"=>"CqVB3nMgMdeQVmj14KmR", "member_id"=>"59ae60528cd03dd94a00000b", {:teeth => [{:chart_manager_id=>"",:tooth_status=>""}], "type"=>"Erupted"}
  
  #
  def add_child_tooth_detail
    begin
      type_param
     
      # params["tooth_manager_id"].each do |a|
        # params[:teeth] << {:chart_manager_id=>a,:tooth_status=>"accomplished"}.with_indifferent_access
      # end
      Child::ToothChart::ToothDetails.add_teeth(current_user,params)
      message = Message::API[:success][:add_tooth]
      status = StatusCode::Status200
    rescue Exception=>e
      message = e.message #Message::API[:error][:add_tooth]
      status = StatusCode::Status100
      @error = e.message
    end

    render json: { status: status, error: @error, message: message }
  end
  
  #  api :PUT, "chart_manager/update_child_tooth_detail", "Update tooth data of eurpted/shedded type for a child"
  #  param :id, String, desc: 'id of tooth', required: true
  #  param :date, String, desc: 'date', required: true
  #  param :type, ["erupted", "shedded"], desc: 'updated tooth detail for erupted/shedded tooth', required: true
  #  param :tooth_status, ["accomplished", "unaccomplished","unknown"], desc: 'tooth status of erupted/shedded,Default:accomplished', required: false
  #  param :status_by, ["user", "system"], desc: 'tooth updated by user or handhold or background job,Default:user', required: false
  #  description 'Controller: api/v4/child/tooth_chart/chart_manager Action: update_child_tooth_detail'
  #  meta jira_id: "https://nurturey.atlassian.net/browse/API-215"
  #
  # {"type"=>"erupted",:tooth_status=>"accomplished/unaccomplished", "date"=>"16 Feb 2018", "token"=>"CqVB3nMgMdeQVmj14KmR", "id"=>"5a85863f8cd03d81ec000007"}
  #
  def update_child_tooth_detail
    begin
      type_param
      status_by = params[:status_by] || "user"
      raise "Please provide valid date" if params[:date].present? && params[:date].to_date > Date.today
      Child::ToothChart::ToothDetails.update_tooth(current_user,params,{:status_by=>status_by})
      status = StatusCode::Status200
      message = Message::API[:success][:update_tooth]
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
      message = e.message #Message::API[:error][:update_tooth]
    end
    render json: { status: status, error: @error, message: message }
  end

  #params={:id=>,type=>"erupted/shedded/all/nil"}
  def destroy
    begin
      @child_tooth_detail = Child::ToothChart::ToothDetails.find(params[:id])
      @child_tooth_detail.delete_tooth(current_user,params)
      status = StatusCode::Status200
      message = Message::API[:success][:delete_tooth]
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
      message = Message::API[:error][:delete_tooth]
    end
    render json: { status: status, error: @error, message: message }
  end
  
  def set_api_version
    @api_version = 6
  end
  
  private
  def calculate_percentage(total,available)
    percentage = {}
    total.each do |k,v|
      begin
        percentage[k] =  ( (available[k][:count].to_f/total[k][:count]) *100  )
      rescue Exception=>e 
         
        percentage[k] = 0
      end
    end  
    percentage
  end

  def type_param
    if params[:type].present?
      params[:type] = params[:type].downcase 
      params[:type] = Child::ToothChart::ToothDetails::Shed if params[:type] == "shadded" || params[:type] =="shad"
      params[:type] = Child::ToothChart::ToothDetails::Erupt if params[:type] =="erupt"
    end
  end

end
