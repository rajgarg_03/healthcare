class Api::V6::Child::VisionHealth::ColorVisionTestResultController < ApiBaseController
  before_filter :set_api_version


  def index
    begin
      member_id = params[:member_id]
      child =  ChildMember.find(member_id)
      member = child
      family_id = member.member_families.first.family_id
      vision_health =  []
      options = {:get_data_for_from_list=>"Eye Sight"}
      options[:family_id] = family_id
     # measurement = Health.get_latest_height_weight(current_user,child).get_unit_converted_record(current_user) rescue nil 
      #create or update records.  
      data = Child::VisionHealth::TestColorVisionApi.get_results(member_id)
      Child::VisionHealth::CvtTest.create_or_update_record(current_user,@api_version,data,member,options) if data.present? 
     
      event_on_vision = Child::VisionHealth::VisionTestEvent.get_event(member_id) 
      screen_name = child.get_screen_name_for_cvtme(event_on_vision)
      event_type = (event_on_vision != nil ? event_on_vision.event_type : nil)
      family = FamilyMember.where(member_id:child.id).last.family
      
      recommended_pointer = nil #ArticleRef.get_recommended_pointer(current_user,child.id,"health")
      
      @tool, @show_subscription_box_status,@info_data,eye_chart = member.get_subscription_show_box_data(current_user,"Vision Health",@api_version,options)
      if @show_subscription_box_status == false
        eye_chart = Child::HealthCard::EyeChart.get_list(member_id)
      end

      
      eye_chart_data = eye_chart #Child::HealthCard::EyeChart.order_by_date(member_id).first
      eye_chart_data = {} if eye_chart_data.nil? 
      last_records = { eye_chart: eye_chart.first}
      vision_health << { eye_chart: eye_chart_data.first }  

      
      
      cvtme_purchase = family.cvtme_purchase
      #@tool, @show_subscription_box_status,@info_data,vision_health_data = child.get_subscription_show_box_data(current_user,"Vision Health",@api_version,options)
      cvt_test = Child::VisionHealth::CvtTest.get_last_test_record(member_id)
      if @show_subscription_box_status == false
        vision_health << { cvtme: cvt_test }
      end

      #summary =  {:height=>(measurement["height"] rescue nil) ,:weight=>(measurement["weight"] rescue nil),:bmi=>(measurement["bmi"].to_f rescue nil),:blood_group=>child.blood_group }

      render :json => {
                       :free_trial_end_date_message=>@info_data[:free_trial_end_date_message],
                       :subscription_box_data=>@info_data[:subscription_box_data],
                       :show_subscription_box=> @show_subscription_box_status, 
                       :system_tool_id=> @tool.id, 
                       :last_records=> last_records, 
                       :vision_health=> vision_health , 
                       :recommended_pointers=>recommended_pointer,
                       :status=>200,
                       :cvtme_purchase=> cvtme_purchase, 
                        
                       :event_on_vision => event_type,
                       :screen_name => screen_name
                      }
    rescue Exception=> e 
      render :json => {:status=>100, :message=>e.message  }
    end
  end



  #http://localhost:3000/api/v5/child/cvtme/color_vision_test_result/result?token=jebGYqVxy7X4t7rzBv3s
  def result
  	begin
      member_id =  params[:member_id]
      member =  Member.find(member_id)
      if member == nil 
        render :json=> {:message=> "Member does not exist.", :status=> StatusCode::Status100}
      end

      family_id = member.member_families.first.family_id
      family = Family.find(family_id)
      options = {:family_id => family_id }
      aicc_sid = params[:aicc_sid]

      
      tool, show_subscription_box_status, info_data, vision_test_dummy_data = member.get_subscription_show_box_data(current_user,"Vision Health",@api_version)
      event_on_vision = Child::VisionHealth::VisionTestEvent.get_event(member_id) 
      screen_name = member.get_screen_name_for_cvtme(event_on_vision)
      event_type = (event_on_vision != nil ? event_on_vision.event_type : nil)
      

      #fetch data from vision api
      data = Child::VisionHealth::TestColorVisionApi.get_result(aicc_sid)

 	    if data["error"] == nil
        #create or update records.	
        Child::VisionHealth::CvtTest.create_or_update_record(current_user,@api_version,[data],member,options)
      end
      
      cvt_test = nil
      
      cvtme_purchase = family.cvtme_purchase
      status = StatusCode::Status200
      msg = Message::API[:success][:cvt_record_fetched]

      if show_subscription_box_status == false
        #prepare response
        cvt_test = Child::VisionHealth::CvtTest.prepare_result(aicc_sid,member_id)      
        render :json=>{
                       :test_data=>cvt_test, 
                       :status=>status, 
                       :message=>msg,
                       :free_trial_end_date_message=>info_data[:free_trial_end_date_message],
                       :subscription_box_data=>info_data[:subscription_box_data],
                       :system_tool_id => tool.id, 
                       :show_subscription_box => show_subscription_box_status, 
                       :event_on_vision => event_type,
                       :screen_name => screen_name,
                       :cvtme_purchase=>cvtme_purchase
                     }
      else
        #prepare response
        cvt_test = System::SampleDataForTool.where(tool_identifier:"Vision Health").first
        render :json=>{:test_data=>cvt_test, 
                       :status=>status, 
                       :message=>msg,
                       :free_trial_end_date_message=>info_data[:free_trial_end_date_message],
                       :subscription_box_data=>info_data[:subscription_box_data],
                       :system_tool_id=>tool.id, 
                       :show_subscription_box=>show_subscription_box_status,
                       :event_on_vision => event_type,
                       :screen_name => screen_name,
                       :cvtme_purchase=>cvtme_purchase

                     }
      end

    rescue Exception=> e 
      render :json=> {:message=> e.message, :status=> StatusCode::Status100}
    end
  end


  def results
  	begin
      member_id =  params[:member_id]
      member =  Member.find(member_id)

      family_id = member.member_families.first.family_id
      family = Family.find(family_id)
      options = {:family_id => family_id }
      
      event_on_vision = Child::VisionHealth::VisionTestEvent.get_event(member_id)
      event_type = event_on_vision.event_type rescue  nil
      screen_name = member.get_screen_name_for_cvtme(event_on_vision)
      
      
      tool, show_subscription_box_status, info_data, vision_test_dummy_data = member.get_subscription_show_box_data(current_user,"Vision Health",@api_version)
      data = Child::VisionHealth::TestColorVisionApi.get_results(member_id)

      #create or update records.  
      Child::VisionHealth::CvtTest.create_or_update_record(current_user,@api_version,data,member,options) if data.present? 
      
      cvtme_purchase = family.cvtme_purchase
      status = StatusCode::Status200
      msg = Message::API[:success][:cvt_record_fetched]

      if show_subscription_box_status == false
        response_data = Child::VisionHealth::CvtTest.prepare_child_test_results(member_id)
      else
        response_data = vision_test_dummy_data
      end
        render :json=>{:test_data=>response_data, 
               :status=>status, 
               :message=>msg,
               :free_trial_end_date_message=>info_data[:free_trial_end_date_message],
               :subscription_box_data=>info_data[:subscription_box_data],
               :system_tool_id=>tool.id, 
               :show_subscription_box=>show_subscription_box_status, 
               :event_on_vision => event_type,
               :screen_name => screen_name,
               :cvtme_purchase=>cvtme_purchase
              }
    rescue Exception=> e 
      render :json=> {:message=> e.message, :status=> StatusCode::Status100}
    end
  end

  # Put
  #/api/v5/child/cvtme/color_vision_test_result/update_family_purchase_for_cvtme?token=jebGYqVxy7X4t7rzBv3s
  # params = {family_id=>,purchase_type=>"free/premium",:member_id=>"",:platform=>"ios"}
  def update_family_purchase_for_cvtme
    begin
      member_id =  params[:member_id]
      family_id = params[:family_id]
      family = Family.find(family_id)
      purchase_type = params[:purchase_type]
      options = {:api_veriosn=>@api_version,:platform=>params[:platform],:member_id=>member_id,:family_id=>family_id,:update_purchase_history=>true,:sync_data=>true}
      cvtme_purchase = ::Child::VisionHealth::Purchase.update_family_purchase_for_cvtme(current_user,@api_version,purchase_type,family_id,options)
      cvtme_purchase = family.cvtme_purchase
      status = StatusCode::Status200
      msg = Message::API[:success][:update_family_purchase_for_cvtme]
    rescue Exception=> e 
      @error = e.message
      status = StatusCode::Status100
      cvtme_purchase = nil
      msg = Message::API[:error][:update_family_purchase_for_cvtme]
    end
    render :json=> {:error=> @error,:mesage=>msg, :status=> status,cvtme_purchase:cvtme_purchase}
  end

  def set_api_version
    @api_version = 6
  end

end