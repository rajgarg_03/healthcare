class Api::V6::Child::HealthCard::SkinAssessmentController < ApiBaseController
  before_filter :set_api_version

  def get_list
    data = {}
    round_off_val = params[:round_off_val] || 2
    begin
      skin_assessment_histroy = Child::SkinAssessment.get_list(params[:member_id],current_user,round_off_val)
      status =  StatusCode::Status200
    rescue Exception=>e
      status =  StatusCode::Status100
      error = e.message
    end

    data.merge!({status: status, error: error, skin_assessments: skin_assessment_histroy })
    render json: data
  end

  #token=CA7cZbxNUkmu8GVidX9a&member_id=594ab0788cd03dff13000001&skin_assessment_id=xxxx
  def skin_assessment_details
    begin
      @member = Member.find(params[:member_id])
      round_off_val = params[:round_off_val] || 2
      skin_assessment_record =  ::Child::SkinAssessment.where(id:params[:skin_assessment_id]).last
      latest_skin_assement_record = ::Child::SkinAssessment.latest_assessment(params[:member_id])
      graph_data = latest_skin_assement_record.data_for_graph rescue {}
      skin_assessment_record = skin_assessment_record || latest_skin_assement_record
      assessment_record_exist_status = skin_assessment_record.present?
      skin_assessment_enable_status = get_skin_assessment_active_status(assessment_record_exist_status,tool_for="child",feature_type="all" )
      skin_assessment_score =  ::Child::SkinAssessment.get_skin_assessment_score( @member.id,!skin_assessment_enable_status)
      
      skin_assessment_score_in_text = ::Child::SkinAssessment.skin_assessment_score_to_text(skin_assessment_score)
      skin_assessment_score_analysis = ::Child::SkinAssessment.skin_assessment_score_analysis(skin_assessment_score,@member)

      photo_taken_date =  skin_assessment_record.assessment_date.to_date.to_date4 rescue nil
        if skin_assessment_record.present?
          # :skin_assessment_analysis=>skin_assessment_score_analysis
          render :json=> {
            :skin_assessment_score => skin_assessment_score_in_text,
            :skin_assessment_score_values=>skin_assessment_score,
            :photo_taken_date=> photo_taken_date,
            :status => StatusCode::Status200,
            :skin_assessment=>skin_assessment_record.in_json(@member,round_off_val),
            :skin_assessment_graph_data=>graph_data
          }
      else
        render :json=> {:status => StatusCode::Status200}
      end
    rescue Exception=> e 
      render :json=>{:message=> e.message,:status=>StatusCode::Status100,:error=>e.message}
    end
  end

  def upload_image_for_skin_assessment
    begin
      @member = Member.find(params[:member_id])
      if params[:media_files].present?
        record,error_message = ::Child::SkinAssessment.create_record(@member,current_user,params,@api_version)
        raise error_message if error_message.present?
      else
        raise 'Please upload image for assessment'
      end  
      status = StatusCode::Status200
      message = Message::API[:success][:upload_image_for_skin_assessment]
    rescue Exception=>e
      Rails.logger.info e.message
      error = e.message
      message = error_message || Message::API[:error][:upload_image_for_skin_assessment]
      status =  StatusCode::Status100
    end
    render json: {error: error, message: message, status: status }
  end

   
  # token=CA7cZbxNUkmu8GVidX9a&member_id=5a00a8618cd03dd3b2000202&skin_assessment_id"
  def delete_skin_assessment
    begin
      skin_assessment = ::Child::SkinAssessment.find(params[:skin_assessment_id])
      skin_assessment.delete_record
      status = StatusCode::Status200
      message = Message::API[:success][:delete_skin_assessment]
    rescue Exception=>e
      status = StatusCode::Status100
      error = e.message
      message = Message::API[:error][:delete_skin_assessment]
    end
    render json: { status: status, error: error, message: message }
  end
  
  def set_api_version
    @api_version = 6
  end
  
  private

  def get_skin_assessment_active_status(record_exist,tool_for="child", feature_type="all")
    skin_assessment_tool_id = ::Child::SkinAssessment.get_tool_id
    if record_exist.present?
      status = Nutrient.is_tool_available?(skin_assessment_tool_id,current_user,@api_version,tool_for,feature_type)
    else
      status = false
    end
  end

end
