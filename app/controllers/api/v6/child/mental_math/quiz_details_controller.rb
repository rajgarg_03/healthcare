class Api::V6::Child::MentalMath::QuizDetailsController < ApiBaseController
  before_filter :set_api_version

  # POST /api/v4/child/mental_math/quiz_details/update_quiz_details.json?
  # params
  # quiz_details[quiz_level]=3
  # quiz_details[question]=This is dummy question
  # quiz_details[answer]=This is dummy ans
  # quiz_details[question_type]=Addition
  # quiz_details[subject]=subject
  # quiz_details[answered]=1
  # quiz_details[member_id]=5afe6b3bbf42274438000003
  # quiz_details[time_taken]=12
  # quiz_details[platform]=ios
  # token=dfhqH3C8tQWsHdLq2fYa
  def add_quiz_details
    begin
      add_quiz_check_blank
      quiz_detail = Child::AlexaQuiz::QuizDetail.new
      options= {:current_user=>current_user, :ip_address=>request.remote_ip}
      quiz_detail.create_quiz(params,options)
      quiz_info = Child::AlexaQuiz::QuizDetail.child_quiz_info(current_user,params[:quiz_details][:member_id],@api_version,params)
      status = StatusCode::Status200
      message = Message::API[:success][:add_quiz_details]
    rescue Exception=> e
      status = StatusCode::Status100
      @error = e.message
      message = e.message
    end
    json = { :status=>status,:error=> @error, :message=> message,:subscription_status=> quiz_info[:subscription_status], :valid_to_take_quiz=> quiz_info[:valid_to_take_quiz], :attempted_question_count=>quiz_info[:attempted_question_count] }
    render :json=> json
  end

  
  # GET localhost:3000/api/v4/child/mental_math/quiz_details/complete_summary.json?
  # params token=dfhqH3C8tQWsHdLq2fYa&member_id=5abb1887bf4227224e00000b
  def complete_summary
    begin
      member = Member.find(params[:member_id])
      options = {:get_data_for_from_list=>"Complete Summary"}
      @tool, @show_subscription_box_status,@info_data,sample_data = member.get_subscription_show_box_data(current_user,'Mental Maths',@api_version,options)
      data = Child::AlexaQuiz::QuizDetail.get_complete_summary(params,current_user,@api_version)
      status = StatusCode::Status200
    rescue Exception=> e
      status = StatusCode::Status100
      @error = e.message
    end
    json = {:free_trial_end_date_message=>@info_data[:free_trial_end_date_message],:subscription_box_data=>@info_data[:subscription_box_data], :status=>status,:error=> @error,:system_tool_id=>@tool.id,:show_subscription_box=>@show_subscription_box_status,:is_google_mental_math_launched=>false }
    json.merge!(data) unless data.blank?
    render :json=> json
  end

  # GET /api/v4/child/mental_math/quiz_details/complete_summary.json?
  # params token=dfhqH3C8tQWsHdLq2fYa&member_id=5abb1887bf4227224e00000b&page=1
  def daily_summary
    begin
      params[:page] =  (params[:page] || 1).to_i rescue 1
      data = Child::AlexaQuiz::QuizDetail.get_daily_summary(params) if validate_member_id
      status = StatusCode::Status200
    rescue Exception=> e
      status = StatusCode::Status100
      @error = e.message
    end
    json = { :status=>status,:error=> @error }
    json.merge!(data) unless data.blank?
    render :json=> json
  end


  # Put /api/v5/child/mental_math/quiz_details/mental_math_banner_skipped.json?
  # Params = {token:""}
  def mental_math_banner_skipped
    begin
      status = StatusCode::Status200
      member_popup = PopupLaunch.where(member_id:current_user.id).last
      count = member_popup["mental_math_launch_count"].to_i + 1  
      options = {:skipped => true}
      PopupLaunch.update_mental_math_free_popup_launch_count(current_user.id,current_user.device_platform,count,options)
      if params[:reset].to_s == "true"
        popup = PopupLaunch.where(member_id:current_user.id).last
        popup.mental_math_launch_count = nil
        popup_info = popup.popup_info
        popup_info["mental_math_free"] = {}
        popup.popup_info = popup_info
        popup.save
      end
    rescue Exception=>e 
      status = StatusCode::Status100
      @error = e.message
    end
    render :json=>{:status=>status,:error=>@error}
  end 
  
  def set_api_version
    @api_version = 6
  end
  
  private

    # TODO: Delete, no use 
    def validate_member_id
      member_ids = Child::AlexaQuiz::QuizDetail.pluck(:member_id).map(&:to_s)
      unless member_ids.include?(params[:member_id])
        @error = "Member have not any record"
        return false
      else
        return true
      end
    end

    def add_quiz_check_blank
      if ValidateParams.check_blank(params[:quiz_details][:question])
        raise 'Please provide question' 
      elsif ValidateParams.check_blank(params[:quiz_details][:question_type])
        raise 'Please provide topic'
      elsif ValidateParams.check_blank(params[:quiz_details][:subject])
        raise 'Please provide subject'
      end
    end
end
