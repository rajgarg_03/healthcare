class Api::V6::Clinic::PharmacyController < ApiBaseController
  before_filter :set_api_version
  before_filter :validate_user_gpsoc_limit
  
  # Get
  # /api/v6/clinic/pharmacy/get_user_nominated_pharmacy.json?token=541ebcb88cd03ddfc4000002
  # params = {:token=>'541ebcb88cd03ddfc4000002',:member_id=>'5df9fa698cd03d42d80001ab' }  
  def get_user_nominated_pharmacy
    begin
      options = {}
      member_id = params[:member_id]
      member = Member.find(member_id)
      options[:current_user] = current_user
      options[:request_data] = params[:request_data] || {}
      response = Clinic::Pharmacy.user_nominated_pharmacy(current_user,member,options)
      @reauthentication_required = current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      @error = e.message
    end
    render :json=>response.merge(:reauthentication_required=>@reauthentication_required)
  end
   


  # Get
  # /api/v6/clinic/pharmacy/list_pharmacies.json?token=541ebcb88cd03ddfc4000002
  # params = {:token=>'541ebcb88cd03ddfc4000002',:member_id=>'5df9fa698cd03d42d80001ab',:request_data=>{:Postcode=>"BR5 1QB",:SearchDistance=>"200"} }  
  def list_pharmacies
    begin
      options = {}
      member_id = params[:member_id]
      member = Member.find(member_id)
      options[:current_user] = current_user
      options[:request_data] = params[:request_data] || {}
      response = Clinic::Pharmacy.list_pharmacies(current_user,member,options)
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      @error = e.message
    end
    render :json=>response.merge(:reauthentication_required=>@reauthentication_required)
  end


  # Put
  # /api/v6/clinic/pharmacy/update_pharmacy_nomination.json?token=541ebcb88cd03ddfc4000002
  # params = {:token=>'541ebcb88cd03ddfc4000002',:member_id=>'5b6d929a8cd03d63360003c7',:request_data=>{:NacsCode=>"",:Postcode=>""} }  
  def update_pharmacy_nomination
    begin
      options = {}
      member_id = params[:member_id]
      member = Member.find(member_id)
      options[:current_user] = current_user
      options[:request_data] = params[:request_data] || {}
      response = Clinic::Pharmacy.update_pharmacy_nomination(current_user,member,options)
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      @error = e.message
    end
    render :json=>response.merge(:reauthentication_required=>@reauthentication_required)
  end

  # Delete
  # /api/v6/clinic/pharmacy/delete_pharmacy_nomination.json?token=541ebcb88cd03ddfc4000002
  # params = {:token=>'541ebcb88cd03ddfc4000002',:member_id=>'5b6d929a8cd03d63360003c7',:request_data=>{:NacsCode=>""} }  
  def delete_pharmacy_nomination
    begin
      options = {}
      member_id = params[:member_id]
      member = Member.find(member_id)
      options[:current_user] = current_user
      options[:request_data] = params[:request_data] || {}
      response = Clinic::Pharmacy.delete_pharmacy_nomination(current_user,member,options)
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      @error = e.message
    end
    render :json=>response.merge(:reauthentication_required=>@reauthentication_required)
  end
   
  def set_api_version
    @api_version = 6
  end

end
