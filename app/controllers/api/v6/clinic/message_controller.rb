class Api::V6::Clinic::MessageController < ApiBaseController
  before_filter :set_api_version
  before_filter :validate_user_gpsoc_limit

  # Get
  # /api/v6/clinic/message/list_messages.json?token=541ebcb88cd03ddfc4000002
  # params = {:token=>'541ebcb88cd03ddfc4000002',member_id=>'5b6d929a8cd03d63360003c7'}  
  def list_messages
    begin
      options = {}
      member_id = params[:member_id]
      member = Member.find(member_id)
      options[:current_user] = current_user
      response = ::Clinic::Message.get_message_list(current_user,member,options)
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      @error = e.message
    end
    render :json=>response.merge(:reauthentication_required=>@reauthentication_required)
  end
  
  # Emis only
  # Get 
  # /api/v6/clinic/message/get_recipients.json?token=541ebcb88cd03ddfc4000002 
  # params = {:token=>'541ebcb88cd03ddfc4000002',member_id=>'5b6d929a8cd03d63360003c7'}  
  def get_recipients
    begin
      options = {}
      member_id = params[:member_id]
      member = Member.find(member_id)
      options[:current_user] = current_user
      response = ::Clinic::Message.get_message_receiptent_list(current_user,member,options)
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      @error = e.message
    end
    render :json=>response.merge(:reauthentication_required=>@reauthentication_required)
  end


  # Get
  # /api/v6/clinic/message/get_message_detail.json?token=541ebcb88cd03ddfc4000002
  # params = {:message_id=>"", :token=>'541ebcb88cd03ddfc4000002',member_id=>'5b6d929a8cd03d63360003c7'}  
  def get_message_detail
    begin
      options = {}
      member_id = params[:member_id]
      message_id = params[:message_id]
      member = Member.find(member_id)
      options[:current_user] = current_user
      message_read_state =  "Read"
      update_message_response = ::Clinic::Message.update_message_read_status(current_user,member,message_id,message_read_state,options) rescue nil
     
      response = ::Clinic::Message.get_message_detail(current_user,member,message_id,options)
      
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      @error = e.message
    end
    render :json=>response.merge(:reauthentication_required=>@reauthentication_required)
  end

  # Delete
  # /api/v6/clinic/message/delete_message.json?token=541ebcb88cd03ddfc4000002
  # params = {:message_id=>"", :token=>'541ebcb88cd03ddfc4000002',:member_id=>'5b6d929a8cd03d63360003c7'}  
  def delete_message
    begin
      options = {}
      member_id = params[:member_id]
      message_id = params[:message_id]
      member = Member.find(member_id)
      options[:current_user] = current_user
      response = ::Clinic::Message.delete_message(current_user,member,message_id,options)
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      @error = e.message
    end
    render :json=>response.merge(:reauthentication_required=>@reauthentication_required)
  end
  
  # Put
  # /api/v6/clinic/message/update_message_read_status.json?token=541ebcb88cd03ddfc4000002
  # params = {:subject=>"",:message=>"", :message_id=>"", message_read_state=>'Unread/Read', :token=>'541ebcb88cd03ddfc4000002',:member_id=>'5b6d929a8cd03d63360003c7'}  
  def update_message_read_status
    begin
      options = {}
      member_id = params[:member_id]
      message_id = params[:message_id] || params[:message_ids]
      message_read_state = params[:message_read_state].titleize rescue "Read"
      member = Member.find(member_id) 
      options[:current_user] = current_user
      if message_id.present?
        response = ::Clinic::Message.update_message_read_status(current_user,member,message_id,message_read_state,options)
      else
        response = {:status=>100,:message=>"No Message ID found"}
      end
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      @error = e.message
    end
    render :json=>response.merge(:reauthentication_required=>@reauthentication_required)
  end

  # post
  # /api/v6/clinic/message/send_message.json?token=541ebcb88cd03ddfc4000002
  # params = {:subject=>"cc",:message=>"cc", :message_id=>"", :recipients=>[], :token=>'541ebcb88cd03ddfc4000002',:member_id=>'5eddd4078cd03d6b040003a0'}  
  # Tpp
  # params = {:subject=>"cc",:message=>"cc", :message_id=>"", :recipients=>["d8aca00000000000"], :token=>'541ebcb88cd03ddfc4000002',:member_id=>'5eddd2dd8cd03d6b04000380'}  
  def send_message
    begin
      options = {}
      member_id = params[:member_id]
      message_id = params[:message_id]
      subject = params[:subject]
      msg = params[:message]
      member = Member.find(member_id)
      options[:current_user] = current_user
      options[:recipients] =   params[:recipients]
      options[:conversation_id] = message_id
      if params[:media_files].present?
        file = params[:media_files].last
        options[:file_name] =  file.original_filename
        options[:file_data] = Base64.strict_encode64(File.read(file.path))
      end
 
      response = ::Clinic::Message.send_message(current_user,member,subject,msg,options)
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      @error = e.message
    end
    render :json=>response.merge(:reauthentication_required=>@reauthentication_required)
  end

  # Get
  #  /api/v6/clinic/message/get_message_attachment
  # params ={:attachement_id=>"",member_id}
  def get_message_attachment
    begin
      options = {}
      member_id = params[:member_id]
      member = Member.find(member_id)
      options[:current_user] = current_user
      attachement_id = params[:attachement_id]
      response = ::Clinic::Message.get_message_attachement_data(current_user,member,attachement_id,options)
      @reauthentication_required = current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      @error = e.message
      Rails.logger.info "Error inside attachement_id ......... #{e.message}"
    end
    render :json=>response.merge(:reauthentication_required=>@reauthentication_required)
  end
  
  
  def set_api_version
    @api_version = 6
  end

end
