class Api::V6::Clinic::PrescribingController < ApiBaseController
  before_filter :set_api_version
  before_filter :validate_user_gpsoc_limit

  # Get
  # /api/v5/clinic/prescribing/list_medication_courses.json?token=541ebcb88cd03ddfc4000002
  # params = {:token=>'541ebcb88cd03ddfc4000002',member_id=>'5b6d929a8cd03d63360003c7'}  
  def list_medication_courses
    begin
      options = {}
      member_id = params[:member_id]
      member = Member.find(member_id)
      options[:current_user] = current_user
      response = Clinic::Prescribing.get_medication_courses(current_user,member,options)
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      @error = e.message
    end
    render :json=>response.merge(:reauthentication_required=>@reauthentication_required)
  end

  # Post
  # /api/v5/clinic/prescribing/request_prescription.json?token=541ebcb88cd03ddfc4000002
  # params = {request_comment=>"", :medication_course_ids=>[{:id=>"",:type=>""}], :token=>'541ebcb88cd03ddfc4000002',member_id=>'5b6d929a8cd03d63360003c7'}  
  def request_prescription
    begin
      options = {}
      member_id = params[:member_id]
      medication_course_ids = params[:medication_course_ids]
      member = Member.find(member_id)
      options[:current_user] = current_user
      options[:request_data] = {}
      options[:request_data][:RequestComment] = params[:request_comment] if params[:request_comment].present?
      response = Clinic::Prescribing.request_prescription(current_user,member,medication_course_ids,options)
      @reauthentication_required = current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      @error = e.message
    end
    render :json=>response.merge(:reauthentication_required=>@reauthentication_required)
  end

  # Get
  # /api/v5/clinic/message/send_message.json?token=541ebcb88cd03ddfc4000002
  # params = {from_date=>"", :token=>'541ebcb88cd03ddfc4000002',:member_id=>'5b6d929a8cd03d63360003c7'}  
  def get_prescription_requests
    begin
      options = {}
      member_id = params[:member_id]
      from_date = params[:from_date].blank? ? (Date.today - 1.year) : params[:from_date]
      member = Member.find(member_id)
      options[:current_user] = current_user
      response = Clinic::Prescribing.get_prescription_requests(current_user,member,from_date,options)
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      @error = e.message
    end
    render :json=>response.merge(:reauthentication_required=>@reauthentication_required)
  end


  # Delete
  # /api/v5/clinic/prescribing/cancel_prescription.json?token=541ebcb88cd03ddfc4000002
  # params = {:cancellation_reason=>, :request_id=>"", :token=>'541ebcb88cd03ddfc4000002',:member_id=>'5b6d929a8cd03d63360003c7'}  
  def cancel_prescription
    begin
      options = {}
      member_id = params[:member_id]
      message_id = params[:request_id]
      member = Member.find(member_id)
      options[:current_user] = current_user
      options[:cancellation_reason] = params[:cancellation_reason]
      response = Clinic::Prescribing.cancel_prescription(current_user,member,message_id,options)
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      @error = e.message
    end
    render :json=>response.merge(:reauthentication_required=>@reauthentication_required)
  end

 
  
  def set_api_version
    @api_version = 6
  end

end
