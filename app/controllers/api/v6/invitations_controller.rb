class Api::V6::InvitationsController < ApiBaseController
  before_filter :set_api_version
  
  # Accept/Reject invitation request from inside mobile app
  def update_status
    @invitation = Invitation.where(id:params[:invitation_id]).first
    family = @invitation.family
    if @invitation.nil? || @invitation.accepted? || @invitation.rejected?
      render :json=>{:message=> "Invitation is not active now.", :status=>StatusCode::Status100} and return
    end
    if params[:status] == 'accept'
  	  @invitation.accept
      count = @owner_requested_user ? 0 : 1
  	  @family_member = FamilyMember.find_or_initialize_by(:family_user_count=>count,:member_id => @invitation.member.id, :family_id => @invitation.family_id, :role => @invitation.role)
      api_message = @family_member.save ? Message::API[:success][:accept_invitation] : @family_member.errors.first
    elsif params[:status] == 'reject'
  	  @invitation.reject unless @invitation.rejected?	  	
  	  api_message = Message::API[:success][:decline_invitation]
    end
    find_sender_and_receiver_for_invitation
    message = "Your #{@owner_requested_user ? 'invitation' : 'request'} to join #{family.name} has been #{@invitation.status} by #{@receiver.first_name || @receiver.email}."
    member = @owner_requested_user ? current_user : @receiver
    add_notification(@sender,message , families_path, current_user,"family")
    if  params[:status] == 'accept'
      if @owner_requested_user
        member = @owner_requested_user ? current_user : @receiver
        UserCommunication::UserActions.new.invited_user_joins_family(member,@invitation)
      else
         UserCommunication::UserActions.new.admin_approves_family_join_request(@invitation.member,@invitation)
      end
    end
    response = {:invitation=> @invitation , message: api_message,  status: StatusCode::Status200}
    if params[:status] == 'accept'
      response.merge!({member: @family_member,welcome_to_family_screen:BasicSetupEvaluator.family_screen_data_prepare(family,{:api_version=>@api_version,:current_user=>current_user})}) 
    end
    respond_to do |format|
      format.json { render :json=> response  }
    end
  end
  
  # Get
  # api/v6/invitations/invite_friend_to_nurturey.json
  # Logs event for referral 
  def invite_friend_to_nurturey
    member_id = current_user.id
    if params[:source] != "settings"
      PopupLaunch.update_app_referral_popup_launch_count(member_id,count=1,options={app_refferal:true}) 
    end
    render :json=> {status:200}   
  end
  
  # Put
  # api/v6/invitations/skip_app_refferal_popup.json
  def skip_app_refferal_popup
    member_id = current_user.id
    PopupLaunch.update_app_referral_popup_launch_count(member_id,count=1,options={skipped:true})
    render :json=>{status:200} 
  end
  
  def set_api_version
    @api_version = 6
  end
  
  private

  def find_sender_and_receiver_for_invitation
    @sender = @invitation.sender_id.present? ? @invitation.sender : @invitation.member
    @receiver = @invitation.sender_id.present? ? @invitation.member : @invitation.family.owner
    @owner_requested_user = @invitation.sender_id.present?    
  end  

end