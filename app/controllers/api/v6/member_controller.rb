class Api::V6::MemberController < ApplicationController
  skip_before_filter  :verify_authenticity_token
  skip_before_filter :authenticate_user!, :only => [:verify_elder_for_family, :verify_elder]
  before_filter :set_api_version
  before_filter :validate_email_status, only: [:create_elder,:update_elder,:resend_invitation]
  require "RMagick"
  include Magick 

  def create_child
    begin
      @family = Family.find(params[:family_id])
      family_member = params[:Member].delete("family_members").merge!(family_id: @family.id)
      @child_member = ChildMember.new(params[:Member])
      @child_member.last_name =   @family.member.last_name rescue nil  
      options = {:save_handhold_data=>params[:add_handhold_data]}
      if @child_member.save
        @child_member.family_members.create(family_member)
        activated_tools,activated_tools_controller = @child_member.run_add_child_setup_contoller(@api_version,current_user,options)
        @message = Message::GLOBAL[:success][:create_child].html_safe #"Your child has been added. Visit <a href='families' data-remote=true>Manage Family</a>."
        Timeline.save_system_timeline_to_member(@child_member.id,current_user.id) unless request.format.json?
        Timeline.save_future_system_timeline_to_member(@child_member.id,current_user.id) if request.format.json?
        Member.add_system_entries(@child_member,current_user,@api_version)
        Health.new.health_setting(@child_member.id)
        family_member_obj = FamilyMember.where(member_id:@child_member.id.to_s).last
        Score.create(family_id: @family.id, activity_type: "Member", activity_id: @child_member.id, activity: "Added #{@child_member.first_name} to #{@family.name}", point: Score::Points["Child"], member_id: @child_member.id)
        #UserCommunication::UserActions.add_user_action(family_member_obj,"Manage Family",current_user,"add child")
        message = Message::API[:success][:create_child] + " #{@family.name}"
        status = StatusCode::Status200
      else
        message =  @child_member.errors.full_messages.join(",")
        status = StatusCode::Status100
      end
       @child_member["message"],@child_member["status"] = message, status
       @child_member["age"] = ApplicationController.helpers.calculate_age(@child_member.birth_date) rescue ""
       @child_member["role"] = @child_member.family_members.first.role rescue "Son"
     rescue Exception=>e
      @error = e.message
      status = StatusCode::Status100
      message = Message::API[:error][:create_child] #@child_member.errors.full_messages.join(",")
     end

     respond_to do |format|
      format.js
      if session[:alexa_user_token].present?
        format.json { render :json=>{:message=>message,:error=>@error, :family_members=>[@child_member], status: status } }
      else
        format.json { render :json=>{:activated_tools_controller=>activated_tools_controller,:message=>message,:error=>@error, :family_members=>[@child_member], :activated_tools => activated_tools, status: status } }
      end
    end

  end

  #  Multiple child add supported
  #  Post
  #  /api/v6/families/59afa6b38cd03de9400001bd/member/createChild.json
  #  params =  {"Member"=>[{"birth_date"=>"2016-09-22", "first_name"=>"Mike", "last_name"=>"Singh", "proxy_details"=>{"patient_uid"=>"5a09eb8f-7e0b-4630-ad48-87e8e05d124f", "practice_ods_code"=>"A28826"}, "family_members"=>{"role"=>"Son"}}, {"birth_date"=>"2013-04-22", "first_name"=>"Mike2", "last_name"=>"Singh2", "proxy_details"=>{"patient_uid"=>"46303b8f-4630-4630-ad48-87e8e32d14630", "practice_ods_code"=>"A28826"}, "family_members"=>{"role"=>"Son"}}], "activated_tools_controller"=>"true", "family_id"=>"59afa6b38cd03de9400001bd"}
  def createChild
    begin
      @family = Family.find(params[:family_id])
      childrens = []
      activated_tools_list = {}
      activated_tools_controller_list = {}
      proxy_linking_details = {}
      [params[:Member]].flatten.each do |params_member|
        member_data = params_member.select{|k,v| ["first_name","last_name","birth_date"].include?(k.to_s)}
        options = {:api_version => 6, :save_handhold_data=>params[:activated_tools_controller].to_s == "true", activated_tools_controller: params[:activated_tools_controller].to_s == "true"}
        @child_member = ChildMember.new(member_data)
        @child_member.last_name =  (@child_member.last_name || @family.member.last_name) rescue nil  
        if @child_member.save
          child_member_id = @child_member.id.to_s
          # Set role for member
          family_member = params_member.delete("family_members").merge!(family_id: @family.id)

          # family_member = {:role=>params_member[:role]}.merge!(family_id: @family.id)
           
          @child_member.family_members.create(family_member)
          
          # Setup proxy linking
          if params_member[:proxy_linking].to_s == "true"
            ods_code = params_member[:proxy_details][:practice_ods_code]
            patient_uid = params_member[:proxy_details][:patient_uid]
            member_id = child_member_id
            authenticate_status,msg = ::Clinic::LinkDetail.link_proxy_user(current_user,member_id,ods_code,patient_uid, options)
            proxy_linking_status = authenticate_status == "authenticated"
            proxy_linking_msg = msg
            clinic_link_detail = proxy_linking_status ? ::Clinic::LinkDetail.get_clinic_link_detail(member_id,current_user,options) : ::Clinic::LinkDetail.new
            proxy_linking_detail = {message:proxy_linking_msg, linked_status:proxy_linking_status,:user_info => clinic_link_detail.user_info}
          else
            proxy_linking_detail = {linked_status:false}
          end
          proxy_linking_details[child_member_id] = proxy_linking_detail
          @child_member.reload
          activated_tools,activated_tools_controller = @child_member.run_add_child_setup_contoller(@api_version,current_user,options)
           
          activated_tools_controller_list[child_member_id] = activated_tools_controller
          activated_tools_list[child_member_id] = activated_tools
          

          @message = Message::GLOBAL[:success][:create_child].html_safe #"Your child has been added. Visit <a href='families' data-remote=true>Manage Family</a>."
          Timeline.save_system_timeline_to_member(@child_member.id,current_user.id) unless request.format.json?
          Timeline.save_future_system_timeline_to_member(@child_member.id,current_user.id) if request.format.json?
          Member.add_system_entries(@child_member,current_user,@api_version)
          Health.new.health_setting(@child_member.id)
          family_member_obj = FamilyMember.where(member_id:@child_member.id.to_s).last
          Score.create(family_id: @family.id, activity_type: "Member", activity_id: @child_member.id, activity: "Added #{@child_member.first_name} to #{@family.name}", point: Score::Points["Child"], member_id: @child_member.id)
          #UserCommunication::UserActions.add_user_action(family_member_obj,"Manage Family",current_user,"add child")
          message = Message::API[:success][:create_child] + " #{@family.name}"
          status = StatusCode::Status200
        else
          status = StatusCode::Status100
          message =  @child_member.errors.full_messages.join(",")
        end
         child_attr = @child_member.attributes
         child_attr["is_expected"] = false
         child_attr["pregnancy_id"] = nil
         child_attr["pregnacy_status"] = nil
         # child_attr["birth_date"] = @child_member["birth_date"].to_time
         child_attr["message"],@child_member["status"] = message, status
         child_attr["age"] = ApplicationController.helpers.calculate_age(@child_member.birth_date) rescue ""
         child_attr["role"] = @child_member.family_members.first.role rescue "Son"
         
         childrens << child_attr
      end
      status = StatusCode::Status200
     rescue Exception=>e
      @error = e.message
      status = StatusCode::Status100
      message = Message::API[:error][:create_child] #@child_member.errors.full_messages.join(",")
     end

     respond_to do |format|
      format.js
      if session[:alexa_user_token].present?
        format.json { render :json=>{:message=>message,:error=>@error, :family_members=>childrens, status: status } }
      else
        format.json { render :json=>{:proxy_linking_details=>proxy_linking_details, :activated_tools_controller=>activated_tools_controller_list,:message=>message,:error=>@error, :family_members=>childrens, :activated_tools => activated_tools_list, status: status } }
      end
    end
  end

  def update_child 
    begin
      @family_member = FamilyMember.find(params[:id])
      @member = @family_member.member
      first_name = @member.first_name
      birth_date = @member.birth_date    
    rescue 
      # to support api in case send member id instead of family_member_id
      @member = Member.find(params[:id])
      @family_member = FamilyMember.where(member_id:@member.id).first
    end
    if params[:parent_member].present?
      @member.update_attributes(params[:parent_member].reject{|k| k == "family_member"})
      @family_member.update_attributes(params[:parent_member]["family_member"])
    else
      if @member.update_attributes(params[:child_member].reject{|k| k == "family_member"})

        @family_member.update_attributes(params[:child_member]["family_member"]) if params[:child_member]["family_member"]
      end  
    end
    if params[:child_member][:birth_date].present? && params[:child_member][:birth_date].to_date != birth_date
     @member.update_member_ref("update",{:birth_date_updated=>true})
    elsif params[:child_member][:first_name].present? && params[:child_member][:first_name] != first_name
      @member.update_member_ref("update",{:name_updated=>true})
    else
    end
    #NOTE: This block might not in use.
    if @pic_valid = !@member.profile_image || (@member.profile_image && @member.profile_image.valid?)
      if (params[:child_member] && params[:child_member][:profile_image_attributes].present?) || (params[:parent_member] && params[:parent_member][:profile_image_attributes].present?)
        crop
        @member.profile_image.reprocess_image
      end
      api_message = Message::API[:success][:upload_profile_picture]
    else
      api_message = Message::API[:success][:update_child]
    end
    if params[:media_files].present?
      picture = Picture.new(local_image: params[:media_files],upload_class: "pic",member_id: @member.id)
      @member.profile_image = picture if picture.valid?
    end  
    #UserCommunication::UserActions.add_user_action(@family_member,"Manage Family",current_user,"add child")

    @member["age"] = ApplicationController.helpers.calculate_age(@member.birth_date) rescue ""
    respond_to do |format| 
      format.js
      format.json { render :json=> {:member=> @member,message: api_message, status: StatusCode::Status200}  }
    end    
  end

  def crop
     selected_member =  @member.class.to_s.underscore
     coord = params[selected_member]["profile_image_attributes"]
    if coord["crop_w"].to_i &&  coord["crop_h"].to_i
     
      local_image = params[selected_member][:profile_image_attributes].delete("local_image")
      image = @family_member.member.profile_image
      local_image1 = local_image ||  URI.parse(image.photo_path_str(:original)) if !FileTest.exist?(image.local_image.path)
      if local_image1.present?
        image.local_image= local_image1
        image.local_image.save
      end
      File.open(image.photo_path_str(:original), "rb") do |f|
        @orig_img = Magick::Image::from_blob(f.read).first
      end
      width = @orig_img.columns > 504 ? 504 : @orig_img.columns
      height = @orig_img.rows > 285 ? 285 : @orig_img.rows
      @orig_img.resize!(width,height) if (width == 504 || height == 285)
      @orig_img.crop!(coord["crop_x"].to_i,coord["crop_y"].to_i , coord["crop_w"].to_i, coord["crop_h"].to_i)
      @orig_img.write(image.local_image.path(:original))
    end
   end

  def delete_child    
    @member = Member.find(params[:id])
    @member.delete_child_member
    respond_to do |format| 
      format.js
      format.json { render :json=> {:member=> @member, message: Message::API[:success][:delete_child], status: StatusCode::Status200}  }
    end
  end  
  
  # Post api api/v3/member/set_metric_system.json?token=UpLZuMvytGdp8ejSsd5d
  # params={"subscibe_mails"=>{ :0 => "off" },:subscribe_notification=>off}
  def set_metric_system
    begin
      length_metric_system = current_user.member_settings.where(name:"length_metric_system").first
      weight_metric_system = current_user.member_settings.where(name:"weight_metric_system").first
      if params[:length_unit].present?
        if length_metric_system.blank?
          MemberSetting.create({member_id: current_user.id, name:"length_metric_system", status:params[:length_unit]})
        else
          length_metric_system.update_attributes({status:params[:length_unit]})
        end
      end
      if params[:weight_unit].present?
        if weight_metric_system.blank?
          MemberSetting.create({member_id: current_user.id, name:"weight_metric_system", status:params[:weight_unit]})
        else
          weight_metric_system.update_attributes({status:params[:weight_unit]})
        end
      end
      if params[:time_zone].present? && current_user.time_zone != params[:time_zone]
        current_user.update_attribute(:time_zone,params[:time_zone].strip)
      end  
      length_metric_system = current_user.member_settings.where(name:"length_metric_system").first
      weight_metric_system = current_user.member_settings.where(name:"weight_metric_system").first
      subscription_mail_present = params[:subscibe_mails].present?
      params[:subscibe_mails].delete_if { |k, v| (v=="0"|| v=="off" || v=="false") } if subscription_mail_present
      
      current_user.subscribe_mail((params[:subscibe_mails]||{}).keys) if subscription_mail_present
      
      if params[:subscribe_notification].present?
        sub_noti = current_user.member_settings.where(name:"subscribe_notification").first ||  MemberSetting.create(name:"subscribe_notification",member_id:current_user.id)
        status_val = params[:subscribe_notification].downcase == "on" ? true : false
        sub_noti.update_attribute(:status,status_val)
        current_user.subscribe_notification(params[:subscribe_notification])
      end 
        current_user.reload
      respond_to do |format|
        format.js {render :layout=> false}
        format.json { render :json=> {:status=>200, :message =>Message::API[:success][:update_setting] ,:time_zone=>current_user.time_zone,:mailer_setting=>(MemberSetting.where(member_id:current_user.id, name:"mail_subscribe").first.note.present? rescue true),:notification=>current_user.notification_subscribed?, :metric_system=>{:length=>(length_metric_system.status rescue "kgs"),:weight=>(weight_metric_system.status rescue "cms")} } }
      end
     rescue  Exception => e
       render :json=> {:message=>Message::API[:error][:update_setting], :status=> 100, :error=>e}
     end
  end
 
  def create_elder
    @family = Family.find(params[:family_id])    
    @elder_member = ParentMember.where(:email => params[:ParentMember][:email].downcase).first
    if @elder_member.blank?
      @elder_member = ParentMember.new(params[:ParentMember].reject{|k| k == "family_members"})
    else 
      @elder_already_present = true
    end  
    @member_exists = FamilyMember.where(member_id: @elder_member.id, family_id: @family.id).present?
    if !@member_exists && @elder_member.save
      @invitation = Invitation.where(member_id: @elder_member.id, family_id: params[:ParentMember][:family_members][:family_id], status: Invitation::STATUS[:invited]).first
      if @invitation.blank?
        @invitation = @elder_member.invitations.new(params[:ParentMember][:family_members])
        @invitation.sender_type = "Family"
        @invitation.sender_id = current_user.id
        @invitation.status =  Invitation::STATUS[:invited]
        @invitation.save        
      end
      @message = "I have sent an email to the invitee" #"An email has been sent to the invitee. Visit <a href='families' data-remote=true>Manage Family</a>."
      if @elder_already_present
        UserCommunication::UserActions.new.user_received_family_join_invite(current_user,@invitation)
      end
      message = Message::API[:success][:create_elder]
      UserMailer.elder_member_request_email(@invitation).deliver
      add_notification(@elder_member, "#{current_user.name} invited to you join his family #{@family.name}.", families_path, current_user,"family") if @elder_already_present
      #family_member_obj = FamilyMember.where(member_id: @elder_member.id, family_id: @family.id).last
      #UserCommunication::UserActions.add_user_action(@invitation ,"Manage Family",current_user,"Add adult")

    else
      message = Message::API[:success][:elder_already_exist] + @family.name
    end
    @elder_member["age"] = ApplicationController.helpers.calculate_age(@elder_member.birth_date) rescue ""
    respond_to do |format|
      format.js  
      format.html 
      format.json { render :json => { :message=> message, :elder_member=>@elder_member ,:invitation=> @invitation, :status => StatusCode::Status200} }
    end  
  end

  
  def update_elder
  begin    
    @family_member = FamilyMember.find(params[:id])
    if params[:ParentMember][:family_member][:role].present? && params[:ParentMember][:family_member][:role].downcase != "mother"
      raise "One or more pregnancies are associated with this member. Please remove the association to change role"  if Pregnancy.where(member_id:@family_member.member_id.to_s,status:"expected",family_id:@family_member.family_id.to_s).present?
    end
    message = Message::API[:success][:update_elder]
    status = StatusCode::Status200
    @family_member.update_attributes(params[:ParentMember][:family_member])
  rescue Exception=>e
    message = e.message
    @error = e.message
    status = StatusCode::Status100
  end
    respond_to do |format| 
      format.js
      format.json { render :json=> {:member=> @family_member,message:message, :error => @error, status: status}  }
    end
  end

  def remove_elder
    begin
      @family_member = FamilyMember.find(params[:id])
      @family_member.destroy
      Score.where(activity_type: "Member", activity_id:@family_member.member_id,family_id:@family_member.family_id).delete
      @family = @family_member.family.reload
      respond_to do |format| 
        format.js
        format.json { render :json=> {:member=> @family_member, :family=> @family, message:Message::API[:success][:remove_elder], status: StatusCode::Status200}  }
      end
    rescue Exception=> e
       render :json=>{ :error=> e.message, :status=>500 }
    end
  end  

  def delete_elder_invitation    
    @invitation = Invitation.find(params[:id])
    @invitation.destroy
    @family = @invitation.family.reload
    respond_to do |format| 
      format.js
      format.json { render :json=> {:family=> @family, message:Message::API[:success][:delete_elder_invitation], status: StatusCode::Status200}  }
    end
  end  

  def resend_invitation
    @invitation = Invitation.find(params[:id])
    UserMailer.elder_member_request_email(@invitation).deliver
    UserCommunication::UserActions.new.user_received_family_join_invite(current_user,@invitation)


    respond_to do |format| 
      format.js {render :layout=>false}
      format.json { render :json=> {:invitation=> @invitation, message:Message::API[:success][:resend_invitation], status: StatusCode::Status200}  }
    end
    
  end  

  # Accept/Reject invitatoin to join family 
  def verify_elder_for_family
     begin
      @invitation = Invitation.find(params[:id])  
       session[:user_id] = nil
     rescue Exception => e
      flash[:notice] = "Invitation is no longer active"
      respond_to do |format| 
        # format.html {redirect_to( "/users/sign_in?pageName=signin") and return}
        # format.html {redirect_to( "/users/sign_in?pageName=signin",:flash => { :notice => "Invalid invitation" }) and return}
        
        format.html {redirect_to( (user_signed_in? ? families_url : "/users/sign_in?pageName=signin"),:flash => { :notice => flash[:notice] })  and return}
        # format.html {redirect_to( families_url,:flash => { :notice => flash[:notice] })  and return}
      end
     end
    
    member = @invitation.member
    @user = member.user #User.where(:email => @invitation.member.email).first
    if @user.present? # == current_user.user
       @user.status = "confirmed" if @user.status == "unconfirmed"
       @user.save
      session[:invited_user_id] = @user.id
      session[:authentication_status]  = User.user_authentication_status(@user,@user.member)
      session[:api_token] = @user.api_token if session[:authentication_status] == "authenticated"

    end
    session[:api_invitation] = @invitation.id.to_s
    session[:api_email] = member.email rescue nil
    session[:api_invitation_status] = "active"
    sign_out if user_signed_in? && @user != current_user.user
    
    # Invitation request is active ?
    if @invitation.status == Invitation::STATUS[:accepted] || @invitation.status == Invitation::STATUS[:rejected]
      session[:api_invitation_status] = "inactive"
      flash[:notice] = user_signed_in? ? "You already #{@invitation.status} this family." : "Please login to continue.You already #{@invitation.status} this family."
      respond_to do |format| 
        format.html {redirect_to(user_signed_in? ? families_url : "/users/sign_in?pageName=signin") and return}
        format.js {redirect_to(user_signed_in? ? families_url : "/users/sign_in?pageName=signin") and return}
        format.json { render :json=> { :auth_token=> session[:api_token], :invitation=> @invitation, :message=>flash[:notice],  status: StatusCode::Status100} and return true }
      end
    elsif params[:status] == 'reject'
      @invitation.reject
      flash[:notice] = Message::GLOBAL[:success][:verify_elder_for_family_reject]#"You have rejected the request."
      respond_to do |format| 
        format.html {redirect_to( (user_signed_in? ? dashboard_url : "/users/sign_in?pageName=signin"),:flash => { :notice => flash[:notice] } ) and return}
        format.js {redirect_to(user_signed_in? ? dashboard_url : "/users/sign_in?pageName=signin") and return}
        format.json { render :json=> { :auth_token=> session[:api_token], :invitation=> @invitation, :message=>flash[:notice],  status: StatusCode::Status200} and return true }
      end
      
    elsif params[:status] == 'accept'
      Score.create(family_id: @invitation.family_id, activity_type: "Member", activity_id: member.id, activity: "Added #{member.email} to #{@invitation.family.name}", point: Score::Points["Member"], member_id: member.id)
      # sign_out if user_signed_in? && @user != current_user.user
      session[:invitation_id] = @invitation.id
      
      @invitation.accept
      @family_member = FamilyMember.new(member_id: @invitation.member_id, family_id: @invitation.family_id, role: @invitation.role)
      
      @family_member.save
      UserCommunication::UserActions.new.invited_user_joins_family(member,@invitation)
      if @user.present?
        flash[:notice] = "You have become a member of #{@invitation.family.name}."
        page_name = "signin"
      else
        flash[:notice] = Message::GLOBAL[:success][:family_user_register]#"Please register with us to proceed."
        page_name = "signup"
      end
      respond_to do |format| 
        format.html {redirect_to(user_signed_in? ? families_url : "/users/sign_in?pageName=#{page_name}") and return }
        format.js { redirect_to(user_signed_in? ? families_url : "/users/sign_in?pageName=#{page_name}") and return }
        format.json { render :json=> {:auth_token=> session[:api_token], :family_member=> @family_member, :user=>@user, :invitation=> @invitation, :message=>flash[:notice],  status: StatusCode::Status200} and return true }
      end
      
    end
  end

  # Accept/reject invitation to join Nurturey
  def verify_elder
    @invitation = Invitation.find(params[:id])
    @user = User.where(:email => @invitation.member.email).first
    if @user && @user.status == "unconfirmed"
      @user.status = "confirmed"
      @user.save
    end
    # App launch info 
    session[:api_token] = @user.api_token if @user.present? # == current_user.user
    session[:api_invitation] = @invitation.id.to_s
    session[:api_email] = member.email rescue nil
    session[:api_invitation_status] = "active"

    sign_out if user_signed_in? && @user != current_user.user    
    if @invitation.status == Invitation::STATUS[:accepted] || @invitation.status == Invitation::STATUS[:rejected]
      flash[:notice] = user_signed_in? ? "You have already #{@invitation.status}." : "You have already #{@invitation.status}."
      session[:api_invitation_status] = "inactive"
      respond_to do |format| 
        format.html {redirect_to(user_signed_in? ? dashboard_url : "/users/sign_in?pageName=signin") and return }
        format.js { redirect_to(user_signed_in? ? dashboard_url : "/users/sign_in?pageName=signin") and return }
        format.json { render :json=> {:family_member=> @family_member, :user=>@user, :invitation=> @invitation, :message=>flash[:notice],  status: StatusCode::Status200} and return true }
      end

    elsif params[:status] == 'reject'
      @invitation.reject
      flash[:notice] = Message::GLOBAL[:success][:verify_elder_reject] #"You have rejected the request."
      respond_to do |format| 
        format.html {redirect_to(user_signed_in? ? dashboard_url : "/users/sign_in?pageName=signin") and return }
        format.js { redirect_to(user_signed_in? ? dashboard_url : "/users/sign_in?pageName=signin") and return}
        format.json { render :json=> {:family_member=> @family_member, :user=>@user, :invitation=> @invitation, :message=>flash[:notice],  status: StatusCode::Status200} and return true }
      end
      
    elsif params[:status] == 'accept'      
      flash[:notice] = Message::GLOBAL[:success][:verify_elder_accept] #"You have accpeted the request."
      Score.create(activity_type: "Invitation", activity_id: @invitation.id, activity: "Invited #{@invitation.member.email} to Nurturey", point: Score::Points["Invitation"], member_id: @invitation.sender.id)      
      session[:invitation_id] = @invitation.id
      if @user.present?
        @invitation.accept
      UserCommunication::UserActions.new.invited_user_joins_family(member,@invitation)

        page_name = "signin"
      else        
        flash[:notice] = Message::GLOBAL[:success][:family_user_register] #"Please register with us to proceed."
        page_name = "signup"
      end
      respond_to do |format| 
        format.html {redirect_to(user_signed_in? ? dashboard_url : "/users/sign_in?pageName=#{page_name}") and return }
        format.js { redirect_to(user_signed_in? ? dashboard_url : "/users/sign_in?pageName=#{page_name}") and return}
        format.json { render :json=> {:family_member=> @family_member, :user=>@user, :invitation=> @invitation, :message=>flash[:notice],  status: StatusCode::Status200} and return true }
      end
      
    end  
  end


  
  def update_current_member 
    begin
      @family_member = FamilyMember.where(id: params[:family_member_id]).first if params[:family_member_id]
      @family_member = @family_member || FamilyMember.where(family_id: params[:family_id],member_id:current_user.id).first if params[:family_id]
      if params[:role].present? && params[:role].downcase != "mother"
        raise "One or more pregnancies are associated with this member. Please remove the association to change role"  if Pregnancy.where(family_id:@family_member.family_id.to_s, member_id:current_user.id.to_s,status:"expected").present?
      end

      params[:Member][:country_code] = params[:Member][:country_code] || params[:country_code]
      @member = current_user
      @updated = @member.update_attributes(params[:Member].except("country_code"))
      user = @member.user
      @member.reload
      Rails.logger.info "............... country_code : #{user.country_code}"
      country_code_to_update = (params[:Member][:country_code].downcase rescue params[:Member][:country_code])
      Rails.logger.info "............... params country_code : #{country_code_to_update}"
      if country_code_to_update.present? && user.country_code != country_code_to_update
        user.update_family_children_country_code(country_code_to_update)
      end
      user.country_code = country_code_to_update
      user.first_name = @member.first_name
      user.last_name = @member.last_name
      user.save(validate:false)
      user.reload
      # user.update_attributes({country_code:country_code_to_update})
      # user.update_attributes({first_name:@member.first_name,last_name:@member.last_name} )
      Rails.logger.info "...............updated country_code : #{user.country_code}"
      if params[:role].present?
        @family_member.role = params[:role]
        @family_member.save
      end 
      current_user.update_childern_refs
      #NOTE: Single file will be received in params[:media_files] instead of array
      if params[:media_files].present?
        picture = Picture.new(local_image: params[:media_files],upload_class: "pic",member_id: @member.id)
        @member.profile_image = picture if picture.valid?
      end  
      @member["age"] = ApplicationController.helpers.calculate_age(@member.birth_date) rescue ""
      # To get user's created and associated families
      find_families
      respond_to do |format|
        format.js {render :layout=> false}
        format.html {render :layout=> false}
        format.json { render :json=> {:member=> @member, :family_member=> @family_member, message:Message::API[:success][:update_current_member], status: StatusCode::Status200}  }

      end
    rescue Exception=> e
      status = StatusCode::Status100
      Rails.logger.info "..............Error inside Update current member detail #{e.message}"
      render :json=>{:message=>e.message,:status=>status, :error=> e.message}
    end
  end

  def invite_friend
  end  

  def hide_invited_user_block
    session[:invited_user] = nil
  end 

  def trigger_tour_step
    session[:tour_step] += 1 if session[:tour_step]
    session[:tour_step] = nil if session[:tour_step] > 2
  end 


  private

  def find_families
    @sent_family_invitations = current_user.sent_join_family_invitations # Needed in view at multiple places
    @received_family_invitations = current_user.received_join_family_invitations
    current_families = current_user.families.includes(:family_members).entries
    asso_families =  Family.where({'_id' => { "$in" => FamilyMember.where(member_id:current_user.id).map(&:family_id).uniq - current_families.map(&:id)}}).includes(:family_members).entries
    @families = current_families + asso_families      
  end  
  
  def set_api_version
    @api_version = 6
  end
end
