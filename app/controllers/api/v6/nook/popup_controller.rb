class Api::V6::Nook::PopupController < ApiBaseController
  before_filter :set_api_version
  
  # Put
  # /api/v6/nook/popup/skip_popup.json
  def skip_popup
    # 
    member_id = current_user.id
    PopupLaunch.update_nook_popup_launch_count(member_id,count=1,options={skipped:true})
    render :json=>{status:200} 
  end
  

  # Put
  # /api/v6/nook/popup/popup_clicked.json
  def popup_clicked
    member_id = current_user.id
    PopupLaunch.update_nook_popup_launch_count(member_id,count=1,options={nook_clicked:true})
    render :json=>{status:200} 
  end
  

  private
  
  def set_api_version
    @api_version = 6
  end
end