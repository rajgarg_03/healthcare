 class Api::V1::UsersController < ApiBaseController
  require 'ostruct'
  before_filter :set_api_version
  skip_before_filter :token_verify , :only=>[:sign_in, :sign_in_via_facebook]
  #before_filter :check_admin_aproval, :only=>[:sign_in]
  def index
    if params[:token].present?
      @user = User.find_user_with_token(params[:token]).first
      @user["image"] = @user.member.profile_image.photo(:small) rescue ""
      status = StatusCode::Status200
    end
    respond_to do |format|
      format.json { render :json => @user.to_json(methods: [:families_count],:include => [:member,:google_cal]) }
    end 
  end
  
  #PUT api/v1/users/skipped_details.json?token
  def skipped_details
    begin
      (params[:skipped_details]|| []).each do |data|
        if data["type"] == "basic_setup"
          record = SkipBasicSetupAction.where(:skipped=>true,member_id:data["member_id"],screen_name:data["action"],screen_id:data["id"]).first
          record  = record || SkipBasicSetupAction.create(:skipped=>(data["skipped"]|| false),member_id:data["member_id"],screen_name:data["action"],screen_id:data["id"])
          count = record.skip_count.to_i + 1
          record.update_attributes({:skip_count=> count, :skipped_up_to=> (Time.zone.now + 3.days) })
        elsif data["type"] == "handhold"
          record = SkipHandHoldAction.where(:skipped=>true, member_id:data["member_id"],screen_name:data["action"],screen_id:data["id"]).first
          record  = record || SkipHandHoldAction.create(:skipped=>(data["skipped"] || false), member_id:data["member_id"],screen_name:data["action"],screen_id:data["id"],skipped_by:current_user.id)
          count = record.skip_count.to_i + 1
          record.update_attributes({:skip_count=> count, :skipped_up_to=> (Time.zone.now + 3.days) })
        end
      end
      user = @current_user || User.find_user_with_token(params[:token]).first.member
      user.add_refs_to_childern
      status = StatusCode::Status200

    rescue Exception => e
      @error = e.message
      status = StatusCode::Status100
    end
    render json: {:status => status,:error=>@error}
  end

  def delete_account
    begin
     user = User.where(email:params[:email]).first
      m = user.member
      email = rand(99999999).to_s + user.email
      user.set(:email, email)
      m.email = email
      user.confirmation_token = email
      m.save
    rescue Exception=> e
      @error = e.message
    end
    render json: {:status => StatusCode::Status200}
  end
  
  #params{:invitation_id=>, email,password,auth_token}
  def sign_in
    if params[:email].present?
      params[:email] =  params[:email].downcase
      user = User.where(email:params[:email]).first
    elsif params[:auth_token].present?
      user = User.where(confirmation_token:params[:auth_token]).first
    end
    if (user.present? && (user.valid_password?(params[:password]) || user.confirmation_token == params[:auth_token] ) )
      if user.status == User::STATUS[:unapproved]
        render json: {status:101, user:user, message:"User is Blocked"}
      elsif params[:auth_token].blank? && (!User.can_access_dashboard_after_signup?(user) && !user.is_approved?)        
        render json: {status:100, user:user, message: Message::API[:success][:email_not_verified]}
      else
        member = user.member
        update_user_login_details(member,user,params)
        render :json=> hh_and_basic_setup_evaluator_json(member,params[:invitation_id])
      end
    elsif (user.present? && !(user.valid_password?(params[:password]) || user.confirmation_token == params[:auth_token]) )
      render json: {message: Message::API[:success][:password_invalid], status:103}
    else
      render json: {message: Message::API[:success][:email_not_registered], status:102}
    end
  end
 

  # api /api/v1/users/get_summary_screen.json?token=131aassdas345474
  #Get
  def get_summary_screen
    begin
        user = User.find_user_with_token(params[:token]).first
        member = user.member
        render :json=> {:NYAllSetToGoScreen=>BasicSetupEvaluator.family_screen_data(member), :status=>StatusCode::Status200}
      rescue Exception=>e
        render json: {error:e.message,  status:100}
      end
  end

  # api
  # GET /api/v1/users/basic_set_up_evaluator_for_user.json?token=131aassdas345474das&invitation_id=
  def basic_set_up_evaluator_for_user
      begin
        user = User.find_user_with_token(params[:token]).first
        render :json=> hh_and_basic_setup_evaluator_json(user.member,params[:invitation_id],{:join_req_setup=>false})
      rescue Exception=>e
        render json: {error:e.message,  status:100}
      end
  end

  # Post
  # params {:invitation_id,fb_data
  def sign_in_via_facebook
    # fb_data = {:provider => 'facebook', :uid => '10200460380989199', :info => {:email => 'rishu@xyz.com', :first_name => 'Rishu', :last_name => 'Kumar', :image => "image_url"}}
    fb_data = User.prepare_fb_data(params)

    auth = OpenStruct.new fb_data
    auth.info = OpenStruct.new auth.info
    user = User.from_omniauth(auth)
     
    respond_to do |format|
      if user.persisted?
        user.skip_confirmation!
        member = user.member
        update_user_login_details(member,user,params)
        # TODO: For invitation and tour step if required in apis
        format.json {render :json=> hh_and_basic_setup_evaluator_json(member,params[:invitation_id])}
      else
        if user.errors.added? :email, :taken
          message = "We already have an existing account using #{auth.info.email}. Please login."
          format.json { render json: {message: message, status: 100} }
        end
      end
    end
  end  

  # Get/api/v1/users/child_members.json?token="iBSqRyQ5pxuxVb1dSz47
  def child_members
     @families = @current_user.families.includes(:family_members).entries
     @assoc_families = Family.where({'_id' => { "$in" => FamilyMember.where(member_id:current_user.id).pluck(:family_id).uniq - @families.map(&:id)}}).includes(:family_members).entries
     respond_to do |format|
      format.json { render :json=> {:families=>JSON.parse(@families.to_json(:include => {:family_members=>{:include=> {:member=> {:include=> :member_nutrients,:methods=>[:member_nutrients,:member_age]}} } })), :status => StatusCode::Status200 } }      
    end
  end

  # Get /api/v1/users/user_dashboard.json?parent_member_id=542991865acefe0ac1000003&token=iBSqRyQ5pxuxVb1dSz47&event_type=past
  def user_dashboard
    begin
    # Get action card data
    data = @current_user.api_dashboard(false,@api_version).take(8)
    @childern_ids = current_user.childern_ids.map(&:to_s) + [current_user.id.to_s]
    # Get User calendar Data
    events = Event.get_event_data("parent","upcoming",nil,@current_user.id).sort_by {|c| c[:start]}[0..1]
    event_data = []
    events.each do |data|
      event_data << {member_id:data[:member_id],name:data[:title],type:data[:type], start_date: data[:start], end_date:data[:end],start_time: (data[:datail]["start_time"].to_time.to_formatted_s(:time) rescue "00:00"),end_time: (data[:datail]["end_time"].to_time.to_formatted_s(:time) rescue "23:00")}
    end
    # User's top 2 pointers
    article_ref = ArticleRef.get_pointers_list(current_user,@childern_ids).order_by("effective_score desc").limit(2)
    status = StatusCode::Status200
    rescue Exception=> e
      @error = e.message
      status = StatusCode::Status100
    end
    respond_to do |format|
      format.json { render :json=> {:error=> @error, :status=> status, :recommended=> data, :event_widget=>event_data, :reference_widget=> article_ref} }
    end
  end

  # Get /api/v1/users/user_info.json?token=iBSqRyQ5pxuxVb1dSz47
  def user_info
    api_version = @api_version
    # Get all basic info related to logged in user
    user_basic_info_data = User.user_basic_info(current_user,nil,api_version)
    user_basic_info_data[:status] = StatusCode::Status200
    respond_to do |format|
       format.json { render json: user_basic_info_data}
    end
  end

   # PUT http://localhost:3000/api/v1/users/dismiss_nutrient.json?token=XrjzH88mP4syK1q4MTeb&card_id=54drtef65v76_health
  # option values for nutrient_name[ health,timelines,nutrients,immunsation,milestones,document]
  def dismiss_nutrient
    child_id,nutrient_name = params[:card_id].split("_")
    
    nutrient = DismissNutrient.where(parent_id:current_user.id, child_id: child_id ,nutrient_name: nutrient_name).last || DismissNutrient.new(parent_id:current_user.id, child_id: child_id ,nutrient_name: nutrient_name ,dismiss_up_to: Date.today+7.day)
    # nutrient = DismissNutrient.new(parent_id:@current_user.id, child_id: params[:child_id] ,nutrient_name:params[:nutrient_name],dismiss_up_to: Date.today-1.day)
    nutrient.dismiss_up_to =  Date.today + 7.days
    msg = nutrient.save ? {:message=>"Dismissed sucessfully", :status=> 200} : {:message=> nutrient.errors.messages, :status=> 100}
    if nutrient_name == "AddSpouse"
      family_id = child_id
      current_user.skip_add_spouse(family_id)
    end
    respond_to do |format|
      format.json {render json: msg}
    end
  end 

  # Get /api/v1/users/recent_items.json?parent_member_id=542991865acefe0ac1000003&token=iBSqRyQ5pxuxVb1dSz47&event_type=past
  def recent_items
    @childern_id = @current_user.childern_ids
    recent_timeline =@current_user.api_recent_timeline(@childern_id)
    recent_health = @current_user.api_recent_health(@childern_id)
    recent_milestone = @current_user.api_recent_milestones(@childern_id)
    recent_vaccine = @current_user.api_recent_immunisation(@childern_id)
    data = recent_timeline + recent_health + recent_milestone + recent_vaccine
    data= (data.sort_by {|c| c[:line3]}).reverse
    paginate_data = data.paginate(:page => (params[:page]), :per_page => 6)
    respond_to do |format|
      format.json { render :json=> {:recent=> paginate_data,:status=> StatusCode::Status200,:total=>data.count,:current_page=>(params[:page]||1), :per_page=>6}}
    end
  end

  # Get /api/v1/users/user_events.json?calendar_type=child&member_id=542991865acefe0ac1000003&token=iBSqRyQ5pxuxVb1dSz47&event_type=past
  # @params: member_id: selected member id
  #          calendar_type[parent/child]: calendar for parent or child
  #          event_type[upcoming/past]: "upcoming event or past event "
  # @return Event and vaccination event detail
  def user_events
    event_per_page = 15
    data = Event.get_event_data( params[:calendar_type] || "parent", params[:event_type] || "upcoming", params[:member_id], @current_user.id)
    if params[:event_type] == "upcoming"
      data= (data.sort_by {|c| c[:start]})
    else
      data= (data.sort_by {|c| c[:start]}).reverse
    end
    if params[:page]
      paginate_data = data.paginate(:page => (params[:page]), :per_page => event_per_page)
      current_page = params[:page].to_i
      total_page,next_page = Utility.page_info(data,current_page,event_per_page)
      per_page = event_per_page
      # pagination_attributes = {current_page: current_page, next_page: next_page, total_page:total_page}
    else
       paginate_data =  data
       current_page = 1
       total_page = 1
       next_page = 1
       per_page = data.count
    end
    paginate_data = paginate_data.group_by {|d| d[:start].to_date.strftime("%b %Y") }
    response_hash = {:status=> StatusCode::Status200, :recent=> paginate_data,:total=>data.count,:current_page=> current_page, :per_page=>per_page, next_page: next_page, total_page:total_page}
    # response_hash.merge!(pagination_attributes) if params[:page]
    respond_to do |format|
      format.json { render :json=> response_hash}
    end
  end

  def upload_image
    member = Member.where(id: params[:member_id]).first
    content = params[:image_data]
    content.gsub!('\\r', "\r")
    content.gsub!('\\n', "\n")
    decode_base64_content = Base64.decode64(content) 
    File.open("public/pictures/#{member.first_name.downcase}_#{member.id}.jpg", "wb") do |f|
      f.write(decode_base64_content)
    end
    file = File.open("public/pictures/#{member.first_name.downcase}_#{member.id}.jpg")
    @picture = Picture.new(local_image:file,upload_class: 'pic')
    if @picture.valid? && member.profile_image = @picture
      @picture_saved = true
      @message = Message::API[:success][:upload_image]
      @status = StatusCode::Status200        
    else
      @picture_saved = false
      @message = Message::API[:error][:upload_image]
      @status = StatusCode::Status100
    end
    File.delete("public/pictures/#{member.first_name.downcase}_#{member.id}.jpg")
    respond_to do |format|
      format.json
    end
  end

# Ap1
# Post /api/v1/users/upload_cover_image.json?token
# Params {member_id=>"123123sad132", :image_data=> }
  def upload_cover_image
    begin
    member = Member.where(id: params[:member_id]).first
    file , file_name= Picture.binary_image_to_file(params[:image_data], member, "cover_image") if params[:image_data].present?
    file = params[:media_files] if params[:media_files].present?
    @picture = Picture.new(local_image:file,upload_class: 'coverimage',member_id:member.id)
    if @picture.valid? && member.user.cover_image = @picture
      @picture_saved = true
      @message = Message::API[:success][:upload_cover_image]
      @status = StatusCode::Status200        
    else
      @picture_saved = false
      @message = Message::API[:error][:upload_cover_image]
      @status = StatusCode::Status100
    end
    File.delete(file_name) if file_name
    render :json=> {:message=> Message::API[:success][:upload_cover_image],:status => StatusCode::Status200, :cover_image=> @picture.attributes.merge(url_small: @picture.complete_image_url(:small), url_large: @picture.complete_image_url(:medium), url_original: @picture.complete_image_url(:original), aws_small_url: @picture.aws_url(:small), aws_original_url: @picture.aws_url)}
    rescue Exception=>e
      render :json=>{:error=>e.message, :message=> Message::API[:error][:upload_cover_image],:status => StatusCode::Status100 }
    end
  end


  private
  def check_admin_aproval
    params[:email] = params[:email].downcase if params[:email]
    user = User.where(:email=>params[:email]).first
    if user && user.status == User::STATUS[:unconfirmed]
      render :json=> {:message=>"Kindly verify your email ID. Please check the email we sent to you.",:screen_name=>["email_verification_screen"],:status=>StatusCode::Status100 } 
      return    
    end
  end
  
  def update_user_login_details(member,user,params)
    UserLoginDetail.create(user_id:user.id,login_time:Time.zone.now,platform:(params[:platform]||"Device") ) rescue nil
    store_device_token(member,params[:device_token],params[:platform],params) #if params[:device_token].present?
    api_sign_in(user)
    @current_user  = member
  end

  def hh_and_basic_setup_evaluator_json(member,invitation_id=nil,options={:join_req_setup=>true})

    member_user_families = member.user_families
    api_version = @api_version
    json_data = {:families=>JSON.parse(Family.family_to_json(member_user_families,api_version,current_user) ), :user_basic_info=> User.user_basic_info(member,member_user_families),:status => StatusCode::Status200}
    if  (Invitation.find(invitation_id).accepted? rescue false) && options[:join_req_setup]
      json_data 
    else 
      controllers,status = BasicSetupEvaluator.run(member,invitation_id)
      unless status["basic_setup"]
        # controllers = {}
        handhold_data =  HandHold.user_complete_handhold(member)
        json_data["handhold_controllers"] = handhold_data
      end
      json_data["basic_setup_controller"] = controllers
      json_data["status"] = StatusCode::Status200
      json_data
    end
    
  end

  # 1. Find device.
  # 2. If device is not present, create one for this member.
  # 3. ElseIf device exists and attached with some other member, then switch the member with this member.
  # options = {os_version, device_model, app_version}
  def store_device_token(member, token, platform,options={})
    options = options || {}
    # device = member.devices.where(token: token).first rescue nil
    status = (member.member_settings.where(name:"subscribe_notification").last || MemberSetting.create(name:"subscribe_notification",member_id:member.id.to_s,status:true)).status rescue false
    if token.present?
      device_detail = member.devices.last || {}
      member.devices.delete_all
      Device.where(token:token).delete_all
    end
    device = member.devices.last || Device.new(member_id:member.id)
    ["email","auth_token","password","action","controller","format","device_token","fb_data"].each do |i|
       options.delete(i)
    end
    options = options.reject{|k,v| v.blank?}
    if options.present?
      options.each do |k,v|
        device[k] = v || device_detail[k]
      end
    end
    begin
      device.screen_skip_count = device_detail["screen_skip_count"]
      device.screen_skip_up_to = (device_detail["screen_skip_up_to"] + Device::Screen_skip_day.days) rescue Date.today - 1.days
    rescue 
    end
    device.token = token if token.present?
    device.enabled = status
    device.app_version = device.app_version || device_detail["app_version"]
    device.os_version = device.os_version || device_detail["os_version"]
    device.build_number = device.build_number || device_detail["build_number"]
    device.platform = platform if platform.present?
    device.save
    user = member.user
    user["app_login_status"] = true
    user.save
    
  end

  private

  def set_api_version
    @api_version = 1
  end
    
end