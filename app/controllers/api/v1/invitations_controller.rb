class Api::V1::InvitationsController < ApiBaseController

  # Accept/Reject invitation request from inside mobile app
  def update_status
    @invitation = Invitation.where(id:params[:invitation_id]).first
    family = @invitation.family
    if @invitation.nil? || @invitation.accepted? || @invitation.rejected?
      render :json=>{:message=> "Invitation is not active now.", :status=>StatusCode::Status100} and return
    end
     find_sender_and_receiver_for_invitation
    if params[:status] == 'accept'
  	  @invitation.accept 
      count = @owner_requested_user ? 0 : 1
  	  @family_member = FamilyMember.find_or_initialize_by(family_user_count:count, :member_id => @invitation.member.id, :family_id => @invitation.family_id, :role => @invitation.role)
  	  api_message = @family_member.save ? Message::API[:success][:accept_invitation] : @family_member.errors.first
    elsif params[:status] == 'reject'
  	  @invitation.reject unless @invitation.rejected?	  	
  	  api_message = Message::API[:success][:decline_invitation]
    end
    
    message = "Your #{@owner_requested_user ? 'invitation' : 'request'} to join #{family.name} has been #{@invitation.status} by #{@receiver.first_name || @receiver.email}."
    add_notification(@sender,message , families_path, current_user,"family")
    response = {:invitation=> @invitation , message: api_message,  status: StatusCode::Status200}
    if params[:status] == 'accept'
      response.merge!({member: @family_member,welcome_to_family_screen:BasicSetupEvaluator.family_screen_data_prepare(family,{:invited=>true})}) 
    end
    respond_to do |format|
      format.json { render :json=> response  }
    end
  end

  private

  def find_sender_and_receiver_for_invitation
    @sender = @invitation.sender_id.present? ? @invitation.sender : @invitation.member
    @receiver = @invitation.sender_id.present? ? @invitation.member : @invitation.family.owner
    @owner_requested_user = @invitation.sender_id.present?    
  end  

end