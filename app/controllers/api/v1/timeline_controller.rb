 class Api::V1::TimelineController < ApiBaseController

 	# GET
 	# /api/v1/timeline.json?token=XrjzH88mP4syK1q4MTeb&member_id=546128200a9a99178300000d
 	# /api/v1/timeline.json?token=XrjzH88mP4syK1q4MTeb&member_id=546128200a9a99178300000d&page=3
  #localhost:3000/api/v1/timeline.json?token=iBSqRyQ5pxuxVb1dSz47&member_id=5644443d5acefe40b4000019
 	def index
 	  @timeline_member = Member.where(id:params[:member_id]).first
    selected_date = (params[:selected_date] || Date.today).to_date + 1.day
    selected_date = selected_date < (Date.today + 1.day) ? selected_date : Date.today + 1.day
    @timelines= @timeline_member.timelines.where(:state=>true,:timeline_at.lte => selected_date).order_by("timeline_at DESC").order_by("id DESC")
    @current_page = params[:page] || 1
    @total_page,@next_page=Utility.page_info(@timelines, @current_page, 10)
    @current_page = 0 if @total_page == 0
    @timelines = @timelines.paginate(:page => (params[:page]), :per_page => 10).entries
    respond_to do |format|
    	format.json
    end	
 	end


#GET /api/v1/timeline/get_timeline_detail.json?token=yyJs6pxzbLRmDLrEPpcA&timeline_id=547610cd8cd03db9a700002e
def get_timeline_detail
  begin
      timeline = Timeline.find(params[:timeline_id])
      message = "Timeline found"
      status = StatusCode::Status200
    rescue
      timeline = nil
      message = "No timeline found"
      status = StatusCode::Status100
    end
    render json: {:message => message, :timeline=>timeline, :status=>status}
  end


 	# GET
 	# /api/v1/timeline/548bfd0e0a9a997d21000009.json?token=XrjzH88mP4syK1q4MTeb&member_id=546128200a9a99178300000d
 	def show
 	  @timeline = Timeline.find(params[:id])
 	  respond_to do |format|
 	  	format.json
 	  end	
 	end

 	# POST
 	# /api/v1/timeline/create_timeline.json
 	# "timeline"=>{"name"=>"hiii", "desc"=>"hiiiiii", "entry_from"=>"timeline", "member_id"=>"546128200a9a99178300000d", "timeline_at"=>"01-03-2015"} 	
 	def create_timeline
    params[:timeline][:name] = nil if (params[:timeline][:name] == "Timeline title" || params[:timeline][:name].blank?)
    @timeline = Timeline.new(params[:timeline])
    @timeline.timeline_at = @timeline.get_timeline_at(params[:timeline][:timeline_at])
    member = @timeline.member
    respond_to do |format|
      if @timeline.save
        @message = Message::API[:success][:create_timeline]
        (params[:upload_media] || [] ).select{|p| p.present?}.each do |media_file|
          @timeline.pictures << Picture.new(local_image: media_file, upload_class: "timeline",member_id:member.id)
        end
        if @timeline.pictures.all?{|p| p.valid?}
          family_id = FamilyMember.where(member_id: member.id).first.family_id
          Score.create(family_id: family_id,activity_type: "Timeline", activity_id: @timeline.id, activity: "Added post #{@timeline.name.truncate(15)}", point: Score::Points["Timeline"], member_id: member.id)
        else
          @pic_invalid = true
        end
      else
      @message = Message::API[:error][:create_timeline]        
	    end
      format.json     
    end
  end

  # PUT
  # /api/v1/timeline/update_timeline.json
  # {"timeline"=>{"name"=>"hii edited", "desc"=>"hellooo", "member_id"=>"546128200a9a99178300000d", "timeline_at"=>"01-03-2015"}, "deleted_image"=>"", "id"=>"54f355110a9a99b38d000008"}
  def update_timeline
    params[:timeline][:name] = nil if (params[:timeline][:name] == "Timeline title" || params[:timeline][:name].blank?)
    @timeline = Timeline.find(params[:id])
    params[:timeline][:timeline_at] = @timeline.get_timeline_at(params[:timeline][:timeline_at])
    member = @timeline.member
    @delete_picture_ids = params[:deleted_image].split(",").compact rescue []
    @delete_picture_ids.each do |picture_id|
      if picture_id.include?('default_')
        #TODO: Do something if pic display is disabled
        # @timeline.update_attribute(:display_default_image,false)
      else  
        picture = Picture.where(id: picture_id).first
        picture.destroy if picture
      end  
    end

    if @timeline.pictures.all?{|p| p.valid?}
      @status = @timeline.update_attributes(params[:timeline]) 
      @timeline.reload
    else
      @pic_invalid = true          
    end    
    respond_to do |format|
      format.json
    end  
  end

  def upload_image  
    @timeline = Timeline.find(params[:id])
    decode_and_save_image(@timeline,params[:image_data])
    respond_to do |format|
      format.json
    end  
  end

  # DELETE
  # /api/v1/timeline/delete_timeline.json?id=:id
  def delete_timeline
    @timeline = Timeline.find(params[:id])
    @timeline.delete
    Score.where(activity_type: "Timeline", activity_id: @timeline.id, member_id: @timeline.member.id).first.destroy rescue nil
    respond_to do |format|
      format.json
    end
  end

  # GET
  # /api/v1/system_milestones/:id/hh_milestones.json?token=oxUHH9CTNzQyhRZ9QtEK
  def get_hh_system_timelines
    begin
      @member = Member.find(params[:id])
      data = HandHold.child_hh_timelines(@member)
      status = StatusCode::Status200
    rescue Exception=> e 
      status = StatusCode::Status100
      @error = e.message
      data = {}
    end
    respond_to do |format|
      format.json {render json: {system_timelines: data[:system_timelines], status: status, error: @error}}
    end
  end

  def add_hh_system_timelines
    @member = Member.find(params[:id])
    child_age_dur = @member.age_in_months > 120 ? "10.years" : @member.age_duration
    system_timeline = SystemTimeline.where(duration: child_age_dur).first
    future_system_timeline_ids = SystemTimeline.where(:sequence.gt => system_timeline.sequence).order_by(sequence: 'asc').map(&:id) rescue []
    all_system_timeline_ids = (params[:system_timeline_ids].split(',').compact + future_system_timeline_ids).uniq
    Timeline.save_system_timeline_to_member(@member.id,current_user.id,all_system_timeline_ids)
    #TODO:
    render json: {:status => StatusCode::Status200} and return
  rescue
    render json: {:status => StatusCode::Status100} and return
  end
 		
 end	