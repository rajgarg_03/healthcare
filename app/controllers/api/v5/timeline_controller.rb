 class Api::V5::TimelineController < ApiBaseController
  before_filter :set_api_version
        # GET
 	# /api/v2/timeline.json?token=XrjzH88mP4syK1q4MTeb&member_id=546128200a9a99178300000d
 	# /api/v2/timeline.json?token=XrjzH88mP4syK1q4MTeb&member_id=546128200a9a99178300000d&page=3
        #localhost:3000/api/v2/timeline.json?token=iBSqRyQ5pxuxVb1dSz47&member_id=5644443d5acefe40b4000019
 	#params {:tool=>["all"],:member_ids=['546128200a9a99178300000d'],:start_date=>"21/1/2016",:end_date=>"25/12/2016",:page=3
 	def index
 		if params[:member_id].present?
	 	  @timeline_member = Member.where(id:params[:member_id]).first
			member_ids = [params[:member_id]]
		else
	    member_ids = params[:member_ids].present? ? params[:member_ids] : current_user.childern_ids.map(&:to_s)
		end
	  begin
	      @status = 200
	      tool_list = {:Immunisations=>["Jab"], :health=>["Health"],:Measurements=>["Health"], :Documents=>["Document"],:Timeline=>["Timeline"],"Prenatal Tests"=>["prenatal_test","MedicalEvent"],:Milestones=>["Milestone"],:Pregnancy=>["Pregnancy"]}.with_indifferent_access
	      tool = tool_list.values_at(*[params[:tool]].flatten ).flatten.compact rescue nil
              selected_date = ("31/12/" + params[:end_date]).to_date rescue Date.today+1
              selected_date = selected_date < Date.today+1 ? selected_date : Date.today+1
	      if tool.present? && params[:tool].first != "all"
          if tool.include?("Timeline")
             tool << "Pregnancy"
             tool << nil
            @timelines= Timeline.includes(:member).in(member_id:member_ids,timelined_type: tool).where(:timeline_at.lte => selected_date)
          else
            @timelines= Timeline.includes(:member).in(member_id:member_ids, timelined_type:tool).where(:timeline_at.lte => selected_date)
	        end
	      else
	        @timelines= Timeline.includes(:member).in(member_id:member_ids).where(:timeline_at.lte => selected_date).order_by("timeline_at DESC").order_by("id DESC")
	      end
	      if @timelines.present? && params[:start_date].present? 
	        event_start_date = ("01/01/" + params[:start_date]).to_date
          event_start_date = event_start_date < Date.today+1 ? event_start_date : Date.today
		      @timelines = @timelines.where(:timeline_at.gte => event_start_date)
	      end
	      
	      if @timelines.present? && params[:end_date].present?
          timeline_end_date = ("31/12/" + params[:end_date]).to_date
          timeline_end_date = timeline_end_date < (Date.today + 1) ? timeline_end_date : Date.today+1
          @timelines = @timelines.where(:timeline_at.lte => timeline_end_date)
	      end
	      if params[:feed_id].present?

          per_page = 10
          begin
            timeline = Timeline.find params[:feed_id] 
            position = @timelines.where(:state=>true).where(:timeline_at.gte=>timeline.timeline_at).order_by("timeline_at DESC").order_by("id DESC").count + per_page
            @current_page = position/per_page.ceil
            per_page = position
          rescue Exception=> e 
            per_page = 10
            @current_page = (params[:page].to_i||1)
          end
        else
          per_page = 10
          @current_page = (params[:page].to_i||1)
        end
        @timelines = @timelines.where(:state=>true).order_by("timeline_at DESC").order_by("id DESC")
	      @total_page,@next_page = Utility.page_info(@timelines, @current_page, 10)
	      @current_page = 0 if @total_page == 0
	      @timelines = @timelines.paginate(:page => (params[:page]), :per_page => per_page).entries
	    rescue Exception=>e
	      @status = 100
	      @error = e.message
	    end
	  respond_to do |format|
      format.json
    end 
 	end
  
  # Get get_pre_define_timeline
  #/api/v3/timeline/get_pre_defined_timeline.json?token=gcjemgbLpVAjbjV4jyzs
  def get_pre_defined_timeline
    begin
      status = StatusCode::Status200
      title_data = PreDefineTimeline.get_predefine_timeline("all",current_user,@api_version)
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
    end
      render json: {:status => status,:pre_defined_timeline_title=>title_data,:erorr=>@error}
  end

  #merged into index.json . Will remove after verify response
  # Post
  # /api/v2/timeline/user_timeline.json?token=XrjzH88mP4syK1q4MTeb&member_id=546128200a9a99178300000d
  # /api/v2/timeline/user_timeline.json?token=XrjzH88mP4syK1q4MTeb
  #params {:tool=>["all"],:member_ids=['546128200a9a99178300000d'],:start_date=>"21/1/2016",:end_date=>"25/12/2016",:page=3
  #localhost:3000/api/v2/timeline/user_timelines.json?token=iBSqRyQ5pxuxVb1dSz47&tool=5644443d5acefe40b4000019
  def user_timeline
    begin
      @status = 200
      member_ids = params[:member_ids].present? ? params[:member_ids] : current_user.childern_ids.map(&:to_s)
      tool_list = {:Immunisations=>["jab"], :health=>["health"],:Measurements=>["health"], :Documents=>["document"],:Timeline=>["timeline"],"Prenatal Tests"=>["prenatal_test","preg"],:Milestones=>["milestone"],:Pregnancy=>["preg","prenatal_test"]}.with_indifferent_access
      tool = tool_list.values_at(*[params[:tool]].flatten ).flatten.compact rescue nil
      # @timeline_member = Member.in(id:member_ids).first
      selected_date = (params[:selected_date] || Time.zone.now).to_date + 1.day
      if tool.present? && params[:tool].first != "all"
        @timelines= Timeline.includes(:member).in(member_id:member_ids, entry_from:tool).where(:timeline_at.lte => selected_date)
      else
        @timelines= Timeline.includes(:member).in(member_id:member_ids).where(:timeline_at.lte => selected_date).order_by("timeline_at DESC").order_by("id DESC")
      end
      if @timelines.present? && params[:start_date].present? 
         @timelines = @timelines.where(:timeline_at.gte => ("01/01/" + params[:start_date]).to_date)
      end
      
      if @timelines.present? && params[:end_date].present?
        @timelines = @timelines.where(:timeline_at.lte => ("31/12/" + params[:end_date]).to_date)
      end
      
      @timelines = @timelines.where(:state=>true).order_by("timeline_at DESC").order_by("id DESC")
      @current_page = (params[:page] || 1).to_i
      @total_page,@next_page=Utility.page_info(@timelines, @current_page, 10)
      @current_page = 0 if @total_page == 0
      @timelines = @timelines.paginate(:page => (params[:page]), :per_page => 10).entries
    rescue Exception=>e
      @status = 100
      @error = e.message
    end
    respond_to do |format|
      format.json
    end 
  end
#GET /api/v1/timeline/get_timeline_detail.json?token=yyJs6pxzbLRmDLrEPpcA&timeline_id=547610cd8cd03db9a700002e
def get_timeline_detail
  begin
      @timeline = Timeline.find(params[:timeline_id])
      @message = "Timeline found"
      @status = StatusCode::Status200
    rescue
      @timeline = nil
      @message = "No timeline found"
      @status = StatusCode::Status100
    end
    respond_to do |format|
      format.json
    end 
    #render json: {:message => message, :timeline=>timeline, :status=>status}
  end


 	# GET
 	# /api/v1/timeline/548bfd0e0a9a997d21000009.json?token=XrjzH88mP4syK1q4MTeb&member_id=546128200a9a99178300000d
 	def show
 	  @timeline = Timeline.find(params[:id])
 	  respond_to do |format|
 	  	format.json
 	  end	
 	end

 	# POST
 	# /api/v1/timeline/create_timeline.json
 	# "timeline"=>{"name"=>"hiii", "desc"=>"hiiiiii", "entry_from"=>"timeline", "member_id"=>"546128200a9a99178300000d", "timeline_at"=>"01-03-2015"} 	
 	def create_timeline
    params[:timeline][:name] = nil if (params[:timeline][:name] == "Timeline title" || params[:timeline][:name].blank?)
    @timeline = Timeline.new(params[:timeline])
    @timeline.timeline_at = @timeline.get_timeline_at(params[:timeline][:timeline_at])
    member = @timeline.member
    respond_to do |format|
      if @timeline.save
        @message = Message::API[:success][:create_timeline]
        (params[:upload_media] || [] ).select{|p| p.present?}.each do |media_file|
          @timeline.pictures << Picture.new(local_image: media_file, upload_class: "timeline",member_id:member.id)
        end        
        if params[:media_files].present?  
          params[:media_files].each do |image|
            @timeline.pictures << Picture.new(local_image: image, upload_class: "timeline",member_id:member.id)
          end
        end  
        if @timeline.pictures.all?{|p| p.valid?}
          family_id = FamilyMember.where(member_id: member.id).first.family_id
          Score.create(family_id: family_id,activity_type: "Timeline", activity_id: @timeline.id, activity: "Added post #{@timeline.name.truncate(15)}", point: Score::Points["Timeline"], member_id: member.id)
        else
          @pic_invalid = true
        end
        # UserActions.add_update_user_action(@timeline,"Timeline",current_user,"Add")
      else
      @message = Message::API[:error][:create_timeline]        
	    end
      format.json     
    end
  end
  
  # POST
  # /api/v2/timeline/create_timeline_for_multi_member.json
  # "timeline"=>{"name"=>"hiii", "desc"=>"hiiiiii", "entry_from"=>"timeline", "member_ids"=>["546128200a9a99178300000d"], "timeline_at"=>"01-03-2015"}   
  def create_timeline_for_multi_member
    @timelines = []
    params[:timeline][:name] = nil if (params[:timeline][:name] == "Timeline title" || params[:timeline][:name].blank?)
    member_ids = params[:timeline][:member_ids]
    params[:timeline].delete("member_ids")
    member_ids.each do |member_id|
      params[:timeline][:member_id] = member_id
      @timeline = Timeline.new(params[:timeline])
      @timeline.timeline_at = @timeline.get_timeline_at(params[:timeline][:timeline_at])
      member = @timeline.member
      if @timeline.save
        @timelines << @timeline
        @message = Message::API[:success][:create_timeline]
        (params[:upload_media] || [] ).select{|p| p.present?}.each do |media_file|
          @timeline.pictures << Picture.new(local_image: media_file, upload_class: "timeline",member_id:member.id)
        end        
        if params[:media_files].present?  
          params[:media_files].each do |image|
            @timeline.pictures << Picture.new(local_image: image, upload_class: "timeline",member_id:member.id)
          end
        end
        if @timeline.pictures.present?  
          if @timeline.pictures.all?{|p| p.valid?}
            family_id = FamilyMember.where(member_id: member.id).first.family_id
            Score.create(family_id: family_id,activity_type: "Timeline", activity_id: @timeline.id, activity: "Added post #{@timeline.name.truncate(15)}", point: Score::Points["Timeline"], member_id: member.id)
          else
            @pic_invalid = true
          end
        end
        # UserActions.add_update_user_action(@timeline,"Timeline",current_user,"Add")

      else
      @message = Message::API[:error][:create_timeline]        
      end
    end  
    
    respond_to do |format|
      format.json     
    end
  end


  # PUT
  # /api/v1/timeline/update_timeline.json
  # {"timeline"=>{"name"=>"hii edited", "desc"=>"hellooo", "member_id"=>"546128200a9a99178300000d", "timeline_at"=>"01-03-2015"}, "deleted_image"=>"", "id"=>"54f355110a9a99b38d000008"}
  def update_timeline
    params[:timeline][:name] = nil if (params[:timeline][:name] == "Timeline title" || params[:timeline][:name].blank?)
    @timeline = Timeline.find(params[:id])
    params[:timeline][:timeline_at] = @timeline.get_timeline_at(params[:timeline][:timeline_at])
    member = @timeline.member
    @delete_picture_ids = params[:deleted_image].split(",").compact rescue []
    @delete_picture_ids.each do |picture_id|
      if picture_id.include?('default_')
        #TODO: Do something if pic display is disabled
        # @timeline.update_attribute(:display_default_image,false)
      else  
        picture = Picture.where(id: picture_id).first
        picture.destroy if picture
      end  
    end
    if params[:media_files].present?
      params[:media_files].each do |image|
        @timeline.pictures << Picture.new(local_image: image, upload_class: "timeline",member_id:member.id)
      end  
    end  
    if @timeline.pictures.all?{|p| p.valid?}
      @status = @timeline.update_attributes(params[:timeline])
      @timeline.reload
    else
      @pic_invalid = true          
    end
    # UserActions.add_update_user_action(@timeline,"Timeline",current_user,"Update")

    respond_to do |format|
      format.json
    end  
  end

  def upload_image  
    @timeline = Timeline.find(params[:id])
    decode_and_save_image(@timeline,params[:image_data])
    respond_to do |format|
      format.json
    end  
  end

  # DELETE
  # /api/v1/timeline/delete_timeline.json?id=:id
  def delete_timeline
    @timeline = Timeline.find(params[:id])
    @timeline.delete
    Score.where(activity_type: "Timeline", activity_id: @timeline.id, member_id: @timeline.member.id).first.destroy rescue nil
    respond_to do |format|
      format.json
    end
  end

  # GET
  # /api/v1/system_milestones/:id/hh_milestones.json?token=oxUHH9CTNzQyhRZ9QtEK
  def get_hh_system_timelines
    begin
      @member = Member.find(params[:id])
      data = HandHold.child_hh_timelines(@member)
      status = StatusCode::Status200
    rescue Exception=> e 
      status = StatusCode::Status100
      @error = e.message
      data = {}
    end
    respond_to do |format|
      format.json {render json: {system_timelines: data[:system_timelines], status: status, error: @error}}
    end
  end

  def add_hh_system_timelines
    begin
    @member = Member.find(params[:id])
    system_timeline_ids = params[:system_timeline_ids].split(',').compact
    options = {:category=>"child"}
    SystemTimeline.save_system_timeline(current_user,@member,system_timeline_ids,options)
    # child_age_dur = @member.age_in_months > 120 ? "10.years" : @member.age_duration
    # system_timeline = SystemTimeline.where(duration: child_age_dur).first
    # future_system_timeline_ids = SystemTimeline.where(:sequence.gt => system_timeline.sequence).order_by(sequence: 'asc').map(&:id) rescue []
    # all_system_timeline_ids = (params[:system_timeline_ids].split(',').compact + future_system_timeline_ids).uniq
    # Timeline.save_system_timeline_to_member(@member.id,current_user.id,all_system_timeline_ids)
    #TODO:
    render json: {:status => StatusCode::Status200} and return
  rescue
    render json: {:status => StatusCode::Status100} and return
  end
 end
   private
    def set_api_version
      @api_version = 5
    end
 end