class Api::V5::FamiliesController < ApplicationController 
  before_filter :set_api_version
  before_filter :validate_email_status, only: [:join_this_family]
  before_filter :find_families, only: [:index, :join_this_family, :search_family]
  skip_before_filter  :verify_authenticity_token, only: :destroy
  
  # list all families in response
  def index
    respond_to do |format|
      format.json { render :json=> {status: StatusCode::Status200,sent_invitations: current_user.sent_invitations_data, received_invitations: current_user.received_invitations_data, :families=>JSON.parse(Family.family_to_json(@families,@api_version,current_user))} }
    end
  end 

  # update family details
  def update
    @family = Family.find(params[:id])
    respond_to do |format|
      if @family.update_attributes(params[:family])
        family = @family.attributes
        family["message"] = Message::API[:success][:update_family]
        family['status'] = 200
        format.json { render json: family.to_json, status: StatusCode::Status200  }
      else
         @family["message"] = Message::API[:error][:update_family]
        format.json { render json: @family.errors, status: StatusCode::Status100 }

      end
    end
  end

  def join_family
    render :layout=>false
  end

  def search_family    
    # ToDo: search by admin's email
    assoc_family_ids = @families.map(&:id)
    if params[:family_uid]
      @families = Family.where(:id.nin => assoc_family_ids).where(:family_uid => params[:family_uid])
    else
      @parent_member_ids = ParentMember.where(email: /#{params[:email]}/).only(:id).map(&:id)
      @families = Family.where(:id.nin => assoc_family_ids, :member_id.in => @parent_member_ids)
    end
    @msg = Message::GLOBAL[:success][:search_family] if @families.blank?
    respond_to do |format|
      format.js {render :layout=> false}
      format.html {render :layout=> false}
      format.json { render :json => { :message=> @msg, :families=>JSON.parse(@families.to_json({include_owner_details: true})), :status => StatusCode::Status200} }
    end
     
  end

 
  def join_this_family
    begin
      @family = Family.find(params[:id])
      invitation_params = params[:Invitation].merge!(member_id: current_user.id)    
      @invitation = Invitation.where(invitation_params).first || Invitation.new(invitation_params)
      @invitation.member_id = current_user.id
      @invitation.role = params[:role] if params[:role].present?
      add_notification(@family.owner, "#{current_user.name} requested you to join #{@family.name}.", families_path, current_user,"family") #if @invitation.new_record?
      @invitation.save
      UserCommunication::UserActions.new.family_owner_received_join_request(current_user,@invitation)
      
      message = Message::API[:success][:join_family_request]
      status = StatusCode::Status200
    rescue Exception=>e
      @error = e.message
      message = ""
      status = StatusCode::Status100
    end
    respond_to do |format|
      format.js 
      format.html
      format.json { render :json => {:error=> @error, :message=>message, :family=>@family ,:invitation=>@invitation, :status => status} }
    end
    UserMailer.join_family_request_email(@invitation).deliver
  end

  def select_role
    @invitation = Invitation.find(params[:invitation_id])
    render layout: false
  end

  def save_role
    status = StatusCode::Status100
    @invitation = Invitation.find(params[:invitation_id])
    if @invitation.update_attributes(params[:invitation])
      @message = "Your joining request has been sent to the creator of the family. Visit <a href='families' data-remote=true>Manage Family</a>.".html_safe
      message = Message::API[:success][:save_role]
      status = StatusCode::Status200
    end  
    respond_to do |format|
      format.json { render :json => {:message=> message ,:invitation=>@invitation, :status => status} }
    end
  end

  # get detail of family
  def show
    @family = Family.find(params[:id])
    respond_to do |format|
      format.json { render json: @family,status: StatusCode::Status200  }
    end
  end
  
  #API-67 point-15
  #Post
  #/family/id/dont_want_add_spouse.json?token
  def dont_want_add_spouse
    begin
      family = Family.find(params[:family_id])
      current_user.skip_add_spouse(family.id.to_s)
      status = 200
      message = Message::GLOBAL[:success][:dont_want_add_spouse]
    rescue Exception=>e
      @error = e.message
      message = ""
      status =  100
    end
    render :json=>{:status=>status, :message=>message, :error=>@error }
  end
  
   # create new family
  def create
    @family = current_user.families.new(params[:family])
    #NOTE: params[:role] will be available by apis only. For web, it will be nil and set to administrator by default
    @family.save
    options = {:update_family_member=> true,:ip_address=>request.remote_ip}
    @family.add_family_member(params[:role],options) 
    @message = ("Your family has been added. Visit <a href='families' data-remote=true>Manage Family</a>.".html_safe) if @family.save
    message = Message::API[:success][:create_famly]
    respond_to do |format| 
      format.json { render :json=> {family: JSON.parse(Family.family_to_json(@family,@api_version,current_user)), message:message, status: StatusCode::Status200  } }
    end    
  end
  
  # delete family and accoiated entries from DB
  def destroy
    begin
      @family = Family.find(params[:id])
      message = @family.delete_family(current_user)
      find_families # get updated user families
      respond_to do |format|
        format.json { render json: {message: message,status: StatusCode::Status200}, status: StatusCode::Status200  }
      end
    rescue Exception=> e
      render :json=> {:message=> e.message, :status=>500}
    end
  end

  def find_member
    begin
      child_info,msg = current_user.find_children(params[:child_name],params[:member_type])
       child_data = []
       # is_child = params[:member_type] == "child"
       child_info.each do | child|
        is_child = child.class.to_s == "ChildMember"
        member_type = is_child == true ? 'child': 'adult'
        if is_child
          quiz_info = Child::AlexaQuiz::QuizDetail.child_quiz_info(current_user,child.id,@api_version,params)
          child_data << {member_type:member_type, age: child.age_format_for_alexa(is_child),quiz_level: child.get_quiz_level, name:child.first_name, id:child.id,matched_percentage:child[:matched_string_percentage]}.merge(quiz_info)
        else
          child_data << {member_type:member_type, age: nil,quiz_level: nil, name:child.first_name, id:child.id,matched_percentage:child[:matched_string_percentage]}
        end
      end 
      response = {message:msg, total_count:child_data.count, child_details: child_data, status: 200 }
      Report::AlexaRequestData.create(response:response, email:current_user.email,request_param:params[:child_name],raw_request_param:params.to_s) rescue nil
      render json: response
    rescue Exception=> e 
      render json: { error: e.message, message: 'Could not understand', status: 500 }
    end
  end

  private
  # list all families associate with user(invited+joined)
  def find_families
    @sent_family_invitations = current_user.sent_join_family_invitations # Needed in view at multiple places
    @received_family_invitations = current_user.received_join_family_invitations
    @families = current_user.user_families      
  end

  def set_api_version
    @api_version = 5
  end
end
