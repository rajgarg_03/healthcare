class Api::V5::VaccinationController < ApplicationController
  before_filter :set_api_version
  def new
    @member = Member.find(params[:member_id])
    @vaccination = Vaccination.new(member_id:@member.id)
    member_selected = @member.vaccinations.not_in(:opted => "false").order_by("name asc")#.map(&:sys_vaccin_id)
    @selected = {}
    member_selected.map{|k| @selected.merge!(k.sys_vaccin_id=> true) }
    @member["age"] = ApplicationController.helpers.calculate_age(@member.birth_date) rescue ""
    #vaccin_list1 = SystemVaccin.system_vaccination_list_for_user(current_user,@member)
    # @vaccin_list = vaccin_list1 
    options = {:immunisation_version=>params[:immunisation_version]}
    @reauthentication_required = current_user.reauthentication_required?(current_user,@member.id,options)

    respond_to do |format|
      format.html {render :layout=> false}
      format.json {render :json => {
        :immunisation_category=>::SystemVaccin::VaccineTypeMapping.values.uniq, 
        :sorted_vaccination_list=> Vaccination.vaccination_list(@member,current_user,options), 
        :member=>@member, 
        :reauthentication_required=>@reauthentication_required,
        :member_selected=>@selected,:status => StatusCode::Status200 } 
      }
    end
  end

  def create
    options = {}
    options[:immunisation_version] = params[:immunisation_version]
    if params[:vaccination]["name"].present?
      params[:vaccination].merge!({:opted=>"true"}) if params[:vaccination]
      @member = Member.find(params["vaccination"]["member_id"]) rescue nil
      vaccin_list1 = SystemVaccin.system_vaccination_list_for_user(current_user,nil,true,{:order_by=>"name",:immunisation_version=>params[:immunisation_version]})
      sorted_vaccination_list = Vaccination.vaccination_list(@member,current_user,options)

      @vaccin = Vaccination.new(params[:vaccination])
      @vaccin.add_to_sys_vaccin(current_user.id)
      
      member_selected = @member.vaccinations.not_in(:opted => "false").order_by("name asc")
      @selected = {}
      member_selected.map{|k| @selected.merge!(k.sys_vaccin_id=> true) }
      message = Message::API[:success][:create_immunisation]
      status = StatusCode::Status200
      added_sys_vaccin = SystemVaccin.where(id:@vaccin.sys_vaccin_id).first
      added_sys_vaccin = added_sys_vaccin.get_json_data(@member,current_user,opted_value=true,options)
      
      # added_sys_vaccin["opted"] = true
      sys_vaccin = [added_sys_vaccin]
      sys_vaccin_res = {:sorted_vaccination_list=> (sys_vaccin + sorted_vaccination_list), :selected_vaccination_list=>member_selected , :member_selected=>@selected  }
    else
      @error = true
      status = StatusCode::Status100
      message = Message::API[:error][:create_immunisation]
      sys_vaccin = []
      sys_vaccin_res = {}
    end
     respond_to do |format|
       format.js {render :layout=> false}
      format.json {render :json => {:sys_vaccination_list=>(sys_vaccin + vaccin_list1) , :vaccin =>Vaccination.vaccination_with_jabs(@vaccin), :status => status, :message=>message  }.merge(sys_vaccin_res) }

    end
  end

  def update_vaccination
    begin
      @vaccin = Vaccination.find(params[:id])
      @vaccin.update_attributes(params[:vaccination])
      # UserActions.add_update_user_action(@vaccin,"Vaccination",current_user,"Update")

      respond_to do |format|
        format.js {render :layout=> false}
        format.json {render :json=> {:vaccin=>@vaccin,:message=>Message::API[:success][:update_immunisation], :status => StatusCode::Status200} }
      end
    rescue Exception => e
      render :json=> {:message=>Message::API[:error][:update_immunisation] ,:status=> 500}
    end
    
  end
  
  def delete_vaccin
    begin
      vaccin = Vaccination.find(params[:vaccin_id])
      sys_vaccin = SystemVaccin.find(vaccin.sys_vaccin_id)
      sys_vaccin.delete if sys_vaccin.member_id.present?
      options = options || {}
      options[:immunisation_version] = params[:immunisation_version]    
    Score.where(activity_type: "Vaccination", activity_id: vaccin.id).delete rescue nil
    vaccin.jabs.where(status: "Planned").not_in(:due_on => [nil]).each do |jab|
      vaccin.member.delete_event_from_parent_google_cal(jab.id.to_s,"jab") rescue nil
    end
    vaccin.jabs.delete_all
    member_id = vaccin.member_id
    vaccin.delete
    @vaccinations = Vaccination.not_in(:opted => "false").where(member_id: member_id).entries
    status = StatusCode::Status200
    message = Message::API[:success][:delete_immunisation]
  rescue Exception=> e
    @error = e.message
    message = Message::API[:error][:delete_immunisation]
    status = StatusCode::Status100
    end
    respond_to do |format|
      format.html 
      format.js
      format.json {render :json=> {:error=>@error, :message=>message,:status => status, :deleted_vaccin=> vaccin,  :vaccinations=> Vaccination.vaccination_with_jabs(@vaccinations,options) } }
    end 
  end

  def add_vaccin
    begin
      @member = Member.find(params[:member_id])
      options = {}
      options[:immunisation_version] = params[:immunisation_version]
      system_vaccine_ids = params[:vaccin_id].split(",")
      @vaccinations = SystemVaccin.add_system_vaccine_to_member(@member,system_vaccine_ids,options={})
      @message = Message::API[:success][:add_immunisation]
      @member["age"] = ApplicationController.helpers.calculate_age(@child_member.birth_date) rescue ""
      status =  StatusCode::Status200
      @reauthentication_required = current_user.reauthentication_required?(current_user,@member.id,options)
    rescue
      @message = Message::API[:error][:add_immunisation]
      status =  StatusCode::Status100
    end
     
    respond_to do |format|
      format.json {render :json=> {:reauthentication_required=>@reauthentication_required,:message =>@message, :status => status, :member=>@member, :vaccinations=> Vaccination.vaccination_with_jabs(@vaccinations,options), :skip_next_screen=> "", :next_screen=> Vaccination::ActionList["vaccination_handhold"], :handhold_screen_data=>{} } }
    end
     
  end

  def set_api_version
    @api_version = 5
  end
end
