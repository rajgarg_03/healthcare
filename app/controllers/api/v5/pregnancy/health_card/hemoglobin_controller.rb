class Api::V5::Pregnancy::HealthCard::HemoglobinController < ApiBaseController
  before_filter :set_api_version
  def index
    begin
      data = Pregnancy::HealthCard::Hemoglobin.where(pregnancy_id:params[:pregnancy_id]).order_by("test_date desc")
      last_updated_record  = data.first.last_updated_record rescue nil
      last_record = data.last
      pregnancy = Pregnancy.where(id:params[:pregnancy_id]).last
      chart_data = data.present? ? data.collect{|rec| [pregnancy.pregnancy_week(rec.test_date),rec[:hemoglobin_count].to_f.round(1)]} : []
      who_chart_data = WhoData::Pregnancy::Chart.hemoglobin
      chart_min_max_axis = Pregnancy::HealthCard.chart_x_y(chart_data + who_chart_data[:low][0] + who_chart_data[:high][0])
      status =  StatusCode::Status200
      options = {:get_data_for_from_list=>"Haemoglobin Count"}
      member = Member.where(:id=>pregnancy.expected_member_id).last
      @tool, @show_subscription_box_status,@info_data,dummy_data = member.get_subscription_show_box_data(current_user,"Pregnancy Health Card",@api_version,options)
      if @show_subscription_box_status == true
        data = dummy_data
      else
        data = data.map(&:to_api_json)
      end
    rescue Exception=>e
      data = []
      last_updated_record = nil
      chart_data = []
      status = StatusCode::Status100
      @error = e.message
    end
    render :json=>{:free_trial_end_date_message=>@info_data[:free_trial_end_date_message],:subscription_box_data=>@info_data[:subscription_box_data],system_tool_id:@tool.id,show_subscription_box:@show_subscription_box_status,:who_chart_data=> {:low=> who_chart_data[:low][0],:high=> who_chart_data[:high][0] },:last_record=>last_record, :last_updated_record=>last_updated_record, :error=>@error,:chart_data=> chart_data,:chart_min_max_axis=> chart_min_max_axis , :data=>data, :status=>status}
  end
  
  #http://localhost:3001/api/v3/preg_hemo_glob.json?token=uSLz1sXRUgUyJXo2wZwB
  #post params={:preg_hemo_glob=>{pregnancy_id=>"", :test_date=>"",:hemoglobin_count=>""}}
  def create
    begin
      pregnancy  = Pregnancy.find(params[:preg_hemo_glob][:pregnancy_id])
      record = Pregnancy::HealthCard::Hemoglobin.create(params[:preg_hemo_glob])
      record.update_attribute(:pregnancy_week,pregnancy.pregnancy_week(record.test_date.to_date))
      status =  StatusCode::Status200
      data = Pregnancy::HealthCard::Hemoglobin.where(pregnancy_id:params[:preg_hemo_glob][:pregnancy_id])
      chart_data = data.present? ? data.collect{|rec| [rec["pregnancy_week"],rec[:hemoglobin_count].to_f.round(1)]} : []
      who_chart_data = WhoData::Pregnancy::Chart.hemoglobin
      chart_min_max_axis = Pregnancy::HealthCard.chart_x_y(chart_data + who_chart_data[:low][0] + who_chart_data[:high][0])
      message = Message::API[:success][:preg_hemoglob_create]
      last_record = data.last
    rescue Exception=>e
      @error = e.message
      status =  StatusCode::Status100
      message = Message::API[:error][:preg_hemoglob_create]
      data = []
    end
    render :json=>{:who_chart_data=> {:low=> who_chart_data[:low][0],:high=> who_chart_data[:high][0] }, :last_record=>last_record,:error=>@error,:record=> record,:message=>message,:chart_min_max_axis=> chart_min_max_axis,:chart_data=> chart_data, :data=>data.map(&:to_api_json), :status=>status}
  end

  #http://localhost:3001/api/v4/preg_hemo_glob.json?token=uSLz1sXRUgUyJXo2wZwB
  #post params={id=>, :preg_hemo_glob=>{:test_date=>"",:hemoglobin_count=>""}}
  def update
    begin
      params[:preg_hemo_glob].delete(:pregnancy_id)
      hemoglobin = Pregnancy::HealthCard::Hemoglobin.where(id:params[:id]).last
      pregnancy  = Pregnancy.find(hemoglobin.pregnancy_id)
      hemoglobin.update_attributes(params[:preg_hemo_glob])
      hemoglobin.update_attribute(:pregnancy_week,pregnancy.pregnancy_week(hemoglobin.test_date.to_date))
      data = Pregnancy::HealthCard::Hemoglobin.where(pregnancy_id:hemoglobin.pregnancy_id)
      chart_data = data.present? ? data.collect{|rec| [rec["pregnancy_week"],(rec[:hemoglobin_count].to_f.round(1)rescue 0.0)]} : []
      who_chart_data = WhoData::Pregnancy::Chart.hemoglobin
      last_record = data.last
      chart_min_max_axis = Pregnancy::HealthCard.chart_x_y(chart_data + who_chart_data[:low][0] + who_chart_data[:high][0])
      
      message = Message::API[:success][:preg_hemoglob_update]
      status =  StatusCode::Status200
    rescue Exception=>e
      data = []
      message = Message::API[:error][:preg_hemoglob_update]
      status = StatusCode::Status100
      @error = e.message
    end
     
    render :json=>{:who_chart_data=> {:low=> who_chart_data[:low][0],:high=> who_chart_data[:high][0] }, :last_record=>last_record,:error=>@error,:chart_min_max_axis=> chart_min_max_axis, :chart_data=>chart_data, :data=>data.map(&:to_api_json),:message=>message, :status=>status}
  end
  
  #Delete
  #http://localhost:3001/api/v4/preg_hemo_glob.json?token=uSLz1sXRUgUyJXo2wZwB&id=
  def destroy
    begin
      hemoglobin = Pregnancy::HealthCard::Hemoglobin.where(id:params[:id]).last
      pregnancy_id = hemoglobin.pregnancy_id
      pregnancy  = Pregnancy.find(pregnancy_id)
      hemoglobin.destroy
      data = Pregnancy::HealthCard::Hemoglobin.where(pregnancy_id:pregnancy_id)
      chart_data = data.present? ? data.collect{|rec| [rec["pregnancy_week"],rec[:hemoglobin_count].to_f.round(1)]} : []
      who_chart_data = WhoData::Pregnancy::Chart.hemoglobin
      chart_min_max_axis = Pregnancy::HealthCard.chart_x_y(chart_data + who_chart_data[:low][0] + who_chart_data[:high][0])
      message = Message::API[:success][:preg_hemoglob_delete]
      last_record = data.last
      status =  StatusCode::Status200
    rescue Exception=>e
      data = []
      chart_data = []
      message = Message::API[:error][:preg_hemoglob_delete]
      status = StatusCode::Status100
      @error = e.message
    end
    render :json=>{:who_chart_data=> {:low=> who_chart_data[:low][0],:high=> who_chart_data[:high][0] },:last_record=>last_record,:error=>@error,:chart_data=>chart_data,:chart_min_max_axis=> chart_min_max_axis, :data=>data.map(&:to_api_json),:message=>message, :status=>status}
  end
  
  def set_api_version
    @api_version = 5
  end   
end