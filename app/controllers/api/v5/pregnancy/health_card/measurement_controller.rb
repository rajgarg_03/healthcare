class Api::V5::Pregnancy::HealthCard::MeasurementController < ApiBaseController
  before_filter :set_api_version

  #http://localhost:3001/api/v5/preg_health.json?token=uSLz1sXRUgUyJXo2wZwB&preg_id=57ea71b5f9a2f382a0000002
  #params={:preg_id=>""}
 # api/v5/preg_health.json?token=uSLz1sXRUgUyJXo2wZwB&pregnancy_id=57ea71b5f9a2f382a0000002
  def index
    begin
    pregnancy = Pregnancy.find(params[:pregnancy_id])
    @member = pregnancy.member
    expected_member = Member.where(id:pregnancy.expected_member_id).last
    health_records = pregnancy.pregnancy_measurements.order_by("test_date desc")
    #@last_health_record = health_records.first
    # @recently_updated_record = pregnancy.pregnancy_measurements.order_by(:last_updated_at.desc,:test_date.desc).first
    @recently_updated_record = pregnancy.pregnancy_measurements.order_by(:created_at.desc,:test_date.desc).first
    blank_values = ["0", "0.0", "", nil]
    metric_system  = current_user.metric_system
    @health_unit = Health.health_unit(metric_system)
    @convert_unit_val_list = metric_system
    
    #@last_height_record = @member.preg_health_records.not_in(:height => values).order_by("test_date desc").first || Health.new
    @last_weight_record = pregnancy.pregnancy_measurements.not_in(:weight => blank_values).order_by("test_date desc").first || Health.new
    # @last_head_circum_record = @member.preg_health_records.not_in(:head_circum => values).order_by("test_date desc").first
     
    @last_bmi_record = pregnancy.pregnancy_measurements.not_in(:bmi => blank_values).order_by("test_date desc").first   || Health.new
    bmi_chart,weight_chart,who_data = chart_info(health_records,pregnancy)
    chart_min_max_axis = Pregnancy::HealthCard.chart_x_y(bmi_chart + who_data[:min][0] + who_data[:max][0])
    options = {:get_data_for_from_list=>"Maternal Measurement"}
    @tool, @show_subscription_box_status,@info_data,preg_health_record = expected_member.get_subscription_show_box_data(current_user,"Pregnancy Health Card",@api_version,options)
      if @show_subscription_box_status == false
        preg_health_record = health_records.map{|a| a.to_api_json(metric_system)}
      end

    render :json=> {
    :chart_min_max_axis => chart_min_max_axis,
    :who_chart_data => {min: who_data[:min][0], max: who_data[:max][0]},
    :bmi_chart=>bmi_chart,
    :preg_health_record =>preg_health_record , 
    :unit_selected=>{
       :lenght=>@convert_unit_val_list["length_unit"],
       :weight=> @convert_unit_val_list["weight_unit"]
      },
    :last_updated_record => @last_bmi_record["bmi"],
    :last_record=>(@recently_updated_record.api_convert_to_unit(@convert_unit_val_list) rescue nil), 
    :status=> StatusCode::Status200,
    :system_tool_id=>@tool.id,
    :free_trial_end_date_message=>@info_data[:free_trial_end_date_message],
    :subscription_box_data=>@info_data[:subscription_box_data],
    :show_subscription_box =>@show_subscription_box_status
  }  
    
    rescue Exception=>e 
      render :json=> {:status=> 100, :message=>e.message,:error=>e.message}
    end
  end
  #api/v5/preg_health.json?token=uSLz1sXRUgUyJXo2wZwB
  #{:preg_health=>{:test_date=>, :height=>12,:weight=>"23",:head_circum=>"33"}}
   def create
    begin
      pregnancy_id = params[:preg_health][:pregnancy_id] #=>"54412e225acefebacd00000a
      pregnancy =  Pregnancy.find(pregnancy_id)
      record = pregnancy.pregnancy_measurements.last || Pregnancy::HealthCard::Measurement.new
      user_metric_system = current_user.metric_system
      is_height_updated = params[:preg_health]["height"].present?
      is_weight_updated = params[:preg_health]["weight"].present?
      params[:preg_health] = record.convert_to_cms_kgs(user_metric_system,params[:preg_health],round_off=2)
      blank_values = ["","0","0.0",nil]
      height = is_height_updated ? params[:preg_health]["height"] : pregnancy.pregnancy_measurements.where(:test_date.lte=>(params[:preg_health][:test_date].to_date) ).not_in(:height => blank_values).order_by("test_date desc").first.height rescue 0
      weight = is_weight_updated ? params[:preg_health]["weight"] : pregnancy.pregnancy_measurements.where(:test_date.lte=>(params[:preg_health][:test_date].to_date) ).not_in(:weight =>  blank_values).order_by("test_date desc").first.weight rescue 0
      bmi = record.calulate_bmi(weight,height)
       params[:preg_health]["height"] = height
       params[:preg_health]["weight"] = weight


      params[:preg_health].merge!({:bmi=> bmi})   
      params[:preg_health][:last_updated_at] = Time.zone.now

      @health_record = Pregnancy::HealthCard::Measurement.create(params[:preg_health])
      #Pregnancy::HealthCard::Measurement.where(pregnancy_id:pregnancy.id).update_all(:height=>height) 
      @health_record.update_attribute(:pregnancy_week,pregnancy.pregnancy_week(@health_record.test_date.to_date))
      
      to_timeline = {"height"=> height,"weight"=> weight}
      @health_record.timelines << Timeline.new({entry_from: "PregnancyHealthCardMeasurement", name:bmi, desc: to_timeline.to_a.flatten.to_s, member_id:pregnancy.member_id,added_by: current_user.first_name.to_s +  "  " + current_user.last_name.to_s, timeline_at: Timeline.new.get_timeline_at(@health_record.test_date)})
      @message = Message::GLOBAL[:success][:preg_health_create] rescue "Your record has been updated."
      status = StatusCode::Status200
    rescue Exception=>e
      @message = Message::GLOBAL[:error][:preg_health_create] rescue "Your record has not been updated."
      status = StatusCode::Status100
    end
    render :json=> {:preg_health=>@health_record.to_api_json(user_metric_system),:message=>@message, :status=> status}    
  end

   
  #http://localhost:3001/api/v5/preg_health/get_detail.json?data_type=height&token=uSLz1sXRUgUyJXo2wZwB&pregnancy_id=57ea71b5f9a2f382a0000002
   def get_detail
    pregnancy = Pregnancy.find(params[:pregnancy_id])
    @member = pregnancy.member
    health_param = params[:data_type]
    metric_system = current_user.metric_system
    @health_unit = Health.health_unit(metric_system)
    @convert_unit_val_list = metric_system
    # @convert_unit_val_list["length_unit"] = "inches" if current_user.metric_system["length_unit"] == "inches"
    # @convert_unit_val_list["weight_unit"] = "lbs" if current_user.metric_system["weight_unit"] == "lbs"
     #Health::preg_health_UNIT[current_user.metric_system][health_param.to_s]
    if params[:data_type] =="all"
      blank_values = ["0","0.0", "", nil]
      
      @last_height_record = pregnancy.pregnancy_measurements.not_in(:height=> blank_values).order_by("test_date desc").first
      @last_weight_record = pregnancy.pregnancy_measurements.not_in(:weight => blank_values).order_by("test_date desc").first
      @last_head_circum_record = nil #pregnancy.pregnancy_measurements.not_in(:head_circum => values).order_by("test_date desc").first
      @last_bmi_record = pregnancy.pregnancy_measurements.not_in(:bmi => blank_values).order_by("test_date desc").first 
    end
       
    @health_records = pregnancy.pregnancy_measurements.order_by("test_date desc")
    @who_data= {}
    bmi_chart_data,weight_chart_data,who_data = chart_info(@health_records,pregnancy)
    unless params[:data_type] =="all"
      @chart_update = @health_records.not_in(params[:data_type] => blank_values).collect{|rec| [pregnancy.pregnancy_week(rec.test_date),rec[health_param].to_f]}
      @health_records = pregnancy.pregnancy_measurements.not_in(params[:data_type] => blank_values).order_by("test_date desc")
      @last_health_record = pregnancy.pregnancy_measurements.order_by("id desc").first
      data = #get_who_data(params[:data_type],@member.member_gender) #WhoHeight.where(:gender=> "1").order_by('month ASC')
      @who_data= {}
      age = get_month(Date.today) + 12
      @who_data[:p3] = [] #data.pluck(:P3)[0..age]
      @who_data[:p15] = []#data.pluck(:P15)[0..age]
      @who_data[:p85] = []#data.pluck(:P85)[0..age]
      @who_data[:p97] = []#data.pluck(:P97)[0..age]
      @who_data[:month] =  (0..40)
    end
    @recently_updated_record = pregnancy.pregnancy_measurements.order_by(:last_updated_at.desc, :test_date.desc).first
    chart_min_max_axis = Pregnancy::HealthCard.chart_x_y(@chart_update + bmi_chart_data + who_data[:min][0] + who_data[:max][0])
    # :chart_min_max_axis => chart_min_max_axis,
    render :json=> {
      :who_chart_data => {min: who_data[:min][0], max: who_data[:max][0]},
      :bmi_chart_data=>bmi_chart_data, 
       :preg_health_record => @health_records.map{|a| a.to_api_json(metric_system)}, 
      :unit_selected=>{
        :lenght=>(@convert_unit_val_list["length_unit"]),
        :weight=> (@convert_unit_val_list["weight_unit"])
        },
      :recently_updated_record => (@recently_updated_record.api_convert_to_unit(@convert_unit_val_list) rescue nil),
      :last_record=>(@last_health_record),
      :last_updated_record => (@last_health_record.api_convert_to_unit(@convert_unit_val_list) rescue nil),
      :chart_param=>params[:data_type],
      :chart_data=>@chart_update,
      :chart_min_max_axis=> Pregnancy::HealthCard.chart_x_y(@chart_update),
      
      :status=> StatusCode::Status200 }
end
  
  # http://localhost:3001/api/v5/preg_health.json?token=uSLz1sXRUgUyJXo2wZwB
  # Put
  # params={:preg_health_id=>"",preg_health=>{:height=>"",:weight=>""}}
  def update
     begin
      @health = Pregnancy::HealthCard::Measurement.find(params[:id])
      params[:preg_health][:last_updated_at] = Time.zone.now
      pregnancy = @health.pregnancy
      is_height_updated = params[:preg_health]["height"].present?
      is_weight_updated = params[:preg_health]["weight"].present?
      blank_values = ["0", "0.0","",nil]
      params[:preg_health] = @health.convert_to_cms_kgs(current_user.metric_system,params[:preg_health],round_off=2)
      height = is_height_updated ? params[:preg_health]["height"] : pregnancy.pregnancy_measurements.where(:test_date.lte=>@health.test_date).not_in(:height =>  blank_values).order_by("test_date desc").first.height rescue 0
      weight = is_weight_updated ? params[:preg_health]["weight"] : pregnancy.pregnancy_measurements.where(:test_date.lte=>@health.test_date).not_in(:weight =>  blank_values).order_by("test_date desc").first.weight rescue 0
       params[:preg_health]["height"] = height
       params[:preg_health]["weight"] = weight
      bmi = @health.calulate_bmi(weight,height)
      params[:preg_health].merge!({:bmi=> bmi})   
      
       
      @health.update_attributes(params[:preg_health])
      #Pregnancy::HealthCard::Measurement.where(pregnancy_id:pregnancy.id).update_all(:height=>params[:preg_health]["height"]) if params[:preg_health]["height"].present?
      @health.update_attribute("test_date",params[:preg_health][:test_date])
      @health.update_attribute(:pregnancy_week,pregnancy.pregnancy_week(@health.test_date.to_date))
      
      timeline = Timeline.where(timelined_type: "Pregnancy::HealthCardMeasurement", timelined_id: @health.id).first || Timeline.create(entry_from:"health", timelined_type: "Health", timelined_id: @health.id, timeline_at: Timeline.new.get_timeline_at(@health.test_date))
      to_timeline = {"height"=>params[:preg_health][:height]|| height,"weight"=>params[:preg_health][:weight] || weight}
      timeline.update_attributes({name:bmi, desc: to_timeline.to_a.flatten.to_s, timeline_at: timeline.get_timeline_at(@health.test_date)})
      @health.reload
      bmi_chart,weight_chart,who_data = chart_info(pregnancy.pregnancy_measurements,pregnancy)
      status = StatusCode::Status200
      message = Message::API[:success][:preg_health_update]
      chart_min_max_axis = Pregnancy::HealthCard.chart_x_y(bmi_chart + weight_chart + who_data[:min][0] + who_data[:max][0])
    rescue Exception=>e
      status = StatusCode::Status100
      message = Message::API[:error][:preg_health_update]
      @error = e.message
    end
    render :json=> { :preg_health=>@health , :message=>message, :status=> status, :error=>@error}
    # render :json=> {:who_data=> who_data[0] ,:preg_health=>@health, :bmi_chart=>bmi_chart, :weight_chart=>weight_chart, :message=>message, :status=> status, :error=>@error}
  end
  
  # http://localhost:3001/api/v5/preg_health/id.json?token=uSLz1sXRUgUyJXo2wZwB
  # Delete
  def destroy
    begin
      bp = Pregnancy::HealthCard::Measurement.where(id:params[:id]).last
      pregnancy_id = bp.pregnancy_id
      bp.delete
      pregnancy = Pregnancy.where(id:pregnancy_id).last
      status =  StatusCode::Status200
      message = Message::API[:success][:preg_health_delete]
      bmi_chart,weight_chart,who_data = chart_info(pregnancy.pregnancy_measurements,pregnancy)
      user_metric_system = current_user.metric_system
      chart_min_max_axis = Pregnancy::HealthCard.chart_x_y(bmi_chart + weight_chart + who_data[:min][0] + who_data[:max][0])
    rescue Exception=>e
      data = []
      message = Message::API[:error][:preg_health_delete]
      status = StatusCode::Status100
      @error = e.message
    end
    data = Pregnancy::HealthCard::Measurement.where(pregnancy_id:pregnancy_id)
    render :json=>{  :error=>@error, :data=>data.map{|a| a.to_api_json(user_metric_system)},:message=>message, :status=>status}
    # render :json=>{:who_data=> who_data[0], :error=>@error,  :bmi_chart=>bmi_chart, :weight_chart=>weight_chart, :data=>data.map{|a| a.to_api_json(user_metric_system)},:message=>message, :status=>status}
  end


  def api_convert_to_unit(rec_val,health_unit)
    (rec_val.to_f * Health::UNIT_VAL[health_unit]).to_f.round(1) rescue rec_val.to_f.round(1)
  end
  
  def get_who_data(health_parameter, gender=1)
    gender_str = gender
    if health_parameter == "weight" && current_user.metric_system["weight_unit"] =="kgs"
      WhoWeight.where(:gender=> gender_str).in(measure_unit: nil).order_by('month ASC')
    elsif  health_parameter == "weight" && current_user.metric_system["weight_unit"] =="lbs"
      WhoWeight.where(:gender=> gender_str).in(measure_unit: "lbs").order_by('month ASC')

    elsif  health_parameter == "height" && current_user.metric_system["length_unit"] =="cms"
      WhoHeight.where(:gender=> gender_str).in(measure_unit: nil).order_by('month ASC')
    elsif  health_parameter == "height" && current_user.metric_system["length_unit"] =="inches"
      WhoHeight.where(:gender=> gender_str).in(measure_unit: "inches").order_by('month ASC')
    
    elsif  health_parameter == "bmi"
          WhoBmi.where(:gender=> gender_str).order_by('month ASC')

    elsif  health_parameter == "waist_circum" && current_user.metric_system["length_unit"] =="cms"
      WhoWaist.where(:gender=> gender_str).in(measure_unit: nil).order_by('month ASC')
    elsif  health_parameter == "waist_circum" && current_user.metric_system["length_unit"] =="inches"
      WhoWaist.where(:gender=> gender_str).in(measure_unit: "inches").order_by('month ASC')
    
    elsif  health_parameter == "head_circum" && current_user.metric_system["length_unit"] =="cms"
      WhoHcf.where(:gender=> gender_str).in(measure_unit: nil).order_by('month ASC')
    elsif  health_parameter == "head_circum" && current_user.metric_system["length_unit"] =="inches"
      WhoHcf.where(:gender=> gender_str).in(measure_unit: "inches").order_by('month ASC')
    else
     (0..24).map{|a| a}
    end

  end
  def get_month(date_obj)
    begin
      date_obj = date_obj.to_date
      d1 = @member.birth_date.to_date
      d2 = date_obj
      (d2 - d1).fdiv(30).to_i
    rescue
      0
    end
    #return  (d2.year * 12 + d2.month) - (d1.year * 12 + d1.month)
  end

  def chart_info(records,pregnancy)
    Pregnancy::HealthCard::Measurement.chart_info(pregnancy)
    # blank_values = ["0","", nil,"0.0"]
    # bmi_data = records.not_in(:bmi => blank_values ).collect{|rec| [rec["pregnancy_week"],rec["bmi"].to_f.round(1)]}
    # weight_data = [[]]#records.not_in(:weight => blank_values).collect{|rec| [rec["pregnancy_week"],rec["weight"].to_f.round(1)]}
    # who_data = WhoData::Pregnancy::Chart.bmi
    # [bmi_data,weight_data,who_data]
  end

  def add_diff(health_records,convert_unit_val_list)
    data =[]
    health_records.each_with_index do |record,index|
     next_record = (health_records[index+1]).deep_dup rescue Health.new
     next_record = next_record.api_convert_to_unit(convert_unit_val_list) rescue Health.new
     record = record.api_convert_to_unit(convert_unit_val_list)
     record["diff_height"] = (record["height"].to_f - next_record["height"].to_f).to_f.round(1)   rescue  record["height"].to_f.round(1) || "-" 
     record["diff_weight"] = (record["weight"].to_f - next_record["weight"].to_f).to_f.round(1)   rescue  record["weight"].to_f.round(1) || "-" 
     record["diff_head_circum"] = (record["head_circum"].to_f - next_record["head_circum"].to_f).to_f.round(1)   rescue  record["head_circum"].to_f.round(1) || "-" 
     record["diff_waist_circum"] = (record["waist_circum"].to_f - next_record["waist_circum"].to_f).to_f.round(1)   rescue  record["waist_circum"].to_f.round(1) || "-" 
     record["diff_bmi"] = (record["bmi"].to_f - next_record["bmi"].to_f).to_f.round(1)   rescue  record["bmi"].to_f.round(1) || "-" 
     record["bmi"] = record["bmi"].to_f.round(1)
     data << record
   end
   data
  end

  def set_api_version
    @api_version = 5
  end

end
