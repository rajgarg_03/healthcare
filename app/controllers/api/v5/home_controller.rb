class Api::V5::HomeController < ApplicationController
  before_filter :set_api_version
  before_filter :authenticate_user!, :except => [:facebook_app_invite, :unsubscribe, :login, :messages,:team,:faq, :get_geoname, :index, :about, :contact, :enquiry, :terms, :privacy, :copyright, :cookies, :accept_join_family_request, :decline_join_family_request]
  def facebook_app_invite
    render layout:false
  end
  def unsubscribe
    @member = Member.where(id:params[:sub_id]).first || current_user
    mail_setting = MemberSetting.where(member_id:@member.id, name:"mail_subscribe").first
    mail_setting = mail_setting || MemberSetting.create(member_id:@member.id, name:"mail_subscribe",note:MemberSetting::MailerType.keys.join(","),status: "subscribe")
    @subscribe_mail_ids = mail_setting.note.split(",")
    if params[:resubscribe] == "true"
      subscription_id = (@subscribe_mail_ids + [params[:mtype]]).uniq
      setting_val = true
    else
      setting_value = false
      subscription_id = @subscribe_mail_ids - [params[:mtype]]
    end
    @member.subscribe_mail(subscription_id)
    respond_to do |format|
      format.json { render :json=>{:status=>status, mailer_setting: setting_val, :message=>Message::GLOBAL[:success][:unsubscribe]}}
    end
  end
  
  def get_geoname
    begin
    gids = params[:gid].split(",")
    all_data = {"totalResultsCount" => 0, "geonames" => []}
    gids.each do |gid|
      url = "http://www.geonames.org/childrenJSON?geonameId=#{gid}&style=long"
      data = JSON.parse(Net::HTTP.get_response(URI.parse(url)).body)
      all_data["totalResultsCount"] += (data["totalResultsCount"] rescue 0)
      all_data["geonames"] += (data["geonames"] rescue [])
    end
  rescue
    all_data
  end
    render :json => all_data
  end  
  
  def set_api_version
    @api_version = 5
  end
end
