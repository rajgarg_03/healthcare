 class Api::V5::DocumentsController < ApiBaseController
  before_filter :set_api_version

  # GET http://localhost:3000/api/v4/documents.json?token=XrjzH88mP4syK1q4MTeb&member_id=546128200a9a99178300000d
  # List all document uploaded by member
  def index
    member = Member.where(id: params[:member_id]).first
    options = {:current_user=>current_user,:api_version=>@api_version}
    if (current_user.user.user_type == "Internal" rescue false)
      document_tool = Nutrient.where(identifier:"Documents").last
      member.sync_data_with_clinic(current_user,@api_version,member.organization_uid,document_tool,options)
    end

    @tool,@show_subscription_box_status,@info_data,@documents = member.get_subscription_show_box_data(current_user,"Documents",@api_version,options)
    if @show_subscription_box_status == false
      @documents = member.documents.order_by("created_at desc") if member
    end
    @reauthentication_required = current_user.reauthentication_required?(current_user,member.id,options)

    member["age"] = ApplicationController.helpers.calculate_age(member.birth_date) rescue ""
    respond_to do |format|
      format.json
    end
  end
 
 # GET http://localhost:3000/api/v3/documents/types.json
 # return types of Document supported
  def types
    render json: {status: StatusCode::Status200, document_types:Document::DOC_TYPES.values} 
  end

   
  # Get /api/v3/documents/get_doc_detail.json?token=XrjzH88mP4syK1q4MTeb&doc_id
  def get_doc_detail
    @document = Document.where(id: params[:id]).first
  end

  # POST http://localhost:3000/api/v4/documents.json?token=XrjzH88mP4syK1q4MTeb
  # "member_id"=>"546128200a9a99178300000d", "document"=>{"name"=>"hiii", "document_type"=>"Health", "file"=>#<ActionDispatch::Http::UploadedFile:0x0000000b048868 @original_filename="Screenshot from 2015-02-18 21:14:05.png", @content_type="image/png", @headers="Content-Disposition: form-data; name=\"document[file]\"; filename=\"Screenshot from 2015-02-18 21:14:05.png\"\r\nContent-Type: image/png\r\n", @tempfile=#<File:/tmp/RackMultipart20150307-3267-reec7w>>}
  def create
    member = Member.where(id: params[:member_id]).first
    document = member.documents.new(params[:document]) if member
    document.file = params[:media_files].first if params[:media_files].present?
    respond_to do |format|
      if document && document.save
        message = Message::API[:success][:add_document]
        format.json { render json: document.attributes.merge(file_path: document.file.path, message: message,status: StatusCode::Status200) }
      else
        message = Message::API[:error][:add_document]
        format.json { render json: {:message=> message, :status=> StatusCode::Status100, :errors => document.try(:errors)} }
      end
    end
  end

  # PUT http://localhost:3000/api/v4/documents/54fa95c20a9a995f0c000002.json?token=XrjzH88mP4syK1q4MTeb
  # Parameters: {"member_id"=>"546128200a9a99178300000d", "document"=>{"name"=>"doc3 updated", "document_type"=>"\"General\""}, "token"=>"XrjzH88mP4syK1q4MTeb", "id"=>"54fa95c20a9a995f0c000002"}
  # Parameters: {"utf8"=>"✓", "authenticity_token"=>"csBsF7lIRuCZfKCd2CgHOHlOAafYxRn5u+C+jH1tQfU=", "member_id"=>"546128200a9a99178300000d", "document"=>{"name"=>"doc3 updated", "document_type"=>"General", "file"=>#<ActionDispatch::Http::UploadedFile:0x00000007fee320 @original_filename="Screenshot from 2015-02-18 21:14:05.png", @content_type="image/png", @headers="Content-Disposition: form-data; name=\"document[file]\"; filename=\"Screenshot from 2015-02-18 21:14:05.png\"\r\nContent-Type: image/png\r\n", @tempfile=#<File:/tmp/RackMultipart20150307-4305-1fu4z6v>>}
  def update
    member = Member.where(id: params[:member_id]).first
    document = member.documents.find(params[:id]) if member
    document.file = params[:media_files].first if params[:media_files].present?
    respond_to do |format|
      if document.update_attributes(params[:document])        
        message = Message::API[:success][:update_document]
        format.json { render json: document.attributes.merge(file_path: document.file.path, message: message,status: StatusCode::Status200 ) }
      else
        message = Message::API[:success][:update_document]
        format.json { render json: {:errors => document.errors, :message=> message, :status=> StatusCode::Status100} }
      end
    end
  end 

  # DELETE http://localhost:3000/api/v4/documents/54fa95c20a9a995f0c000002.json?token=XrjzH88mP4syK1q4MTeb
  def destroy
    @document = Document.where(id: params[:id]).first
    @document.destroy
    respond_to do |format|
      format.json
    end
  end 

   def get_clinic_document_list
    begin
      options = {}
      member_id = params[:member_id]
      member = Member.find(member_id)
      options[:current_user] = current_user
      response = ::Clinic::Document.list_document(current_user,member,options)
      @reauthentication_required = current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      @error = e.message
    end
    render :json=>response.merge(:reauthentication_required=>@reauthentication_required)
  end
  
  #params = {:member_id=>"", :document_id=>''}
  def clinic_document_content
    begin
      options = {}
      member_id = params[:member_id]
      member = Member.find(member_id)
      document = Document.find(params[:document_id])
      document_id = document.uid
      options[:current_user] = current_user
      response = ::Clinic::Document.document_detail(current_user,member,document_id,options)
      @reauthentication_required = current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      @error = e.message
    end
    render :json=>response.merge(:reauthentication_required=>@reauthentication_required)
  end

  # GET http://localhost:3000/api/v4/documents/search?utf8=%E2%9C%93&member_id=546128200a9a99178300000d&Document%5Bname%5D=doc
  def search
    member = Member.find(params[:member_id])
    options = {}
    @tool, @show_subscription_box_status,@info_data,documents = member.get_subscription_show_box_data(current_user,"Documents",@api_version,options)
    if @show_subscription_box_status == false
      if params[:Document][:name].present?
        documents = member.documents#.any_of(name: /#{params[:Document][:name]}.*/)
      else
        documents = member.documents
      end
    end
     
    if @show_subscription_box_status
       render :json=>{:free_trial_end_date_message=>@info_data[:free_trial_end_date_message],:subscription_box_data=>@info_data[:subscription_box_data],:documents=>documents,:show_subscription_box=>@show_subscription_box_status, :system_tool_id=>@tool.id}
    else
       render :json=> documents
    end
   
  end 

  # DELETE http://localhost:3000/api/v4/documents/548bfd0e0a9a997d21000008/delete_attachment
  def delete_attachment
    @document = Document.find(params[:id])
    @document.file = nil
    @document.save
    respond_to do |format|
      format.json
    end
  end

   
  def upload_file
    @document = Document.find(params[:id])
    decode_and_save_file(@document,params[:file_data]) if params[:file_data].present?
  rescue
    render json: {status: StatusCode::Status100}
  end

 
  def set_api_version
    @api_version = 5
  end

  private

  def decode_and_save_file(nutrient,image_data)
    member = nutrient.member
    content = image_data
    content.gsub!('\\r', "\r")
    content.gsub!('\\n', "\n")
    decode_base64_content = Base64.decode64(content)
    File.open("public/documents/#{nutrient.class.name.downcase}_#{nutrient.id}.jpg", "wb") do |f|
      f.write(decode_base64_content)
    end
    file = File.open("public/documents/#{nutrient.class.name.downcase}_#{nutrient.id}.jpg")
    nutrient.file = file
    nutrient.save if nutrient.valid?
    File.delete("public/documents/#{nutrient.class.name.downcase}_#{nutrient.id}.jpg")
  end

end  