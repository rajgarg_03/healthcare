class Api::V5::CalendarController < ApplicationController
  before_filter :set_api_version

  def index
    session[:show_variable_home] = 'Calendar'
    session[:dashboard] = params["type"]
    session[:type] = params[:member_id] #.present? ? "child" : "parent"
    @member = Member.find(params[:member_id]|| current_user.id) #rescue current_user
    @event_fetch_url = params["type"] == "child" ? "get_data?" : "get_parent_data"
    # redirect_to root_url if (session[:selected_member_id].nil? || session[:selected_parent_id].nil?) and params[:id].nil?
    if params["type"] == "child"
      session[:selected_member_id] = params[:id]
    else
      session[:selected_parent_id] = params[:id]
    end
    @google_synced = MemberSetting.where(member_id: current_user.id, name:"google_syn",status: "true").first

    info_popup_status(@member,"calendar")
    respond_to do |format|
      format.js {render :layout=> false}
      format.html
    end
  end
  def new 
    @event = Event.new
    if params[:date].present?
    selected_date =  params[:date].split("/")
    date_form = "#{selected_date[2]}-#{selected_date[0]}-#{selected_date[1]}" 
      @event.start_time = date_form.to_date rescue nil 
      @event.end_time = date_form.to_date rescue nil 
    end
    render :layout=> false
  end


  # api : create new event, returns event created and message
  # GET "/calendar/get_event_datail.json?token=asdasdevent_id=1212" 
  def get_event_datail
    begin
      event = Event.find(params[:event_id])
      message = "Event found"
      status  = StatusCode::Status200
    rescue
      event = nil
      status  = StatusCode::Status100
      message = "No event found"
    end
    render json: {:message => message, :event=>event,:status=>status}
  end
   

  # api : create new event, returns event created and message
  # POST "/calendar/create_event.json?token=asdasd" 
  # Parameters: "event"=>{"name"=>"WW", "member_id"=>"541ebcb88cd03ddfc4000002", "all_day"=>"0", "start_date"=>"19-06-2015", "start_time"=>"11:19", "end_date"=>"19-06-2015", "end_time"=>"12:19", "description"=>""}

  def create_event
    if (params[:event][:all_day].to_s == "1" rescue false)
      params[:event][:end_time] = "23:55"
      params[:event][:start_time] = "00:00"
    end
    status = StatusCode::Status100
    params[:event][:start_date] = params[:event][:start_date] + " " + params[:event][:start_time] rescue params[:event][:start_date]
    params[:event][:end_date] = params[:event][:end_date] + " " + params[:event][:end_time] rescue params[:event][:end_date]
    params[:event][:start_time]= params[:event][:start_date]
    params[:event][:end_time] = params[:event][:end_date]
    @event = Event.new(params[:event])
    message = Message::API[:error][:event_create]
    if @event.save
      #UserCommunication::UserActions.add_user_action(@event,"Calendar",current_user,"Add")
      @event.member.send_event_to_parent_google_cal(@event) rescue nil
      message = Message::API[:success][:event_create]
      status = StatusCode::Status200
    end
    respond_to do |format|
      format.js {render :layout=> false}
      format.html
      format.json {render :json => {:event=> @event, :message=>message, :status=>status } }
    end
  end

  def edit_event
    @event = Event.find(params[:event_id])
    render :layout=> false
  end
  
  #API : Update selected event
  # return event and message
  # PUT "/calendar/event_update"
  # Parameters: {"event"=>{"name"=>"d test ", "all_day"=>"1", "start_date"=>"03-06-2015", "end_date"=>"12-06-2015", "description"=>""}, "event_id"=>"556dcb608cd03dfbef000003"}


  def event_update
    if (params[:event][:all_day].to_s == "1" rescue false)
      params[:event][:end_time] = "23:55"
      params[:event][:start_time] = "00:00"
    end
    params[:event][:start_date] = params[:event][:start_date] + " " + params[:event][:start_time] rescue params[:event][:start_date]
    params[:event][:end_date] = params[:event][:end_date] + " " + params[:event][:end_time] rescue params[:event][:end_date]
    params[:event][:start_time]= params[:event][:start_date]
    params[:event][:end_time] = params[:event][:end_date]
    @event = Event.find(params[:event_id])
    #if @event.update_calendar(current_user,params[:event])
       @event.update_attributes(params[:event])
      # UserCommunication::UserActions.add_user_action(@event,"Calendar",current_user,"update")
       @event.member.send_event_to_parent_google_cal(@event,"event","update") rescue nil
      message = Message::API[:success][:update_event]
      status = StatusCode::Status200

    # else
    #   message = Message::API[:error][:update_event]
    #   status = StatusCode::Status100
    # end
    
    respond_to do |format|
      format.js   
      format.html
      format.json {render :json => {:event=> @event, :message=>message, :status=>status} }
    end
  end

  #API : delete selected event
  #Get calendar/event_delete.json?event_id&event_type
  # return event and message
  def event_delete
    begin
      params[:event_type] =  params[:event_type].downcase rescue  params[:event_type]
      if params[:event_type]== "jab"
        @event = Jab.find(params[:event_id])
        @event.destroy
      elsif params[:event_type] == "prenataltest"
        medical_event = MedicalEvent.find(params[:event_id])
        @event = medical_event
        medical_event.delete_medical_event
      elsif params[:event_type] == "checkupplanner"
        checkup_schedule = Child::CheckupPlanner::Checkup.where(id:params[:event_id]).last
        @event = checkup_schedule
        checkup_schedule.delete_schedule(current_user)
      elsif params[:event_type] == "activityplanner"
        activity_planner = Child::ActivityPlanner::Activity.where(id:params[:event_id]).delete_all
        @event = activity_planner
      else
        @event = Event.find(params[:event_id])
        @event.member.delete_event_from_parent_google_cal(@event.id.to_s)
        @event.destroy
      end
      
      status = StatusCode::Status200
      message = Message::API[:success][:event_delete]
    rescue Exception=>e 
      status = StatusCode::Status100
      message = Message::API[:error][:event_delete]
      @error = e.message
    end
    respond_to do |format|
      format.js  
      format.html
      format.json {render :json => {:error=>@error, :event=> @event,:status=> status, :message=>message} }

    end
  end

  def get_parent_data
    # parent = selected_parent
    # s_time = Time.at(params['start'].to_i).to_formatted_s(:db)
    # e_time = Time.at(params['end'].to_i).to_formatted_s(:db)
    calendar_data = []
    if session[:type].blank?
    Event.where(:member_id =>current_user.id).each do |event|
    calendar_data << {:id => event.id,
          :title => (event.name rescue ""),
          :description => event.description,
          :start => "#{(event.start_date|| Date.today).iso8601}",
          :end => "#{(event.end_date || Date.today).iso8601}",
          :allDay => (event.all_day rescue false),
          :recurring => false,
          :backgroundColor => "#C0D230",
          :textColor => "black",
          :type => "Event"
        } 
    end
    Event.all.where({'member_id' => { "$in" => current_user.childern_ids} }).each do |event|
    calendar_data << {:id => event.id,
          :title => (event.name rescue ""),
          :description => event.description,
          :start => "#{(event.start_date|| Date.today).iso8601}",
          :end => "#{(event.end_date || Date.today).iso8601}",
          :allDay => (event.all_day rescue false),
          :recurring => false,
          :backgroundColor => "#C0D230",
          :textColor => "black",
          :type => "Event"
        } 
    end

    vaccinations = Vaccination.all.where({'member_id' => { "$in" => current_user.childern_ids} }).where(opted:"true").includes(:jabs)
    

    else
      vaccinations = Vaccination.all.where('member_id' => session[:type]).where(opted:"true").includes(:jabs)
      Event.all.where('member_id' => session[:type]).each do |event|
      calendar_data << {:id => event.id,
            :title => (event.name rescue ""),
            :description => event.description,
            :start => "#{(event.start_date|| Date.today).iso8601}",
            :end => "#{(event.end_date || Date.today).iso8601}",
            :allDay => (event.all_day rescue false),
            :recurring => false,
            :backgroundColor => "#C0D230",
            :textColor => "black",
            :type => "Event"
          } 
      end
    end
    
     vaccinations.each do |vaccin|
      vaccin.jabs.where(status: "Planned").or(:due_on.ne => nil).or(:est_due_time.ne => nil).each do |cpe|
        if cpe.due_on.blank? && !cpe.est_due_time.blank?
          status = "-Estimated due on"
        else
          status = "-Due On"
        end
        calendar_data<< {:id => cpe.id,
          :title => ("#{vaccin.get_name} -" + (cpe.name || "Jab" + (vaccin.jabs.map(&:id).index(cpe.id)+1).to_s )+  status).slice(0..25),
          :description => cpe.desc,
          :start => "#{(cpe.due_on|| cpe.est_due_time || Date.today).iso8601}",
          :end => "#{(cpe.due_on || cpe.est_due_time || Date.today).iso8601}",
          :allDay => true,
          :recurring => false,
          :backgroundColor => "#C0D890",
          :textColor => "black",
          :type => "Jab"
        }
     end
   end
      
    render :json => calendar_data.to_json
  end
  
  
  def get_data
    s_time = Time.at(params['start'].to_i).to_formatted_s(:db)
    e_time = Time.at(params['end'].to_i).to_formatted_s(:db)

    calendar_data = []
    if params[:type].present?
      render :text=>params[:type].inspect and return
    else
      calendar_data = populate_exams(calendar_data, s_time, e_time)
      calendar_data = populate_assignments(calendar_data, s_time, e_time)
      calendar_data = populate_holidays(calendar_data, s_time, e_time)
      calendar_data = populate_activities(calendar_data, s_time, e_time)
    end
    render :json => calendar_data.to_json
  end

  def calendar_info
    render layout: false
  end
  
  def set_api_version
    @api_version = 5
  end

  private
  
  def format_time(time_str)
    hour = time_str.to_i
    if time_str.match(/PM/)
      hour = time_str.gsub('PM','').to_i + 12
      hour = "00" if hour >= 24
    end
    mins = time_str.split(":")[1].match(/\d+/).to_s
    return "#{hour}:#{mins}"
  end
  
end
