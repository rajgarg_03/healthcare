class Api::V5::MeasurementsController < ApplicationController
  before_filter :set_api_version
  
  def get_details
    begin
      round_off_val = params[:decimal_round_off] || 2
      unit_text = {"kgs"=>"kilograms","cms"=>"centimeters", "lbs"=>"pounds", "inches"=>"inches"}
       if params[:child_id].present?
         child = ChildMember.find(params[:child_id])
         child_name = child.first_name.capitalize 
       end

       unless child
         child = Family.get_first_family_first_child(current_user)
         child_name = child.first_name.capitalize 
       end
      @user_metric_system = current_user.metric_system
      user_metric_system= {}
      user_metric_system["length_unit"] =  "inches" if @user_metric_system["length_unit"] == "inches" #? "inches" : "cms"      
      user_metric_system["weight_unit"] =  "lbs" if @user_metric_system["weight_unit"] == "lbs"  #? "lbs" : "kgs"
      
      health_record = child.health_records.order_by(updated_at: 'desc').first#.api_convert_to_unit(user_metric_system,round_off_val) rescue ''
      unless health_record.blank?
        text = ''
        if params[:intent] == 'GetHeight'
          health_record = child.health_records.not_in(:height=>["",nil,0,0.0]).order_by(updated_at: 'desc').first.api_convert_to_unit(user_metric_system,round_off_val) rescue nil
          text += (child_name + '\'s height is ' + health_record["height"].to_s + ' ' +  unit_text[@user_metric_system["length_unit"]]) rescue ''
        elsif params[:intent] == 'GetWeight'
          health_record = child.health_records.not_in(:weight=>["",nil,0,0.0]).order_by(updated_at: 'desc').first.api_convert_to_unit(user_metric_system,round_off_val) rescue nil
          text += (child_name + '\'s weight is ' + health_record["weight"].to_s + ' ' + unit_text[@user_metric_system["weight_unit"]]) rescue ''
        elsif params[:intent] == 'GetHeadCF'
          health_record = child.health_records.not_in(:head_circum=>["",nil,0,0.0]).order_by(updated_at: 'desc').first.api_convert_to_unit(user_metric_system,round_off_val) rescue nil
          text += (child_name + '\'s head circumference is ' + health_record["head_circum"].to_s + ' ' + unit_text[@user_metric_system["length_unit"]]) rescue ''
        elsif params[:intent] == 'GetMeasurements'
          text += "#{child_name}'s" + " "
          arr = []
          health_record_height = (child.health_records.not_in(:height=>["",nil,0,0.0]).order_by(updated_at: 'desc').first).dup.api_convert_to_unit(user_metric_system,round_off_val) rescue nil
          health_record_weight = (child.health_records.not_in(:weight=>["",nil,0,0.0]).order_by(updated_at: 'desc').first).dup.api_convert_to_unit(user_metric_system,round_off_val) rescue nil
          health_record_hc =     (child.health_records.not_in(:head_circum=>["",nil,0,0.0]).order_by(updated_at: 'desc').first).dup.api_convert_to_unit(user_metric_system,round_off_val) rescue nil
          if (health_record_height.present? && health_record_height["height"].present?)
            arr << 'height is ' + health_record_height["height"].to_s + ' ' + unit_text[@user_metric_system["length_unit"]] rescue ''
          end
          if (health_record_weight.present? && health_record_weight["weight"].present?)
            arr << 'weight is ' + health_record_weight["weight"].to_s + ' ' + unit_text[@user_metric_system["weight_unit"]] rescue ''
          end
          if (health_record_hc.present? && health_record_hc["head_circum"].present?)
            arr << ('head circumference is ' + health_record_hc["head_circum"].to_s + ' ' + unit_text[@user_metric_system["length_unit"]]) rescue ''
          end
          text += arr.to_sentence(:last_word_connector => ' and ')
        else
          text += 'Please check parameters'
        end
         text = "Please update #{child_name}'s measurements" if health_record.blank?
        render json: { message: text, status: 200 }
      else
        render json: { message: "Please update #{child_name}'s measurements",matched_string_percentage:child["matched_string_percentage"], status: 200 }
      end
    rescue Exception=>e
      render json: { error:e.message, message: 'I am unable to contact Nurturey at this time. Let’s try again later.', status: 100 }
    end
  end

  def set_api_version
    @api_version = 5
  end
end
