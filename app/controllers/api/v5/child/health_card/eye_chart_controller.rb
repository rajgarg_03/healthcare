class Api::V5::Child::HealthCard::EyeChartController < ApiBaseController
  before_filter :set_api_version

  def list
    data = {}
    begin
      member =  Member.find(params[:member_id])
      options = {:get_data_for_from_list=>"Eye Sight"}
      tool_identifier_for_subscription_box = params[:tool_identifier] || "Child Health Card"
      @tool, @show_subscription_box_status,@info_data,eye_data_list = member.get_subscription_show_box_data(current_user,tool_identifier_for_subscription_box,@api_version,options)
      if @show_subscription_box_status == false
        eye_data_list = Child::HealthCard::EyeChart.get_list(params[:member_id])
      end
      status =  StatusCode::Status200
    rescue Exception=>e
      status =  StatusCode::Status100
      error = e.message
    end

    data.merge!({:free_trial_end_date_message=>@info_data[:free_trial_end_date_message],:subscription_box_data=>@info_data[:subscription_box_data],system_tool_id:@tool.id, show_subscription_box:@show_subscription_box_status,status: status, error: error, eye_chart: eye_data_list, eye_chart_last_record: eye_data_list.first })
    render json: data
  end

  def create_eye_data
    begin
      check_date_available and return
      check_if_all_params_nil and return
      data_with_date = Child::HealthCard::EyeChart.where(date: params[:eye_sight_chart][:date]).where(member_id: params[:eye_sight_chart][:member_id]).first
      if data_with_date
        data_with_date.update_attributes(params[:eye_sight_chart])
      else
        Child::HealthCard::EyeChart.create_record(params[:eye_sight_chart])
      end
      status = StatusCode::Status200
      message = Message::API[:success][:eye_sight]
    rescue Exception=>e
      error = e.message
      message = Message::API[:error][:eye_sight]
      status =  StatusCode::Status100
    end
    render json: {error: error, message: message, status: status }
  end

  def update_eye_data
    begin
      if params[:eye_sight_chart][:member_id].present?
        check_date_available and return
        check_if_all_params_nil and return if params[:eye_sight_chart][:date].nil?
        data_with_date = Child::HealthCard::EyeChart.where(date: params[:eye_sight_chart][:date]).where(member_id: params[:eye_sight_chart][:member_id]).first
        if data_with_date && data_with_date.id.to_s != params[:eye_sight_chart][:id]
          data_with_id = Child::HealthCard::EyeChart.find(params[:eye_sight_chart][:id])
          data_with_id.destroy
          data_with_date.update_attributes(params[:eye_sight_chart])
        else
          Child::HealthCard::EyeChart.update_record(params[:eye_sight_chart])
        end
        status = StatusCode::Status200
        message = Message::API[:success][:update_eye_sight]
      else
        status = StatusCode::Status100
        error = Message::API[:error][:update_eye_sight]
        message = Message::API[:error][:update_eye_sight]
      end
    rescue Exception=>e
      status = StatusCode::Status100
      error = e.message
      message = Message::API[:error][:update_eye_sight]
    end
    render json: { status: status, error: error, message: message }
  end

  def destroy_eye_data
    begin
      eye_data = Child::HealthCard::EyeChart.find(params[:id])
      eye_data.destroy
      status = StatusCode::Status200
      message = Message::API[:success][:delete_eye_sight]
    rescue Exception=>e
      status = StatusCode::Status100
      error = e.message
      message = Message::API[:error][:delete_eye_sight]
    end
    render json: { status: status, error: error, message: message }
  end
  
  def set_api_version
    @api_version = 5
  end

  private

  def check_date_available
    if params[:eye_sight_chart][:date].blank? || params[:eye_sight_chart][:date].nil?
      status = StatusCode::Status100
      error = Message::API[:error][:eye_date_validation]
      message = Message::API[:error][:eye_date_validation]
      render json: { status: status, error: error, message: message } and return true
    end
  end

  def check_if_all_params_nil
    val = params[:eye_sight_chart].except(:date, :member_id, :id).delete_if{|key,value| value.blank?}
    if val.blank?
      status = StatusCode::Status100
      error = Message::API[:error][:eye_data_validation]
      message = Message::API[:error][:eye_data_validation]
      render json: { status: status, error: error, message: message } and return true
    end
  end

end
