class Api::V5::Child::HealthCard::AllergySymptomController < ApiBaseController
  before_filter :set_api_version
  
  #http://localhost:3000/api/v4/child/health_card/allergy_symptom/list_all_allergy_symptom.json?token=gcjemgbLpVAjbjV4jyzs&allergy_id=59004378f9a2f37963000002
  def list_all_allergy_symptom
    all_allergies = AllergySymptomManager.get_list(current_user,params[:allergy_id]) #where(member_id:[nil,current_user.id])
    status = StatusCode::Status200
    options = {}
    @reauthentication_required= current_user.reauthentication_required?(current_user,current_user.id,options)
    render :json=>{ :reauthentication_required=>@reauthentication_required, :all_allergie_symptoms=> all_allergies, :status=>status}
  end
  
  #http://localhost:3000/api/v4/child/health_card/allergy_symptom/add_symptom_to_allergy_symptom_manager.json?token=gcjemgbLpVAjbjV4jyzs&allergy_id=59004378f9a2f37963000002
  #post params={"name"=>"xyz", "category"=>"water", "country"=>"uk",allergy_id=>}
  def add_symptom_to_allergy_symptom_manager
    begin
      allergy_symptom = AllergySymptomManager.add_new_symptom(current_user,params)
      status = StatusCode::Status200
      all_allergy_symptoms = AllergySymptomManager.get_list(current_user,params[:allergy_id]) 
      message = Message::API[:error][:create_new_symptom]
    rescue Exception=>e
      @error = e.message
      message = Message::API[:error][:create_new_symptom]
      status =  StatusCode::Status100
    end
    render :json=>{:error=>@error,:allergy_symptom=> allergy_symptom,:message=>message,:all_allergie_symptoms => all_allergy_symptoms, :status=>status}
  end

   

  def delete_symptom
    begin
      allery_symptom_manager = AllergySymptomManager.where(id:params[:id]).last
      allery_symptom_manager.delete
      all_allergies = AllergySymptomManager.get_list(current_user)  
      message = Message::API[:success][:delete_symptom]
      status =  StatusCode::Status200
    rescue Exception=>e
      status = StatusCode::Status100
      message = Message::API[:error][:delete_symptom]
      @error = e.message
    end
    render :json=>{:error=>@error,:all_allergie_symptoms => all_allergy_symptoms ,:message=>message, :status=>status}
  end

  def set_api_version
    @api_version = 5
  end
   
end
