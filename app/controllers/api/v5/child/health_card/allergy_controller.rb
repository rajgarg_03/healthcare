class Api::V5::Child::HealthCard::AllergyController < ApiBaseController
  before_filter :set_api_version
    
  #http://localhost:3000/api/v4/child/health_card/allergy/list_all_allergies.json?token=gcjemgbLpVAjbjV4jyzs&member_id=
  def list_all_allergies
    all_allergies = AllergyManager.get_list(current_user,params) 
    options = {}
    status = StatusCode::Status200
    @reauthentication_required= current_user.reauthentication_required?(current_user,current_user.id,options)

    render :json=>{:reauthentication_required=>@reauthentication_required, :allergies=> all_allergies, :status=>status}
  end
  
   #http://localhost:3000/api/v4/child/health_card/allergy/list_all_allergies.json?token=gcjemgbLpVAjbjV4jyzs&member_id=
   def list_member_allergies
      status = StatusCode::Status200
      options = {}
      all_allergies = Child::HealthCard::Allergy.get_list(current_user,params[:member_id]) 
      @reauthentication_required= current_user.reauthentication_required?(current_user,params[:member_id],options)
      message = Message::API[:success][:update_allergy]
    render :json=>{:reauthentication_required=>@reauthentication_required, :allergies=> all_allergies, :status=>status}
  end
   

  #http://localhost:3000/api/v4/child/health_card/allergy/add_allergy_to_allergy_manager.json?token=gcjemgbLpVAjbjV4jyzs
  #Post Params={"name"=>"abc", "category"=>"food",:member_id=>}
  def add_allergy_to_allergy_manager
    begin
      allergy = AllergyManager.add_new_allergy(current_user,params)
      status = StatusCode::Status200
      params[:category] = "all" 
      all_allergies = AllergyManager.get_list(current_user,params) 
      message = Message::API[:success][:add_allergy_to_allergy_manager]
    rescue Exception=>e
      @error = e.message
      message = Message::API[:error][:add_allergy_to_allergy_manager]
      status =  StatusCode::Status100
      data = []
    end
    render :json=>{:error=>@error,:record=> allergy,:message=>message,:allergies => all_allergies, :status=>status}
  end


  #http://localhost:3000/api/v4/child/health_card/allergy/add_allergy_to_member_list.json?token=gcjemgbLpVAjbjV4jyzs
  #post params={"allergy_manager_ids"=>[],member_id=>""}
  def add_allergy_to_member_list
    begin
      allergy = Child::HealthCard::Allergy.add_allergy(current_user,params)
      status = StatusCode::Status200
      all_allergies = Child::HealthCard::Allergy.get_list(current_user,params[:member_id]) 
      message = Message::API[:success][:add_allergy_to_member_list]
    rescue Exception=>e
      @error = e.message
      message = Message::API[:error][:add_allergy_to_member_list]
      status =  StatusCode::Status100
      data = []
    end
    render :json=>{:allergies=>all_allergies, :error=>@error,:record=> allergy,:message=>message,  :status=>status}
  end

  #http://localhost:3000/api/v4/child/health_card/allergy/update_allergy.json?token=gcjemgbLpVAjbjV4jyzs
  #Put params={"notes"=>"no notes avaible", "severity"=>"medium", "allergy_date"=>"12-12-2016", "member_id"=>"57c0213af9a2f3c16400000a", "allergy_symptoms"=>"a,c,v,b", "name"=>"abc", "allergy_id"=>"59004378f9a2f37963000002"} 
  def update_allergy
    begin
      allergy = Child::HealthCard::Allergy.find(params[:allergy_id])
      params[:member_id] = allergy.member_id
      allergy.update_allergy(current_user,params)
      status = StatusCode::Status200
      all_allergies = Child::HealthCard::Allergy.get_list(current_user,params[:member_id] ) 
      message = Message::API[:success][:update_allergy]
    rescue Exception=>e
      @error = e.message
      message = Message::API[:error][:update_allergy]
      status =  StatusCode::Status100
      data = []
    end
    render :json=>{:allergies=>all_allergies, :error=>@error,:record=> allergy,:message=>message,  :status=>status}
  end
  
  #http://localhost:3000/api/v4/child/health_card/allergy/delete_allergy.json?token=gcjemgbLpVAjbjV4jyzs&allergy_id=
  #delete
  def delete_allergy
    begin
      allergy = Child::HealthCard::Allergy.find(params[:allergy_id])
      params[:member_id] = allergy.member_id
      allergy.delete
      message = Message::API[:success][:delete_allergy]
      status =  StatusCode::Status200
      all_allergies = Child::HealthCard::Allergy.get_list(current_user,params[:member_id]) 
    rescue Exception=>e
      data = []
      status = StatusCode::Status100
      message = Message::API[:error][:delete_allergy]
      @error = e.message
    end
    render :json=>{:allergies=>all_allergies, :error=>@error, :message=>message, :status=>status}
  end
    
  def set_api_version
    @api_version = 5
  end
  
end
