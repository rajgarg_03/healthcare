class Api::V5::Child::CheckupPlanner::CheckupScheduleController < ApiBaseController
  before_filter :set_api_version

  # Api get_checkup_plans
  # Get /api/v4/child/checkup_planner/checkup_schedule/get_checkup_plans.json?token=gcjemgbLpVAjbjV4jyzs&member_id=57c0213af9a2f3c16400000a
  # params =>{member_id,type}
  def get_checkup_plans
    begin
      member_id =  params[:member_id]
      member = Member.find(member_id)
      recommended_pointer = nil #ArticleRef.get_recommended_pointer(current_user,child.id,"health")
      # type past/upcoming /all
      type = params[:type].downcase rescue "all"
      options = {:get_data_for_from_list=>type}
      @tool, @show_subscription_box_status,@info_data,schedules = member.get_subscription_show_box_data(current_user,"Checkup Planner",@api_version,options)
      if @show_subscription_box_status == false
        schedules = Child::CheckupPlanner::Checkup.get_list_with_test(current_user,member_id,type,@api_version)
      end
      # last_records = {:schedule=>schedules.last}
      if type == "past" || type == "upcoming"
        response_data = {:checkup_plans=>schedules, :month_order=>(schedules.keys rescue [])}   
      else
        summary = Child::CheckupPlanner::Checkup.checkup_summary(current_user,member_id,@api_version)
        response_data = {:checkup_plans=>schedules , recommended_pointers:recommended_pointer, :summary=>summary  } 
      end 
      status = StatusCode::Status200
      @reauthentication_required = current_user.reauthentication_required?(current_user,member.id,options)
      response_data[:reauthentication_required] = @reauthentication_required
      msg = Message::API[:success][:get_schedule_list]
      response_data.merge!(:free_trial_end_date_message=>@info_data[:free_trial_end_date_message],:subscription_box_data=>@info_data[:subscription_box_data],:show_subscription_box=>@show_subscription_box_status, :system_tool_id=>@tool.id,:status=>status,:message=>msg)
      render :json => response_data
    rescue Exception=> e 
      render :json=> {:message=> e.message, :status=> StatusCode::Status100}
    end

  end
 
  # Method: POST
  # Url: api/v4/child/checkup_planner/checkup_schedule/add_test_to_checkup_plan"
  # params: {"system_test_ids": ["5acef236f9a2f3dab60002a8","5acef236f9a2f3dab60002ad"], "checkup_plan_id": "5ad07e64f9a2f35fe1000082"}
  def add_test_to_checkup_plan
    begin
      unless params[:system_test_ids].blank? || params[:system_test_ids] == "[]" || params[:checkup_plan_id].blank?
        checkup = Child::CheckupPlanner::Checkup.find(params[:checkup_plan_id])
        checkup.add_test_to_checkup(params[:system_test_ids])
        status =  StatusCode::Status200
        message = Message::API[:success][:add_test_to_checkup_plan]
      else
        message = params[:system_test_ids].blank? || params[:system_test_ids] == "[]" ? 
                            Message::API[:error][:system_ids_blank_checkup_plan] : 
                            Message::API[:error][:add_test_to_checkup_plan]
        status =  StatusCode::Status100
      end
    rescue Exception=> e 
      message = Message::API[:error][:add_test_to_checkup_plan]
      status =  StatusCode::Status100
    end
    render :json=>{:message=>message,:status=>status}
  end


  # Api -get_checkup_test_list
  # get http://localhost:3000/api/v4/child/checkup_planner/checkup_schedule/get_system_tests.json?token=AuSnoCzp2uceDGA1JGqb
  def get_system_tests
    begin
      checkup_tests = System::Child::CheckupPlanner::Test.get_list(current_user,@api_version,params[:checkup_plan_id])
      status = StatusCode::Status200
      msg = Message::API[:success][:get_schedule_list]
      render :json=>{:system_tests=>checkup_tests, status:status, :message=>msg}
    rescue Exception=>e
      render :json=> {:error=> e.message, :checkup_schedules=>nil, :status=> StatusCode::Status100}
    end
  end


  # Api- add_schedule_to_schedule_manager
  # Post /api/v4/child/checkup_planner/checkup_schedule/add_test_to_system.json?token=gcjemgbLpVAjbjV4jyzs
  # params {:system_test=>{:title=>"",:description=>"",:country_code=>""}}
  def add_test_to_system
    begin
      unless params[:system_test][:title].blank?
        params[:system_test][:member_id] = current_user.id.to_s
        country = current_user.country_code rescue "uk"
        params[:system_test][:country_code] = params[:system_test][:country_code] || country
        params[:system_test][:added_by] = "user"

        custom_test = System::Child::CheckupPlanner::Test.add_custom_test(current_user,params,@api_version)
        checkup_tests = System::Child::CheckupPlanner::Test.get_list(current_user,@api_version, params[:checkup_plan_id])

        # make added element as first element
        checkup_tests.collect{|ct| custom_test = ct if ct.id == custom_test.id}
        checkup_tests.delete(custom_test)
        custom_test[:select_test] = true
        checkup_tests = checkup_tests.unshift(custom_test)

        status = StatusCode::Status200
        message = Message::API[:success][:add_test_to_system]
        render :json=>{:status=> status, :message=>message, :system_tests=>checkup_tests}
      else
        status = StatusCode::Status100
        message = Message::API[:error][:checkup_planner_test_title_missing]
        render :json=>{ :status=> status, :message=>message }
      end
    rescue Exception=>e
      status = StatusCode::Status100
      message = Message::API[:error][:add_test_to_system]
      render :json=>{:status=> status, :error=>e.message,:message=>message}
    end
  end

  # Api - add_schedule_for_member
  # Post /api/v4/child/checkup_planner/checkup_schedule/add_schedule_for_member.json?token=gcjemgbLpVAjbjV4jyzs 
  # params ={"checkup_plan=>{:member_id=>"57c0213af9a2f3c16400000a", :checkup_schedule_manager_ids=>["59019af2f9a2f323c0000001", "59019b07f9a2f323c0000002"]}}
  def add_checkup_plan 
    begin
      unless params[:checkup_plan][:title].blank? || params[:checkup_plan][:date].blank?
        member_id =  params[:checkup_plan][:member_id]
        status = StatusCode::Status200
        msg = Message::API[:success][:add_checkup_plan]
        # raise "Please add checkup date" if params[:checkup_plan][:date].blank?
        unless params[:checkup_plan][:status].blank?
          params[:checkup_plan][:status] = params[:checkup_plan][:status].humanize
        end
        Child::CheckupPlanner::Checkup.add_checkup_schedule(current_user,params)
        # schedules = Child::CheckupPlanner::Checkup.get_schedules(current_user,member_id)
        render :json=>{ status:status, :message=>msg}
      else
        msg = params[:checkup_plan][:title].blank? ? 
                                    Message::API[:error][:tite_checkup_plan_missing] :
                                    Message::API[:error][:date_checkup_plan_missing]
        render :json=>{:status=> StatusCode::Status100, :message=>msg}
      end
    rescue Exception=>e
      msg = Message::API[:error][:add_checkup_plan]
      render :json=>{:status=> StatusCode::Status100, :message=>msg, :error=>e.message}
    end
  end

  # Api - update_schedule_for_member
  # Put api/v4/child/checkup_planner/checkup_schedule/update_checkup_plan.json?token=gcjemgbLpVAjbjV4jyzs&member_id=57c0213af9a2f3c16400000a
  # params ={:checkup_plan =>{:status=> :member_id=>,:title=>,:description=>,:date=>,:end_time=>,end_time},checkup_plan_id=>""}
  def update_checkup_plan 
    begin
      unless params[:checkup_plan][:title].blank?
        checkup_schedule = Child::CheckupPlanner::Checkup.find(params[:checkup_plan_id])
        member_id =  checkup_schedule.member_id
        status = StatusCode::Status200
        msg = Message::API[:success][:update_checkup_plan]
        checkup_schedule.update_checkup_schedule(current_user,params)
        # schedules = Child::CheckupPlanner::Checkup.get_schedules(current_user,member_id)
      else
        status = StatusCode::Status100
        msg = Message::API[:error][:tite_checkup_plan_missing]
      end
      render :json=>{ status:status, :message=>msg}
    rescue Exception=>e
      msg = Message::API[:error][:update_checkup_plan]
      render :json=>{:status=> StatusCode::Status100,:message=> msg, :error=>e.message}
    end
  end
  

  #APi- delete_checkup_tests
  #Post api/v4/child/checkup_planner/checkup_schedule/delete_checkup_plan.json?token=gcjemgbLpVAjbjV4jyzs&member_id=57c0213af9a2f3c16400000a
  #params = {:checkup_system_test_ids=>[],checkup_plan_id}
  def delete_checkup_tests
     begin
      checkup_schedule = Child::CheckupPlanner::Checkup.where(id:params[:checkup_plan_id]).last
      status = StatusCode::Status200
      msg = Message::API[:success][:delete_checkup_tests]
      Child::CheckupPlanner::CheckupTest.where(checkup_id:checkup_schedule.id, :system_checkup_planner_test_id.in=>params[:checkup_system_test_ids]).delete_all
      render :json=>{ status:status, :message=>msg}
    rescue Exception=>e
      msg = Message::API[:error][:delete_checkup_tests]
      render :json=>{:status=> StatusCode::Status100,:message=> msg, :error=>e.message}
    end
  end

  #APi- delete_checkup_plan
  #Delete api/v4/child/checkup_planner/checkup_schedule/delete_checkup_plan.json?token=gcjemgbLpVAjbjV4jyzs&member_id=57c0213af9a2f3c16400000a
  #params = {:checkup_plan_id=>}
  def delete_checkup
     begin
      checkup_schedule = Child::CheckupPlanner::Checkup.where(id:params[:checkup_plan_id]).last
      status = StatusCode::Status200
      msg = Message::API[:success][:delete_checkup]
      checkup_schedule.delete_schedule(current_user)
      # schedules = Child::CheckupPlanner::Checkup.get_schedules(current_user,member_id)
      render :json=>{ status:status, :message=>msg}
    rescue Exception=>e
      msg = Message::API[:error][:delete_checkup]
      render :json=>{:status=> StatusCode::Status100,:message=> msg, :error=>e.message}
    end
  end
 
    
  def set_api_version
    @api_version = 5
  end

end
