class Api::V5::Child::VisionHealth::EventController < ApiBaseController
  before_filter :set_api_version
  
  # depricated
  def index
    begin
      member_id = params[:member_id]
      event_type = params[:event_type]
      if event_type == "schedule" || event_type == "skip"
        event = Child::VisionHealth::VisionTestEvent.new
        event.member_id = member_id
        event.event_type = event_type
        event.save
        status = StatusCode::Status200
        message = Message::API[:success][:vision_event_added]
        render :json=>{:message=>message,  
               :status=>status}
      else 
        render :json => {:status=>StatusCode::Status100, :message=>"Incorrect event_type. event_type can be schedule/skip"}
      end
    rescue Exception=>e
      render :json => {:status=>StatusCode::Status100, :message=>e.message}
    end
  end

  # api : create new event, returns event created and message
  # POST "/api/v5/child/vision_health/event/add_event.json?token=asdasd" 
  # Parameters: {"member_id"=>"541ebcb88cd03ddfc4000002", :event_type=>"schedule" }
  def add_event
    begin
      member_id = params[:member_id]
      member = Member.find(member_id)
      event_type = params[:event_type]
      if event_type == "schedule" || event_type == "skip"
        event = Child::VisionHealth::VisionTestEvent.new
        event.member_id = member_id
        event.event_type = event_type
        event.save
      else
        raise "invalid event"
      end
      if event_type == "schedule" && member.birth_date > Date.today - 3.years
        params[:event] = {:start_date=>(member.birth_date + 3.years).strftime("%d-%m-%Y")} 
        params[:event][:all_day] = 1
        params[:event][:start_time] = params[:event][:start_time] || "00:00"
        params[:event][:end_time] = params[:event][:end_time] ||  "23:55"
        params[:event][:member_id] = member.id
        params[:event][:end_date] = params[:event][:start_date] + " " + params[:event][:end_time] rescue params[:event][:start_date]
        params[:event][:start_date] = params[:event][:start_date] + " " + params[:event][:start_time] rescue params[:event][:start_date]

        params[:event][:start_time]= params[:event][:start_date]
        params[:event][:end_time] = params[:event][:end_date]
        params[:event][:tool_identifier] = "Vision Health"
        params[:event][:name] = "Take a colour vision test of #{member.first_name}"
        calendar_event = Event.new(params[:event])
        calendar_event.save!
      end
      message = Message::API[:success][:vision_event_added]
      status = StatusCode::Status200
     
    rescue Exception=>e 
      status = StatusCode::Status100
      calendar_event = nil
      @error =  e.message
    end
    render :json=>{:status=>status,:error=>@error,:event=>calendar_event}
  end

  def set_api_version
    @api_version = 5
  end

end