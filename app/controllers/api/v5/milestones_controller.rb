class Api::V5::MilestonesController < ApiBaseController
  before_filter :set_api_version

  require "base64"

  def index
    @member = Member.find(params[:member_id])
    #@recommended_pointer = ArticleRef.get_recommended_pointer(current_user,@member,"milestone")
    @member["age"] = ApplicationController.helpers.calculate_age(@member.birth_date) rescue ""
    @category = @member.find_category
    @child_male =  @member.male?
    @system_milestones_count = {}
    @system_milestones_type_count = {}
    country_code = @member.get_country_code
    options = {:country_code => country_code}
    # Get  dummy data if show subscription box true
    @tool, @show_subscription_box_status,@info_data,@member_data = @member.get_subscription_show_box_data(current_user,"Milestones",@api_version,options)
    
    all_data = SystemMilestone.in_country_code(options)
    all_data.group_by{|sm| sm.category}.each{|sm_group| @system_milestones_count[sm_group.first] = sm_group.last.count}

    @added_milestones_count = Milestone.added_milestones(@member)
    @percentage = calculate_percentage(@system_milestones_count,@added_milestones_count)
    respond_to do |format|
      # format.json { render :json => milestones.to_json(include: [:system_milestone,:pictures]) }
      format.json
    end 
  end

  # will deprecated in api version 6
  def member_milestone_list
    @member = Member.find(params[:member_id])
    #@recommended_pointer = [] #ArticleRef.get_recommended_pointer(current_user,@member,"milestone")
    @member["age"] = ApplicationController.helpers.calculate_age(@member.birth_date) rescue ""
    @category = @member.find_category
    @child_male =  @member.male?
    @system_milestones_count = {}
    @system_milestones_type_count = {}
    country_code = @member.get_country_code
    options = {:country_code => country_code}
    added_system_milestone_ids = Milestone.where(member_id:@member.id).pluck(:system_milestone_id)
    @milestone_sequence = SystemMilestone.in_country_code(options).where(category: @category).first.sequence

    @system_milestones_count = SystemMilestone.get_milestone_count_group_by_category(nil,options)
    @system_milestones_type_count = SystemMilestone.get_milestone_count_group_by_type(@milestone_sequence, nil,options )
    
    # added_system_milestone_ids = Milestone.where(member_id:@member.id).pluck(:system_milestone_id)
    milestones_accomplished_by_user = Milestone.achieved_milestones(@member).pluck(:system_milestone_id)
    
    @added_milestones_count = Milestone.added_milestones(@member,added_system_milestone_ids)
    @added_milestones_group_by_type_count = Milestone.added_milestones_group_by_type(@member,milestones_accomplished_by_user)
   
    estimated_system_milestone_ids = Milestone.estimated_milestones(@member).pluck(:system_milestone_id)
    achieved_milestone_ids = Milestone.achieved_milestones(@member).pluck(:system_milestone_id)
    
    @member_milestones_having_estamted_date_count = Milestone.added_milestones_group_by_type(@member,estimated_system_milestone_ids)
    
    @milestone_category_stats  = []
    milestone_progress_report = Milestone.get_progress_report_by_type(@member,added_system_milestone_ids)
    @added_milestones_group_by_type_count.each do |k,v|
      milestone_title_description = SystemMilestone::SystemMilestoneDescription[k.to_s]
     line2 = (@member_milestones_having_estamted_date_count[k] rescue 0).to_s + " Estimated" 
     #:added_milestone_count=>v,:total_milestone_count=>@system_milestones_type_count[k],:estimated_milestone_count=>@member_milestones_having_estamted_date_count[k]
     line3 = (milestone_progress_report[k.to_s]["delayed"] rescue 0).to_s + " Due"
     line1 = v.to_s + "/" + (@system_milestones_type_count[k]||0) .to_s
      @milestone_category_stats << {line1: line1, line2:line2, line3:line3, title:milestone_title_description[:title], description:milestone_title_description[:description] }
    end
    @achieved_milestones_count = Milestone.added_milestones(@member,achieved_milestone_ids)
    @estimated_milestone_count = Milestone.added_milestones(@member,estimated_system_milestone_ids)
    @estimated_percentage = calculate_percentage(@system_milestones_count,@estimated_milestone_count)
    @achieved_percentage = calculate_percentage(@system_milestones_count,@achieved_milestones_count)
    
    @tool, @show_subscription_box_status,@info_data, @member_data = @member.get_subscription_show_box_data(current_user,"Milestones",@api_version,options)
    respond_to do |format|
      format.json
    end 
  end
  
  def memberMilestoneList
    @member = Member.find(params[:member_id])
    @member["age"] = ApplicationController.helpers.calculate_age(@member.birth_date) rescue ""
    @category = @member.find_category
    @child_male =  @member.male?
    @system_milestones_count = {}
    @system_milestones_type_count = {}
    country_code = @member.get_country_code
    options = {:country_code => country_code}
    options[:important_milestones] = params[:important_milestones] 
    added_system_milestone_ids = Milestone.where(member_id:@member.id).pluck(:system_milestone_id)
    @milestone_sequence = SystemMilestone.in_country_code(options).where(category: @category).first.sequence

    milestones_accomplished_by_user = Milestone.achieved_milestones(@member).pluck(:system_milestone_id)
    
    @system_milestones_count = SystemMilestone.get_milestone_count_group_by_category(nil,options)
    @system_milestones_type_count = SystemMilestone.get_milestone_count_group_by_type(@milestone_sequence, nil,options )
    
    @added_milestones_count = Milestone.added_milestones(@member,added_system_milestone_ids)
    @added_milestones_group_by_type_count = Milestone.added_milestones_group_by_type(@member,milestones_accomplished_by_user)
   
    achieved_milestone_ids = Milestone.achieved_milestones(@member).pluck(:system_milestone_id)
    estimated_system_milestone_ids = Milestone.estimated_milestones(@member).pluck(:system_milestone_id)
    
    @member_milestones_having_estamted_date_count = Milestone.added_milestones_group_by_type(@member,estimated_system_milestone_ids)
    
    @milestone_category_stats  = []
    milestone_progress_report = Milestone.get_progress_report_by_type(@member,added_system_milestone_ids)
    @added_milestones_group_by_type_count.each do |k,v|
      milestone_title_description = SystemMilestone::SystemMilestoneDescription[k.to_s]
     line2 = (@member_milestones_having_estamted_date_count[k] rescue 0).to_s + " Estimated" 
     line3 = (milestone_progress_report[k.to_s]["delayed"] rescue 0).to_s + " Due"
     line1 = v.to_s + "/" + (@system_milestones_type_count[k]||0) .to_s
      @milestone_category_stats << {line1: line1, line2:line2, line3:line3, title:milestone_title_description[:title], description:milestone_title_description[:description] }
    end
    
    @achieved_milestones_count = Milestone.added_milestones(@member,achieved_milestone_ids)
    @estimated_milestone_count = Milestone.added_milestones(@member,estimated_system_milestone_ids)
    @estimated_percentage = calculate_percentage(@system_milestones_count,@estimated_milestone_count)
    @achieved_percentage = calculate_percentage(@system_milestones_count,@achieved_milestones_count)
    options[:get_data_for_from_list] = "version1"
    
    # Graph title text
    milestone_data = Milestone.member_milestone_graph_data(@member,current_user,options)
    percantage_achieved = milestone_data[:milestones][:achieved_percentage]  rescue 0
    percantage_achieved = percantage_achieved.to_i  rescue percantage_achieved
    @milestone_graph_title_text = "#{@member.first_name} has achieved #{percantage_achieved}% of the milestones expected by #{@member.pronoun} current age".humanize


    @tool, @show_subscription_box_status,@info_data, @member_data = @member.get_subscription_show_box_data(current_user,"Milestones",@api_version,options)
    respond_to do |format|
      format.json
    end 
  end

  # Get Milestone detail
  # Get /api/v1/milestones/get_milestone_detail.json?token=XrjzH88mP4syK1q4MTeb&milestone_id
  def get_milestone_detail
    begin
      @milestone = Milestone.find(params[:milestone_id])
      @milestone["message"] = "Milestone found"
    rescue
      message = "No milestone found"
      render json: {:message => message,:status=>100}
    end    
  end

  def milestone_detail
    begin
      @milestone = Milestone.find(params[:milestone_id])
      @milestone["message"] = "Milestone found"
    rescue
      message = "No milestone found"
      render json: {:message => message,:status=>100}
    end
  end

  # {milestone: {member_id: "", system_milestone_id: "", description: "", date: ""}}
  def create
    member = Member.find(params[:milestone][:member_id])
    params[:milestone][:milestone_status] = "accomplished"
    params[:milestone][:status_by]  = "user"
    milestone = Milestone.new(params[:milestone])
    respond_to do |format|
      unless member.already_having_milestone?(milestone)
        if milestone.save
          push_identifier = 4
          child = member
          msg = "Awesome! #{current_user.first_name} has updated 1 new milestone for #{child.first_name}. Click to check"
          current_user.push_user_action_notification(push_identifier,milestone,msg,child)

          if params[:media_files].present?
            params[:media_files].each do |pic|
              milestone.pictures << Picture.new(local_image: pic,upload_class: "milestone",member_id: member.id)
            end
          end  
          message = Message::API[:success][:milestone_create] rescue  "Selected milestone has been added to the accomplished list."
          milestone["message"] = message rescue ""
          milestone["status"] = StatusCode::Status200
          format.json { render :json => milestone.to_json(include: message) }
        end	    
      else
        already_having = true
        message = Message::API[:error][:milestone_create] rescue "You have already added this milestone."
        format.json { render :json => {already_having: true, message:message,status:StatusCode::Status100} }  
      end
    end
  end

  #{id: "", milestone: {member_id: "", system_milestone_id: "", description: "", date: ""}, delete_picture_ids: "123,456,789"}
  def update
  	@milestone = Milestone.where(id:params[:id]).last || Milestone.where(member_id:params[:milestone][:member_id],:system_milestone_id=>params[:milestone][:system_milestone_id]).last
    @member = @milestone.member
    (params[:upload_media] || []).select{|p| p.present?}.each do |pic|
      @milestone.pictures << Picture.new(local_image:pic,upload_class: "milestone",member_id:@member.id)
    end
    respond_to do |format|      
     # params[:milestone][:milestone_status] = "accomplished"
      params[:milestone][:status_by]  = "user"

      if @milestone.update_attributes(params[:milestone])
        #UserCommunication::UserActions.add_user_action(@milestone,"Milestones",current_user,"Update")

        if params[:delete_picture_ids].present? # To delete pics from api
          @delete_picture_ids = params[:delete_picture_ids].split(',')           
          @delete_picture_ids.each do |picture_id|
            if picture_id.include?('default_')
              @milestone.update_attribute(:display_default_image,false)
            else  
              picture = Picture.where(id: picture_id).first
              picture.destroy if picture
            end  
          end  
        end
        if params[:media_files].present?
          params[:media_files].each do |pic|
            @milestone.pictures << Picture.new(local_image:pic,upload_class: "milestone",member_id:@member.id)
          end
        end
        if @milestone.milestone_status != "accomplished"
          @milestone.delete_timeline
        end  
        @milestone["message"] = Message::API[:success][:milestone_update] rescue ""
        @milestone["status"] = StatusCode::Status200
        no_of_milestone_added = 1
        push_identifier = 4
        child = @member
        system_milestone = @milestone.system_milestone
        msg = "Awesome! #{current_user.first_name} has updated milestone \"#{system_milestone.title}\" for #{child.first_name}. Click to check"
        current_user.push_user_action_notification(push_identifier,@milestone,msg,child)

        format.json {render :json => @milestone.to_json(include: :pictures)}
  	  else
  	  	format.json {render :json => @milestone.errors}
  	  end
  	end
  end

  def add_multiple
    @member = Member.find(params[:member_id])
    added_milestones = []
    success = true
    if params[:milestones].present?
      params[:milestones].each do |milestone_data|        
        if SystemMilestone.where(id:milestone_data[:system_milestone_id]).present?
          @milestone = Milestone.find_or_initialize_by(member_id: @member.id, system_milestone_id: milestone_data[:system_milestone_id])
          @milestone.date = milestone_data[:date]
          @milestone.milestone_status = milestone_data[:milestone_status] || "accomplished"
          @milestone.status_by = "user"
          if !@milestone.save
            success = false
          else
            added_milestones << {:system_milestone_id=>@milestone.system_milestone_id,:milestone_id=>@milestone.id}
          end
        end
      end
      no_of_milestone_added = params[:milestones].count
      push_identifier = 4
      push_identifier = 4
      child = @member
      if @milestone.present?
        @milestone["new_id"]  = @milestone.id.to_s + rand(10000).to_s 
        if no_of_milestone_added > 1
          msg =   "Awesome! #{current_user.first_name} has updated #{no_of_milestone_added} new milestones for #{child.first_name}. Click to check"
        elsif no_of_milestone_added == 1
          msg =   "Awesome! #{current_user.first_name} has updated #{no_of_milestone_added} new milestone for #{child.first_name}. Click to check"
        end
        current_user.push_user_action_notification(push_identifier,@milestone,msg,child)
      end
    end      
    message = success ? Message::API[:success][:milestone_create] : Message::API[:error][:milestone_create]
    @member.milestones.where({'system_milestone_id' => {"$in" => params[:delete_milestone_ids].split(',')}}).destroy_all if params[:delete_milestone_ids].present?
    respond_to do |format|
      format.json {render json: {:status => (success ? StatusCode::Status200 : 'error'),message: message,:added_milestones=>added_milestones}}
    end  
  end


  def add_milestone_from_handhold
    @member = Member.find(params[:member_id])
    params[:delete_milestone_ids] = params[:delete_milestone_ids] || ""
    options = {:delete_milestone_ids=>params[:delete_milestone_ids].split(',')}
    success = SystemMilestone.save_handhold_data(@member,params[:milestones],options)
    # child_male_status = @member.male?
    # success = true
    # if params[:milestones].present?
    #   params[:milestones].each do |milestone_data|        
    #     @milestone = Milestone.find_or_initialize_by(member_id: @member.id, system_milestone_id: milestone_data[:system_milestone_id])
    #     @milestone.date = milestone_data[:date]
    #     @milestone.milestone_status =   "accomplished" 
    #     @milestone.child_male_status = child_male_status
    #     @milestone.status_by = "system"
    #     @milestone.child_male_status = child_male_status
    #     success = false unless @milestone.save
    #   end
    # end      
    message = success ? Message::API[:success][:milestone_create] : Message::API[:error][:milestone_create]
    # @member.milestones.where({'system_milestone_id' => {"$in" => params[:delete_milestone_ids].split(',')}}).destroy_all if params[:delete_milestone_ids].present?
    respond_to do |format|
      format.json {render json: {:status => (success ? StatusCode::Status200 : 'error'),message: message}}
    end  
  end



  
  def destroy
    @milestone = Milestone.where(id: params[:id]).first
    respond_to do |format|
      if @milestone
        if @milestone.destroy
          api_response = {status:StatusCode::Status200, deleted: true, message: Message::API[:success][:milestone_delete]}
        else
          api_response = {status:StatusCode::Status200, deleted: false, message: Message::API[:error][:milestone_delete]}
        end
        format.json {render :json => api_response}
      else
        format.json {render :json => {message: "Milestone is not present", status: :unprocessible_entity}}
      end      
    end  
  end

  def milestones_for_graph    
    member = Member.where(id: params[:id]).first
    @system_milestones_count = {}
    country_code = member.get_country_code
    options = {:country_code => country_code}
    SystemMilestone.in_country_code(options).group_by{|sm| sm.category}.each{|sm_group| @system_milestones_count[sm_group.first] = sm_group.last.count}
    @added_milestones_count = Milestone.added_milestones(member)
    @percentage = calculate_percentage(@system_milestones_count,@added_milestones_count)
  end

  def upload_image
    @milestone = Milestone.find(params[:id])
    decode_and_save_image(@milestone,params[:image_data])
  end  

  private

  # def added_milestones(member,milestone_ids=nil)
  #   # arr = member.milestones.group_by{|m| m.system_milestone.category}.map{|k,grp| {k => grp.count} }
  #   member_milestone_list = SystemMilestone.get_member_milestone_count_group_by_category(member.id,milestone_ids)
  #   data = Hash.new
  #   SystemMilestone::CATEGORIES_RANGE.values.map do |v|
  #     data[v] = member_milestone_list[v].to_i
  #   end
  #   data
  # end


  # def added_milestones_group_by_type(member,milestone_ids=[])
  #   if milestone_ids.present?
  #     member_milestone_list = SystemMilestone.get_member_milestone_count_group_by_type(member.id,milestone_ids)
  #   else
  #    member_milestone_list = {}
  #   end
  #   data = Hash.new
  #   country_code = member.get_country_code
  #   options = {:country_code=>country_code}
  #   SystemMilestone.in_country_code(options).distinct(:milestone_type).map do |v|
  #     data[v] = member_milestone_list[v].to_i
  #   end
  #   data
  # end


  # def get_progress_report_by_type(member,system_milestone_id=[])
  #   data = {}
  #   score = ((1/3.to_f) * GlobalSettings::MilestoneProgressCalculationFactors[:scale]).round(2)
  #   scale = GlobalSettings::MilestoneProgressCalculationFactors[:scale]
  #   data = SystemMilestone.get_member_milestone_group_by_type(member,system_milestone_id)
  #   data.each do |type,system_milestone_ids|
  #     stats = {"cannot_tell"=>0,"delayed"=>0}
  #     milestones = Milestone.where(member_id:member.id).in(system_milestone_id:system_milestone_ids)
  #     milestones.each do |milestone|
  #       system_milestone = milestone.system_milestone
  #       if milestone.milestone_status == "unaccomplished" && milestone.progress_score(system_milestone,member) < score
  #         stats["delayed"] = stats["delayed"] + 1
  #       end
  #     end
  #     data[type] = stats
  #   end
  #   data
  # end
    

  

  def calculate_percentage(system_milestones_count,added_milestones_count)
    Milestone.calculate_percentage(system_milestones_count,added_milestones_count)
    # percentage = {}
    # system_milestones_count.each do |k,v|
    #   begin
    #     percentage[k] =  ( (added_milestones_count[k].to_f/system_milestones_count[k]) *100  )
    #   rescue Exception=>e 
         
    #     percentage[k] = 0
    #   end
    # end  
    # percentage
  end
    
  def set_api_version
    @api_version = 5
  end
end

