class Api::V5::Clinic::MessageController < ApiBaseController
  before_filter :set_api_version

  # Get
  # /api/v5/clinic/message/list_messages.json?token=541ebcb88cd03ddfc4000002
  # params = {:token=>'541ebcb88cd03ddfc4000002',member_id=>'5b6d929a8cd03d63360003c7'} 
  # TPP 
  # params = {:token=>'541ebcb88cd03ddfc4000002',member_id=>'5b6d929a8cd03d63360003c7',:start_date=>(Date.today-7.days).to_time,:end_date=>(Date.today+1.day).to_time }  
  def list_messages
    begin
      options = {}
      member_id = params[:member_id]
      member = Member.find(member_id)
      options[:current_user] = current_user
      options[:start_date] = params[:start_date] || (Date.today - 1.year)
      options[:end_date] = params[:end_date] || (Date.today + 1.day)
      response = ::Clinic::Message.get_message_list(current_user,member,options)
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      @error = e.message
    end
    render :json=>response.merge(:reauthentication_required=>@reauthentication_required)
  end
  
  # Emis only
  # Get 
  # /api/v5/clinic/message/get_recipients.json?token=541ebcb88cd03ddfc4000002 
  # params = {:token=>'541ebcb88cd03ddfc4000002',member_id=>'5b6d929a8cd03d63360003c7'}  
  def get_recipients
    begin
      options = {}
      member_id = params[:member_id]
      member = Member.find(member_id)
      options[:current_user] = current_user
      response = ::Clinic::Message.get_message_receiptent_list(current_user,member,options)
      # response = ::Emis::Message.get_message_receiptent_list(current_user,member_id,options)
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      @error = e.message
    end
    render :json=>response.merge(:reauthentication_required=>@reauthentication_required)
  end


  # Get
  # /api/v5/clinic/message/get_message_detail.json?token=541ebcb88cd03ddfc4000002
  # params = {:message_id=>"", :token=>'541ebcb88cd03ddfc4000002',member_id=>'5b6d929a8cd03d63360003c7'}  
  def get_message_detail
    begin
      options = {}
      member_id = params[:member_id]
      message_id = params[:message_id]
      member = Member.find(member_id)
      options[:current_user] = current_user
      message_read_state =  "Read"
      update_message_response = ::Clinic::Message.update_message_read_status(current_user,member,message_id,message_read_state,options) rescue nil
     
      response = ::Clinic::Message.get_message_detail(current_user,member,message_id,options)
      
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      @error = e.message
    end
    render :json=>response.merge(:reauthentication_required=>@reauthentication_required)
  end

  # Delete
  # /api/v5/clinic/message/delete_message.json?token=541ebcb88cd03ddfc4000002
  # params = {:message_id=>"", :token=>'541ebcb88cd03ddfc4000002',:member_id=>'5b6d929a8cd03d63360003c7'}  
  def delete_message
    begin
      options = {}
      member_id = params[:member_id]
      message_id = params[:message_id]
      member = Member.find(member_id)
      options[:current_user] = current_user
      response = ::Clinic::Message.delete_message(current_user,member,message_id,options)
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      @error = e.message
    end
    render :json=>response.merge(:reauthentication_required=>@reauthentication_required)
  end
  
  # Put
  # /api/v5/clinic/message/update_message_read_status.json?token=541ebcb88cd03ddfc4000002
  # params = {:message_id=>"",  :token=>'541ebcb88cd03ddfc4000002',:member_id=>'5b6d929a8cd03d63360003c7'}  
  def update_message_read_status
    begin
      options = {}
      member_id = params[:member_id]
      message_id = params[:message_id]
      message_read_state = params[:message_read_state].titleize rescue "Read"
      member = Member.find(member_id) 
      options[:current_user] = current_user
      response = ::Clinic::Message.update_message_read_status(current_user,member,message_id,message_read_state,options)
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      @error = e.message
    end
    render :json=>response.merge(:reauthentication_required=>@reauthentication_required)
  end

  # post
  # /api/v5/clinic/message/send_message.json?token=541ebcb88cd03ddfc4000002
  # params = {:subject=>"",:message=>"", :message_id=>"", recipients=>[] :token=>'541ebcb88cd03ddfc4000002',:member_id=>'5b6d929a8cd03d63360003c7'}  
  def send_message
    begin
      options = {}
      member_id = params[:member_id]
      message_id = params[:message_id]
      subject = params[:subject]
      msg = params[:message]
      member = Member.find(member_id)
      options[:current_user] = current_user
      options[:recipients] = params[:recipients]
      response = ::Clinic::Message.send_message(current_user,member,subject,msg,options)
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      @error = e.message
    end
    render :json=>response.merge(:reauthentication_required=>@reauthentication_required)
  end
  
  def set_api_version
    @api_version = 5
  end

end
