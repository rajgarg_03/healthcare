class Api::V5::Clinic::BookAppointmentController < ApiBaseController
  before_filter :set_api_version

  # Get
  # /api/v5/clinic/book_appointment/list_user_appointment.json?token=541ebcb88cd03ddfc4000002
  # params = {:token=>'541ebcb88cd03ddfc4000002',member_id=>'5b6d929a8cd03d63360003c7'}  
  def list_user_appointment
    begin
      options = {}
      member_id = params[:member_id]
      options[:current_user] = current_user
      appointments,status,message = Clinic::BookAppointment.list_user_appointment(current_user,member_id,options)
      status = StatusCode::Status200
      @message = message
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      status = StatusCode::Status100
      appointments = []
      @error = e.message
    end
    render :json=>{:message=>@message, :reauthentication_required=>@reauthentication_required,:status=>status,:error=>@error,:appointments=>appointments}
  end
 
  # Get
  # /api/v5/clinic/book_appointment/available_appointments.json?token=541ebcb88cd03ddfc4000002
  #smrthi
  # params = {:token=>'541ebcb88cd03ddfc4000002',:member_id=>'5b6d929a8cd03d63360003c7',:doctor_details=>{:doctor_clinic_id=>"1",doctor_id=>"16"}}  
  # Emis
  # params = {:token=>'541ebcb88cd03ddfc4000002',:member_id=>'5b6d929a8cd03d63360003c7',:doctor_details=>{:doctor_clinic_id=>"3386"}  
  # doctor_details = {:location_id=> 4884,:clinician_id=>3386, :from_date_time=>(Time.now + 7.day), :to_date_time=>(Time.now + 8.day)
  # Tpp
  # params = {:token=>'541ebcb88cd03ddfc4000002',:member_id=>'5b6d929a8cd03d63360003c7',:doctor_details=>{:doctor_clinic_id=>"3386"}  
  # doctor_details = { :doctor_id=>"Elizabeth Bramley", :from_date_time=>(Time.now + 7.day), :to_date_time=>(Time.now + 8.day)}
  
  def available_appointments
    begin
      member_id = params[:member_id]
      member = Member.find(member_id)
      options = {:current_user=>current_user,:doctor_details=>doctor_details}
      organization_uid = member.organization_uid
      clinic = member.get_clinic_detail(current_user,options)
      doctor_details = params[:doctor_details] #|| {:doctor_clinic_id=>17, :doctor_id=>16}
      options = {:current_user=>current_user,:doctor_details=>doctor_details}

      if clinic.is_emis?(options) ||clinic.is_tpp?(options) #["emis","gpsoc","tpp"].include?(clinic.clinic_type.downcase)
        doctor_details = doctor_details || {}
        options[:request_data] = {:LocationId=>doctor_details[:location_id],:ClinicianId=> doctor_details[:doctor_id] , :FromDateTime=>doctor_details[:from_date_time],:ToDateTime=>doctor_details[:to_date_time] }
      end
      
      status,available_appointments = Clinic::BookAppointment.get_available_appointments(organization_uid,member_id,options)
      doctor_list = []
        
      if clinic.is_emis?(options) ||clinic.is_tpp?(options)
        doctor_list = available_appointments[:doctor_list]
        # clinicain_id = params[:doctor_details][:doctor_clinic_id]
        slots_count_with_appointment_date = available_appointments[:slots_data].map {|appointment_date,slots| {appointment_date=>slots.count} }.inject(:merge)
        available_appointments = available_appointments[:slots_data]
        sorted_available_appointments = available_appointments.keys
      else
        slots_count_with_appointment_date = available_appointments.map {|appointment_date,slots| {appointment_date=>slots.map{|a| a[:appointment_count]}.map(&:to_i).sum }}.inject(:merge)

      end
      raise 'No appointment available' if status != 200
       
      status = StatusCode::Status200
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      status = StatusCode::Status100
      @error = e.message
      sorted_available_appointments = []
      slots_count_with_appointment_date = {}
      available_appointments = {}
    end
    render :json=>{:reauthentication_required=>@reauthentication_required,:doctor_list=>doctor_list,:slots_count_with_appointment_date=>slots_count_with_appointment_date,:sorted_available_appointments=>sorted_available_appointments, :status=>status,:error=>@error,:available_appointments=>available_appointments}
  end

  # Get
  # /api/v5/clinic/book_appointment/get_doctor_list.json?token=541ebcb88cd03ddfc4000002
  # params = {:token=>'541ebcb88cd03ddfc4000002',:member_id=>'5b6d929a8cd03d63360003c7'} 
  # TPP
  # params = {:token=>'541ebcb88cd03ddfc4000002',:member_id=>'5b6d929a8cd03d63360003c7',:start_date=>(Date.today),:number_of_days=>15} 
  def get_doctor_list
    begin
      member_id = params[:member_id]
      member = Member.find(member_id)
      organization_uid = member.organization_uid
      options = {:current_user=>current_user,:start_date=>params[:start_date],:number_of_days=>params[:number_of_days]} 

      doctor_list_for_appointment = Clinic::BookAppointment.get_doctor_list(organization_uid,member_id,options)
      status = StatusCode::Status200
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      status = StatusCode::Status100
      @error = e.message
      doctor_list_for_appointment = []
       
    end
    render :json=>{:reauthentication_required=>@reauthentication_required,:status=>status,:error=>@error,:doctor_list_for_appointment=>doctor_list_for_appointment}
  end

  # Get
  # /api/v5/clinic/book_appointment/get_appointment_purpose_list.json?token=541ebcb88cd03ddfc4000002
  # params = {:token=>'541ebcb88cd03ddfc4000002',:member_id=>'5b6d929a8cd03d63360003c7'}  
  def get_appointment_purpose_list
    begin
      member_id = params[:member_id]
      member = Member.find(member_id)
      options = {:current_user=>current_user}
      purpose_list_for_appointment = Clinic::BookAppointment.get_appointment_purpose_list(member,options)
      status = StatusCode::Status200
      @reauthentication_required = current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      status = StatusCode::Status100
      @error = e.message
      doctor_list_for_appointment = []
       
    end
    render :json=>{:reauthentication_required=>@reauthentication_required,:status=>status,:error=>@error,:purpose_list_for_appointment=>purpose_list_for_appointment}
  end




  # Post
  # /api/v5/clinic/book_appointment/create_appointment.json?token=541ebcb88cd03ddfc4000002
  # params = {:token=>'541ebcb88cd03ddfc4000002',:member_id=>'5b6d929a8cd03d63360003c7', :appointment_details=>{} }  
  # smrthi
  # :appointment_details=>{doctor_code=>'SUB',:start_date=> '10-07-2019',:start_time=>'9:00',:end_time=>'10:00',:appointment_purpose=>'consulatation'} }  
  # EMIS
  # :appointment_details=>{:appointment_id=>'YXRrPTc1NTJhZTgxYWRmOWQz…mYyMzB8c2xvdD0xMzI4MjE=',:appointment_purpose=>'consulatation'} }  
  # TPP
  # :appointment_details=>{:appointment_id=>'YXRrPTc1NTJhZTgxYWRmOWQz…mYyMzB8c2xvdD0xMzI4MjE=',:appointment_purpose=>'consulatation',:start_date=>"10-07-2019", :start_time=>'09:00',:end_time=>'09:15'} }  
  def create_appointment
    begin
      member = Member.find(params[:member_id])
      member_id = member.id
      organization_uid = member.organization_uid
      appointment_details = params[:appointment_details]
      if appointment_details[:start_date].present?
        appointment_details[:start_time] = "#{appointment_details[:start_date]} #{appointment_details[:start_time]}"
        appointment_details[:end_time] = "#{appointment_details[:start_date]} #{appointment_details[:end_time]}"
        appointment_details[:start_time] = Time.zone.parse(appointment_details[:start_time])
        appointment_details[:end_time] = Time.zone.parse(appointment_details[:end_time])#.to_time.in_time_zone
      end
      options = {:current_user=>current_user}
      appointment,booking_status = ::Clinic::BookAppointment.save_appointment(organization_uid,member_id,appointment_details,options)
      appointment["_id"] = appointment["appointment_id"]
      if booking_status
        status = StatusCode::Status200
        message = appointment[:message] || "Appointment booked successfully"
      else
        status = StatusCode::Status100
        message = appointment[:message] || "Unable to book appointment for now. Try again later"
      end
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
    rescue Exception => e
      status = StatusCode::Status100
      message = "Unable to book appointment for now. Try again later"
    end
    render :json=>{:reauthentication_required=>@reauthentication_required,:status=>status,:error=>@error,:appointment=>appointment,:message=>message,:booking_status=>booking_status}
  end
  

  # Delete
  # /api/v5/clinic/book_appointment/cancel_appointment.json?token=541ebcb88cd03ddfc4000002
  # params = {:token=>'541ebcb88cd03ddfc4000002,:member_id=>"5cac82d58cd03dc976000dbb",:appointment_id=>'YXRrPWFhNGE4MmQ5ZjJkOTk1Yzg2YjRmZjNlZjFlODY1MDAxMDNhMDA4NWZ8c2xvdD0yNDU5NQ==',:cancellation_reason=>"no reason"}  
  def cancel_appointment
   begin
      member_id = params[:member_id]
      member = Member.find(member_id)
      appointment = ::Clinic::BookAppointment.new(member_id:member_id, appointment_id:params[:appointment_id],organization_uid:member.organization_uid)
      cancellation_reason = params[:cancellation_reason] || "No reason specified"
      options = {:current_user=>current_user}
      cancel_appointment_status = appointment.cancel_appointment(cancellation_reason,options)
      raise 'failed to cancel appointment' if !cancel_appointment_status
      status = StatusCode::Status200
      message = "Appointment cancelled successfully"
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
   rescue Exception => e
      status = StatusCode::Status100
      @error = e.message
      message = "Unable to cancel appointment. Try again later"
   end
    render :json=>{:reauthentication_required=>@reauthentication_required,:status=>status,:error=>@error,:message=>message}
  end
  
  # Put
  # /api/v5/clinic/book_appointment/update_appointment.json?token=541ebcb88cd03ddfc4000002
  # params = {:token=>'541ebcb88cd03ddfc4000002',:appointment_id=>'5d1ef94fcb51a2ffda000002',:appointment_details=>{appointment_purpose=>'consultation',:start_date=> '10-07-2019',:start_time=>'9:00',:end_time=>'10:00'}  }

  def update_appointment
    begin
      appointment = ::Clinic::BookAppointment.find(params[:appointment_id])
      member_id = appointment.member_id
      appointment_details = params[:appointment_details] 
      appointment_details[:start_time] = "#{appointment_details[:start_date]} #{appointment_details[:start_time]}"
      appointment_details[:end_time] = "#{appointment_details[:start_date]} #{appointment_details[:end_time]}"
    

      appointment_details[:start_time] = Time.zone.parse(appointment_details[:start_time])#.to_time.in_time_zone
      options = {:current_user=>current_user}
      appointment_details[:end_time] = Time.zone.parse(appointment_details[:end_time])#.to_time.in_time_zone
      appointment,status = appointment.update_appointment(appointment_details,options)
      raise 'failed to reschdule appointment' if !status
      status = StatusCode::Status200
      message = "Appointment updated successfully"
      @reauthentication_required= current_user.reauthentication_required?(current_user,member_id,options)
   rescue Exception => e
      status = StatusCode::Status100
      @error = e.message
      message = "Unable to update appointment. Try again later"
   end
    render :json=>{:reauthentication_required=>@reauthentication_required,:status=>status,:error=>@error,:message=>message,:appointment=>appointment}
    
  end

  
  def set_api_version
    @api_version = 5
  end

end
