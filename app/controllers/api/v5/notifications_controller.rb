class Api::V5::NotificationsController < ApiBaseController
  before_filter :set_api_version
  def index   
    if params[:page] && params[:page] == 'all'      
      @notifications = current_user.notifications.order_by("created_at DESC")
    else  
      @notifications = current_user.notifications.order_by("created_at DESC").paginate(:page => (params[:page]), :per_page => 10)
    end
    respond_to do |format|
      format.json #{render json: @notifications}
    end 
  end

  def unread_count
    unread_notifications_count = current_user.notifications.unread.count
    unread_article_refs_count = current_user.all_article_refs.unread.count

    respond_to do |format|
      format.json {render json: {status:StatusCode::Status200,unread_notifications_count: unread_notifications_count, unread_article_refs_count: unread_article_refs_count}}
    end 
  end 

  def mark_read
    if params[:ids].present?
      if params[:ids] == 'all'
        current_user.notifications.unread.update_all(checked: true)
      else
        current_user.notifications.where({'_id' => { "$in" => params[:ids].split(',')}}).update_all(checked: true)
      end
      respond_to do |format|
        format.json {render json: {status:StatusCode::Status200, unread_notifications_count: current_user.notifications.unread.count, total_count: current_user.notifications.count}}
      end   
    end 
  end
  
  # /api/v5/notifications/mark_user_notification_responded
  # Params = {:platform=>"ios/android",:notification_id=>'',:deeplink_data=>{},:notification_type=>'',:token=>''}
  # Type Put
  def mark_user_notification_responded
    begin
      data = params
      data[:response_type] = "push"
      status = StatusCode::Status200
      UserNotification::ClientResponse.save_client_response(current_user,data)
    rescue Exception=>e 
      status = StatusCode::Status100
      @error = e.message
    end
    render :json=>{:status=>status,:error=>@error}
  end 


  


  # /api/v5/notifications/log_pixel_event
  # Params = {:event_name=>"ios/android",:info=>{},:token=>''}
  # Type Put
  
  def log_event
    params = {:event_name=>"alexa_event_test_dev",:info=>{:member_id=>ChildMember.last.id}}
    @info = params[:info] || {}
    @event_name = params[:event_name]
    member_id = @info[:member_id]
    family_id = (@info[:family_id] || FamilyMember.where(member_id:member_id)) rescue nil
    report_event = Report::Event.where(:name=>/#{Regexp.escape(@event_name)}/i).last rescue nil
    
    if  report_event.present?
      ::Report::ApiLog.log_event(current_user.user_id,family_id,report_event.custom_id,options) rescue nil
    end
  end  
  
  def set_api_version
    @api_version = 5
  end
end   