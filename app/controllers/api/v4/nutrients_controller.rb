class Api::V4::NutrientsController < ApiBaseController
  before_filter :set_api_version
  DISPLAY_SETTING_NAME = {timeline: "timeline", calendar: "calendar", immunisations: "immunisation", documents: "documents", measurements: "health", milestone: "milestones"}

 #api Get
  #http://localhost:3000/api/v3/nutrients.json?token=member_id=
  def index
    begin
      member = Member.find(params[:member_id])
      api_version = @api_version
      member_tools = Nutrient.get_member_tools(member,current_user,api_version).entries
      member_tools.map do |n| 
        n[:identifier] = n.get_identifier(member,{:api_version=>api_version})
        n[:title] = n.title
        n[:launch_status] = (n.launch_status == "soft_launch" ? "launched": n.launch_status)
      end
      render json: member_tools
    rescue Exception=>e
      render json: {error:e.message,status: 100}
    end
  end

 # GET "http://localhost:3000/api/v3/nutrients/546128110a9a999a09000005/acitvate.json?token=XrjzH88mP4syK1q4MTeb&member_id=546128200a9a99178300000d"
  def acitvate
   begin
      api_version = @api_version
      tool = Nutrient.find(params[:id])
      member_nutrient = tool.activate_tool(api_version,current_user,params)
    rescue Exception=>e
      member_nutrient = {'error'=>e.message}
      member_nutrient['message'] = Message::API[:error][:acitvate_nutrient]
      member_nutrient['status'] = StatusCode::Status100
    end
    respond_to do |format|
      format.json {render json: member_nutrient}
    end
  end

  # DELETE "http://localhost:3000/api/v1/nutrients/546128110a9a999a09000005/deactivate.json?token=XrjzH88mP4syK1q4MTeb&member_id=546128200a9a99178300000d"
  def deactivate
    tool = Nutrient.find(params[:id])
    @member_nutrient = tool.deactivate_tool(@api_version,current_user,params)
    message = Message::API[:success][:deacitvate_nutrient]
    status = StatusCode::Status200
    respond_to do |format|
      format.json {render json: {message: message, status: status, deactivated: (@member_nutrient.persisted? ? false : true)}}
    end
  end
  
  # API-25:Add category tags in Tools
  #changed name change_category to categories
  # GET http://localhost:3000/api/v2/nutrients/categories.json?token=XrjzH88mP4syK1q4MTeb&member_id=546128200a9a99178300000d&category=Health
  def change_categories
    api_version = @api_version
    @member = Member.find(params[:member_id])
    @member_nutrients = Nutrient.get_member_tools(@member,current_user,api_version).entries
    if params[:category].present? &&  params[:category].downcase == "all" || params[:category].blank?
      @recommended_nutrients = @member.get_nutrients(api_version,current_user,{:show_feature_type=>"tools"})
    else
      @recommended_nutrients = @member.get_nutrients(api_version,current_user,{:show_feature_type=>"tools"}).select{|n| n.categories.any?{|cat| cat==params[:category]}}
    end
    @is_member_expected = @member.is_expected?
    @tools = @recommended_nutrients.map(&:categories).flatten.uniq - ["Mind", "Education","Other"]
    respond_to do |format|
      format.json
    end 
  end

  # API-25:Add category tags in Tools
  #changed name change_category to categories
  # GET http://localhost:3000/api/v3/nutrients/categories.json?token=XrjzH88mP4syK1q4MTeb&member_id=546128200a9a99178300000d&category=Health
  def categories
    api_version = @api_version
    @member = Member.find(params[:member_id])
    @member_nutrients = Nutrient.get_member_tools(@member,current_user,api_version).entries
    if params[:category].present? &&  params[:category].downcase == "all" || params[:category].blank?
      @recommended_nutrients = @member.get_nutrients(api_version,current_user,{:show_feature_type=>"tools"})
    else
      @recommended_nutrients = @member.get_nutrients(api_version,current_user,{:show_feature_type=>"tools"}).select{|n| n.categories.any?{|cat| cat==params[:category]}}
    end
    @is_member_expected = @member.is_expected?
    @tools = @recommended_nutrients.map(&:categories).flatten.uniq - ["Mind", "Education","Other"]
    respond_to do |format|
      format.json
    end 
  end


  def nutrients_status
    member = Member.where(id: params[:id]).first
    nutrients_status = {}
    api_version = @api_version
    member_tools = Nutrient.get_member_tools(member,current_user,api_version)
    member_tools.each do |member_nutrient|
      member_setting = member.member_settings.where(name:"display_#{member_nutrient.nutrient.title.downcase}_info").first 
      status = (member_setting && member_setting.status == 'false') ? false : true
      # status false means, tool is visited
      nutrients_status[member_nutrient.nutrient.title] = status
    end
    respond_to do |format|
      format.json {render json: nutrients_status}
    end  
  end  

  def update_nutrient_status
    member = Member.where(id: params[:member_id]).first
    nutrient = Nutrient.where(id: params[:id]).first
    info_popup_status(member,DISPLAY_SETTING_NAME[nutrient.title.downcase.to_sym])
    respond_to do |format|
      format.json {render json: {head: :ok, :status => StatusCode::Status200}}
    end
  end  
  
  private
  def set_api_version
    @api_version = 4
  end

end