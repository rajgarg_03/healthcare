 class Api::V4::SystemMilestonesController < ApiBaseController
  
  def index
  	@member = Member.where(id: params[:member_id]).first
    @category = @member.find_category
    country_code = @member.get_country_code
    options = {:country_code=> country_code}
    @system_milestones = SystemMilestone.in_country_code(options)
    respond_to do |format|
      # format.json { render :json => @system_milestones.group_by(&:category).to_json(methods: [:added]) }
      format.json
    end 
  end

  def hh_milestones
    begin
      @member = Member.find(params[:id]) #rescue ChildMember.last
      @system_milestones = HandHold.child_hh_milestones(@member)

      status = StatusCode::Status200
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
      @system_milestones = {}
    end
    render :json=>{ :status=> status, :error=>@error, :system_milestones=>@system_milestones[:system_milestones]}
  end
   
end