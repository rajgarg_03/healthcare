class Api::V4::HealthController < ApplicationController
  before_filter :set_api_version
    
  #api Get
  # api /api/v4/families/Nurt/members/57c0213af9a2f3c16400000a/health.json
  def index
    round_off_val = (params[:decimalroundoff]|| 1).to_i
    @member = Member.find(params[:member_id])
    @health_records = @member.health_records.order_by("updated_at desc")
    get_measurement_units(current_user)
    @last_health_record = @member.health_records.order_by("last_updated_at desc").first.convert_to_unit(@measurement_units,round_off_val) rescue nil
    @recently_updated_record = @health_records.first
    values = ["", nil]
    zscore_tool_id = ::Child::Zscore.get_tool_id
    zscore_tool = Nutrient.find(zscore_tool_id) rescue Nutrient.new
    feature_type = "all"
    tool_for = "child"
    if @health_records.present?
      is_zscore_flag = Nutrient.is_tool_available?(zscore_tool_id,current_user,@api_version,tool_for,feature_type)
    else
      is_zscore_flag = false
    end
    get_last_measurement_record(@member,round_off_val) 
    
    recommended_pointer = ArticleRef.get_recommended_pointer(current_user,@member,"health")
    
    respond_to do |format| 
      format.json { render :json=> {:zscore_feature_type=>zscore_tool.feature_type ,:zscore_tool_id=> zscore_tool_id, recommended_pointers:recommended_pointer, 
        :member=> @member,
        :show_z_score_flag => is_zscore_flag,
        :health_record =>add_diff(@health_records.entries,@measurement_units,round_off_val,@member,is_zscore_flag) , 
        :unit_selected=>{:lenght=> @measurement_units["length_unit"],
        :weight=> @measurement_units["weight_unit"]},
        :last_updated_record => 
        {:bmi => @last_bmi_record,
        :head_circum => @last_head_circum_record ,:height=>@last_height_record ,
        :weight => @last_weight_record, :waist=>@last_waist_circum_record },
        :last_record=>@last_health_record,:recently_updated_record=>(@recently_updated_record.convert_to_unit(@measurement_units,round_off_val) rescue nil), :status=> StatusCode::Status200}  }
    end
  end

  # GET /health/get_zscore_detail.json?token=iBSqRyQ5pxuxVb1dSz47&member_id=12
  # http://localhost:3000/api/v4/health/get_zscore_detail.json?token=AuSnoCzp2uceDGA1JGqb&member_id=595a6f8bf9a2f346c3000008
  def get_zscore_detail
    round_off_val = (params[:decimalroundoff]|| 2).to_i
    @member = Member.find(params[:member_id])
    health_unit = Health.health_unit(current_user.metric_system)
    who_chart_data = {"height"=>{},"weight"=>{},"bmi"=>{} }
    member_chart_data = {}
    zscore_data = []
    recommended_pointer = ArticleRef.get_recommended_pointer(current_user,@member,"health",{:list_all=>true})
    
    family_id = FamilyMember.where(member_id:@member.id).last.family_id rescue nil
    subscription_manager  = SubscriptionManager.premium_subscriptions.valid_subscription_manager(current_user,api_version=4).first
    subscription_detail = {plan_type:subscription_manager.plan_type, 
      title:subscription_manager.title,
      duration:subscription_manager.plan_duration,
      ios_product_id:subscription_manager.ios_product_id,
      android_product_id:subscription_manager.android_product_id
    } rescue {}
     
    zscore_tool_id = ::Child::Zscore.get_tool_id
    tool_for = "child"
    feature_type = "all"
    zscore_tool = Nutrient.find(zscore_tool_id) rescue Nutrient.new
    options = {:get_data_for_from_list=>"all"}
    @tool, @show_subscription_box_status,@info_data,sample_data = @member.get_subscription_show_box_data(current_user,zscore_tool.identifier,api_version,options)
           
  
    current_measurement = @member.health_records.order_by("updated_at desc").limit(1).first || Health.new rescue Health.last
    if current_measurement.present?
      zscore_enable_status = Nutrient.is_tool_available?(zscore_tool_id,current_user,@api_version,tool_for,feature_type)
    else
      zscore_enable_status = false
    end
    ["height","weight","bmi"].each do |measurement_parameter|
    
      health_unit = Health.health_unit(current_user.metric_system)
      get_measurement_units(current_user)
      chart_unit = health_unit[measurement_parameter.to_s] 
      data = get_who_data(measurement_parameter,@member.member_gender).entries
      who_data= {}
      age = get_month(Date.today) + 12
      
      who_chart_data[measurement_parameter] ["zscore_-3"] = data.map(&"#{measurement_parameter}_for_zscore_minus_3".to_sym)[0..age] rescue []
      who_chart_data[measurement_parameter]["zscore_-2"] = data.map(&"#{measurement_parameter}_for_zscore_minus_2".to_sym)[0..age] rescue []
      who_chart_data[measurement_parameter]["zscore_-1"] = data.map(&"#{measurement_parameter}_for_zscore_minus_1".to_sym)[0..age] rescue []
      who_chart_data[measurement_parameter]["zscore_0"] = data.map(&"#{measurement_parameter}_for_zscore_0".to_sym)[0..age] rescue []
      who_chart_data[measurement_parameter]["zscore_1"] = (data.map(&"#{measurement_parameter}_for_zscore_1".to_sym)[0..age])  rescue []
      who_chart_data[measurement_parameter]["zscore_2"] = (data.map(&"#{measurement_parameter}_for_zscore_2".to_sym)[0..age]) rescue []
      who_chart_data[measurement_parameter]["zscore_3"] = (data.map(&"#{measurement_parameter}_for_zscore_3".to_sym)[0..age])  rescue []
      who_chart_data[measurement_parameter][:month] = data.map(&:month)[0..age]
      if zscore_enable_status
        member_chart_data[measurement_parameter] = Health.member_health_records(@member,measurement_parameter).order_by("updated_at desc").collect{|record_obj| [get_month_in_float(record_obj.updated_at.to_date),api_convert_to_unit(record_obj[measurement_parameter],chart_unit,round_off_val)]}
      end
    end

    update_due_status = Health.is_update_due?(@member,current_user)
    z_score =  ::Child::Zscore.get_calculated_zscore( @member.id,!zscore_enable_status)
    
    z_score_in_text = ::Child::Zscore.zscore_to_text(z_score)
    z_score_analysis = ::Child::Zscore.zscore_analysis(z_score,@member)
    zscore_chart_min_max_detail = ::Child::Zscore.chart_min_max_data(who_chart_data,member_chart_data.to_h)
    respond_to do |format| 
      if @show_subscription_box_status == false
        format.json { render :json=> {  
          :who_height_chart_data=> who_chart_data["height"],
          :height_min_max_axis=>zscore_chart_min_max_detail["height_min_max_axis"],
          :weight_min_max_axis=>zscore_chart_min_max_detail["weight_min_max_axis"],
          :bmi_min_max_axis=>zscore_chart_min_max_detail["bmi_min_max_axis"],
          :member_chart_data => member_chart_data,
          :who_weight_chart_data=> who_chart_data["weight"],
          :subscription=> subscription_detail,
          :updated_at => (current_measurement.created_at rescue Date.today),
          :is_update_due=> update_due_status ,
          :who_bmi_chart_data=> who_chart_data["bmi"],
          :z_score_analysis=>z_score_analysis,
          :z_score => z_score_in_text,
          :z_score_values=>z_score,
          :child_pointers=> recommended_pointer,
          :zscore_feature_type => zscore_tool.feature_type,
          :system_tool_id=> @tool.id,
          :show_subscription_box=> @show_subscription_box_status,
          :status=> StatusCode::Status200}  }
      else
        format.json { render :json=> {  
        :who_height_chart_data=> who_chart_data["height"],
        :height_min_max_axis=>zscore_chart_min_max_detail["height_min_max_axis"],
        :weight_min_max_axis=>zscore_chart_min_max_detail["weight_min_max_axis"],
        :bmi_min_max_axis=>zscore_chart_min_max_detail["bmi_min_max_axis"],
        :member_chart_data => member_chart_data,
        :system_tool_id=> @tool.id,
        :show_subscription_box=> @show_subscription_box_status,
        :who_weight_chart_data=> who_chart_data["weight"]}.merge(sample_data)  }
      end
    end
  end


  def get_detail
    @member = Member.find(params[:member_id])
    round_off_val = (params[:decimalroundoff] || 1).to_i
    health_param = params[:data_type]
    @chart_param = health_param.camelcase
    current_member = current_user
    @health_unit = Health.health_unit(current_member.metric_system)
    get_measurement_units(current_member)
    @chart_unit = @health_unit[health_param.to_s] #Health::HEALTH_UNIT[current_user.metric_system][health_param.to_s]
    @health_records =   Health.member_health_records(@member,params[:data_type]).order_by("updated_at desc")
    family_id = nil #FamilyMember.where(member_id:@member.id).last.family_id rescue nil
    zscore_tool_id = ::Child::Zscore.get_tool_id
    zscore_tool = Nutrient.find(zscore_tool_id) rescue Nutrient.new
    tool_for = "child"
    feature_type = "all"
    if @health_records.present?
      zscore_flag = Nutrient.is_tool_available?(zscore_tool_id,current_user,@api_version,tool_for,feature_type)
    else
      zscore_flag = false
    end
    unless params[:data_type] == "all"
      @chart_update = Health.member_health_records(@member,params[:data_type]).order_by("updated_at desc").collect{|rec| [get_month_in_float(rec.updated_at.to_date),api_convert_to_unit(rec[health_param],@chart_unit,round_off_val)]}
      @last_health_record = @member.health_records.order_by("last_updated_at desc").first
      data = get_who_data(params[:data_type],@member.member_gender) #WhoHeight.where(:gender=> "1").order_by('month ASC')
      @who_data= {}
      age = get_month(Date.today) + 12
      data = data.entries
      @who_data[:p3] = data.map(&:P3)[0..age]
      @who_data[:p15] = data.map(&:P15)[0..age]
      @who_data[:p85] = data.map(&:P85)[0..age]
      @who_data[:p97] = data.map(&:P97)[0..age]
      @who_data[:month] = data.map(&:month)[0..age]
    end
    @recently_updated_record = @health_records.first
    respond_to do |format| 
      
      format.json { render :json=> {:zscore_feature_type=>zscore_tool.feature_type, :zscore_tool_id=> zscore_tool_id, :show_z_score_flag => zscore_flag,:member=> @member,:health_record => add_diff(@health_records.entries,@measurement_units,round_off_val,@member,zscore_flag), 
        :unit_selected=>{:lenght=>(@measurement_units["length_unit"] || "cms"),
        :weight=> (@measurement_units["weight_unit"] || "kgs")},
        :recently_updated_record => (@recently_updated_record.api_convert_to_unit(@measurement_units,round_off_val) rescue nil),
        :last_record=>(@last_health_record),
        :last_updated_record => (@last_health_record.api_convert_to_unit(@measurement_units,round_off_val) rescue nil),:chart_param=>params[:data_type],:chart_data=>@chart_update,
        :chart_min_max_axis=> Health.chart_min_max_axis(@chart_update,@who_data[:p97],@who_data[:p3],@who_data[:month]) ,:who_data =>{:p3=>@who_data[:p3], :p15=>@who_data[:p15],:p85=>@who_data[:p85], :p97=>@who_data[:p97],:month=>@who_data[:month] },
        :status=> StatusCode::Status200}  }
    end
  end
   
  # api get healt record detail
  # GET /health/get_health_detail.json?token=iBSqRyQ5pxuxVb1dSz47&measurement_id=12
  def get_health_detail
    begin
       health = Health.find(params[:measurement_id])
      get_measurement_units(current_user)
      health.api_converted_data(@measurement_units)
      message = Message::API[:success][:get_health_detail]
      status = StatusCode::Status200
    rescue
      health = nil
      status = StatusCode::Status100
      message = Message::API[:error][:get_health_detail] #{}"No health found"
    end
    render json: {:status=> status, :message => message, :health=>health, :metric_system => @measurement_units}
  end

  def create_record
    begin
      # check if any parameter for measurment is updated
      if params[:health].length > 2
        member_id = params[:health][:member_id] #=>"54412e225acefebacd00000a
        @health_record = Health.create_measurement_record(current_user,params)
        message = Message::GLOBAL[:success][:health_create_record] #"Your record has been updated."
        status = StatusCode::Status200
      end
    rescue Exception=>e
      message = Message::GLOBAL[:error][:health_create_record] #"Your record has been updated."
      status = StatusCode::Status100
      @error = e.message
    end
    render :json=> {:health=>@health_record,:message=>message, :status=> status,:error=>@error}
  end


  def create_record_from_alexa
    user_metric_system = current_user.metric_system
    params["health"] = eval(params["health"]) rescue params["health"]
    params["health"] = params["health"].with_indifferent_access
  
    length_unit_hash = {"centimeter"=>"cms", "centimeters"=>"cms","cms"=>"cms","cm"=>"cms","inches"=>"inches","inch"=>"inches"} 
    weight_unit_hash = {"kgs"=>"kgs", "kg"=>"kgs","kilogram"=>"kgs","kilograms"=>"kgs","lbs"=>"lbs", "pound"=>"lbs","pounds"=>"lbs"} 
  
    params[:length_unit] = length_unit_hash[params[:length_unit].downcase] rescue nil
    params[:weight_unit] = weight_unit_hash[params[:weight_unit].downcase] rescue nil
    
    if params[:length_unit].present?
      params[:length_unit] = ["centimeter", "centimeters","cms","cms"].include?(params[:length_unit].downcase) ? "cms" : "inches" 
    else
      params[:length_unit] =  user_metric_system["length_unit"] rescue "cms"
    end
    if params[:weight_unit].present?
      params[:weight_unit] = ["kilograms", "kilogram","kg","kgs"].include?(params[:weight_unit].downcase) ? "kgs" : "lbs"  
    else
      params[:weight_unit] = user_metric_system["weight_unit"] rescue "kgs"
    end
    unit_text = {"kgs"=>"kilograms","cms"=>"centimeters", "lbs"=>"pounds", "inches"=>"inches"}
    
    round_off_val = params[:decimal_round_off] || 2
    begin
      # check if any parameter for measurment is updated
      if params[:health].length >= 2
        member_id = params[:health][:member_id] #=>"54412e225acefebacd00000a
        message_array = []
        message_array << "height to #{params[:health][:height]} #{unit_text[params[:length_unit]]}" if params[:health][:height].present? 
        message_array << "weight to #{params[:health][:weight]} #{unit_text[params[:weight_unit]]}" if params[:health][:weight].present? 
        child = ChildMember.find(member_id)
        @health_record = Health.create_measurement_record(current_user,params)
         # height = params[:health]["height"] || child.health_records.where(:updated_at.lte=> params[:health][:updated_at]).not_in(:height => blank_values).order_by("updated_at desc").first.height rescue 0
         # weight = params[:health]["weight"] || child.health_records.where(:updated_at.lte=> params[:health][:updated_at]).not_in(:weight =>  blank_values).order_by("updated_at desc").first.weight rescue 0
          
         # @health_record = @health_record.api_convert_to_unit(user_metric_system,round_off_val) rescue nil
         
        message = "Great. #{child.first_name} is growing fast. I have updated #{message_array.to_sentence}. New BMI for #{child.first_name} is #{@health_record['bmi']}"
        status = StatusCode::Status200
        else
          message = "Insufficient data to update measurement"

      end
    rescue Exception=>e
      message = Message::GLOBAL[:error][:health_create_record] #"Your record has been updated."
      status = StatusCode::Status100
      @error = e.message
    end
    render :json=> {:health=>@health_record,:message=>message, :status=> status,:error=>@error}
  end
    
  def update_record
    begin
    measurement_id =  params[:health_record_id]
    @health = Health.update_measurement_record(measurement_id,params,current_user)
    message = Message::GLOBAL[:success][:health_update_record] #"Your record has been updated."
    status = StatusCode::Status200
    rescue Exception=> e
      message = Message::GLOBAL[:error][:health_update_record] #"Your record has been updated."
      status = StatusCode::Status100
      @error =  e.message
    end
    render :json=> {:health=>@health,:message=>message, :status=> status, :error=>@error}
  end

  def delete_parameter
    begin
      @health = Health.delete_measurement_unit(params,current_user)
      message = Message::API[:success][:health_delete_record]
      status = StatusCode::Status200
    rescue Exception => e
      message = Message::API[:error][:health_delete_record]
      status = StatusCode::Status100
    end
    respond_to do |format| 
      format.json {render :json=> {:health=>@health,:message=>message, :status=> status}}
    end
  end

  

  
  
  def get_who_data(health_parameter, gender=1)
    gender_str = gender
    if health_parameter == "weight" && current_user.metric_system["weight_unit"] =="kgs"
      WhoWeight.where(:gender=> gender_str).in(measure_unit: nil).order_by('month ASC')
    elsif  health_parameter == "weight" && current_user.metric_system["weight_unit"] =="lbs"
      WhoWeight.where(:gender=> gender_str).in(measure_unit: "lbs").order_by('month ASC')

    elsif  health_parameter == "height" && current_user.metric_system["length_unit"] =="cms"
      WhoHeight.where(:gender=> gender_str).in(measure_unit: nil).order_by('month ASC')
    elsif  health_parameter == "height" && current_user.metric_system["length_unit"] =="inches"
      WhoHeight.where(:gender=> gender_str).in(measure_unit: "inches").order_by('month ASC')
    
    elsif  health_parameter == "bmi"
          WhoBmi.where(:gender=> gender_str).order_by('month ASC')

    elsif  health_parameter == "waist_circum" && current_user.metric_system["length_unit"] =="cms"
      WhoWaist.where(:gender=> gender_str).in(measure_unit: nil).order_by('month ASC')
    elsif  health_parameter == "waist_circum" && current_user.metric_system["length_unit"] =="inches"
      WhoWaist.where(:gender=> gender_str).in(measure_unit: "inches").order_by('month ASC')
    
    elsif  health_parameter == "head_circum" && current_user.metric_system["length_unit"] =="cms"
      WhoHcf.where(:gender=> gender_str).in(measure_unit: nil).order_by('month ASC')
    elsif  health_parameter == "head_circum" && current_user.metric_system["length_unit"] =="inches"
      WhoHcf.where(:gender=> gender_str).in(measure_unit: "inches").order_by('month ASC')
    else
     (0..24).map{|a| a}
    end

  end
 

  private
     

    def convert_to_cms_kgs(params)
      Health.convert_to_cms_kgs(params,current_user)
    end

    def get_month(date_obj)
      begin
        d1 = @member.birth_date.to_date
        d2 = date_obj
        (d2 - d1).fdiv(30).to_i
      rescue
        0
      end
    end

    def get_month_in_float(date_obj,round_off_val=nil)
      return get_month(date_obj) if round_off_val.blank?
      begin
        d1 = @member.birth_date.to_date
        d2 = date_obj
        (d2 - d1).fdiv(30).round(round_off_val)
      rescue
        0
      end
    end
    
  def api_convert_to_unit(rec_val,health_unit,round_off_val=1)
    (rec_val.to_f * Health::UNIT_VAL[health_unit]).to_f.round(round_off_val) rescue rec_val.to_f.round(round_off_val)
  end

  def add_diff(measurements,convert_unit_val_list,round_off_val=1,child_member,zscore_flag)
    data =[]
    measurement_member = child_member rescue nil
    measurements.each_with_index do |measurement,index|
      age = measurement_member.birth_date <= measurement.updated_at.to_date ? ApplicationController.helpers.calculate_age(measurement_member.birth_date,measurement.updated_at) : "0d" rescue "0d"
      measurement["age"] = age.gsub(" years","y").gsub(" year","y").gsub(" months","m").gsub(" month","m").gsub(" weeks","w").gsub(" week","w").gsub(" days","d").gsub("day","d").gsub("and","")
      measurement["child_age"] =  age.gsub("and","").gsub("  "," ")
      next_measurement = measurements[index+1].clone rescue {}
      next_measurement = (next_measurement).api_convert_to_unit(convert_unit_val_list,round_off_val) rescue Health.new
      measurement = measurement.api_convert_to_unit(convert_unit_val_list,round_off_val)
      measurement["diff_height"] = (measurement["height"].to_f - next_measurement["height"].to_f).to_f.round_to(round_off_val)   rescue  measurement["height"].to_f.round_to(round_off_val) || "-" 
      measurement["diff_weight"] = (measurement["weight"].to_f - next_measurement["weight"].to_f).to_f.round_to(round_off_val)   rescue  measurement["weight"].to_f.round_to(round_off_val) || "-" 
      measurement["diff_head_circum"] = (measurement["head_circum"].to_f - next_measurement["head_circum"].to_f).to_f.round_to(round_off_val)   rescue  measurement["head_circum"].to_f.round_to(round_off_val) || "-" 
      measurement["diff_waist_circum"] = (measurement["waist_circum"].to_f - next_measurement["waist_circum"].to_f).to_f.round_to(round_off_val)   rescue  measurement["waist_circum"].to_f.round_to(round_off_val) || "-" 
      measurement["diff_bmi"] = (measurement["bmi"].to_f - next_measurement["bmi"].to_f).to_f.round_to(round_off_val)   rescue  measurement["bmi"].to_f.round_to(round_off_val) || "-" 
      measurement["bmi"] = measurement["bmi"].to_f.round_to(round_off_val)
      if zscore_flag && params[:data_type].blank?
        #text = ["appears taller than age","weight gain is normal"].sample
        #measurement["z_score_analysis"] = "#{measurement_member.first_name} #{text}"
      end
      data << measurement
    end
    data
  end

  private
    def get_last_measurement_record(member,round_off_val=1)
      values = ["",nil]
      last_height_record = member.health_records.not_in(:height => values).order_by("updated_at desc").first
      last_weight_record = member.health_records.not_in(:weight => values).order_by("updated_at desc").first
      last_head_circum_record = member.health_records.not_in(:head_circum => values).order_by("updated_at desc").first
      last_waist_circum_record = member.health_records.not_in(:waist_circum => values).order_by("updated_at desc").first
      last_bmi_record = member.health_records.not_in(:bmi => values).order_by("updated_at desc").first
      
      @last_bmi_record = last_bmi_record.convert_to_unit(@measurement_units,round_off_val)["bmi"] rescue nil
      @last_head_circum_record = last_head_circum_record.convert_to_unit(@measurement_units,round_off_val)["head_circum"] rescue nil
      @last_height_record = last_height_record.convert_to_unit(@measurement_units,round_off_val)["height"] rescue nil
      @last_weight_record =  last_weight_record.convert_to_unit(@measurement_units,round_off_val)["weight"] rescue nil
      @last_waist_circum_record =  last_waist_circum_record.convert_to_unit(@measurement_units,round_off_val)["waist_circum"] rescue nil
    end
  
    def get_measurement_units(current_user)
      user_measurement_metric = current_user.metric_system
      @measurement_units = {}
      @measurement_units["length_unit"] =  "inches" if user_measurement_metric["length_unit"] == "inches" #? "inches" : "cms"      
      @measurement_units["weight_unit"] =  "lbs" if user_measurement_metric["weight_unit"] == "lbs"  #? "lbs" : "kgs"
    end

    def set_api_version
      @api_version = 4
    end
    
end
