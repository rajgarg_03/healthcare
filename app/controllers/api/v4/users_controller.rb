 class Api::V4::UsersController < ApiBaseController
  require 'ostruct'
  before_filter :set_api_version
  skip_before_filter :token_verify , :only=>[:get_face_info , :secure_alexa_sign_in, :resend_confirmation_link,:sign_in, :sign_in_via_facebook,:secure_sign_in]
  #before_filter :check_admin_aproval, :only=>[:sign_in]
  def index
    if params[:token].present?
      @user = User.find_user_with_token(params[:token]).first
      @user["image"] = @user.member.profile_image.photo(:small) rescue ""
      status = StatusCode::Status200
    end
    respond_to do |format|
      format.json { render :json => @user.to_json(methods: [:families_count],:include => [:member,:google_cal]) }
    end 
  end

  def get_face_info
  image_url =  params[:image_url]
  api_secret = "p5eNWLCB2TYFbBtk7rr5jAQp7TQ3qmP0"
  api_key = "JhrTRDJmhECb3oy4yQ0sTd42FePDM3C4"
  image_path = image_url || "/home/osboxes/personal/nurtureyreloaded/pictures/local_images/521c/c10a/ed35/780f/4400/0003/original/IMG_0008.JPG"
  #a = `curl -X POST https://api-us.faceplusplus.com/facepp/v3/detect -F api_key=#{api_key} -F api_secret=#{api_secret} -F image_file=@#{image_path} -F return_landmark=1 -F return_attributes=gender,age`
   
  render json:{data:JSON.parse(a)}
      
end

  #PUT api/v1/users/skipped_details.json?token
  def skipped_details
    begin
      (params[:skipped_details]|| []).each do |data|
        if data["type"] == "basic_setup"
          record = SkipBasicSetupAction.where(:skipped=>true,member_id:data["member_id"],screen_name:data["action"],screen_id:data["id"]).first
          record  = record || SkipBasicSetupAction.create(:skipped=>(data["skipped"]|| false),member_id:data["member_id"],screen_name:data["action"],screen_id:data["id"])
          count = record.skip_count.to_i + 1
          record.update_attributes({:skip_count=> count, :skipped_up_to=> (Time.zone.now + 3.days) })
        elsif data["type"] == "handhold"
          record = SkipHandHoldAction.where(:skipped=>true, member_id:data["member_id"],screen_name:data["action"],screen_id:data["id"],skipped_by:current_user.id).first
          record  = record || SkipHandHoldAction.create(:skipped=>(data["skipped"] || false), member_id:data["member_id"],screen_name:data["action"],screen_id:data["id"],skipped_by:current_user.id)
          count = record.skip_count.to_i + 1
          record.update_attributes({:skip_count=> count, :skipped_up_to=> (Time.zone.now + 3.days) })
        end
      end
      user = @current_user || User.find_user_with_token(params[:token]).first.member
      user.add_refs_to_childern
      status = StatusCode::Status200

    rescue Exception => e
      @error = e.message
      status = StatusCode::Status100
    end
    render json: {:status => status,:error=>@error}
  end

  # def delete_account
  #   begin
  #    user = User.where(email:params[:email]).first
  #     m = user.member
  #     email = rand(99999999).to_s + user.email
  #     user.set(:email, email)
  #     m.email = email
  #     user.confirmation_token = email
  #     m.save
  #   rescue Exception=> e
  #     @error = e.message
  #   end
  #   render json: {:status => StatusCode::Status200}
  # end
  
  # Get #/api/v4/users/test_push_notification
  #params = {device_token:""}
  def test_push_notification
    device_token = params[:device_token] 

    current_member = Device.where(token:device_token).last.member rescue current_user
    current_member = current_user if device_token.blank?
    identifier = "action_for_home"
    @notification = BatchNotification::BatchNotificationManager.where(:identifier=>/^#{identifier}/i,status:"Active").last
    UserNotification.add_home_action(current_member,@notification,4)
    user_notifications = UserNotification.where(user_id:current_member.user_id,status:"pending")
    data = []
    user_notifications.each do |user_notification|
      UserNotification.push_notification(user_notification,current_member)
      data << user_notification.get_deeplink_data(current_member)
    end
    render :json =>{status:200,data:data}
  end

# /api/v4/users/test_rake_task?token=CA7cZbxNUkmu8GVidX9a&task_name=assign_pointers&country=IN
# /api/v4/users/test_rake_task?token=CA7cZbxNUkmu8GVidX9a&task_name=assign_action_for_home&country=IN
# /api/v4/users/test_rake_task?token=CA7cZbxNUkmu8GVidX9a&task_name=send_batch_measurement_push&country=IN
# /api/v4/users/test_rake_task?token=CA7cZbxNUkmu8GVidX9a&task_name=assign_measurement_push&batch_no=1&priority=4
# /api/v4/users/test_rake_task?token=CA7cZbxNUkmu8GVidX9a&task_name=send_push_notification_by_batch&batch_no=1&country=UK
# /api/v4/users/test_rake_task?token=CA7cZbxNUkmu8GVidX9a&task_name=delete_all_batch_notification
# /api/v4/users/test_rake_task?token=CA7cZbxNUkmu8GVidX9a&task_name=assign_batch_notification_for_identifier_1&identifier=1
# /api/v4/users/test_rake_task?token=CA7cZbxNUkmu8GVidX9a&task_name=assign_batch_notification_for_identifier_7&identifier=7
# /api/v4/users/test_rake_task?token=CA7cZbxNUkmu8GVidX9a&task_name=assign_batch_notification_for_identifier_14&identifier=14
# /api/v4/users/test_rake_task?token=CA7cZbxNUkmu8GVidX9a&task_name=assign_batch_notification_for_identifier_15&identifier=15
# /api/v4/users/test_rake_task?token=CA7cZbxNUkmu8GVidX9a&task_name=assign_batch_notification_for_identifier_16&identifier=16
# /api/v4/users/test_rake_task?token=CA7cZbxNUkmu8GVidX9a&task_name=assign_identifier_18
  def test_rake_task
    task_name = params[:task_name]
    case task_name
    when "send_batch_measurement_push"
      BatchNotification::Tools::Measurement.send_batch_measurement_push(params[:country])
    when "assign_measurement_push"
      BatchNotification::Tools::Measurement.assign_measurement_push
    when "mark_unknown_milestone_to_unaccomplished"
      member = Member.find(params[:member_id])
      Milestone.mark_milestone_to_unknown_unaccomplished(member)
    when "send_push_notification_by_batch"
      UserNotification.send_push_notification_by_batch(params[:batch_no],params[:country])
    when "assign_batch_notification_for_identifier_1"
      BatchNotification::PushNotification.assign_batch_notification_for_identifier_1(params[:identifier])
    when "assign_batch_notification_for_identifier_5"
      BatchNotification::PushNotification.assign_batch_notification_for_identifier_5(params[:identifier])
   when "assign_batch_notification_for_identifier_3"
      BatchNotification::Tools::Milestones.assign_batch_notification_for_identifier_3(params[:identifier])
   when "assign_batch_notification_for_identifier_2"
      BatchNotification::Tools::Milestones.assign_batch_notification_for_identifier_2(params[:identifier])
   when "assign_batch_notification_for_identifier_7"
      BatchNotification::Tools::Immunisations.assign_batch_notification_for_identifier_7(params[:identifier])
    when "assign_batch_notification_for_identifier_8"
      BatchNotification::Tools::Immunisations.assign_batch_notification_for_identifier_8(params[:identifier])
    when "assign_batch_notification_for_identifier_9"
      BatchNotification::Tools::Milestones.assign_batch_notification_for_identifier_9(params[:identifier])
    when "assign_batch_notification_for_identifier_10"
      BatchNotification::Tools::Measurement.assign_batch_notification_for_identifier_10(params[:identifier])
    when "assign_batch_notification_for_identifier_12"
      BatchNotification::Tools::Measurement.assign_batch_notification_for_identifier_12(params[:identifier])
    when "assign_batch_notification_for_identifier_15"
      BatchNotification::Tools::ToothChart.assign_batch_notification_for_identifier_15(params[:identifier])
    when "assign_batch_notification_for_identifier_14"
      BatchNotification::Tools::Measurement.assign_batch_notification_for_identifier_14(params[:identifier])
    when "assign_batch_notification_for_identifier_16"
      BatchNotification::GeneralNotification.assign_batch_notification_for_identifier_16(params[:identifier])
    when "assign_pointers"
      system("rake create_user_notification[#{params[:country]},'pointer'] RAILS_ENV=#{Rails.env}")
    when "assign_action_for_home"
      system("rake create_user_notification[#{params[:country]},'action_for_home'] RAILS_ENV=#{Rails.env}")
    when "assign_identifier_18"
      system("rake send_email_for_identifier_17_subscription_after_28_06_2018 RAILS_ENV=#{Rails.env}")
      system("rake send_email_for_identifier_17_subscription_before_28_06_2018 RAILS_ENV=#{Rails.env}")
      system("rake create_report_for_send_email_for_pending_identifier_18 RAILS_ENV=#{Rails.env}")
    when "delete_all_batch_notification"
      UserNotification.where( :user_id=>current_user.user_id).delete_all
      BatchNotification::PushNotification.where(object_id:current_user.id.to_s).delete_all
      UserNotification::PushDataForValidation.where(user_id:current_user.user_id.to_s).delete_all
      UserNotification::EmailDataForValidation.where(user_id:current_user.user_id.to_s).delete_all
    end
    render :json =>{status:200}
  end

  # Post #/api/v4/users/test_deeplink
  #params = {deeplink_data:{},token:""}
  def test_deeplink
    msg = "this is test data for deeplink" 
    unread_notification = 0
    pusher  = UserNotification.notification_pusher
    devices = current_user.devices
    user_notification = params[:deeplink_data] rescue {}
    deep_link_data = params[:deeplink_data] rescue {}
    type = 'test_deeplink'
    push_category = deep_link_data["content_url"].present? ? "image_content" : type

    if devices.present? && devices.last.platform == "android"
        fcm =  Fcm.new(APP_CONFIG["fcm_key"])
        registration_ids = [devices.last.token] # an array of one or more client registration tokens
        if deep_link_data.present?
          options = { data: {title: msg, body: msg , notification_type:type,deeplink_params:deep_link_data,content_url:user_notification["content_url"] }}
        else
          options = { data: {title: msg, body: msg , notification_type:type,content_url:user_notification["content_url"] }}
        end
        response = fcm.send(registration_ids, options)
      else
        device = devices.first
        if deep_link_data.present?
          custome_msg = {notification_type: type,deeplink_params:deep_link_data, content_url:user_notification["content_url"]}
        else
          custome_msg = {notification_type: type,content_url:user_notification["content_url"]}
        end
        notification = Grocer::Notification.new(
          device_token: device.token,
          alert: msg,
          custom:custome_msg,
          badge: unread_notification,
          category: push_category, # optional; used for custom notification actions
          sound: "siren.aiff", # optional
          expiry: Time.now + 60*60, # optional; 0 is default, meaning the message is not stored
          identifier: 1234, # optional; must be an integer
          content_available: true # optional; any truthy value will set 'content-available' to 1
        )
        pusher.push(notification)
      end
      render :json=>{:status=>200,:deeplink=>params}
    end

  def test_birthday_instant_push
    childerns = current_user.childerns
     UserNotification.where(:type =>"birthday_wish").delete_all
     UserCommunication::BirthdayWish.where(user_id:current_user.user_id).delete_all
    childerns.batch_size(100).each do |child|
      if Pregnancy.where(expected_member_id:child.id.to_s).pluck("id").blank?
        if (child.birth_date.present?) # && child.birth_date.month == Date.today.month && child.birth_date.day == Date.today.day )
          age = child.age_in_months
          child.family_elder_members.each do |parent_id|
            user = User.where(member_id:parent_id).first
            if user.present? #&& user.is_valid_user_to_send_push?
              begin
                UserCommunication::BirthdayWish.add_to_push_notification(user,child,age)
              rescue
                Rails.logger.warn("Pushnot sent to #{user.email}")
              end
            end
          end
        end
      end
    end
    render :json=>{:status=>200}
  end
  
  def test_birthday_mail_deeplink
    begin
    parent = current_user.user
    child = current_user.childerns.first
    age =  params[:age_in_year].to_i rescue 1
    send_mail(parent,child,age)
  rescue Exception=>e 
    @error = e.message
  end
  render :json=>{:status=>200,:error=>@error}
  end

  def send_mail(parent,child,child_age={})
    if child_age == 1
     UserMailer.first_birthday_email(parent,child,child_age).deliver!
    elsif child_age == 2
     UserMailer.second_birthday_email(parent,child,child_age).deliver!
    elsif child_age == 3
      UserMailer.third_birthday_email(parent,child,child_age).deliver!
    elsif child_age == 4
     UserMailer.fourth_birthday_email(parent,child,child_age).deliver!
    elsif child_age >= 5
      UserMailer.generic_birthday_email(parent,child,child_age).deliver!
    else
      UserMailer.generic_birthday_email(parent,child,child_age).deliver!
    end
     
   puts("Sent birthday email to #{parent.email}")
  end

  def send_scheduled_birthday_wish_push
    pusher = UserNotification.notification_pusher
    # UserCommunication::BirthdayWish.all_schedule_user_notification.batch_size(50)
    UserNotification.where(:type =>"birthday_wish", :status=>"pending").each do |notification|
      user = User.find(notification.user_id)
      if user.is_valid_user_to_send_push?
        member = user.member
        devices = member.devices
        UserNotification.push_notification(notification,member,pusher,devices)
        birthday_wish_record = UserCommunication::BirthdayWish.find(notification.identifier)
        birthday_wish_record.update_record_for_schedule_push
      end
    end
    render :json=>{:status=>200}
  end


  #params{:invitation_id=>, email,password,auth_token}
  def sign_in
    if params[:email].present?
      params[:email] = params[:email].downcase
      user = User.where(email:params[:email]).first
    elsif params[:auth_token].present?
      user = User.find_user_with_token(params[:auth_token]).first
    end
    if (user.present? && (user.valid_password?(params[:password]) || user.is_token_valid?(params[:auth_token],false) ) )
      if user.status == User::STATUS[:unapproved]
        event_name = "user attempt to login failed"
        user.delay.log_signin_event(event_name,params) rescue nil
        render json: {status:101, user:user, message:"User is Blocked"}
      elsif  (!User.can_access_dashboard_after_signup?(user,@api_version) && !user.is_approved?)        
        event_name = "user attempt to login failed"
        user.delay.log_signin_event(event_name,params) rescue nil
        render json: {status:100, user:user, message: Message::API[:success][:email_not_verified]}
      else
        member = user.member
        if !params[:platform].blank?  && !User.is_alexa_platform?(params[:platform])
          update_user_login_details(member,user,params)
        end
        if User.is_alexa_platform?(params[:platform])
          from_sign_in = false
          join_setup_request_status = false
        else
          join_setup_request_status = true
          from_sign_in = true
        end
        event_name = "user logged in successfully"
        user.delay.log_signin_event(event_name,params) rescue nil
        render :json=> hh_and_basic_setup_evaluator_json(member,params[:invitation_id],{:join_req_setup=>join_setup_request_status,:check_basic_setup_and_handhold_count=> true,:from_sign_in=>from_sign_in})
      end
    elsif (user.present? && !(user.valid_password?(params[:password]) || user.is_token_valid?(params[:auth_token],false)) )
      event_name = "user attempt to login failed"
      user.delay.log_signin_event(event_name,params) rescue nil
      render json: {message: Message::API[:success][:password_invalid], status:103}
    else
      render json: {message: Message::API[:success][:email_not_registered], status:102}
    end
  end
  
    # use encrypt password/token
  def secure_sign_in
    if params[:email].present?
      params[:email] = params[:email].downcase
      user = User.where(email:params[:email]).first
    elsif params[:auth_token].present?
      user = User.find_user_with_token(params[:auth_token]).first
    end
    if (user.present? && (user.has_valid_password?(params[:password]) || user.is_token_valid?(params[:auth_token],false) ) )
      if user.status == User::STATUS[:unapproved]
        event_name = "user attempt to login failed"
        user.delay.log_signin_event(event_name,params) rescue nil
        render json: {status:101, user:user, message:"User is Blocked"}
      elsif (!User.can_access_dashboard_after_signup?(user,@api_version) && !user.is_approved?)        
        event_name = "user attempt to login failed"
        user.delay.log_signin_event(event_name,params) rescue nil
        render json: {status:100, user:user, message: Message::API[:success][:email_not_verified]}
      else
        member = user.member
        event_name = "user logged in successfully"
        user.delay.log_signin_event(event_name,params) rescue nil
        update_user_login_details(member,user,params)
        render :json=> hh_and_basic_setup_evaluator_json(member,params[:invitation_id],{:join_req_setup=>true,:check_basic_setup_and_handhold_count=> true,:from_sign_in=>true})
      end
    elsif (user.present? && !(user.has_valid_password?(params[:password]) || user.is_token_valid?(params[:auth_token],false)) )
      event_name = "user attempt to login failed"
      user.delay.log_signin_event(event_name,params) rescue nil
      render json: {message: Message::API[:success][:password_invalid], status:103}
    else
      render json: {message: Message::API[:success][:email_not_registered], status:102}
    end
  end

  # params {state,client_id,scope,response_type,redirect_uri}
  # use encrypt password/token
  def secure_alexa_sign_in
    if params[:email].present?
      params[:email] = params[:email].downcase
      user = User.where(email:params[:email]).first
    elsif params[:auth_token].present?
      user = User.find_user_with_token(params[:auth_token]).first
    end
    if user.present?
      # request from web. so password will be plain
      if session[:alexa_flow_status].to_s == "true"
        # params[:platform] = "alexa"
        user_has_valid_password = user.valid_password?(params[:password])
      else
        user_has_valid_password = user.has_valid_password?(params[:password])
      end
    end
    if (user.present? && (user_has_valid_password || user.is_token_valid?(params[:auth_token],true) ) )
      if user.status == User::STATUS[:unapproved]
      event_name = "user attempt to login failed"
      user.delay.log_signin_event(event_name,params) rescue nil

        render json: {status:101, user:user, message:"User is Blocked"}
      elsif (!User.can_access_dashboard_after_signup?(user,@api_version) && !user.is_approved?)        
        event_name = "user attempt to login failed"
        user.delay.log_signin_event(event_name,params) rescue nil
        render json: {status:100, user:user, message: Message::API[:success][:email_not_verified]}
      else
        redirect_url = params[:redirect_uri] + "#" + "state=#{params[:state]}&access_token=#{user.api_token(true)}&token_type=Bearer"
        member = user.member
        update_user_login_details(member,user,params)
        event_name = "user logged in successfully"
        user.delay.log_signin_event(event_name,params) rescue nil
        if request.xhr?
          render json: {redirect_to:redirect_url, status:StatusCode::Status200}

        else
          redirect_to redirect_url and return true
        end
      end
    elsif (user.present? && !(user.has_valid_password?(params[:password]) || user.is_token_valid?(params[:auth_token],false)) )
      event_name = "user attempt to login failed"
      user.delay.log_signin_event(event_name,params) rescue nil
      render json: {message: Message::API[:success][:password_invalid], status:103}
    else
      render json: {message: Message::API[:success][:email_not_registered], status:102}
    end
  end



  # api /api/v4/users/get_summary_screen.json?token=131aassdas345474
  #Get
  def get_summary_screen
    begin
        user = User.find_user_with_token(params[:token]).first
        member = user.member
        render :json=> {:NYAllSetToGoScreen=>BasicSetupEvaluator.family_screen_data(member), :status=>StatusCode::Status200}
      rescue Exception=>e
        render json: {error:e.message,  status:100}
      end
  end

  # api
  # GET /api/v1/users/basic_set_up_evaluator_for_user.json?token=131aassdas345474das&invitation_id=
  def basic_set_up_evaluator_for_user
      begin
        user = User.find_user_with_token(params[:token]).first
        render :json=> hh_and_basic_setup_evaluator_json(user.member,params[:invitation_id],{:join_req_setup=>false})
      rescue Exception=>e
        render json: {error:e.message,  status:100}
      end
  end

  # Post
  # params {:invitation_id,fb_data
  def sign_in_via_facebook
    # fb_data = {:provider => 'facebook', :uid => '10200460380989199', :info => {:email => 'rishu@xyz.com', :first_name => 'Rishu', :last_name => 'Kumar', :image => "image_url"}}
    fb_data = User.prepare_fb_data(params)
    auth = OpenStruct.new fb_data
    auth.info = OpenStruct.new auth.info
    user = User.from_omniauth(auth)
    respond_to do |format|
      if user.persisted?
        user.skip_confirmation!
        member = user.member
        update_user_login_details(member,user,params)
        event_name = "user logged in successfully"
        user.delay.log_signin_event(event_name,params) rescue nil

        # TODO: For invitation and tour step if required in apis
        format.json {render :json=> hh_and_basic_setup_evaluator_json(member,params[:invitation_id],{:join_req_setup=>true,:check_basic_setup_and_handhold_count=> true,:from_sign_in=>true})}
      else
        if user.errors.added? :email, :taken
          event_name = "user attempt to login failed"
          user.delay.log_signin_event(event_name,params) rescue nil
          message = "We already have an existing account using #{auth.info.email}. Please login."
          format.json { render json: {message: message, status: 100} }
        end
      end
    end
  end  

  # Get/api/v1/users/child_members.json?token="iBSqRyQ5pxuxVb1dSz47
  def child_members
     @families = @current_user.families.includes(:family_members).entries
     @assoc_families = Family.where({'_id' => { "$in" => FamilyMember.where(member_id:current_user.id).pluck(:family_id).uniq - @families.map(&:id)}}).includes(:family_members).entries
     respond_to do |format|
      format.json { render :json=> {:families=>JSON.parse(@families.to_json(:include => {:family_members=>{:include=> {:member=> {:include=> :member_nutrients,:methods=>[:member_nutrients,:member_age]}} } })), :status => StatusCode::Status200 } }      
    end
  end

  # Get /api/v1/users/user_dashboard.json?parent_member_id=542991865acefe0ac1000003&token=iBSqRyQ5pxuxVb1dSz47&event_type=past
  def user_dashboard
    begin
      # action cards
    data = @current_user.api_dashboard(false,@api_version,limit=8).take(8)
    @childern_ids = current_user.childern_ids.map(&:to_s) + [current_user.id.to_s]
      #Calendar data
    events = Event.get_event_data("parent","upcoming",nil,@current_user.id,limit=2).sort_by {|c| c[:start]}[0..1]
    event_data = []
    events.each do |data|
      event_data << {member_id:data[:member_id],name:data[:title],type:data[:type], start_date: data[:start], end_date:data[:end],start_time: (data[:datail]["start_time"].to_time.to_formatted_s(:time) rescue "00:00"),end_time: (data[:datail]["end_time"].to_time.to_formatted_s(:time) rescue "23:00")}
    end
     # Pointer data
    article_ref = ArticleRef.get_pointers_list(current_user,@childern_ids).order_by("effective_score desc").limit(2)
     #Preganacy widget
    pregnancy_widget = Pregnancy.pregnancy_widget(@current_user)
    status = StatusCode::Status200
    rescue Exception=> e
      @error = e.message
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,params,"Error inside user_dashboard")
      status = StatusCode::Status100
    end
    begin
      respond_to do |format|
        format.json { render :json=> {:error=> @error, :status=> status, :recommended=> data, :event_widget=>event_data, :reference_widget=> article_ref,:pregnancy_widget=>pregnancy_widget} }
      end
    rescue Exception => e
      Rails.logger.info "Error inside user_dashboard"
      Rails.logger.info e.message
      UserMailer.delay.notify_dev_for_error(e.message,e.backtrace,params,"Error inside user_dashboard")
      Rails.logger.info e.backtrace
      render :json=> {:error=> @error, :status=> status, :recommended=> [], :event_widget=>[], :reference_widget=> [],:pregnancy_widget=>[]}
    end
  end

  
  # Api-68
  #Post
  #/api/v3/users/skip_push_screen.json?token= 
  #params {:device_token=>}
  def skip_push_screen
    begin
    device = Device.where(member_id:current_user.id.to_s,:device_token=>params[:device_token]).last
    device.screen_skip_count = device.screen_skip_count.to_i + 1
    device.screen_skip_up_to = Time.now.in_time_zone + 3.days
    device.save
    status = 200
    message = "Request processed sucessfully"
  rescue Exception=>e
    status = 100
    @error = e.message
    message = "Sorry! I could not fulfill your request due to an error. Please try again."
  end
    render :json=> { :status=>status, :message=> message, :error=>@error, :device_detail=> device }
  end
  
  def set_version_controller
    begin
      version_control = VersionController.where(member_id:current_user.id).last
      version_control.notification_count = 0
      version_control.save
      status = 200
    rescue Exception=>e
      @error = e.message
      status = 100
    end
    render :json=>{:status=> status, :error=>@error}
  end
  
  #Logout from app.
  def user_sign_out
    begin
      resource_name = current_user.user
      resource_name["app_login_status"] = false
      signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)) 
      status = StatusCode::Status200
      event_name = "user signout successfully"
      resource_name.delay.log_signin_event(event_name,params) rescue nil
    rescue Exception=>e
      status = StatusCode::Status100
      event_name = "user attempt to signout failed"
      resource_name.delay.log_signin_event(event_name,params) rescue nil rescue nil
      @error = e.message
    end
      render :json=>  {:message=>Message::DEVISE[:m16] ,:status => status,:error=>@error } 
  end

  def resend_confirmation_link
    if current_user && params[:email].blank?
      current_user.user.send_confirmation_instructions
      @message = Message::DEVISE[:m8] #"Verification email has been sent to your email."
    else
      begin
        params[:email] = params[:email].downcase
        user = User.where(email:params[:email]).first
        user.send_confirmation_instructions 
      rescue Exception => e
        redirect_to after_registration_path(email:params[:email]) and return true
      end
      respond_to do |format|
        format.html{ redirect_to home_messages_path(email_resent: true) and return true  }
        format.json{render :json=>  {:message=>Message::DEVISE[:m10] ,:status => StatusCode::Status200 } and return true}
      end
    end
    respond_to do |format|
      format.html
      format.js
      format.json{render :json=>  {:flash=> Message::DEVISE[:m8], :message=>Message::DEVISE[:m10] ,:status => StatusCode::Status200 }}
    end
  end
  # api 
  # post /api/v2/users/enable_push_notification.json?token=
  # params = {:device_token=>"jhjhjhjhrqweqcs",:platform=>"ios"}
  def enable_push_notification
    begin
      sub_noti = current_user.member_settings.where(name:"subscribe_notification").first ||  MemberSetting.create(name:"subscribe_notification",member_id:current_user.id)
      status_val = true
      sub_noti.update_attribute(:status,status_val)
      current_user.subscribe_notification("on")
      store_device_token(current_user,params[:device_token],params[:platform])
      message = Message::API[:success][:enable_push_notification]
      status = StatusCode::Status200
    rescue Exception=>e
      status = StatusCode::Status100
      message = Message::API[:error][:enable_push_notification]
      @error = e.message
    end
    render :json=>  {:message=>message,:status=>status,:error=>@error}
  end
  
  # api
  # GET /api/v2/users/basic_set_up_evaluator_for_user.json?token=131aassdas345474das&invitation_id=
  def basic_set_up_evaluator_for_user
    begin
      user = User.find_user_with_token(params[:token]).first
      data =  hh_and_basic_setup_evaluator_json(user.member,params[:invitation_id],{:join_req_setup=>false,:check_basic_setup_and_handhold_count=>false})
      data.delete(:families) rescue nil
      render :json=> data
    rescue Exception=>e
      render json: {error:e.message,  status:100}
    end
  end

  def sec_email_add
    @success = false
    token = Digest::MD5.hexdigest(params[:secondary_emails][:email])
    SecondaryEmail.where(:user_id=>current_user.id).delete    
    @sec_email = current_user.build_secondary_email(params[:secondary_emails].merge(:confirmation_token => token))
    if @sec_email.save()
      url = request.host_with_port+"/users/verify_secondary_email?token=#{token}"
      UserMailer.secondary_email(@sec_email, "Nurturey-Email Verification", url, Time.now).deliver
      @success = true
    end
    respond_to do |format|
    format.json  { render :json=>{:email=>params[:secondary_emails][:email], :status=> StatusCode::Status200, :head => :ok} }
    end
  end
  
  def sec_email_del
    @success = false
    @empty = true
    if !current_user.secondary_email.nil?
      if current_user.secondary_email.destroy
        @success = true
        @empty = false
      end
    end
      respond_to do |format|
      format.json  { render :json=>{ :status=> StatusCode::Status200, :head => :ok} }
    end
  end

  def set_default_cover_image
    begin
      user = current_user.user
      user.cover_image.delete
      message = "Default cover image is set successfully."
      status = StatusCode::Status200
    rescue Exception=> e
      message = Message::API[:success][:set_default_cover_image]
      status = StatusCode::Status100
      @error = e.message
    end
    render :json=>{:message=>message, :status=> status,:error=>@error}
  end  

  def upload_image_multipart
    member = Member.where(id: params[:member_id]).first    
    #NOTE: Single file will be received instead of array
    pic = params[:media_files] if params[:media_files].present?
    @picture = Picture.new(local_image:pic ,upload_class: 'pic')
    if @picture.valid? && member.profile_image = @picture
      @picture_saved = true
      @message = Message::API[:success][:upload_image]
      @status = StatusCode::Status200
    else
      @picture_saved = false
      @message = Message::API[:error][:upload_image]
      @status = StatusCode::Status100
    end
    render json: {picture: @picture.attributes.merge(url_small: @picture.photo_url_str(:small), url_large: @picture.photo_url_str(:medium), complete_url_small: @picture.complete_image_url(:small), complete_url_large: @picture.complete_image_url(:medium), aws_small_url: @picture.aws_url(:small), aws_original_url: @picture.aws_url)}
  end

  # Get /api/v2/users/user_info.json?token=iBSqRyQ5pxuxVb1dSz47
  def user_info
    # Get all basic info related to logged in user
    user_basic_info_data = User.user_basic_info(current_user,nil,@api_version)
    user_basic_info_data[:status] = StatusCode::Status200
    respond_to do |format|
       format.json { render json: user_basic_info_data}
    end
  end

   # PUT http://localhost:3000/api/v1/users/dismiss_nutrient.json?token=XrjzH88mP4syK1q4MTeb&card_id=54drtef65v76_health
  # option values for nutrient_name[ health,timelines,nutrients,immunsation,milestones,document]
  def dismiss_nutrient
    child_id,nutrient_name = params[:card_id].split("_")
    
    nutrient = DismissNutrient.where(parent_id:current_user.id, child_id: child_id ,nutrient_name: nutrient_name).last || DismissNutrient.new(parent_id:current_user.id, child_id: child_id ,nutrient_name: nutrient_name ,dismiss_up_to: Date.today+7.day)
    # nutrient = DismissNutrient.new(parent_id:@current_user.id, child_id: params[:child_id] ,nutrient_name:params[:nutrient_name],dismiss_up_to: Date.today-1.day)
    nutrient.dismiss_up_to =  Date.today + 7.days
    msg = nutrient.save ? {:message=>"Dismissed sucessfully", :status=> 200} : {:message=> nutrient.errors.messages, :status=> 100}
    if nutrient_name == "AddSpouse"
      family_id = child_id
      current_user.skip_add_spouse(family_id)
    end
    respond_to do |format|
      format.json {render json: msg}
    end
  end 

  # Get /api/v1/users/recent_items.json?parent_member_id=542991865acefe0ac1000003&token=iBSqRyQ5pxuxVb1dSz47&event_type=past
  def recent_items
    @childern_id = @current_user.childern_ids
    recent_timeline =@current_user.api_recent_timeline(@childern_id)
    recent_health = @current_user.api_recent_health(@childern_id)
    recent_milestone = @current_user.api_recent_milestones(@childern_id)
    recent_vaccine = @current_user.api_recent_immunisation(@childern_id)
    data = recent_timeline + recent_health + recent_milestone + recent_vaccine
    data= (data.sort_by {|c| c[:line3]}).reverse
    paginate_data = data.paginate(:page => (params[:page]), :per_page => 6)
    respond_to do |format|
      format.json { render :json=> {:recent=> paginate_data,:status=> StatusCode::Status200,:total=>data.count,:current_page=>(params[:page]||1), :per_page=>6}}
    end
  end

  # Get /api/v1/users/user_events.json?calendar_type=child&member_id=542991865acefe0ac1000003&token=iBSqRyQ5pxuxVb1dSz47&event_type=past
  # @params: member_id: selected member id
  #          calendar_type[parent/child]: calendar for parent or child
  #          event_type[upcoming/past]: "upcoming event or past event "
  # @return Event and vaccination event detail
  def user_events
    event_per_page = 15
    data = Event.get_event_data( params[:calendar_type] || "parent", params[:event_type] || "upcoming", params[:member_id], @current_user.id)
    if params[:event_type] == "upcoming"
      data= (data.sort_by {|c| c[:start]})
    else
      data= (data.sort_by {|c| c[:start]}).reverse
    end
    if params[:page]
      paginate_data = data.paginate(:page => (params[:page]), :per_page => event_per_page)
      current_page = params[:page].to_i
      total_page,next_page = Utility.page_info(data,current_page,event_per_page)
      per_page = event_per_page
      # pagination_attributes = {current_page: current_page, next_page: next_page, total_page:total_page}
    else
       paginate_data =  data
       current_page = 1
       total_page = 1
       next_page = 1
       per_page = data.count
    end
    paginate_data = paginate_data.group_by {|d| d[:start].to_date.strftime("%b %Y") }
    response_hash = {:status=> StatusCode::Status200, :recent=> paginate_data,:total=>data.count,:current_page=> current_page, :per_page=>per_page, next_page: next_page, total_page:total_page}
    # response_hash.merge!(pagination_attributes) if params[:page]
    respond_to do |format|
      format.json { render :json=> response_hash}
    end
  end

  def upload_image
    member = Member.where(id: params[:member_id]).first
    content = params[:image_data]
    content.gsub!('\\r', "\r")
    content.gsub!('\\n', "\n")
    decode_base64_content = Base64.decode64(content) 
    File.open("public/pictures/#{member.first_name.downcase}_#{member.id}.jpg", "wb") do |f|
      f.write(decode_base64_content)
    end
    file = File.open("public/pictures/#{member.first_name.downcase}_#{member.id}.jpg")
    @picture = Picture.new(local_image:file,upload_class: 'pic')
    if @picture.valid? && member.profile_image = @picture
      @picture_saved = true
      @message = Message::API[:success][:upload_image]
      @status = StatusCode::Status200        
    else
      @picture_saved = false
      @message = Message::API[:error][:upload_image]
      @status = StatusCode::Status100
    end
    File.delete("public/pictures/#{member.first_name.downcase}_#{member.id}.jpg")
    respond_to do |format|
      format.json
    end
  end

# Ap1
# Post /api/v1/users/upload_cover_image.json?token
# Params {member_id=>"123123sad132", :image_data=> }
  def upload_cover_image
    begin
    member = Member.where(id: params[:member_id]).first
    file , file_name= Picture.binary_image_to_file(params[:image_data], member, "cover_image") if params[:image_data].present?
    file = params[:media_files] if params[:media_files].present?
    @picture = Picture.new(local_image:file,upload_class: 'coverimage',member_id:member.id)
    if @picture.valid? && member.user.cover_image = @picture
      @picture_saved = true
      @message = Message::API[:success][:upload_cover_image]
      @status = StatusCode::Status200        
    else
      @picture_saved = false
      @message = Message::API[:error][:upload_cover_image]
      @status = StatusCode::Status100
    end
    File.delete(file_name) if file_name
    render :json=> {:message=> Message::API[:success][:upload_cover_image],:status => StatusCode::Status200, :cover_image=> @picture.attributes.merge(url_small: @picture.complete_image_url(:small), url_large: @picture.complete_image_url(:medium), url_original: @picture.complete_image_url(:original), aws_small_url: @picture.aws_url(:small), aws_original_url: @picture.aws_url)}
    rescue Exception=>e
      render :json=>{:error=>e.message, :message=> Message::API[:error][:upload_cover_image],:status => StatusCode::Status100 }
    end
  end


  private
  def check_admin_aproval
    params[:email] = params[:email].downcase if params[:email]
    user = User.where(:email=>params[:email]).first
    if user && user.status == User::STATUS[:unconfirmed]
      render :json=> {:message=>"Kindly verify your email ID. Please check the email we sent to you.",:screen_name=>["email_verification_screen"],:status=>StatusCode::Status100 } 
      return    
    end
  end
  
  def update_user_login_details(member,user,params)
    UserLoginDetail.create(user_id:user.id,login_time:Time.zone.now,platform:(params[:platform]||"Device") ) rescue nil
    store_device_token(member,params[:device_token],params[:platform],params) #if params[:device_token].present?
    api_sign_in(user)
    @current_user  = member
  end

  def hh_and_basic_setup_evaluator_json(member,invitation_id=nil,options={:join_req_setup=>true,:check_basic_setup_and_handhold_count=>false,:from_sign_in=>false})
    member.reload
    user =  User.where(member_id:member.id.to_s).last
    api_version = @api_version
    user_info = nil
    member_user_families = member.user_families
      user_info = User.user_basic_info(member,nil,api_version) 
    user_family_list = nil
    user_family_list  = JSON.parse(Family.family_to_json(member_user_families,api_version,member) )
    json_data = {:families=>user_family_list, :user_basic_info=> user_info,:status => StatusCode::Status200}
    # json_data = {:families=>JSON.parse(Family.family_to_json(member_user_families,api_version,member) ), :user_basic_info=> User.user_basic_info(member,member_user_families,api_version),:status => StatusCode::Status200}
    
    if User.is_alexa_platform?(params[:platform])
      vc = false # don't run version check if coming from alexa reference
    else
      version_controller_options  = options[:from_sign_in] == true ? {platform:params[:platform],os_version:params[:os_version],app_version:params[:app_version]} : {}
      version_controller,vc = VersionController.run(member,version_controller_options)
    end 
    if vc
       json_data["NYVersionCheckerController"]= version_controller
    end
    join_setup_controller,@js_status = JoinReqSetup.run(member) if (options[:join_req_setup] && !User.is_alexa_platform?(params[:platform]))
     
    if @js_status 
      json_data["NYJoinSetupController"] = join_setup_controller
      return json_data
    else
     is_deeplink_available = params[:destination_name].blank? || params[:destination_name].downcase == "home"
     valid_to_run_handhold_and_basic_setup = user.valid_to_run_handhold_and_basic_setup?(params[:platform]) 
     
      if is_deeplink_available && (!options[:check_basic_setup_and_handhold_count]  || valid_to_run_handhold_and_basic_setup)
        controllers,status = BasicSetupEvaluator.run(member,invitation_id,api_version,{:platform=>params[:platform],:app_name=>params[:app_name]})
        unless status["basic_setup"]
          handhold_data =  HandHold.user_complete_handhold(member,@api_version)
          json_data["handhold_controllers"] = handhold_data
        end
        json_data["basic_setup_controller"] = controllers
        json_data["status"] = StatusCode::Status200
      end
      json_data
    end
    
  end
  
  def verify_family_id(rnd_id)      
    rand_id = rnd_id
    if !Family.where(:family_id =>rand_id).first
      return rand_id
    else
      rand_id = "10000" + rand(99999).to_s
      verify_family_id(rand_id)
    end
  end

  # 1. Find device.
  # 2. If device is not present, create one for this member.
  # 3. ElseIf device exists and attached with some other member, then switch the member with this member.
  # options = {os_version, device_model, app_version}
  def store_device_token(member, token, platform,options={})
    member.store_device_info(member, token, platform,options.merge({:ip_address=>request.remote_ip}))
     
    
  end

  
   
  def set_api_version
    @api_version = 4
  end
 
  def dashboard_data
    @user_family_member = current_user.family_members(FamilyMember::STATUS[:accepted])
    @families = current_user.user_families
    score = Score.where(:family_id.in=>@families.map(&:id)).sum(:point) + current_user.scores.where(:activity_type.in => ['User','Invitation']).sum(:point)
    @score = Score::badge(score)
    @childern_id = current_user.childern_ids
    @childern = current_user.childerns(@childern_id)
    @recent_timeline = current_user.recent_timeline(@childern_id).sample(1)
    @recent_health = current_user.recent_health(@childern_id).sample(1)
    @recent_milestone = current_user.recent_milestones(@childern_id).sample(1)
    @recent_vaccine = current_user.recent_immunisation(@childern_id).sample(1)
    @recent_picture =  current_user.recent_picture(@childern_id).sample(2)
    if @recent_timeline.blank? && @recent_health.blank? && @recent_milestone.blank? && @recent_picture.blank?
      @no_recent_item = true
    end
  end
end