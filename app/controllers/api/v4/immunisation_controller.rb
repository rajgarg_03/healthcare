class Api::V4::ImmunisationController < ApplicationController
  def index
    @member = Member.find(params[:member_id])
    @vaccinations =  @member.vaccinations.not_in(:opted => "false").includes(:jabs).sort_by{|i| i.get_name.upcase}
    info_popup_status(@member,"immunisation") 
    remommended_pointers = ArticleRef.get_recommended_pointer(current_user,@member,"vaccination")
    options = {}
    @tool, @show_subscription_box_status,@info_data,vaccination_list = @member.get_subscription_show_box_data(current_user,"Immunisations",api_version=4,options)
    if @show_subscription_box_status == false
      vaccination_list = Vaccination.vaccination_with_jabs(@vaccinations)
    end

    respond_to do |format|
      format.html
      format.js {render :layout=>false}
      # format.json {render :json=> {:status => StatusCode::Status200, :member=>@member, :system_vaccination=>Vaccination.vaccination_list(@member,current_user), :vaccinations=> JSON.parse(@vaccinations.to_json(:include=>:jabs,:methods=>[:total_jabs_count,:completed_jabs_count])), :skip_next_scrren=> Vaccination::ActionList["skip_screen_name"]} }
      format.json {render :json=> {:system_tool_id=>@tool.id,:show_subscription_box=>@show_subscription_box_status, :remommended_pointers=>remommended_pointers, :status => StatusCode::Status200,:vaccin_report=>Vaccination.vaccination_report_for_member(@member), :member=>@member, :vaccinations=> vaccination_list} }
    end    
  end
  
  #returns upcoming jab detail. Used for api 
  #/immunisation/upcoming_jabs_group_by_vaccin.json?member_id=5486d29d5acefe697a000066&token=iBSqRyQ5pxuxVb1dSz47"
  def upcoming_jabs_group_by_vaccin
    @member = Member.find(params[:member_id])
    data = []
    detail = {}
    detail["vaccination"]  = {}
    detail["vaccination"]["jabs"]  = {}
    jab_detail = {}
    vaccinations = @member.vaccinations.not_in(:opted => "false")#.map(&:id)
    vaccinations.each do |vaccin|
      jabs = []
      
      detail["vaccination"]["name"] = vaccin.get_name rescue ""
      detail["vaccination"]["category"] = vaccin.category rescue ""
      detail["vaccination"]["sys_vaccin_id"] = vaccin.sys_vaccin_id rescue ""
      jab_list = vaccin.jabs.or(:due_on.gte => DateTime.now).or({:est_due_time.gte => DateTime.now}).order_by("due_on est_due_time DESC")
      detail["vaccination"]["completed"] = jab_list.where(status:"Administered").count rescue 0
      
      jab_list.each_with_index do |jab,index|
        jab_detail["id"] = vaccin.id rescue ""
        jab_detail["name"] = "jab#{(index + 1)}" rescue ""
        if jab.status != "Administered" && jab.due_on.blank? && !jab.est_due_time.blank?
          jab_detail["status"] = "Estimated due date on"
        else
          jab_detail["status"] = (jab.status == "Administered" ? "Administered on" : "Due On")
        end
        jab_detail["date"] = (jab.due_on || jab.est_due_time) rescue ""
        jab_detail["id"] = jab.id rescue ""
        jabs << jab_detail
        jab_detail  = {}
      end
      detail["vaccination"]["jabs"] = jabs
      data << detail
      detail = {}
      detail["vaccination"]  = {}
    detail["vaccination"]["jabs"]  = {}
        
    end

    respond_to do |format|
      format.html
      format.js {render :layout=>false}
      format.json {render :json=> {:status => StatusCode::Status200, :data=> data} }
    end 
  end
 
 # returns upcoming jab detail. Used for api
 # http://localhost:3000/immunisation/upcoming_jabs.json?member_id=551fc5ae5acefee7ad0001dc&token=iBSqRyQ5pxuxVb1dSz47&past_jab=true
  def upcoming_jabs
    @member = Member.find(params[:member_id])
    # Find all jab included in vaccination of member
    vaccinations = @member.vaccinations.not_in(:opted => "false").map(&:id)
    if params[:past_jab] == "true"
     jabs = Jab.in(:vaccination_id=> vaccinations).or(:due_on.lt => DateTime.now).or(:due_on=> nil,:est_due_time.lt => DateTime.now).map{ |j| j["vaccin_date"] = (j.due_on|| j.est_due_time);j }
    else
     jabs = Jab.in(:vaccination_id=> vaccinations).or({:due_on.gte => DateTime.now},{:due_on=> nil,:est_due_time.gte => DateTime.now}).map{ |j| j["vaccin_date"] = (j.due_on|| j.est_due_time);j }
    end
    if params[:past_jab]
      jabs = jabs.sort_by{|jab| jab["vaccin_date"]}.reverse
    else
       jabs = jabs.sort_by{|jab| jab["vaccin_date"]}
    end
    data = []
    detail = {}
    detail["vaccination"]  = {}
    detail["vaccination"]["jabs"]  = {}
    jab_detail = {}
    #Prepare data for api response. 
    jabs.each_with_index do |jab, index|
      vaccin = jab.vaccination
      detail["vaccination"]["name"] = vaccin.get_name rescue ""
      detail["vaccination"]["category"] = vaccin.category rescue ""
      detail["vaccination"]["sys_vaccin_id"] = vaccin.sys_vaccin_id rescue ""
      detail["vaccination"]["vaccin_id"] = vaccin.id
      jab_detail["name"] = "jab#{(index + 1)}" rescue ""
      if jab.status != "Administered" && jab.due_on.blank? && !jab.est_due_time.blank?
        jab_detail["status"] = "Estimated due date on"
      else
        jab_detail["status"] = (jab.status == "Administered" ? "Administered on" : "Due On")
      end
      jab_detail["date"] = (jab.due_on || jab.est_due_time) rescue ""
      start_time,end_time = Vaccination.start_end_time(jab)
      jab_detail["start_time"] = start_time
      jab_detail["end_time"] = end_time
      jab_detail["fullday"] = jab.fullday
      jab_detail["id"] = jab.id rescue ""
      detail["vaccination"]["jabs"] = jab_detail
      data << detail
      detail ={}
      jab_detail = {}
      detail["vaccination"]  = {}
      detail["vaccination"]["jabs"]  = {}
    end 
    respond_to do |format|
      format.html
      format.js {render :layout=>false}
      format.json {render :json=> {:status => StatusCode::Status200, :data=> data} }
    end 
  end

  # API: GET /immunisation/jab_detail.json?jab_id=121&token=
  def jab_detail
    begin
      jab_obj = Jab.find(params[:jab_id])
      jab = jab_obj.attributes
      start_time,end_time = Vaccination.start_end_time(jab_obj)
      jab["start_time"] = start_time
      jab["end_time"] = end_time
      vaccine = jab_obj.vaccination
      jab["vaccine_title"] = vaccine.get_name
      index = (vaccine.jabs.map(&:id).index(jab_obj.id)+1).to_s 
      jab["name"] = "Jab" +  index
     render :json=> {:status => StatusCode::Status200, :jab=> jab}  
   rescue 
     render :json=> {:status => StatusCode::Status100, :message=> Message::GLOBAL[:success][:jab_detail]}  
    end 
  end

  # Add new Jab without any due on .
  # Get
  # /api/v3/immunisation/new_jab.json
  #params: {"vacci_id"=>"5805c58f8cd03d730d0001fd", "token"=>"541ebcb88cd03ddfc4000002"}
  def new_jab
    begin
      # Find Vaccination in which jab need to add. vacci_id is vaccination id
      @vaccination = Vaccination.where(id:params[:vacci_id]).first
      # Create/Add Jab in Vaccination
      @jab = Jab.create(vaccination_id: @vaccination.id, fullday: true )
      #Count Index of added Jab
      @index = @vaccination.jabs.count - 1
      @jab["name"] = "Jab #{@index+1}"
      @jab["vaccine_title"] = @vaccination.get_name
      status = StatusCode::Status200
    rescue Exception=>e
      @error = e.message
      status = StatusCode::Status100
    end
    respond_to do |format|
      format.js {render action: "save_jab", :layout=> false}
      format.json {render :json=> {:message=> Message::API[:success][:save_jab], :status => StatusCode::Status200, :error=>@error,:jab=>@jab,:index=>@index, :vaccination=> @vaccination } }

    end
  end

def save_jab
    @vaccination = Vaccination.where(id:params[:jab][:vaccination_id]).first
    @jab = Jab.create(params[:jab] )
    @index = @vaccination.jabs.map(&:id).index(@jab.id)+1 
     @vaccination.member.send_event_to_parent_google_cal(@jab,"jab","create",{name:@vaccination.get_name, index: @index.to_s}) rescue ""
     @jab["name"] = "Jab #{@index}"
    respond_to do |format|
      format.html {render :layout=>false}
      format.js {render :layout=>false}
      format.json {render :json=> {:message=> Message::API[:success][:save_jab],:status => StatusCode::Status200, :jab=>@jab,:index=>@index, :vaccinations=> Vaccination.vaccination_with_jabs(@vaccination) } }
    end   
  end

  def edit_jab
    @jab = Jab.where(id: params[:jab_id] ).first
    @vaccination = Vaccination.where(id: @jab.vaccination_id).first
    @index = @vaccination.jabs.map(&:id).sort.index(@jab.id)+1
    @jab["name"] = "Jab #{@index}" 
    vaccine_name = @vaccination.get_name
    @jab["vaccine_title"] = vaccine_name
    Timeline.where(timelined_type: "Jab",timelined_id: @jab.id).delete
    if params[:administered] == "true"
      @jab.update_attributes(status:"Administered", due_on: Date.today) 
      @jab.timelines << Timeline.new({entry_from:"jab", name:vaccine_name,desc:"Jab#{@index} Administered on #{toDate1(@jab.due_on)}", member_id:@vaccination.member_id,added_by: current_user.id,timeline_at: Timeline.new.get_timeline_at(@jab.due_on || Date.today)})
    end
    @url = "/immunisation/update_jab?id=#{@jab.id}"
    render :action=> "new_jab", layout: false
  end
 
  # update Jab detail
  # Type: PUT 
  # Url: /api/v3/immunisation/update_jab.json?
  # Params : {"id"=>"589afc088cd03d9956000002", "jab"=>{"note"=>"", "name"=>"Jab 6", "jab_location"=>"", "facility"=>"", "status"=>"Planned", "doc_name"=>"", "due_on"=>"08 Feb 2017"}, "token"=>"541ebcb88cd03ddfc4000002"}
  def update_jab
    @jab = Jab.find(params[:id])
    jab_ref_date = (@jab.due_on || @jab.est_due_time).to_date rescue nil
    cerate_to_google = (@jab.est_due_time.present? && @jab.due_on.blank?) || (@jab.est_due_time.blank? && @jab.due_on.blank?)
    administered_to_planned = @jab.status == "Administered" && params[:jab][:status] == "Planned"
    
    # Handle if no time sent only Date updated.
    jab_start_date = params[:jab][:due_on].to_date
    if (params[:jab][:due_on] || @jab.due_on) && params[:jab][:start_time]
      params[:jab][:start_time] = jab_start_date.to_time1((params[:jab][:start_time] || "00:00"))
      params[:jab][:end_time] =   jab_start_date.to_time1((params[:jab][:end_time] || "23:00"))
      params[:jab][:fullday] =  params[:jab][:start_time].blank? rescue true
    end
    params[:jab][:due_on] = params[:jab][:start_time] || (jab_start_date.to_time1("00:00") rescue nil) # set Date and time for Jab due on 
    
    #update Jab and update Pointer if jab date is changed
    @jab.update_attributes(params[:jab])
     if jab_ref_date != ((@jab.due_on || @jab.est_due_time).to_date rescue nil)
      ArticleRef.update_jab_ref(@jab)
     end
    @vaccination = @jab.vaccination
    vaccine_name = @vaccination.get_name
    @index = @vaccination.jabs.map(&:id).sort.index(@jab.id)+1
    if @jab.status == "Planned" && @jab.due_on.present?
      if cerate_to_google || administered_to_planned
      # UserActions.add_update_user_action(@jab,"Calendar",current_user,"Add")

        @vaccination.member.send_event_to_parent_google_cal(@jab,"jab","create",{name:vaccine_name, index: @index.to_s})
      else
        @vaccination.member.send_event_to_parent_google_cal(@jab,"jab","update",{name:vaccine_name, index: @index.to_s})
      end
    end

    # Add to timeline if Jab is administered and delete event from Googl Cal 
    Timeline.where(timelined_type: "Jab",timelined_id: @jab.id).delete
    if @jab.status == "Administered"
      @vaccination.member.delete_event_from_parent_google_cal(@jab.id.to_s,"jab")
      @jab.timelines << Timeline.new({entry_from:"jab", name:vaccine_name[0..48],desc:"Jab#{@index} Administered on #{toDate1(@jab.due_on)}", member_id:@vaccination.member_id,added_by: current_user.id, timeline_at: Timeline.new.get_timeline_at(@jab.due_on || Date.today)})
    end
    respond_to do |format|
      format.html {render :layout=>false}
      format.js {render :layout=>false}
      format.json {render :json=> {:message=> Message::API[:success][:update_jab], :status => StatusCode::Status200, :jab=>@jab,:index=>@index, :vaccinations=> Vaccination.vaccination_with_jabs(@vaccination) } }
    end  
  end

  def delete_jab
    @jab = Jab.find(params[:jab_id])
    @vaccination = @jab.vaccination
    Timeline.where(timelined_type: "Jab",timelined_id: @jab.id).delete
    @vaccination.member.delete_event_from_parent_google_cal(@jab.id.to_s,"jab")
    ArticleRef.where(record_id:@jab.id.to_s).delete_all
    @jab.delete
    @vaccination = @vaccination.reload
    respond_to do |format|
      format.html 
      format.js
      format.json {render :json=> {:message=> Message::API[:success][:delete_jab],:status => StatusCode::Status200, :jab=>@jab, :vaccinations=> Vaccination.vaccination_with_jabs(@vaccination) } }
    end
  end

  # returns upcoming jab detail month vise. Used for api
  # http://localhost:3000/api/v4/immunisation/jabs_month_wise.json?member_id=551fc5ae5acefee7ad0001dc&token=iBSqRyQ5pxuxVb1dSz47&jab_type=upcoming || past
  def jabs_month_wise
    @member = Member.find(params[:member_id])
    # Find all jab included in vaccination of member
    
    options = {}
    options = options.merge({:get_data_for_from_list=>params[:jab_type]})
    @tool, @show_subscription_box_status,@info_data,data = @member.get_subscription_show_box_data(current_user,"Immunisations",api_version=4,options)
    if @show_subscription_box_status == false
      data, month_order = {}, {}
      vaccinations = @member.vaccinations.not_in(:opted => "false").pluck(:id)
      if params[:jab_type] == "past"
        data = past_jab_details(vaccinations, data)
      elsif params[:jab_type] = 'upcoming'
        data = upcoming_jabs_details(vaccinations, data)
      end
    end

    month_order = data.keys

    respond_to do |format|
      format.html
      format.js {render :layout=>false}
      format.json {render :json=> {:system_tool_id=>@tool.id,:show_subscription_box=>@show_subscription_box_status,:status => StatusCode::Status200, data: data, month_order: month_order } }
    end
  end

  def upcoming_jabs_details(vaccinations, data)
    # jabs = Jab.in(:vaccination_id=> vaccinations).or({:due_on.gte => DateTime.now.beginning_of_month},{:due_on=> nil,:est_due_time.gte => DateTime.now.beginning_of_month},{:due_on=>nil,:est_due_time=>nil}).map{ |j| j["vaccin_date"] = (j.due_on || j.est_due_time);j }
    jabs = Jab.in(:vaccination_id=> vaccinations).not_in(status:"Administered").map{ |j| j["vaccin_date"] = (j.due_on || j.est_due_time || Time.now);j }
    jabs = jabs.sort_by{|jab| jab["vaccin_date"]}
    jabs = jabs.group_by { |m| m.due_on || m.est_due_time }
    data['Overdue'], data['This Month'], data['Next Month'] = [], [], []

    #Prepare data for api response.

    today_date = DateTime.now.in_time_zone.beginning_of_day.to_date
    month_end_date = DateTime.now.in_time_zone.end_of_month.to_date
    jabs.each do |time_of_jab, month_wise_jabs|
      time_of_jab = time_of_jab.to_date rescue Date.today
      month_wise_jabs.each_with_index do |jab, index|
        detail = {}
        detail["vaccination"], detail["vaccination"]["jabs"]  = {}, {}
        if (time_of_jab < today_date)  
          detail = upcoming_jabs_detail_object(jab, index, detail)
          data['Overdue'] << detail
        elsif (time_of_jab >= today_date) && time_of_jab <= month_end_date
          detail = upcoming_jabs_detail_object(jab, index, detail)
          data['This Month'] << detail
        elsif (time_of_jab >= (today_date + 1.month).at_beginning_of_month)  && (time_of_jab <= (today_date + 1.month).at_end_of_month)
          detail = upcoming_jabs_detail_object(jab, index, detail)
          data['Next Month'] << detail
        elsif (time_of_jab > (today_date + 1.month))
          detail = upcoming_jabs_detail_object(jab, index, detail)
          if data[time_of_jab.strftime('%B %Y')].nil?
            data[time_of_jab.strftime('%B %Y')] = []
            data[time_of_jab.strftime('%B %Y')] << detail
          else
            data[time_of_jab.strftime('%B %Y')] << detail
          end
        end
      end
    end
    data.delete_if { |k, v| v.blank? }
  end

  def past_jab_details(vaccinations, data)
    jabs = Jab.in(:vaccination_id=> vaccinations).where(status:"Administered").map{ |j| j["vaccin_date"] = (j.due_on|| j.est_due_time || Time.now);j }
    jabs = jabs.sort_by{|jab| jab["vaccin_date"]}
    jabs = jabs.group_by { |m| (m.due_on || m.est_due_time).beginning_of_month }
    data['This Month'] = []

    #Prepare data for api response.
    jabs.each do |time_of_jab, month_wise_jabs|
      time_of_jab =  Date.today if time_of_jab.blank? 
      month_wise_jabs.each_with_index do |jab, index|
        detail = {}
        detail["vaccination"], detail["vaccination"]["jabs"]  = {}, {}
        if time_of_jab.month != Time.now.month
          detail = upcoming_jabs_detail_object(jab, index, detail, true)
          if data[time_of_jab.strftime('%B %Y')].nil?
            data[time_of_jab.strftime('%B %Y')] = []
            data[time_of_jab.strftime('%B %Y')] << detail
          else
            data[time_of_jab.strftime('%B %Y')] << detail
          end
        elsif time_of_jab.month == Time.now.month
          detail = upcoming_jabs_detail_object(jab, index, detail, true)
          data['This Month'] << detail
        end
      end
    end
    data.delete_if { |k, v| v.blank? }
  end

  def upcoming_jabs_detail_object(jab, index, detail, past=false)
    jab_detail = {}
    vaccin = jab.vaccination
    detail['vaccination']['name'] = vaccin.get_name rescue ""
    detail['vaccination']['category'] = vaccin.category rescue ""
    detail['vaccination']['sys_vaccin_id'] = vaccin.sys_vaccin_id rescue ""
    detail['vaccination']['vaccin_id'] = vaccin.id

    jab_detail['status_text'], jab_detail['status_color_code'] = Vaccination.jab_status_text(jab) rescue ''

    if past && jab.get_jab_status == 'Administered'
      age_diff = ApplicationController.helpers.calculate_age(@member.birth_date, jab.due_on) rescue "0d"
      age_diff = age_diff.gsub("and","").gsub("  "," ")
      jab_detail['child_age'] = age_diff
    end

    jab_detail['name'] = "Jab #{(index + 1)}" rescue ""
    status = jab.get_jab_status
    jab_detail["status"] = status

    jab_detail["date"] = (jab.due_on || jab.est_due_time) rescue ""
    start_time,end_time = Vaccination.start_end_time(jab)
    jab_detail["start_time"] = start_time
    jab_detail["end_time"] = end_time
    jab_detail["fullday"] = jab.fullday
    jab_detail["id"] = jab.id rescue ""
    detail["vaccination"]["jabs"] = jab_detail
    detail
  end

end
