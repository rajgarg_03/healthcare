class Api::V4::MedicalEventController < ApplicationController
  
  # Api
  # Get /prenatal_test/get_recommended_medical_event.json?token=&preg_id=
  # parmas
  def get_recommended_medical_event
    text="Following pregnancy related tests are common in #{current_user.user.country_code}.Let's add!"
    pregnancy = Pregnancy.find(params[:preg_id])
    child = Member.where(id:pregnancy.expected_member_id).first #rescue nil
    data = SysMedicalEvent.recommended_test_name(child)
    render :json=> {:data=> data, :skip_screen_name=>"add_preg_timetime", :status=> StatusCode::Status200}
  end

  #Api
  # Post /prenatal_test/create_new_component.json?token=
  # parmas {:member_id=>"5629d5a65acefe91d2000037", system_component_name="xxxz", category="Blood Test"}
  def create_new_component 
    begin
      # medical_event = MedicalEvent.find(params[:medical_event_id])
      country = current_user.country_code rescue "uk"
      category_title = SysMedicalEvent.where(category:params[:category]).first.category_title
      sys_comp = SysMedicalEvent.or({:member_id=>current_user.id.to_s},{:member_id=>nil})
      sys_comp =  sys_comp.flatten.compact.uniq
      sys_comp = sys_comp.group_by(&:category)[params[:category].to_s] if  params[:category].present?
      sys_comp = sys_comp.sort_by{|i| i[:name]}
      

      sys_medical_event = SysMedicalEvent.create(category_title: category_title, country:country, member_id: current_user.id,recommended:true,category:params[:category],status: "Estimated",name:params[:system_component_name])
       data = []
      if sys_medical_event.save
        status = StatusCode::Status200
        message =Message::API[:success][:create_new_component]
        data = [sys_medical_event] + sys_comp
      else
        status = StatusCode::Status100
        @errors = sys_medical_event.errors
        data = sys_comp
        message =Message::API[:error][:create_new_component]
      end
      render :json=>{:status=> status, :message=>message, :sys_components=>data}
    rescue Exception=>e
      render :json=>{:status=> StatusCode::Status100, :message=>e.message}
    end
  end


  # Api
  # Post /prenatal_test/add_component_to_medical_event.json?token=
  # parmas { medical_event_id="", system_component_id=[]}
  def add_component_to_medical_event 
    begin
      medical_event = MedicalEvent.find(params[:medical_event_id])
      medical_event.component = SysMedicalEvent.in(id:params[:system_component_id]).map(&:name)
      if medical_event.save
        status = StatusCode::Status200
        message = Message::API[:success][:add_component_to_medical_event]
      else
        status = StatusCode::Status100
        message = medical_event.errors
      end
      render :json=>{:status=> status, :message=>message, medical_event:medical_event.to_api_json}
    rescue Exception=>e
      render :json=>{:status=> StatusCode::Status100, :message=>e.message}
    end
  end

  #  Api : Get member Prenatal Tests
  #  GET /prenatal_test.json?token=541ebcb88cd03ddfc4000002&member_id=563c4df55acefed4f4000001&info="past/upcoming"
  def index
    begin
      @member = Member.find(params[:member_id])
      options = {}
      recommended_pointer = ArticleRef.get_recommended_pointer(current_user,@member,"prenatal_test")
      @medical_events =  @member.medical_events.not_in(:opted => "false")
      if params[:info] == "past"
        options = options.merge( {:get_data_for_from_list=>"past"})
        @medical_events = @medical_events.where(:est_due_time.lte=> Date.today)
      end
      if params[:info] == "upcoming"
        options = options.merge({:get_data_for_from_list=>"upcoming"})
        @medical_events = @medical_events.where(:est_due_time.gt=> Date.today)#.includes(:pictures)
      end
      @tool, @show_subscription_box_status,@info_data,@medical_event_data = @member.get_subscription_show_box_data(current_user,"Prenatal Tests",api_version=4,options)
      if @show_subscription_box_status == false
        @medical_events = @medical_events.order_by("est_due_time desc" ).order_by("event_index desc" )
        @medical_events = @medical_events.map(&:to_api_json)
      else
        @medical_events = @medical_event_data
      end
      # @medical_events = @medical_events.order_by("event_index asc" ).order_by("est_due_time asc" )
      @status = StatusCode::Status200
    rescue Exception=>e
      @status =StatusCode::Status100
      @error = e.message
    end
    respond_to do |format|
      format.json {render :json=> {:show_subscription_box=> @show_subscription_box_status, :system_tool_id=> @tool.id,:remommended_pointers=> recommended_pointer, :status => status, :error=>@error, :prenatal_test=>@medical_events} }

    end    
  end
  


  # Api
  # Post /prenatal_test/add_recommended_test.json?token=
  # parmas {:preg_id=>"5629d5a65acefe91d2000037", category=["Blood Test", "Ultrasound Scan Test","Urine Test"]} changed
  # parmas {:preg_id=>"5629d5a65acefe91d2000037", name=["Prenatal test 1", "Ultrasound Scan Test","Urine Test"]}
  def add_recommended_test
    begin
      preg = Pregnancy.find(params[:preg_id])
      member = Member.find(preg.expected_member_id)
      SysMedicalEvent.save_system_prenatal_test(member,params[:name],options={})
      # country_code = User.where(member_id:member.family_mother_father.first.to_s).first.country_code rescue "other"
      # last_medical_event_index =  member.medical_events.order_by("event_index asc" ).last.event_index rescue 0
      # medical_event = member.medical_events.order_by("event_index asc" ).order_by("est_due_time asc" )
      # # saved_test = medical_event.map(&:category)
      # data = SysMedicalEvent.recommended_test_with_component(params[:name],member,country_code)
      # data.each do |test|
      #     last_medical_event_index = test[:name].strip[-1].to_i rescue 
      #     start_time = nil # test[:est_due_date].to_date.to_time1("00:00") rescue nil
      #     end_time = nil # test[:est_due_date].to_date.to_time1("23:30") rescue nil
      #     medical_event = MedicalEvent.new(event_index: last_medical_event_index, end_time: end_time,start_time: start_time, fullday:true, est_due_time: test[:est_due_date], member_id: preg.expected_member_id,opted:true,name:test[:name],category:test[:category],status: "Estimated",component:test[:component])
      #     medical_event.save!
      # end
      status = StatusCode::Status200
      next_screen_name = "preg_handhold_8"
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
    end
    respond_to do |format|
      format.html
      format.js {render :layout=>false}
      format.json {render :json=> {:next_screen_name=> next_screen_name, :status => status, :error=>@error} }
    end 
  end


  # Api: Create new medical event 
  # Post /prenatal_test/
  # params : {:prenatal_test => {note:"", desc:"",name:"",start_time:"",end_time:"",  est_due_time: date, category: category, status: "Planned", component: [sys_comp_ids_array], member_id:} 
  def create

    begin
      params[:prenatal_test][:start_time] = params[:prenatal_test][:start_time].blank? ? "05:30" : params[:prenatal_test][:start_time]
      params[:prenatal_test][:end_time] = params[:prenatal_test][:end_time].blank? ? "23:55" : params[:prenatal_test][:end_time]
      member = Member.find(params[:prenatal_test][:member_id]) 
      last_medical_event_index =  member.medical_events.order_by("event_index asc" ).last.event_index + 1 rescue  1
      params[:prenatal_test][:opted] = true
      component = SysMedicalEvent.in(id:params[:prenatal_test][:component]).map(&:name)
      
      est_time = (params[:prenatal_test][:est_due_time].to_date)
      start_time  = est_time.to_time1(params[:prenatal_test][:start_time])
      end_time  = est_time.to_time1(params[:prenatal_test][:end_time])
      params[:prenatal_test][:fullday] =  params[:prenatal_test][:start_time].blank?
      params[:prenatal_test][:component] = component
      params[:prenatal_test][:end_time] = end_time
      params[:prenatal_test][:start_time] = start_time
      params[:prenatal_test][:name] = params[:prenatal_test][:name] || "Prenatal test #{last_medical_event_index}"
      params[:prenatal_test][:event_index] = last_medical_event_index
      medical_event = MedicalEvent.new(params[:prenatal_test])
      if (start_time < end_time) && medical_event.save
        if params[:media_files].present?
          params[:media_files].each do |pic|
            Rails.logger.info("##### Inside media_files loop #####")
            medical_event.pictures << Picture.new(local_image:pic,upload_class: "medical_event",member_id: member.id)
          end
        end  
        #UserActions.add_update_user_action(medical_event,"PrenatalTest",current_user,"Add")

        Rails.logger.info("##### pictures: #{medical_event.pictures.count} #####")
        @status = StatusCode::Status200
        message = Message::API[:success][:create_medical_event]
        # prenatal_test = medical_event.attributes
        # prenatal_test["pictures"] = medical_event.medical_event_pictures
      else
        @status = StatusCode::Status100
        message = Message::API[:error][:create_medical_event]

        @error = medical_event.errors
      end
      # Add to timeline
      # medical_event.timelines << Timeline.new({name:medical_event.name,desc:"#{medical_event.name} #{MedicalEvent::Medical_Event_State[medical_event.status]} on #{toDate1(medical_event.est_due_time)}",member_id:medical_event.member_id,added_by: current_user.id, timeline_at: Timeline.new.get_timeline_at(medical_event.est_due_time || Date.today) })
    end
    respond_to do |format|
      format.html
      format.js {render :layout=>false}
      format.json {render :json=> {:message=> message, :status => @status, :error=>@error, :prenatal_test=>medical_event.to_api_json }  }
    end  
  end

  # Api: Get  medical event detail
  # Get /prenatal_test/get_medical_event_detail.json?token=asdasdz213qdsa&medical_event_id=12asd213
  # params : {:medical_event_id=>12asd213}
  def get_medical_event_detail
    begin
      medical_event = MedicalEvent.find(params[:medical_event_id])
      status = StatusCode::Status200
      response = {:status => status, :prenatal_test=>medical_event.to_api_json }
    rescue Exception=> e
      status = StatusCode::Status100
      error = e.message
      response = {:status => status, :error => error}
    end
    respond_to do |format|
      format.html
      format.js {render :layout=>false}
      format.json {render :json=> response }
    end  
  end


  # Api: Update medical event 
  # Put /prenatal_test/medical_event_id.json?token=asdasdz213qdsa
  # params : {:prenatal_test => {note:"", desc:"",name:"",end_time:"22:33",start_time:"23:22", est_due_time: date, category: category, status: "Planned", component: [sys_comp_name_array], member_id:Member.last.id} #).save
  def update
    begin
      medical_event = MedicalEvent.find(params[:id])
      medical_event_ref_date = medical_event.est_due_time.to_date
      component = SysMedicalEvent.in(id:params[:prenatal_test][:component]).map(&:name)
      params[:prenatal_test][:component] = component.present? ? component : medical_event.component
      
      if params[:prenatal_test][:start_time].present?
        est_time = (params[:prenatal_test][:est_due_time] || medical_event.est_due_time).to_date
        params[:prenatal_test][:start_time] = params[:prenatal_test][:start_time].blank? ? "05:30" : params[:prenatal_test][:start_time]
        params[:prenatal_test][:end_time] = params[:prenatal_test][:end_time].blank? ? "23:55" : params[:prenatal_test][:end_time]
        start_time  = est_time.to_time1(params[:prenatal_test][:start_time] || "05:30")
        end_time  = est_time.to_time1(params[:prenatal_test][:end_time] || "23:55")
        params[:prenatal_test][:start_time] = start_time
        params[:prenatal_test][:end_time] = end_time
        params[:prenatal_test][:fullday] =  params[:prenatal_test][:start_time].blank?
        raise "Invalid start time " if start_time > end_time
      else
        params[:prenatal_test][:fullday] = true
      end
      medical_event.update_attributes!(params[:prenatal_test])
      # UserActions.add_update_user_action(medical_event,"PrenatalTest",current_user,"Update")

      decode_and_save_image(medical_event,params[:image_data]) if params[:image_data].present?
      if params[:media_files].present?
        params[:media_files].each do |pic|
          medical_event.pictures << Picture.new(local_image:pic,upload_class: "medical_event",member_id: medical_event.member.id)
        end
      end
      status = StatusCode::Status200
      message = Message::API[:success][:update_medical_event]
      medical_event.timelines.delete_all
      if medical_event.status == "Completed"
        medical_event.timelines << Timeline.new({category:medical_event.category, entry_from: "prenatal_test" ,name:medical_event.name ,desc:"Completed on #{toDate1(medical_event.est_due_time)}",member_id:medical_event.member_id,added_by: current_user.id, timeline_at: Timeline.new.get_timeline_at(medical_event.est_due_time || Date.today) })
        medical_event.save

      end
      if medical_event_ref_date != medical_event.est_due_time.to_date
        ArticleRef.update_prenatal_ref(medical_event)
      end
      @delete_picture_ids = params[:deleted_image].split(",").compact rescue []
      @delete_picture_ids.each do |picture_id|
        picture = Picture.where(id: picture_id).first
        picture.destroy if picture
      end
      response = {:status => status, :prenatal_test=>medical_event.to_api_json }
    rescue Exception=> e
      status = StatusCode::Status100
      error = e.message
      message = Message::API[:error][:update_medical_event]
      response = {:status => status, :error => error}
    end
    respond_to do |format|
      format.html
      format.js {render :layout=>false}
      format.json {render :json=> response }
    end  
  end

  def upload_image  
    @medical_event = MedicalEvent.find(params[:id])
    decode_and_save_image(@medical_event,params[:image_data])
    message, status = @picture_saved ? [Message::API[:success][:prenatal_upload_image],StatusCode::Status200] :[Message::API[:success][:prenatal_upload_image],StatusCode::Status100]
    respond_to do |format|
      format.json {render json: {status: status,message:message}}
    end
  end


  #Api : Get all system component detail
  #Get : /prenatal_test/get_sys_components?token=541ebcb88cd03ddfc4000002&category="Blood Test"
  #params: 
  def get_sys_components
    country_code_list = SysMedicalEvent.distinct(:country)
    if country_code_list.present?
      country_code = current_user.get_country_code
      sanitize_country_code = Member.sanitize_country_code(country_code)
      record_exist_for_user_country = SysMedicalEvent.where(:country.in=>sanitize_country_code)
      if record_exist_for_user_country.blank?
        country_code = country_code_list.first
        sanitize_country_code = Member.sanitize_country_code(country_code)
      end
    else
      sanitize_country_code = []
    end
    sys_comp = []
    sys_comp << SysMedicalEvent.where(:member_id=>current_user.id.to_s)
    if country_code_list.present?
      sys_comp << SysMedicalEvent.where(:recommended=>"true").in(:member_id=>nil).where(:country.in=>sanitize_country_code)
      sys_comp << SysMedicalEvent.where(:recommended=>"false").in(:member_id=>nil).where(:country.in=>sanitize_country_code)
    else
      sys_comp << SysMedicalEvent.where(:recommended=>"true").in(:member_id=>nil)
      sys_comp << SysMedicalEvent.where(:recommended=>"false").in(:member_id=>nil)
    end 
    # sys_comp = SysMedicalEvent.or({:member_id=>current_user.id.to_s},{:member_id=>nil}).entries
    sys_comp =  sys_comp.flatten.compact.uniq
    sys_comp = sys_comp.group_by(&:category)[params[:category].to_s] if  params[:category].present?
    sys_comp = sys_comp.sort_by{|i| i[:name]}
    render :json=>{:sys_components =>sys_comp, :status=> StatusCode::Status200}
  end
  
  # Api: delete component from medical event
  # Delete /prenatal_test/delete_component_from_medical_event
  # params : { :medical_event_id=>"sdasda213as", medical_event_component: "name"} 
  def delete_component_from_medical_event
    begin
      medical_event = MedicalEvent.find(params[:medical_event_id])
      medical_event.component.delete(params[:medical_event_component])
      medical_event.save
      message = Message::API[:success][:delete_component]
      status = StatusCode::Status200
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
      message = Message::API[:error][:delete_component]
    end
    respond_to do |format|
      format.html
      format.js {render :layout=>false}
      format.json {render :json=> {:status => status, :message => message, :error=>@error, :prenatal_test=>medical_event.to_api_json} }
    end 
  end

  # Api: delete component from System
  # Delete /prenatal_test/delete_component_from_medical_event
  # params : { :sys_comp_id => "1zxc1as2zxvt" }
  def delete_component_from_sys
    begin
      medical_event = SysMedicalEvent.where(id:params[:sys_comp_id],member_id:current_user.id).first
      medical_event.delete
      status = StatusCode::Status200
      message = Message::API[:success][:delete_component_from_system]
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
      message = Message::API[:error][:delete_component_from_system]
    end
    respond_to do |format|
      format.html
      format.js {render :layout=>false}
      format.json {render :json=> {:status => status, :message => message, :error=>@error} }
    end 
  end

  # Api: delete Parental Test from member list
  # Delete /prenatal_test/delete_parental_test
  # params : { :medical_event_id => "1zxc1as2zxvt" }
  def delete_parental_test
    begin
      medical_event = MedicalEvent.find(params[:medical_event_id])
      medical_event.delete_medical_event
      status = StatusCode::Status200
      message = Message::API[:success][:delete_parental_test]
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
      message = Message::API[:error][:delete_parental_test]
    end
    respond_to do |format|
      format.html
      format.js {render :layout=>false}
      format.json {render :json=> {:status => status, :message => message, :error=>@error} }
    end 
  end

end
