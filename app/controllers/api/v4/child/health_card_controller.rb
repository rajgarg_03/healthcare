class Api::V4::Child::HealthCardController < ApiBaseController
  
  #http://localhost:3000/api/v4/child/health_card.json?token=gcjemgbLpVAjbjV4jyzs&member_id=58eb64cff9a2f36bc0000072
  def index
    begin
      member_id = params[:member_id]
      
      options = {}
      child =  ChildMember.find(member_id)
      measurement = Health.get_latest_height_weight(current_user,child).get_unit_converted_record(current_user) rescue nil
      
      allergies = Child::HealthCard::Allergy.get_list(current_user,member_id) 
      
      recommended_pointer = nil #ArticleRef.get_recommended_pointer(current_user,child.id,"health")
      eye_chart = Child::HealthCard::EyeChart.get_list(member_id)
      eye_chart_data = Child::HealthCard::EyeChart.order_by_date(member_id).first
      eye_chart_data = {} if eye_chart_data.nil? 
      last_records = { eye_chart: eye_chart.first, allergy: allergies.last }
      @tool, @show_subscription_box_status,@info_data,health_card_widget = child.get_subscription_show_box_data(current_user,"Child Health Card",api_version=4,options)
      if @show_subscription_box_status == false
        health_card_widget = []
        health_card_widget << { eye_chart: eye_chart_data }
        health_card_widget << { allergies: allergies }
      end
      summary =  {:height=>(measurement["height"] rescue nil) ,:weight=>(measurement["weight"] rescue nil),:bmi=>(measurement["bmi"].to_f rescue nil),:blood_group=>child.blood_group }
      skin_assessment_tool_id = ::Child::SkinAssessment.get_tool_id

      record_exist  = measurement.present? || allergies.present? || eye_chart_data.present?
      show_skin_assessment_flag = get_skin_assessment_active_status(record_exist, tool_for="child", feature_type="all") 

      render :json => {:show_subscription_box=> @show_subscription_box_status, :system_tool_id=> @tool.id, :show_skin_assessment_flag=> show_skin_assessment_flag, :skin_assessment_tool_id=>skin_assessment_tool_id, last_records: last_records, health_card_widget: health_card_widget , recommended_pointers:recommended_pointer,:status=>200, :summary=>summary  }
    rescue Exception=> e 
      render :json => {:status=>100, :message=>e.message  }
    end
  end

  #http://localhost:3000/api/v4/child/health_card/list_allergies.json?token=gcjemgbLpVAjbjV4jyzs&member_id=59b00354bf42279ad0000006 
  def list_allergies
    begin
      member_id = params[:member_id]
      member = Member.find(member_id)
      options = {:get_data_for_from_list=>"Allergies"}
      @tool, @show_subscription_box_status,@info_data,allergies = member.get_subscription_show_box_data(current_user,"Child Health Card",api_version=4,options)
      if @show_subscription_box_status == false
        allergies = Child::HealthCard::Allergy.get_list(current_user, member_id) 
      end
      render :json => {system_tool_id:@tool.id,show_subscription_box:@show_subscription_box_status, allergies: allergies, status: 200 }
    rescue Exception=> e 
      render :json => {:status=>100, :message=>e.message  }
    end
  end

  # put
  def update_blood_group
    begin
      member =  Member.find(params[:member_id])
      member["blood_group"] = params[:blood_group].gsub(" ","+")
      member.save
      message = Message::API[:success][:update_blood_group]
      status =StatusCode::Status200
    rescue Exception=>e 
      status =StatusCode::Status100
      message = Message::API[:error][:update_blood_group]
    end
    render :json=>{:status=> status, :message=>message, :member=>member}
   end
  private 

  def get_skin_assessment_active_status(record_exist,tool_for="child", feature_type="all")
    skin_assessment_tool_id = ::Child::SkinAssessment.get_tool_id
    @api_version = 4
    if record_exist.present?
      status = Nutrient.is_tool_available?(skin_assessment_tool_id,current_user,@api_version,tool_for,feature_type)
    else
      status = false
    end
  end
end
