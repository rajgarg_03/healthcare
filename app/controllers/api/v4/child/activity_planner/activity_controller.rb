class Api::V4::Child::ActivityPlanner::ActivityController < ApiBaseController
  
  
  # Get
  #http://localhost:3000/api/v4/child/activity_planner/activity/list_all_activity.json?token=AuSnoCzp2uceDGA1JGqb&member_id=59004378f9a2f37963000002
  #params = {:member_id=>,category=>"Club/Play Date/Birthday Party"}
  def list_all_activity
    member_ids = params[:member_id].present? ? [params[:member_id]] : current_user.childern_ids
    if params[:category].present?
      all_activities = Child::ActivityPlanner::Activity.where(category:params[:category]).in(member_id:member_ids)
    else
      all_activities = Child::ActivityPlanner::Activity.in(member_id:member_ids)
    end
    if params[:past_activity].present?
      all_activities = all_activities.where(:start_time.lt=>Time.now.in_time_zone.to_date).order_by("start_time desc")
       
    elsif params[:upcoming_activity].present?
      all_activities = all_activities.where(:start_time.gte=>Time.now.in_time_zone.to_date).order_by("start_time desc")
    else
      all_activities = all_activities.order_by("start_time desc")
    end

    activity_list = Child::ActivityPlanner::Activity.month_wise_data(all_activities)
    month_order = activity_list.keys
    status = StatusCode::Status200
    render :json=>{ :activities => activity_list, :status=>status, :month_order=>month_order}
  end
  
  # Post
  # http://localhost:3000/api/v4/child/activity_planner/activity/add_activity.json?token=gcjemgbLpVA
  # params = {:activity_planner=>{:title=> "activity_name", :participants=> ["a","c","d"], :category=> nil, :start_time=> nil, :end_time=> nil, :location_name=> nil, :location_address=>nil, :member_id=> nil, :activity_date=> "2017-10-24"}}
  def add_activity
    begin
      param_data = params[:activity_planner] 
      time_zone = current_user.time_zone
      start_time = ActiveSupport::TimeZone[time_zone].parse("#{param_data[:activity_date]} #{param_data[:start_time]}").utc 
      end_time = ActiveSupport::TimeZone[time_zone].parse("#{param_data[:activity_date]} #{param_data[:end_time]}").utc 
      if   start_time < end_time
        param_data[:start_time] = start_time
        param_data[:end_time] = end_time
        #param_data[:participants] = Child::ActivityPlanner::Participant.in(id:param_data[:participants].uniq).pluck(:name) rescue []
        Child::ActivityPlanner::Activity.create(param_data )
        status = StatusCode::Status200
        message = "Activity added successfully"
      else
        raise "Please select valid time."
      end
    rescue Exception=>e 
      @error = e.message
      message = "Unable to add activity"
      status = StatusCode::Status100
    end
    render :json=>{ :error=>@error,message:message,:status=>status}
  end 
  
  # Put
  # http://localhost:3000/api/v4/child/activity_planner/activity/update_activity.json?token=gcjemgbLpVA
  # params = {:activity_id=> , :activity_planner=>{:participants=> ["a","c","d"], :category=> nil, :start_time=> nil, :end_time=> nil, :location_name=> nil, :location_address=>nil,:activity_date=> "2017-10-24"}}
  def update_activity
    begin
      param_data = params[:activity_planner] 
      time_zone = current_user.time_zone
      start_time = ActiveSupport::TimeZone[time_zone].parse("#{param_data[:activity_date]} #{param_data[:start_time]}").utc 
      end_time = ActiveSupport::TimeZone[time_zone].parse("#{param_data[:activity_date]} #{param_data[:end_time]}").utc 
      if   start_time < end_time
        param_data[:start_time] = start_time
        param_data[:end_time] = end_time
        activity = Child::ActivityPlanner::Activity.find(params[:activity_id])
        activity.update_attributes(param_data )
        status = StatusCode::Status200
        message = "Activity added successfully"
      else
        raise "Please select valid time."
      end
    rescue Exception=>e 
      @error = e.message
      message = "Unable to add activity"
      status = StatusCode::Status100
    end
    render :json=>{ :error=>@error,message:message,:status=>status}
  end

  # Delete
  # http://localhost:3000/api/v4/child/activity_planner/activity/delete_activity.json?token=gcjemgbLpVA&participant_id=
  # params= {:activity_id=>}
  def delete_activity
    begin
      activity = Child::ActivityPlanner::Activity.where(id:params[:activity_id]).delete_all
      status = StatusCode::Status200
      message = "Activity deleted successfully"

    rescue Exception=>e 
      @error = e.message
      message = "Unable to delete activity"
      status = StatusCode::Status100
      
    end
    render :json=>{ :error=>@error,message:message,:status=>status }
  end
 


  # Post
  # http://localhost:3000/api/v4/child/activity_planner/activity/add_participant.json?token=gcjemgbLpVA
  # params = {:name=> "participant name"}
  def add_participant
    begin
      Child::ActivityPlanner::Participant.create(name:params[:name],user_id:current_user.user_id)
      status = StatusCode::Status200
      message = "Participant added successfully"
      participants = Child::ActivityPlanner::Participant.get_participant_list(current_user.user_id,params[:activity_id])
    rescue Exception=>e 
      @error = e.message
      message = "Unable to add participant"
      status = StatusCode::Status100
      participants = []
    end
    render :json=>{ :error=>@error,message:message,:status=>status,:participants=>participants}
  end


  # Delete
  # http://localhost:3000/api/v4/child/activity_planner/activity/delete_participant.json?token=gcjemgbLpVA&participant_id=
  # params= {:participant_id=>}
  def delete_participant
    begin
      participant = Child::ActivityPlanner::Participant.where(id:params[:participant_id]).delete_all
      status = StatusCode::Status200
      message = "Participant deleted successfully"
      participants = Child::ActivityPlanner::Participant.get_participant_list(current_user.user_id,params[:activity_id])

    rescue Exception=>e 
      @error = e.message
      message = "Unable to delete participant"
      status = StatusCode::Status100
      participants = []
    end
    render :json=>{ :error=>@error,message:message,:status=>status,:participants=>participants}
  end

  # Get
  # http://localhost:3000/api/v4/child/activity_planner/activity/list_participant.json?token=gcjemgbLpVA
  # params = {:activity_id=> ""}
  def list_participant
    begin
      participants = Child::ActivityPlanner::Participant.get_participant_list(current_user.user_id,params[:activity_id])
      status = StatusCode::Status200
    rescue Exception=>e 
      @error = e.message
      status = StatusCode::Status100
    end
    render :json=>{ :error=>@error, :participants=> participants, :status=>status}
  end

   
end
