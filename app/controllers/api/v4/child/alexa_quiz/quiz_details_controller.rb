class Api::V4::Child::AlexaQuiz::QuizDetailsController < ApiBaseController

	# Get
  #http://localhost:3000/api/v4/child/alexa_quiz/quiz_details/update_quiz_level.json?token=AuSnoCzp2uceDGA1JGqb&member_id=59004378f9a2f37963000002
  #params = {:child_id=>,questions=>[{}],current_quiz_level, :next_quiz_level=>}
  def update_quiz_level
    begin
    child = ChildMember.find(params[:child_id])
    Child::AlexaQuiz::QuizDetail.save_quiz_detail(current_user,params,api_version=4)
    message = Message::API[:success][:update_quiz_level] %{:child_name=>child.first_name}
    status = StatusCode::Status200
    rescue Exception=> e
      status = StatusCode::Status100
      @error = e.message
      message = Message::API[:error][:update_quiz_level]
    end
    render :json=>{ :status=>status, :message=>message ,:error=> @error}
  end

end
