 class Api::V4::SubscriptionController < ApiBaseController
  before_filter :set_api_version
  skip_before_filter :token_verify , :only=>[:update_subscription_receipt,:update_subscription_from_apple_server,:update_subscription_from_play_store]
  # list all subscription of user
  # GET http://localhost:3000/api/v3/subscription.json?token=XrjzH88mP4syK1q4MTeb&family_id=546128200a9a99178300000d
  def index
    begin  
      api_version = @api_version
      subscriptions = Subscription.find_family_member_subscription(current_user.id.to_s,params[:family_id]).entries
      status = StatusCode::Status200
      data = []
      subscriptions.each do |subscription|
        data << Subscription.formatted_data_for_api(subscription,params[:family_id],subscription,current_user,api_version)
      end
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
    end
    respond_to do |format| 
      
      format.json {render :json=>{ :subscriptions=> data, :status=> status, :errors => @error} }
    end
  end

  # Get all tools for subscribed subscription
  # GET http://localhost:3000/api/v4/subscription/subscriptions_tool_list.json?token=AuSnoCzp2uceDGA1JGqb
  def subscriptions_tool_list
    begin 
      api_version = @api_version
      basic_tools,premium_tools = Subscription.tool_list(current_user,api_version) 
      status =StatusCode::Status200
      data = {:basic_tools=> basic_tools}
      data.merge!({:premium_tools=>premium_tools}) if premium_tools.present?
      
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
    end
    data.merge!({:status=> status, :errors => @error})
    respond_to do |format| 
      format.json {render :json=> data}
    end
  end

  def update_subscription_from_apple_server
    Rails.logger.info "Request from Apple server"
    # Nothing to to if initail buy
    if !(params[:notification_type] == "INITAIL_BUY" ||  params[:notification_type] == "INTERACTIVE_RENEWAL")
      receipt_data = params[:latest_receipt]
      latest_receipt_info = params[:latest_receipt_info]
      SubscriptionReceipt::Ios.update_receipt(receipt_data,latest_receipt_info,source="apple_server")
    end
    respond_to do |format| 
      format.json {render :json=> {:status=>200}}
    end
  end

  def update_subscription_from_play_store
    Rails.logger.info "Request from Play Store"
    play_store_plain_info = JSON.parse(Base64.decode64(params[:message][:data]) ).with_indifferent_access
    if play_store_plain_info[:subscriptionNotification][:notificationType].to_i != 4
      receipt_data = {}
      receipt_data[:purchase_token] = play_store_plain_info[:subscriptionNotification][:purchaseToken]
      receipt_data[:product_id] = play_store_plain_info[:subscriptionNotification][:subscriptionId]
      SubscriptionReceipt::Android.update_receipt(receipt_data,source="play_store")
    end
    respond_to do |format| 
      format.json {render :json=> {:status=>200}}
    end
  end
  

  # Get all tools for subscribed subscription in flat structure
  # GET http://localhost:3000/api/v4/subscription/get_tool_list.json?token=AuSnoCzp2uceDGA1JGqb
  def get_tool_list
    begin 
      flat_structure = true
      api_version = @api_version
      options = {}
      options[:show_all_tool] = params[:show_all_tools]
      options[:member_id] = params[:member_id]
      data = Subscription.tool_list(current_user,api_version,flat_structure,params[:tool_id],options) 
      status =StatusCode::Status200
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
    end
    respond_to do |format| 
      format.json {render :json=> {:tools=> data,:status=> status, :errors => @error} }
    end
  end

  
  # PUT http://localhost:3000/api/v2/subscription/create_or_update_subscription.json?token=AuSnoCzp2uceDGA1JGqb
  # params = {:transaction_id=>,:buyer_name=>"Raej" :receipt_data=>"base64 string",:platform=>"iOS",:family_id=> "57c02117f9a2f3c164000005", :subscription_manager_id:"584ec190f9a2f37a5a000003"}
  # receipt_data=>{"product_id"=>,"purchase_token"=>} for android
  # buyer_name :optional
  # upgrade subscription or create new subscription if not subscribed for any after validate receipt.
  def create_or_update_subscription
    begin
      params[:ip_address] = request.remote_ip
      api_version = @api_version
      # validate family exist
      Family.find(params[:family_id]) rescue raise "Invalid family"
      raise "Invalid device platform. Please verify." if !["ios","android"].include?(params[:platform].to_s.downcase)
      # validate  subscription receipt 
      is_valid, response ,status_code = Subscription.validate_subscription_receipt(params[:receipt_data],params[:secret_key],params[:platform],{:original_transaction_id=>params[:original_transaction_id],:family_id=>params[:family_id]})
      member = current_user
       # get subscribed subscription if it is
      subscribed_plan = Subscription.family_free_or_premium_subscription(params[:family_id]) || Subscription.add_default_subscription(current_user,params[:family_id],params[:platform],@api_version)
      # check recipt is valid and user can subscribed for plan 
      raise "Receipt is invalid. Please verify receipt." if !is_valid
      is_valid_user_to_update, validation_error_msg = Subscription.valid_user_to_subscribe_plan_for_family(current_user.id,params[:family_id],params[:platform].to_s.downcase, check_platform=true,api_version)
      if is_valid && response.present? && is_valid_user_to_update
        requested_subscription_manager = SubscriptionManager.valid_subscription_manager(current_user,@api_version).where(id:params[:subscription_manager_id]).last
        subscribed_plan_manager = SubscriptionManager.valid_subscription_manager(current_user,@api_version).where(:id=>subscribed_plan.subscription_manager_id).last rescue nil # if no subscription taken
         # check if requested subscription is higer then subscribed subscription
            # check if subscribed user is upgrading subscription
          if (subscribed_plan && (subscribed_plan.subscription_listing_order(subscribed_plan_manager) > 0) && subscribed_plan.member_id.to_s != current_user.id.to_s)
          status_code = StatusCode::Status401
          raise Message::Subscription[:not_valid_user_to_update] 
          
          end
          valid_user_to_subscribe = true
          subscription_manager = requested_subscription_manager
          params[:receipt_end_date] = Time.at(response["expires_date_ms"].to_f/1000).to_date  
          params[:receipt_response] = response
          if subscribed_plan.present?
            # update subscription
            count = (subscribed_plan.renew_count.to_i + 1) rescue 0
            subscribed_plan.update_subscription(subscription_manager,@api_version,current_user,params[:family_id],params[:platform],params)
            subscribed_plan.reload
          else
             # create subscription if not already
             count = 0
            subscription_manager  = requested_subscription_manager
            # delete user default subscription  
            Subscription.where(member_id:member_id).delete_all
            requested_subscription = Subscription.create_subscription(subscription_manager, @api_version,current_user,params[:family_id],params[:platform],params)
          end
          requested_subscription = subscribed_plan
          subscription_receipt_info = requested_subscription.create_subscription_receipt(params[:receipt_data], params[:secret_key],{:family_id=>params[:family_id],:platform=>params[:platform],:response=>response,:status_code=>status_code,:buyer_name=>params[:buyer_name]})
          raise "subscription receipt could not saved" if subscription_receipt_info.blank?
          subscription_detail = Subscription.formatted_data_for_api(requested_subscription,params[:family_id],subscribed_plan,current_user,api_version,{:tool_require_subscription_level=>0,:valid_user_to_subscribe=>valid_user_to_subscribe,:platform=>params[:platform]})
          # send email and Push notification to subscriber and family member
          # Handled by instant push notification setting notify by -"both"
          batch_manager = BatchNotification::BatchNotificationManager.find_active_notification("17")
          if batch_manager.blank? || batch_manager.notify_by == "push"
            UserMailer.delay.subscription_taken(current_user,params[:family_id],requested_subscription)
          end
          InstantNotification::Subscription.notification_for_identifier_17("17",current_user,params[:family_id],options={:subscription=>requested_subscription})
          
          status =StatusCode::Status200
          message = Message::Subscription[:success]
      else
        status_code = StatusCode::Status401
        status = status_code
        raise (validation_error_msg || Message::Subscription[:not_valid_user_to_update])
      end
      
    
      member_user_families = current_user.user_families
      api_version = @api_version
      data =  {:subscription_receipt_info=>subscription_receipt_info, :families=>JSON.parse(Family.family_to_json(member_user_families,api_version,current_user) ), :user_basic_info=> User.user_basic_info(member,member_user_families), :message=> message, :subscription=> subscription_detail, :status=> status, :errors => @error} 
    rescue Exception=>e
      @error = e.message
      status = status_code || StatusCode::Status100
      message = e.message
      data = {:subscription_receipt_info=>subscription_receipt_info, :message=> message, :subscription=> subscription_detail, :status=> status, :errors => @error}
    end
      render :json=> data

  end 
  
  # Get 
  # api/v4/subscription/get_available_subscriptions.json?token=AuSnoCzp2uceDGA1JGqb
  # api/v4/subscription/get_available_plans.json?token=AuSnoCzp2uceDGA1JGqb
  def get_available_plans
    subscriptions = SubscriptionManager.valid_subscription_manager(current_user,@api_version).entries
    message = ""
    status =StatusCode::Status200
    render :json=>{:message=> message, :subscription_manager=> subscriptions, :status=> status} 

  end
  

  def unsubscribe_from_subscription
    subscription = Subscription.find_family_member_subscription(params[:subscriber_id], params[:family_id]).where(:subscription_manager_id=>params[:subscription_manager_id]).last
    subscription.delete_subscription
    respond_to do |format|
      format.json
    end
  end 

  # update_subscription_receipt
  # params = {receipt_data:"", platform:"ios"}
  # receipt_data = {"product_id"=>,"purchase_token"=>}/{family_id=>, :member_id=>} for android
  # receipt_data = "hashencryptedstring" for ios
  def update_subscription_receipt
    platform = params[:platform].downcase rescue nil
    receipt_data = params[:receipt_data]
    raise "Invalid platform for subscription" if platform.blank?
    raise "Invalid receipt for subscription" if receipt_data.blank?
    begin
       # status,response,status_code = Subscription.validate_subscription_receipt(receipt_data,key=nil,platform)
      if platform == "ios"
        # subscription_receipt = SubscriptionReceipt::Ios.where(receipt_data:receipt_data).last
         status  = SubscriptionReceipt::Ios.update_receipt(receipt_data)
      end
      if platform == "android"
        if receipt_data[:purchase_token].blank?
          # reload android subscription from client 
          if receipt_data[:family_id].present? && receipt_data[:member_id].present?
            subscription_receipt = SubscriptionReceipt::Android.where(member_id:receipt_data[:member_id], family_id:receipt_data[:family_id]).last
            receipt_data = {:product_id=>subscription_receipt.product_id,:purchase_token=>subscription_receipt.purchase_token}
            status =  SubscriptionReceipt::Android.update_receipt(receipt_data)
          else
            raise "Invalid info for user family subscription"
          end
        else
          status =  SubscriptionReceipt::Android.update_receipt(receipt_data)
        end
       # subscription_receipt.receipt =  receipt_data 
       # subscription_receipt.save
      end
      raise "Invalid receipt" if (status == StatusCode::Status100 || status == false)
      status = StatusCode::Status200
      message = "Receipt updated"
    rescue Exception=>e 
      status = StatusCode::Status100
      @error = e.message
      message = "unable to update receipt"
    end
    render :json=>{:message=> message, :error=> @error, :status=> status} 
  end
  

  # API-32
  #api Get /api/v2/subscription/verify_subscription.json?tool_id=58578d33f9a2f3b79e000006&token=AuSnoCzp2uceDGA1JGqb&family_id=5858e7aaf9a2f300cb000007&member_id=5858e8e1f9a2f300cb000045
  #tool_id : optional
  def verify_subscription
    response = SubscriptionManager.verify_subscription(current_user,params,@api_version)
    render :json=> response
  end
 
private
  def set_api_version
    @api_version = 4
  end
 end  