 class Api::V2::SystemMilestonesController < Api::V1::SystemMilestonesController
  
  def index
  	@member = Member.where(id: params[:member_id]).first
    @system_milestones = SystemMilestone.all
    respond_to do |format|
      # format.json { render :json => @system_milestones.group_by(&:category).to_json(methods: [:added]) }
      format.json
    end 
  end

  def hh_milestones
    begin
      @member = Member.find(params[:id]) rescue ChildMember.last
      @system_milestones = HandHold.child_hh_milestones(@member)

      status = StatusCode::Status200
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
      @system_milestones = {}
    end
    render :json=>{ :status=> status, :error=>@error, :system_milestones=>@system_milestones[:system_milestones]}
    # @system_milestones = SystemMilestone.system_milestone_for_member(@member)
    # respond_to do |format|
    #   format.json 
    # end
  end

end