class Api::V2::HomeController < HomeController
  before_filter :authenticate_user!, :except => [:unsubscribe, :login, :messages,:team,:faq, :get_geoname, :index, :about, :contact, :enquiry, :terms, :privacy, :copyright, :cookies, :accept_join_family_request, :decline_join_family_request]

  def index    
    @resource = User.new
    @invitation = Invitation.find(session[:invitation_id]) if session[:invitation_id]
    if current_user
      redirect_to dashboard_path and return
    end
    render layout: false
  end

  def login
    render layout: false
  end

  def messages
    if params[:forgot_password] == "true"
      @message = Message::GLOBAL[:success][:forgot_password].html_safe #"You will receive an email with instructions about how to reset your password in a few minutes.<br/><a href='/users/sign_in' class='btn btn-type2 login_btn'>Login</a>"
    elsif params[:status] == "unapproved"
      @message = "Kindly verify your email ID. Please check the email we sent to you.<br/><a href='/resend_confirmation_link?email=#{session[:resend_verify_email]}' class='btn btn-type2 login_btn'>Resend Email</a>".html_safe
    elsif params[:verify_email_sent] == "true"
      @message = "Please verify your email ID through the link we have emailed to you. Do check your spam folder, if you haven't received the email.<br/><a href='/users/sign_in' class='btn btn-type2 login_btn'>Login</a>&nbsp;&nbsp;&nbsp;<a href='/resend_confirmation_link?email=#{session[:resend_verify_email]}' class='btn resend-email-link' style='vertical-align:sub' >Resend Email</a>".html_safe
    elsif params[:email_resent] == "true"
      @message = "We have resent the verification email to you. Do check your spam folder, if you haven't received the email.<br/><a href='/users/sign_in' class='btn btn-type2 login_btn' >Login</a>&nbsp;&nbsp;&nbsp;<a href='/resend_confirmation_link?email=#{session[:resend_verify_email]}' class='resend-email-link btn' style='vertical-align:sub'>Resend Email</a>".html_safe
    end
    render layout: "login"  
    # layout "login"
  end  

  def unsubscribe
    @member = Member.where(id:params[:sub_id]).first || current_user
    mail_setting = MemberSetting.where(member_id:@member.id, name:"mail_subscribe").first
    mail_setting = mail_setting || MemberSetting.create(member_id:@member.id, name:"mail_subscribe",note:MemberSetting::MailerType.keys.join(","),status: "subscribe")
    @subscribe_mail_ids = mail_setting.note.split(",")
    if params[:resubscribe] == "true"
      subscription_id = (@subscribe_mail_ids + [params[:mtype]]).uniq
      setting_val = true
    else
      setting_value = false
      subscription_id = @subscribe_mail_ids - [params[:mtype]]
    end
    @member.subscribe_mail(subscription_id)
    respond_to do |format|
      format.html { render layout: "login"  }
      format.js
      format.json { render :json=>{:status=>status, mailer_setting: setting_val, :message=>Message::GLOBAL[:success][:unsubscribe]}}
    end
  end
  
  def terms 
  end

  def privacy 
  end

  def team
  end

  def copyright
  end

  def cookies
  end  
  
  def faq
    render layout: "home"
  end
  def youtube_video
    render :partial=>"youtube_video", :layout=>false
  end
  def vimeo_video
    @video_link = params[:link]
    render :partial=>"vimeo_video", :layout=>false

  end
  def about
    @resource = User.new
  end

  def contact
    # @resource = User.new
    redirect_to "/" and return true

  end

  def get_geoname
    begin
    gids = params[:gid].split(",")
    all_data = {"totalResultsCount" => 0, "geonames" => []}
    gids.each do |gid|
      url = "http://www.geonames.org/childrenJSON?geonameId=#{gid}&style=long"
      data = JSON.parse(Net::HTTP.get_response(URI.parse(url)).body)
      all_data["totalResultsCount"] += (data["totalResultsCount"] rescue 0)
      all_data["geonames"] += (data["geonames"] rescue [])
    end
  rescue
    all_data
  end
    render :json => all_data
  end  

  # def enquiry
  #   @success = false
  #   @enquiry = Enquiry.new(params[:enquiry])
  #   if @enquiry.save
  #     @success = true
  #   end
  #   puts @enquiry.errors.to_a
  # end

  def popup_url
    render :partial=> params[:url], params[:family_member_id] => params[:family_member_id]
  end

  def accept_join_family_request
    @invitation = Invitation.find(params[:invitation_id])
    @invitation.accept
    @family_member = FamilyMember.new(:member_id => @invitation.member.id, :family_id => @invitation.family_id, :role => @invitation.role)
    if @family_member.save
      @user = @invitation.family.owner.user
      if @user && @user.status == "unconfirmed"
        @user.status = "confirmed"
        @user.save
      end
      session[:api_token] = @user.confirmation_token if @user.present? # == current_user.user
      session[:api_invitation] = @invitation.id.to_s
      session[:api_email] = @user.email rescue nil
      session[:api_invitation_status] = "active"
      add_notification(@invitation.member, "Your request to join #{@invitation.family.name} has been #{@invitation.status} by #{@invitation.family.owner.first_name}.", families_path, @invitation.family.owner,"family")
    end
    respond_to do |format|
      sign_out if user_signed_in? && @invitation.family.owner != current_user
      format.html {redirect_to (current_user.present? ? dashboard_path : "/users/sign_in"), notice: "Invitation #{@invitation.status}"}
    end
  end

  def decline_join_family_request
    @invitation = Invitation.find(params[:invitation_id])
    @invitation.reject
    @user = @invitation.family.owner.user
    if @user && @user.status == "unconfirmed"
      @user.status = "confirmed"
      @user.save
    end
    session[:api_token] = @user.confirmation_token if @user.present? # == current_user.user
    session[:api_invitation] = @invitation.id.to_s
    session[:api_email] = @user.email rescue nil
    session[:api_invitation_status] = "active"
    add_notification(@invitation.member, "Your request to join #{@invitation.family.name} has been #{@invitation.status} by #{@invitation.family.owner.first_name}.", families_path, @invitation.family.owner,"family")
    respond_to do |format|
      sign_out if user_signed_in? && @invitation.family.owner != current_user
      format.html {redirect_to (current_user.present? ? dashboard_path : "/users/sign_in"), notice: "Invitation #{@invitation.status}"}
    end
  end  


  def notification
    notification = Notification.find(params[:id])
    notification.update_attribute(:checked, true) unless notification.blank?
    redirect_to notification.blank? ? root_path : notification.link
  end
end
