class Api::V2::PregnancyController < PregnancyController
  
  # API: Get pregnancy info
  # Get "/pregnancy/get_preg_info.json?token=iBSqRyQ5pxuxVb1dSz47"
  # parmas = {:preg_id=>"asdasd2123123"}}
  def get_preg_info
    begin
      preg = Pregnancy.find(params[:preg_id])
      born_child = Member.in(id:preg.added_child).last || Member.find((preg.expected_member_id) ) rescue nil
      preg["born_on"] = born_child.birth_date    rescue preg.expected_on.to_date
      recommended_pointer = ArticleRef.get_recommended_pointer(current_user,born_child,"pregnancy")

      status = StatusCode::Status200
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
    end
      render :json=> {:remommended_pointers=>recommended_pointer,:error =>@error,status: status, pregnanacy:preg}

  end

  # API : Update pregnancy details
  # PUT "/pregnancy/update_preg.json?token=iBSqRyQ5pxuxVb1dSz47"
  # parmas = {:preg_id=> "5644443d5acefe40b4000020", :pregnancy=> {:family_id=>"12123", :name =>"abc", :expected_on=>"1-3-2015",preg_member_id=>"asdasd2123123"}}
  def update_preg
    begin
      preg = Pregnancy.find(params[:preg_id])
      name = preg.name
      birth_date = preg.expected_on
      params["pregnancy"][:member_id] = params["pregnancy"].delete(:preg_member_id) if params["pregnancy"]["preg_member_id"].present?
      preg.update_attributes(params[:pregnancy])
      updated_birth_date = (params[:pregnancy][:expected_on] || preg.expected_on)

      member_ids = ([preg.expected_member_id] << preg.added_child).flatten.compact
      members = Member.in(id:member_ids)
      members.first.update_attributes(first_name:preg.name,birth_date:updated_birth_date,skip_birth_date_validation: true)
      if params[:pregnancy][:expected_on].present? && params[:pregnancy][:expected_on].to_date != birth_date
         members.first.update_member_ref("update",{:birth_date_updated=>true})
      elsif params[:pregnancy][:name].present? && params[:pregnancy][:name] != name
        members.first.update_member_ref("update",{:name_updated=>true})
      else
      end  

      if (params[:pregnancy][:family_id].present? rescue false)
        members.each do |member|
          fm = member.family_members.first.update_attribute(:family_id,params[:pregnancy][:family_id]) rescue nil
        end
      end
      status = StatusCode::Status200
      message = Message::API[:success][:update_preg] rescue "success_update_preg"

    rescue Exception=>e
      status = StatusCode::Status100
      message = Message::API[:error][:update_preg] rescue "error_update_preg"
      @error = e.message
    end
    render :json => {:message=> message, :status=> status, :error=> @error, :pregnancy=> preg}
  end

  # API
  # POST "/pregnancy/create_preg.json?token=iBSqRyQ5pxuxVb1dSz47"
  # parmas = {:pregnancy=> {:family_id=>"12123", :name =>"abc", :expected_birth_date=>"1-3-2015",preg_member_id=>"asdasd2123123"}}
  def create_preg
    begin
      family = Family.find(params[:pregnancy][:family_id])
      pregnancy = Pregnancy.new(member_id:params[:pregnancy][:preg_member_id], name:params[:pregnancy][:name], expected_on:params[:pregnancy][:expected_birth_date], added_by: current_user.id,family_id:params[:pregnancy][:family_id] )
      @child_member = pregnancy.build_expected_member(first_name:params[:pregnancy][:name],  birth_date:params[:pregnancy][:expected_birth_date], skip_birth_date_validation: true)
      if @child_member.save
        FamilyMember.create(family_id:family.id, member_id:@child_member.id,role: (params[:pregnancy]["role"]|| "Son"))
        Score.create(family_id: family.id, activity_type: "Member", activity_id: @child_member.id, activity: "Added #{@child_member.first_name} to #{family.name}", point: Score::Points["Child"], member_id: @child_member.id)
        pregnancy.save
        api_version = @api_version
        @child_member.add_default_nutrients(api_version,current_user)
       sys_ref_list  = SysArticleRef.where(status:"Active").where(:category=> /pregnancy/i)
       ArticleRef.assign_article_ref(@child_member.id,{:member_obj=>@child_member,:sys_ref_list=>sys_ref_list})
        message = Message::API[:success][:create_preg]
        status = StatusCode::Status200
      else
        message = Message::API[:error][:create_preg]
        status = StatusCode::Status100
      end
      
      @child_member["message"],@child_member["status"] = message, status
      @child_member["preg_id"] = pregnancy.id
    rescue Exception=> e
      @error = e.message
      message = Message::API[:error][:create_preg]
      status = StatusCode::Status100
    end
    respond_to do |format|
      format.js
      format.json { render :json=> {:error=> @error, :pregnancy_detail=> pregnancy, :message=> message,:status=> status, 
        :child_member=>@child_member.to_json(:include => [:family_members]) ,:handhold_controllers=>HandHold.get_handhold_data(current_user,[pregnancy.id],'pregnancy_created','pregnancy')} }
    end
  end

  # api
  # Get /pregnancy/get_mother_list.json?token=iBSqRyQ5pxuxVb1dSz47
  # params = {:family_id=>"541ac2065acefe02aa000005"}
  def get_mother_list
    data = current_user.mothers_in_user_family(params[:family_id])
    message = Message::API[:success][:get_mother_list]
    status = StatusCode::Status200
    respond_to do |format|
      format.js
      format.json { render :json=> {:skip_screen_name=>"preg_handhold_1", :next_screen_name=>"preg_handhold_1",:list=> data, message:message , status: status}  }
    end
  end
  # api
  # Post /pregnancy/create_system_timeline.json?token=iBSqRyQ5pxuxVb1dSz47
  # params = {:pregnancy_id=>"541ac2065acefe02aa000005",:system_timeline_ids=>[]}
  def create_system_timeline
    begin
      pregnancy = Pregnancy.where(id:params[:pregnancy_id]).first
      child_member = Member.where(id:pregnancy.expected_member_id).first
      system_timelines = SystemTimeline.for_category('preg').in(:id => params[:system_timeline_ids]).order_by(sequence: 'asc').entries
      preg_id = pregnancy.id
      system_timelines.each do |system_timeline|
        timelined_at = child_member.birth_date - 40.weeks + eval(system_timeline.duration)
        timeline = Timeline.create({timelined_id:system_timeline.id, timeline_at:timelined_at ,name:system_timeline.name}.merge({system_entry: "true", member_id: child_member.id,added_by: current_user.id,entry_from: "preg", timelined_type: "Pregnancy"}))
      end
      #Add future timelines
      all_system_timelines = SystemTimeline.where(:category => "preg").not_in(:id => params[:system_timeline_ids]).order_by(sequence: 'asc').entries
      all_system_timelines.each do |system_timeline|
        timelined_at = child_member.birth_date - 40.weeks + eval(system_timeline.duration)
        if timelined_at > Date.today
          timeline = Timeline.create({timelined_id:system_timeline.id, timeline_at:timelined_at ,name:system_timeline.name}.merge({system_entry: "true", member_id: child_member.id,added_by: current_user.id,entry_from: "preg", timelined_type: "Pregnancy"}))
        end
      end
      message = Message::API[:success][:create_system_timeline]
      status = StatusCode::Status200
    rescue Exception=>e
      status = StatusCode::Status100
      message = Message::API[:error][:create_system_timeline]
      @error = e.message
    end
    respond_to do |format|
      format.js
      format.json { render :json=> {:skip_screen_name=>"timeline_list_screen", :next_screen_name=>"preg_handhold_10", message:message , status: status,error:@error}  }
    end
  end

  # Api
  # PUT "/pregnancy/update_preg_child.json?token=iBSqRyQ5pxuxVb1dSz47"
  # params = {:preg_id=>"12312",:preg_member_id=>"asasda" }}
  def update_preg_child
    begin
      preg = Pregnancy.find(params[:preg_id])
      preg.update_attributes(member_id:params[:preg_member_id])
       render :json => {:next_screen_name=> "preg_handhold_1",:message=>Message::API[:success][:update_preg_child], :status=> 200}
    rescue Exception=> e
        render :json=> {:message => Message::API[:error][:update_preg_child] , :error =>e.message,status: 100}
    end
  end
  
  # Api
  # Get /pregnancy/get_default_timelines.json?token=iBSqRyQ5pxuxVb1dSz47&preg_id
  def get_default_timelines
    begin
      preg = Pregnancy.find(params[:preg_id])
      child = ChildMember.find(preg.expected_member_id)
      data = HandHold.child_hh_timelines(child,"preg")
       render :json => {:default_system_timeline=> data[:system_timelines], :next_screen_name=> "preg_handhold_1",:message=>"", :status=> 200}
    rescue Exception=> e
        render :json=> {:message => "" , :error =>e.message,status: 100}
    end
  end

  #api
  # PUT /pregnancy/mark_preg_complete.json?token= 
  # Params = {:member=>[{"family_members"=>{"role"=>"Son"}, :first_name=>"first_c",:last_name=>"preg",:birth_date=> 2015-11-06},{:first_name=>"second_c",:last_name=>"preg",:birth_date=> 2015-11-06}],:preg_id=>"563c4df55acefed4f400000a", :status=>"completed"}
  def mark_preg_complete
     begin
      preg = Pregnancy.find(params[:preg_id])
      first_member_id = preg.expected_member_id #.class.to_s == "Array" ? preg.child_member[0] : preg.child_member
      member  =Member.find(first_member_id)
      data = member.preg_to_born(preg,current_user,params[:member])
      preg_type = params[:member].count rescue 1
      preg.update_attribute(:status,params[:status])
      preg.update_attribute(:child_count,preg_type)
      members = [] #Member.in(id:preg.added_child) rescue nil
      status= StatusCode::Status200
      Member.in(id:preg.added_child).includes(:family_members).each do |member|
        member["role"] = member.family_members.first.role rescue "Son"
        members << member
      end      
      render :json=>{:pregnancy=>preg, :family_members=>members,:message=>Message::API[:success][:mark_preg_complete], :handhold_controllers=> HandHold.get_handhold_data(current_user,preg.added_child,'pregnancy_completed'),:status=> status}
    rescue Exception=>e
      render :json=> {:message=>Message::API[:error][:mark_preg_complete],:error=>e.message,:status=> 100 }
    end

  end


  # Api: Delete pregnancy
  # Delete /pregnancy/delete_preg.json?token= 
  # Params = {:preg_id=>"12az23z32ec98scjhbh8273e"}
  def delete_preg
     begin
      api_version = @api_version # delete all tool irrespective of api version. Latest version here is 3
      preg = Pregnancy.find(params[:preg_id])
      member = Member.find(preg.expected_member_id)
      FamilyMember.where(member_id:preg.expected_member_id).first.destroy
      member.delete_child_member
      member.timelines.delete_all
      member.member_nutrients(api_version,current_user).delete_all 
      member.medical_events.delete_all
      member.article_refs.delete_all rescue nil
      preg.destroy
      render :json=> {:message=> Message::API[:success][:delete_preg] ,:status=> 200 }
    rescue Exception=>e
      render :json=> {:message=>Message::API[:error][:delete_preg] ,:error=>e.message,:status=> 100 }
    end

  end
  private
   def set_api_version
     @api_version = 2
   end
end