 class Api::V2::DocumentsController < Api::V1::DocumentsController

 	# GET http://localhost:3000/api/v1/documents.json?token=XrjzH88mP4syK1q4MTeb&member_id=546128200a9a99178300000d
 	def index
	  member = Member.where(id: params[:member_id]).first
	  @documents = member.documents if member
	  member["age"] = ApplicationController.helpers.calculate_age(member.birth_date) rescue ""
	  respond_to do |format|
	  	#format.json { render json: (documents || []) }
	  	format.json
	  end
	end
  
  # Get /api/v1/documents/get_doc_detail.json?token=XrjzH88mP4syK1q4MTeb&doc_id
	def get_doc_detail
	  @document = Document.where(id: params[:id]).first
	end

	# POST http://localhost:3000/api/v1/documents.json?token=XrjzH88mP4syK1q4MTeb
	# "member_id"=>"546128200a9a99178300000d", "document"=>{"name"=>"hiii", "document_type"=>"Health", "file"=>#<ActionDispatch::Http::UploadedFile:0x0000000b048868 @original_filename="Screenshot from 2015-02-18 21:14:05.png", @content_type="image/png", @headers="Content-Disposition: form-data; name=\"document[file]\"; filename=\"Screenshot from 2015-02-18 21:14:05.png\"\r\nContent-Type: image/png\r\n", @tempfile=#<File:/tmp/RackMultipart20150307-3267-reec7w>>}
	def create
	  member = Member.where(id: params[:member_id]).first
	  document = member.documents.new(params[:document]) if member
	  document.file = params[:media_files].first if params[:media_files].present?
	  respond_to do |format|
	  	if document && document.save
	  	  message = Message::API[:success][:add_document]
	  	  format.json { render json: document.attributes.merge(file_path: document.file.path, message: message,status: StatusCode::Status200) }
	  	else
	  	  message = Message::API[:error][:add_document]
	  	  format.json { render json: {:message=> message, :status=> StatusCode::Status100, :errors => document.try(:errors)} }
	  	end
	  end
	end

	# PUT http://localhost:3000/api/v1/documents/54fa95c20a9a995f0c000002.json?token=XrjzH88mP4syK1q4MTeb
	# Parameters: {"member_id"=>"546128200a9a99178300000d", "document"=>{"name"=>"doc3 updated", "document_type"=>"\"General\""}, "token"=>"XrjzH88mP4syK1q4MTeb", "id"=>"54fa95c20a9a995f0c000002"}
	# Parameters: {"utf8"=>"✓", "authenticity_token"=>"csBsF7lIRuCZfKCd2CgHOHlOAafYxRn5u+C+jH1tQfU=", "member_id"=>"546128200a9a99178300000d", "document"=>{"name"=>"doc3 updated", "document_type"=>"General", "file"=>#<ActionDispatch::Http::UploadedFile:0x00000007fee320 @original_filename="Screenshot from 2015-02-18 21:14:05.png", @content_type="image/png", @headers="Content-Disposition: form-data; name=\"document[file]\"; filename=\"Screenshot from 2015-02-18 21:14:05.png\"\r\nContent-Type: image/png\r\n", @tempfile=#<File:/tmp/RackMultipart20150307-4305-1fu4z6v>>}
	def update
	  member = Member.where(id: params[:member_id]).first
	  document = member.documents.find(params[:id]) if member
	  document.file = params[:media_files].first if params[:media_files].present?
	  respond_to do |format|
	    if document.update_attributes(params[:document])	      
     	  message = Message::API[:success][:update_document]
	      format.json { render json: document.attributes.merge(file_path: document.file.path, message: message,status: StatusCode::Status200 ) }
	    else
	      message = Message::API[:success][:update_document]
	      format.json { render json: {:errors => document.errors, :message=> message, :status=> StatusCode::Status100} }
	    end
	  end
	end	

	# DELETE http://localhost:3000/api/v1/documents/54fa95c20a9a995f0c000002.json?token=XrjzH88mP4syK1q4MTeb
	def destroy
	  @document = Document.where(id: params[:id]).first
	  @document.destroy
	  respond_to do |format|
	    format.json
	  end
	end	

	# GET http://localhost:3000/api/v1/documents/search?utf8=%E2%9C%93&member_id=546128200a9a99178300000d&Document%5Bname%5D=doc
	def search
	  member = Member.find(params[:member_id])
	  if params[:Document][:name].present?
	  	documents = member.documents.any_of(name: /#{params[:Document][:name]}.*/)
	  else
	  	documents = member.documents
	  end	
	  respond_to do |format|
	    format.json {render json: documents}	    
	  end
	end	

	# DELETE http://localhost:3000/api/v1/documents/548bfd0e0a9a997d21000008/delete_attachment
	def delete_attachment
	  @document = Document.find(params[:id])
	  @document.file = nil
	  @document.save
	  respond_to do |format|
	    format.json
	  end
	end

	# GET http://localhost:3000/api/v1/documents/types.json
	def types
	  respond_to do |format|
	  	format.json {render json: Document::DOC_TYPES.values}
	  end	
	end	

	# def download
	#   document = Document.find(params[:id])
	#   send_file document.file.path
	# end	

	def upload_file
	  @document = Document.find(params[:id])
	  decode_and_save_file(@document,params[:file_data]) if params[:file_data].present?
	rescue
	  render json: {status: StatusCode::Status100}
	end

	private

	def decode_and_save_file(nutrient,image_data)
	    member = nutrient.member
	    content = image_data
	    content.gsub!('\\r', "\r")
	    content.gsub!('\\n', "\n")
	    decode_base64_content = Base64.decode64(content)
	    File.open("public/documents/#{nutrient.class.name.downcase}_#{nutrient.id}.jpg", "wb") do |f|
	      f.write(decode_base64_content)
	    end
	    file = File.open("public/documents/#{nutrient.class.name.downcase}_#{nutrient.id}.jpg")
	    nutrient.file = file
	    nutrient.save if nutrient.valid?
	    File.delete("public/documents/#{nutrient.class.name.downcase}_#{nutrient.id}.jpg")
    end

 end	