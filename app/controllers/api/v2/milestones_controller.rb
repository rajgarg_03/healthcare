class Api::V2::MilestonesController < Api::V1::MilestonesController
  require "base64"

  def index
  	@member = Member.find(params[:member_id])
    @rec_pointer = ArticleRef.get_recommended_pointer(current_user,@member,"milestone")
    @member["age"] = ApplicationController.helpers.calculate_age(@member.birth_date) rescue ""
    @category = @member.find_category
    @system_milestones_count = {}
    SystemMilestone.all.group_by{|sm| sm.category}.each{|sm_group| @system_milestones_count[sm_group.first] = sm_group.last.count}
    @added_milestones_count = added_milestones(@member)
    @percentage = calculate_percentage(@system_milestones_count,@added_milestones_count)
    respond_to do |format|
      # format.json { render :json => milestones.to_json(include: [:system_milestone,:pictures]) }
      format.json
    end 
  end

  # Get Milestone detail
  # Get /api/v1/milestones/get_milestone_detail.json?token=XrjzH88mP4syK1q4MTeb&milestone_id
  def get_milestone_detail
    begin
      @milestone = Milestone.find(params[:milestone_id])
      @milestone["message"] = "Milestone found"
    rescue
      message = "No milestone found"
      render json: {:message => message,:status=>100}
    end    
  end

  # {milestone: {member_id: "", system_milestone_id: "", description: "", date: ""}}
  def create
  	member = Member.find(params[:milestone][:member_id])
    milestone = Milestone.new(params[:milestone])
    respond_to do |format|
      unless member.already_having_milestone?(milestone)
        if milestone.save          
          if params[:media_files].present?
            params[:media_files].each do |pic|
              milestone.pictures << Picture.new(local_image: pic,upload_class: "milestone",member_id: member.id)
            end
          end  
          message = Message::API[:success][:milestone_create] rescue  "Selected milestone has been added to the accomplished list."
          milestone["message"] = message rescue ""
          milestone["status"] = StatusCode::Status200
          format.json { render :json => milestone.to_json(include: message) }
        end	    
      else
        already_having = true
        message = Message::API[:error][:milestone_create] rescue "You have already added this milestone."
        format.json { render :json => {already_having: true, message:message,status:StatusCode::Status100} }  
      end
    end
  end

  #{id: "", milestone: {member_id: "", system_milestone_id: "", description: "", date: ""}, delete_picture_ids: "123,456,789"}
  def update
  	@milestone = Milestone.find(params[:id])
    @member = @milestone.member
    (params[:upload_media] || []).select{|p| p.present?}.each do |pic|
      @milestone.pictures << Picture.new(local_image:pic,upload_class: "milestone",member_id:@member.id)
    end
    respond_to do |format|      
      if @milestone.update_attributes(params[:milestone])
        if params[:delete_picture_ids].present? # To delete pics from api
          @delete_picture_ids = params[:delete_picture_ids].split(',')           
          @delete_picture_ids.each do |picture_id|
            if picture_id.include?('default_')
              @milestone.update_attribute(:display_default_image,false)
            else  
              picture = Picture.where(id: picture_id).first
              picture.destroy if picture
            end  
          end  
        end
        if params[:media_files].present?
          params[:media_files].each do |pic|
            @milestone.pictures << Picture.new(local_image:pic,upload_class: "milestone",member_id:@member.id)
          end
        end  
        @milestone["message"] = Message::API[:success][:milestone_update] rescue ""
        @milestone["status"] = StatusCode::Status200
        format.json {render :json => @milestone.to_json(include: :pictures)}
  	  else
  	  	format.json {render :json => @milestone.errors}
  	  end
  	end
  end

  def add_multiple
    @member = Member.find(params[:member_id])
    success = true
    if params[:milestones].present?
      params[:milestones].each do |milestone_data|        
        @milestone = Milestone.find_or_initialize_by(member_id: @member.id, system_milestone_id: milestone_data[:system_milestone_id])
        @milestone.date = milestone_data[:date]
        success = false unless @milestone.save
      end
    end      
    message = success ? Message::API[:success][:milestone_create] : Message::API[:error][:milestone_create]
    @member.milestones.where({'system_milestone_id' => {"$in" => params[:delete_milestone_ids].split(',')}}).destroy_all if params[:delete_milestone_ids].present?
    respond_to do |format|
      format.json {render json: {:status => (success ? StatusCode::Status200 : StatusCode::Status100),message: message}}
    end  
  end
  
  def destroy
    @milestone = Milestone.where(id: params[:id]).first
    respond_to do |format|
      if @milestone
        if @milestone.destroy
          api_response = {status:StatusCode::Status200, deleted: true, message: Message::API[:success][:milestone_delete]}
        else
          api_response = {status:StatusCode::Status200, deleted: false, message: Message::API[:error][:milestone_delete]}
        end
        format.json {render :json => api_response}
      else
        format.json {render :json => {message: "Milestone is not present", status: :unprocessible_entity}}
      end      
    end  
  end

  def milestones_for_graph    
    member = Member.where(id: params[:id]).first
    @system_milestones_count = {}
    SystemMilestone.all.group_by{|sm| sm.category}.each{|sm_group| @system_milestones_count[sm_group.first] = sm_group.last.count}
    @added_milestones_count = added_milestones(member)
    @percentage = calculate_percentage(@system_milestones_count,@added_milestones_count)
  end

  def upload_image
    # debugger
    @milestone = Milestone.find(params[:id])
    decode_and_save_image(@milestone,params[:image_data])
    # @message      
  end  

  private

  def added_milestones(member)
    arr = member.milestones.group_by{|m| m.system_milestone.category}.map{|k,grp| {k => grp.count} }
    SystemMilestone::CATEGORIES_RANGE.values.map{|v| v==arr}
    @h1 = Hash.new
    SystemMilestone::CATEGORIES_RANGE.values.map do |v|
      arr.each do |h1|
        @value = v==h1.keys.first ? h1.values.first : 0
        break if @value != 0
      end
      @h1[v] = @value.to_i
    end
    @h1
  end

  def calculate_percentage(system_milestones_count,added_milestones_count)
    percentage = {}
    system_milestones_count.each do |k,v|
      percentage[k] = (added_milestones_count[k].to_f/system_milestones_count[k]) *100
    end  
    percentage
  end  

end