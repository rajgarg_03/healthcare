class Api::V2::MemberController < MemberController
  #skip_before_filter  :verify_authenticity_token
  #skip_before_filter :authenticate_user!, :only => [:verify_elder_for_family, :verify_elder]
  require "RMagick"
  include Magick 

  def create_child
    begin
      @family = Family.find(params[:family_id])
      family_member = params[:Member].delete("family_members").merge!(family_id: @family.id)
      @child_member = ChildMember.new(params[:Member])
      @child_member.last_name =   @family.member.last_name rescue nil  
      if @child_member.save
        api_version = 2
        @child_member.add_default_nutrients(api_version,current_user)
        @child_member.family_members.create(family_member)
        @message = Message::GLOBAL[:success][:create_child].html_safe #"Your child has been added. Visit <a href='families' data-remote=true>Manage Family</a>."
        Timeline.save_system_timeline_to_member(@child_member.id,current_user.id) unless request.format.json?
        Health.new.health_setting(@child_member.id)
        Score.create(family_id: @family.id, activity_type: "Member", activity_id: @child_member.id, activity: "Added #{@child_member.first_name} to #{@family.name}", point: Score::Points["Child"], member_id: @child_member.id)
        
        message = Message::API[:success][:create_child] + " #{@family.name}"
        status = StatusCode::Status200
      else
        message =  @child_member.errors.full_messages.join(",")
        status = StatusCode::Status100
      end
       @child_member["message"],@child_member["status"] = message, status
       @child_member["age"] = ApplicationController.helpers.calculate_age(@child_member.birth_date) rescue ""
       @child_member["role"] = @child_member.family_members.first.role rescue "Son"
     rescue Exception=>e
      @error = e.message
      status = StatusCode::Status100
      message = Message::API[:error][:create_child] #@child_member.errors.full_messages.join(",")
     end
     respond_to do |format|
      format.js
      format.json { render :json=>{:message=>message,:error=>@error, :family_members=>[@child_member], :handhold_controllers => (HandHold.get_handhold_data(current_user, [@child_member.id],'new_child_added') rescue []) , status: status } }
    end

  end


  def update_child 
    begin
      @family_member = FamilyMember.find(params[:id])
      @member = @family_member.member
      first_name = @member.first_name
      birth_date = @member.birth_date    
    rescue 
      # to support api in case send member id instead of family_member_id
      @member = Member.find(params[:id])
      @family_member = FamilyMember.where(member_id:@member.id).first
    end
    if params[:parent_member].present?
      @member.update_attributes(params[:parent_member].reject{|k| k == "family_member"})
      @family_member.update_attributes(params[:parent_member]["family_member"])
    else
      if @member.update_attributes(params[:child_member].reject{|k| k == "family_member"})

        @family_member.update_attributes(params[:child_member]["family_member"]) if params[:child_member]["family_member"]
      end  
    end
    if params[:child_member][:birth_date].present? && params[:child_member][:birth_date].to_date != birth_date
     @member.update_member_ref("update",{:birth_date_updated=>true})
    elsif params[:child_member][:first_name].present? && params[:child_member][:first_name] != first_name
      @member.update_member_ref("update",{:name_updated=>true})
    else
    end
    #NOTE: This block might not in use.
    if @pic_valid = !@member.profile_image || (@member.profile_image && @member.profile_image.valid?)
      if (params[:child_member] && params[:child_member][:profile_image_attributes].present?) || (params[:parent_member] && params[:parent_member][:profile_image_attributes].present?)
        crop
        @member.profile_image.reprocess_image
      end
      api_message = Message::API[:success][:upload_profile_picture]
    else
      api_message = Message::API[:success][:update_child]
    end
    if params[:media_files].present?
      picture = Picture.new(local_image: params[:media_files],upload_class: "pic",member_id: @member.id)
      @member.profile_image = picture if picture.valid?
    end  
    @member["age"] = ApplicationController.helpers.calculate_age(@member.birth_date) rescue ""
    respond_to do |format| 
      format.js
      format.json { render :json=> {:member=> @member,message: api_message, status: StatusCode::Status200}  }
    end    
  end

  def crop
     selected_member =  @member.class.to_s.underscore
     coord = params[selected_member]["profile_image_attributes"]
    if coord["crop_w"].to_i &&  coord["crop_h"].to_i
     
      local_image = params[selected_member][:profile_image_attributes].delete("local_image")
      image = @family_member.member.profile_image
      local_image1 = local_image ||  URI.parse(image.photo_path_str(:original)) if !FileTest.exist?(image.local_image.path)
      if local_image1.present?
        image.local_image= local_image1
        image.local_image.save
      end
      File.open(image.photo_path_str(:original), "rb") do |f|
        @orig_img = Magick::Image::from_blob(f.read).first
      end
      width = @orig_img.columns > 504 ? 504 : @orig_img.columns
      height = @orig_img.rows > 285 ? 285 : @orig_img.rows
      @orig_img.resize!(width,height) if (width == 504 || height == 285)
      @orig_img.crop!(coord["crop_x"].to_i,coord["crop_y"].to_i , coord["crop_w"].to_i, coord["crop_h"].to_i)
      @orig_img.write(image.local_image.path(:original))
    end
   end

  def delete_child    
    @member = Member.find(params[:id])
    @member.delete_child_member
    respond_to do |format| 
      format.js
      format.json { render :json=> {:member=> @member, message: Message::API[:success][:delete_child], status: StatusCode::Status200}  }
    end
  end  

  def create_elder
    @family = Family.find(params[:family_id])    
    @elder_member = ParentMember.where(:email => params[:ParentMember][:email].downcase).first
    if @elder_member.blank?
      @elder_member = ParentMember.new(params[:ParentMember].reject{|k| k == "family_members"})
    else 
      @elder_already_present = true
    end  
    @member_exists = FamilyMember.where(member_id: @elder_member.id, family_id: @family.id).present?
    if !@member_exists && @elder_member.save
      @invitation = Invitation.where(member_id: @elder_member.id, family_id: params[:ParentMember][:family_members][:family_id], status: Invitation::STATUS[:invited]).first
      if @invitation.blank?
        @invitation = @elder_member.invitations.new(params[:ParentMember][:family_members])
        @invitation.sender_type = "Family"
        @invitation.sender_id = current_user.id
        @invitation.status =  Invitation::STATUS[:invited]
        @invitation.save        
      end
      @message = Message::GLOBAL[:success][:create_elder].html_safe #"An email has been sent to the invitee. Visit <a href='families' data-remote=true>Manage Family</a>."
      message = Message::API[:success][:create_elder]
      UserMailer.elder_member_request_email(@invitation).deliver
      add_notification(@elder_member, "#{current_user.name} invited to you join his family #{@family.name}.", families_path, current_user,"family") if @elder_already_present
    else
      message = Message::API[:success][:elder_already_exist] + @family.name
    end
    @elder_member["age"] = ApplicationController.helpers.calculate_age(@elder_member.birth_date) rescue ""
    respond_to do |format|
      format.js  
      format.html 
      format.json { render :json => { :message=> message, :elder_member=>@elder_member ,:invitation=> @invitation, :status => StatusCode::Status200} }
    end  
  end

  
  def update_elder
  begin    
    @family_member = FamilyMember.find(params[:id])
    if params[:ParentMember][:family_member][:role].present? && params[:ParentMember][:family_member][:role].downcase != "mother"
      raise "One or more pregnancies are associated with this member. Please remove the association to change role"  if Pregnancy.where(member_id:@family_member.member_id.to_s,status:"expected",family_id:@family_member.family_id.to_s).present?
    end
    message = Message::API[:success][:update_elder]
    status = StatusCode::Status200
    @family_member.update_attributes(params[:ParentMember][:family_member])
  rescue Exception=>e
    message = e.message
    @error = e.message
    status = StatusCode::Status100
  end
    respond_to do |format| 
      format.js
      format.json { render :json=> {:member=> @family_member,message:message, :error => @error, status: status}  }
    end
  end

  def delete_elder_invitation    
    @invitation = Invitation.find(params[:id])
    @invitation.destroy
    @family = @invitation.family.reload
    respond_to do |format| 
      format.js
      format.json { render :json=> {:family=> @family, message:Message::API[:success][:delete_elder_invitation], status: StatusCode::Status200}  }
    end
  end  

  def resend_invitation
    @invitation = Invitation.find(params[:id])
    UserMailer.elder_member_request_email(@invitation).deliver
    respond_to do |format| 
      format.js {render :layout=>false}
      format.json { render :json=> {:invitation=> @invitation, message:Message::API[:success][:resend_invitation], status: StatusCode::Status200}  }
    end
    
  end  

  # Accept/Reject invitatoin to join family 
  def verify_elder_for_family
     begin
      @invitation = Invitation.find(params[:id])  
       session[:user_id] = nil
     rescue Exception => e
      flash[:notice] = "Invitation is no longer active"
      respond_to do |format| 
        # format.html {redirect_to( "/users/sign_in?pageName=signin") and return}
        # format.html {redirect_to( "/users/sign_in?pageName=signin",:flash => { :notice => "Invalid invitation" }) and return}
        
        format.html {redirect_to( (user_signed_in? ? families_url : "/users/sign_in?pageName=signin"),:flash => { :notice => flash[:notice] })  and return}
        # format.html {redirect_to( families_url,:flash => { :notice => flash[:notice] })  and return}
      end
     end
    
    member = @invitation.member
    @user = member.user #User.where(:email => @invitation.member.email).first
    if @user.present? # == current_user.user
       @user.status = "confirmed" if @user.status == "unconfirmed"
       @user.save
      session[:invited_user_id] = @user.id
      session[:authentication_status]  = User.user_authentication_status(@user,@user.member)
      session[:api_token] = @user.confirmation_token if session[:authentication_status] == "authenticated"

    end
    session[:api_invitation] = @invitation.id.to_s
    session[:api_email] = member.email rescue nil
    session[:api_invitation_status] = "active"
    sign_out if user_signed_in? && @user != current_user.user
    # debugger
    # Invitation request is active ?
    if @invitation.status == Invitation::STATUS[:accepted] || @invitation.status == Invitation::STATUS[:rejected]
      session[:api_invitation_status] = "inactive"
      flash[:notice] = user_signed_in? ? "You already #{@invitation.status} this family." : "Please login to continue.You already #{@invitation.status} this family."
      respond_to do |format| 
        format.html {redirect_to(user_signed_in? ? families_url : "/users/sign_in?pageName=signin") and return}
        format.js {redirect_to(user_signed_in? ? families_url : "/users/sign_in?pageName=signin") and return}
        format.json { render :json=> { :auth_token=> session[:api_token], :invitation=> @invitation, :message=>flash[:notice],  status: StatusCode::Status100} and return true }
      end
    elsif params[:status] == 'reject'
      @invitation.reject
      flash[:notice] = Message::GLOBAL[:success][:verify_elder_for_family_reject]#"You have rejected the request."
      respond_to do |format| 
        format.html {redirect_to( (user_signed_in? ? dashboard_url : "/users/sign_in?pageName=signin"),:flash => { :notice => flash[:notice] } ) and return}
        format.js {redirect_to(user_signed_in? ? dashboard_url : "/users/sign_in?pageName=signin") and return}
        format.json { render :json=> { :auth_token=> session[:api_token], :invitation=> @invitation, :message=>flash[:notice],  status: StatusCode::Status200} and return true }
      end
      
    elsif params[:status] == 'accept'
      # member = @invitation.member
      # @user = member.user #User.where(:email => @invitation.member.email).first
      Score.create(family_id: @invitation.family_id, activity_type: "Member", activity_id: member.id, activity: "Added #{member.email} to #{@invitation.family.name}", point: Score::Points["Member"], member_id: member.id)
      # sign_out if user_signed_in? && @user != current_user.user
      session[:invitation_id] = @invitation.id
      
      @invitation.accept
      @family_member = FamilyMember.new(member_id: @invitation.member_id, family_id: @invitation.family_id, role: @invitation.role)
      @family_member.save
      if @user.present?
        flash[:notice] = "You have become a member of #{@invitation.family.name}."
        page_name = "signin"
      else
        flash[:notice] = Message::GLOBAL[:success][:family_user_register]#"Please register with us to proceed."
        page_name = "signup"
      end
      respond_to do |format| 
        format.html {redirect_to(user_signed_in? ? families_url : "/users/sign_in?pageName=#{page_name}") and return }
        format.js { redirect_to(user_signed_in? ? families_url : "/users/sign_in?pageName=#{page_name}") and return }
        format.json { render :json=> {:auth_token=> session[:api_token], :family_member=> @family_member, :user=>@user, :invitation=> @invitation, :message=>flash[:notice],  status: StatusCode::Status200} and return true }
      end
      
    end
  end

  # Accept/reject invitation to join Nurturey
  def verify_elder
    @invitation = Invitation.find(params[:id])
    @user = User.where(:email => @invitation.member.email).first
    if @user && @user.status == "unconfirmed"
      @user.status = "confirmed"
      @user.save
    end
    # App launch info 
    session[:api_token] = @user.confirmation_token if @user.present? # == current_user.user
    session[:api_invitation] = @invitation.id.to_s
    session[:api_email] = member.email rescue nil
    session[:api_invitation_status] = "active"

    sign_out if user_signed_in? && @user != current_user.user    
    if @invitation.status == Invitation::STATUS[:accepted] || @invitation.status == Invitation::STATUS[:rejected]
      flash[:notice] = user_signed_in? ? "You have already #{@invitation.status}." : "You have already #{@invitation.status}."
      session[:api_invitation_status] = "inactive"
      respond_to do |format| 
        format.html {redirect_to(user_signed_in? ? dashboard_url : "/users/sign_in?pageName=signin") and return }
        format.js { redirect_to(user_signed_in? ? dashboard_url : "/users/sign_in?pageName=signin") and return }
        format.json { render :json=> {:family_member=> @family_member, :user=>@user, :invitation=> @invitation, :message=>flash[:notice],  status: StatusCode::Status200} and return true }
      end

    elsif params[:status] == 'reject'
      @invitation.reject
      flash[:notice] = Message::GLOBAL[:success][:verify_elder_reject] #"You have rejected the request."
      respond_to do |format| 
        format.html {redirect_to(user_signed_in? ? dashboard_url : "/users/sign_in?pageName=signin") and return }
        format.js { redirect_to(user_signed_in? ? dashboard_url : "/users/sign_in?pageName=signin") and return}
        format.json { render :json=> {:family_member=> @family_member, :user=>@user, :invitation=> @invitation, :message=>flash[:notice],  status: StatusCode::Status200} and return true }
      end
      
    elsif params[:status] == 'accept'      
      flash[:notice] = Message::GLOBAL[:success][:verify_elder_accept] #"You have accpeted the request."
      Score.create(activity_type: "Invitation", activity_id: @invitation.id, activity: "Invited #{@invitation.member.email} to Nurturey", point: Score::Points["Invitation"], member_id: @invitation.sender.id)      
      session[:invitation_id] = @invitation.id
      if @user.present?
        @invitation.accept
        page_name = "signin"
      else        
        flash[:notice] = Message::GLOBAL[:success][:family_user_register] #"Please register with us to proceed."
        page_name = "signup"
      end
      respond_to do |format| 
        format.html {redirect_to(user_signed_in? ? dashboard_url : "/users/sign_in?pageName=#{page_name}") and return }
        format.js { redirect_to(user_signed_in? ? dashboard_url : "/users/sign_in?pageName=#{page_name}") and return}
        format.json { render :json=> {:family_member=> @family_member, :user=>@user, :invitation=> @invitation, :message=>flash[:notice],  status: StatusCode::Status200} and return true }
      end
      
    end  
  end


  
  def update_current_member 
    begin
      @family_member = FamilyMember.where(id: params[:family_member_id]).first if params[:family_member_id]
      @family_member = @family_member || FamilyMember.where(family_id: params[:family_id],member_id:current_user.id).first if params[:family_id]
      if params[:role].present? && params[:role].downcase != "mother"
        raise "One or more pregnancies are associated with this member. Please remove the association to change role"  if Pregnancy.where(family_id:@family_member.family_id.to_s, member_id:current_user.id.to_s,status:"expected").present?
      end

      params[:Member][:country_code] = params[:Member][:country_code] || params[:country_code]
      @member = current_user
      @updated = current_user.update_attributes(params[:Member].except("country_code"))
      user = @member.user

      user.update_attributes({country_code:(params[:Member][:country_code].downcase rescue params[:Member][:country_code])})
      user.update_attributes({first_name:@member.first_name,last_name:@member.last_name} )
      if params[:role].present?
        @family_member.role = params[:role]
        @family_member.save
      end 
      current_user.update_childern_refs
      #NOTE: Single file will be received in params[:media_files] instead of array
      if params[:media_files].present?
        picture = Picture.new(local_image: params[:media_files],upload_class: "pic",member_id: @member.id)
        @member.profile_image = picture if picture.valid?
      end  
      @member["age"] = ApplicationController.helpers.calculate_age(@member.birth_date) rescue ""
      # To get user's created and associated families
      find_families
      respond_to do |format|
        format.js {render :layout=> false}
        format.html {render :layout=> false}
        format.json { render :json=> {:member=> @member, :family_member=> @family_member, message:Message::API[:success][:update_current_member], status: StatusCode::Status200}  }

      end
    rescue Exception=> e
      status = StatusCode::Status100
      render :json=>{:message=>e.message,:status=>status, :error=> e.message}
    end
  end

  
  def hide_invited_user_block
    session[:invited_user] = nil
  end 

  def trigger_tour_step
    session[:tour_step] += 1 if session[:tour_step]
    session[:tour_step] = nil if session[:tour_step] > 2
  end 

# Post api api/v2/member/set_metric_system.json?token=UpLZuMvytGdp8ejSsd5d
# params={"subscibe_mails"=>{ :0 => "off" },:subscribe_notification=>off}

  def set_metric_system
    begin
      length_metric_system = current_user.member_settings.where(name:"length_metric_system").first
      weight_metric_system = current_user.member_settings.where(name:"weight_metric_system").first
      if params[:length_unit].present?
        if length_metric_system.blank?
          MemberSetting.create({member_id: current_user.id, name:"length_metric_system", status:params[:length_unit]})
        else
          length_metric_system.update_attributes({status:params[:length_unit]})
        end
      end
      if params[:weight_unit].present?
        if weight_metric_system.blank?
          MemberSetting.create({member_id: current_user.id, name:"weight_metric_system", status:params[:weight_unit]})
        else
          weight_metric_system.update_attributes({status:params[:weight_unit]})
        end
      end
      if params[:time_zone].present? && current_user.time_zone != params[:time_zone]
        current_user.update_attribute(:time_zone,params[:time_zone].strip)
      end  
      length_metric_system = current_user.member_settings.where(name:"length_metric_system").first
      weight_metric_system = current_user.member_settings.where(name:"weight_metric_system").first
      
      
     
      current_user.subscribe_mail((params[:subscibe_mails]||{}).keys)
      
      if params[:subscribe_notification].present?
        sub_noti = current_user.member_settings.where(name:"subscribe_notification").first ||  MemberSetting.create(name:"subscribe_notification",member_id:current_user.id)
        status_val = params[:subscribe_notification].downcase == "on" ? true : false
        sub_noti.update_attribute(:status,status_val)
        current_user.subscribe_notification(params[:subscribe_notification])
      end 
        current_user.reload
        mailer_setting_status = MemberSetting.where(member_id:current_user.id, name:"mail_subscribe").first.note.present? rescue true
      respond_to do |format|
        format.js {render :layout=> false}
        format.json { render :json=> {:status=>200, :message =>Message::API[:success][:update_setting] ,:time_zone=>current_user.time_zone,:mailer_setting=>mailer_setting_status,:notification=>current_user.notification_subscribed?, :metric_system=>{:length=>(length_metric_system.status rescue false),:weight=>(weight_metric_system.status rescue false)} } }
      end
     rescue  Exception => e
       render :json=> {:message=>Message::API[:error][:update_setting], :status=> 100, :error=>e}
     end
  end

  private

  def find_families
    @sent_family_invitations = current_user.sent_join_family_invitations # Needed in view at multiple places
    @received_family_invitations = current_user.received_join_family_invitations
    current_families = current_user.families.includes(:family_members).entries
    asso_families =  Family.where({'_id' => { "$in" => FamilyMember.where(member_id:current_user.id).map(&:family_id).uniq - current_families.map(&:id)}}).includes(:family_members).entries
    @families = current_families + asso_families      
  end  

end
