class Api::V2::RegistrationsController <  RegistrationsController #Devise::RegistrationsController
   #before_filter :sign_up_params
   #before_filter :require_no_authentication, :only => :new

  #params => {user=>{:time_zone=> , :password=>,:email=>"",:confirmation_email,:invitaion_id=>,:first_name=>,:last_name=>}}
  def create
    # Note: sign_up_paramsis nil so assigned manually, this line should be removed once sign_up_params works
    #sign_up_params = {email: params[:user][:email], password: params[:user][:password]}
    ######  
    build_resource(params[:user])
    @resource = resource
    # skip confirmation for now
    resource.skip_confirmation!
    
    if resource.save
      session[:tour_step] = 0
      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_navigational_format?
        sign_up(resource_name, resource)
        session[:invitation_id] = session[:invitation_id] || params[:invitation_id]
        resource.send_confirmation_instructions  if session[:invitation_id].blank?        
	      if session[:invitation_id].present?
          @invitation = Invitation.where(id:session[:invitation_id]).first
          member = ParentMember.where(:email => @invitation.member.email).first
          member_params = params[:user].has_key?(:first_name) ? {first_name: params[:user][:first_name],
           last_name: params[:user][:last_name], user_id: resource.id, time_zone: (params[:user][:time_zone]|| params[:time_zone])} : {user_id: resource.id}
          member.update_attributes(member_params)
          resource.verify
          if @invitation.status == Invitation::STATUS[:invited]
            @invitation.accept
            family_member = FamilyMember.create(member_id: @invitation.member_id, family_id: @invitation.family_id, role: @invitation.role) if @invitation.family_id.present?
          end
          session[:invitation_id] = nil
          session[:invited_user] = true
          message = "You have become a member of #{@invitation.family.name}."
        else
          member_params = params[:user].has_key?(:first_name) ? {email: params[:user][:email], 
            first_name: params[:user][:first_name], last_name: params[:user][:last_name],
           user_id:resource.id, time_zone: (params[:user][:time_zone]|| params[:time_zone])} : {email:params[:user][:email], user_id:resource.id}
          member = ParentMember.where(email: params[:user][:email]).first || ParentMember.create(member_params)
          member.update_attributes(member_params)
          member.update_attribute(:user_id,resource.id) if member.user_id.blank?
          resource.status = User::STATUS[:unconfirmed]
          message = Message::DEVISE[:m1]
        end
        resource.member_id = member.id
        resource.confirmation_token || resource.send(:generate_confirmation_token)
        resource.save
        # UserMailer.welcome_email(resource).deliver!
        Score.create(activity_type: "User", activity_id: resource.id, activity: "Sign up", point: Score::Points["Signup"], member_id: member.id)
        sign_out#(resource)
        screen_name,token = @invitation.present? ? ["login_screen",resource.confirmation_token] : ["email_verification_screen",nil]
        #redirect_to "/users/after_registration" and return
        session[:resend_verify_email] = resource.email
        # show login screen after signup for device and redirect to dashboard as logged in user for web site
        if resource.present? && User.can_access_dashboard_after_signup?(resource)
          screen_name,token = ["login_screen",resource.confirmation_token]
          sign_in(:user,resource)
          user_redirection_path = "/"
        else
          user_redirection_path = home_messages_path(verify_email_sent: true)
        end

        respond_to do |format|
          format.html {redirect_to user_redirection_path and return true}  
          format.json { render :json => {:auth_token=>token, :message=>message,:screen=>screen_name, :status => StatusCode::Status200} and return true } 
        end
        
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_navigational_format?
        expire_session_data_after_sign_in!
        respond_with resource, :location => after_inactive_sign_up_path_for(resource)
        respond_to do |format|
           format.html  
          format.json { render :json => {:message =>"signed_up_but_#{resource.inactive_message}",  :status => StatusCode::Status100} and return true } 
         end
      end
    elsif resource && (resource.errors.blank? || resource.errors[:email].first == "is already taken") && !sign_up_params[:email].blank?
      user = User.where(:email => sign_up_params[:email]).first
      if !user.confirmed?
        Devise::Mailer.confirmation_instructions(user).deliver
        flash[:notice] = Message::DEVISE[:m2] #"You have tried to register using this email in the past, but did not verify your email. We are sending the verification email again. Kindly verify your email to continue."
        respond_to do |format|
          format.html  { redirect_to after_registration_path }
          format.json { render :json => {:message => flash[:notice],:screen=>"email_verification_screen",  :status => StatusCode::Status100}  } 
        end

      elsif user.confirmed? && user.status =="unapproved"
        flash[:notice] = Message::DEVISE[:m3] #"Your email has been verified. As we are offering only limited number of registrations for Nurturey Beta, you will be able to login after we approve your registration. You will shortly receive an email from Nurturey's support team. In case of any questions. please write to support@nurturey.com."
        respond_to do |format|
          format.html  { redirect_to after_registration_path }
          format.json { render :json => {:message => flash[:notice],:screen=>"message_screen",  :status => StatusCode::Status100}  } 
        end
      elsif user.confirmed? && user.status =="approved"
        session[:reset_error] = Message::DEVISE[:m4] #"We already have an existing account using"
        # session[:reset_email] = "#{user.email}. Please login." #TODO: Need to confirm, where using this.
        session[:email] = user.email
        respond_to do |format|
          format.html  { redirect_to "/users/sign_in?pageName=login" }
          format.json { render :json => {:screen=>"login_screen", :message =>Message::DEVISE[:m4],  :status => StatusCode::Status100} and return true } 
         end
        # redirect_to after_registration_path(:type=>"forgot_password")
      else
        clean_up_passwords resource    
        session[:reset_error] = Message::DEVISE[:m4] #"We already have an existing account using"
        # session[:reset_email] = "#{user.email}. Please login." #TODO: Need to confirm, where using this.
        session[:email] = user.email
        respond_to do |format|
          format.html  { redirect_to "/users/sign_in?pageName=login" }
          format.json { render :json => {:message =>"#{Message::DEVISE[:m4]} #{user.email}. Please login.",  :status => StatusCode::Status100} and return true } 
         end
        
      end
    else
      clean_up_passwords resource    
      flash[:notice] = Message::DEVISE[:m5] + resource.errors.messages.keys.last(2).join("and") #"Please Fill Valid"
      errors = resource.errors.messages.keys.first(2)
      errors.delete(:encrypted_password)
      session[:signup_error] =   Message::DEVISE[:m5] + errors.join(" and ")
      session[:signup_error] =  session[:signup_error] + " with min 8 characters." if errors.include?(:password)
      session[:email] = params[:user][:email]
      # session[:signup_email] = sign_up_params[:email]
      respond_to do |format|
          format.html  { redirect_to "/users/sign_in?pageName=signup" }
          format.json { render :json => {:message =>session[:signup_error],  :status => StatusCode::Status200} and return true } 
         end
     
    end
  end

  def secure_signup
    # Note: sign_up_paramsis nil so assigned manually, this line should be removed once sign_up_params works
    #sign_up_params = {email: params[:user][:email], password: params[:user][:password]}
    ######  
    params[:user][:password] = ShaAlgo.decryption(params[:user][:password].gsub(" ","+")) if params[:user][:password].present?
    build_resource(params[:user])
    @resource = resource
    # skip confirmation for now
    resource.skip_confirmation!
    
    if resource.save
      session[:tour_step] = 0
      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_navigational_format?
        sign_up(resource_name, resource)
        session[:invitation_id] = session[:invitation_id] || params[:invitation_id]
        resource.send_confirmation_instructions  if session[:invitation_id].blank?        
        if session[:invitation_id].present?
          @invitation = Invitation.where(id:session[:invitation_id]).first
          member = ParentMember.where(:email => @invitation.member.email).first
          member_params = params[:user].has_key?(:first_name) ? {first_name: params[:user][:first_name],
           last_name: params[:user][:last_name], user_id: resource.id, time_zone: (params[:user][:time_zone]|| params[:time_zone])} : {user_id: resource.id}
          member.update_attributes(member_params)
          resource.verify
          if @invitation.status == Invitation::STATUS[:invited]
            @invitation.accept
            family_member = FamilyMember.create(member_id: @invitation.member_id, family_id: @invitation.family_id, role: @invitation.role) if @invitation.family_id.present?
          end
          session[:invitation_id] = nil
          session[:invited_user] = true
          message = "You have become a member of #{@invitation.family.name}."
        else
          member_params = params[:user].has_key?(:first_name) ? {email: params[:user][:email], 
            first_name: params[:user][:first_name], last_name: params[:user][:last_name],
           user_id:resource.id, time_zone: (params[:user][:time_zone]|| params[:time_zone])} : {email:params[:user][:email], user_id:resource.id}
          member = ParentMember.where(email: params[:user][:email]).first || ParentMember.create(member_params)
          member.update_attributes(member_params)
          member.update_attribute(:user_id,resource.id) if member.user_id.blank?
          resource.status = User::STATUS[:unconfirmed]
          message = Message::DEVISE[:m1]
        end
        resource.member_id = member.id
        resource.confirmation_token || resource.send(:generate_confirmation_token)
        resource.save
        # UserMailer.welcome_email(resource).deliver!
        Score.create(activity_type: "User", activity_id: resource.id, activity: "Sign up", point: Score::Points["Signup"], member_id: member.id)
        sign_out#(resource)
        screen_name,token = @invitation.present? ? ["login_screen",resource.confirmation_token] : ["email_verification_screen",nil]
        # show login screen after signup for device and redirect to dashboard as logged in user for web site
        resource.reload
        if resource.present? && User.can_access_dashboard_after_signup?(resource)
          screen_name,token = ["login_screen",resource.confirmation_token]
          sign_in(:user,resource)
          user_redirection_path = "/"
        else
          user_redirection_path = home_messages_path(verify_email_sent: true)
        end 
        session[:resend_verify_email] = resource.email
        respond_to do |format|
          format.html {redirect_to user_redirection_path and return true}  
          format.json { render :json => {:auth_token=>token, :message=>message,:screen=>screen_name, :status => StatusCode::Status200} and return true } 
        end
        
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_navigational_format?
        expire_session_data_after_sign_in!
        respond_with resource, :location => after_inactive_sign_up_path_for(resource)
        respond_to do |format|
           format.html  
          format.json { render :json => {:message =>"signed_up_but_#{resource.inactive_message}",  :status => StatusCode::Status100} and return true } 
         end
      end
    elsif resource && (resource.errors.blank? || resource.errors[:email].first == "is already taken") && !sign_up_params[:email].blank?
      user = User.where(:email => sign_up_params[:email]).first
      if !user.confirmed?
        Devise::Mailer.confirmation_instructions(user).deliver
        flash[:notice] = Message::DEVISE[:m2] #"You have tried to register using this email in the past, but did not verify your email. We are sending the verification email again. Kindly verify your email to continue."
        respond_to do |format|
          format.html  { redirect_to after_registration_path }
          format.json { render :json => {:message => flash[:notice],:screen=>"email_verification_screen",  :status => StatusCode::Status100}  } 
        end

      elsif user.confirmed? && user.status =="unapproved"
        flash[:notice] = Message::DEVISE[:m3] #"Your email has been verified. As we are offering only limited number of registrations for Nurturey Beta, you will be able to login after we approve your registration. You will shortly receive an email from Nurturey's support team. In case of any questions. please write to support@nurturey.com."
        respond_to do |format|
          format.html  { redirect_to after_registration_path }
          format.json { render :json => {:message => flash[:notice],:screen=>"message_screen",  :status => StatusCode::Status100}  } 
        end
      elsif user.confirmed? && user.status =="approved"
        session[:reset_error] = Message::DEVISE[:m4] #"We already have an existing account using"
        # session[:reset_email] = "#{user.email}. Please login." #TODO: Need to confirm, where using this.
        session[:email] = user.email
        respond_to do |format|
          format.html  { redirect_to "/users/sign_in?pageName=login" }
          format.json { render :json => {:screen=>"login_screen", :message =>Message::DEVISE[:m4],  :status => StatusCode::Status100} and return true } 
         end
       

        # redirect_to after_registration_path(:type=>"forgot_password")
      else
        clean_up_passwords resource    
        session[:reset_error] = Message::DEVISE[:m4] #"We already have an existing account using"
        # session[:reset_email] = "#{user.email}. Please login." #TODO: Need to confirm, where using this.
        session[:email] = user.email
        respond_to do |format|
          format.html  { redirect_to "/users/sign_in?pageName=login" }
          format.json { render :json => {:message =>"#{Message::DEVISE[:m4]} #{user.email}. Please login.",  :status => StatusCode::Status100} and return true } 
         end
        
      end
    else
      clean_up_passwords resource    
      flash[:notice] = Message::DEVISE[:m5] + resource.errors.messages.keys.last(2).join("and") #"Please Fill Valid"
      errors = resource.errors.messages.keys.first(2)
      errors.delete(:encrypted_password)
      session[:signup_error] =   Message::DEVISE[:m5] + errors.join(" and ")
      session[:signup_error] =  session[:signup_error] + " with min 8 characters." if errors.include?(:password)
      session[:email] = params[:user][:email]
      # session[:signup_email] = sign_up_params[:email]
      respond_to do |format|
          format.html  { redirect_to "/users/sign_in?pageName=signup" }
          format.json { render :json => {:message =>session[:signup_error],  :status => StatusCode::Status200} and return true } 
         end
     
    end
  end

  protected
  
  #override devise helper method to make working for v2 
  def sign_up_params
    params[:user]
  end
  def build_resource(*args)
    super
    if session[:no_confirmation_needed] || session[:no_confirmation_needed_group]
      @user.confirmed_at = Time.now # we don't need to confirm the account if they are using external authentication
    end
  end

  def after_sign_up_path_for(resource)
    session[:no_confirmation_needed] ? root_path : after_registration_path 
    session[:no_confirmation_needed_group] ? root_path : after_registration_path
  end
  
  def after_inactive_sign_up_path_for(resource)
    session[:no_confirmation_needed] ? root_path : after_registration_path 
    session[:no_confirmation_needed_group] ? root_path : after_registration_path 
  end

end
