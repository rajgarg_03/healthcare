 class Api::V2::UsersController < Api::V1::UsersController
  skip_before_filter  :token_verify, only: [:resend_confirmation_link, :push_msg,:secure_sign_in]

  def sign_in
    super
  end
  
  # use encrypt password/token
  def secure_sign_in
    if params[:email].present?
      user = User.where(email:params[:email]).first
    elsif params[:auth_token].present?
      user = User.where(confirmation_token:params[:auth_token]).first
    end
    if (user.present? && (user.has_valid_password?(params[:password]) || user.confirmation_token == params[:auth_token] ) )
      if user.status == User::STATUS[:unapproved]
        render json: {status:101, user:user, message:"User is Blocked"}
      elsif params[:auth_token].blank? && (!User.can_access_dashboard_after_signup?(user) && !user.is_approved?)        
        render json: {status:100, user:user, message: Message::API[:success][:email_not_verified]}
      else
        member = user.member
        update_user_login_details(member,user,params)
        render :json=> hh_and_basic_setup_evaluator_json(member,params[:invitation_id])
      end
    elsif (user.present? && !(user.has_valid_password?(params[:password]) || user.confirmation_token == params[:auth_token]) )
      render json: {message: Message::API[:success][:password_invalid], status:103}
    else
      render json: {message: Message::API[:success][:email_not_registered], status:102}
    end
  end

  # Get all data of user required for Home  
  # Get /api/v2/users/user_dashboard.json?parent_member_id=542991865acefe0ac1000003&token=iBSqRyQ5pxuxVb1dSz47&event_type=past
  def user_dashboard
    begin
    # Get action card data
    data = @current_user.api_dashboard.take(8)
    @childern_ids = current_user.childern_ids.map(&:to_s) + [current_user.id.to_s]
    # Get User calendar Data
    events = Event.get_event_data("parent","upcoming",nil,@current_user.id).sort_by {|c| c[:start]}[0..1]
    event_data = []
    events.each do |data|
      event_data << {member_id:data[:member_id],name:data[:title],type:data[:type], start_date: data[:start], end_date:data[:end],start_time: (data[:datail]["start_time"].to_time.to_formatted_s(:time) rescue "00:00"),end_time: (data[:datail]["end_time"].to_time.to_formatted_s(:time) rescue "23:00")}
    end
    # User's top 2 pointers
    article_ref = ArticleRef.get_pointers_list(current_user,@childern_ids).order_by("effective_score desc").limit(2)
    status = StatusCode::Status200
    rescue Exception=> e
      @error = e.message
      status = StatusCode::Status100
    end
    respond_to do |format|
      format.json { render :json=> {:error=> @error, :status=> status, :recommended=> data, :event_widget=>event_data, :reference_widget=> article_ref} }
    end
  end

  
  def set_version_controller
    begin
      version_control = VersionController.where(member_id:current_user.id).last
      version_control.notification_count = 0
      version_control.save
      status = 200
    rescue Exception=>e
      @error = e.message
      status = 100
    end
    render :json=>{:status=> status, :error=>@error}
  end
  
  #Logout from app.
  def user_sign_out
    begin
      resource_name = current_user.user
      resource_name["app_login_status"] = false
      signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)) 
      status = StatusCode::Status200
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
    end
      render :json=>  {:message=>Message::DEVISE[:m16] ,:status => status,:error=>@error } 
  end

  def resend_confirmation_link
    if current_user && params[:email].blank?
      current_user.user.send_confirmation_instructions
      @message = Message::DEVISE[:m8] #"Verification email has been sent to your email."
    else
      begin
        user = User.where(email:params[:email]).first
        user.send_confirmation_instructions 
      rescue Exception => e
        redirect_to after_registration_path(email:params[:email]) and return true
      end
      respond_to do |format|
        format.html{ redirect_to home_messages_path(email_resent: true) and return true  }
        format.json{render :json=>  {:message=>Message::DEVISE[:m10] ,:status => StatusCode::Status200 } and return true}
      end
    end
    respond_to do |format|
      format.html
      format.js
      format.json{render :json=>  {:flash=> Message::DEVISE[:m8], :message=>Message::DEVISE[:m10] ,:status => StatusCode::Status200 }}
    end
  end
  # api 
  # post /api/v2/users/enable_push_notification.json?token=
  # params = {:device_token=>"jhjhjhjhrqweqcs",:platform=>"ios"}
  def enable_push_notification
    begin
      sub_noti = current_user.member_settings.where(name:"subscribe_notification").first ||  MemberSetting.create(name:"subscribe_notification",member_id:current_user.id)
      status_val = true
      sub_noti.update_attribute(:status,status_val)
      current_user.subscribe_notification("on")
      store_device_token(current_user,params[:device_token],params[:platform])
      message = Message::API[:success][:enable_push_notification]
      status = StatusCode::Status200
    rescue Exception=>e
      status = StatusCode::Status100
      message = Message::API[:error][:enable_push_notification]
      @error = e.message
    end
    render :json=>  {:message=>message,:status=>status,:error=>@error}
  end
  
  # api
  # GET /api/v2/users/basic_set_up_evaluator_for_user.json?token=131aassdas345474das&invitation_id=
  def basic_set_up_evaluator_for_user
    begin
      user = User.find_user_with_token(params[:token]).first
      data =  hh_and_basic_setup_evaluator_json(user.member,params[:invitation_id],{:join_req_setup=>false})
      data.delete(:families) rescue nil
      render :json=> data
    rescue Exception=>e
      render json: {error:e.message,  status:100}
    end
  end

  def sec_email_add
    @success = false
    token = Digest::MD5.hexdigest(params[:secondary_emails][:email])
    SecondaryEmail.where(:user_id=>current_user.id).delete    
    @sec_email = current_user.build_secondary_email(params[:secondary_emails].merge(:confirmation_token => token))
    if @sec_email.save()
      url = request.host_with_port+"/users/verify_secondary_email?token=#{token}"
      UserMailer.secondary_email(@sec_email, "Nurturey-Email Verification", url, Time.now).deliver
      @success = true
    end
    respond_to do |format|
    format.json  { render :json=>{:email=>params[:secondary_emails][:email], :status=> StatusCode::Status200, :head => :ok} }
    end
  end
  
  def sec_email_del
    @success = false
    @empty = true
    if !current_user.secondary_email.nil?
      if current_user.secondary_email.destroy
        @success = true
        @empty = false
      end
    end
      respond_to do |format|
      format.json  { render :json=>{:status=> StatusCode::Status200, :head => :ok} }
    end
  end

  def set_default_cover_image
    begin
      user = current_user.user
      user.cover_image.delete
      message = "Default cover image is set successfully."
      status = StatusCode::Status200
    rescue Exception=> e
      message = Message::API[:success][:set_default_cover_image]
      status = StatusCode::Status100
      @error = e.message
    end
    render :json=>{:message=>message, :status=> status,:error=>@error}
  end  

  def upload_image_multipart
    member = Member.where(id: params[:member_id]).first    
    #NOTE: Single file will be received instead of array
    pic = params[:media_files] if params[:media_files].present?
    @picture = Picture.new(local_image:pic ,upload_class: 'pic')
    if @picture.valid? && member.profile_image = @picture
      @picture_saved = true
      @message = Message::API[:success][:upload_image]
      @status = StatusCode::Status200
    else
      @picture_saved = false
      @message = Message::API[:error][:upload_image]
      @status = StatusCode::Status100
    end
    render json: {picture: @picture.attributes.merge(url_small: @picture.photo_url_str(:small), url_large: @picture.photo_url_str(:medium), complete_url_small: @picture.complete_image_url(:small), complete_url_large: @picture.complete_image_url(:medium), aws_small_url: @picture.aws_url(:small), aws_original_url: @picture.aws_url)}
  end

  # Get /api/v2/users/user_info.json?token=iBSqRyQ5pxuxVb1dSz47
  def user_info
    api_version = @api_version
    # Get all basic info related to logged in user
    user_basic_info_data = User.user_basic_info(current_user,nil,api_version)
    user_basic_info_data[:status] = StatusCode::Status200
    respond_to do |format|
       format.json { render json: user_basic_info_data}
      # format.json { render :json=> {:setting_info=> info ,:cover_image=> cover_image, :member=>@current_user,:profile_image=> (@current_user.profile_image.complete_image_url("small") rescue ""), :aws_profile_image_url => (@current_user.profile_image.aws_url("small") rescue "") } }
    end
  end

  private
  def hh_and_basic_setup_evaluator_json(member,invitation_id=nil,options={:join_req_setup=>true})
    member.reload
    api_version = @api_version
    member_user_families = member.user_families
    json_data = {:families=>JSON.parse(Family.family_to_json(member_user_families,api_version,member) ), :user_basic_info=> User.user_basic_info(member,member_user_families,api_version),:status => StatusCode::Status200}
    version_controller,vc = VersionController.run(member)
    if vc
       json_data["NYVersionCheckerController"]= version_controller
    end
    join_setup_controller,@js_status = JoinReqSetup.run(member) if options[:join_req_setup]
    if @js_status
      json_data["NYJoinSetupController"] = join_setup_controller
      return json_data
    else 
      controllers,status = BasicSetupEvaluator.run(member,invitation_id,api_version)
      unless status["basic_setup"]
        handhold_data =  HandHold.user_complete_handhold(member)
        json_data["handhold_controllers"] = handhold_data
      end
      json_data["basic_setup_controller"] = controllers
      json_data["status"] = StatusCode::Status200
      json_data
    end
    
  end

    def verify_family_id(rnd_id)      
    rand_id = rnd_id
    if !Family.where(:family_id =>rand_id).first
      return rand_id
    else
      rand_id = "10000" + rand(99999).to_s
      verify_family_id(rand_id)
    end
  end

  def set_api_version
    @api_version = 2
  end
 
  def dashboard_data
    @user_family_member = current_user.family_members(FamilyMember::STATUS[:accepted])
    @families = current_user.user_families
    score = Score.where(:family_id.in=>@families.map(&:id)).sum(:point) + current_user.scores.where(:activity_type.in => ['User','Invitation']).sum(:point)
    @score = Score::badge(score)
    @childern_id = current_user.childern_ids
    @childern = current_user.childerns(@childern_id)
    @recent_timeline = current_user.recent_timeline(@childern_id).sample(1)
    @recent_health = current_user.recent_health(@childern_id).sample(1)
    @recent_milestone = current_user.recent_milestones(@childern_id).sample(1)
    @recent_vaccine = current_user.recent_immunisation(@childern_id).sample(1)
    @recent_picture =  current_user.recent_picture(@childern_id).sample(2)
    if @recent_timeline.blank? && @recent_health.blank? && @recent_milestone.blank? && @recent_picture.blank?
      @no_recent_item = true
    end
  end
end