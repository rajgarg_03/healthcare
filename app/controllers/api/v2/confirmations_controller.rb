class Api::V2::ConfirmationsController < ConfirmationsController #Devise::ConfirmationsController

# confirmation link verification
  def show
    user = User.find_by_verification_token(params[:confirmation_token]) 
     
    if user
      session[:mobile_verification] = true
      user.verify
      session[:tour_step] = 0 if user.sign_in_count <= 1
      sign_in(:user,user)
      redirect_to "/" and return true
    else
      redirect_to "/messages?verification_link_expired=true" and return true
    end
  end
end