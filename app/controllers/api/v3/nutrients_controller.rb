class Api::V3::NutrientsController < Api::V2::NutrientsController
 
 #api Get
  #http://localhost:3000/api/v3/nutrients.json?token=member_id=
  def index
    begin
      member = Member.find(params[:member_id])
      api_version = @api_version
      member_tools = Nutrient.get_member_tools(member,current_user,api_version).entries
      member_tools.map do |n| 
        n[:identifier] = n.get_identifier(member,{:api_version=>api_version})
        n[:title] = n.title
      end
      render json: member_tools
    rescue Exception=>e
      render json: {error:e.message,status: 100}
    end
  end

 # GET "http://localhost:3000/api/v3/nutrients/546128110a9a999a09000005/acitvate.json?token=XrjzH88mP4syK1q4MTeb&member_id=546128200a9a99178300000d"
  def acitvate
   begin
      api_version = @api_version
      tool = Nutrient.find(params[:id])
      member = Member.find(params[:member_id])
      family_id = member.member_families.last.family_id
      member_nutrient_obj = MemberNutrient.where(member_id:params[:member_id],nutrient_id:tool.id, position: tool.position).first_or_create
      member_nutrient = member_nutrient_obj.attributes
      member_nutrient['message'] = Message::API[:success][:acitvate_nutrient]
      member_nutrient['status'] = StatusCode::Status200
      Timeline.update_timeline_state_for_tool(member, tool.title,true)
    rescue Exception=>e
      member_nutrient = {'error'=>e.message}
      member_nutrient['message'] = Message::API[:error][:acitvate_nutrient]
      member_nutrient['status'] = StatusCode::Status100
    end
    respond_to do |format|
      format.json {render json: member_nutrient}
    end
  end

  # API-25:Add category tags in Tools
  #changed name change_category to categories
  # GET http://localhost:3000/api/v3/nutrients/categories.json?token=XrjzH88mP4syK1q4MTeb&member_id=546128200a9a99178300000d&category=Health
  def categories
    api_version = @api_version
    @member = Member.find(params[:member_id])
    @member_nutrients = Nutrient.get_member_tools(@member,current_user,api_version).entries
    if params[:category].present? &&  params[:category].downcase == "all" || params[:category].blank?
      @recommended_nutrients = @member.get_nutrients(api_version,current_user)
    else
      @recommended_nutrients = @member.get_nutrients(api_version,current_user).select{|n| n.categories.any?{|cat| cat==params[:category]}}
    end
    @tools = @recommended_nutrients.map(&:categories).flatten.uniq - ["Mind", "Education","Other"]
    respond_to do |format|
      format.json
    end 
  end

  private
  def set_api_version
    @api_version = 3
  end
end