 class Api::V3::DocumentsController < Api::V2::DocumentsController
   
  # GET http://localhost:3000/api/v1/documents.json?token=XrjzH88mP4syK1q4MTeb&member_id=546128200a9a99178300000d
 	def index
	  member = Member.where(id: params[:member_id]).first
	  @documents = member.documents if member
	  member["age"] = ApplicationController.helpers.calculate_age(member.birth_date) rescue ""
	  respond_to do |format|
	  	format.json
	  end
	end
   # GET http://localhost:3000/api/v3/documents/types.json
	def types
    render json: {status: StatusCode::Status200, document_types:Document::DOC_TYPES.values}	
	end

	# DELETE http://localhost:3000/api/v1/documents/54fa95c20a9a995f0c000002.json?token=XrjzH88mP4syK1q4MTeb
	def destroy
	  @document = Document.where(id: params[:id]).first
	  @document.destroy
	  respond_to do |format|
	    format.json
	  end
	end		
	# Get /api/v3/documents/get_doc_detail.json?token=XrjzH88mP4syK1q4MTeb&doc_id
	def get_doc_detail
	  @document = Document.where(id: params[:id]).first
	end

 end	