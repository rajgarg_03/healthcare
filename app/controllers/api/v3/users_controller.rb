 class Api::V3::UsersController < Api::V2::UsersController
  def sign_in
    super
  end

  # API-48-For pregnancy add a pregnancy progress bar on home page
  # Get /api/v3/users/user_dashboard.json?parent_member_id=542991865acefe0ac1000003&token=iBSqRyQ5pxuxVb1dSz47&event_type=past
  def user_dashboard
    begin
      # action cards
    data = @current_user.api_dashboard(false,@api_version).take(8)
    @childern_ids = current_user.childern_ids.map(&:to_s) + [current_user.id.to_s]
      #Calendar data
    events = Event.get_event_data("parent","upcoming",nil,@current_user.id).sort_by {|c| c[:start]}[0..1]
    event_data = []
    events.each do |data|
      event_data << {member_id:data[:member_id],name:data[:title],type:data[:type], start_date: data[:start], end_date:data[:end],start_time: (data[:datail]["start_time"].to_time.to_formatted_s(:time) rescue "00:00"),end_time: (data[:datail]["end_time"].to_time.to_formatted_s(:time) rescue "23:00")}
    end
     # Pointer data
    article_ref = ArticleRef.get_pointers_list(current_user,@childern_ids).order_by("effective_score desc").limit(2)
     #Preganacy widget
    pregnancy_widget = Pregnancy.pregnancy_widget(@current_user)
    status = StatusCode::Status200
    rescue Exception=> e
      @error = e.message
      status = StatusCode::Status100
    end
    respond_to do |format|
      format.json { render :json=> {:error=> @error, :status=> status, :recommended=> data, :event_widget=>event_data, :reference_widget=> article_ref,:pregnancy_widget=>pregnancy_widget} }
    end
  end
  
  # Api-68
  #Post
  #/api/v3/users/skip_push_screen.json?token= 
  #params {:device_token=>}
  def skip_push_screen
    begin
    device = Device.where(member_id:current_user.id.to_s,:device_token=>params[:device_token]).last
    device.screen_skip_count = device.screen_skip_count.to_i + 1
    device.screen_skip_up_to = Time.now.in_time_zone + 3.days
    device.save
    status = 200
    message = "Request processed sucessfully"
  rescue Exception=>e
    status = 100
    @error = e.message
    message = "Sorry! I could not fulfill your request due to an error. Please try again."
  end
    render :json=> { :status=>status, :message=> message, :error=>@error, :device_detail=> device }
  end

  # Get /api/v1/users/user_info.json?token=iBSqRyQ5pxuxVb1dSz47
  def user_info
    api_version = @api_version
    # Get all basic info related to logged in user
    user_basic_info_data = User.user_basic_info(current_user,nil,api_version)
    user_basic_info_data[:status] = StatusCode::Status200
    respond_to do |format|
       format.json { render json: user_basic_info_data}
    end
  end


   
  private
  def set_api_version
    @api_version = 3
  end

  def hh_and_basic_setup_evaluator_json(member,invitation_id=nil,options={:join_req_setup=>true})
    member.reload
    api_version = @api_version
    member_user_families = member.user_families
    json_data = {:families=>JSON.parse(Family.family_to_json(member_user_families,api_version,member) ), :user_basic_info=> User.user_basic_info(member,member_user_families,api_version),:status => StatusCode::Status200}
    version_controller,vc = VersionController.run(member)
    if vc
       json_data["NYVersionCheckerController"]= version_controller
    end
    join_setup_controller,@js_status = JoinReqSetup.run(member) if options[:join_req_setup]
    if @js_status
      json_data["NYJoinSetupController"] = join_setup_controller
      return json_data
    else 
      controllers,status = BasicSetupEvaluator.run(member,invitation_id,api_version)
      unless status["basic_setup"]
        handhold_data =  HandHold.user_complete_handhold(member)
        json_data["handhold_controllers"] = handhold_data
      end
      json_data["basic_setup_controller"] = controllers
      json_data["status"] = StatusCode::Status200
      json_data
    end
    
  end

  
end