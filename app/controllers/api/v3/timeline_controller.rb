 class Api::V3::TimelineController < Api::V2::TimelineController
        # GET
 	# /api/v2/timeline.json?token=XrjzH88mP4syK1q4MTeb&member_id=546128200a9a99178300000d
 	# /api/v2/timeline.json?token=XrjzH88mP4syK1q4MTeb&member_id=546128200a9a99178300000d&page=3
        #localhost:3000/api/v2/timeline.json?token=iBSqRyQ5pxuxVb1dSz47&member_id=5644443d5acefe40b4000019
 	#params {:tool=>["all"],:member_ids=['546128200a9a99178300000d'],:start_date=>"21/1/2016",:end_date=>"25/12/2016",:page=3
 
 	def index
 		if params[:member_id].present?
	 	  @timeline_member = Member.where(id:params[:member_id]).first
			member_ids = [params[:member_id]]
		else
	    member_ids = params[:member_ids].present? ? params[:member_ids] : current_user.childern_ids.map(&:to_s)
		end
	  begin
	      @status = 200
	      tool_list = {:Immunisations=>["Jab"], :health=>["Health"],:Measurements=>["Health"], :Documents=>["Document"],:Timeline=>["Timeline"],"Prenatal Tests"=>["prenatal_test","MedicalEvent"],:Milestones=>["Milestone"],:Pregnancy=>["Pregnancy"]}.with_indifferent_access
	      tool = tool_list.values_at(*[params[:tool]].flatten ).flatten.compact rescue nil
              selected_date = ("31/12/" + params[:end_date]).to_date rescue Date.today+1
              selected_date = selected_date < Date.today+1 ? selected_date : Date.today+1
	      if tool.present? && params[:tool].first != "all"
          if tool.include?("Timeline")
             tool << "Pregnancy"
             tool << nil
            @timelines= Timeline.includes(:member).in(member_id:member_ids,timelined_type: tool).where(:timeline_at.lte => selected_date)
          else
            @timelines= Timeline.includes(:member).in(member_id:member_ids, timelined_type:tool).where(:timeline_at.lte => selected_date)
	        end
	      else
	        @timelines= Timeline.includes(:member).in(member_id:member_ids).where(:timeline_at.lte => selected_date).order_by("timeline_at DESC").order_by("id DESC")
	      end
	      if @timelines.present? && params[:start_date].present? 
	        event_start_date = ("01/01/" + params[:start_date]).to_date
          event_start_date = event_start_date < Date.today+1 ? event_start_date : Date.today
		      @timelines = @timelines.where(:timeline_at.gte => event_start_date)
	      end
	      
	      if @timelines.present? && params[:end_date].present?
          timeline_end_date = ("31/12/" + params[:end_date]).to_date
          timeline_end_date = timeline_end_date < (Date.today + 1) ? timeline_end_date : Date.today+1
          @timelines = @timelines.where(:timeline_at.lte => timeline_end_date)
	      end
	      
	      @timelines = @timelines.where(:state=>true).order_by("timeline_at DESC").order_by("id DESC")
	      @current_page = (params[:page] || 1).to_i
	      @total_page,@next_page=Utility.page_info(@timelines, @current_page, 10)
	      @current_page = 0 if @total_page == 0
	      @timelines = @timelines.paginate(:page => (params[:page]), :per_page => 10).entries
	    rescue Exception=>e
	      @status = 100
	      @error = e.message
	    end
	  respond_to do |format|
      format.json
    end 
 	end
  
  # Get get_pre_define_timeline
  #/api/v3/timeline/get_pre_defined_timeline.json?token=gcjemgbLpVAjbjV4jyzs
  def get_pre_defined_timeline
    begin
      api_version = 3
      status = StatusCode::Status200
      title_data = PreDefineTimeline.get_predefine_timeline("all",current_user,api_version)
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
    end
      render json: {:status => status,:pre_defined_timeline_title=>title_data,:erorr=>@error}
  end

#merged into index.json . Will remove after verify response
  # Post
  # /api/v2/timeline/user_timeline.json?token=XrjzH88mP4syK1q4MTeb&member_id=546128200a9a99178300000d
  # /api/v2/timeline/user_timeline.json?token=XrjzH88mP4syK1q4MTeb
  #params {:tool=>["all"],:member_ids=['546128200a9a99178300000d'],:start_date=>"21/1/2016",:end_date=>"25/12/2016",:page=3
  #localhost:3000/api/v2/timeline/user_timelines.json?token=iBSqRyQ5pxuxVb1dSz47&tool=5644443d5acefe40b4000019
  def user_timeline
    begin
      @status = 200
      member_ids = params[:member_ids].present? ? params[:member_ids] : current_user.childern_ids.map(&:to_s)
      tool_list = {:Immunisations=>["jab"], :health=>["health"],:Measurements=>["health"], :Documents=>["document"],:Timeline=>["timeline"],"Prenatal Tests"=>["prenatal_test","preg"],:Milestones=>["milestone"],:Pregnancy=>["preg","prenatal_test"]}.with_indifferent_access
      tool = tool_list.values_at(*[params[:tool]].flatten ).flatten.compact rescue nil
      # @timeline_member = Member.in(id:member_ids).first
      selected_date = (params[:selected_date] || Time.zone.now).to_date + 1.day
      if tool.present? && params[:tool].first != "all"
        @timelines= Timeline.includes(:member).in(member_id:member_ids, entry_from:tool).where(:timeline_at.lte => selected_date)
      else
        @timelines= Timeline.includes(:member).in(member_id:member_ids).where(:timeline_at.lte => selected_date).order_by("timeline_at DESC").order_by("id DESC")
      end
      if @timelines.present? && params[:start_date].present? 
         @timelines = @timelines.where(:timeline_at.gte => ("01/01/" + params[:start_date]).to_date)
      end
      
      if @timelines.present? && params[:end_date].present?
        @timelines = @timelines.where(:timeline_at.lte => ("31/12/" + params[:end_date]).to_date)
      end
      
      @timelines = @timelines.where(:state=>true).order_by("timeline_at DESC").order_by("id DESC")
      @current_page = (params[:page] || 1).to_i
      @total_page,@next_page=Utility.page_info(@timelines, @current_page, 10)
      @current_page = 0 if @total_page == 0
      @timelines = @timelines.paginate(:page => (params[:page]), :per_page => 10).entries
    rescue Exception=>e
      @status = 100
      @error = e.message
    end
    respond_to do |format|
      format.json
    end 
  end
  private
   def set_api_version
     @api_version = 3
   end
 end	