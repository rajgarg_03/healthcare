class Api::V3::MemberController < Api::V2::MemberController
  

  def create_child
    begin
      @family = Family.find(params[:family_id])
      api_version = 3
      family_member = params[:Member].delete("family_members").merge!(family_id: @family.id)
      @child_member = ChildMember.new(params[:Member])
      @child_member.last_name =   @family.member.last_name rescue nil  
      if @child_member.save
        @child_member.add_default_nutrients(api_version,current_user)
        @child_member.family_members.create(family_member)
        @message = Message::GLOBAL[:success][:create_child].html_safe #"Your child has been added. Visit <a href='families' data-remote=true>Manage Family</a>."
        Timeline.save_system_timeline_to_member(@child_member.id,current_user.id) unless request.format.json?
        Health.new.health_setting(@child_member.id)
        Score.create(family_id: @family.id, activity_type: "Member", activity_id: @child_member.id, activity: "Added #{@child_member.first_name} to #{@family.name}", point: Score::Points["Child"], member_id: @child_member.id)
        
        message = Message::API[:success][:create_child] + " #{@family.name}"
        status = StatusCode::Status200
      else
        message =  @child_member.errors.full_messages.join(",")
        status = StatusCode::Status100
      end
       @child_member["message"],@child_member["status"] = message, status
       @child_member["age"] = ApplicationController.helpers.calculate_age(@child_member.birth_date) rescue ""
       @child_member["role"] = @child_member.family_members.first.role rescue "Son"
     rescue Exception=>e
      @error = e.message
      status = StatusCode::Status100
      message = Message::API[:error][:create_child] #@child_member.errors.full_messages.join(",")
     end
     respond_to do |format|
      format.js
      format.json { render :json=>{:message=>message,:error=>@error, :family_members=>[@child_member], :handhold_controllers => (HandHold.get_handhold_data(current_user, [@child_member.id],'new_child_added') rescue []) , status: status } }
    end

  end


   

  # Post api api/v3/member/set_metric_system.json?token=UpLZuMvytGdp8ejSsd5d
  # params={"subscibe_mails"=>{ :0 => "off" },:subscribe_notification=>off}
  def set_metric_system
    begin
      length_metric_system = current_user.member_settings.where(name:"length_metric_system").first
      weight_metric_system = current_user.member_settings.where(name:"weight_metric_system").first
      if params[:length_unit].present?
        if length_metric_system.blank?
          MemberSetting.create({member_id: current_user.id, name:"length_metric_system", status:params[:length_unit]})
        else
          length_metric_system.update_attributes({status:params[:length_unit]})
        end
      end
      if params[:weight_unit].present?
        if weight_metric_system.blank?
          MemberSetting.create({member_id: current_user.id, name:"weight_metric_system", status:params[:weight_unit]})
        else
          weight_metric_system.update_attributes({status:params[:weight_unit]})
        end
      end
      if params[:time_zone].present? && current_user.time_zone != params[:time_zone]
        current_user.update_attribute(:time_zone,params[:time_zone].strip)
      end  
      length_metric_system = current_user.member_settings.where(name:"length_metric_system").first
      weight_metric_system = current_user.member_settings.where(name:"weight_metric_system").first
      susbscription_mail_present = params[:subscibe_mails].present?
      params[:subscibe_mails].delete_if { |k, v| (v=="0"|| v=="off" || v=="false") } if susbscription_mail_present
      
      current_user.subscribe_mail((params[:subscibe_mails]||{}).keys) if susbscription_mail_present
      
      if params[:subscribe_notification].present?
        sub_noti = current_user.member_settings.where(name:"subscribe_notification").first ||  MemberSetting.create(name:"subscribe_notification",member_id:current_user.id)
        status_val = params[:subscribe_notification].downcase == "on" ? true : false
        sub_noti.update_attribute(:status,status_val)
        current_user.subscribe_notification(params[:subscribe_notification])
      end 
        current_user.reload
      respond_to do |format|
        format.js {render :layout=> false}
        format.json { render :json=> {:status=>200, :message =>Message::API[:success][:update_setting] ,:time_zone=>current_user.time_zone,:mailer_setting=>(MemberSetting.where(member_id:current_user.id, name:"mail_subscribe").first.note.present? rescue true),:notification=>current_user.notification_subscribed?, :metric_system=>{:length=>(length_metric_system.status rescue "kgs"),:weight=>(weight_metric_system.status rescue "cms")} } }
      end
     rescue  Exception => e
       render :json=> {:message=>Message::API[:error][:update_setting], :status=> 100, :error=>e}
     end
  end
 
   
  def create_elder
    @family = Family.find(params[:family_id])    
    @elder_member = ParentMember.where(:email => params[:ParentMember][:email].downcase).first
    if @elder_member.blank?
      @elder_member = ParentMember.new(params[:ParentMember].reject{|k| k == "family_members"})
    else 
      @elder_already_present = true
    end  
    @member_exists = FamilyMember.where(member_id: @elder_member.id, family_id: @family.id).present?
    if !@member_exists && @elder_member.save
      @invitation = Invitation.where(member_id: @elder_member.id, family_id: params[:ParentMember][:family_members][:family_id], status: Invitation::STATUS[:invited]).first
      if @invitation.blank?
        @invitation = @elder_member.invitations.new(params[:ParentMember][:family_members])
        @invitation.sender_type = "Family"
        @invitation.sender_id = current_user.id
        @invitation.status =  Invitation::STATUS[:invited]
        @invitation.save        
      end
      @message = "I have sent an email to the invitee" #"An email has been sent to the invitee. Visit <a href='families' data-remote=true>Manage Family</a>."
      message = Message::API[:success][:create_elder]
      UserMailer.elder_member_request_email(@invitation).deliver
      add_notification(@elder_member, "#{current_user.name} invited to you join his family #{@family.name}.", families_path, current_user,"family") if @elder_already_present
    else
      message = Message::API[:success][:elder_already_exist] + @family.name
    end
    @elder_member["age"] = ApplicationController.helpers.calculate_age(@elder_member.birth_date) rescue ""
    respond_to do |format|
      format.js  
      format.html 
      format.json { render :json => { :message=> message, :elder_member=>@elder_member ,:invitation=> @invitation, :status => StatusCode::Status200} }
    end  
  end

end
