class Api::V3::HealthController < Api::V2::HealthController

	def add_diff(health_records,convert_unit_val_list)
      data =[]
      record_member = health_records.first.member rescue nil
      health_records.each_with_index do |record,index|

       age = record_member.birth_date <= record.updated_at.to_date ? ApplicationController.helpers.calculate_age(record_member.birth_date,record.updated_at) : "0d" rescue "0d"
       record["age"] = age.gsub(" years","y").gsub(" year","y").gsub(" months","m").gsub(" month","m").gsub(" weeks","w").gsub(" week","w").gsub(" days","d").gsub("day","d").gsub("and","")
       next_record = (health_records[index+1]).api_convert_to_unit(convert_unit_val_list) rescue Health.new
       record = record.api_convert_to_unit(convert_unit_val_list)
       record["diff_height"] = (record["height"].to_f - next_record["height"].to_f).to_f.round(1)   rescue  record["height"].to_f.round(1) || "-" 
       record["diff_weight"] = (record["weight"].to_f - next_record["weight"].to_f).to_f.round(1)   rescue  record["weight"].to_f.round(1) || "-" 
       record["diff_head_circum"] = (record["head_circum"].to_f - next_record["head_circum"].to_f).to_f.round(1)   rescue  record["head_circum"].to_f.round(1) || "-" 
       record["diff_waist_circum"] = (record["waist_circum"].to_f - next_record["waist_circum"].to_f).to_f.round(1)   rescue  record["waist_circum"].to_f.round(1) || "-" 
       record["diff_bmi"] = (record["bmi"].to_f - next_record["bmi"].to_f).to_f.round(1)   rescue  record["bmi"].to_f.round(1) || "-" 
       record["bmi"] = record["bmi"].to_f.round(1)
       data << record
     end
     data
    end
end
