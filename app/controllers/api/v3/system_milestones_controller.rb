 class Api::V3::SystemMilestonesController < Api::V2::SystemMilestonesController
  
  def index
  	@member = Member.where(id: params[:member_id]).first
    @system_milestones = SystemMilestone.all
    respond_to do |format|
      # format.json { render :json => @system_milestones.group_by(&:category).to_json(methods: [:added]) }
      format.json
    end 
  end

end