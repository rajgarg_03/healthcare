class Api::V3::FamiliesController < Api::V2::FamiliesController 
  
  def index
    api_version = 3
    respond_to do |format|
      format.json { render :json=> {status: StatusCode::Status200,sent_invitations: current_user.sent_invitations_data, received_invitations: current_user.received_invitations_data, :families=>JSON.parse(Family.family_to_json(@families,api_version,current_user))} }
    end
  end 
  def update
    @family = Family.find(params[:id])
    respond_to do |format|
      if @family.update_attributes(params[:family])
        family = @family.attributes
        family["message"] = Message::API[:success][:update_family]
        family['status'] = 200
        format.json { render json: family.to_json, status: StatusCode::Status200  }
      else
         @family["message"] = Message::API[:error][:update_family]
        format.json { render json: @family.errors, status: StatusCode::Status100 }

      end
    end
  end
  #API-67 point-15
  #Post
  #/family/id/dont_want_add_spouse.json?token
  def dont_want_add_spouse
    begin
      family = Family.find(params[:family_id])
      current_user.skip_add_spouse(family.id.to_s)
      status = 200
      message ="Sucessfully proccess the request"
    rescue Exception=>e
      @error = e.message
      message = ""
      status =  100
    end
    render :json=>{:status=>status, :message=>message, :error=>@error }
  end

  def create
    @family = current_user.families.new(params[:family])
    #NOTE: params[:role] will be available by apis only. For web, it will be nil and set to administrator by default
    @family.add_family_member(params[:role]) 
    @message = ("Your family has been added. Visit <a href='families' data-remote=true>Manage Family</a>.".html_safe) if @family.save
    message = Message::API[:success][:create_famly]
    api_version = 3
    respond_to do |format| 
      format.json { render :json=> {family: JSON.parse(Family.family_to_json(@family,api_version,current_user)), message:message, status: StatusCode::Status200  } }
    end    
    find_families
  end

  def destroy
    begin
      @family = Family.find(params[:id])
      message = @family.delete_family(current_user)
      find_families
      respond_to do |format|
        format.json { render json: {message: message,status: StatusCode::Status200}, status: StatusCode::Status200  }
      end
    rescue Exception=> e
      render :json=> {:message=> e.message, :status=>500}
    end
  end 
end
