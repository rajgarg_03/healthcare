class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  layout false 
  def google_oauth2
    @auth = request.env['omniauth.auth']['credentials']
    user = current_user.user
    if @auth['refresh_token'].present? && @auth['token'].present?
      GoogleCal.where(user_id: user.id).delete
      GoogleCal.create(user_id: user.id,email: request.env['omniauth.auth']["info"]["email"], token: @auth['token'],refresh_token: @auth['refresh_token'],uid:request.env['omniauth.auth']["uid"])
      MemberSetting.where(member_id: current_user.id, name:"google_syn").delete 
      MemberSetting.create(member_id: current_user.id, name:"google_syn",status: "true")
      Delayed::Job.enqueue(SynEventWithGoogle.new(user.id))
    end
    redirect_to "/calendar"
  end

  # def get_orion_access_token_with_code
  def orion
    options = {:member_id=>params[:state], auth_code:params[:code],:create_deeplink_data=>true}
    access_token = nil
    token_type = nil
    referesh_token = nil
    login_failed_url = "/messages?orion_login_status=failed"
       
    deeplink,status  = ::Orion::LoginDetail.fetch_user_info(access_token,token_type,referesh_token,options)
      
    if status == 200
      redirect_to deeplink
    else
      @message = deeplink[:message]
      redirect_to login_failed_url
    end

  end
  
  def facebook
    Rails.logger.info "Facebook signin"
    Rails.logger.info session
    @platform = session[:alexa_platform] if session[:alexa_flow_status] == true
    # Post to facebook
    if current_user.present?
        timeline = Timeline.where(id: request.env["omniauth.params"]["timeline_id"]).first
        access_token = request.env["omniauth.auth"]['credentials']['token']
        @graph = Koala::Facebook::API.new(access_token)
        @graph.put_picture(view_context.timeline_image_url(timeline), {:message => request.env["omniauth.params"]["message"]}) rescue nil       
        flash[:notice] = Message::GLOBAL[:success][:facebook] #"Post updated on Facebook successfully."
           
      if session[:alexa_flow_status] == true
         (render :layout=> "layouts/alexa_signup") and return
      else
        redirect_to (timeline ? family_member_timeline_index_path(timeline.member.family_members.first.family.url_name,timeline.member_id) : dashboard_path) and return
      end
    end  
    if session[:alexa_flow_status] == true
      signup_source_details =  {:signup_source=>session[:alexa_platform],:signup_source_reference=>session[:alexa_source_reference]}
    else
      @platform = params[:platform]
      signup_source_details = {}
    end
    # You need to implement the method below in your model (e.g. app/models/user.rb)
    @user = User.from_omniauth(request.env["omniauth.auth"],signup_source_details)
    if @user.present? && (@user.persisted? && @user.account_type_allowed?("Facebook"))
      @user.skip_confirmation!
      if @user.sign_in_count == 0
        if session[:invitation_id].present?
          @invitation = Invitation.find(session[:invitation_id])
          @invitation.accept
          family_member = FamilyMember.create(member_id: @invitation.member_id, family_id: @invitation.family_id, role: @invitation.role)
          session[:invitation_id] = nil
          session[:invited_user] = true
        end
        session[:tour_step] = 0
      end
      set_flash_message(:notice, :success, :kind => "Facebook") if is_navigational_format?
      if session[:alexa_flow_status] == true
        session[:alexa_user_token] = @user.api_token(session[:alexa_flow_status])
        respond_to do |format|
          (render :layout=> "layouts/alexa_signup") and return
        end
      else
        sign_in(:user,@user)
        redirect_to root_path and return
      end
      # sign_in_and_redirect @user, :event => :authentication #this will throw if @user is not activated      
    else
      if (@user.present? && !@user.account_type_allowed?("Facebook")) || (@user.errors.added? :email, :taken)
        session[:reset_error] = "We already have an existing account using"
        session[:reset_email] = "#{@user.email}. Please login."
        session[:email] = @user.email

        if session[:alexa_flow_status] == true
          session[:alexa_user_token] = @user.api_token(session[:alexa_flow_status])
          respond_to do |format|
            (render :layout=> "layouts/alexa_signup") and return
          end
        end        
        
        redirect_to "/users/sign_in?pageName=login" and return
      else  
        session["devise.facebook_data"] = request.env["omniauth.auth"]
        if session[:alexa_flow_status] == true
          session[:alexa_user_token] = @user.api_token(session[:alexa_flow_status])
          respond_to do |format|
            (render :layout=> "layouts/alexa_signup") and return
          end
        end    
        redirect_to "/users/sign_in?pageName=signup"
      end  
    end
  end

  def failure
    if request.env["PATH_INFO"] == "/users/auth/google_oauth2/callback"
      redirect_to "/calendar" and return true
    end
    session[:signup_error] = "Permissions denied."
    if session[:alexa_flow_status] != true
      redirect_to "/users/sign_in?pageName=signup" and return
    end
  end  

end


# if session[:invitation_id].present?
#   @invitation = Invitation.find(session[:invitation_id])
#   member = ParentMember.where(:email => @invitation.member.email).first
#   member.update_attribute(:user_id,resource.id)
#   @invitation.accept
#   resource.verify
#   family_member = FamilyMember.create(member_id: @invitation.member_id, family_id: @invitation.family_id, role: @invitation.role)
#   session[:invitation_id] = nil
#   session[:invited_user] = true
# else
#   member = ParentMember.where(email: params[:user][:email]).first || ParentMember.create(email:params[:user][:email],user_id:resource.id)
#   member.update_attribute(:user_id,resource.id) if member.user_id.blank?
#   resource.status = User::STATUS[:unconfirmed]
# end