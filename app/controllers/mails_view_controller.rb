class MailsViewController < ApplicationController
  skip_before_filter :authenticate_user!, :check_verified_session

  def show
    params[:data] = Rack::Utils.parse_nested_query params[:params]
    @browser = true
    # calling method dynamically
    # TODO: security check
    send(params[:data][:mail])
  end

  private

  def get_class_name(class_string)
    class_string.classify.constantize rescue nil
  end
  
  def batch_mail_text_html
    class_name, user_notification = get_class_and_object
    @data =   UserNotification::EmailDataToSend.where(user_notification_id:user_notification.id,user_id:user_notification.user_id,member_id:user_notification.member_id)
    @browser = true
    render '/mails_view/batch_mail_text_html.html', layout: false
  end

  def get_class_and_object
    class_name = get_class_name(params[:data][:object_name])
    object_name = class_name.find(params[:data][:object_id])
    [class_name, object_name]
  end

  def welcome_email
    class_name, @user = get_class_and_object
    @email = @user.email
    render '/user_mailer/welcome_email.html', layout: false
  end

  def reset_password_instructions
    class_name, @resource = get_class_and_object
    @email = @resource.email
    render '/devise/mailer/reset_password_instructions.html', layout: false
  end

  def confirmation_instructions
    class_name, @resource = get_class_and_object
    @email = @resource.email
    @verify_link = @resource.account_verification_token
    render '/devise/mailer/confirmation_instructions.html', layout: false
  end

  # render template for elder_member_request_email
  def elder_member_request_email
    class_name, invitation = get_class_and_object
    elder_member_request_email_data(invitation)

    render '/user_mailer/elder_member_request_email.html', layout: '/updated_mailer_layout/updated_mailer'
  end

  # define variables for elder_member_request_email
  def elder_member_request_email_data(invitation)
    @invitation = invitation
    @invitee = @invitation.member rescue nil
    @user = @invitee.user
    @pregnancy = nil
    @email =  @invitee.email #rescue nil
    @family = @invitation.family
    @mother_for_pregnancy = false
    @role = @invitation.role.downcase
    @family_owner = @family.owner
    @url = verify_elder_for_family_member_url(@invitation)
  end

end
