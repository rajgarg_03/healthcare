class OrionController < ApplicationController
  skip_before_filter :authenticate_user!
  
  def initiate_user_authentication
    authorize_url = APP_CONFIG['orion_auth_base_url'] + "/oauth2/authorize"
    client_id = APP_CONFIG['orion_client_id']
    member_id = params[:member_id]
    scope = APP_CONFIG['orion_login_scope']
    callback_url = APP_CONFIG["default_host"]+ "/users/auth/orion"
    query_string = {state:member_id, :client_id=>client_id,:redirect_uri=>callback_url,:response_type=>"code"}.to_query
    redirect_url = authorize_url + "?" + query_string
    redirect_to redirect_url and return
  end

  def orion_authentication_completed
    render :layout=>false
  end

  def user_info
  access_token = params[:access_token]
  begin
    user_info = ::Orion::LoginDetail.fetch_user_info(access_token,"Bearer")
    render json:user_info
  rescue Exception=>e
    Rails.logger.info e.message
    render :json=>{:error=>e.message,:message=>::Message::NhsLogin[:error_message],status:100}
  end
end

private
  def set_api_version
    @api_version = 5
  end
end