class UsersController < ApplicationController
  before_filter :authenticate_user!
  # skip_before_filter :authenticate_user!, :only => [:resend_confirmation_link, :after_registration, :accept_invitee_to_group, :verify_group_member, :fetch_location]
  skip_before_filter :authenticate_user!, :only => [:alexa_login, :set_default_cover_image, :resend_confirmation_link, :after_registration, :accept_invitee_to_group, :verify_group_member, :nhs_authentication_completed]

  def dashboard
    dashboard_data
    respond_to do |format|
      format.html # show.html.erb
      format.js{render :layout=> false}
    end
  end

  def after_registration
    if current_user && current_user.user.status != "approved"
      session.clear
      @status = "unapproved"
    end    
    flash[:notice] =  Message::DEVISE[:m9] if params[:email] #"No user found with this email"
    render :layout=> "login"
  end


  def alexa_login
    session.clear
   render :layout=> "login"
  end
  
  def home_page
    dashboard_data
    respond_to do |format|
      format.html # show.html.erb
      format.js{render :layout=> false}
    end
  end

  def nhs_authentication_completed
    render layout: false
  end

  def resend_confirmation_link
    if current_user
      Rails.logger.info "current user activated"
      Rails.logger.info "User before sending ....#{ current_user.user.inspect}  ...."
      current_user.user.send_confirmation_instructions
      Rails.logger.info "User after sending ....#{ current_user.user.inspect}  ...."
      @message = Message::DEVISE[:m8] #"Verification email has been sent to your email."
      #render text: "Sent" and return true
    else
      begin
        Rails.logger.info "sending for email"
        params[:email] = params[:email].downcase
        user = User.where(email:params[:email]).first
        Rails.logger.info "User before sending ....#{ user.inspect}  ...."
        user.send_confirmation_instructions
        user.save
        Rails.logger.info "User after sending ....#{ user.inspect}  ...."
      rescue Exception => e
        Rails.logger.info 'Error inside resend_confirmation_link'
        Rails.logger.info e.message
        redirect_to after_registration_path(email:params[:email]) and return true
      end
      respond_to do |format|
        format.html{ redirect_to home_messages_path(email_resent: true) and return true  }
        #"We have resent the verification email to you. Do check your spam folder, if you haven't received the email"
        format.json{render :json=>  {:message=>Message::DEVISE[:m10] ,:status => StatusCode::Status200 } and return true}
      end
    end
    respond_to do |format|
      format.html
      format.js
      format.json{render :json=>  {:flash=> Message::DEVISE[:m8], :message=>Message::DEVISE[:m10] ,:status => StatusCode::Status200 }}
    end
  end
  
  def change_email
    sec_email = current_user.secondary_email
    user = current_user
    if sec_email.status
      if params[:primary_email] == sec_email.email
        sec_email.update_attributes("email"=>current_user.email, :status=>1)
        user.set(:email,params[:primary_email])
        user.email = params[:primary_email]
        user.save
      end
    end
    respond_to do |format|
      format.js { render :json=>{:pe =>current_user.email, :se=>sec_email.email, :status=>200} }
    end
  end


  def sec_email_add
    @success = false
    token = Digest::MD5.hexdigest(params[:secondary_emails][:email])
    SecondaryEmail.where(:user_id=>current_user.id).delete    
    @sec_email = current_user.build_secondary_email(params[:secondary_emails].merge(:confirmation_token => token))
      if @sec_email.save()
          url = request.host_with_port+"/users/verify_secondary_email?token=#{token}"
          UserMailer.secondary_email(@sec_email, "Nurturey-Email Verification", url, Time.now).deliver
          @success = true
      end

      respond_to do |format|
      format.json  { render :json=>{:email=>params[:secondary_emails][:email], :head => :ok} }
    end
  end

  def verify_secondary_email
    email = SecondaryEmail.where(:confirmation_token => params[:token]).first
    if email && !email.confirmation_token.nil?
      email.confirmation_token = nil
      email.status = 1
      email.save
      flash[:notice] = Message::DEVISE[:m11] #"Secondary email verified."
      redirect_to root_path
    end
  end
  
  def settings
    @families = current_user.user_families
    score = Score.where(:family_id.in=>@families.map(&:id)).sum(:point) + current_user.scores.where(:activity_type.in => ['User','Invitation']).sum(:point)
    @score = Score::badge(score)
    mail_setting = MemberSetting.where(member_id:current_user.id, name:"mail_subscribe").first
    mail_setting = mail_setting || MemberSetting.create(member_id:current_user.id, name:"mail_subscribe",note:MemberSetting::MailerType.keys.join(","),status: "subscribe")
    @subscribe_mail_ids = mail_setting.note.split(",") rescue MemberSetting::MailerType.keys.map(&:to_s)
    respond_to do |format|
      format.js { render :layout=>false}
    end
  end

  def sec_email_del
    @success = false
    @empty = true
    if !current_user.secondary_email.nil?
      if current_user.secondary_email.destroy
        @success = true
        @empty = false
      end
    end
      respond_to do |format|
      format.json  { render :json=>{:status=>@success, :head => :ok} }
    end
  end


  def associate_to_family
    @family = Family.find_by_family_id(params[:family_id].to_i)    
    @to_user = @family.user.first_name if !@family.blank?
    if FamilyMember.find_by_email_and_family_id(current_user.email,params[:family_id])
      flash[:notice] = Message::DEVISE[:m12] #"This email is already assosiated to this family. "
    else
      if !@family.blank?
        @family_member = FamilyMember.create(:first_name => current_user.first_name,
          :last_name => current_user.last_name,
          :email => current_user.email,
          :confirm_email => current_user.email,
          :role_name => params[:role_name],
          :family_id =>  @family.id,
          :user_id => current_user.id,
          :status => "waiting",
          :email_verification_hash=> SecureRandom.hex + Time.now.to_i.to_s)
        UserMailer.family_associate_request_email(@family_member,@to_user).deliver
        flash[:notice] = Message.m06(@family.id)
      else
        flash[:notice] = Message::GLOBAL[:error][:associate_to_family] #"Family ID not found." 
      end
    end    
    redirect_to :action => :dashboard and return
  end


    def index
      redirect_to root_url unless current_user.email=="gentle0219@gmail.com"
      @users = User.all
    end

    def destroy
      user = User.find(params[:id])
      unless user == current_user
        user.destroy
        redirect_to root_path, :notice => Message::GLOBAL[:success][:user_destroy]
      else
        redirect_to root_path, :notice => Message::GLOBAL[:error][:user_destroy] #"Can't delete yourself."
      end
    end

  # group functionality

  def edit_cover_image
    @user = current_user.user
    @user.build_cover_image if @user.cover_image.blank?
    render layout: false
  end

  def set_default_cover_image
    begin
      user = current_user.user
      user.cover_image.delete
      message = "Default cover image is set successfully."
      status = StatusCode::Status200
    rescue Exception=> e
      message = Message::API[:success][:set_default_cover_image]
      status = StatusCode::Status100
      @error = e.message
    end
    render :json=>{:message=>message, :status=> status,:error=>@error}
  end  

  def update_cover_image
    @user = current_user.user
    @user.update_attributes(params[:user])
    # @user.cover_image = Picture.new(local_image:params[:user_cover_iamge],upload_class: "coverimage", member_id:current_user.id)
    # debugger
    # if @pic_valid = Picture.new(local_image: params[:user][:cover_image_attributes][:local_image]).valid?
    if @pic_valid = @user.cover_image.valid?
      crop
      @user.cover_image.reprocess_image
    end  
  end

  def crop
    # selected_member =  @member.class.to_s.underscore  
    # coord = params[selected_member]["profile_image_attributes"]
    coord = params[:user][:cover_image_attributes]
    local_image = params[:user][:cover_image_attributes].delete("local_image")
    image = @user.cover_image
    # debugger
    local_image1 = local_image ||  URI.parse(image.photo_path_str(:original)) if !FileTest.exist?(image.local_image.path)
    if local_image1.present?
      image.local_image= local_image1
      image.local_image.save
    end
    File.open(image.photo_path_str(:original), "rb") do |f|
      @orig_img = Magick::Image::from_blob(f.read).first
    end
    width = @orig_img.columns > 504 ? 504 : @orig_img.columns
    height = @orig_img.rows > 285 ? 285 : @orig_img.rows
    @orig_img.resize!(width,height) if (width == 504 || height == 285)
    @orig_img.crop!(coord["crop_x"].to_i,coord["crop_y"].to_i , coord["crop_w"].to_i, coord["crop_h"].to_i)
    @orig_img.write(image.local_image.path(:original))
  end

  def delete_cover_image
    @user = current_user.user
    @user.cover_image.delete # = Picture.new(local_image:params[:user_cover_iamge],upload_class: "coverimage", member_id:current_user.id)
    respond_to do |format|
      format.js { render :layout=>false}
    end
  end

  # def fetch_location
  #   # session[:user_country] = (request.location.country_code.downcase rescue 'uk') #unless (current_user && current_user.user.country_code)
  #   session[:user_country] = Geocoder.search(request.remote_ip).first.data[:country_code2].downcase rescue 'uk'
  #   respond_to do |format|
  #     format.js { render :text => "done", :layout=>false}
  #   end
  # end  

  private
  def verify_family_id(rnd_id)      
    rand_id = rnd_id
    if !Family.where(:family_id =>rand_id).first
      return rand_id
    else
      rand_id = "10000" + rand(99999).to_s
      verify_family_id(rand_id)
    end
  end
 
  def dashboard_data
    # @country_code = (current_user.user.country_code || request.location.try(:country_code) || 'uk').downcase
    @user_family_member = current_user.family_members(FamilyMember::STATUS[:accepted])
    @families = current_user.user_families
    score = Score.where(:family_id.in=>@families.map(&:id)).sum(:point) + current_user.scores.where(:activity_type.in => ['User','Invitation']).sum(:point)
    @score = Score::badge(score)
     
    @childern_id = current_user.childern_ids
    @childern = current_user.childerns(@childern_id)
    @recent_timeline = current_user.recent_timeline(@childern_id).sample(1)
    @recent_health = current_user.recent_health(@childern_id).sample(1)
    @recent_milestone = current_user.recent_milestones(@childern_id).sample(1)
    @recent_vaccine = current_user.recent_immunisation(@childern_id).sample(1)
    @recent_picture =  current_user.recent_picture(@childern_id).sample(2)
    if @recent_timeline.blank? && @recent_health.blank? && @recent_milestone.blank? && @recent_picture.blank?
      @no_recent_item = true
    end
  end

end
