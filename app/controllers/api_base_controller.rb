class ApiBaseController < ActionController::Base

  before_filter :token_verify
  before_filter :log_request

  around_filter :set_time_zone
  alias_method :api_sign_in, :sign_in
  rescue_from Exception, with: :show_exception
  # before_filter :validate_user_gpsoc_limit
 
  def authorization_status(current_user)
    status = true 

    if params[:member_id].present? || params[:family_id].present?
      family_ids = FamilyMember.where(member_id:current_user).pluck(:family_id).map(&:to_s)
      member_ids = FamilyMember.where(:family_id.in=>family_ids).pluck(:member_id).map(&:to_s)
      pregnnacy_ids = Pregnancy.where(:expected_member_id.in=>member_ids).pluck(:id).map(&:to_s)
      if (params[:family_id].present? && params[:family_id].to_s.length > 10 ) && !family_ids.include?(params[:family_id])
        status =  false 
      elsif params[:member_id].present? && !member_ids.include?(params[:member_id]) && current_user.id.to_s != params[:member_id]
        status =  false 
      elsif params[:pregnancy_id].present? && !pregnnacy_ids.include?(params[:pregnancy_id])
        status = false
      end
    end
     # check if user present? Fix for android
    if status == false && params[:member_id].present?
      status = (User.find(params[:member_id]).present?  rescue false)
    end
    status 
  end

  #  Validate user not exceed  allowed request gpsoc count limit
  def validate_user_gpsoc_limit
    if @current_user.present?
      @current_user.gpsoc_api_requested_ip = request.remote_ip
      @current_user.save(validate:false)
      if !@current_user.clinic_services_active?
        render status:200, :json=> {:status=> 401,:error=>::Message::Clinic[:clinic_access_blocked],:message=>::Message::Clinic[:clinic_access_blocked]} and return true
      elsif @current_user.is_gpsoc_api_request_limit_exceed?
        render status:200, :json=> {:status=> 429,:error=>::Message::Clinic[:clinic_access_limit_exceeded],:message=>::Message::Clinic[:clinic_access_limit_exceeded]} and return true
      end
    end
  end

  def check_clinic_account_active
    if @current_user.present?
      if !@current_user.clinic_services_active?
        render status:200, :json=> {:status=> 401,:error=>::Message::Clinic[:clinic_access_blocked],:message=>::Message::Clinic[:clinic_access_blocked]} and return true
      end
    end
  end

  def token_verify
    begin
      api_controller_action = params[:controller].split("/")[1]
      @apiVersion =  api_controller_action == "v4" ? 4 : (api_controller_action.gsub("v","").to_i rescue 1)
      Rails.logger.info "Base Api version is( Token Verify)...............................................#{@apiVersion}"
      Rails.logger.info "Token is (TOken Verify)............................................ #{request.headers["HTTP_TOKEN"]}"
    
      if @apiVersion >= 6
        token = request.headers["HTTP_TOKEN"]
        refresh_token = request.headers["HTTP_REFRESHTOKEN"] || params[:refresh_token]
        params[:refresh_token] = refresh_token
      else
        token = params[:token]
        refresh_token = params[:refresh_token]|| params[:authentication_token] || params[:auth_token] 
      end
      Rails.logger.info "Key is(Base Api user)............................................ #{refresh_token}"
      user = User.find_user_with_token(token,refresh_token,@apiVersion).first
      begin
      if user["app_login_status"].blank?
        user["app_login_status"] = true
        user.save
      end
      rescue
      end
      @current_user = user.member rescue nil
      Rails.logger.info "Member is(Base Api)............................................ #{@current_user.id}" rescue nil
      if user.blank?
        if ["unread_count","user_info", "user_sign_out","enable_push_notification","update_subscription_receipt"].include?(params[:action])
          Rails.logger.info "Not supproted token"
          params[:token] = token
          render status:200, :json=> {:status=> 401,:screen_name=>"login_screen",:error=>"",:message=>"Session expired"} and return true
        elsif params[:action] == "mark_user_notification_responded"
          params[:refresh_token] = token
        
        else
          Rails.logger.info "Unauthorized for action #{params.inspsct}"
          render status:401, :json=> {:status=> 401,:screen_name=>"login_screen",:error=>"Invalid token",:message=>::Message::DEVISE[:m22]} and return true
        end
      elsif user.get_account_status == "Blocked"
          render status:200, :json=> {:status=> 401,:screen_name=>"login_screen",:error=>"Account blocked by admin",:message=>::Message::DEVISE[:m23]} and return true    
      elsif  !User.can_access_dashboard_after_signup?(user,@apiVersion)
        render :json=> {:status=> 100,:screen_name=>"email_verification_screen",:message=>Message::DEVISE[:m1]} and return true
      elsif !authorization_status(@current_user)
        render :json=> {:status=> 100,:error=>"",:message=>"Unauthorized access"} and return true
      else
          params[:token] = token
      end
    rescue Exception=>e 
        render status:401, :json=> {:status=> 401,:screen_name=>"login_screen",:error=>"Invalid token",:message=>Message::DEVISE[:m22]} and return true
    end
  end
  

  def log_request
    begin
      api_controller_action = params[:controller].split("/")[1]
      @apiVersion =  api_controller_action == "v4" ? 4 : (api_controller_action.gsub("v","").to_i rescue 1)
      if @apiVersion >= 6
        token = request.headers["HTTP_TOKEN"]
        refresh_token = request.headers["HTTP_REFRESHTOKEN"] || params[:refresh_token]
        params[:refresh_token] = refresh_token
      else
        token = params[:token]
        refresh_token = params[:refresh_token]|| params[:authentication_token] || params[:auth_token] 
        # params[:refresh_token] = refresh_token
      end
      # Rails.logger.info "Key is(Log Request)........................ ....   #{refresh_token}"
      # Rails.logger.info "token is(Log Request) ..........................   #{token}"
   
      options = {:ip_address=>request.remote_ip, :request_type=>request.method,:request_format=>(request.format.symbol.to_s rescue nil)}
      member = current_user || (User.or({confirmation_token: refresh_token},{api_access_token:token},{email:params[:email]}).last.member rescue nil)
      #log event info
      report_event = Report::Event.get_event_data(params[:controller], params[:action]) rescue nil
      member_id = Report::ApiLog.get_key_value(params,"member_id")
      pregnancy_id = Report::ApiLog.get_key_value(params,"pregnancy_id") || Report::ApiLog.get_key_value(params,"preg_id")
      options[:member_id] = member_id
      options[:pregnancy_id] = pregnancy_id
      options[:family_id] = Report::ApiLog.get_key_value(params,"family_id")
      if member.blank?
        Report::ApiLog.create_record(email=nil,params,options) 
        Report::ApiLog.log_event(user_id=nil,family_id=nil,report_event.custom_id,options)  if report_event.present?
      else
        if member.class.to_s == "User"
          member = member.member
        end
        options[:current_user] = member
        member.delay.log_api(params,options) rescue nil
        begin
          member.delay.log_event(family_id=nil,report_event.custom_id,options) if report_event.present?
        rescue Exception=>e 
        end
      end
    rescue Exception=>e 
      Rails.logger.info "Api Request log is failing....................."
      Rails.logger.info e.message
    end
    Mongoid.override_database(APP_CONFIG["database_name"])
  end

  
  def info_popup_status(member,tool_name)
    tool_info = member.member_settings.where(name:"display_#{tool_name}_info").first || member.member_settings.create(name:"display_#{tool_name}_info", status:"true")
    if tool_info.status != "false"
      eval("@display_#{tool_name}_info = true")
      tool_info.update_attribute(:status, "false")
    end
  end

  def decode_and_save_image(record_obj,image_data)
    member = record_obj.member    
    content = image_data
    content.gsub!('\\r', "\r")
    content.gsub!('\\n', "\n")
    decode_base64_content = Base64.decode64(content) 
    File.open("public/pictures/#{record_obj.class.name.downcase}_#{record_obj.id}.jpg", "wb") do |f|
      f.write(decode_base64_content)
    end
    file = File.open("public/pictures/#{record_obj.class.name.downcase}_#{record_obj.id}.jpg")
    picture = Picture.new(local_image:file,upload_class: record_obj.class.name.downcase,member_id:member.id)
    if picture.valid? && record_obj.pictures << picture
      @picture_saved = true        
    else
      @picture_saved = false
    end
    File.delete("public/pictures/#{record_obj.class.name.downcase}_#{record_obj.id}.jpg")
  end

  def add_notification(member, title, link, sender,type)
    member.notifications.create(:title => title, :link => link, sender: sender.id, type:type)
  end

protected
    def show_exception(exception)
      UserMailer.delay.notify_dev_for_error(exception.message,exception.backtrace,params,"Error code 500")
      if exception.message == "Invalid token"
        status = StatusCode::Status401
        message = ::Message::DEVISE[:m22]
        screen_name = "login_screen"
        http_status = 401
      elsif exception.message == "Account blocked by admin"
        status = StatusCode::Status401
        message = ::Message::DEVISE[:m23]
        screen_name = "login_screen"
        http_status = 200
      else
        status = StatusCode::Status100
        message = ""
        screen_name = ""
        http_status = 200
      end
      respond_to do |format|
        format.json {render status:http_status, json: {status: status, screen_name:screen_name ,message:message, error:exception.message}}
        format.html
        format.js
      end
    end
  private


  def validate_email_status
    # check if user email is confirmed or approved 
    if  !current_user.user.email_verification_status
      render :json=> {:status=> 100,:screen_name=>"email_verification_screen",:message=>Message::DEVISE[:m18]} and return true
    end
  end

  def set_time_zone
    begin
      if current_user
        if current_user.time_zone.present?
          @time_zone = current_user.time_zone
        elsif cookies && cookies[:browser_timezone].present?
          @time_zone = cookies[:browser_timezone]
          @time_zone = "Kolkata" if cookies[:browser_timezone] == "ASIA/KOLKATA"
        end
        begin
          Time.zone = @time_zone 
        rescue Exception=>e 
          @time_zone = "Kolkata"
        end
        current_user.update_attribute(:time_zone,@time_zone) 
        time_zone = current_user.time_zone
      else
        time_zone =  "Kolkata"
      end
    rescue
      time_zone =  "Kolkata"
    end
    
    Time.use_zone(time_zone) { yield } 
  
  end  


end