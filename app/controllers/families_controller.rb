class FamiliesController < ApplicationController  
   
  before_filter :find_families, only: [:index, :join_this_family, :search_family]
  skip_before_filter  :verify_authenticity_token, only: :destroy

  def index
    api_version = 1
    respond_to do |format|
      format.js {render :layout=> false}
      format.html # index.html.erb
      format.json { render :json=> {sent_invitations: current_user.sent_invitations_data, received_invitations: current_user.received_invitations_data, :families=>JSON.parse(Family.family_to_json(@families,api_version,current_user))} }
    end
  end

  def join_family
    render :layout=>false
  end

  def search_family    
    # ToDo: search by admin's email
    assoc_family_ids = @families.map(&:id)
    if params[:family_uid]
      @families = Family.where(:id.nin => assoc_family_ids).where(:family_uid => params[:family_uid])
    else
      @parent_member_ids = ParentMember.where(email: /#{params[:email]}/).only(:id).map(&:id)
      @families = Family.where(:id.nin => assoc_family_ids, :member_id.in => @parent_member_ids)
    end
    respond_to do |format|
      format.js {render :layout=> false}
      format.html {render :layout=> false}
      format.json { render :json => {:families=>JSON.parse(@families.to_json({include_owner_details: true})), :status => StatusCode::Status200} }
    end
     
  end

 
  def join_this_family
    begin
      @family = Family.find(params[:id])
      invitation_params = params[:Invitation].merge!(member_id: current_user.id)    
      @invitation = Invitation.where(invitation_params).first || Invitation.new(invitation_params)
      @invitation.member_id = current_user.id
      @invitation.role = params[:role] if params[:role].present?
      add_notification(@family.owner, "#{current_user.name} requested you to join #{@family.name}.", families_path, current_user,"family") #if @invitation.new_record?
      @invitation.save
      UserCommunication::UserActions.new.family_owner_received_join_request(current_user,@invitation)
      message = Message::API[:success][:join_family_request]
      status = StatusCode::Status200
    rescue Exception=>e
      @error = e.message
      message = ""
      status = StatusCode::Status100
    end
    respond_to do |format|
      format.js 
      format.html
      format.json { render :json => {:error=> @error, :message=>message, :family=>@family ,:invitation=>@invitation, :status => status} }
    end
    UserMailer.join_family_request_email(@invitation).deliver
  end

  def select_role
    @invitation = Invitation.find(params[:invitation_id])
    render layout: false
  end

  def save_role
    status = StatusCode::Status100
    @invitation = Invitation.find(params[:invitation_id])
    if @invitation.update_attributes(params[:invitation])
      @message = "Your joining request has been sent to the creator of the family. Visit <a href='families' data-remote=true>Manage Family</a>.".html_safe
      message = Message::API[:success][:save_role]
      status = StatusCode::Status200
    end  
    respond_to do |format|
      format.js 
      format.html
      format.json { render :json => {:message=> message ,:invitation=>@invitation, :status => status} }
    end
  end

  def show
    @family = Family.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @family,status: StatusCode::Status200  }
    end
  end

  def new
    @family = current_user.families.build
    render :layout=>false
  end

  def edit
    @family = Family.find(params[:id])
    render :layout=>false
  end

  def create
    api_version = 1
    @family = current_user.families.new(params[:family])
    @family.save
    #NOTE: params[:role] will be available by apis only. For web, it will be nil and set to administrator by default
    options = {:ip_address=>request.remote_ip}
    @family.add_family_member(params[:role],options) 
    @message = ("Your family has been added. Visit <a href='families' data-remote=true>Manage Family</a>.".html_safe) if @family.save
    message = Message::API[:success][:create_famly]
    find_families
    respond_to do |format| 
      format.html { redirect_to "/dashboard" and return true if session[:dashboard] }
      format.js { redirect_to "/dashboard" and return true if session[:dashboard] }
      format.json { render :json=> {family: JSON.parse(Family.family_to_json(@family,api_version,current_user)), message:message, status: StatusCode::Status200  } }
    end    
  end

  def update
    @family = Family.find(params[:id])
    respond_to do |format|
      if @family.update_attributes(params[:family])
         @family["message"] = Message::API[:success][:update_family]
        format.html { redirect_to @family, notice: Message::GLOBAL[:success][:family_update] }
        format.js 
        format.json { render json: @family.to_json, status: StatusCode::Status200  }
      else
         @family["message"] = Message::API[:error][:update_family]
        format.html { render action: "edit" }
        format.js
        format.json { render json: @family.errors, status: StatusCode::Status100 }

      end
    end
  end

  def destroy
    begin
      @family = Family.find(params[:id])
      options= {:api_version=>@api_version}
      message = @family.delete_family(current_user,options)
      find_families
      respond_to do |format|
        format.html
        format.js 
        format.json { render json: {message: message}, status: StatusCode::Status200  }
      end
    rescue Exception=> e
      render :json=> {:message=> e.message, :status=>500}
    end
  end

  private
  
    def find_families
      @sent_family_invitations = current_user.sent_join_family_invitations # Needed in view at multiple places
      @received_family_invitations = current_user.received_join_family_invitations
      @families = current_user.user_families      
    end  
end
