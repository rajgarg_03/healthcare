class HealthController < ApplicationController
  def index
    @member = Member.find(params[:member_id])
    @health_records = @member.health_records.order_by("updated_at desc")
    #@last_health_record = @health_records.first
    @last_health_record = @member.health_records.order_by("id desc").first
    @recently_updated_record = @member.health_records.order_by(:last_updated_at.desc,:updated_at.desc).first
    values = ["", nil,"0","0.0"]
    @health_unit = Health.health_unit(current_user.metric_system)
    @convert_unit_val_list = {}
    @convert_unit_val_list["length_unit"] = "inches" if current_user.metric_system["length_unit"] == "inches"
    @convert_unit_val_list["weight_unit"] = "lbs" if current_user.metric_system["weight_unit"] == "lbs"
    @last_height_record = @member.health_records.not_in(:height => values).order_by("updated_at desc").first
    @last_weight_record = @member.health_records.not_in(:weight => values).order_by("updated_at desc").first
    @last_head_circum_record = @member.health_records.not_in(:head_circum => values).order_by("updated_at desc").first
    @last_waist_circum_record = @member.health_records.not_in(:waist_circum => values).order_by("updated_at desc").first
    @last_bmi_record = @member.health_records.not_in(:bmi => values).order_by("updated_at desc").first
    info_popup_status(@member,"health")
    respond_to do |format| 
      format.html
      format.js
      format.json { render :json=> {:member=> @member,:health_record =>add_diff(@health_records,@convert_unit_val_list) , 
        :unit_selected=>{:lenght=>(@convert_unit_val_list["length_unit"] || "cms"),
        :weight=> (@convert_unit_val_list["weight_unit"] || "kgs")},
        :last_updated_record => {:bmi => (@last_bmi_record.api_convert_to_unit(@convert_unit_val_list) rescue Health.new)["bmi"].to_f.round(1),:head_circum => (@last_head_circum_record.api_convert_to_unit(@convert_unit_val_list) rescue Health.new)["head_circum"] ,:height=>(@last_height_record.api_convert_to_unit(@convert_unit_val_list) rescue Health.new)["height"] ,
        :weight => ((@last_weight_record.api_convert_to_unit(@convert_unit_val_list))["weight"] rescue ""), :waist=>(@last_waist_circum_record.api_convert_to_unit(@convert_unit_val_list) rescue Health.new)["waist_circum"] },:last_record=>(@last_health_record.api_convert_to_unit(@convert_unit_val_list) rescue nil),:recently_updated_record=>(@recently_updated_record.api_convert_to_unit(@convert_unit_val_list) rescue nil), :status=> StatusCode::Status200}  }
    end
  end

  def get_detail
    @member = Member.find(params[:member_id])
    @health_setting =  @member.member_settings.where(name:"health_setting").first
    health_param = params[:data_type]
    @chart_param = health_param.camelcase
    @health_unit = Health.health_unit(current_user.metric_system)
    @convert_unit_val_list = {}
    @convert_unit_val_list["length_unit"] = "inches" if current_user.metric_system["length_unit"] == "inches"
    @convert_unit_val_list["weight_unit"] = "lbs" if current_user.metric_system["weight_unit"] == "lbs"
    @chart_unit = @health_unit[health_param.to_s] #Health::HEALTH_UNIT[current_user.metric_system][health_param.to_s]
    if params[:data_type] =="all"
      values = ["", nil,"0","0.0"]
      @last_height_record = @member.health_records.not_in(:height=> values).order_by("updated_at desc").first
      @last_weight_record = @member.health_records.not_in(:weight => values).order_by("updated_at desc").first
      @last_head_circum_record = @member.health_records.not_in(:head_circum => values).order_by("updated_at desc").first
      @last_waist_circum_record = @member.health_records.not_in(:waist_circum => values).order_by("updated_at desc").first
      @last_bmi_record = @member.health_records.not_in(:bmi => values).order_by("updated_at desc").first
 
    end
       
    @health_records = @member.health_records.order_by("updated_at desc")
   
    @chart_update = @health_records.not_in(params[:data_type] => values).collect{|rec| [get_month(rec.updated_at.to_date),rec[health_param].to_f]}
    unless params[:data_type] =="all"
      @chart_update = @health_records.not_in(params[:data_type] => values ).collect{|rec| [get_month(rec.updated_at.to_date),convert_to_unit(rec[health_param],@chart_unit)]}
      @health_records = @member.health_records.not_in(params[:data_type] => values).order_by("updated_at desc")
      #@last_health_record = @health_records.first
      @last_health_record = @member.health_records.order_by("id desc").first
      data = get_who_data(params[:data_type],@member.member_gender) #WhoHeight.where(:gender=> "1").order_by('month ASC')
      @who_data= {}
      age = get_month(Date.today) + 12
      @who_data[:p3] = data.pluck(:P3)[0..age]
      @who_data[:p15] = data.pluck(:P15)[0..age]
      @who_data[:p85] = data.pluck(:P85)[0..age]
      @who_data[:p97] = data.pluck(:P97)[0..age]
      @who_data[:month] = data.pluck(:month)[0..age]
    end
    @recently_updated_record = @member.health_records.order_by(:last_updated_at.desc, :updated_at.desc).first
    respond_to do |format| 
      format.html
      format.js
      format.json { render :json=> {:member=> @member,:health_record => add_diff(@health_records,@convert_unit_val_list), 
        :unit_selected=>{:lenght=>(@convert_unit_val_list["length_unit"] || "cms"),
        :weight=> (@convert_unit_val_list["weight_unit"] || "kgs")},
        :recently_updated_record => (@recently_updated_record.api_convert_to_unit(@convert_unit_val_list) rescue nil),
        :last_record=>(@last_health_record),
        :last_updated_record => (@last_health_record.api_convert_to_unit(@convert_unit_val_list) rescue nil),:chart_param=>params[:data_type],:chart_data=>@chart_update,
        :chart_min_max_axis=> Health.chart_min_max_axis(@chart_update,@who_data[:p97],@who_data[:p3],@who_data[:month]) ,:who_data =>{:p3=>@who_data[:p3], :p15=>@who_data[:p15],:p85=>@who_data[:p85], :p97=>@who_data[:p97],:month=>@who_data[:month] },
        :status=> StatusCode::Status200}  }
    end
  end
  
  def new_health_record
    @member =   Member.find(params[:member_id])    
    @health =  Health.new
    @health_setting =  params[:type].present? ? [params[:type]] : @member.member_settings.where(name:"health_setting").first.note.split(",") rescue []
    @health.attributes = @member.health_records.last.attributes   if  @member.health_records.last # Health.where(member_id: params[:member_id]).last ||
    @health.updated_at = Date.today
    @health.created_at = Date.today
    get_health_metric_unit
    
    render :layout=> false
  end

  # api get healt record detail
  # GET /health/get_health_detail.json?token=iBSqRyQ5pxuxVb1dSz47&measurement_id=12
  def get_health_detail
    begin
       health = Health.find(params[:measurement_id])
      convert_unit_val_list = {}
      convert_unit_val_list["length_unit"] =  current_user.metric_system["length_unit"] == "inches" ? "inches" : "cms"      
      convert_unit_val_list["weight_unit"] =  current_user.metric_system["weight_unit"] == "lbs"  ? "lbs" : "kgs"
      health.api_converted_data(convert_unit_val_list)
      message = Message::API[:success][:get_health_detail]
    rescue
      health = nil
      message = Message::API[:error][:get_health_detail] #{}"No health found"
    end
    render json: {:message => message, :health=>health, :metric_system => convert_unit_val_list}
  end

  def create_record
    begin
      if params[:health].length > 2
        member_id = params[:health][:member_id] #=>"54412e225acefebacd00000a
        member =  Member.find(member_id)
        record = member.health_records.last || Health.new
        params[:health] = convert_to_cms_kgs(params[:health])
        params[:health][:updated_at] = DateTime.now if params[:health][:updated_at].to_date == Date.today
       
        #if params[:health]["height"] || params[:health]["weight"] 
          height = params[:health]["height"] || member.health_records.where(:updated_at.lte=> params[:health][:updated_at]).not_in(:height => ["0","0.0", "",nil]).order_by("updated_at desc").first.height rescue 0
          weight = params[:health]["weight"] || member.health_records.where(:updated_at.lte=> params[:health][:updated_at]).not_in(:weight =>  ["0","0.0","",nil]).order_by("updated_at desc").first.weight rescue 0
          bmi = record.calulate_bmi(weight,height)

            params[:health].merge!({:bmi=> bmi})   
        #end
        params[:health][:last_updated_at] = Time.now
        @health_record = Health.create(params[:health])
        @health_unit = Health.health_unit(current_user.metric_system)
        
        @health_setting =  member.member_settings.where(name:"health_setting").first
        chang_attr = (params[:health])
        
        chang_attr.delete("updated_at")
        chang_attr.delete("_id")
        chang_attr.delete("member_id")
        @health_setting.update_attributes({note:chang_attr.keys.join(",")})
        
        to_timeline = {"height"=>params[:health][:height]|| height,"weight"=>params[:health][:weight] || weight}
        @health_record.timelines << Timeline.new({entry_from: "health", name:bmi, desc: to_timeline.to_a.flatten.to_s, member_id:member_id,added_by: current_user.first_name +  "  " + current_user.last_name, timeline_at: Timeline.new.get_timeline_at(@health_record.updated_at)})
        @message = Message::GLOBAL[:success][:health_create_record] #"Your record has been updated."
        status = StatusCode::Status200

        # Send push notification to other elder member
        @health_record.send_push_to_elder_members(current_user,member)
      end
    rescue Exception=>e
      @message = Message::GLOBAL[:error][:health_create_record] #"Your record has been updated."
      status = StatusCode::Status100
    end
    respond_to do |format| 
      format.html {render :layout=> false}
      format.js {render :layout=> false}
      format.json {render :json=> {:health=>@health_record,:message=>@message, :status=> status}}
    end
    
  end

  def edit
    @health = Health.find(params[:health_record_id])
    @member = @health.member
    @health_date = @health.updated_at
    begin
      @health_setting =   params[:type].present? ? [params[:type]] :  @member.member_settings.where(name:"health_setting").first.note.split(",") 
    rescue 
      @health_setting =   []
    end
    get_health_metric_unit

    @url = "/health/update_record?health_record_id=#{@health.id}"
    render :action=>"new_health_record", :layout=> false
  end
  
  def update_record
     @health = Health.find(params[:health_record_id])
     params[:health][:updated_at] = DateTime.now if params[:health][:updated_at].to_date == Date.today
     params[:health][:last_updated_at] = Time.now
     member = @health.member
     params[:health] = convert_to_cms_kgs(params[:health])
     @health_setting =  member.member_settings.where(name:"health_setting").first
     #if params[:health]["height"] || params[:health]["weight"]
        height = params[:health]["height"] || member.health_records.where(:updated_at.lte=>@health.updated_at).not_in(:height =>  ["0","0.0", "",nil]).order_by("updated_at desc").first.height rescue 0
        weight = params[:health]["weight"] || member.health_records.where(:updated_at.lte=>@health.updated_at).not_in(:weight =>  ["0","0.0", "",nil]).order_by("updated_at desc").first.weight rescue 0
        bmi = @health.calulate_bmi(weight,height)
        params[:health].merge!({:bmi=> bmi})   
      #end
     
     note = params[:health].keys.join(",")
     @health_setting.update_attributes({note:note})
     @health.update_attributes(params[:health])
     # @health.save
     @health_unit = Health.health_unit(current_user.metric_system)
    
     @health.update_attribute("updated_at",params[:health][:updated_at])
     timeline = Timeline.where(timelined_type: "Health", timelined_id: @health.id).first || Timeline.create(entry_from:"health", timelined_type: "Health", timelined_id: @health.id, timeline_at: Timeline.new.get_timeline_at(@health.updated_at))
     to_timeline = {"height"=>params[:health][:height]|| height,"weight"=>params[:health][:weight] || weight}
     timeline.update_attributes({name:bmi, desc: to_timeline.to_a.flatten.to_s, timeline_at: timeline.get_timeline_at(@health.updated_at)})
     # Send push notification to other elder member
     @health.send_push_to_elder_members(current_user,member)
  
     @health.reload
    respond_to do |format| 
      format.html 
      format.js 
      format.json {render :json=> {:health=>@health,:message=>Message::API[:success][:health_update_record], :status=> StatusCode::Status200}}
    end
  end

  def delete_parameter
    @health = Health.find(params[:id])
    updated_at = @health.updated_at
    not_in_value  = ["",0,nil,"0.0"]
    if params[:type]
      @health.update_attributes({params[:type]=> nil,last_updated_at:Time.now})
      if params[:type].downcase == "height" || params[:type].downcase == "weight"
        member = @health.member
         
        height =   member.health_records.where(:updated_at.lte=>updated_at).not_in(:height => not_in_value).order_by("updated_at desc").first.height rescue 0
        weight =   member.health_records.where(:updated_at.lte=>updated_at).not_in(:weight => not_in_value).order_by("updated_at desc").first.weight rescue 0
        bmi = @health.calulate_bmi(weight,height)
        @health.bmi =  bmi
        @health.save
        @health.send_push_to_elder_members(current_user,member)   
      end
      @health.update_attributes({:updated_at=>updated_at})
      if(@health.height.blank? && @health.weight.blank? && @health.head_circum.blank? && @health.waist_circum.blank?)
        Score.where(activity_type: "Health", activity_id:@health.id).delete
        @health.delete
      end
    else
      Score.where(activity_type: "Health", activity_id:@health.id).delete
      Timeline.where(timelined_type: "Health", timelined_id: @health.id).delete
      @health.delete

    end
    respond_to do |format| 
      format.html 
      format.js 
      format.json {render :json=> {:health=>@health,:message=>Message::API[:success][:health_delete_record], :status=> StatusCode::Status200}}
    end
  end

  def get_history_data()
    @family_member = ChildMember.last
    gon.height_update = @height_update = @family_member.health_records.order_by("updated_at asc").collect{|rec| [get_month(rec.updated_at.to_date),rec.height.to_i]}
  end

  
  
  def get_who_data(health_parameter, gender=1)
    gender_str = gender
    if health_parameter == "weight" && current_user.metric_system["weight_unit"] =="kgs"
      WhoWeight.where(:gender=> gender_str).in(measure_unit: nil).order_by('month ASC')
    elsif  health_parameter == "weight" && current_user.metric_system["weight_unit"] =="lbs"
      WhoWeight.where(:gender=> gender_str).in(measure_unit: "lbs").order_by('month ASC')

    elsif  health_parameter == "height" && current_user.metric_system["length_unit"] =="cms"
      WhoHeight.where(:gender=> gender_str).in(measure_unit: nil).order_by('month ASC')
    elsif  health_parameter == "height" && current_user.metric_system["length_unit"] =="inches"
      WhoHeight.where(:gender=> gender_str).in(measure_unit: "inches").order_by('month ASC')
    
    elsif  health_parameter == "bmi"
          WhoBmi.where(:gender=> gender_str).order_by('month ASC')

    elsif  health_parameter == "waist_circum" && current_user.metric_system["length_unit"] =="cms"
      WhoWaist.where(:gender=> gender_str).in(measure_unit: nil).order_by('month ASC')
    elsif  health_parameter == "waist_circum" && current_user.metric_system["length_unit"] =="inches"
      WhoWaist.where(:gender=> gender_str).in(measure_unit: "inches").order_by('month ASC')
    
    elsif  health_parameter == "head_circum" && current_user.metric_system["length_unit"] =="cms"
      WhoHcf.where(:gender=> gender_str).in(measure_unit: nil).order_by('month ASC')
    elsif  health_parameter == "head_circum" && current_user.metric_system["length_unit"] =="inches"
      WhoHcf.where(:gender=> gender_str).in(measure_unit: "inches").order_by('month ASC')
    else
     (0..24).map{|a| a}
    end

  end

  

  def health_info
    @member = Member.find(params[:member_id])
    render layout: false
  end

  def close_info_pop
    @member = Member.find(params[:member_id])
    @health_records = @member.health_records.order_by("updated_at desc")
    render layout: false
  end  

  private
    def get_health_metric_unit
      metric_system = current_user.metric_system
      health_unit_to_convert = {}
      health_unit_to_convert["length_unit"] = "inches" if metric_system["length_unit"] == "inches"
      health_unit_to_convert["weight_unit"] = "lbs" if metric_system["weight_unit"] == "lbs"
      converted_val = @health.convert_to_unit(health_unit_to_convert) #if health_unit_to_convert.present?
      @health.height = converted_val["height"]
      @health.weight = converted_val["weight"]
      @health.head_circum = converted_val["head_circum"]
      @health.waist_circum = converted_val["waist_circum"]
      @health_unit = Health.health_unit(metric_system)
      @health_min_max = Health.health_min_max_value(metric_system)
    end

    def convert_to_cms_kgs(params)
      metric_system = current_user.metric_system
      health_unit_to_convert = {}
      health_unit_to_convert["length_unit"] = "cms" if metric_system["length_unit"] == "inches"
      health_unit_to_convert["weight_unit"] = "kgs" if metric_system["weight_unit"] == "lbs"
      health_unit_to_convert["data"] = params
      Health.new.convert_to_unit(health_unit_to_convert)
    end

    def get_month(date_obj)
      begin
        d1 = @member.birth_date.to_date
        d2 = date_obj
        (d2 - d1).fdiv(30).to_i
      rescue
        0
      end
      #return  (d2.year * 12 + d2.month) - (d1.year * 12 + d1.month)
    end
    def convert_to_unit(rec_val,health_unit)
      (rec_val.to_f * Health::UNIT_VAL[health_unit]).round(2) rescue rec_val.to_f
       
    end


    def api_convert_to_unit(rec_val,health_unit)
      (rec_val.to_f * Health::UNIT_VAL[health_unit]).to_f.round(1) rescue rec_val.to_f.round(1)
       
    end

    def add_diff(health_records,convert_unit_val_list)
      data =[]
      health_records.each_with_index do |record,index|
       next_record = (health_records[index+1]).api_convert_to_unit(convert_unit_val_list) rescue Health.new
       record = record.api_convert_to_unit(convert_unit_val_list)
       record["diff_height"] = (record["height"].to_f - next_record["height"].to_f).to_f.round(1)   rescue  record["height"].to_f.round(1) || "-" 
       record["diff_weight"] = (record["weight"].to_f - next_record["weight"].to_f).to_f.round(1)   rescue  record["weight"].to_f.round(1) || "-" 
       record["diff_head_circum"] = (record["head_circum"].to_f - next_record["head_circum"].to_f).to_f.round(1)   rescue  record["head_circum"].to_f.round(1) || "-" 
       record["diff_waist_circum"] = (record["waist_circum"].to_f - next_record["waist_circum"].to_f).to_f.round(1)   rescue  record["waist_circum"].to_f.round(1) || "-" 
       record["diff_bmi"] = (record["bmi"].to_f - next_record["bmi"].to_f).to_f.round(1)   rescue  record["bmi"].to_f.round(1) || "-" 
       record["bmi"] = record["bmi"].to_f.round(1)
       data << record
     end
     data
    end
    
end
