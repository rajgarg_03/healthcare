class Admin::NhsSyndicatedContentController < ApplicationController
  before_filter :admin_basic_auth
  http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
  before_filter :validate_user_access
  layout 'admin'
  autocomplete :link_tool, :title  


  def autocomplete_link_tool_title
    term = params[:term]
    class_name = params[:class_name]
    class_obj = eval(class_name)
    country_code = params[:country_code]
    sanitize_country_code = Member.sanitize_country_code(country_code)
    fields_for_search =  ::System::Template.get_serach_column_for_mapped_table(class_name)
    country_column =  ::System::Template.get_country_column_for_mapped_table(class_name)
    if class_obj.present?
      if term.blank?
        content_list = class_obj.where(nil)
      elsif  fields_for_search.blank?
        content_list = class_obj.where(nil)
      else
        condition_list = []
        fields_for_search.each do |column_field|
          condition_list << {column_field=>/#{term}/i}
        end
        content_list = class_obj.where({"$or"=>condition_list})
        
      end
      if country_code.present? && country_column.present?
        content_list = content_list.where(country_column.to_sym.in=>sanitize_country_code)
      end
      render :json => content_list.map { |content| {:id => content.id, :label => "#{fields_for_search[0]}: #{content[fields_for_search[0]]} , #{fields_for_search[1]}: #{content[fields_for_search[1]]}", :value => "#{fields_for_search[0]}: #{content[fields_for_search[0]]} , #{fields_for_search[1]}: #{content[fields_for_search[1]]}"} }
    else
      render :json => [{:id => nil, :label => "No record found", :value => "No record found"}] 

    end

  end

  
  def index
    @syndicated_content = ::Nhs::SyndicatedContent.all
    @source = "syndicated_content_controller"
    
  end
  
  def link_with_tool
    @syndicated_content = ::Nhs::SyndicatedContent.where(id:params[:id]).last
    @tool_class = ::Nhs::SyndicatedContentLinkTool.tool_list_for_linking
     
    render :layout=>false
  end

  #params =  {"tool_class"=>"SystemVaccin", "content_id"=>"5f154843cb51a2fd48000038", "country_code"=>"AU", "linked_tool_class"=>"name: Haemophilus Influenzae type B vaccine (HIB) , vaccin_type: Recommended", "linked_tool_id"=>"545644d68cd03d91a700009e", "commit"=>"Save"}
  def save_link_tool_linking
    tool = eval(params[:tool_class]).where(id:params[:linked_tool_id]).last
    if tool.present?
      nhs_syndicated_content =  ::Nhs::SyndicatedContent.find(params[:content_id])
      ::Nhs::SyndicatedContentLinkTool.add_to_list(tool,nhs_syndicated_content.custom_id)
      @status = 200
    elsif  params[:tool_class].present? && params[:linked_tool_class].blank?
      # Link tool level
      nhs_syndicated_content =  ::Nhs::SyndicatedContent.find(params[:content_id])
      ::Nhs::SyndicatedContentLinkTool.add_tool_to_list( params[:tool_class],nhs_syndicated_content.custom_id)
      @status = 200
    else
      @status = 100
    end
  end

  def get_linkd_tool_with_sydicated_content
    syndicated_content = Nhs::SyndicatedContent.find(params[:content_id])
    @syndicated_linked_tools = ::Nhs::SyndicatedContentLinkTool.where(nhs_syndicated_content_custom_id:syndicated_content.custom_id)
    render :layout=>false
  end

  def get_country_list_for_tool_class
    @class_name = eval(params[:class_name])
    country_column = System::Template.get_country_column_for_mapped_table(params[:class_name])
    if country_column == "NA"
      @country_data = ["NA"]
    else
      @country_data = @class_name.distinct(country_column.to_sym).reject{|a| a.blank?} rescue  [["all",""] ]
    end
    render :json=>@country_data
  end

  def delink_tool_with_syndicated_content
    ::Nhs::SyndicatedContentLinkTool.where(id:params[:id]).delete_all
  end

  def paginated_data
    if params[:id].present?
      @syndicated_content = ::Nhs::SyndicatedContent.where(id:params[:id])
    else
      @syndicated_content = ::Nhs::SyndicatedContent.datatable_filter(params['search']['value'], params['columns'])
    end

    @source = "syndicated_content_controller"
    syndicated_content_count = @syndicated_content.count
    @syndicated_content = @syndicated_content.datatable_order(params['order']['0']['column'].to_i,
                                params['order']['0']['dir'])
    if params['length'].to_i >= 0
      @syndicated_content = @syndicated_content.paginate(:page=>(params['start'].to_i/(params['length'].to_i) + 1),:per_page=>params['length'])
    end
    render :json=> {:syndicated_content_list=>@syndicated_content,
                 draw: params['draw'].to_i,
                 recordsTotal: ::Nhs::SyndicatedContent.count,
                 recordsFiltered: syndicated_content_count 
                }
  end
  
  def get_linked_items
    data = {}
    @disable_value = params[:linked_tool_custome_id].present?
    @syndicated_content = ::Nhs::SyndicatedContent.find(params[:id])
    @linked_items = @syndicated_content.linked_items
    # if params[:source] == "syndicated_content_controller"
    #   @source = params[:source]
    # else
      render :layout=>false
    # end
  end

  def show_content
    syndicated_content = ::Nhs::SyndicatedContent.find(params[:id])
    @content = syndicated_content.content
    render :layout=>false
  end

  def fetch_content
    syndicated_content = ::Nhs::SyndicatedContent.find(params[:id])

    # syndicated_content.fetch_content
     syndicated_content.delay.fetch_content
  end


  def new_syndicated_content
    data = {}
    @disable_value = params[:linked_tool_custome_id].present?
    @syndicated_content = ::Nhs::SyndicatedContent.new(data)
    if params[:source] == "syndicated_content_controller"
      @source = params[:source]
    else
      render :layout=>false
    end
  end

  def edit_syndicated_content
    data = {}
    @disable_value = params[:linked_tool_custome_id].present?
    @syndicated_content = ::Nhs::SyndicatedContent.find(params[:id])
    @categories = [@syndicated_content.category].flatten.compact
    @next_syndicated_content_custom_id = ::Nhs::SyndicatedContent.where(:custom_id.gt=>@syndicated_content.custom_id.to_i).order_by("custom_id asc").first.custom_id rescue 0 
    @previous_syndicated_content_custom_id =  ::Nhs::SyndicatedContent.where(:custom_id.lt=>@syndicated_content.custom_id.to_i).order_by("custom_id desc").first.custom_id rescue 0 
    @max_custom_id = ::Nhs::SyndicatedContent.max(:custom_id)
    # @syndicated_content.custom_id.to_i - 1
 
    if params[:source] == "syndicated_content_controller"
      @source = params[:source]
    else
      render :layout=>false
    end
  end

  def edit_next_syndicated_content
    if @access_level > 1
      @source  = "syndicated_content_controller"
      @syndicated_content = ::Nhs::SyndicatedContent.where(custom_id:params[:id]).last
      # @next_syndicated_content_custom_id = @syndicated_content.custom_id.to_i + 1
      @max_custom_id = ::Nhs::SyndicatedContent.max(:custom_id)
      # @previous_syndicated_content_custom_id = @syndicated_content.custom_id.to_i - 1
    @next_syndicated_content_custom_id = ::Nhs::SyndicatedContent.where(:custom_id.gt=>@syndicated_content.custom_id.to_i).order_by("custom_id asc").first.custom_id rescue 0 
    @previous_syndicated_content_custom_id =  ::Nhs::SyndicatedContent.where(:custom_id.lt=>@syndicated_content.custom_id.to_i).order_by("custom_id desc").first.custom_id rescue 0 

    end
  end

  def edit_previous_syndicated_content
    if @access_level > 1
      @source  = "syndicated_content_controller"
      @syndicated_content = ::Nhs::SyndicatedContent.where(custom_id:params[:id]).last
      # @next_syndicated_content_custom_id = @syndicated_content.custom_id.to_i + 1
      @max_custom_id = ::Nhs::SyndicatedContent.max(:custom_id)
      # @previous_syndicated_content_custom_id = @syndicated_content.custom_id.to_i - 1
    @next_syndicated_content_custom_id = ::Nhs::SyndicatedContent.where(:custom_id.gt=>@syndicated_content.custom_id.to_i).order_by("custom_id asc").first.custom_id rescue 0 
    @previous_syndicated_content_custom_id =  ::Nhs::SyndicatedContent.where(:custom_id.lt=>@syndicated_content.custom_id.to_i).order_by("custom_id desc").first.custom_id rescue 0 

    end
  end



  def add_syndicated_content
    if ["","NA"].include?(params["notification_expiry_period"]) || ["","NA"].include?(params["notification_expiry_frequency"])
      params[:nhs_syndicated_content][:notification_expiry] = nil
    else
      params[:nhs_syndicated_content][:notification_expiry] = params["notification_expiry_period"] + "." + params["notification_expiry_frequency"]
    end
    params[:nhs_syndicated_content][:category] = params[:categories].split(",")
    data = params[:nhs_syndicated_content]
    @syndicated_content = ::Nhs::SyndicatedContent.new(data)
    @source = params[:source] || params[:source_controller]
    @next_syndicated_content_custom_id = ::Nhs::SyndicatedContent.where(:custom_id.gt=>@syndicated_content.custom_id.to_i).order_by("custom_id asc").first.custom_id rescue 0 
    @previous_syndicated_content_custom_id =  ::Nhs::SyndicatedContent.where(:custom_id.lt=>@syndicated_content.custom_id.to_i).order_by("custom_id desc").first.custom_id rescue 0 

    if @syndicated_content.save 
       @syndicated_content.update_scenario(params[:system_scenario_detail_custom_id])

      @save = true
    else
      @save = false
    end
    if  !(request.xhr? || request.format.js?) && @save
      redirect_to "/admin/nhs_syndicated_content/#{@syndicated_content.id}" and return true
    else
      respond_to do |format|
        format.js { render layout: false }
        format.html  
      end
    end 
  end

  def clone_syndicated_content
    if @access_level > 1
      @source  = "syndicated_content_controller"
      syndicated_content_obj = ::Nhs::SyndicatedContent.find(params[:id])
      syndicated_content = syndicated_content_obj.attributes
      syndicated_content.delete("_id")
      custom_id = ::Nhs::SyndicatedContent.last.custom_id + 1 
      @cloned_syndicated_content = ::Nhs::SyndicatedContent.new(syndicated_content) 
      @cloned_syndicated_content.custom_id =  custom_id
      @cloned_syndicated_content.status = "Draft"
      @cloned_syndicated_content.save
    end
  end

  #on click next link , It will render next pointer in list
  def go_to_next_syndicated_content
    @source  = "syndicated_content_controller"
    @syndicated_content = ::Nhs::SyndicatedContent.where(custom_id:params[:id]).last
    @max_custom_id = ::Nhs::SyndicatedContent.max(:custom_id)
    next_syndicated_content = ::Nhs::SyndicatedContent.where(:custom_id.gt=>@syndicated_content.custom_id.to_i).order_by("custom_id asc").first
    @next_syndicated_content_custom_id = next_syndicated_content.custom_id rescue 0 
    @previous_syndicated_content_custom_id =  ::Nhs::SyndicatedContent.where(:custom_id.lt=>@syndicated_content.custom_id.to_i).order_by("custom_id desc").first.custom_id rescue 0 
    params[:id] = @syndicated_content.id rescue @syndicated_content.id

  end

 #on click next link , It will render next pointer in list
  def go_to_previous_syndicated_content
    @source  = "syndicated_content_controller"
    @syndicated_content = ::Nhs::SyndicatedContent.where(custom_id:params[:id]).last
    @max_custom_id = ::Nhs::SyndicatedContent.max(:custom_id)
    @next_syndicated_content_custom_id = ::Nhs::SyndicatedContent.where(:custom_id.gt=>@syndicated_content.custom_id.to_i).order_by("custom_id asc").first.custom_id rescue 0 
    previous_syndicated_content =  ::Nhs::SyndicatedContent.where(:custom_id.lt=>@syndicated_content.custom_id.to_i).order_by("custom_id desc").first
    @previous_syndicated_content_custom_id = previous_syndicated_content.custom_id rescue 0 
    params[:id] = @syndicated_content.id# rescue @syndicated_content.id
  end
   
  # vaidate user access  with role and access level for pointer Module
  def validate_user_access(module_name="PointersMaintenance")
    @access_level = ModuleAccessForRole.user_access_level_for_module(current_user,module_name)
    unless (@access_level > 0) || current_user.email == AppConfig["superadmin_email"] 
     redirect_to "/"
    end
  end

  def update_syndicated_content
    begin
      @syndicated_content = ::Nhs::SyndicatedContent.find(params[:syndicated_content_id])
      if params["notification_expiry_period"] == "NA" || ["","NA"].include?(params["notification_expiry_frequency"])
        params[:nhs_syndicated_content][:notification_expiry] = nil
      else
        params[:nhs_syndicated_content][:notification_expiry] = params["notification_expiry_period"] + "." + params["notification_expiry_frequency"]
      end
      params[:nhs_syndicated_content][:category] = params[:categories].split(",")
      
      data = params[:nhs_syndicated_content]
      @save =  @syndicated_content.update_attributes(data)
      if @save
       @syndicated_content.update_scenario(params[:system_scenario_detail_custom_id])

     end
    next_syndicated_content_custom_id = ::Nhs::SyndicatedContent.where(:custom_id.gt=>@syndicated_content.custom_id.to_i).order_by("custom_id asc").first.custom_id rescue 0 
     

      next_syndicated_content_custom_id = @syndicated_content.custom_id + 1
      next_syndicated_content = ::Nhs::SyndicatedContent.where(custom_id:next_syndicated_content_custom_id).last
      
    rescue Exception => e 
      Rails.logger.info "error inside update_syndicated_content-#{e.message}"
      @save = false
    end
    if  !(request.xhr? || request.format.js?) && @save
    @source = "syndicated_content_controller"
    if params[:update_and_edit_next].present? && next_syndicated_content.present?
        redirect_to "/admin/nhs_syndicated_content/edit_syndicated_content?id=#{next_syndicated_content.id}&source=syndicated_content_controller" and return
      else
        redirect_to "/admin/nhs_syndicated_content/#{@syndicated_content.id}" and return true
      end
    else
      respond_to do |format|
        format.js { render layout: false }
        format.html  
      end
    end 
  end

  def delete_syndicated_content
    @syndicated_content = ::Nhs::SyndicatedContent.find(params[:syndicated_content_id])
    @syndicated_content.delete
  end

  def show
    @syndicated_content = ::Nhs::SyndicatedContent.where(id:params[:id])
    @source = "syndicated_content_controller"
    @syndicated_content = @syndicated_content.first
    @source = params[:source] || params[:source_controller] || "syndicated_content_controller"
    # @next_syndicated_content_custom_id = @syndicated_content.custom_id.to_i + 1
    @max_custom_id = ::Nhs::SyndicatedContent.max(:custom_id)
    # @previous_syndicated_content_custom_id = @syndicated_content.custom_id.to_i - 1
 
    @next_syndicated_content_custom_id = ::Nhs::SyndicatedContent.where(:custom_id.gt=>@syndicated_content.custom_id.to_i).order_by("custom_id asc").first.custom_id rescue 0
    @previous_syndicated_content_custom_id =  ::Nhs::SyndicatedContent.where(:custom_id.lt=>@syndicated_content.custom_id.to_i).order_by("custom_id desc").first.custom_id rescue 0 

    if @source != "syndicated_content_controller"
 
      render :action=> :index,:layout=>false
    else
      render :action=> :index
    end
  end
   

  private
  def validate_user_access(module_name="NhsSyndicatedContent")
    @access_level = ModuleAccessForRole.user_access_level_for_module(current_user,module_name)
    unless (@access_level > 0) || current_user.email == AppConfig["superadmin_email"] 
     redirect_to "/"
    end
  end 

end