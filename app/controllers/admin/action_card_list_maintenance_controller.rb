class Admin::ActionCardListMaintenanceController < ApplicationController
  # skip_before_filter :authenticate_user!
  before_filter :admin_basic_auth
  http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
  
  before_filter :validate_user_access
  layout 'admin'

  def index
  	@action_cards = ActionCardList.all
  end

  def new_action_card
    redirect_to "/admin/action_card_list_maintenance" and return if @access_level < 2
  	@action_card = ActionCardList.new
     
  end

  def create
    redirect_to "/admin/action_card_list_maintenance" and return if @access_level < 2
     
    @action_card = ActionCardList.new(params[:action_card_list])
    if @action_card.save
      redirect_to "/admin/action_card_list_maintenance"
    else
       
    	render :new_tool
    end
  end

  def edit
    redirect_to "/admin/action_card_list_maintenance" and return if @access_level < 2
  	@action_card = ActionCardList.find(params[:id])
     
  end

  def update
    redirect_to "/admin/action_card_list_maintenance" and return if @access_level < 2
  	@action_card = ActionCardList.find(params[:id])
    
  	if @action_card.update_attributes(params[:action_card_list])
      redirect_to "/admin/action_card_list_maintenance"
  	else
  		render :edit
  	end
  end

  def delete_action_card
    redirect_to "/admin/action_card_list_maintenance" and return if @access_level < 3
  	@action_card = ActionCardList.find(params[:id])
  	@action_card.delete
  end

  private
   
  def validate_user_access(module_name="ActionCardListMaintenance")
    @access_level = ModuleAccessForRole.user_access_level_for_module(current_user,module_name)
    unless (@access_level > 0) || current_user.email == AppConfig["superadmin_email"] 
     redirect_to "/"
    end
  end
end