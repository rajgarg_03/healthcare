class Admin::AlexaController < ApplicationController
  before_filter :admin_basic_auth
  http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
  layout 'admin'
  def index
    @all_request = Report::AlexaRequestData.order_by("id desc").limit(10)
  end
end