class Admin::SystemSettingsController < ApplicationController
  skip_before_filter :authenticate_user!
  before_filter :admin_basic_auth
  http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
  before_filter :validate_user_access

  layout 'admin'

  def index
  	@system_settings = System::Settings.all
  end

  def new_setting
  	@system_setting = System::Settings.new
  end

  def create
    begin
      params[:system_settings][:setting_values] = eval(params[:system_settings][:setting_values]) || {}
      @system_setting = System::Settings.new(params[:system_settings])
      if @system_setting.save
        redirect_to "/admin/system_settings"
      else
      	render :new_setting
      end
    rescue Exception=>e
        render :new_setting
    end
  end

  def edit
  	@system_setting = System::Settings.find(params[:id])

  end

  def update
    begin

  	@system_setting = System::Settings.find(params[:id])
    params[:system_settings][:setting_values] = eval(params[:system_settings][:setting_values]) || {}
    
  	if @system_setting.update_attributes(params[:system_settings])
      redirect_to "/admin/system_settings"
  	else
     
  		render :edit
  	end
  rescue
      render :edit
  end
  end

  def delete_system_setting
  	@system_setting = System::Settings.find(params[:id])
  	@system_setting.delete
  end

  private
  def validate_user_access(module_name="SystemSettings")
    @access_level = ModuleAccessForRole.user_access_level_for_module(current_user,module_name)
    unless (@access_level > 0) || current_user.email == AppConfig["superadmin_email"] 
     redirect_to "/"
    end
  end

end