class Admin::ToolListMaintenanceController < ApplicationController
  skip_before_filter :authenticate_user!
  before_filter :admin_basic_auth
  http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
  before_filter :validate_user_access
  layout 'admin'

  def index
  	@tools = Nutrient.all
  end

  def new_tool
    redirect_to "/admin/tool_list_maintenance" and return if @access_level < 2
  	@tool = Nutrient.new
    @tool_categories = Nutrient.all.map(&:categories).flatten.uniq
  end

  def create
    redirect_to "/admin/tool_list_maintenance" and return if @access_level < 2
    params[:nutrient][:categories] = params[:categories].split(",")
    params[:nutrient][:android_supported_app_version_str] = params[:nutrient][:android_supported_app_version_str].upcase.strip
    params[:nutrient][:ios_supported_app_version_str] = params[:nutrient][:ios_supported_app_version_str].upcase.strip
    params[:nutrient][:android_supported_app_version] = Nutrient.convert_device_app_version_number(params[:nutrient][:android_supported_app_version_str],"android")
    params[:nutrient][:ios_supported_app_version] = Nutrient.convert_device_app_version_number(params[:nutrient][:ios_supported_app_version_str],"ios")
    params[:nutrient][:subscription_box_data] = params[:nutrient][:subscription_box_data].strip rescue params[:nutrient][:subscription_box_data] 
    params[:nutrient][:subscription_box_data] = eval(params[:nutrient][:subscription_box_data])
    @tool = Nutrient.new(params[:nutrient])
    if @tool.save
      redirect_to "/admin/tool_list_maintenance"
    else
      @tool_categories = params[:nutrient][:categories]
    	render :new_tool
    end
  end

  def edit
    redirect_to "/admin/tool_list_maintenance" and return if @access_level < 2
  	@tool = Nutrient.find(params[:id])
    @tool_categories = @tool.categories
  end

  def update
    redirect_to "/admin/tool_list_maintenance" and return if @access_level < 2
  	@tool = Nutrient.find(params[:id])
    params[:nutrient][:android_supported_app_version_str] = params[:nutrient][:android_supported_app_version_str].upcase.strip
    params[:nutrient][:ios_supported_app_version_str] = params[:nutrient][:ios_supported_app_version_str].upcase.strip
    
    params[:nutrient][:android_supported_app_version] = Nutrient.convert_device_app_version_number(params[:nutrient][:android_supported_app_version_str],"android")
    params[:nutrient][:ios_supported_app_version] = Nutrient.convert_device_app_version_number(params[:nutrient][:ios_supported_app_version_str],"ios")
   
    params[:nutrient][:categories] = params[:categories].split(",")
    params[:nutrient][:subscription_box_data] = params[:nutrient][:subscription_box_data].strip rescue params[:nutrient][:subscription_box_data] 
  	params[:nutrient][:subscription_box_data] = eval(params[:nutrient][:subscription_box_data])
    if @tool.update_attributes(params[:nutrient])
      redirect_to "/admin/tool_list_maintenance"
  	else
  		render :edit
  	end
  end

  def delete_tool
    redirect_to "/admin/tool_list_maintenance" and return if @access_level < 3
  	@tool = Nutrient.find(params[:id])
  	@tool.delete
  end

  private
   
  def validate_user_access(module_name="ToolsListMaintenance")
    @access_level = ModuleAccessForRole.user_access_level_for_module(current_user,module_name)
    unless (@access_level > 0) || current_user.email == AppConfig["superadmin_email"] 
     redirect_to "/"
    end
  end

end