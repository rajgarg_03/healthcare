class Admin::FacePlusController < ApplicationController
  # skip_before_filter :authenticate_user!
  before_filter :admin_basic_auth
  http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
  
  layout 'admin'
  

  def index
    validate_user_access("FacePlusPlus")
  end
  
  def get_response_from_face_plus_plus
    validate_user_access("FacePlusPlus")
    local_image =  params[:local_image]
    begin 
    api_secret = APP_CONFIG["faceplusplus_secret"] #"p5eNWLCB2TYFbBtk7rr5jAQp7TQ3qmP0"
    api_key =  APP_CONFIG["faceplusplus_key"] # "JhrTRDJmhECb3oy4yQ0sTd42FePDM3C4"
     
    @face_plus_obj = FacePlus.create
    picture = Picture.create(:imageable_type=>"FacePlus",:imageable_id=>@face_plus_obj.id,:upload_class=>"face_plus",local_image:local_image)
    @face_plus_obj.pictures << picture
    @face_plus_obj.save
    # picture = @face_plus_obj.pictures.last
    picture.upload_to_s3
    picture.local_image.destroy
    image_path = picture.aws_url(:large)# || "/home/osboxes/personal/nurtureyreloaded/pictures/local_images/521c/c10a/ed35/780f/4400/0003/original/IMG_0008.JPG"
    @data = JSON.parse(`curl -X POST https://api-us.faceplusplus.com/facepp/v3/detect -F api_key=#{api_key} -F api_secret=#{api_secret} -F image_url=#{image_path} -F return_landmark=2 -F return_attributes=gender,age`)
    # a = `curl -X POST https://api-us.faceplusplus.com/facepp/v3/detect -F api_key=#{api_key} -F api_secret=#{api_secret} -F image_file=@#{image_path} -F return_landmark=1 -F return_attributes=gender,age`
    @image_path = image_path
    @face_token = []
    @data["faces"].each do |faces|
     @face_token << faces["face_token"]
   end
   @skin_data =  []
   @face_token.each do |face_token|
      @skin_data << JSON.parse(`curl -X POST https://api-us.faceplusplus.com/facepp/v3/face/analyze -F api_key=#{api_key} -F api_secret=#{api_secret} -F face_tokens=#{face_token} -F  return_attributes=skinstatus,ethnicity`)

   end
   rescue Exception=>e
    Rails.logger.info e.message
   end
  # render json:{data:JSON.parse(a)}
  end
  
  def validate_user_access(module_name="FacePlusPlus")
    @access_level = ModuleAccessForRole.user_access_level_for_module(current_user,module_name)
    unless (@access_level > 0) || current_user.email == AppConfig["superadmin_email"] 
     redirect_to "/"
    end
  end 

end