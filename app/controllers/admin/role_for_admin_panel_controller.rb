class Admin::RoleForAdminPanelController < ApplicationController
  # skip_before_filter :authenticate_user!
  # before_filter :basic_auth
  
  before_filter :admin_basic_auth
  http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
  
  before_filter :validate_user_access
  layout 'admin'

  def index
  	@roles = RoleForAdminPanel.all
  end

  def new_role
  	@role = RoleForAdminPanel.new
  end

  def create
  	@role = RoleForAdminPanel.new(params[:role_for_admin_panel])
  	@role.access_level = RoleForAdminPanel::LevelmappingWithName[params[:role_for_admin_panel][:access_level_name].to_s]
    if @role.save
      redirect_to "/admin/role_for_admin_panel"
    else
    	render :new_role
    end
  end

  def edit
  	@role = RoleForAdminPanel.find(params[:id])

  end

  def update
  	@role = RoleForAdminPanel.find(params[:id])
    params[:role_for_admin_panel][:access_level] = RoleForAdminPanel::LevelmappingWithName[params[:role_for_admin_panel][:access_level_name].to_s]

  	if @role.update_attributes(params[:role_for_admin_panel])
      redirect_to "/admin/role_for_admin_panel"
  	else
  		render :edit
  	end
  end

  def delete_role
  	@role = RoleForAdminPanel.find(params[:id])
  	@role.delete
  end

  private
  def validate_user_access(module_name="SystemRolesMaintenance")
    @access_level = ModuleAccessForRole.user_access_level_for_module(current_user,module_name)
    unless (@access_level > 0) || current_user.email == AppConfig["superadmin_email"] 
     redirect_to "/"
    end
  end 
  
end