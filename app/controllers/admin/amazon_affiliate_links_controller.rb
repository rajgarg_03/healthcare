class Admin::AmazonAffiliateLinksController < ApplicationController
  skip_before_filter :authenticate_user!
  before_filter :admin_basic_auth
  http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
  before_filter :validate_user_access
  layout 'admin'

  def index
    @products = System::AffiliateMarketingProgram::AmazonAffiliatedLink.all
  end


  def search_product
    options = {}
    search_keywords  = params[:search_keywords]
    search_index = params[:search_index]
    options[:country_code] = params[:country_code]
      System::AffiliateMarketingProgram::AmazonAffiliatedLink.search_items(current_user,search_keywords,search_index,options)
    @products = System::AffiliateMarketingProgram::AmazonAffiliatedLink.all
    render :action=>"index"
  end

  private
   
  def validate_user_access(module_name="AffiliateLinks")
    @access_level = ModuleAccessForRole.user_access_level_for_module(current_user,module_name)
    unless (@access_level > 0) || current_user.email == AppConfig["superadmin_email"] 
     redirect_to "/"
    end
  end
end