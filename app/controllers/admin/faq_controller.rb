class Admin::FaqController < ApplicationController
  # skip_before_filter :authenticate_user!
  before_filter :admin_basic_auth
  before_filter :validate_user_access, :except=>["autocomplete_system_question_custom_id"]

  http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
  
  before_filter :validate_user_access
  layout 'admin'

  autocomplete :system_question, :custom_id  

  def autocomplete_system_question_custom_id
    term = params[:term]
    if term.blank?
      system_faq_questions = System::Faq::Question.all.order_by("custome_id asc")
    else
      system_faq_questions = System::Faq::Question.or({:title=>/#{term}/i},{:custom_id=>term}).order_by("custom_id asc")
    end
    render :json => system_faq_questions.map { |system_faq_question| {:id => system_faq_question.custom_id, :label => "ID: #{system_faq_question.custom_id} , Title: #{system_faq_question.title}", :value => "ID: #{system_faq_question.custom_id} , Title: #{system_faq_question.title}"} }

  end

  
  # list all faq topic
  def index
    @topics = System::Faq::Topic.all
  end

  def new_topic
    # redirect_to "/admin/faq" and return if @access_level < 2
    @topic = System::Faq::Topic.new
    @topic.sort_order = System::Faq::Topic.distinct(:sort_order).max.to_i + 1
    @form_for = "topic"
    render :new_form
  end

  def create_topic
    @topic = System::Faq::Topic.new(params[:system_faq_topic])
    @form_for = "topic"
     if @topic.save
       action_type = "add"
       current_sorting_order = @topic.sort_order
       options = {:previous_sort_order=>nil,:id=>@topic.id }
       System::Faq::Topic.update_sorting_order(action_type,current_sorting_order,current_user,api_version=5,options)
    
      redirect_to "/admin/faq/show_topic?topic_id=#{@topic.id}"
    else
      render :new_form
    end
  end

   def edit_topic
    @form_for = "topic"
    @topic = System::Faq::Topic.find(params[:system_faq_topic_id])
    render :new_form
  end

   def update_topic
    @topic = System::Faq::Topic.find(params[:system_faq_topic_id])
    previous_sort_order = @topic.sort_order
    @form_for = "topic"
    if @topic.update_attributes(params[:system_faq_topic])
     action_type = "update"
     current_sorting_order = @topic.sort_order
     options = {:previous_sort_order=>previous_sort_order,:id=>@topic.id }
     System::Faq::Topic.update_sorting_order(action_type,current_sorting_order,current_user,api_version=5,options)
     redirect_to "/admin/faq/show_topic?topic_id=#{@topic.id}"
    else
      render :new_form
    end
  end

  def show_topic
    @form_for = "topic"
    @topic = System::Faq::Topic.find(params[:topic_id])
    @topics = [@topic]
    @subtopics = @topic.sub_topics
    render :show_list
  end

# SubTopic 

  def new_subtopic
    # redirect_to "/admin/faq" and return if @access_level < 2
    @topic = System::Faq::Topic.find(params[:topic_id])
    @subtopic = System::Faq::SubTopic.new
    @subtopic.sort_order = System::Faq::SubTopic.where(:topic_id=>@topic.id).distinct(:sort_order).max.to_i + 1 
    @form_for = "subtopic" 
    
    render :new_form
  end

  def create_subtopic
    @form_for = "subtopic"
    @subtopic = System::Faq::SubTopic.new(params[:system_faq_sub_topic])
     if @subtopic.save
       action_type = "add"
       current_sorting_order = @subtopic.sort_order
       options = {:previous_sort_order=>nil,:id=>@subtopic.id }
       System::Faq::SubTopic.update_sorting_order(action_type,current_sorting_order,current_user,api_version=5,options)
    
      redirect_to "/admin/faq/show_subtopic?subtopic_id=#{@subtopic.id}"
    else
    @topic = System::Faq::Topic.find(params[:system_faq_sub_topic][:topic_id])
      render :new_form
    end
  end

   def edit_subtopic
    @form_for = "subtopic"
    @subtopic = System::Faq::SubTopic.find(params[:system_faq_subtopic_id])
    @topic = System::Faq::Topic.find(@subtopic.topic_id)
    render :new_form
  end

   def update_subtopic
    @form_for = "subtopic"
    @subtopic = System::Faq::SubTopic.find(params[:system_faq_subtopic_id])
    previous_sort_order = @subtopic.sort_order
    if @subtopic.update_attributes(params[:system_faq_sub_topic])
       action_type = "update"
       current_sorting_order = @subtopic.sort_order
       options = {:previous_sort_order=>previous_sort_order,:id=>@subtopic.id }
       System::Faq::SubTopic.update_sorting_order(action_type,current_sorting_order,current_user,api_version=5,options)
    
      redirect_to "/admin/faq/show_subtopic?subtopic_id=#{@subtopic.id}"
    else
      @topic = System::Faq::Topic.find(@subtopic.topic_id)
      render :new_form
    end
  end

  def show_subtopic
    @form_for = "subtopic"
    @subtopic = System::Faq::SubTopic.find(params[:subtopic_id])
    @topic = System::Faq::Topic.find(@subtopic.topic_id) #find(params[:topic_id])
    @subtopics = [@subtopic]
    @questions = @subtopic.questions
    render :show_list
  end


# Question 

  def new_question
    @subtopic = System::Faq::SubTopic.find(params[:sub_topic_id])
    # @topic = System::Faq::Topic.find(@subtopic.topic_id)
    @question = System::Faq::Question.new
    @question.sort_order = System::Faq::Question.where(sub_topic_id:@subtopic).distinct(:sort_order).max.to_i + 1
    @form_for = "question"
    render :new_form
  end

  def create_question
    @form_for = "question"
    if ["","NA"].include?(params["notification_expiry_period"]) || ["","NA"].include?(params["notification_expiry_frequency"])
      params[:system_faq_question][:notification_expiry] = nil
    else
      params[:system_faq_question][:notification_expiry] = params["notification_expiry_period"] + "." + params["notification_expiry_frequency"]
    end
    # previous_sort_order = @question.sort_order
    @question = System::Faq::Question.new(params[:system_faq_question])
     if @question.save
       @question.update_scenario(params[:system_scenario_detail_custom_id])
      
       action_type = "add"
       current_sorting_order = @question.sort_order
       options = {:previous_sort_order=>nil,:id=>@question.id }
       System::Faq::Question.update_sorting_order(action_type,current_sorting_order,current_user,api_version=5,options)
    
      redirect_to "/admin/faq/show_question?question_id=#{@question.id}"
    else
    @subtopic = System::Faq::SubTopic.find(params[:system_faq_question][:sub_topic_id])
      render :new_form
    end
  end

   def edit_question
    @form_for = "question"
    @question = System::Faq::Question.find(params[:system_faq_question_id])
    @subtopic = System::Faq::SubTopic.find(@question.sub_topic_id)
    render :new_form
  end

        
   def update_question
    @form_for = "question"
    if ["","NA"].include?(params["notification_expiry_period"]) || ["","NA"].include?(params["notification_expiry_frequency"])
      params[:system_faq_question][:notification_expiry] = nil
    else
      params[:system_faq_question][:notification_expiry] = params["notification_expiry_period"] + "." + params["notification_expiry_frequency"]
    end
    @question = System::Faq::Question.find(params[:system_faq_question_id])
    previous_sort_order = @question.sort_order
    if @question.update_attributes(params[:system_faq_question])

       @question.update_scenario(params[:system_scenario_detail_custom_id])
       action_type = "update"
       current_sorting_order = @question.sort_order
       options = {:previous_sort_order=>previous_sort_order,:id=>@question.id }
       System::Faq::Question.update_sorting_order(action_type,current_sorting_order,current_user,api_version=5,options)
    
      redirect_to "/admin/faq/show_question?question_id=#{@question.id}"
    else
      @subtopic = System::Faq::SubTopic.find(@question.sub_topic_id)
      render :new_form
    end
  end

  def show_question
    @form_for = "question"
    @question = System::Faq::Question.find(params[:question_id])
    @subtopic = System::Faq::SubTopic.find(@question.sub_topic.id)
    @topic = System::Faq::Topic.find(@subtopic.topic_id)
    @questions = [@question]
    @answer = @question.answer
    render :show_list
  end

  def test_question_scenario_validation_to_assign
    system_scenario_list = System::Scenario::ScenarioDetails.where(custome_id:params[:custom_id])
    child_member_id = params[:member_id]
    options = {:system_scenario_list=>system_scenario_list}
    Child::Scenario.assign_scenario(child_member_id,options)
    @scenario =  Child::Scenario.where(member_id:child_member_id,scenario_custom_id:params[:custom_id].to_i).last
  end


# Answer 

  def new_answer
    @question = System::Faq::Question.find(params[:question_id])
    @answer = System::Faq::Answer.new
    @form_for = "answer"
    render :new_form
  end

  def create_answer
    @form_for = "answer"
    question_id = params[:system_faq_answer][:question_id]
    existing_answer_for_question = System::Faq::Answer.where(:question_id=>question_id).last
    title = params[:system_faq_answer][:title].gsub(";;",";") rescue params[:system_faq_answer][:title]
    title = title.split(";").join(";")
    status = false
    data = {:question_id=>question_id, :title=> title}
      
    if existing_answer_for_question.present?
      answer_title = existing_answer_for_question.title.to_s
      updated_answer_title = answer_title + ";" + title
      updated_answer_title = updated_answer_title.gsub(";;",";")
      updated_answer_title = updated_answer_title.split(";").join(";")
      status = existing_answer_for_question.update_attribute(:title,updated_answer_title)
    else
      @answer = System::Faq::Answer.new(data)
      status = @answer.save
    end
    if status
      redirect_to "/admin/faq/show_question?question_id=#{question_id}"
    else
      @question = System::Faq::Question.new(params[:system_faq_answer][:question_id])
      render :new_form
    end
  end

   def edit_answer
    @form_for = "answer"
    @answer = System::Faq::Answer.find(params[:system_faq_answer_id])
    @question = System::Faq::Question.find(@answer.question_id)
    render :new_form
  end

   def update_answer
    @form_for = "answer"
    @answer = System::Faq::Answer.find(params[:system_faq_answer_id])
    if @answer.update_attributes(params[:system_faq_answer])
      @answer.reload 
      title = @answer.title
      @answer.title = title.gsub(";;",";").split(";").join(";")
      @answer.save
      redirect_to "/admin/faq/show_question?question_id=#{@answer.question_id}"
    else
      @question = System::Faq::Question.new(@answer.question_id)
      render :new_form
    end
  end

  


  def delete_record
    # redirect_to "/admin/tool_list_maintenance" and return if @access_level < 3
    redirect_show_page_list = {"topic"=>"","subtopic"=>"topic","question"=>"sub_topic","answer"=>"question"}
    @record = eval(params[:class_name]).find(params[:id])
    @table_id = params[:class_name].split("::").last.downcase
    @parent_record = @record.send(redirect_show_page_list[@table_id].to_sym) rescue nil
    @header_link_status = params[:header_link]
    action_type = "delete"
    current_sorting_order = @record.sort_order
    options = {:previous_sort_order=>nil,:id=>@record.id }
    eval(params[:class_name]).update_sorting_order(action_type,current_sorting_order,current_user,api_version=5,options)
    @record.destroy
  end

  private
  def validate_user_access(module_name="SystemFaq")
    @access_level = ModuleAccessForRole.user_access_level_for_module(current_user,module_name)
    unless (@access_level > 0) || current_user.email == AppConfig["superadmin_email"] 
     redirect_to "/"
    end
  end
  
end