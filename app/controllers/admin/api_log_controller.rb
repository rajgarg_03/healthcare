class Admin::ApiLogController < ApplicationController
  # skip_before_filter :authenticate_user!
  before_filter :admin_basic_auth
  http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
  
  before_filter :validate_user_access, :except=>[:nhs_api_logs,:nhs_api_log_paginated_data, :get_nhs_api_filter_data, :get_emis_filter_data, :get_filter_data,:tpp_logs, :event_log_data, :emis_logs,:paginated_data,:emis_log_paginated_data,:cvtm_purchase_event_log_data,:get_event_description,:update_event]
  layout 'admin'
  
  def emis_logs
    validate_user_access("EmisLog")
    options = params
    @time_zone =  cookies[:browser_timezone] || "UTC"
    @log_data =  {} #::Log::EmisLog.get_data(option)
  end
 
  def emis_log_paginated_data
    validate_user_access("EmisLog")

    @emis_logs  = ::Log::EmisLog.get_requests(params)
    # @emis_logs = ::Log::EmisLog.datatable_filter(params['search']['value'], params['columns'])
    @emis_logs = @emis_logs.datatable_filter(params['search']['value'], params['columns'])
    @source = "scenario_controller"
    @time_zone =  cookies[:browser_timezone] || "UTC"

    emis_logs_count = @emis_logs.length
    # @emis_logs = @emis_logs.datatable_order(params['order']['0']['column'].to_i,
                                # params['order']['0']['dir'])

    page = (params['start'].to_i/(params['length'].to_i) + 1)
    page = page > 0 ? page :  0
    per_page = params['length'].to_i 
    if params['length'].to_i >= 0
      @emis_logs = @emis_logs.skip(params['start'].to_i).limit(per_page)
    end
    render :json=> {:emis_logs_list=>@emis_logs,
                 draw: params['draw'].to_i,
                 recordsTotal: emis_logs_count,
                 recordsFiltered: emis_logs_count 
                }
  end

  def tpp_logs
    validate_user_access("TppLog")
    options = params
    @time_zone =  cookies[:browser_timezone] || "UTC"
    @log_data = ::Log::TppLog.get_data(option)
  end


  def tpp_log_paginated_data
    validate_user_access("TppLog")
    @tpp_logs = ::Log::TppLog.datatable_filter(params['search']['value'], params['columns'])
    @source = "scenario_controller"
    @time_zone =  cookies[:browser_timezone] || "UTC"

    tpp_logs_count = @tpp_logs.count

    @tpp_logs = @tpp_logs.order_by("id desc")
    if params['length'].to_i >= 0
      @tpp_logs = @tpp_logs.paginate(:page=>(params['start'].to_i/(params['length'].to_i) + 1),:per_page=>params['length'])
    end
    render :json=> {:tpp_logs_list=>@tpp_logs,
                 draw: params['draw'].to_i,
                 recordsTotal: ::Log::TppLog.count,
                 recordsFiltered: tpp_logs_count 
                }
  end


  def get_emis_filter_data
    validate_user_access("EmisLog")

    @time_zone =  cookies[:browser_timezone] || "UTC"

    if  params[:commit] == "Export to Excel"
      @emis_logs  = ::Log::EmisLog.get_requests(params)
      headers.delete("Content-Length")
      headers["Content-Disposition"] = "attachment; filename=\"#{Date.today.to_date2}_user_export.csv\""
      headers["Content-Type"] = "text/csv"
      headers["Cache-Control"] ||= "no-cache"
      self.response_body = download_emis_data(@emis_logs)
      params[:commit] = nil
    else
      render :action=> "emis_logs"
    end
   end

  def download_emis_data(data)
    Enumerator.new do |yielder|
      yielder << CSV.generate_line(["Event","Requested At","Status", "IP Address","Message ID","OrganizationId"])
      data.each do |log|
        begin
          # uid = user.uid 
          status = log.request_status == true ? "Success" : "Failed"
          yielder << CSV.generate_line([log.action, log.created_at, status, log.ip_address, log.message_id, log.organization_id])
        rescue Exception=>e 
        end
      end
    end
  end

  
  


  def nhs_api_logs
    validate_user_access("NhsApiLog")
    options = params
    @time_zone =  cookies[:browser_timezone] || "UTC"
    @log_data = ::Log::NhsApiLog.get_data(option)
  end
  
  def nhs_api_log_paginated_data
    validate_user_access("NhsApiLog")

    @nhs_api_logs  = ::Log::NhsApiLog.get_requests(params)
    @nhs_api_logs = @nhs_api_logs.datatable_filter(params['search']['value'], params['columns'])
    @source = "api_log_controller"
    @time_zone =  cookies[:browser_timezone] || "UTC"

    nhs_api_logs_count = @nhs_api_logs.count

    @nhs_api_logs = @nhs_api_logs.order_by("id desc")
    if params['length'].to_i >= 0
      @nhs_api_logs = @nhs_api_logs.paginate(:page=>(params['start'].to_i/(params['length'].to_i) + 1),:per_page=>params['length'])
    end
    render :json=> {:nhs_api_logs_list=>@nhs_api_logs,
                 draw: params['draw'].to_i,
                 recordsTotal: ::Log::NhsApiLog.count,
                 recordsFiltered: nhs_api_logs_count 
                }
  end


  def get_nhs_api_filter_data
    validate_user_access("NhsApiLog")

    @time_zone =  cookies[:browser_timezone] || "UTC"
    if  params[:commit] == "Export to Excel"
      @nhs_api_logs  = ::Log::NhsApiLog.get_requests(params)
      headers.delete("Content-Length")
      headers["Content-Disposition"] = "attachment; filename=\"#{Date.today.to_date2}_user_export.csv\""
      headers["Content-Type"] = "text/csv"
      headers["Cache-Control"] ||= "no-cache"
      self.response_body = download_nhs_api_data(@nhs_api_logs)
      params[:commit] = nil
    else
      render :action=> "nhs_api_logs"
    end
  end


  def download_nhs_api_data(data)
    Enumerator.new do |yielder|
      yielder << CSV.generate_line(["Url","Requested At","Status"])
      data.each do |log|
        begin
          # uid = user.uid 
          # status = log.request_status == true ? "Success" : "Failed"
          yielder << CSV.generate_line([log.fetch_url, log.created_at, log.request_status])
        rescue Exception=>e 
        end
      end
    end
  end


  def index
  	options = params
    @log_data = {} #::Report::ApiLog.get_data(options={})
  end

  def get_filter_data
  	params.delete(:email) if params[:email] == "Email"
    options = params
    params[:event_custom_id] = [params[:event_custom_id]].flatten
    @controller_access = params[:controller_access]
    # @log_data = ::Report::ApiLog.get_data(options)
    render :action=> "index"
  end


  

  def event_log_data
    validate_user_access("EventLog")
    options = params
    # @log_data = ::Report::ApiLog.get_data(options={:log_category=>"event"})
    params[:log_category] = "event"
    render :action=> "index"
  end

  def cvtm_purchase_event_log_data
    validate_user_access("CvtmePurchaseEventLog")
    options = params
    @log_data = ::Report::ApiLog.get_data(options={:log_category=>"event",:event_custom_id=>"238"})
    params[:log_category] = "event"
    params[:event_custom_id] = ["238"]
    params[:event_log_type] = "CvtmePurchaseEventLog"
    render :action=> "index"
  end
    
  def event_list
    @events = ::Report::Event.all
  end
  

  def get_event_description
    @event = ::Report::Event.where(custom_id:params[:event_custom_id]).last
    render :text=>"<b>Event TItle</b>:<br/><br/> #{@event.title}" ,layout:false
  end

  def paginated_data
    params.delete(:email) if params[:email] == "Email"
    options = params
    options[:datatable_filter] = true
    options[:datatable_order] = true
     
    @controller_access = params[:controller_access]
    options.merge!({:page=>(params['start'].to_i/(params['length'].to_i) + 1),:per_page=>params['length']})
    @log_data ,log_data_count = ::Report::ApiLog.get_data(options)
        render :json=> {:log_data=>@log_data,
                 draw: params['draw'].to_i,
                 recordsTotal: log_data_count,
                 recordsFiltered: log_data_count 
                }

  end

  def admin_panel_access_log
    options = params
    @log_data = ::Report::ApiLog.get_data(options={:controller_access=>"admin"})
    @controller_access = "admin"
    render :action=> "index"
    
  end

  def edit_event
    @event = ::Report::Event.find(params[:event_id])
  end

  def update_event
    @event = ::Report::Event.find(params["report_event"][:id])
    @event.title = params["report_event"][:title]
    @event.save
    redirect_to "/admin/api_log/event_list"
  end

   private
   
  def validate_user_access(module_name="ApiLog")

    @access_level = ModuleAccessForRole.user_access_level_for_module(current_user,module_name)
    unless (@access_level > 0) || current_user.email == AppConfig["superadmin_email"] 
     redirect_to "/"
    end
  end 
    
end