class Admin::UsersController < ApplicationController
  # skip_before_filter :authenticate_user!
  before_filter :validate_user_access, :except=>[:home,:kpi_data,:push_notification_identifier_response_summary_report,:push_notification_response_report,:push_notification_report_identifier_wise,:push_notification_report]
  # http_basic_authenticate_with name: "admin", password: "w!nt3r(00l" if !Rails.env.production?
  before_filter :admin_basic_auth
  http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
  # before_filter :basic_auth
  layout 'admin'

  # require 'countries/global'
  
  # GET /relevances
  # GET /relevances.json
  def index
 	 session[:show_variable_home] = 'Users'
    session[:dashboard] = 'parent'
    @all_countries = valid_country_list
    @users = User.order_by("id DESC").paginate(:page => params[:page]||1, :per_page => 20)
    
  end
  
  def home
  end

  def kpi_data
    validate_user_access("KpiData")
     
    @data = Report::KpiData.report_data(params[:start_date],params[:end_date]).with_indifferent_access
  end
  
  def deleted_user_details
    @deleted_users = ::System::DeletedUserDetail.order_by("created_at desc")
  end

  
  def get_user_device_details
    @user = User.find(params[:id])
    render :layout=> false
   
  end
  
  def user_not_signed_in_since_form
    render :layout=>false
  end
  
  def user_not_signed_in_since
    @user_count = User.where(:current_sign_in_at.lte => (Date.today - params[:no_of_month].to_i.months) ).count
    
  end

  def get_push_notification_identifier_detail
    @notification = BatchNotification::BatchNotificationManager.where(identifier:params[:identifier]).last
    render :layout=>false
  end

  def user_login_atleast_once_at_interval_in_duration_form
    render :layout=>false
  end
  
  def user_login_atleast_once_at_interval_in_duration
    duration = params[:duration].to_i
    interval = params[:interval].to_i
    @count = Report::UserBasicSummaryReportData.signin_atleast_at_interval_in_duration(duration,interval)
     
  end


  def edit_user_detail
    @all_countries = valid_country_list
    @user =  User.find(params[:id])
    @member = @user.member
  end

  def get_user_detail
    if params[:user_email].present?
      @user =  User.where(email:params[:user_email]).last
    elsif params[:member_id].present?
      @user =  User.where(member_id:params[:member_id]).last
      
    else
      @user =  User.find(params[:id])
    end

    @user.status = "approved" if @user.status == "confirmed"
    @user.save
    @member =  @user.member
    if params[:user_email].present? ||  params[:member_id].present?
      render :layout=>false
    end
  end

  def update_user_datail
    @user =  User.find(params[:id])
    allowed_signin_provider = @user.allowed_signin_provider || []
    allowed_signin_provider << ["Nurturey"] if @user.provider.blank? && allowed_signin_provider.blank?
    allowed_signin_provider = allowed_signin_provider.flatten.compact.uniq
    Rails.logger.info "User before update #{@user.inspect}"
    @member = @user.member
    data = {}
    user_trial_days = @user.trial_days
    pricing_exemption = @user.pricing_exemption
    data[:status] = params[:status]
    data[:country_code] = params[:country_code]
    data[:user_type] = params[:user_type]
    data[:trial_days] = params[:trial_days]     
    data[:activity_status] = params[:activity_status]     
    data[:pricing_exemption] = params[:pricing_exemption]
    data[:allowed_signin_provider] = params[:allowed_signin_provider]
    if (allowed_signin_provider & data[:allowed_signin_provider]).blank?
      @user.refresh_api_token
    end

    if current_user.email == AppConfig["superadmin_email"] || @user.role_for_admin_panel == "superadmin"
      data[:role_for_admin_panel] = params[:role_for_admin_panel]     
    end
    

    Rails.logger.info "User info updating"
    data.each do |key,val|
      @user[key] = val
    end
    @user.unregister_from_mail = params[:subscibe_mails] == "true" 
    @user.save(validate:false)
    @user.reload
    Rails.logger.info "User after update #{@user.inspect}"
    
    @user.update_attributes(data)
    @member.reload
    if user_trial_days != data[:trial_days]
      @member.update_family_and_user_trial_days(@member,options={})
    end
    if pricing_exemption != data[:pricing_exemption]
      @member.update_family_and_user_pricing_exemption(@member,options={})
    end

    notification_on = params[:notification_on] ==  "true" ? "on" : "off"
    @member.subscribe_notification(notification_on)

    params[:subscibe_mails] =  params[:subscibe_mails] == "true" ?  {"0"=>"on"}  : {"0"=>"off"} 

    params[:subscibe_mails] = params[:subscibe_mails].with_indifferent_access
    subscription_mail_present = params[:subscibe_mails].present?
    params[:subscibe_mails].delete_if { |k, v| (v=="0"|| v=="off" || v=="false") } if subscription_mail_present
    
    @member.subscribe_mail((params[:subscibe_mails]||{}).keys) if subscription_mail_present

    redirect_to "/admin/users/get_user_detail?id=#{@user.id}" and return true
  end

  def user_families_detail
    if params[:family_id].present?
      @families =  Family.in(:id=>params[:family_id])
    else
      member = Member.find(params[:member_id])
      family_ids =  FamilyMember.where(:member_id=>member.id).pluck(:family_id).uniq
      @families =  Family.in(:id=>family_ids)
    end
    render :layout=>false

  end

  def user_type
    # all_user = User.all.select("created_at").order_by("created_at DESC").paginate(:page => params[:page], :per_page => 3).group_by{ |u| u.created_at.to_date.to_s}
    # web_user = User.where(uid:nil).order_by("created_at DESC").paginate(:page => params[:page], :per_page => 3).group_by { |u| u.created_at.to_date.to_s }
    # fb_user = User.not_in(uid:nil).order_by("created_at DESC").paginate(:page => params[:page], :per_page => 3).group_by { |u| u.created_at.to_date.to_s }
    # @data = {}
    # @kk = User.all.order_by("created_at DESC").paginate(:page => params[:page], :per_page => 3)
    # all_user.keys.each do | k| 
    #   @data[k] = {:fb_user=> (fb_user[k].count rescue 0), :web_user=> (web_user[k].count rescue 0)}
    # end


  page = params[:page]||1
  page = page.to_i
  per_page = 20
  hash_data = []
  hash_data = User.collection.aggregate([
    {"$match": {:deleted_at=>nil}},
    {"$group": {_id: { "$dateToString":{ format: "%Y-%m-%d", date:"$created_at" } },
             "fb_user": {
                "$sum": {
                    "$cond": [ { "$ne": [ "$uid",  "$null" ] }, 1, 0]
                }
            },
            "web_user": {
                "$sum": {
                    "$cond": [ { "$eq": [ "$uid",  "$null" ] }, 1, 0]
                }
            }
             } 
                         },
    {"$sort": {_id: -1}},
    {"$limit": (page*per_page)} ,
    {"$skip": ((page-1) * per_page) } ,
     {
        "$project": {
        "_id": 0,
           "created_at":"$_id", 
            "fb_user": 1,
            "web_user":1,
                          
        }
    }

])
 @data = hash_data
 all_user = User.only("created_at").all.order_by("created_at DESC").group_by{ |u| u.created_at.to_date.to_s}.keys
 @paginated_data = all_user.paginate(:page => params[:page], :per_page => per_page)
 

  end
  
  def aprove_user
  	User.in(:id=>params[:ids]).update_all({"status"=>"approved"})
  	 respond_to do |format|
      format.json {render :json=>{ids:params[:ids]}}
    end
  end

  def report
    @report_data = Report::UserBasicSummaryReportData.last || Report::UserBasicSummaryReportData.new
    if @report_data.report_data.blank? || @report_data.updated_at.utc.to_date < (Time.now - 4.hour).utc.to_date 
      current_user.delay.save_user_report_for_admin_panel
    end
    @all_countries = @report_data.country_list.map{|a| [a,(ISO3166::Country.find_country_by_name(a).alpha2.downcase rescue "")]}
    @report = @report_data.report_data
  end
  
  def search_user
    @all_countries =[]
    @all_countries = valid_country_list
    params[:first_name] = nil if params[:first_name] == "Name"
    params[:email] = nil if params[:email] == "Email"
    params[:status] = nil if params[:status] == "Status"
    if params[:country_code].blank?
      params[:country_code] = nil  
    else
      params[:country_code] = params[:country_code].map{|code| eval(code)}.flatten
    end

    if params[:activity_status].blank?
      params[:activity_status] = nil  
    else
      params[:activity_status] = params[:activity_status]
    end

    if params[:segment].blank?
      params[:segment] = nil  
    else
      params[:segment] = params[:segment]
    end
    if params[:user_type].blank?
      params[:user_type] = nil  
    else
      params[:user_type] = params[:user_type]
    end
    
    if params[:stage].blank?
      params[:stage] = nil  
    else
      params[:stage] = params[:stage]
    end
    if params[:email_domain_status].blank?
      params[:email_domain_status] = nil  
    else
      params[:email_domain_status] = params[:email_domain_status]
    end

    data = {:email_domain_status=>params[:email_domain_status],:user_type=>params[:user_type], :stage=> params[:stage], :segment=>params[:segment], :activity_status=> params[:activity_status], :first_name=>params[:first_name], :email=>params[:email], :status=>params[:status] ,:country_code=>params[:country_code]}
    
    data = data.reject{|k,v| v.blank?}
    if params[:page].blank?
      session[:data] = data 
    else
      data = session[:data]
    end
    params[:first_name] = data[:first_name]
    first_query =  false
    if data.present?
      data.each do |k,v|
        if k.to_s == "country_code"
          val = (v)
        else
          optRegexp = []
          [v].flatten.each do |opt|
            if k == :first_name || k == :email
              optRegexp.push(/#{opt}/i) 
            else
              optRegexp.push(/^#{opt}/i) 
            end
          end
          val = optRegexp
        end
        if first_query
          if k == :first_name || k == :email
            @users = @users.where(k=>val[0])
          else
            @users = @users.in(k=>val)
          end
        else

          first_query = true
          if k == :first_name || k == :email
            @users = User.where(k=>val[0])
          else
            @users = User.in(k=>val)
          end
        end
      end 
    else
      @users = User.all.order_by("id DESC")
    end
    if params[:user_subscription].present?
      members_with_premium_subscription = []
      free_subscription_member_ids = []
      subscriptions = params[:user_subscription].deep_copy
        subscription_manager_for_free = SubscriptionManager.free_subscription
      if subscriptions.include?(subscription_manager_for_free.title)
        members_with_premium_subscription = Subscription.where(:subscription_manager_id.ne=>subscription_manager_for_free).only(:member_id).pluck(:member_id)
        subscriptions.delete(subscription_manager_for_free.title)
        free_subscription_member_ids = Member.where(:id.nin=>members_with_premium_subscription).pluck(:id) #if members_with_premium_subscription.present?
      end 
      if subscriptions.present?
        subscription_manager_id = SubscriptionManager.where(:title.in=>subscriptions).pluck(:id)
        members_with_premium_subscription = Subscription.where(:subscription_manager_id.in=>subscription_manager_id).only(:member_id).pluck(:member_id)
      end
        subscription_selected_member_ids = free_subscription_member_ids + members_with_premium_subscription
        @users = @users.where(:member_id.in=>subscription_selected_member_ids) #if members_with_premium_subscription.present?
       
    end
     
     
    @users = @users.order_by("id DESC")
    if  params[:commit] == "Export to Excel"
      headers.delete("Content-Length")
      headers["Content-Disposition"] = "attachment; filename=\"#{Date.today.to_date2}_user_export.csv\""
      headers["Content-Type"] = "text/csv"
      headers["Cache-Control"] ||= "no-cache"
      self.response_body = download_data(@users)
      params[:commit] = nil
    else
      @users = @users.paginate(:page => params[:page], :per_page => 20)
    end

  end
  
  def download_data(data)
    Enumerator.new do |yielder|
        yielder << CSV.generate_line(["UserId", "Name", "Email","User Type","Status","Provider","FB ID","Signup Source", "Country Code", "Activity Status","Activity Status v2", "Segment","Trial Days","FBPixelID"])
      data.each do |user|
        begin
          uid = user.uid 
          yielder << CSV.generate_line([user.id,user.first_name, user.email,user.user_type,user.status,user.provider,uid,user.signup_source,(user.country_code.upcase rescue nil),user.activity_status,user.activity_status_v2,user.segment,user.trial_days,(user.member_id+user.id)])
        rescue Exception=>e 
        end
      end
    end
  end

  def search
    @report = []
    @all_countries =[]
     
    all_user_countries = valid_country_list
    @all_countries = all_user_countries.keys
    begin
      if params[:start_date].to_date.present? && params[:end_date].to_date.present?
        users = User.gte(last_sign_in_at: params[:start_date].to_date).lte(last_sign_in_at: params[:end_date])
        returned = users.gt(sign_in_count: 1)
        @average_user = (returned.count.to_f / users.count.to_f).round(2)
        @average_user =  @average_user.to_s == "NaN" ? 0 :  @average_user
      end
    rescue Exception=> e
    end
    @countries = params[:country].present? && all_user_countries[params[:country]].present? ? {params[:country]=>all_user_countries[params[:country]]} : all_user_countries
    @countries[nil] =  ["",nil]  if ( (!@countries.include?(nil) || !@countries.include?("")) && !params[:country].present?)
    @countries.each do |country_code,country_name|      
      user_country_name = (ISO3166::Country[country_code] || (country_code.try(:titleize))) rescue country_code
        
      member_ids = User.in(country_code: country_name)
      begin
        if params[:start_date].to_date.present?
         member_ids= member_ids.gte(created_at: params[:start_date].to_date)
        end
      rescue
      end
      begin
        if params[:end_date].present?
         member_ids =  member_ids.lte(created_at: params[:end_date].to_date)
        end
      rescue Exception => e

      end
      user_ids = member_ids.pluck(:id)#.map(&:id)
      member_ids = ParentMember.in(user_id: user_ids ).pluck("id")
      family_ids = Family.in(member_id: member_ids ).pluck("id")
      childern = FamilyMember.in(family_id: family_ids).in(role: ["Son","Daughter"]).pluck(:member_id).flatten.uniq
      milestones =   Milestone.in(member_id:childern).pluck("id")
      measurements = Health.in(member_id:childern).only(:id)
      immunisations = Vaccination.where({'member_id' => { "$in" => childern} }).where(opted: "true").pluck(:id)
      jabs = Jab.in(vaccination_id:immunisations).count
      timelines = Timeline.where({'member_id' => { "$in" => childern} }).pluck("id")
      documents = Document.where({'member_id' => { "$in" => childern} }).pluck("id")

      picture_count = 0
      picture_count = picture_count + Picture.in(imageable_id: immunisations).pluck("id").count
      picture_count =  picture_count + Picture.in(imageable_id: milestones  ).pluck("id").count
      picture_count = picture_count + Picture.in(imageable_id: member_ids ).pluck("id").count
      picture_count = picture_count + Picture.in(imageable_id: childern ).pluck("id").count
      picture_count = picture_count + Picture.in(imageable_id: timelines ).pluck("id").count
      

      @report << {documents:documents.count, jabs: jabs,users:user_ids.count, country:user_country_name, pics:picture_count, immunisations: immunisations.count,timelines:timelines.count, childern: childern.count,milestones:milestones.count,measurements:measurements.count }
    end
     @report = @report#.paginate(:page => params[:page], :per_page => 15)
  end

  def update_user_status
    @user = User.find(params[:id])
    @last_status = @user.status
    options = {:ip_address=>request.remote_ip}
    if @user.status==User::STATUS[:confirmed] || @user.status==User::STATUS[:approved]
      @user.update_attribute("status",User::STATUS[:unapproved])
    else
      @user.update_attribute("status",User::STATUS[:confirmed])
      @user.update_attribute("failed_login_attempt_count",0)
    end
    member = @user.member
    @user.delay.update_user_stage(stage_value=nil,options)
    member.update_family_and_user_segment(nil,options)
    member.update_family_and_user_trial_days(member,options)

    respond_to do |format|
      format.js
    end
  end
  def user_subscription_history
    @subscription_history = Subscription::History.where(member_id:params[:id]).order_by("created_at desc")
    render :layout=>false
    
  end  

  def delete_user
    begin
      @user = User.where(:id=>params[:id]).last
      @user.delete_account(deleted_by=current_user)
      @status = true
    rescue Exception=>e 
      @status = false
    end
    respond_to do |format|
      format.js
    end
  end

# if unregister_from_mail is true no mail will be sent to user in future
  def update_user_for_mail_registration
    @user = User.find(params[:id])
    @last_status = @user.unregister_from_mail
    if @user.unregister_from_mail== true
      @user.update_attribute("unregister_from_mail",false)
    else
      @user.update_attribute("unregister_from_mail",true)
    end
    respond_to do |format|
      format.js { render :js=> "$('#mail-#{@user.id}').text('#{user_mail_status(@user)}')"}
    end
  end


  def push_notification_report
    validate_user_access("PushNotificationReport")
    @user_notifications = Report::PushNotificationReport.push_notification_report_data("created_at",params)
    @user_notification_sent = {}
    Report::PushNotificationReport.push_notification_report_data("updated_at",params).map{|a| @user_notification_sent[a["sent_date"]] = a }
  end
  
  # identifier data for on selected date
  def push_notification_report_identifier_wise
    validate_user_access("PushNotificationReport")
    @selected_date = params[:selected_date]
    destination = params[:destination]
    @identifier_list = UserNotification.distinct(:type)
    @user_notifications = Report::PushNotificationReport.data_for_identifier("created_at",destination,@selected_date,params )
    @user_notification_sent = {}
    Report::PushNotificationReport.data_for_identifier("updated_at",destination,@selected_date,params ).map{|a| @user_notification_sent.merge!(a) }
    #[{"2019-01-12"=>{"total_count"=>5, "4"=>2, "sent"=>5, "pending"=>0, "11"=>3}}, {"2019-01-14"=>{"total_count"=>69180, "12"=>105, "sent"=>22875, "pending"=>46305, "2"=>14888, "9"=>51, "1"=>67, "15"=>116, "birthday_wish"=>4, "pointers"=>5520, "10"=>59, "4"=>2, "7"=>558, "11"=>3, "14"=>1501, "17"=>1}}, {"2019-01-16"=>{"total_count"=>14283, "5"=>4182, "sent"=>12209, "pending"=>2074, "4"=>4, "pointers"=>5519, "9"=>22, "1"=>47, "19"=>82, "15"=>12, "7"=>841, "10"=>28, "14"=>1472}}, {"2019-01-20"=>{"total_count"=>11563, "4"=>2, "sent"=>11296, "pending"=>267, "17"=>2, "12"=>2258, "7"=>743, "10"=>33, "19"=>104, "1"=>8154}}, {"2019-01-21"=>{"total_count"=>72624, "4"=>3, "sent"=>25165, "pending"=>47459, "pointers"=>5719, "9"=>110, "15"=>1196, "19"=>84, "7"=>657, "10"=>46, "11"=>4, "17"=>1, "2"=>15281, "12"=>103, "14"=>1865, "1"=>96}}, {"2019-01-15"=>{"total_count"=>7832, "11"=>5, "sent"=>5968, "pending"=>1864, "pointers"=>306, "12"=>897, "birthday_wish"=>9, "3"=>137, "9"=>93, "8"=>1796, "1"=>71, "14"=>1411, "7"=>693, "10"=>46, "15"=>493, "4"=>11}}, {"2019-01-22"=>{"total_count"=>22135, "11"=>4, "sent"=>19961, "pending"=>2174, "12"=>98, "8"=>16149, "3"=>145, "9"=>61, "19"=>85, "15"=>924, "14"=>1413, "1"=>90, "7"=>731, "17"=>1, "4"=>3, "pointers"=>232, "10"=>25}}, {"2019-01-23"=>{"total_count"=>14271, "11"=>7, "sent"=>12027, "pending"=>2244, "5"=>4024, "19"=>82, "7"=>540, "14"=>1438, "9"=>28, "15"=>121, "10"=>37, "pointers"=>5713, "1"=>33, "4"=>4}}, {"2019-01-24"=>{"total_count"=>68337, "17"=>1, "sent"=>20110, "pending"=>48227, "11"=>1, "pointers"=>211, "12"=>887, "2"=>15413, "9"=>124, "1"=>241, "10"=>430, "15"=>355, "7"=>921, "19"=>119, "14"=>1407}}, {"2019-01-29"=>{"total_count"=>7643, "pointers"=>236, "sent"=>5565, "pending"=>2078, "19"=>94, "1"=>116, "7"=>727, "15"=>133, "3"=>141, "10"=>39, "4"=>2, "12"=>21, "action_for_home"=>1, "8"=>2194, "9"=>454, "14"=>1407}}, {"2019-01-19"=>{"total_count"=>1877, "11"=>1, "sent"=>1515, "pending"=>362, "12"=>95, "9"=>139, "19"=>81, "7"=>736, "1"=>100, "10"=>44, "pointers"=>319}}, {"2019-01-30"=>{"total_count"=>14707, "4"=>1, "sent"=>12373, "pending"=>2334, "pointers"=>5835, "9"=>156, "action_for_home"=>1, "10"=>34, "7"=>543, "15"=>161, "19"=>82, "1"=>57, "14"=>1470, "5"=>4033}}, {"2019-01-27"=>{"total_count"=>6325, "10"=>44, "sent"=>6064, "pending"=>261, "7"=>740, "19"=>107, "12"=>115, "1"=>5058}}, {"2019-01-31"=>{"total_count"=>60587, "4"=>3, "sent"=>17373, "pending"=>43214, "12"=>99, "15"=>73, "pointers"=>246, "9"=>53, "2"=>13806, "1"=>244, "action_for_home"=>1, "7"=>1157, "14"=>1520, "10"=>33, "19"=>138}}, {"2019-02-02"=>{"total_count"=>2105, "11"=>3, "sent"=>1871, "pending"=>234, "pointers"=>364, "1"=>162, "15"=>0, "7"=>657, "14"=>0, "12"=>263, "19"=>106, "9"=>269, "10"=>46, "4"=>1}}, {"2019-02-03"=>{"total_count"=>9641, "4"=>8, "sent"=>9407, "pending"=>234, "10"=>57, "7"=>936, "14"=>0, "19"=>95, "1"=>8118, "11"=>9, "12"=>184}}, {"2019-02-04"=>{"total_count"=>18427, "4"=>2, "sent"=>5601, "pending"=>12826, "pointers"=>3214, "2"=>488, "1"=>28, "10"=>5, "7"=>600, "9"=>103, "11"=>1, "19"=>92, "12"=>31, "14"=>692, "action_for_home"=>1, "15"=>344}}, {"2019-02-05"=>{"total_count"=>23277, "17"=>2, "sent"=>22236, "pending"=>1041, "4"=>5, "12"=>2204, "9"=>87, "19"=>143, "7"=>1013, "11"=>1, "pointers"=>1173, "8"=>16163, "15"=>158, "1"=>255, "14"=>840, "3"=>126, "action_for_home"=>1, "10"=>65}}, {"2019-01-13"=>{"total_count"=>6453, "17"=>2, "sent"=>6202, "pending"=>251, "12"=>96, "birthday_wish"=>4, "10"=>45, "1"=>5136, "7"=>919}}, {"2019-02-06"=>{"total_count"=>15534, "17"=>1, "sent"=>13894, "pending"=>1640, "11"=>1, "5"=>8353, "4"=>9, "pointers"=>3311, "15"=>376, "19"=>102, "10"=>43, "7"=>912, "14"=>627, "1"=>89, "9"=>70}}, {"2019-01-28"=>{"total_count"=>78964, "11"=>1, "sent"=>29688, "pending"=>49276, "4"=>3, "pointers"=>5952, "12"=>2159, "2"=>15655, "19"=>103, "9"=>147, "1"=>100, "action_for_home"=>1, "15"=>3153, "7"=>845, "10"=>48, "14"=>1521}}, {"2019-01-17"=>{"total_count"=>65673, "1"=>131, "sent"=>18573, "pending"=>47100, "19"=>127, "15"=>99, "10"=>426, "pointers"=>317, "9"=>145, "12"=>130, "4"=>11, "2"=>15000, "11"=>3, "14"=>1519, "7"=>665}}, {"2019-01-25"=>{"total_count"=>10833, "4"=>1, "sent"=>9125, "pending"=>1708, "pointers"=>5775, "19"=>84, "3"=>149, "9"=>162, "12"=>135, "1"=>220, "15"=>65, "10"=>120, "7"=>908, "14"=>1506}}, {"2019-02-08"=>{"total_count"=>10670, "pointers"=>3352, "sent"=>10162, "pending"=>508, "19"=>75, "action_for_home"=>1, "1"=>262, "7"=>908, "15"=>101, "3"=>135, "14"=>690, "9"=>75, "10"=>4447, "12"=>106, "4"=>10}}, {"2019-02-09"=>{"total_count"=>3627, "17"=>1, "sent"=>3467, "pending"=>160, "pointers"=>1227, "10"=>41, "7"=>878, "9"=>117, "12"=>929, "19"=>91, "1"=>165, "11"=>18}}, {"2019-02-07"=>{"total_count"=>65590, "17"=>1, "sent"=>18349, "pending"=>47241, "pointers"=>1282, "2"=>15158, "9"=>49, "19"=>78, "14"=>597, "10"=>46, "15"=>78, "7"=>670, "12"=>63, "1"=>315, "11"=>1, "4"=>10, "16"=>0, "action_for_home"=>1}}, {"2019-01-18"=>{"total_count"=>11825, "11"=>5, "sent"=>10022, "pending"=>1803, "3"=>135, "10"=>964, "9"=>184, "12"=>101, "pointers"=>5613, "7"=>701, "14"=>1871, "15"=>12, "19"=>91, "1"=>345}}, {"2019-02-10"=>{"total_count"=>6334, "19"=>99, "sent"=>6180, "pending"=>154, "7"=>715, "10"=>46, "12"=>284, "4"=>4, "1"=>5015, "11"=>17}}, {"2019-01-26"=>{"total_count"=>1667, "12"=>109, "sent"=>1375, "pending"=>292, "19"=>113, "1"=>115, "10"=>54, "pointers"=>264, "9"=>87, "7"=>633}}, {"2019-02-11"=>{"total_count"=>69603, "4"=>2, "sent"=>21876, "pending"=>47727, "19"=>106, "10"=>21, "15"=>72, "7"=>857, "14"=>717, "12"=>183, "1"=>130, "9"=>67, "pointers"=>4349, "action_for_home"=>1, "2"=>15371}}, {"2019-02-12"=>{"total_count"=>7435, "pointers"=>186, "sent"=>3604, "pending"=>3831, "8"=>1348, "3"=>16, "action_for_home"=>0, "10"=>0, "14"=>451, "7"=>708, "19"=>90, "1"=>6, "9"=>78, "15"=>721, "12"=>0}}, {"2019-02-01"=>{"total_count"=>9903, "1"=>401, "sent"=>8460, "pending"=>1443, "14"=>1051, "11"=>1, "action_for_home"=>1, "15"=>114, "19"=>101, "pointers"=>4044, "10"=>723, "12"=>905, "3"=>123, "9"=>71, "7"=>925}}].map{|a| @user_notification_sent.merge!(a) }
     
  end
  
  # response data for a selected indentifier for a time range
  def push_notification_response_report
    validate_user_access("PushNotificationReport")
    @identifier_list = UserNotification.distinct(:type)
    @identifier = params[:identifier]
    options = {:start_date=>(params[:start_date1]|| (Date.today - 30.days)),:end_date=>(params[:end_date1] || Date.today)}.merge(params)
    @user_notifications =  Report::PushNotificationReport.data_for_identifier("created_at",@destination,@selected_date,options )
    @user_notification_sent = {}
    @user_notification_assigned = {}
    @user_notifications.map{|a| @user_notification_assigned.merge!(a) }
    Report::PushNotificationReport.data_for_identifier("updated_at",@destination,@selected_date,options ).map{|a| @user_notification_sent.merge!(a) }
  
  end

  # response data for a all  indentifier for a time range
  def push_notification_identifier_response_summary_report
    validate_user_access("PushNotificationReport")
    @identifier_list = UserNotification.distinct(:type)
    sorted_identifier = @identifier_list.select{|i| i.to_i >0}.map(&:to_i).sort.map(&:to_s)
    @identifier_list = (@identifier_list - sorted_identifier) + sorted_identifier
    options = {:start_date=>params[:start_date1],:end_date=>params[:end_date1]}.merge(params)
    @user_notifications =  Report::PushNotificationReport.data_for_identifier("created_at",@destination,@selected_date,options )
    @user_notification_sent = {}
    @user_notification_assigned = {}
    @user_notifications.map{|a| @user_notification_assigned.merge!(a) }
    Report::PushNotificationReport.data_for_identifier("updated_at",@destination,@selected_date,options ).map{|a| @user_notification_sent.merge!(a) }
  
  end
  
  def user_subscription_change_form
    member =  Member.find(params[:id])
    current_user = member
    api_version = 4
    subscription_manager_id =  SubscriptionManager.free_subscription.id.to_s
    @subscription_available = SubscriptionManager.valid_subscription_manager(member,4)
    user_families_id = member.user_families.map{|a| a.id.to_s }
    @user_subscription = Subscription.in(family_id:user_families_id,member_id:member.id.to_s).not_expired.last 
     
    user_subscribed_family_id = Subscription.in(family_id:user_families_id,member_id:member.id.to_s).not_expired.premium_subscription(current_user,api_version).pluck(:family_id)

    family_subscribed_for_free_subscription = user_subscribed_family_id.present? && Subscription.is_premium?(@user_subscription.subscription_listing_order)  ? [] : user_families_id - Subscription.in(family_id:user_families_id).not_expired.premium_subscription(current_user,api_version).pluck(:family_id).map(&:to_s)
    @family_list = Family.in(id: (user_subscribed_family_id + family_subscribed_for_free_subscription).uniq)
    render :layout=>false

  end


  def edit_credit_in_family_cvtme_purchase
    member =  Member.find(params[:id])
    current_user = member
    api_version = 5
    user_families_id = member.user_families.map{|a| a.id.to_s }
    @family_list = member.user_families
    render :layout=>false

  end
  def update_credit_in_family_cvtme_purchase
    member =  Member.find(params[:member_id])
    current_user = member
    credit_count = params[:credit_test_count].to_i
    @family = Family.find(params[:family_id])
    cvtme = Child::VisionHealth::Purchase.where(family_id:@family.id).last || Child::VisionHealth::Purchase.default_subscription(@family.id)
    if params[:commit] == "Set remaining test count to 0"
      cvtme.total_attempt_allowed  = cvtme.total_attempt_taken
    else
      if credit_count == -1
        cvtme.total_attempt_allowed = -1
        cvtme.admin_credit = -1
        cvtme.purchase_date = params[:purchase_date]
      else
        cvtme.total_attempt_allowed = cvtme.total_attempt_allowed.to_i + credit_count
        cvtme.admin_credit = cvtme.admin_credit.to_i + credit_count
        cvtme.purchase_date = params[:purchase_date]
      end
    end
    cvtme.save
    @cvtme = @family.cvtme_purchase
    render :layout=>false

  end

  def update_user_subscription
    family_id = params[:family_id]
    @current_member = Member.find(params[:member_id])
    subscription = Subscription.where(family_id:family_id).last || Subscription.find_create_subscription(@current_member,family_id,params[:subscription_device],api_version=4)
    api_version = 4
    receipt_response = {}
    receipt_response[:is_trial_period] = params[:is_trial_period].present?
    receipt_response[:original_purchase_date_ms] = params[:subscription_start_date].to_date.strftime('%Q') rescue nil
    receipt_response[:purchase_date_ms] = (params[:subscription_last_renewal_date].present? ? params[:subscription_last_renewal_date] : params[:subscription_start_date]).to_date.strftime('%Q') rescue nil
    receipt_response[:expires_date_ms] = params[:subscription_expiry_date].to_date.strftime('%Q') rescue nil
    receipt_response[:cancellation_date] = (params[:subscription_cancellation_date_selected].to_date.strftime('%Q') if params[:cancellation_date].present?) rescue nil
    

    subscription_manager = SubscriptionManager.find(params[:subscription_manager_id])
    params[:subscription_expiry_date] = params[:subscription_expiry_date].blank? ? subscription_manager.calculate_end_date : params[:subscription_expiry_date]
    params[:subscription_expiry_date] = nil if subscription_manager.subscription_listing_order == Subscription::Level["free"]
    params[:receipt_response] = receipt_response
    begin
      if subscription.present? && !subscription.new_record?
        platform = params[:subscription_device] || subscription.platform 
        begin
          subscription.update_subscription(subscription_manager,api_version,@current_member,family_id,platform,params)
        rescue Exception=>e 
           
        end
        subscription.platform = platform
      else
         @device = @current_member.devices.last  
        platform = params[:subscription_device] || @device.platform 
        subscription = Subscription.create_subscription(subscription_manager,api_version,@current_member,family_id,platform,params)
      end  
      subscription.assigned_by = "admin"
      subscription.save
      Subscription::History.where(subscription_id:subscription.id).last.update_attribute(:assigned_by,"admin")
      @subscription =  !subscription.expired? ? subscription  : Subscription.default_subscription(family_id)
    rescue Exception => e
      Rails.logger.info e.message
      @error = true  
    end  

    render :layout=>false

  end

  private

  def valid_country_list
    @report = []
    @all_countries =[]
     
    aa = {}    
    user_country_codes = User.distinct(:country_code).map{|a| a.try(:downcase)}.uniq
    valid_country_code = user_country_codes.select{|country| ISO3166::Country[country]}
    valid_country_code.each do |i|
      aa[i]  = [i, i.upcase]
    end
    invalid_country_code = user_country_codes - valid_country_code
    invalid_country_code = invalid_country_code.compact

    invalid_country_code.delete("uk")
    invalid_country_code.delete("UK")
    invalid_country_code.push("gb")
    valid_country_with_name = []
     
    invalid_country_code.each do |i|   
      if (temp = ISO3166::Country.find_country_by_name(i.titleize)).present?
        code = temp.alpha2
        valid_country_with_name << i
        if aa[code.downcase].present?
          aa[code.downcase] << i
        else
          aa[code.downcase] = [i]
        end
      end
    end
    (invalid_country_code - valid_country_with_name).each do |i|
      aa[i] = [i,i.upcase]
    end    
    if aa["gb"]
      aa["gb"] << "uk"
      aa["gb"] << "UK"
    end  
    aa
  end


  def user_mail_status(user)
    if user.unregister_from_mail==true 
      "Register for mail"
    else
      "Unregister from mail"
    end
  end

private
  
  def validate_user_access(module_name="SummeryReports&UserList")
    begin
      @user_role = UserRoleForAdminPanel.where(email:current_user.email).last.role
      module_access_status = ModuleAccessForRole.where(module_name: module_name, role: @user_role).present?
      @access_level = module_access_status ? 2 : 0
    rescue Exception=> e
      @access_level = 0
    end 
    @access_level = 3 if current_user.email == AppConfig["superadmin_email"] 
    unless (@access_level > 0) || current_user.email == AppConfig["superadmin_email"] 
     redirect_to "/"
    end
  end

end