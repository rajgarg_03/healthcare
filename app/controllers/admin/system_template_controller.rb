class Admin::SystemTemplateController < ApplicationController
  # skip_before_filter :authenticate_user!
  before_filter :admin_basic_auth
  http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
  
  before_filter :validate_user_access
  before_filter :get_template_class
  layout 'admin'
  
  autocomplete :gpsoc_immunisation_jab, :title  
  autocomplete :system_immunisation_jab, :title  
  autocomplete :nhs_syndicate_content, :title  

  def autocomplete_gpsoc_immunisation_jab_title
    term = params[:term]
    if term.blank?
      scenarios = ::System::GpsocImmunisationJab.all.order_by("custom_id asc")
    else
      scenarios = ::System::GpsocImmunisationJab.or({:term=>/#{term}/i},{:custom_id=>term},{:snomed_code=>/#{term}/i},{:read_v2_code=>/#{term}/i},{:ctv3_code=>/#{term}/i},{:snomed_ct_description_id=>/#{term}/i}).order_by("custom_id asc")
    end
    render :json => scenarios.map { |scenario| {:id => scenario.custom_id, :label => "ID: #{scenario.custom_id} , Title: #{scenario.term}, SNOME code: #{scenario.snomed_code}, SNOMEDCT ID: #{scenario.snomed_ct_description_id}", :value => "ID: #{scenario.custom_id} , Title: #{scenario.term}, SNOME code: #{scenario.snomed_code}, SNOMEDCT ID: #{scenario.snomed_ct_description_id}"} }

  end
  def autocomplete_system_immunisation_jab_title
    term = params[:term]
    if term.blank? && params[:country_code].blank?
      scenarios = ::SystemJab.all.order_by("due_on_in_day_int asc").map(&:get_jab_detail).compact
    elsif params[:country_code].present?
      country_code = Member.sanitize_country_code(params[:country_code])
      scenarios = ::SystemVaccin.where(:name=>/#{term}/i).where(:country.in=>country_code).map(&:system_jabs).flatten.compact.sort_by{|a| a["due_on_in_day_int"].to_i}.map(&:get_jab_detail).compact
    else
      country_code = Member.sanitize_country_code("uk")
      scenarios = ::SystemVaccin.where(:name=>/#{term}/i).where(:country.in=>country_code).map(&:system_jabs).flatten.compact.sort_by{|a| a["due_on_in_day_int"].to_i}.map(&:get_jab_detail).compact
    end
    render :json => scenarios.map { |system_jab| {:id => system_jab.id, :label => "Vaccine: #{system_jab['name']},  Due_on: #{system_jab['due_on']}, JabType: #{system_jab['jab_type']}" , :value => "Vaccine: #{system_jab['name']},  Due_on: #{system_jab['due_on']}, JabType: #{system_jab['jab_type']}"} }

  end


  def autocomplete_nhs_syndicate_content_title
    term = params[:term]
    if term.blank?
      content_list = ::Nhs::SyndicatedContent.all.order_by("custom_id asc")
    else
      content_list = ::Nhs::SyndicatedContent.or({:title=>/#{term}/i},{:custom_id=>term}).order_by("custom_id asc")
    end
    render :json => content_list.map { |content| {:id => content.id, :label => "CustomID: #{content.custom_id} , Title: #{content.title}", :value => "CustomID: #{content.custom_id} , Title: #{content.title}"} }

  end


   
  def get_gpsoc_immunisation_jab_detail 
    @class_name = ::System::GpsocImmunisationJab
    @record = @class_name.where(custom_id:params[:custom_id]).last
    country_column = get_country_column_for_mapped_table(@class_name)
    @country_code = @record[country_column] || params[:country_code]
    @data = [@record]
    @show_only = true
    @fields = get_record_fields(@record)
    next_previous_system_record(@record,@class_name,country_column)
    render :action=>"show_system_record", layout:false
  end


  def get_linked_syndicated_content
    linked_tool_object_id = params[:record_id]
    linked_tool_class = params[:class_name]
    @syndicated_linked_tools  = ::Nhs::SyndicatedContentLinkTool.where(linked_tool_object_id:linked_tool_object_id,linked_tool_class:linked_tool_class)
    render :layout=>false
  end

  def get_system_jab_detail
    @class_name = SystemJab
    @record = @class_name.where(id:params[:system_jab_id]).last.get_jab_detail
    country_column = get_country_column_for_mapped_table(@class_name)
    @country_code = @record[country_column] || params[:country_code]
    @data = [@record]
    @show_only = true
    @fields = get_record_fields(@record)
    next_previous_system_record(@record,@class_name,country_column)
    render :action=>"show_system_record", layout:false
  end


  def index
    # validate_user_access("Syste")
    @system_templates = System::Template.all
  end
  
  def get_template_class
    @system_template = System::Template.find(params[:template_id]) if params[:template_id].present?
     
    if params[:class_name].present?
      @class_name = eval(params[:class_name])
    elsif params[:template_id].present?
      @class_name = eval(@system_template.mapped_table) 
    end

  end

  def new_template
    @system_template = System::Template.new
  end

  def create
    @system_template = System::Template.new(params[:system_template])

    if @system_template.save
      redirect_to "/admin/system_template"
    else
      render :new_system_template
    end
  end


  def edit
    @system_template = System::Template.find(params[:id])
  end
  

  def show
    @system_template = System::Template.find(params[:id])
    @class_name = eval(@system_template.mapped_table)
    column_name = get_country_column_for_mapped_table(@class_name)
    @country_data = @class_name.distinct(column_name.to_sym).reject{|a| a.blank?} rescue  [["all",""] ]
    params[:country_code] = "UK" if (params[:country_code].downcase == "gb" rescue false)
    @country_data = ((@country_data - ["GB", "UK", "gb","uk"]) +  ["UK"]) if  @country_data.include?("UK") || @country_data.include?("uk") || @country_data.include?("GB") ||@country_data.include?("gb")
    @country_data << ["all",''] if @country_data.blank?
  end


  def get_country_column_for_mapped_table(table_name)
    System::Template.get_country_column_for_mapped_table(table_name)
  end
  
  def new_system_record
    # @system_template = System::Template.find(params[:template_id])
    # class_name = eval(@system_template.mapped_table)
    @country_code = params[:country_code]
    @record = @class_name.new
    @redirect_to_url = get_redirect_url(@class_name)

    @parent_mapped_id = params[:parent_mapped_id]
  end

  def create_system_record
    @record,status = @class_name.create_record(params)
     
    if status
      redirect_to_url = @record.class.to_s
      # redirect_to_url,record_id = get_redirect_url(@class_name)
      redirect_to "/admin/system_template/show_system_record?country_code=#{params[:country_code]}&class_name=#{redirect_to_url}&template_id=#{@system_template.id}&record_id=#{@record.id}"
    
      # redirect_to "/admin/system_template/show_system_record?template_id=#{@system_template}&record_id=#{@record.id}&class_name=#{@record.class.to_s}" 
    else
      render :new_system_record and return
    end
  end

  def duplicate_system_record

    @record,@parent_record = @class_name.duplicate_record(params)
    @fields = get_record_fields(@record)
    @country_code = params[:country_code]
    layout false
  end
  
  def clone_form_for_country
    country_column = get_country_column_for_mapped_table(@class_name)
      
    @country_data = @class_name.distinct(country_column.to_sym).reject{|a| a.blank?} rescue  []
    @available_country_list = (@country_data.map(&:upcase) - ["GB", "UK"]) + ["UK"]
    @available_country_for_clone = ISO3166::Country.all_names_with_codes.reject{|a| @available_country_list.include?(a[1])}
    render :layout=>false
  end

  def clone_complete_data_for_country
    @status, @msg = @class_name.duplicate_all_records(params)
    layout false
  end

  def show_system_record
    @record = @class_name.find(params[:record_id]) rescue @class_name.new
     

    country_column = get_country_column_for_mapped_table(@class_name)
    @country_code = @record[country_column] || params[:country_code]
    @data = [@record]
    @fields = get_record_fields(@record)
    next_previous_system_record(@record,@class_name,country_column)
  end
  
  def next_previous_system_record(current_record_id,class_name,country_column=nil)
    @class_name = eval(class_name.to_s)
    current_record = @class_name.find(current_record_id)
    country_column = country_column || get_country_column_for_mapped_table(@class_name)
    if country_column.present?
      all_data = @class_name.where(member_id:nil,country_column.to_sym=>current_record[country_column]).order_by("id asc").pluck(:id)
    else
      all_data = @class_name.where(member_id:nil).order_by("id asc").pluck(:id)
    end
    current_record_index =   all_data.find_index(current_record.id)
    @next_record_id = all_data.at(current_record_index+1) rescue nil
    @previous_record_id = all_data.at(current_record_index-1) rescue nil
    # @next_record = @class_name.find(next_record_id) rescue nil
    # @previous_record = @class_name.find(previous_record_id) rescue nil
    # @data = @data.where(country_column.to_sym =>params[:country_code])
  end

  def get_record_fields(record)
    fields = record.attribute_names
    fields.delete("_id")
    fields.delete("member_id")
    fields.delete("updated_at")
    fields.delete("created_at")
    fields
  end

  def get_parent_mapped_id(record)
    record_class = record.class.to_s
    if record_class == "SystemJab"
      record["system_vaccin"]
    elsif record_class == "System::Child::CheckupPlanner::Test"
      record.get_system_checkup.id rescue nil
    end
  end

  def get_system_data_for_selected_country
    # @system_template = System::Template.find(params[:template_id])
    # class_name = eval(@system_template.mapped_table)
    @data = @class_name.where(member_id:nil)
    if params[:country_code].present?
      @selected_country = params[:country_code]
      country_column = get_country_column_for_mapped_table(@class_name)
      sanitize_country_code = Member.sanitize_country_code(params[:country_code])
      @data = @data.in(country_column.to_sym =>sanitize_country_code).order_by("id asc")
    else
      @data = @data.order_by("id asc")
    end
    if @data.count == 0 
      #@data = [@class_name.new]
    end
    @data = @data.entries
    @fields = @class_name.attribute_names
    @fields.delete("_id")
    if @class_name.to_s == "SystemVaccin"
      @fields.delete("route")
    end
    if @class_name.to_s == "SystemMilestone"
      @fields.delete("n2_m2")
      @fields.delete("n2_2m2")
    end
    @fields.delete("member_id")
    @fields.delete("updated_at")
    @fields.delete("created_at")
  end

  def update
    @system_template = System::Template.find(params[:id])
    if @system_template.update_attributes(params[:system_template])
      redirect_to "/admin/system_template/"
    else
      render :new_system_template
    end
  end

  def delete_template
    redirect_to "/admin/system_template" and return if @access_level < 3
    @system_template = System::Template.find(params[:id])
    @system_template.delete
  end
  
  def edit_system_record
     
    @record = @class_name.find(params[:record_id])
    country_column = get_country_column_for_mapped_table(@class_name)
    @country_code = params[:country_code] ||  @record[country_column] 
     
    @country_code = Member.member_country_code(@country_code)
    @parent_mapped_id = get_parent_mapped_id(@record) 
    @redirect_to_url = get_redirect_url(@class_name)
    
    render :action=>:new_system_record
  end

  def update_system_record
    @record = @class_name.find(params[:record_id])

    @record,status = @record.update_record(params)
    if status
      redirect_to_url = get_redirect_url(@class_name)
      redirect_to "/admin/system_template/show_system_record?country_code=#{params[:country_code]}&class_name=#{redirect_to_url}&template_id=#{@system_template.id}&record_id=#{@record.id}"
    else
      render :action=>:new_system_record
    end
  end
  
  def add_existing_checkup_test
    @record,status = @class_name.add_existing_test(params)
    if status
      redirect_to_url = @record.class.to_s
      redirect_to "/admin/system_template/show_system_record?country_code=#{params[:country_code]}&class_name=#{redirect_to_url}&template_id=#{@system_template.id}&record_id=#{@record.id}"
    else
      render :new_system_record and return
    end

  end
  
  def add_syndicated_content_action_card_category
    render :layout=>false
  end

  def save_syndicated_content_action_card_category
    if params[:title].present?
      @status = Nhs::GuideTilesCategory.new(title:params[:title]).save
    else
      @status = false
    end
  end
  
  def get_redirect_url(class_name)
    class_name = class_name.to_s
    if class_name == "SystemVaccin"
      "SystemVaccin"
    elsif class_name == "SystemJab"
      "SystemVaccin"
    end
  end
  
  def delete_system_record
    @class_name =  eval(params[:class_name])
    # @record = class_name.find(params[:record_id])
     
    @class_name.delete_record(params[:record_id])
  end
  
  def validate_user_access(module_name="SystemTemplate")
    @access_level = ModuleAccessForRole.user_access_level_for_module(current_user,module_name)
    unless (@access_level > 0) || current_user.email == AppConfig["superadmin_email"] 
     redirect_to "/"
    end
  end

end