class Admin::ScenarioDetailsController < ApplicationController
  before_filter :admin_basic_auth
  http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
  before_filter :validate_user_access
  layout 'admin'
  
  def index
    @scenarios = System::Scenario::ScenarioDetails.all
    @source = "scenario_controller"
    
  end

  def paginated_data
    if params[:id].present?
      @scenarios = System::Scenario::ScenarioDetails.where(id:params[:id])
    else
      @scenarios = System::Scenario::ScenarioDetails.datatable_filter(params['search']['value'], params['columns'])
    end
    @source = "scenario_controller"
    scenario_count = @scenarios.count
    @scenarios = @scenarios.datatable_order(params['order']['0']['column'].to_i,
                                params['order']['0']['dir'])
    if params['length'].to_i >= 0
      @scenarios = @scenarios.paginate(:page=>(params['start'].to_i/(params['length'].to_i) + 1),:per_page=>params['length'])
    end
    render :json=> {:scenario_list=>@scenarios,
                 draw: params['draw'].to_i,
                 recordsTotal: System::Scenario::ScenarioDetails.count,
                 recordsFiltered: scenario_count 
                }
  end
  
  def get_linked_items
    data = {}
    @disable_value = params[:linked_tool_custome_id].present?
    @scenario = System::Scenario::ScenarioDetails.find(params[:id])
    @linked_items = @scenario.linked_items
    # if params[:source] == "scenario_controller"
    #   @source = params[:source]
    # else
      render :layout=>false
    # end
  end

  def new_scenario
    data = {}
    @disable_value = params[:linked_tool_custome_id].present?
    @scenario = System::Scenario::ScenarioDetails.new(data)
    if params[:source] == "scenario_controller"
      @source = params[:source]
    else
      render :layout=>false
    end
  end

  def edit_scenario
    data = {}
    @disable_value = params[:linked_tool_custome_id].present?
    @scenario = System::Scenario::ScenarioDetails.find(params[:id])
    @next_scenario_custome_id = System::Scenario::ScenarioDetails.where(:custome_id.gt=>@scenario.custome_id.to_i).order_by("custome_id asc").first.custome_id rescue 0 
    @previous_scenario_custome_id =  System::Scenario::ScenarioDetails.where(:custome_id.lt=>@scenario.custome_id.to_i).order_by("custome_id desc").first.custome_id rescue 0 
    @max_custome_id = System::Scenario::ScenarioDetails.max(:custome_id)
    # @scenario.custome_id.to_i - 1
 
    if params[:source] == "scenario_controller"
      @source = params[:source]
    else
      render :layout=>false
    end
  end

  def edit_next_scenario
    if @access_level > 1
      @source  = "scenario_controller"
      @scenario = System::Scenario::ScenarioDetails.where(custome_id:params[:id]).last
      # @next_scenario_custome_id = @scenario.custome_id.to_i + 1
      @max_custome_id = System::Scenario::ScenarioDetails.max(:custome_id)
      # @previous_scenario_custome_id = @scenario.custome_id.to_i - 1
    @next_scenario_custome_id = System::Scenario::ScenarioDetails.where(:custome_id.gt=>@scenario.custome_id.to_i).order_by("custome_id asc").first.custome_id rescue 0 
    @previous_scenario_custome_id =  System::Scenario::ScenarioDetails.where(:custome_id.lt=>@scenario.custome_id.to_i).order_by("custome_id desc").first.custome_id rescue 0 

    end
  end

  def edit_previous_scenario
    if @access_level > 1
      @source  = "scenario_controller"
      @scenario = System::Scenario::ScenarioDetails.where(custome_id:params[:id]).last
      # @next_scenario_custome_id = @scenario.custome_id.to_i + 1
      @max_custome_id = System::Scenario::ScenarioDetails.max(:custome_id)
      # @previous_scenario_custome_id = @scenario.custome_id.to_i - 1
    @next_scenario_custome_id = System::Scenario::ScenarioDetails.where(:custome_id.gt=>@scenario.custome_id.to_i).order_by("custome_id asc").first.custome_id rescue 0 
    @previous_scenario_custome_id =  System::Scenario::ScenarioDetails.where(:custome_id.lt=>@scenario.custome_id.to_i).order_by("custome_id desc").first.custome_id rescue 0 

    end
  end



  def add_scenario
    params[:system_scenario_scenario_details][:keywords] = params[:keywords].split(",")
     data = params[:system_scenario_scenario_details]
    @scenario = System::Scenario::ScenarioDetails.new(data)
    @source = params[:source] || params[:source_controller]
    @next_scenario_custome_id = System::Scenario::ScenarioDetails.where(:custome_id.gt=>@scenario.custome_id.to_i).order_by("custome_id asc").first.custome_id rescue 0 
    @previous_scenario_custome_id =  System::Scenario::ScenarioDetails.where(:custome_id.lt=>@scenario.custome_id.to_i).order_by("custome_id desc").first.custome_id rescue 0 

    if @scenario.save 
      @save = true
    else
      @save = false
    end
    if  !(request.xhr? || request.format.js?) && @save
      redirect_to "/admin/scenario_details/#{@scenario.id}" and return true
    else
      respond_to do |format|
        format.js { render layout: false }
        format.html  
      end
    end 
  end

  def clone_scenario
    if @access_level > 1
      @source  = "scenario_controller"
      scenario_obj = System::Scenario::ScenarioDetails.find(params[:id])
      scenario = scenario_obj.attributes
      scenario.delete("_id")
      custome_id = System::Scenario::ScenarioDetails.last.custome_id + 1 
      @cloned_scenario = System::Scenario::ScenarioDetails.new(scenario) 
      @cloned_scenario.custome_id =  custome_id
      @cloned_scenario.status = "Draft"
      @cloned_scenario.save
    end
  end

  #on click next link , It will render next pointer in list
  def go_to_next_scenario
    @source  = "scenario_controller"
    @scenario = System::Scenario::ScenarioDetails.where(custome_id:params[:id]).last
    @max_custome_id = System::Scenario::ScenarioDetails.max(:custome_id)
    next_scenario = System::Scenario::ScenarioDetails.where(:custome_id.gt=>@scenario.custome_id.to_i).order_by("custome_id asc").first
    @next_scenario_custome_id = next_scenario.custome_id rescue 0 
    @previous_scenario_custome_id =  System::Scenario::ScenarioDetails.where(:custome_id.lt=>@scenario.custome_id.to_i).order_by("custome_id desc").first.custome_id rescue 0 
    params[:id] = @scenario.id rescue @scenario.id

  end

 #on click next link , It will render next pointer in list
  def go_to_previous_scenario
    @source  = "scenario_controller"
    @scenario = System::Scenario::ScenarioDetails.where(custome_id:params[:id]).last
    @max_custome_id = System::Scenario::ScenarioDetails.max(:custome_id)
    @next_scenario_custome_id = System::Scenario::ScenarioDetails.where(:custome_id.gt=>@scenario.custome_id.to_i).order_by("custome_id asc").first.custome_id rescue 0 
    previous_scenario =  System::Scenario::ScenarioDetails.where(:custome_id.lt=>@scenario.custome_id.to_i).order_by("custome_id desc").first
    @previous_scenario_custome_id = previous_scenario.custome_id rescue 0 
    params[:id] = @scenario.id# rescue @scenario.id
  end
   
  # vaidate user access  with role and access level for pointer Module
  def validate_user_access(module_name="PointersMaintenance")
    @access_level = ModuleAccessForRole.user_access_level_for_module(current_user,module_name)
    unless (@access_level > 0) || current_user.email == AppConfig["superadmin_email"] 
     redirect_to "/"
    end
  end

  def update_scenario
    begin
      @scenario = System::Scenario::ScenarioDetails.find(params[:scenario_id])
      params[:system_scenario_scenario_details][:keywords] = params[:keywords].split(",")
      
      data = params[:system_scenario_scenario_details]
      @save =  @scenario.update_attributes(data)
    next_scenario_custome_id = System::Scenario::ScenarioDetails.where(:custome_id.gt=>@scenario.custome_id.to_i).order_by("custome_id asc").first.custome_id rescue 0 
     

      next_scenario_custome_id = @scenario.custome_id + 1
      next_scenario = System::Scenario::ScenarioDetails.where(custome_id:next_scenario_custome_id).last
      
    rescue Exception => e 
      Rails.logger.info "error inside update_scenario-#{e.message}"
      @save = false
    end
    if  !(request.xhr? || request.format.js?) && @save
    @source = "scenario_controller"
    if params[:update_and_edit_next].present? && next_scenario.present?
        redirect_to "/admin/scenario_details/edit_scenario?id=#{next_scenario.id}&source=scenario_controller" and return
      else
        redirect_to "/admin/scenario_details/#{@scenario.id}" and return true
      end
    else
      respond_to do |format|
        format.js { render layout: false }
        format.html  
      end
    end 
  end

  def delete_scenario
    @scenario = System::Scenario::ScenarioDetails.find(params[:scenario_id])
    @scenario.delete
  end

  def show
    if params[:linked_tool_id].present?
      @scenarios_link_tool  = ::System::Scenario::ScenarioLinkedToTool.where(tool_class:params[:tool_class],linked_tool_id:params[:linked_tool_id]).last rescue nil
      @scenarios = System::Scenario::ScenarioDetails.where(custome_id: @scenarios_link_tool.system_scenario_detail_custom_id)
      params[:id] =  @scenarios.last.id.to_s
    else
      @scenarios = System::Scenario::ScenarioDetails.where(id:params[:id])
    end
    @source = "scenario_controller"
    @scenario = @scenarios.first
    @source = params[:source] || params[:source_controller] || "scenario_controller"
    # @next_scenario_custome_id = @scenario.custome_id.to_i + 1
    @max_custome_id = System::Scenario::ScenarioDetails.max(:custome_id).to_i
    # @previous_scenario_custome_id = @scenario.custome_id.to_i - 1
 
    @next_scenario_custome_id = System::Scenario::ScenarioDetails.where(:custome_id.gt=>@scenario.custome_id.to_i).order_by("custome_id asc").first.custome_id rescue 0
    @previous_scenario_custome_id =  System::Scenario::ScenarioDetails.where(:custome_id.lt=>@scenario.custome_id.to_i).order_by("custome_id desc").first.custome_id rescue 0 

    if @source != "scenario_controller"
 
      render :action=> :index,:layout=>false
    else
      render :action=> :index
    end
  end
  def test_notification_validation_to_assign
    options = {}
    scenario_detail = System::Scenario::ScenarioDetails.where(:custome_id=>params[:custom_id]).last
    member = Member.where(id:params[:member_id]).last
    pregnancy = Pregnancy.where(expected_member_id:member.id).last
    country_code = member.get_country_code rescue  "GB"
    options[:country_code] = country_code
    scenario_category = System::Scenario::ScenarioDetails.get_scenario_category(scenario_detail,options)
     
    @scenario_data = System::Scenario::ScenarioDetails.get_data_after_validate(scenario_detail,scenario_category,member,country_code,{:linked_tool=>nil,:pregnancy=>pregnancy,:linked_tool_topic=>""})
  end

  private
  def validate_user_access(module_name="ScenarioDetails")
    @access_level = ModuleAccessForRole.user_access_level_for_module(current_user,module_name)
    unless (@access_level > 0) || current_user.email == AppConfig["superadmin_email"] 
     redirect_to "/"
    end
  end 

end