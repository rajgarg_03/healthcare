class Admin::KpiController < ApplicationController
  before_filter :admin_basic_auth
  http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
  
  before_filter :validate_user_access
  
  def kpi_data_on_date
    date = (params[:start_date]||Date.today).to_date
    @data = ::Report::KpiData.data_on(date).with_indifferent_access
   
  end

  private
  def validate_user_access(module_name="KpiData")

    @access_level = ModuleAccessForRole.user_access_level_for_module(current_user,module_name)
    unless (@access_level > 0) || current_user.email == AppConfig["superadmin_email"] 
     redirect_to "/"
    end
  end 
    
end