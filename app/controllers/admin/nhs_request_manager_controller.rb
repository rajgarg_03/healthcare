class Admin::NhsRequestManagerController < ApplicationController
before_filter :admin_basic_auth
http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
before_filter :validate_user_access
layout 'admin'

  def index 
  end

  def nhs_news_request
  end

  def nhs_search_request
  end


  def show_saved_articles
    @data = Nhs::NhsData.where(:search_type=> "article")
  end

  def show_saved_media
    @data = Nhs::NhsData.where(:search_type=> "media")
  end

  def nhs_search_request_output
    query = params[:q]
    begin
      @data = Nhs::NhsApi.get_search_data(query)
    rescue Exception=>e
      Rails.logger.info e.message
    end
  end 

  def nhs_news_request_output
    @start_date = params[:start_date] == "select" ? (Date.today-7).to_s : params[:start_date]
    @end_date = params[:end_date] == "select" ? Date.today.to_s : params[:end_date]

    @order = params[:order] 
    @page = params[:page]
    @keywords_string = params[:keywords]

    keywords = Utility.get_words_array(@keywords_string)
    
    if params[:commit] == "Save News content" 
      begin
        @message = "Content Saved for below response."
        search_type = "article"
        @data = Nhs::NhsContentManager.format_and_save_nhs_article_data(@start_date,@end_date,@order,@page,keywords,search_type,true)
      rescue Exception=>e
        Rails.logger.info e.message
      end
    
    else

      begin
        @message = ""
        # @data = JSON.pretty_generate(Nhs::NhsApi.get_news_data(@start_date,@end_date,@page,@order))
        @data = Nhs::NhsContentManager.format_and_save_nhs_article_data(@start_date,@end_date,@order,@page,keywords,search_type)
      rescue Exception=>e
        Rails.logger.info e.message
      end
    end
  end

  
  def get_data
    @start_date = params[:start_date] == "select" ? (Date.today-7).to_s : params[:start_date]
    @end_date = params[:end_date] == "select" ? Date.today.to_s : params[:end_date]

    @page = params[:page_value]
    @keywords_string = params[:term]

    term = Utility.get_words_array(@keywords_string)
    
    if params[:commit] == "Save" 
      begin
        @message = "Content Saved for below response."
        search_type = "media"
        @data = Nhs::NhsContentManager.format_and_save_media_data(@start_date,@end_date,@page,term,search_type,true)
      rescue Exception=>e
        Rails.logger.info e.message
      end
    
    else
      begin
        @message = ""
        @data = Nhs::NhsContentManager.format_and_save_media_data(@start_date,@end_date,@page,term,search_type)
        
      rescue Exception=>e
        Rails.logger.info e.message
      end
    end
  end

  def show_organisation_data
    @organisation_type = params[:organisation_type]? params[:organisation_type]: "GP"
    @limit = params[:limit]? params[:limit]: "25"
    @offset = params[:offset]? params[:offset] : 0
    @service_code = params[:service_code] ? params[:service_code] : ""
    @ods_code = params[:ods_code] ? params[:ods_code] : ""
    @select_param = params[:select_param] ? params[:select_param] : ""
    @order_by = params[:order_by] ? params[:order_by] : ""
    @order = params[:order] ? params[:order_by] : "asc"
    @lat = params[:lat] ? params[:lat] : ""
    @long = params[:long] ? params[:long] : ""
  
    begin
      @message = ""
      @data = Nhs::NhsApi.get_organisations(@organisation_type, @limit, @offset, @service_code, @ods_code, @select_param, @order_by, @order, @lat, @long)    
    rescue Exception=>e
      Rails.logger.info e.message
    end
  end

  private
  def validate_user_access(module_name="NHSRequestManager")
    @access_level = ModuleAccessForRole.user_access_level_for_module(current_user,module_name)
    unless (@access_level > 0) || current_user.email == AppConfig["superadmin_email"] 
     redirect_to "/"
    end
  end

  def get_organisation_data
    
  end
  
end
