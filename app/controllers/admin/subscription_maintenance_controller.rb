class Admin::SubscriptionMaintenanceController < ApplicationController
  # skip_before_filter :authenticate_user!
  before_filter :admin_basic_auth
  http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
  
  before_filter :validate_user_access, :except=>["subscription_report","subscription_history_paginated_data"]
  layout 'admin'

  def index
  	@subscriptions = SubscriptionManager.all || []
  end

  def new_subscription
    redirect_to "/admin/subscription_maintenance" and return if @access_level < 2
  	@subscription = SubscriptionManager.new
  end
  
  def subscription_history_paginated_data

    # byebug
    current_user = Member.find(params[:id])
    options = {:order=>params['order']}

    @subscription_data = ::Subscription::History.get_data(current_user,options)

    subscription_data_count = @subscription_data.count
    # @emis_logs = @emis_logs.datatable_order(params['order']['0']['column'].to_i,
                                # params['order']['0']['dir'])

    # @subscription_data = @subscription_data.order_by("id desc")
    if params['length'].to_i >= 0
      @subscription_data = @subscription_data.paginate(:page=>(params['start'].to_i/(params['length'].to_i) + 1),:per_page=>params['length'])
    end
    render :json=> {:subscription_data=>@subscription_data,
         draw: params['draw'].to_i,
         recordsTotal: subscription_data_count,
         recordsFiltered: subscription_data_count 
        }
  end

   

  def create
     
    redirect_to "/admin/subscription_maintenance" and return if @access_level < 2
    params[:subscription_manager][:grace_period] = params["grace_period"] + "." + params["grace_frequency"]
    params[:subscription_manager][:android_supported_app_version_str] = params[:subscription_manager][:android_supported_app_version_str].upcase.strip
    params[:subscription_manager][:ios_supported_app_version_str] = params[:subscription_manager][:ios_supported_app_version_str].upcase.strip
    params[:subscription_manager][:android_supported_app_version] = Nutrient.convert_device_app_version_number(params[:subscription_manager][:android_supported_app_version_str],"android")
    params[:subscription_manager][:ios_supported_app_version] = Nutrient.convert_device_app_version_number(params[:subscription_manager][:ios_supported_app_version_str],"ios")

    params[:subscription_manager][:plan_info] = eval(params[:subscription_manager][:plan_info])
    params[:subscription_manager]["trial_duration"] = params["free_trial_period"] + "." + params["free_trial_frequency"]
    # params[:subscription_manager][:subscription_listing_order] = Subscription::Level["free"] if params[:subscription_manager][:category].downcase == "free"
    @subscription = SubscriptionManager.new(params[:subscription_manager])

    if @subscription.save
      redirect_to "/admin/subscription_maintenance"
    else
    	render :new_subscription
    end
  end

  def edit
    redirect_to "/admin/subscription_maintenance" and return if @access_level < 2
  	@subscription = SubscriptionManager.find(params[:id])
  end

  def update
      
    params[:subscription_manager][:trial_duration] = params["free_trial_period"] + "." + params["free_trial_frequency"]
    params[:subscription_manager][:grace_period] = params["grace_period"] + "." + params["grace_frequency"]
    params[:subscription_manager][:plan_info] = eval(params[:subscription_manager][:plan_info])

    params[:subscription_manager][:android_supported_app_version_str] = params[:subscription_manager][:android_supported_app_version_str].upcase.strip
    params[:subscription_manager][:ios_supported_app_version_str] = params[:subscription_manager][:ios_supported_app_version_str].upcase.strip
    params[:subscription_manager][:android_supported_app_version] = Nutrient.convert_device_app_version_number(params[:subscription_manager][:android_supported_app_version_str],"android")
    params[:subscription_manager][:ios_supported_app_version] = Nutrient.convert_device_app_version_number(params[:subscription_manager][:ios_supported_app_version_str],"ios")
        
    #redirect_to "/admin/subscription_maintenance" and return if @access_level < 2
    @subscription = SubscriptionManager.find(params[:id])
    # params[:subscription_manager][:subscription_listing_order] = Subscription::Level["free"] if params[:subscription_manager][:category].present? && params[:subscription_manager][:category].downcase == "free"
  	if @subscription.update_attributes(params[:subscription_manager])
      redirect_to "/admin/subscription_maintenance/"
  	else
  		render :edit
  	end
  end

  def delete_subscription
    redirect_to "/admin/subscription_maintenance" and return if @access_level < 3
  	@subscription = SubscriptionManager.find(params[:id])
  	@subscription.delete
  end


  def subscription_report
    validate_user_access("SubscriptionReport")
    premium_subscription_manager_list = SubscriptionManager.premium_subscriptions.valid_subscription_manager(current_user,api_version=4).pluck(:id).map(&:to_s)
    @subscriptions ,params_data = filter_user_subscription #
    if params_data.blank?
      @subscriptions = Subscription.in(subscription_manager_id:premium_subscription_manager_list)
    end
    @subscriptions = @subscriptions.order_by("updated_at desc").paginate(:page => params[:page]||1, :per_page => 500)

  end
  

  # def paginated_data
  #   premium_subscription_manager_list = SubscriptionManager.premium_subscriptions.valid_subscription_manager(current_user,api_version=4).pluck(:id).map(&:to_s)
  #   @subscriptions ,params_data = filter_user_subscription #
    
  #   if params_data.blank?
  #     @subscriptions = Subscription.in(subscription_manager_id:premium_subscription_manager_list)
  #   end
  #   subscription_count = @subscriptions.count
    
  #   if params['length'].to_i >= 0
  #     @subscriptions = @subscriptions.paginate(:page=>(params['start'].to_i/(params['length'].to_i) + 1),:per_page=>params['length'])
  #   end
  #   render :json=> {:subscription_report=>@subscriptions,
  #                draw: params['draw'].to_i,
  #                recordsTotal: Ssubscription_count,
  #                recordsFiltered: @subscriptions.count 
  #               }
  # end

  def filter_user_subscription
    params[:email] = nil if params[:email] == "Email"
    params[:country_code] =  params[:country_code].reject{|a| a.blank?} rescue nil
    params[:platform] = params[:platform].reject{|a| a.blank?} rescue nil
    params_country_code = params[:country_code]
    params[:subscription_manager_id] = params[:subscription_manager_id].reject{|a| a.blank?} rescue nil
    if params[:country_code].blank?
      params[:country_code] = nil  
    else
      params[:country_code] = params[:country_code].map{|code| eval(code)}.flatten
    end
    data = {:subscription_manager_id=>params[:subscription_manager_id],:platform=>params[:platform],:email=>params[:email],:country_code=>params[:country_code],
      :expiry_date=>{:expiry_start_date=>params[:expiry_start_date],:expiry_end_date=>params[:expiry_end_date]},:start_date=>{:purchase_end_date=>params[:purchase_end_date],:purchase_start_date=>params[:purchase_start_date]}}

    if params[:start_start_date].blank? 
      data[:start_date] = nil  
    end
    

    if params[:expiry_start_date].blank?
      data[:expiry_date] = nil  
    end
     
    data = data.reject{|k,v| v.blank?}
    if params[:page].blank?
      session[:data] = data 
    else
      data = session[:data]
    end
    @subscriptions = Subscription.where(nil)
    if data.present?
      data.each do |k,v|
        field_name = k
     

        if k.to_s == "country_code" || k.to_s == "email"
          field_name = :member_id
          if data[:country_code].present? && data[:email].present?
            val = User.where(:country_code.in=>data[:country_code]).where(:email=>/#{data[:email]}/i).pluck(:member_id)
          elsif  data[:country_code].present? && data[:email].blank?
            val = User.where(:country_code.in=>data[:country_code]).pluck(:member_id)
          else
            val = User.where(:email=>/#{data[:email]}/i).pluck(:member_id)
          end
           
          @subscriptions = @subscriptions.where(field_name.in=>val)
         
          
        elsif k.to_s == "expiry_date"
          @subscriptions = @subscriptions.where(field_name=>{"$gte"=>v[:expiry_start_date],"$lte"=>(v[:expiry_end_date]||v[:expiry_start_date])})
         elsif k.to_s == "start_date"
          @subscriptions = @subscriptions.where(field_name=>{"$gte"=>v[:purchase_start_date],"$lte"=>(v[:purchase_end_date]||v[:purchase_start_date])})
       
        else
          optRegexp = []
          [v].flatten.each do |opt|
            if k == :email
              optRegexp.push(/#{opt}/i) 
            else
              optRegexp.push(opt) 
            end
          end
          val = optRegexp
          @subscriptions = @subscriptions.where(field_name.in=>val)
        end
      end 
    end
    @subscriptions = @subscriptions
     
    params[:country_code] = params_country_code
    [@subscriptions,data]
  end

  def get_users_list_checking_subcription_list
    validate_user_access("SubscriptionReport")
    @log_data = Report::ApiLog.get_user_for_api_action({:action=>"verify_subscription"})
    render :layout=>false
  end
  private
  def validate_user_access(module_name="SubscriptionMaintenance")
    @access_level = ModuleAccessForRole.user_access_level_for_module(current_user,module_name)
    unless (@access_level > 0) || current_user.email == AppConfig["superadmin_email"] 
     redirect_to "/"
    end
  end 
  
end