class Admin::ManageNhsAccountController  < ApplicationController
  before_filter :admin_basic_auth
  http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
  before_filter :validate_user_access
  layout 'admin'

  def index
    @member_ids = User.where(provider:"nhs").pluck(:member_id)
    @nhs_account_count = Member.or({:id.in=>@member_ids},{:clinic_link_state.gt=>2}).count
    @nhs_accounts = Member.or({:id.in=>@member_ids},{:clinic_link_state.gt=>2}).order_by("updated_at desc").limit(300)#.includes(:user) 
    if @nhs_account_count > 300
      @message = "Showing latest 300 record details."
    else
      @message = "Showing #{@nhs_account_count} record details."
    end
  end
  
  def get_filter_data
    options = params.deep_dup
    options[:return_member_id] = true
    user_filter_status, member_ids = ::Report::ApiLog.get_user_selected(options)
    clinic_link_detail = Clinic::LinkDetail.where(nil)
    if params[:start_date].present?
      clinic_link_detail = clinic_link_detail.where(:created_at=>{"$gte"=>params[:start_date].to_date})
    end
    if params[:end_date].present?
      clinic_link_detail = clinic_link_detail.where(:created_at=>{"$lte"=>params[:end_date].to_date})
    end

    if params[:end_date].present? ||  params[:start_date].present?

      if user_filter_status
        member_ids = member_ids & clinic_link_detail.pluck(:member_id)
      else
        member_ids = clinic_link_detail.pluck(:member_id)
      end
      user_filter_status = true
    end
 
    @member_ids = User.where(provider:"nhs")
    if user_filter_status
      @member_ids = @member_ids.where(:member_id.in=>member_ids)
      @nhs_accounts = Member.where(:id.in=>member_ids)
    else
      @nhs_accounts = Member.where(nil)
    end
  
    @member_ids = @member_ids.only(:member_id).pluck(:member_id)
    if params[:clinic_linked] == "Linked"
       @nhs_accounts =  @nhs_accounts.where(:clinic_link_state.gt=>2)#.includes(:user) 
    elsif params[:clinic_linked] == "Not Linked"
       @nhs_accounts =  @nhs_accounts.where(:clinic_link_state.lte=>2)#.includes(:user) 
    end
    @nhs_account_count = @nhs_accounts.count
    @message = "Filter result data."

    render :action=>:index
  end

  def update_clinic_account_status
    @member = Member.find(params[:id])
    @last_status = @member.clinic_account_status
    options = {:ip_address=>request.remote_ip}
    @action_text = "Block"
    if @member.clinic_account_status == "Active" ||  @member.clinic_account_status.blank?
      @member.clinic_account_status = "Blocked"
       @action_text = "Active"
    else
      @member.clinic_account_status = "Active"
      @action_text = "Block"
    end
    @member.save(validate:false)
    @member.reload
    respond_to do |format|
      format.js
    end
  end

private
  def validate_user_access(module_name="NhsLogin/GPLinked AccountManager")
    @access_level = ModuleAccessForRole.user_access_level_for_module(current_user,module_name)
    unless (@access_level > 0) || current_user.email == AppConfig["superadmin_email"] 
     redirect_to "/"
    end
  end
  
end