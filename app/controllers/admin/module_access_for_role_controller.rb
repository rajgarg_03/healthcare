class Admin::ModuleAccessForRoleController < ApplicationController
  # skip_before_filter :authenticate_user!
  before_filter :admin_basic_auth
  http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
  
  before_filter :validate_user_access

  layout 'admin'

  def index
  	@module_access_list_for_role = ModuleAccessForRole.all || []
  end

  def new_module_access
  	@module_access_for_role = ModuleAccessForRole.new
  end

  def create
    begin
      @module_access_for_role = ModuleAccessForRole.new(params[:module_access_for_role])
       
      if @module_access_for_role.save
        redirect_to "/admin/module_access_for_role"
      else
      	render :new_module_access
      end
    rescue Exception=>e
      render :new_module_access
    end
  end

  def edit
  	@module_access_for_role = ModuleAccessForRole.find(params[:id])

  end

  def update
    begin
  	@module_access_for_role = ModuleAccessForRole.find(params[:id])
    
  	if @module_access_for_role.update_attributes(params[:module_access_for_role])
      redirect_to "/admin/module_access_for_role"
  	else
  		render :edit
  	end
  rescue
      render :edit
  end
  end

  def delete_module_access
       
  	@module_access_for_role = ModuleAccessForRole.find(params[:id])
  	@module_access_for_role.delete
  end

  private
  def validate_user_access(module_name="ModuleAccessMaintenance")
    @access_level = ModuleAccessForRole.user_access_level_for_module(current_user,module_name)
    unless (@access_level > 0) || current_user.email == AppConfig["superadmin_email"] 
     redirect_to "/"
    end
  end

end
