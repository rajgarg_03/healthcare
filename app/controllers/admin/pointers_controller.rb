class Admin::PointersController < ApplicationController
  # skip_before_filter :authenticate_user!
  before_filter :admin_basic_auth
  http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
  before_filter :validate_user_access, :except=>["autocomplete_system_pointer_custom_id", "autocomplete_system_scenario_scenario_details_title"]
  layout 'admin'
  autocomplete :system_scenario_scenario_details, :title  
  autocomplete :system_pointer, :custom_id  

  def index
  	@sys_pointers = SysArticleRef.all#.paginate(:page => params[:page], :per_page => 20)
    @system_pointer_assigned_count = Report::PointerAssignedData.get_assigned_pointers_data # 30 days data# ArticleRef.assigned_pointer_group_by_system_pointer_id
    @pointer_links = ArticleRef.pointer_links_group_by_system_pointer_custom_id
    @next_level_mapping_text = {"DevReview"=>"Activate","Active"=>"Deactivate"}
    
  end
  
  def validate_all_pointer_links
    current_user.delay.validate_all_pointer_links
  end

  def show
    @member_list =  current_user.childerns 
    @next_level_mapping_text = {"DevReview"=>"Activate","Active"=>"Deactivate"}
    if params[:show_only] == "true"
      @system_pointer = SysArticleRef.where(custome_id:params[:custom_id].to_i).last
      @system_pointer_assigned_count = ArticleRef.assigned_pointer_group_by_system_pointer_id(@system_pointer.id)
      render layout:false
    else
      @system_pointer = SysArticleRef.find(params[:id])
      @system_pointer_assigned_count = ArticleRef.assigned_pointer_group_by_system_pointer_id(@system_pointer.id)
     

      @system_pointer_links = SysArticleRefLink.where(custome_id:@system_pointer.custome_id)
      @next_pointer_custome_id = @system_pointer.custome_id.to_i + 1
      @max_custome_id = SysArticleRef.max(:custome_id)
      @previous_pointer_custome_id = @system_pointer.custome_id.to_i - 1
    end 
    
  end

  def autocomplete_system_scenario_scenario_details_title
    validate_user_access("ScenarioDetails")
    term = params[:term]
    if term.blank?
      scenarios = System::Scenario::ScenarioDetails.all.order_by("custome_id asc")
    else
      scenarios = System::Scenario::ScenarioDetails.or({:title=>/#{term}/i},{:custome_id=>term}).order_by("custome_id asc")
    end
    render :json => scenarios.map { |scenario| {:id => scenario.custome_id, :label => "ID: #{scenario.custome_id} , Title: #{scenario.title}", :value => "ID: #{scenario.custome_id} , Title: #{scenario.title}"} }

  end
  def autocomplete_system_pointer_custom_id
    term = params[:term]
    if term.blank?
      system_pointers = SysArticleRef.all.order_by("custome_id asc")
    else
      system_pointers = SysArticleRef.or({:topic=>/#{term}/i},{:custome_id=>term}).order_by("custome_id asc")
    end
    render :json => system_pointers.map { |system_pointer| {:id => system_pointer.custome_id, :label => "ID: #{system_pointer.custome_id} , Title: #{system_pointer.topic}", :value => "ID: #{system_pointer.custome_id} , Title: #{system_pointer.topic}"} }

  end

  def new_pointer
    if @access_level > 1
      @system_pointer = SysArticleRef.new
    else
      redirect_to "/admin/pointers"
    end
  end

  def get_link_details
    @link_details = Utilities::LinkDetails.fetch_link_content(params[:link])
  end

  def create_pointer
    if @access_level > 1
      params[:sys_article_ref][:status] = "Draft" if @access_level < 3
      params[:sys_article_ref][:description] = params[:sys_article_ref][:topic]
      @system_pointer = SysArticleRef.new(params[:sys_article_ref])

      @system_pointer.custome_id = (SysArticleRef.last.custome_id).to_i + 1
      if @system_pointer.save
         @system_pointer.update_scenario(params[:sys_article_ref][:system_scenario_detail_custom_id])

        redirect_to "/admin/pointers/#{@system_pointer.id}"
      else
        render :new_pointer
      end
    else
      redirect_to "/admin/pointers"
    end
  end

  def edit_pointer
    if @access_level > 1
      @system_pointer = SysArticleRef.find(params[:id])
      @next_pointer_custome_id = @system_pointer.custome_id.to_i + 1
      @max_custome_id = SysArticleRef.max(:custome_id)
      @previous_pointer_custome_id = @system_pointer.custome_id.to_i - 1
    else
      redirect_to "/admin/pointers"
    end
  end
  def update_pointer_status
     
    if @access_level > 1
      next_level_mapping = {"DevReview"=>"Active","Active"=>"Deactive"}
      next_level_mapping_text = {"DevReview"=>"Activate","Active"=>"Deactivate"}
      @system_pointer = SysArticleRef.find(params[:id])
      status = next_level_mapping[@system_pointer.status]
      if status.present?
        @system_pointer.status = status
        @pointer_next_status = next_level_mapping_text[status]
        @system_pointer.save
      end
    else
      redirect_to "/admin/pointers"
    end
  end

  def edit_next_pointer
    if @access_level > 1
      @system_pointer = SysArticleRef.where(custome_id:params[:id]).last
      @next_pointer_custome_id = @system_pointer.custome_id.to_i + 1
      @max_custome_id = SysArticleRef.max(:custome_id)
      @previous_pointer_custome_id = @system_pointer.custome_id.to_i - 1
    end
  end

  def edit_previous_pointer
    if @access_level > 1
      @system_pointer = SysArticleRef.where(custome_id:params[:id]).last
      @next_pointer_custome_id = @system_pointer.custome_id.to_i + 1
      @max_custome_id = SysArticleRef.max(:custome_id)
      @previous_pointer_custome_id = @system_pointer.custome_id.to_i - 1
    end
  end


  def update_pointer
    if @access_level > 1
      system_pointer = SysArticleRef.find(params[:sys_article_ref][:id])
      params[:sys_article_ref][:description] = params[:sys_article_ref][:topic]  
      params[:sys_article_ref][:name]  = params[:sys_article_ref][:scenarios] if params[:sys_article_ref][:name].blank? 
      # params[:sys_article_ref][:status] = "Draft" if @access_level < 3
      params[:sys_article_ref][:status] = "Draft" if (@access_level < 3 && !System::Settings.pointer_access_to_update?(current_user))
       
      system_pointer.update_attributes(params[:sys_article_ref])
      system_pointer.update_scenario(params[:sys_article_ref][:system_scenario_detail_custome_id])

      next_pointer_custome_id = system_pointer.custome_id + 1
      next_system_pointer = SysArticleRef.where(custome_id:next_pointer_custome_id).last
      if params[:update_and_edit_next].present? && next_system_pointer.present?
        redirect_to "/admin/pointers/edit_pointer?id=#{next_system_pointer.id}"
      else
        redirect_to "/admin/pointers/#{system_pointer.id}"
      end
    else
      redirect_to "/admin/pointers"
    end
  end
  
  def delete_pointer
    if @access_level > 2 || System::Settings.pointer_access_to_update?(current_user)
      @sys_pointer = SysArticleRef.where(custome_id:params[:custome_id].to_i).last
      SysArticleRefLink.where(custome_id:@sys_pointer.custome_id).delete_all
      ArticleRef.where(custome_id:@sys_pointer.custome_id).delete_all
      @sys_pointer.delete
    else
      
    end
  end

  def all_pointer_links
    @system_pointer_links = SysArticleRefLink.all
  end

  def clone_pointer
    if @access_level > 1
      sys_pointer_obj = SysArticleRef.find(params[:id])
      sys_pointer = sys_pointer_obj.attributes
      sys_pointer_ref_links = SysArticleRefLink.where(custome_id:sys_pointer_obj.custome_id)
      sys_pointer.delete("_id")
      custome_id = SysArticleRef.last.custome_id + 1 
      @cloned_sys_pointer = SysArticleRef.new(sys_pointer) 
      @cloned_sys_pointer.custome_id =  custome_id
      @cloned_sys_pointer.status = "Draft"
      @cloned_sys_pointer.save
      sys_pointer_ref_links.each do |ref_link|
        ref_link_attr = ref_link.attributes
        ref_link_attr.delete("_id")
        cloned_ref_links = SysArticleRefLink.new(ref_link_attr)
        cloned_ref_links.custome_id = custome_id
        cloned_ref_links.status = "Draft"
        cloned_ref_links.save
      end
    else
      #redirect_to "/admin/pointers"
    end
  end

  def new_pointer_link
    if @access_level > 1
      @system_pointer = SysArticleRef.where(custome_id:params[:custome_id]).last
      @system_pointer_link = SysArticleRefLink.new(custome_id:params[:custome_id])
      @counteries = (SysArticleRefLink.distinct(:country).sort - ["other"]) + ["other"]
    else
      redirect_to "/admin/pointers"
    end
  end

  def validate_pointer_link
    if params[:pointer_id].present?
      custom_id = SysArticleRef.find(params[:pointer_id]).custome_id
      SysArticleRefLink.where(custome_id:custom_id).map(&:validate_and_update_links_status)
    elsif params[:pointer_link_id].present?
      @pointer_link = SysArticleRefLink.where(id:params[:pointer_link_id]).last 
      @pointer_link.validate_and_update_links_status
    end
  end

  def get_pointer_history
    if @access_level > 1
       @system_pointer = SysArticleRef.find(params[:id])

      @report_data =  @system_pointer.get_pointer_history(30,"created_at")
    else
      redirect_to "/admin/pointers" and return
    end
     render :layout=> false
  end

  def create_pointer_link
    if @access_level > 1
      @system_pointer_link = SysArticleRefLink.new(params[:sys_article_ref_link])
      @system_pointer = SysArticleRef.where(custome_id:@system_pointer_link.custome_id).last
      if @system_pointer_link.save
        redirect_to "/admin/pointers/#{@system_pointer.id}"
      else
        @counteries = (SysArticleRefLink.distinct(:country).sort - ["other"]) + ["other"]
        render :new_pointer_link
      end
    else
      redirect_to "/admin/pointers"
    end
  end

  def edit_pointer_link
    if @access_level > 1
      @system_pointer_link = SysArticleRefLink.find(params[:id])
      @system_pointer = SysArticleRef.where(custome_id:@system_pointer_link.custome_id).last
      @counteries = (SysArticleRefLink::CountryList - [nil]).sort - ["other"] + ["other"]
       
      @next_pointer_link_id = SysArticleRefLink.where(:id.gt => @system_pointer_link.id).order_by([[:_id, :asc]]).limit(1).first.id rescue nil
      @previous_pointer_link_id = SysArticleRefLink.where(:id.lt => @system_pointer_link.id).order_by([[:_id, :desc]]).limit(1).first.id rescue nil
       
    else
      redirect_to "/admin/pointers"
    end
  end

  def edit_next_pointer_link
    if @access_level > 1
      @system_pointer_link = SysArticleRefLink.find(params[:id])
      @system_pointer = SysArticleRef.where(custome_id:@system_pointer_link.custome_id).last
      @counteries = (SysArticleRefLink.distinct(:country) -[nil]).sort - ["other"] + ["other"]
       
      @previous_pointer_link_id  = SysArticleRefLink.where(:id.lt => @system_pointer_link.id,custome_id:@system_pointer_link.custome_id).order_by([[:_id, :desc]]).limit(1).first.id rescue nil
      
      @next_pointer_link_id= SysArticleRefLink.where(:id.gt => @system_pointer_link.id,custome_id:@system_pointer_link.custome_id).order_by([[:_id, :asc]]).limit(1).first.id rescue nil
       
    else
      redirect_to "/admin/pointers"
    end
  end
   def edit_previous_pointer_link
    if @access_level > 1
      @system_pointer_link = SysArticleRefLink.find(params[:id])
      @system_pointer = SysArticleRef.where(custome_id:@system_pointer_link.custome_id).last
      @counteries = (SysArticleRefLink.distinct(:country) -[nil]).sort - ["other"] + ["other"]
        
      @previous_pointer_link_id = SysArticleRefLink.where(:id.lt => @system_pointer_link.id,custome_id:@system_pointer_link.custome_id).order_by([[:_id, :desc]]).limit(1).first.id rescue nil
      @next_pointer_link_id = SysArticleRefLink.where(:id.gt => @system_pointer_link.id,custome_id:@system_pointer_link.custome_id).order_by([[:_id, :asc]]).limit(1).first.id rescue nil
       
     
    end
  end


  def update_pointer_link

    if @access_level > 1
      sys_pointer_link = SysArticleRefLink.find(params[:sys_article_ref_link][:id])
      sys_pointer_link.update_attributes(params[:sys_article_ref_link])
      sys_pointer = SysArticleRef.where(custome_id:sys_pointer_link.custome_id).last
      @next_pointer_link_id = SysArticleRefLink.where(:id.gt => sys_pointer_link.id,custome_id:sys_pointer_link.custome_id).order_by([[:_id, :asc]]).limit(1).first.id rescue nil
      if params[:update_and_edit_next].present? && @next_pointer_link_id.present?
        redirect_to "/admin/pointers/edit_pointer_link?id=#{@next_pointer_link_id}"
      else
        redirect_to "/admin/pointers/#{sys_pointer.id}"
      end
    else
      redirect_to "/admin/pointers"
    end
  end
  
  def delete_pointer_link
    if @access_level > 2 || System::Settings.pointer_access_to_update?(current_user)
      @sys_pointer_link = SysArticleRefLink.where(id:params[:id]).last
      @sys_pointer_link.delete
    else
      #redirect_to "/admin/pointers"
    end
  end
  
  # Duplicate all pointer link 
  def clone_pointer_link
    if @access_level > 1
      sys_pointer_link_obj = SysArticleRefLink.find(params[:id])
      sys_pointer_link = sys_pointer_link_obj.attributes
      sys_pointer_link.delete("_id")
      @cloned_sys_pointer_link = SysArticleRefLink.new(sys_pointer_link) 
      @cloned_sys_pointer_link.status = "Draft"
      
      @cloned_sys_pointer_link.save
     else
       
    end
  end

  #delete all pointer having custome id in params 
  def test_notification_off
    @system_pointer = SysArticleRef.where(custome_id:params[:custome_id]).last
    child_ids = current_user.childern_ids.map(&:to_s)

    identifiers = ArticleRef.where(custome_id:params[:custome_id]).in(member_id:child_ids).map(&:s_id)
    UserNotification.in(identifier:identifiers).where(user_id:current_user.user.id).delete_all
    ArticleRef.in(s_id:identifiers).delete_all
     
  end


  def test_notification_validation_to_asign
    @system_pointer = SysArticleRef.where(custome_id:params[:custome_id]).last
    child_member_id = params[:member_id]
    ArticleRef.assign_article_ref(child_member_id,{:sys_ref_list=>[@system_pointer]})
     @pointer =  ArticleRef.where(member_id:child_member_id,custome_id:params[:custome_id].to_i).last
  end

  def test_notification_on
    # Add an Pointer entry in random child account of current user. 
    milestone = Milestone.last
    prenatal_test = MedicalEvent.last
    vaccination = Vaccination.last
    system_vaccin = SystemVaccin.find(vaccination.sys_vaccin_id) rescue SystemVaccin.last
    health = Health.last
    pregnancy = Pregnancy.last
    @system_pointer = SysArticleRef.where(custome_id:params[:custome_id]).last
    scenario_detail = @system_pointer.scenario_detail
    member = Member.where(id:current_user.childern_ids.last).last
    #Send Same pointer with different ref link of different country available in system to test 
    # countries =   User.distinct(:country_code)
    country_codes = {"india" => "in","other"=>"other", "uk"=>"uk", "us"=>"us", "ae"=>"ae"}
    countries =  country_codes.keys
    countries.each do |country|
      country_code = country_codes[country]
      ref_link_data = ArticleRef.ref_link_for_refrence(@system_pointer,country)
      begin
        pointer_name = scenario_detail.title_in_tech.gsub("sleep","sileep")
        name =  eval(pointer_name) 
      rescue Exception=>e
        name = pointer_name
      end
       name.gsub!("sileep","sleep") rescue nil
      begin
        pointer_description = @system_pointer.description.gsub("sleep","sileep")
        description =  eval(pointer_description) 
      rescue Exception=>e
        description = @system_pointer.description
      end
      description.gsub!("sileep","sleep") rescue nil
        data = 
        { 
          :name=>name,
          :description=>  description,
          :ref_start_date=>  (eval(scenario_detail.start_date) rescue Date.today) ,
          :ref_peak_relevence_date => (eval(scenario_detail.peak_rel_date) rescue Date.today),
          :ref_expire_date =>  (Date.today+3) ,
          :sys_article_ref => @system_pointer.id.to_s,
          :record_id => (eval(@sys_article_category).id rescue nil),
          :custome_id => @system_pointer.custome_id,
          :N_factor => (scenario_detail.N_factor rescue 1),
          :category => scenario_detail.category,
          :status => @system_pointer.status,
          :score=> (scenario_detail.score rescue 1),
          :member_id => member.id.to_s,
          :ref_info => ref_link_data,
          :new_added=> true,
          :valid_for_role=> @system_pointer.valid_for_role,
          :s_id=> ((@system_pointer.custome_id.to_s + "_" + member.id.to_s) rescue nil) 
        }
        obj = ArticleRef.new(data)
        obj["effective_score"] = 100
        obj.save(validate:false)
        UserNotification::PushDataForValidation.where(user_id:current_user.user_id.to_s).delete_all
        UserNotification::EmailDataForValidation.where(user_id:current_user.user_id.to_s).delete_all
       
      # Add entry in UserNotification Table and push on current user device.  
        add_pointer_notification_to_user_notification(obj.id.to_s,current_user,1,country_code)
         
        user_notification = UserNotification.where(identifier:obj.s_id).last
        if user_notification.present?
          begin 
            status = "sent"
      
            UserNotification.push_notification(user_notification,current_user)
          rescue Exception=>e 
            Rails.logger.info e.message
            status =  "pending" 
            @error = e.message             
          end
          user_notification.status = status
          user_notification.push_error_message = @error
          user_notification.save
        end
    end   
  end
  
  #Add Notification to UserNotification for Pointer
  def add_pointer_notification_to_user_notification(pointer_id, current_user,pointer_limit=2,country_code)
    begin
      user_id = current_user.user_id
      batch_notification = BatchNotification::BatchNotificationManager.where(identifier:/^pointer/i,status:"Active").last
      if batch_notification.blank? || !batch_notification.is_valid_notification?
        Rails.logger.info "Pointer batch notification is not in active status"
        return false
      end
      type = batch_notification.identifier
      notify_by = batch_notification.notify_by
      destination_name = batch_notification.destination
      category = batch_notification.category
      destination_type = "User"
       
      batch_no = batch_notification.batch_no 
      pointer_limit = pointer_limit
      count = 0
      childern_ids = current_user.childern_ids
      notification_pointer_ids = UserNotification.where(user_id:user_id,status:"sent",type:type).map(&:identifier).map(&:to_s)
      #UserNotification.where(user_id:user_id,status:"pending",type:type).delete_all
      pointers = ArticleRef.where(id:pointer_id)
      pointers.each do |pointer|
        deep_link_data = UserNotification.prepare_data_for_deeplink(current_user,pointer.member_id,destination_name,destination_type,pointer.id)
        begin
            message= (pointer.name + "\n" + pointer.description)
            obj = UserNotification.new({country_code:country_code, batch_no: batch_no, category:category, created_at:Time.now, priority:1,user_id:user_id,member_id:pointer.member_id.to_s,message:message, email_status:"pending",notify_by:notify_by,  status:"pending",identifier:pointer.s_id,type:type,score:pointer.effective_score}.merge(deep_link_data))
            if obj.upsert
              count = count + 1
            end
        rescue Exception=>e
          Rails.logger.info e.message
        end
         break if count >= pointer_limit
      end
    rescue Exception=>e
      Rails.logger.info e.message
    end
  end
  # get 3 day count of push pointer 
  def push_pointer_count_report
    @data = []
     @total_notification_assigned_one_day_back = UserNotification.where(:created_at.gte=>Date.today-1.day).count
    @sent_notification_assigned_one_day_back = UserNotification.where(:created_at.gte=>Date.today-1.day).where(status:"sent").count
    @pending_notification_assigned_one_day_back = UserNotification.where(:created_at.gte=>Date.today-1.day).where(status:"pending").count
    
    @data << {:date=>Date.today - 1.day, :total=>@total_notification_assigned_one_day_back, :sent=>@sent_notification_assigned_one_day_back, :pending=> @pending_notification_assigned_one_day_back}
    @total_notification_assigned_two_day_back = UserNotification.where(:created_at.gte=>Date.today-2.day,:created_at.lte=>Date.today-1.day).count
    @sent_notification_assigned_two_day_back = UserNotification.where(:created_at.gte=>Date.today-2.day,:created_at.lte=>Date.today-1.day).where(status:"sent").count
    @pending_notification_assigned_two_day_back = UserNotification.where(:created_at.gte=>Date.today-2.day,:created_at.lte=>Date.today-1.day).where(status:"pending").count
    @data << {:date=>Date.today - 2.day,:total=>@total_notification_assigned_two_day_back, :pending=>@pending_notification_assigned_two_day_back, :sent=> @sent_notification_assigned_two_day_back}
    
    @total_notification_assigned_3_day_back = UserNotification.where(:created_at.gte=>Date.today-3.day,:created_at.lte=>Date.today-2.day).count
    @sent_notification_assigned_3_day_back = UserNotification.where(:created_at.gte=>Date.today-3.day,:created_at.lte=>Date.today-2.day).where(status:"sent").count
    @pending_notification_assigned_3_day_back = UserNotification.where(:created_at.gte=>Date.today-3.day,:created_at.lte=>Date.today-2.day).where(status:"pending").count
     
    @data << {:date=>Date.today - 3.day, :total=>@total_notification_assigned_3_day_back, :sent=>@sent_notification_assigned_3_day_back, :pending=> @pending_notification_assigned_3_day_back}
  
  end
  
  #on click next link , It will render next pointer in list
  def go_to_next_pointer
    @system_pointer = SysArticleRef.where(custome_id:params[:id]).last
    @system_pointer_assigned_count = ArticleRef.assigned_pointer_group_by_system_pointer_id(@system_pointer.id)
    @next_level_mapping_text = {"DevReview"=>"Activate","Active"=>"Deactivate"}

    @system_pointer_links = SysArticleRefLink.where(custome_id:@system_pointer.custome_id)
    @next_pointer_custome_id = @system_pointer.custome_id.to_i + 1
    
    @max_custome_id = SysArticleRef.max(:custome_id)
    @previous_pointer_custome_id = @system_pointer.custome_id.to_i - 1
  end

 #on click next link , It will render next pointer in list
  def go_to_previous_pointer
    @system_pointer = SysArticleRef.where(custome_id:params[:id]).last
    @system_pointer_assigned_count = ArticleRef.assigned_pointer_group_by_system_pointer_id(@system_pointer.id)
    @next_level_mapping_text = {"DevReview"=>"Activate","Active"=>"Deactivate"}

    @system_pointer_links = SysArticleRefLink.where(custome_id:@system_pointer.custome_id)
    @next_pointer_custome_id = @system_pointer.custome_id.to_i + 1
    @max_custome_id = SysArticleRef.max(:custome_id)
    @previous_pointer_custome_id = @system_pointer.custome_id.to_i - 1
  end
  
  def paginated_link_data
    if params[:id].present?
      @pointer_links = SysArticleRefLink.where(id:params[:id])
    else
      @pointer_links = SysArticleRefLink.datatable_filter(params['search']['value'], params['columns'])
    end
    @source = "pointer_controller"
    scenario_count = @pointer_links.count
    @pointer_links = @pointer_links.datatable_order(params['order']['0']['column'].to_i,
                                params['order']['0']['dir'])
    if params['length'].to_i >= 0
      @pointer_links = @pointer_links.paginate(:page=>(params['start'].to_i/(params['length'].to_i) + 1),:per_page=>params['length'])
    end
    render :json=> {:pointer_links=>@pointer_links,
                 draw: params['draw'].to_i,
                 recordsTotal: SysArticleRefLink.count,
                 recordsFiltered: scenario_count 
                }
  end

  # vaidate user access  with role and access level for pointer Module
  def validate_user_access(module_name="PointersMaintenance")

    @access_level = ModuleAccessForRole.user_access_level_for_module(current_user,module_name)
    unless (@access_level > 0) || current_user.email == AppConfig["superadmin_email"] 
     redirect_to "/"
    end
  end
   
end