class Admin::BatchNotificationManagerController < ApplicationController
  # skip_before_filter :authenticate_user!
  before_filter :validate_user_access
  before_filter :admin_basic_auth
  http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
  
  # http_basic_authenticate_with name: "super",password: "upper" if Rails.env.production?
  # http_basic_authenticate_with name: AppConfig["admin_user_name"],password: AppConfig["admin_password"] if !Rails.env.production?
  # before_filter :admin_basic_auth
  layout 'admin'
  
  

  def index
    @batch_notifications = BatchNotification::BatchNotificationManager.all || []
  end

  
  def new_batch_notification
    @batch_notification = BatchNotification::BatchNotificationManager.new
  end

  def create
    begin
      params[:batch_notification_batch_notification_manager][:identifier] = params[:batch_notification_batch_notification_manager][:identifier].strip.downcase rescue nil
      if (params[:duration].blank? ||  params[:duration] == "NA" || params[:frequency] == "NA")
        params[:batch_notification_batch_notification_manager][:added_child_member_tenure] = "NA"
      else
        params[:batch_notification_batch_notification_manager][:added_child_member_tenure] = params[:duration] + "."  + params[:frequency]
      end
      params[:batch_notification_batch_notification_manager][:subscription_status] =  params[:batch_notification_batch_notification_manager][:subscription_status].reject{|x| x.blank?}
      params[:batch_notification_batch_notification_manager][:user_activity_status] = params[:batch_notification_batch_notification_manager][:user_activity_status].reject{|x| x.blank?}
      params[:batch_notification_batch_notification_manager][:user_segment] =         params[:batch_notification_batch_notification_manager][:user_segment].reject{|x| x.blank?}
      params[:batch_notification_batch_notification_manager][:notify_by] =            params[:batch_notification_batch_notification_manager][:notify_by].reject{|x| x.blank?}
      params[:batch_notification_batch_notification_manager][:week_days_for_push] =   params[:batch_notification_batch_notification_manager][:week_days_for_push].reject{|x| x.blank?}
    
      @batch_notification = BatchNotification::BatchNotificationManager.new(params[:batch_notification_batch_notification_manager])

      if @batch_notification.save
        redirect_to "/admin/batch_notification_manager"
      else
        render :new_batch_notification
      end
    rescue Exception=>e
      render :new_batch_notification
    end
  end

  def edit
    @batch_notification = BatchNotification::BatchNotificationManager.find(params[:id])

  end

  def update
    begin
    @batch_notification = BatchNotification::BatchNotificationManager.find(params[:id])
    params[:batch_notification_batch_notification_manager][:identifier] =           params[:batch_notification_batch_notification_manager][:identifier].strip.downcase rescue nil
    params[:batch_notification_batch_notification_manager][:subscription_status] =  params[:batch_notification_batch_notification_manager][:subscription_status].reject{|x| x.blank?}
    params[:batch_notification_batch_notification_manager][:user_activity_status] = params[:batch_notification_batch_notification_manager][:user_activity_status].reject{|x| x.blank?}
    params[:batch_notification_batch_notification_manager][:user_segment] =         params[:batch_notification_batch_notification_manager][:user_segment].reject{|x| x.blank?}
    params[:batch_notification_batch_notification_manager][:notify_by] =            params[:batch_notification_batch_notification_manager][:notify_by].reject{|x| x.blank?}
    params[:batch_notification_batch_notification_manager][:week_days_for_push] =   params[:batch_notification_batch_notification_manager][:week_days_for_push].reject{|x| x.blank?}
    
    if (params[:duration].blank?  || params[:duration] == "NA" || params[:frequency] == "NA")
      params[:batch_notification_batch_notification_manager][:added_child_member_tenure] = "NA"
    else
      params[:batch_notification_batch_notification_manager][:added_child_member_tenure] = params[:duration] + "." + params[:frequency]
    end
    if @batch_notification.update_attributes(params[:batch_notification_batch_notification_manager])
      redirect_to "/admin/batch_notification_manager"
    else
      render :edit
    end
  rescue
      render :edit
  end
  end

  def delete_batch_notification
       
    @batch_notification = BatchNotification::BatchNotificationManager.find(params[:id])
    @batch_notification.delete
  end
  
  def clear_notification_count
    begin
      # score 11 used to distinguish test notification from real notification
      UserNotification.where(:score=>11, :user_id=>current_user.user_id).delete_all
      UserNotification.where(:score=>11, :user_id=>current_user.user_id).delete_all
      BatchNotification::PushNotification.where(:score=>11,object_id:current_user.id.to_s).delete_all
      UserNotification::PushDataForValidation.where(user_id:current_user.user_id.to_s).delete_all
      UserNotification::EmailDataForValidation.where(user_id:current_user.user_id.to_s).delete_all
    rescue Exception => e 
      Rails.logger.info "inside test notification off..........."
      Rails.logger.info e.message
      @error = true
    end
  end

  def test_notification_on
  # score 11 used to distinguish test notification from real notification
    begin
      identifier = params[:identifier].to_s
      family_id = current_user.user_families.last.id.to_s rescue Family.last.id.to_s
      batch_notification = BatchNotification::BatchNotificationManager.where(identifier:identifier).last
      notify_by = batch_notification.notify_by
      if UserNotification.notify_by_email?(notify_by)
        initial_test_notification_pending_count =  UserNotification.where(:score=>11, :user_id=>current_user.user_id,email_status:"pending").count
        initial_email_test_notification_sent_count =  UserNotification.where(:score=>11, :user_id=>current_user.user_id,email_status:"sent").count
        initial_push_test_notification_sent_count = 0
      elsif UserNotification.notify_by_email_and_push?(notify_by)
        initial_test_notification_pending_count =  UserNotification.where(:score=>11, :user_id=>current_user.user_id,email_status:"pending").count
        initial_email_test_notification_sent_count =  UserNotification.where(:score=>11, :user_id=>current_user.user_id,email_status:"sent").count
        initial_push_test_notification_sent_count =  UserNotification.where(:score=>11, :user_id=>current_user.user_id,status:"sent").count
      else
        initial_test_notification_pending_count =  UserNotification.where(:score=>11, :user_id=>current_user.user_id,status:"pending").count
        initial_push_test_notification_sent_count =  UserNotification.where(:score=>11, :user_id=>current_user.user_id,status:"sent").count
        initial_email_test_notification_sent_count = 0
     
      end

      if params[:check_validation_status] == "true" || params[:commit] == "Test notification on(With validatation)"
        options = {:family_id=>family_id, :score=>11, :user_ids=>[current_user.user_id],:child_member_ids=>current_user.childern_ids,:test_without_validation=> false}
      else  
        options = {:family_id=>family_id, :score=> 11, :user_ids=>[current_user.user_id],:child_member_ids=>current_user.childern_ids,:test_without_validation=> true}
      end
      case identifier
      when "assign_measurement_push"
        BatchNotification::Tools::Measurement.assign_measurement_push
      when "1"
        BatchNotification::PushNotification.assign_batch_notification_for_identifier_1(params[:identifier],options)
     when "2"
        BatchNotification::Tools::Milestones.assign_batch_notification_for_identifier_2(params[:identifier],options)
     when "3"
        BatchNotification::Tools::Milestones.assign_batch_notification_for_identifier_3(params[:identifier],options)
      when "5"
        BatchNotification::PushNotification.assign_batch_notification_for_identifier_5(params[:identifier],options)
     when "7"
        BatchNotification::Tools::Immunisations.assign_batch_notification_for_identifier_7(params[:identifier],options)
      when "8"
        BatchNotification::Tools::Immunisations.assign_batch_notification_for_identifier_8(params[:identifier],options)
      when "9"
        BatchNotification::Tools::Milestones.assign_batch_notification_for_identifier_9(params[:identifier],options)
      when "10"
        BatchNotification::Tools::Measurement.assign_batch_notification_for_identifier_10(params[:identifier],options)
      when "12"
        BatchNotification::Tools::Measurement.assign_batch_notification_for_identifier_12(params[:identifier],options)
      when "14"
        BatchNotification::Tools::Measurement.assign_batch_notification_for_identifier_14(params[:identifier],options)
      when "15"
        BatchNotification::Tools::ToothChart.assign_batch_notification_for_identifier_15(params[:identifier],options)
      when "16"
        BatchNotification::GeneralNotification.assign_batch_notification_for_identifier_16(params[:identifier],options)
       when "pointers"
        BatchNotification::Tools::Pointers.assign_batch_notification_for_identifier_pointer(params[:identifier],options)
      when "17"
        family_id = current_user.user_families.last.id rescue Family.last.id.to_s
        subscription = Subscription.where(family_id:family_id,member_id:current_user.id).last
        if subscription.blank?
          subscription = Subscription.where(family_id:family_id).last
          subscription.member_id = current_user.id
          options[:subscription] = subscription
        end
        InstantNotification::Subscription.notification_for_identifier_17(params[:identifier],current_user,family_id,options)
      when "19"
        BatchNotification::Tools::Timeline.assign_batch_notification_for_identifier_19(identifier,options)
      when "20"
        BatchNotification::Tools::Timeline.assign_batch_notification_for_identifier_20(identifier,options)
      when "21"
        BatchNotification::Tools::MentalMaths.assign_batch_notification_for_identifier_21(identifier,options)
      when "23"
        BatchNotification::Tools::Faq.assign_batch_notification_for_identifier_23(params[:identifier],options)
      when "24"
        BatchNotification::Tools::Subscription.assign_batch_notification_for_identifier_24(params[:identifier],options)
      when "25"
        BatchNotification::Tools::Subscription.assign_batch_notification_for_identifier_25(params[:identifier],options)
      # when "27"
      #   BatchNotification::Tools::Subscription.assign_batch_notification_for_identifier_27(params[:identifier],options)
      when "26"
        BatchNotification::Tools::MentalMaths.assign_batch_notification_for_identifier_26(identifier,options)
      when "27"
        child_id = current_user.ordered_pregnancy_list.first || Pregnancy.last.expected_member_id
        InstantNotification::Pregnancy.notification_for_identifier_27(identifier,current_user,child_id,options)
      when "28"
        BatchNotification::Tools::MentalMaths.assign_batch_notification_for_identifier_28(identifier,options)
      when "29"
        child_id = current_user.ordered_child_list_excluding_pregnancy.first || ChildMember.last.id
        InstantNotification::Member.notification_for_identifier_29(identifier,current_user,child_id,options)
       when "30"
        options[:pregnancy_ids] = Pregnancy.where(:expected_member_id.in=>current_user.childern_ids).map(&:expected_member_id) rescue nil
        BatchNotification::GeneralNotification.assign_batch_notification_for_identifier_30(identifier,options)
      
      when "31"
        BatchNotification::Tools::MentalMaths.assign_batch_notification_for_identifier_31(identifier,options)

      when "32"
        BatchNotification::Tools::MentalMaths.assign_batch_notification_for_identifier_32(identifier,options)

      when "33"
        options[:pregnancy_ids] = Pregnancy.where(:expected_member_id.in=>current_user.childern_ids).map(&:id) rescue nil
        BatchNotification::GeneralNotification.assign_batch_notification_for_identifier_33(identifier,options)
       when "34"
        BatchNotification::Tools::VisionHealth.assign_batch_notification_for_identifier_34(identifier,options)
      when "35"
        BatchNotification::GeneralNotification.assign_batch_notification_for_identifier_35(params[:identifier],options)
      when "36"
        BatchNotification::Nhs::SyndicatedContent.assign_batch_notification_for_identifier_36(params[:identifier],options)
      when "37"
        BatchNotification::Nhs::SyndicatedContent.assign_batch_notification_for_identifier_37(params[:identifier],options)

      when "38"
        options[:pregnancy_ids] = Pregnancy.where(:expected_member_id.in=>current_user.childern_ids).map(&:expected_member_id) rescue nil
        BatchNotification::GeneralNotification.assign_batch_notification_for_identifier_38(identifier,options)

      when "39"
        BatchNotification::GpLinkingNotification.assign_batch_notification_for_identifier_39(params[:identifier],options)
     
      when "delete_all_batch_notification"
        UserNotification.where(:user_id=>current_user.user_id,:score=>11).delete_all
        BatchNotification::PushNotification.where(object_id:current_user.id.to_s).delete_all
        UserNotification::PushDataForValidation.where(user_id:current_user.user_id.to_s).delete_all
        UserNotification::EmailDataForValidation.where(user_id:current_user.user_id.to_s).delete_all
      end
         
      begin
        if UserNotification.notify_by_email?(notify_by)
          total_assigned_notification_count =  UserNotification.where(:score=>11, :user_id=>current_user.user_id,email_status:"pending").count
        else
          total_assigned_notification_count =  UserNotification.where(:score=>11, :user_id=>current_user.user_id,status:"pending").count
        end
        @assigned_notification_count =  total_assigned_notification_count - initial_test_notification_pending_count

        notify_by = (batch_notification.notify_by || "push") rescue "push"
        if batch_notification.push_type == "Instant"
          UserNotification.where(type:identifier,:user_id.in=>options[:user_ids]).each do |notification_obj|
            if UserNotification.notify_by_push?(notify_by) || UserNotification.notify_by_email_and_push?(notify_by)
              UserNotification.push_notification(notification_obj,current_user,nil,nil,nil,{notification_type:"instant"}.merge(options)) rescue Rails.logger.info "Push not sent for id #{notification_obj.id}"
            end
            
            if UserNotification.notify_by_email?(notify_by) || UserNotification.notify_by_email_and_push?(notify_by)
              UserNotification.send_email_notification(notification_obj,current_user,{notification_type:"instant"}.merge(options)) rescue Rails.logger.info "notification id #{notification_obj.id} not sent"
            end
          end
        else
          UserNotification.send_push_notification_by_batch(batch_notification.batch_no,current_user.user.country_code,options)
        end
        
        if UserNotification.notify_by_email?(notify_by)
          total_email_test_notification_sent_count =  UserNotification.where(:score=>11, :user_id=>current_user.user_id,email_status:"sent").count
          total_push_test_notification_sent_count = 0
        elsif UserNotification.notify_by_email_and_push?(notify_by)
          total_email_test_notification_sent_count =  UserNotification.where(:score=>11, :user_id=>current_user.user_id,email_status:"sent").count
          total_push_test_notification_sent_count =  UserNotification.where(:score=>11, :user_id=>current_user.user_id,status:"sent").count
        else
          total_push_test_notification_sent_count =  UserNotification.where(:score=>11, :user_id=>current_user.user_id,status:"sent").count
          total_email_test_notification_sent_count = 0
        end
        @sent_push_notification_count = total_push_test_notification_sent_count - initial_push_test_notification_sent_count   
        @sent_email_notification_count = total_email_test_notification_sent_count - initial_email_test_notification_sent_count   
        
        if batch_notification.push_type == "Instant"
          @assigned_notification_count = @sent_push_notification_count > 0 ?  @sent_push_notification_count : @sent_email_notification_count
        end
      rescue Exception=> e 
        Rails.logger.info "inside test batch push test notification on..........."
        Rails.logger.info e.message
        UserMailer.notify_dev_for_error(e.message,e.backtrace,options,"Error inside test_notification_on").deliver!
        @error = true
      end
    rescue Exception=> e 
      Rails.logger.info "inside test batch push test notification on..........."
      Rails.logger.info e.message
      UserMailer.notify_dev_for_error(e.message,e.backtrace,options,"Error inside test_notification_on").deliver!
      @error = true
    end
  end


  private
  
  def validate_user_access(module_name="BatchNotificationManager")
    @access_level = ModuleAccessForRole.user_access_level_for_module(current_user,module_name)
    unless (@access_level > 0) || current_user.email == AppConfig["superadmin_email"] 
     redirect_to "/"
    end
  end 

end
