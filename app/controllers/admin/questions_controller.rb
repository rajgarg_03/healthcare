class Admin::QuestionsController < ApplicationController
  # skip_before_filter :authenticate_user!
  before_filter :admin_basic_auth
  http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
  layout 'admin'
  
  # GET /questions
  # GET /questions.json
  def index
    @questionnaire = SystemQuestionnaire.where(:id => params[:questionnaire_id]).first    
    @questions = @questionnaire.system_questions

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @questions }
    end
  end

  # GET /questions/1
  # GET /questions/1.json
  def show
    @questionnaire = SystemQuestionnaire.where(:id => params[:questionnaire_id]).first
    @question = SystemQuestion.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @question }
    end
  end

  # GET /questions/new
  # GET /questions/new.json
  def new
    @questionnaire = SystemQuestionnaire.where(:id => params[:questionnaire_id]).first
    @question = @questionnaire.system_questions.build

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @question }
    end
  end

  # GET /questions/1/edit
  def edit
    @questionnaire = SystemQuestionnaire.where(:id => params[:questionnaire_id]).first 
    @question = SystemQuestion.where(:id => params[:id]).first
  end

  # POST /questions
  # POST /questions.json
  def create
    @questionnaire = SystemQuestionnaire.where(:id => params[:questionnaire_id]).first
    @question = @questionnaire.system_questions.build(params[:question])

    respond_to do |format|
      if @question.save
        format.html { redirect_to admin_questionnaire_question_path(@questionnaire.id,@question.id), notice: 'Question was successfully created.' }
        format.json { render json: admin_questionnaire_question_path(@questionnaire.id,@question.id), status: :created, location: @question }
      else
        format.html { render action: "new" }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /questions/1
  # PUT /questions/1.json
  def update
    @questionnaire = SystemQuestionnaire.where(:id => params[:questionnaire_id]).first
    @question = SystemQuestion.where(:id => params[:id]).first

    respond_to do |format|
      if @question.update_attributes(params[:question])
        format.html { redirect_to admin_questionnaire_question_path(@questionnaire.id,@question.id), notice: 'Question was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /questions/1
  # DELETE /questions/1.json
  def destroy
    @question = SystemQuestion.find(params[:id])
    @question.destroy

    respond_to do |format|
      format.html { redirect_to admin_questionnaire_questions_url }
      format.json { head :no_content }
    end
  end
  
  # def take_quiz
  #   p params
  #   @question = SystemQuestion.find(params[:question_id])
  #   @answer = @question.answers.build
  # end
  
  # def save_answer
  #   @answer = Answer.new(params[:answer])
  #   @answer.question_id = params[:question_id]
  #   if @answer.save
  #     flash[:notice] = 'Thank you for your feedback.'
  #     redirect_to admin_questionnaire_questions_path
  #   else
  #     render "take_quiz"
  #   end
  # end
  
end
