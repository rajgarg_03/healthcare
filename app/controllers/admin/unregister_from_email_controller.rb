class Admin::UnregisterFromEmailController  < ApplicationController
  before_filter :admin_basic_auth
  http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
  before_filter :validate_user_access
  layout 'admin'

  def index
    @unregister_email = System::UnregisterFromEmail.all.order_by("unregister_on_date desc")
  end

  def unregister_email_from_list
    if params[:email_list_file].present?
      extension =  params[:email_list_file].original_filename.split(".").last
      file =  Roo::Spreadsheet.open(params[:email_list_file],extension: extension)
      System::UnregisterFromEmail.unregister_from_email(current_user,file,options={})
      redirect_to "/admin/unregister_from_email" and return true
    end
  end

private
  def validate_user_access(module_name="UnregisterFromEmail")
    @access_level = ModuleAccessForRole.user_access_level_for_module(current_user,module_name)
    unless (@access_level > 0) || current_user.email == AppConfig["superadmin_email"] 
     redirect_to "/"
    end
  end
  
end