class Admin::ClinicController < ApplicationController
  before_filter :admin_basic_auth
  http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
  before_filter :validate_user_access
  layout 'admin'

  def index
    @organizations = Clinic::Organization.all || []
  end
  
  def new_organization
    @organization = Clinic::Organization.new
  end

  def create_organization
    redirect_to "/admin/clinic" and return if @access_level < 2
    options = {:organization_type=>(params[:clinic_organization][:clinic_type] || "smrthi")}
    @organization,status  = Clinic::Organization.create_organization(current_user,params[:clinic_organization],options)
    
    if status == true
      redirect_to "/admin/clinic"
    else
      render :new_organization
    end
  end

  def edit
    redirect_to "/admin/clinic" and return if @access_level < 2
    @organization = Clinic::Organization.find(params[:id])
  end

  def update_organization
    @organization = Clinic::Organization.find(params[:id])
    options = {:organization_type=>(params[:clinic_organization][:clinic_type] || "smrthi")}
    options[:current_user] = current_user
    organization, status = @organization.update_organization(current_user,params[:clinic_organization],options)
    if status == true
      redirect_to "/admin/clinic/"
    else
      render :edit
    end
  end

  def delete_organization
    redirect_to "/admin/clinic" and return if @access_level < 3
    @organization = Clinic::Organization.find(params[:id])
    @organization.delete
  end

def list_organization_user
    if params[:organization_id].present?
      @organization = Clinic::Organization.find(params[:organization_id])
      @organization_users = Clinic::OrganizationUser.where(organization_id:params[:organization_id])
    else
      @organization_users = Clinic::OrganizationUser.all || []
    end
  end
  
  def new_organization_user
    if params[:organization_id].present?
      @organization = Clinic::Organization.find(params[:organization_id])
    end
    @organization_user = Clinic::OrganizationUser.new
  end

  def create_organization_user
    redirect_to "/admin/clinic" and return if @access_level < 2
    @organization_user = Clinic::OrganizationUser.new(params[:clinic_organization_user])
    if @organization_user.save
      redirect_to "/admin/clinic/list_organization_user?organization_id=#{@organization_user.organization_id}"
    else
      render :new_organization_user
    end
  end

  def edit_user
    redirect_to "/admin/clinic" and return if @access_level < 2
    @organization_user = Clinic::OrganizationUser.find(params[:id])
  end
  
  def import_user_from_file
    redirect_to "/admin/clinic" and return if @access_level < 2
    @organization_user = Clinic::OrganizationUser.new
    if params[:organization_id].present?
      @organization = Clinic::Organization.find(params[:organization_id])
    end
  end
  def save_imported_organization_user
    file = params[:user_list]
    options = {}
    organization_id = params[:clinic_organization_user][:organization_id].present? ? params[:clinic_organization_user][:organization_id]:nil
    if(Clinic::OrganizationUser.import_from_xls(organization_id,file,current_user,options))
      @organization = Clinic::Organization.find(params[:organization_id]) rescue nil
      redirect_to "/admin/clinic/list_organization_user?organization_id=#{organization_id}"
    else
      @organization_user = Clinic::OrganizationUser.new
      @error_message = "Failed to import."
      render :import_user_from_file
    end
  end
  

  def update_organization_user
    @organization_user = Clinic::OrganizationUser.find(params[:id])
    if @organization_user.update_attributes(params[:clinic_organization_user])
      redirect_to "/admin/clinic/list_organization_user"
    else
      render :edit_user
    end
  end

  def delete_organization_user
    redirect_to "/admin/clinic" and return if @access_level < 3
    @organization_user = Clinic::OrganizationUser.find(params[:id])
    @organization_user.delete
  end

  
  private
  def validate_user_access(module_name="Clinic")
    @access_level = ModuleAccessForRole.user_access_level_for_module(current_user,module_name)
    unless (@access_level > 0) || current_user.email == AppConfig["superadmin_email"] 
     redirect_to "/"
    end
  end 
  
end