class Admin::UserRoleForAdminPanelController < ApplicationController
  skip_before_filter :authenticate_user!
  before_filter :admin_basic_auth
  http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
  before_filter :validate_user_access

  layout 'admin'

  def index
  	@user_roles = UserRoleForAdminPanel.all
  end

  def new_role
  	@user_role = UserRoleForAdminPanel.new
  end

  def create
    begin
      @user_role = UserRoleForAdminPanel.new(params[:user_role_for_admin_panel])
      member_id = Member.where(email:params[:user_role_for_admin_panel][:email]).last.id
      @user_role.member_id = member_id
      if @user_role.save
        redirect_to "/admin/user_role_for_admin_panel"
      else
      	render :new_role
      end
    rescue Exception=>e
      @user_role.errors.add(:email, "is not valid")
        
        render :new_role
    end
  end

  def edit
  	@user_role = UserRoleForAdminPanel.find(params[:id])

  end

  def update
    begin
  	@user_role = UserRoleForAdminPanel.find(params[:id])
    member_id = Member.where(email:params[:user_role_for_admin_panel][:email]).last.id
    params[:user_role_for_admin_panel][:member_id] = member_id
  	if @user_role.update_attributes(params[:user_role_for_admin_panel])
      redirect_to "/admin/user_role_for_admin_panel"
  	else
      @user_role.errors.add(:email, "is not valid")
  		render :edit
  	end
  rescue
      render :edit
  end
  end

  def delete_role
  	@user_role = UserRoleForAdminPanel.find(params[:id])
  	@user_role.delete
  end

  private
  def validate_user_access(module_name="UserRoleMaintenance")
    @access_level = ModuleAccessForRole.user_access_level_for_module(current_user,module_name)
    unless (@access_level > 0) || current_user.email == AppConfig["superadmin_email"] 
     redirect_to "/"
    end
  end

end