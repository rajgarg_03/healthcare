class Admin::QuestionnairesController < ApplicationController
  # skip_before_filter :authenticate_user!
  before_filter :admin_basic_auth
  http_basic_authenticate_with name: AppConfig["admin_user_name"], password: AppConfig["admin_password"] if Rails.env.production?
  # filter :basic_auth
  layout 'admin'
  
  # GET /questionnaires
  # GET /questionnaires.json
  def index
    @questionnaires = SystemQuestionnaire.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @questionnaires }
    end
  end

  # GET /questionnaires/1
  # GET /questionnaires/1.json
  def show
    @questionnaire = SystemQuestionnaire.where(:id => params[:id]).first

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @questionnaire }
    end
  end

  # GET /questionnaires/new
  # GET /questionnaires/new.json
  def new
    @questionnaire = SystemQuestionnaire.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @questionnaire }
    end
  end

  # GET /questionnaires/1/edit
  def edit
    @questionnaire = SystemQuestionnaire.where(:id => params[:id]).first
    @id = @questionnaire.id
  end

  # POST /questionnaires
  # POST /questionnaires.json
  def create
    @questionnaire = SystemQuestionnaire.new(params[:questionnaire])

    respond_to do |format|
      if @questionnaire.save
        format.html { redirect_to admin_questionnaire_path(@questionnaire.id), notice: 'questionnaire was successfully created.' }
        format.json { render json: admin_questionnaire_path(@questionnaire.id), status: :created, location: @questionnaire }
      else
        format.html { render action: "new" }
        format.json { render json: @questionnaire.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /questionnaires/1
  # PUT /questionnaires/1.json
  def update
    @questionnaire = SystemQuestionnaire.where(:id => params[:id]).first

    respond_to do |format|
      if @questionnaire.update_attributes(params[:questionnaire])
        format.html { redirect_to admin_questionnaire_path(@questionnaire.id), notice: 'questionnaire was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @questionnaire.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /questionnaires/1
  # DELETE /questionnaires/1.json
  def destroy
    @questionnaire = SystemQuestionnaire.where(:id => params[:id]).first
    @questionnaire.destroy

    respond_to do |format|
      format.html { redirect_to admin_questionnaires_url }
      format.json { head :no_content }
    end
  end
end
