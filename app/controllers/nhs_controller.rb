class NhsController < ApplicationController
skip_before_filter :authenticate_user!

  def token
    begin
      result = {}
      if params[:refresh_token] != nil 
        result = Nhs::NhsApi.generate_token_via_refresh_token(params[:refresh_token])
      else 
        result = Nhs::NhsApi.generate_login_token(params[:refresh_token])
      end
      render json: result
      rescue Exception=>e
        Rails.logger.info e.message
        render :json=>{error:e.message,:message=>::Message::NhsLogin[:error_message]}
    end
end

  def authenticate_user
    code = params[:code]
    state_data = eval(ShaAlgo.decryption(params[:state].gsub(' '+ '+'))).with_indifferent_access rescue {}
    options = {:user_email=>state_data[:email],:login_type=>state_data[:login_type]}
    nhs_login_failed_url = "/messages?nhs_login_status=failed"
    begin
      token_response = {}
        token_response = Nhs::NhsApi.generate_login_token(code)
        if token_response[:status] == 200
          Rails.logger.info "In Authorize redirect ===>>>  "
          @user_info = Nhs::NhsApi.fetch_user_info(token_response["access_token"])
          if @user_info[:status] == 200
            Rails.logger.info "token is fetching correctly ===>>>   "+token_response["access_token"]    
            @deeplink = Nhs::NhsApi.create_deeplink_data(@user_info,token_response,options)
            redirect_to @deeplink
          else
            @message = @user_info[:message]
            redirect_to nhs_login_failed_url
          end
        else
          @message = token_response[:message]
          redirect_to nhs_login_failed_url
        end
    rescue Exception=>e
      Rails.logger.info e.message
      @message = ::Message::NhsLogin[:error_message]
      redirect_to @nhs_login_failed_url
    end
  end

  def initiate_user_authentication
    authorize_url = APP_CONFIG['nhs_login_authorize_url']
    user_email = params[:email] || ''
    state = ShaAlgo.encryption({email:user_email,login_type:params[:type]}.to_s)
    client_id = APP_CONFIG['nhs_login_client_id']
    scope = APP_CONFIG['nhs_login_scope']
    redirect_url = APP_CONFIG['nhs_login_redirect']

    final_redirect_url = authorize_url+"&state="+state+"&client_id="+client_id+"&redirect_uri="+redirect_url+"&scope="+scope
    redirect_to final_redirect_url
  end

  #used for nhslogin redirect
  def authorize_redirect
    authenticate_user
  end
end