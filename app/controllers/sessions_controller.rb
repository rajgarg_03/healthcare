class SessionsController < Devise::SessionsController
  before_filter :check_admin_aproval, :only=>[:create]
  layout "login"

  def new
    # clear alexa session
    session[:alexa_source_reference] = nil
    session[:alexa_redirect_uri] = nil
    session[:alexa_flow_status] = nil
    session[:alexa_action_for_fb] = nil

    if !session[:login_error].blank? || flash[:alert].present?
      session[:email] = params[:user][:email] rescue nil
       
      @error = Message::DEVISE[:m6]  #"Username and Password combination you entered is incorrect"
      session[:login_error] = nil
    end
    if params[:pageName] == "signup"
      @error = session[:signup_error]
      @error2 = session[:signup_email]
      session[:signup_error] = nil
      session[:signup_email] = nil
    end
    if params[:pageName] == "login" || params[:pageName] == "reset"
      @error = session[:reset_error]
      @error2 = session[:reset_email] 
      session[:reset_email] = nil
      session[:reset_error] = nil
     end
     @invitation = Invitation.find(session[:invitation_id]) if session[:invitation_id]
    super  
  end
  
  def create
    params[:platform] =     params[:platform] || "web"
    session[:login_error] = Message::DEVISE[:m7] #"Invailid Email and password"
    params[:user][:email] = params[:user][:email].downcase
    # user = User.where(:email=>params[:user][:email]).first
    user = warden.authenticate(auth_options)
     
    if user.present? && user.account_type_allowed?("Nurturey") && (["approved","confirmed"].include?(user.status) || User.can_access_dashboard_after_signup?(user) )
      self.resource = warden.authenticate!(auth_options)    
      sign_in(resource_name, resource)
      event_name = "user logged in successfully"
      user.delay.log_signin_event(event_name,params) rescue nil
      UserLoginDetail.create(user_id:user.id,login_time:Time.zone.now,platform:"Web") rescue nil
      options = {:ip_address=>request.remote_ip}
      member = user.member
      member.update_family_and_user_segment(nil,options) rescue nil
    else
      user = User.where(:email=>params[:user][:email]).first
      session.clear
      event_name = "user attempt to login failed"
      user.delay.log_signin_event(event_name,params) rescue nil

    end
    @error = false
    
    # if user has no child redirect or has uncomplete kick start flow then take him to kick start flow 
    if params[:alexa].to_s == "true"
        redirect_url = params[:redirect_uri] + "#" + "state=#{params[:state]}&access_token=#{user.api_token(true)}&token_type=Bearer"

      redirect_to redirect_url and return
    end
    respond_with resource, :location => after_sign_in_path_for(resource)
  end

  def destroy
    params[:platform] = params[:platform] || "web"
    event_name = "user signout successfully"
    resource_name.delay.log_signin_event(event_name,params) rescue nil
    redirect_path = after_sign_out_path_for(resource_name)    
    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))

    respond_to do |format|
      format.all { head :no_content }
      format.any(*navigational_formats) { redirect_to redirect_path }
    end
  end
   
  def check_admin_aproval
    params[:user][:email] = params[:user][:email].downcase
    user = User.where(:email=>params[:user][:email]).first
    user_blocked = false
    system_allowed_retry_login_count = System::Settings.allowed_login_attempt
    if user.present? && system_allowed_retry_login_count != -1 && user.failed_login_attempt_count >= system_allowed_retry_login_count
      user.status = User::STATUS[:unapproved]
      user.save(validate:false)
      user_blocked = true
    end
     if user.present? && (!User.can_access_dashboard_after_signup?(user) || user_blocked)
      session.clear
      session[:resend_verify_email] = user.email
      redirect_to home_messages_path(:status=>"unapproved") and return true   
    end
  end
end
