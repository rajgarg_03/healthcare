class ArticleRefController < ApplicationController

 # List all pointer having effective score greater than 0
 # Get /article_ref.json?token=3123&shortlisted=true/false
 # param {}
  def index
    begin
      member = current_user.childern_ids.map(&:to_s)
      all_article_refs = ArticleRef.not_in(effective_score:nil).in(member_id:member).or({:remind_me_on=> nil,:ref_expire_date.gte=>Time.zone.now.to_date},{:remind_me_on.lte=>Time.zone.now.to_date})
      # all_article_refs = ArticleRef.in(member_id:member).where(:ref_expire_date.gte=>Date.today).or(shortlisted:true)
      if params[:shortlisted]== "true"
        all_article_refs = all_article_refs.where(shortlisted:true)
      end
      article_refs =   all_article_refs.order_by("effective_score desc").paginate(:page => (params[:page].to_i||1))
      @current_page = (params[:page].to_i||1)
      

      @total_page,@next_page = Utility.page_info(all_article_refs,@current_page)
      # @total_page = @total_page + 1 if (all_article_refs.count%5 !=0)
      # @next_page = (@current_page + 1) if ((@total_page - @current_page) >0)
      all_article_refs.update_all(checked: true) #NOTE: Mark all referances read.
      status = StatusCode::Status200
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
    end
      render :json=> { :status=>status, :current_page=> @current_page,:total_page=>@total_page, :next_page=>@next_page || @total_page,  :error=>@error, :article_ref=> article_refs }
  end



  #Api art_ref_detail : Get the detail of particular ArticleRef
  # Details of a requested pointer id will be return in response 
  # Get /article_ref/art_ref_detail.json?token=
  # params = {:article_ref_id=>"" }
  def art_ref_detail
    begin
      article_ref = ArticleRef.find(params[:article_ref_id])
      status = StatusCode::Status200
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
    end
    render :json=> { :status=>status, :error=>@error, :article_ref=> article_ref }
  end

  # Update pointer details
  # Put /art_ref_detail/update_art_ref.json?token=
  # params = {:article_ref_id=>"", :data=>{:name=>"", :description=>""} }
  def update_art_ref
    begin
      article_ref = ArticleRef.find(params[:article_ref_id])
      article_ref.update_attributes(params[:data])
      status = StatusCode::Status200
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
    end
      render :json=> { :status=>status, :error=>@error, :article_ref=> article_ref }
  end

  # Update pointers remind me value and counter.
  # Put /art_ref_detail/remind_me_later.json?token=
  # params = {:article_ref_id=>"", :data=>{:after_days=>"12"} }
  def remind_me_later
    begin
      article_ref = ArticleRef.find(params[:article_ref_id])
      article_ref.reminder_count = article_ref.reminder_count.to_i + 1
      article_ref.remind_me_on = Date.today + eval(params[:data][:after_days]+".days")
      article_ref.save
      status = StatusCode::Status200
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
    end
      render :json=> { :status=>status, :error=>@error, :article_ref=> article_ref }
  end

  # Shortlist a pointer  
  # Put /art_ref_detail/add_to_favorite.json?token=
  # params = {:article_ref_id=>"", :data=>{:shortlisted=>true/false} }
  def add_to_favorite
    begin
      article_ref = ArticleRef.find(params[:article_ref_id])
      article_ref.update_attributes(params[:data])
      status = StatusCode::Status200
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
    end
      render :json=> { :status=>status, :error=>@error, :article_ref=> article_ref }
  end


  # Delete pointer from list.set score 0, status deleted. This will delete in rake job
  # Delete /art_ref_detail/delete_article_ref.json?token=
  # params {:article_ref_id=>"", }
  def delete_article_ref
    begin
      article_ref = ArticleRef.find(params[:article_ref_id])
      article_ref.status = "deleted"
      article_ref.score = 0
      article_ref.effective_score = 0
      article_ref.save
      status = StatusCode::Status200
    rescue Exception=>e
      status = StatusCode::Status100
      @error = e.message
    end
      render :json=> { :status=>status, :error=>@error }
  end

  # Get highest scored pointer of user. only 2 pointer will be recommended
  def recommended_art_ref
    begin
      member = current_user.childern_ids.map(&:to_s)
      article_ref = ArticleRef.get_pointers_list(current_user, member).order_by("effective_score desc").limit(2) || []
      response = {status: StatusCode::Status200, article_ref: article_ref}
    rescue Exception=>e
      response = {status: StatusCode::Status100, error: e.message}
    end
    render :json=> { :status=>status, :article_ref=> article_ref }
  end  
  
end
