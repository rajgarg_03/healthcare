class MilestonesController < ApplicationController
  skip_before_filter  :verify_authenticity_token, only: [:destroy, :remove_picture]

  def index
    @member = Member.find(params[:member_id])
    family = @member.get_family
    api_version = 4
    tool = Nutrient.get_tool("milestones")
    show_subscription_box_status = tool.show_subscription_box_status(current_user,family.id,api_version) 
    
    find_quick_add
    info_popup_status(@member,"milestones")
    # @milestones = @member.milestones.paginate(:page => (params[:page]), :per_page => 10)
    find_member_milestones
  end

  def change_category
    @system_milestones = SystemMilestone.where(category: params[:category]).entries
    @member = Member.find(params[:member_id])
  end  

  def edit
    # @member = Member.find(params[:member_id])
    @milestone = Milestone.find(params[:id])
    render layout: false
  end  

  def new
    @milestone = Milestone.new(system_milestone_id: params[:system_milestone_id], member_id: params[:member_id])
    render layout: false
  end  

  def create    
    @member = Member.find(params[:milestone][:member_id])
    params[:milestone][:milestone_status] = "accomplished"
    params[:milestone][:status_by] = "user"
    @milestone = Milestone.new(params[:milestone])
    unless @member.already_having_milestone?(@milestone)                    
      if @milestone.save
        no_of_milestone_added = 1
        push_identifier = 4
        child = @member
        msg = "Awesome! #{current_user.first_name} has updated #{no_of_milestone_added} new milestone for #{child.first_name}. Click to check"
        current_user.push_user_action_notification(push_identifier,@milestone,msg,child)


        (params[:upload_media] || []).select{|p| p.present?}.each do |pic|
          @milestone.pictures << Picture.new(local_image:pic,upload_class: "milestone",member_id:@member.id)
        end      
        @member.reload
        @message = Message::GLOBAL[:success][:milestone_create] #"Selected milestone has been added to the accomplished list."
      else
        @message = Message::GLOBAL[:error][:milestone_create]
      end
    else
      @already_having = true    
    end
    find_quick_add
    find_member_milestones
  end

  def update
    @milestone = Milestone.find(params[:id])
    @member = @milestone.member
    (params[:upload_media] || []).select{|p| p.present?}.each do |pic|
      @milestone.pictures << Picture.new(local_image:pic,upload_class: "milestone",member_id:@member.id)
    end
    params[:milestone][:milestone_status] = "accomplished"
      params[:milestone][:status_by] = "user"
    @milestone.update_attributes(params[:milestone])
    @timeline = Timeline.where(timelined_type: 'Milestone', timelined_id: @milestone.id).first
    
    push_identifier = 4
    system_milestone = @milestone.system_milestone
    child = @member
    msg = "Awesome! #{current_user.first_name} has updated milestone \"#{system_milestone.title}\" for #{child.first_name}. Click to check"
    current_user.push_user_action_notification(push_identifier,@milestone,msg,child)
  end  

  def add_multiple
    @member = Member.find(params[:member_id])    
    params[:system_milestone_ids] ||= []
    added_system_milestone_ids = Milestone.where(member_id:@member.id).pluck(:system_milestone_id).map(&:to_s)
    params[:system_milestone_ids].each do |sys_m_id|
      @milestone = Milestone.find_or_initialize_by(member_id: @member.id, system_milestone_id: sys_m_id)
      @milestone.milestone_status = "accomplished"
      @milestone.status_by = "user"
      @milestone.save
    end
      no_of_milestone_added = (params[:system_milestone_ids] - added_system_milestone_ids ).count

      push_identifier = 4
      child = @member
      @milestone["new_id"]  = @milestone.id.to_s + rand(10000).to_s
      if no_of_milestone_added > 1
        msg =   "Awesome! #{current_user.first_name} has updated #{no_of_milestone_added} new milestones for #{child.first_name}. Click to check"
      elsif no_of_milestone_added == 1
        msg =   "Awesome! #{current_user.first_name} has updated #{no_of_milestone_added} new milestone for #{child.first_name}. Click to check"
      end
      current_user.push_user_action_notification(push_identifier,@milestone,msg,child)

    
    @system_milestone_ids = SystemMilestone.where(category: params[:category]).pluck(:id).map(&:to_s)
    removable_system_milestone_ids = (@system_milestone_ids - params[:system_milestone_ids])
    @member.milestones.where({'system_milestone_id' => { "$in" => removable_system_milestone_ids }}).destroy_all
    @member.reload
    find_quick_add
    find_member_milestones
    @message = Message::GLOBAL[:success][:add_multiple] #"Selected milestones have been added to the accomplished list."
         
  end 

  def add_description
    @member = Member.find(params[:member_id])
    @milestone = Milestone.find(params[:id]) if params[:id]
    incomplete_milestones = @member.milestones.where(date: nil).sort_by{|m| m.created_at}
    @milestone ||= incomplete_milestones.first    
    @milestone_sno = incomplete_milestones.index(@milestone).to_i+1
    render :layout=>false
  end  

  def skip
    @member = Member.find(params[:member_id])
    @this_milestone = Milestone.find(params[:id])
    incomplete_milestones = @member.milestones.where(date: nil).sort_by{|m| m.created_at}
    @milestone = incomplete_milestones[incomplete_milestones.index(@this_milestone)+1]
    @milestone_sno = incomplete_milestones.index(@milestone).to_i+1
  end  

  def save_and_next    
    @member = Member.find(params[:member_id])
    @this_milestone = Milestone.find(params[:id])
    (params[:upload_media]||[]).each do |pic|
      @this_milestone.pictures << Picture.new(local_image:pic,upload_class: "milestone",member_id:@member.id)
    end
    incomplete_milestones = @member.milestones.where(date: nil).sort_by{|m| m.created_at}
    @milestone = incomplete_milestones[incomplete_milestones.index(@this_milestone)+1]
    @milestone_sno = incomplete_milestones.index(@milestone).to_i+1
    @this_milestone.update_attributes(params[:milestone])
    @display_setting = @member.member_settings.where(name:"milestone_display").first || @member.member_settings.create(name:"milestone_display", status:"true" )
    find_member_milestones
  end  

  def destroy
    @milestone = Milestone.find(params[:id])
    @milestone.destroy
    @member = @milestone.member.reload
  end  

  def display_later
    @member = Member.find(params[:member_id])
    display_setting = @member.member_settings.where(name:"milestone_display").first
    status = params[:status] == "display_later" ? "true" : "false"
    display_setting.update_attributes({:status => status, note: 7.days.from_now})
  end

  def remove_picture    
    @picture = Picture.find(params[:id])
    @picture.destroy
  end  

  def display_picture
    @picture = Picture.find(params[:id])
    @milestone = @picture.imageable
    render :layout=>false
  end

  def milestones_info
    render layout: false
  end  

  private

  def find_quick_add
    @category = @member.find_category
    @system_milestones = SystemMilestone.where(category: @category).entries
    @quick_add_milestones = (@system_milestones - @member.milestones.map(&:system_milestone).uniq).first(4)
    @display_setting = @member.member_settings.where(name:"milestone_display").first || @member.member_settings.create(name:"milestone_display", status:"true" )
  end

  def find_member_milestones
    @milestones =  Milestone.order_by_date(@member.milestones).paginate(:page => (params[:page]), :per_page => 10)
  end  

end 