class Alexa::SignUpController < ApplicationController
  layout "alexa_signup"
  skip_before_filter :authenticate_user!#, :except => [:user_details,:set_token,:sign_up, :login4, :mobile_experience_popup, :app_invite_fb, :unsubscribe, :login, :messages,:team,:faq, :get_geoname, :index, :about, :contact, :enquiry, :terms, :privacy,]
  before_filter :set_reference_params, :except=>[:sign_in,:index]
  def set_reference_params
    @reference_params = "state=#{session[:alexa_state]}&redirect_uri=#{session[:alexa_redirect_uri]}&app_name=#{session[:alexa_source_reference]}"

  end
  def sign_in
    # session.clear
    session[:alexa_flow_status] = true
    session[:alexa_source_reference] = params[:app_name]
    session[:alexa_redirect_uri] = params[:redirect_uri]
    session[:alexa_state] = params[:state]
    session[:alexa_platform] = params[:platform]
    session[:alexa_user_token] = nil
    Rails.logger.info session.inspect
    @reference_params = "state=#{session[:alexa_state]}&redirect_uri=#{session[:alexa_redirect_uri]}&app_name=#{session[:alexa_source_reference]}"
  end  
  
  # alexa user signup
  def index
    # session = {}
    session[:alexa_flow_status] = true
    session[:alexa_source_reference] =  params[:app_name]
    session[:alexa_redirect_uri] = params[:redirect_uri]
    session[:alexa_state] = params[:state]
    session[:alexa_user_token] = nil
    session[:alexa_platform] = params[:platform]
    @email_available  = false 
    @reference_params = "state=#{session[:alexa_state]}&redirect_uri=#{session[:alexa_redirect_uri]}&app_name=#{session[:alexa_source_reference]}"

    Rails.logger.info session.inspect
  end

  # def create_family
  #   Rails.logger.info "Create family............."
  #   Rails.logger.info session.inspect

  #   session[:alexa_redirect_uri] = params[:redirect_uri].present? ? params[:redirect_uri] : session[:alexa_redirect_uri] 
  #   session[:alexa_state] =  params[:state].present? ? params[:state] : session[:alexa_state] || params[:state]
  #   session[:alexa_user_created_family_id] = nil 
  #   session[:alexa_user_token] = params[:token].present? ? params[:token] : session[:alexa_user_token] ||  params[:token]
  #   session[:alexa_platform] = params[:platform].present? ? params[:platform] : session[:alexa_platform]
  #   if session[:alexa_user_token].blank?
  #     redirect_to "/alexa/sign_up?redirect_uri=#{session[:alexa_redirect_uri]}&state=#{session[:alexa_state]}&app_name=#{session[:alexa_source_reference]}" and return true
  #   end
  #   respond_to do |format|
  #     format.html # show.html.erb
  #     format.js {render :layout=> false}
  #     format.json {render :json=> {status:200,auth_token:session[:alexa_user_token]} }
  #   end
  # end

  # def add_child
       

  #   Rails.logger.info "Add child.................."
  #   Rails.logger.info session.inspect
    
  #   session[:alexa_redirect_uri] = session[:alexa_redirect_uri] || params[:redirect_uri]
  #   session[:alexa_state] =  session[:alexa_state] || params[:state]
  #   session[:alexa_user_token] = params[:token] || session[:alexa_user_token]
  #   session[:alexa_user_created_family_id] = session[:alexa_user_created_family_id] || params[:family_id]
  #   session[:alexa_platform] = params[:platform].present? ? params[:platform] : session[:alexa_platform]
  #   if session[:alexa_user_created_family_id].blank?
  #     redirect_to "/alexa/sign_up?redirect_uri=#{session[:alexa_redirect_uri]}&state=#{session[:alexa_state]}&app_name=#{session[:alexa_source_reference]}" and return true
  #   end
  #   respond_to do |format|
  #     format.html # show.html.erb
  #     format.js {render :layout=> false}
  #     format.json {render :json=> {status:200,family_id:session[:alexa_user_created_family_id]} }
  #   end
  # end
  
  def get_email_verification_screen
   Rails.logger.info "email verification screen........" 
       Rails.logger.info session.inspect
   session_data = (params[:session_data] || params) || {}
   session[:alexa_redirect_uri] =    !session[:alexa_redirect_uri].blank? ? session[:alexa_redirect_uri]: session_data[:redirect_uri] 
   session[:redirect_uri] = session[:alexa_redirect_uri]
   session[:state]=                  !session[:state].blank? ? session[:state]  : session_data[:state] 
   session[:alexa_flow_status]=     true
   session[:alexa_source_reference]= !session[:alexa_source_reference] .blank? ? session[:alexa_source_reference]: session_data[:alexa_source_reference] 
   session[:alexa_platform] =        !session[:alexa_platform].blank? ? session[:alexa_platform]       : session_data[:alexa_platform]
   session[:email] =                 !session[:email].blank? ? session[:email]  : session_data[:email].downcase
   session[:password] =              !session[:password].blank? ? session[:password]             : session_data[:password]
   session[:alexa_state] = session[:state]
    
    email = params[:email].blank? ? session[:email] : params[:email]
    email = User.last.email
    if email.blank?
      redirect_to "/alexa/sign_in?app_name=#{session[:alexa_source_reference]}&redirect_uri=#{session[:alexa_redirect_uri]}&state=#{session[:alexa_state]}" and return true
    end
    session[:email] = email
    session[:password] = params[:password] || session[:password]
    @user =  User.where(email:/^#{email}$/i).last
    respond_to do |format|
      format.html # show.html.erb
      format.js {render :layout=> false}
    end
  end

  def finish_setup
    begin
      Rails.logger.info "Finish Setup............."
      Rails.logger.info session.inspect
      session[:alexa_user_token] =  params[:session_data].present? && params[:session_data][:token].present? ? params[:session_data][:token] : session[:alexa_user_token] 
      @alexa_redirect_url  = session[:alexa_redirect_uri] + "#" + "state=#{session[:alexa_state]}&access_token=#{session[:alexa_user_token]}&token_type=Bearer"
      @user = User.find_user_with_token(session[:alexa_user_token]).last
      @reference_params = "state=#{session[:alexa_state]}&redirect_uri=#{session[:alexa_redirect_uri]}&app_name=#{session[:alexa_source_reference]}"
      
      if params[:help_data].present?
        @help_screen_data = []
        params[:help_data].map{|aa,value| @help_screen_data << {"title"=>value["title"],"description"=>value["description"]} } 
      else 
        @help_screen_data = @user.member.alexa_help_screen_data(session[:alexa_platform],session[:alexa_source_reference])
      end
      if @help_screen_data.blank?
        redirect_to "/alexa/sign_in?#{@reference_params}" and return true
      end
    rescue Exception=> e 
        redirect_to "/alexa/sign_in?#{@reference_params}" and return true
    end
    # session[:alexa_flow_status] = false
    # session[:alexa_user_token] = nil
    Devices::HomeDevice.add_device(session[:alexa_platform],@user.member_id)
    respond_to do |format|
      format.html # show.html.erb
      format.js {render :layout=> false}
      format.json {render :json=> {status:200,auth_token:session[:alexa_user_token]} }
    end
  end  


  def get_user
   session_data = (params[:session_data] || params) || {}
   session[:alexa_redirect_uri] =    !session[:alexa_redirect_uri].blank? ? session[:alexa_redirect_uri]: session_data[:redirect_uri] 
   session[:redirect_uri] = session[:alexa_redirect_uri]
   session[:state]=                  !session[:state].blank? ? session[:state]  : session_data[:state] 
   session[:alexa_flow_status]=     true
   session[:alexa_source_reference]= !session[:alexa_source_reference] .blank? ? session[:alexa_source_reference]: session_data[:alexa_source_reference] 
   session[:alexa_platform] =        !session[:alexa_platform].blank? ? session[:alexa_platform]       : session_data[:alexa_platform]
   session[:email] =                 params[:user][:email].downcase
   session[:password] =              !session[:password].blank? ? session[:password]             : session_data[:password]
   session[:alexa_state] = session[:state]
   
    user = User.where(:email=> params[:user][:email].downcase).last
    # user = User.where(:email=> params[:user][:email]).last
    session[:alexa_user_token] = user.api_token(is_alexa_signin=true)
    controllers,status = BasicSetupEvaluator.run(user.member,nil,5,{:platform=>(session[:alexa_platform]|| "alexa"),:app_name=>session[:alexa_source_reference]})
    api_token = user.api_token(is_alexa_signin=true)
    respond_to do |format|
      format.json {render :json => {user_detail:user,status:200,token:user.api_token(is_alexa_signin=true), controllers:controllers,basic_setup_status:status} }
      format.html
    end
  end

  def get_family
    member_id = params[:member_id]
    family = Family.where(:member_id=>member_id).first

    if family == nil 
      return {status:100,message:"User is not associated to family yet."}
    else 
      return {status:200,user_details:family}
    end
  end  



end
