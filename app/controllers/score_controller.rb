class ScoreController < ApplicationController
  
  def index
    family_ids = current_user.user_families.map(&:id)
    @score_badges = Score.any_of({:family_id.in=>family_ids}, {:member_id => current_user.id,:activity_type.in => ['User','Invitation']}).paginate(:page => (params[:page]), :per_page => 5)
    @score_badges_sum = Score.where(:family_id.in=>family_ids).sum(:point) + current_user.scores.where(:activity_type.in => ['User','Invitation']).sum(:point)
    @badge_name = Score::badge(@score_badges_sum)
    respond_to do |format|
      format.js { render layout: false }
    end
  end

end
