class NotificationsController < ApplicationController

  def view
  	notification = Notification.where(id: params[:id]).first
  	notification.update_attribute(:checked, true) unless notification.checked?
  	redirect_to notification.link || dashboard_path
  end

  def check_all
  	current_user.notifications.unread.update_all(checked: true)
  	redirect_to dashboard_path
  end	

end	