if milestone.pictures.present?
	json.pictures milestone.pictures do |picture|
		json.picture picture.attributes.merge(url_small: picture.complete_image_url(:small),url_large: picture.complete_image_url(:medium), url_original: picture.complete_image_url(:original), type: 'non_default',aws_small_url: picture.aws_url(:small),aws_original_url: picture.aws_url)
	end
elsif milestone.system_milestone.default_image_exists?
	pictures = [default_image_url(milestone)]
	json.pictures pictures do |picture|
	  json.picture do
	  	json._id 'default_1'
		json.url_small default_small_image_url(milestone)
		json.url_large default_image_url(milestone)
		json.url_original default_image_url(milestone)
		json.type 'default'
	  end
	end	
end