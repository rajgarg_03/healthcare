if @errors.blank?
  json.timelines(@timelines) do |timeline|
		json.timeline timeline
		if timeline.pictures.present?
		  json.pictures timeline.pictures.each do |picture|
				json.picture picture.attributes.merge(url_small: picture.complete_image_url(:small), url_large: picture.complete_image_url(:medium), url_original: picture.complete_image_url(:original), type: 'non_default')
			end	
		elsif (timeline.system_entry == "true" rescue false)
		  system_timeline = SystemTimeline.where(id: timeline.timelined_id).first
		  if system_timeline && system_timeline.default_image_exists?
		    json.pictures [0].each do        
		      picture = {}
		      json.picture picture.merge(_id: 'default_1',:url_small => default_small_image_url(timeline), :url_large =>default_image_url(timeline), :url_original =>default_image_url(timeline), type: 'default')
		    end
		  end
		elsif timeline.timelined && timeline.timelined.is_a?(Milestone)
	      json.partial! 'milestone_picture', locals: { milestone: timeline.timelined }         
		end
	end
	json.status StatusCode::Status200
  json.message @message
else
	json.errors @errors
	json.status StatusCode::Status100
  json.message @message
end	
#json.pic_invalid @pic_invalid
