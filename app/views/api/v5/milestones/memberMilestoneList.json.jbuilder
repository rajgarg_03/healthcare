options = {:group_by=>"child_age"}
options[:important_milestones] = params[:important_milestones] 
 
if @show_subscription_box_status
  milestones = @member_data
  milestone_due = Milestone.milestone_due(current_user,@member,options)
else 
  if params[:filter_by].present?
    milestones = []
    options[:current_user] = current_user
    milestone_due = nil
    overdue_milestones = nil
    milestones = Milestone.milestones_by_order(@member, params[:filter_by],options)
  elsif params[:filter_list].present?
    options[:filter_list] = params[:filter_list]
    milestones,overdue_milestones = Milestone.accomplished_milestone_list(current_user,@member,options)
    milestone_due = Milestone.milestone_due(current_user,@member,options)
  else
    milestones,overdue_milestones = Milestone.accomplished_milestone_list(current_user,@member,options)
    milestone_due = Milestone.milestone_due(current_user,@member,options)
  
  end
end
json.milestone_list milestones
json.key_order milestones.keys  
json.milestone_due milestone_due
json.default_range @category
json.don_not_tell_message "Accurate date of milestone is not available"
json.behind_message "Milestone achieved is little behind"
json.on_track_message "Milestone achieved in time"
json.ahead_message "Milestone achieved in advance"
json.status 200
json.system_tool_id @tool.id
json.show_subscription_box @show_subscription_box_status
json.free_trial_end_date_message @info_data[:free_trial_end_date_message] 
json.subscription_box_data @info_data[:subscription_box_data]
json.filter_list ["Important",  "Overdue", "Accomplished", "Estimated"]
unless  params[:filter_by].present?
  json.milestone_category_stats  @milestone_category_stats
  json.current_age_progress do
    json.text @milestone_graph_title_text
    json.highlight "current age"
  end
  json.graph do
    json.system_milestones_count @system_milestones_count
    json.added_milestones_count @added_milestones_count
    json.achieved_milestones_pecentage @achieved_percentage 
    json.expected_percentage @estimated_percentage #@uncomplete_percentage
  end
end
