json.milestone @milestone.attributes.merge(picture_saved: @picture_saved)
json.pictures @milestone.pictures do |picture|
	json.picture picture.attributes.merge(url_small: picture.photo_url_str(:small), url_large: picture.photo_url_str(:medium), url_original: picture.photo_url_str(:original),complete_url_small: picture.complete_image_url(:small), complete_url_large: picture.complete_image_url(:medium), complete_url_original: picture.complete_image_url(:original), aws_small_url: picture.aws_url(:small), aws_original_url: picture.aws_url(:medium), aws_original_url: picture.aws_url )
end
json.message @message
json.status @status