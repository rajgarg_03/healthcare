		milestone_attributes = @milestone.attributes
        system_milestone = @milestone.system_milestone
        member = @milestone.member
        is_male = member.male?
		milestone_attributes["title"] = system_milestone.get_dynamic_title(member,is_male)
		milestone_attributes["description"] = milestone_attributes["description"].present? ? milestone_attributes["description"] : system_milestone.description
		
		milestone_attributes["milestone_type"] = system_milestone.milestone_type
		milestone_attributes["milestone_category"] = system_milestone.category
        milestone_pictures = []
		if @milestone.pictures.present?
			@milestone.pictures.each do |picture|
				milestone_pictures << {url_small: picture.complete_image_url(:small),url_large: picture.complete_image_url(:medium),url_original: picture.complete_image_url(:original), type: 'non_default',aws_small_url: picture.aws_url(:small), aws_large_url: picture.aws_url(:large)}
			end
		elsif @milestone.system_milestone.default_image_exists?
			pictures = [default_image_url(@milestone)]
			pictures.each do |picture|
			  milestone_pictures <<	{_id: 'default_1',
				url_small: default_small_image_url(@milestone),
				url_large: default_image_url(@milestone),
				url_original: default_image_url(@milestone)}
			end	
		end
		milestone_accomplished_status = @milestone.milestone_accomplished_status(system_milestone,@milestone.member)
        milestone_attributes["milestone_accomplished_status"] = milestone_accomplished_status
 
		milestone_attributes["pictures"] = milestone_pictures
		json.milestone milestone_attributes
		json.status StatusCode::Status200
		 