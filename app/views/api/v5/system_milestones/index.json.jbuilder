json.status StatusCode::Status200
json.member_category @category
json.system_milestones @system_milestones.group_by(&:category) do |system_milestone_grp|
	json.category system_milestone_grp[0] 
	system_milestones = system_milestone_grp[1] 

	json.system_milestones(system_milestones) do |system_milestone|
		 json.partial! '/api/v1/shared/system_milestones', system_milestone: system_milestone, child_member: @member
         if SystemMilestone::CATEGORIES_ORDER[@category] < SystemMilestone::CATEGORIES_ORDER[system_milestone_grp[0]]
           json.milestone_status "unknown"
           json.milestone_progress_status "can't tell"
           json.status_by "system"

         end
		if milestone = @member.milestones.where(system_milestone_id: system_milestone.id).first
			json.added true
			json.milestone_progress_status = milestone.milestone_accomplished_status(system_milestone,@member)
            json.milestone_status  milestone.milestone_status
			json.status_by milestone.status_by
			json.milestone_date milestone.date if milestone.date.present? && milestone.status_by != "system"
		end		
		#json.system_milestone system_milestone.attributes.merge(added: @member.milestones.map(&:system_milestone).include?(system_milestone))
	end	
end

#milestone_accomplished_status
#milestone_progress_status
#accomplished_by