json.status 200
if @show_subscription_box_status
  json.documents @documents 
else
  json.documents @documents do |document|
    json.document document.attributes.merge(file_path: document.complete_file_url, aws_url:document.aws_url)
  end
end
json.system_tool_id @tool.id
json.show_subscription_box @show_subscription_box_status
json.free_trial_end_date_message @info_data[:free_trial_end_date_message]
json.subscription_box_data @info_data[:subscription_box_data]
json.reauthentication_required @reauthentication_required
