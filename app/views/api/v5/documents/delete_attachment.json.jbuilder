if @document.errors.blank?
  json.document @document.attributes.merge(message: Message::API[:success][:delete_doc_attach], status: StatusCode::Status200)
else
  json.message Message::API[:error][:delete_doc_attach]
  json.status StatusCode::Status100
  json.errors @document.errors
end