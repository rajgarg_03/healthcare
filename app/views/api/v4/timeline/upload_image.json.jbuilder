json.message Message::API[:success][:upload_image]
json.status StatusCode::Status200
json.timeline @timeline.attributes.merge(picture_saved: @picture_saved)
if @timeline.pictures.present?
  json.pictures @timeline.pictures.each do |picture|
	json.picture picture.attributes.merge(url_small: picture.complete_image_url(:small), url_large: picture.complete_image_url(:medium), url_original: picture.complete_image_url(:original), type: 'non_default')
  end
elsif (@timeline.system_entry == "true" rescue false)
  system_timeline = SystemTimeline.where(id: @timeline.timelined_id).first
  if system_timeline && system_timeline.default_image_exists?
    json.pictures [0].each do        
      picture = {}
      json.picture picture.merge(_id: 'default_1',:url_small => default_small_image_url(@timeline), :url_large =>default_image_url(@timeline), :url_original =>default_image_url(@timeline), type: 'default')
    end
  end       
end