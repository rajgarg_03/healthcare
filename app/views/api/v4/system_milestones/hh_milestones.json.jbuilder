json.status StatusCode::Status200
json.system_milestones(@system_milestones) do |system_milestone|
	json.partial! '/api/v1/shared/system_milestones', system_milestone: system_milestone, child_member: @member	
	if milestone = @member.milestones.where(system_milestone_id: system_milestone.id).first
		json.added true
		json.milestone_date milestone.date if milestone.date.present?
	else
	  	json.expected_date system_milestone.get_expected_date(@member)	
	end		
end