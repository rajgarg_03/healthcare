json.status @status
if @error
  json.error @error
else  
  json.prenatal_test @medical_events.each do |medical_event|
    json._id medical_event.id
    json.name medical_event.name
    json.desc medical_event.desc
    json.opted medical_event.opted
    json.category medical_event.category
    json.status medical_event.status
    json.est_due_time medical_event.est_due_time
    json.note medical_event.note
    json.component medical_event.component
    json.member_id medical_event.member_id

    if medical_event.pictures.present?
      json.pictures medical_event.pictures.each do |picture|
        json.picture picture.attributes.merge(url_small: picture.complete_image_url(:small), url_large: picture.complete_image_url(:medium), url_original: picture.complete_image_url(:original), type: 'non_default', aws_small_url: picture.aws_url(:small), aws_original_url: picture.aws_url)
      end
    end
  end
end  