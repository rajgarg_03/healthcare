json.status 200
if @show_subscription_box_status
  json.documents @documents 
else
  json.documents @documents do |document|
    json.document document.attributes.merge(file_path: document.complete_file_url, aws_url:document.aws_url)
  end
end
json.system_tool_id @tool.id
json.show_subscription_box @show_subscription_box_status