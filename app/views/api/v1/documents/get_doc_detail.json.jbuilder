if @document.present?
	json.document @document.attributes.merge(file_path: @document.complete_file_url, aws_url: @document.aws_url, message: "Document found")
else
	json.message "Document not found"
end	