if @document.errors.blank?
  json.document @document
  json.message Message::API[:success][:delete_document]
  json.deleted true
else
  json.message Message::API[:error][:delete_document]
  json.errors @document.errors
end