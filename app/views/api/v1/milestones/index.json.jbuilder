json.all_milestones Milestone.order_by_date(@member.milestones).group_by{|m| m.date ? m.date.strftime("%b %Y") : 'No date'} do |milestones_group|
	json.month milestones_group[0]
	json.milestones(milestones_group[1]) do |milestone|
		json.milestone milestone.attributes
		json.system_milestone do
			json.partial! '/api/v1/shared/system_milestones', system_milestone: milestone.system_milestone, child_member: @member
		end	


		if milestone.pictures.present?
			json.pictures milestone.pictures do |picture|
				json.picture picture.attributes.merge(url_small: picture.complete_image_url(:small),url_large: picture.complete_image_url(:medium),url_original: picture.complete_image_url(:original), aws_small_url: picture.aws_url(:small), aws_original_url: picture.aws_url, type: 'non_default')
			end
		elsif milestone.system_milestone.default_image_exists?
			pictures = [default_image_url(milestone)]
			json.pictures pictures do |picture|
			  json.picture do
			  	json._id 'default_1'
				json.url_small default_small_image_url(milestone)
				json.url_large default_image_url(milestone)
				json.url_original default_image_url(milestone)
				json.type 'default'
			  end
			end	
		end
	end
end
json.default_range @category
json.remommended_pointers @rec_pointer
json.status 200
json.graph do
	json.system_milestones_count @system_milestones_count
	json.added_milestones_count @added_milestones_count
	json.percentage @percentage
end