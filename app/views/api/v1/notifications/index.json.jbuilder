json.notifications @notifications do |notification|
	#json.id notification.id	
	#json.title notification.title
	#json.link notification.link
	#json.member_id notification.member_id
	#json.sender_id notification.sender_id
	#json.checked notification.checked
	#json.created_at notification.created_at
	#json.updated_at notification.updated_at
	if notification.type != "family" && notification["child_member_id"].present?
      member = Member.where(id:notification["child_member_id"].to_s).first
    else
      member = notification.sender
    end
	if member && member.profile_image
		json.notification notification.attributes.merge(pic_url_small: member.profile_image.complete_image_url(:small), pic_url_large: member.profile_image.complete_image_url(:medium), pic_aws_small_url: member.profile_image.aws_url(:small), pic_aws_large_url: member.profile_image.aws_url(:large))
	else
		json.notification notification.attributes
	end

end
json.status StatusCode::Status200
json.total_pages @notifications.is_a?(WillPaginate::Collection) ? @notifications.total_pages : (@notifications.count/10.0).ceil
@notifications.select{|n| n.checked==false}.map{|n| n.update_attribute(:checked,true)}