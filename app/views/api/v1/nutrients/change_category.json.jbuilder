json.status StatusCode::Status200
json.array! @recommended_nutrients do |nutrient|
  #nutrient.attributes
  json.id nutrient.id
  json.created_at nutrient.created_at
  json.updated_at nutrient.updated_at
  json.description @member.is_expected? ? nutrient.pregnancy_description : nutrient.description
  json.title nutrient.title
  json.identifier nutrient.get_identifier(@member)
  json.categories nutrient.categories
  json.recommended nutrient.recommended
  json.position nutrient.position
  json.tool_category nutrient.tool_category
  json.tool_for nutrient.tool_for
  json.launch_status nutrient.launch_status
  json.activated @member_nutrients.include?(nutrient)
end

#json.array! @recommended_nutrients
