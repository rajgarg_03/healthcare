json.status StatusCode::Status200
json.categories (["All"] + @tools)
json.tools @recommended_nutrients do |nutrient|
  #nutrient.attributes
  json.id nutrient.id
  json.created_at nutrient.created_at
  json.updated_at nutrient.updated_at
  json.description @member.is_expected? ? nutrient.pregnancy_description : nutrient.description
  json.title nutrient.title
  json.identifier nutrient.get_identifier(@member,{:api_version=>3})
  json.categories nutrient.categories
  json.recommended nutrient.recommended
  json.launch_status nutrient.launch_status
  json.position nutrient.position
  json.tool_category nutrient.tool_category
  json.tool_for nutrient.tool_for
  json.activated @member_nutrients.include?(nutrient)
end

#json.array! @recommended_nutrients
