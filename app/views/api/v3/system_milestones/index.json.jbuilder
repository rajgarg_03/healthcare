json.status StatusCode::Status200
json.system_milestones @system_milestones.group_by(&:category) do |system_milestone_grp|
	json.category system_milestone_grp[0] 
	system_milestones = system_milestone_grp[1] 

	json.system_milestones(system_milestones) do |system_milestone|
		 json.partial! '/api/v1/shared/system_milestones', system_milestone: system_milestone, child_member: @member

		if milestone = @member.milestones.where(system_milestone_id: system_milestone.id).first
			json.added true
			json.milestone_date milestone.date if milestone.date.present?
		end		
		#json.system_milestone system_milestone.attributes.merge(added: @member.milestones.map(&:system_milestone).include?(system_milestone))
	end	
end