json.status 200
json.documents @documents do |document|
	json.document document.attributes.merge(file_path: document.complete_file_url, aws_url:document.aws_url)
end