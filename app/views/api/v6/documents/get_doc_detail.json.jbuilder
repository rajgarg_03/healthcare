if @document.present?
	json.status 200
	json.document @document.attributes.merge(file_path: @document.complete_file_url(:original,api_version = 6), aws_url: @document.aws_url(:original,api_version = 6), message: "Document found")
else
	json.status 100
	json.message "Document not found"
end	