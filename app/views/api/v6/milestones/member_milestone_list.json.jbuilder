 
if @show_subscription_box_status
  all_milestones = @member_data
else 
  all_milestones = []
Milestone.milestones_by_order(@member, params[:filter_by]).each  do |milestone|
  milestone_attributes = milestone.attributes
  system_milestone = milestone.system_milestone
  milestone_attributes["milestone_accomplished_at"] = milestone.milestone_accomplished_at_text(@member)
  milestone_attributes["description"] = milestone_attributes["description"].present? ? milestone_attributes["description"] : system_milestone.description
  milestone_attributes["title"] = milestone.milestone_status == "accomplished" ? system_milestone.get_dynamic_title(@member,@child_male) : system_milestone.get_dynamic_title(@member,@child_male).gsub(@member.first_name, "Child")
  milestone_attributes["milestone_type"] = system_milestone.milestone_type
  milestone_attributes["milestone_category"] = system_milestone.category
  milestone_attributes["age"] = milestone.milestone_added_at_child_age(@member)
  milestone_data = milestone_attributes
  milestone_pictures = []
  milestone_accomplished_status = milestone.milestone_accomplished_status(system_milestone,@member)
  milestone_data["milestone_progress_status"] = milestone_accomplished_status
  
  milestone_data["is_progress_bar_enabled"] = milestone.is_progress_bar_enabled?
  # milestone_data["seleted_progress_bar"] = milestone.seleted_progress_bar(milestone_accomplished_status)
  milestone_data["progress_score"] = milestone.progress_score(system_milestone,@member)
  milestone_data["progress_text"] = milestone.progress_text(milestone_data["progress_score"])
  milestone_data["is_important"] = system_milestone.is_important?
  milestone_data["date"] = nil if (milestone.status_by == "system" && milestone.milestone_status == "accomplished")
  # milestone_data["accomplished_by"] = milestone.status_by
  if milestone.pictures.present?
    milestone.pictures.each do |picture|
      milestone_pictures  << {_id:picture.id, url_small: picture.complete_image_url(:small,api_version=6),url_large: picture.complete_image_url(:medium,api_version=6),url_original: picture.complete_image_url(:original,api_version=6), aws_small_url: picture.aws_url(:small,api_version=6), aws_original_url: picture.aws_url(:original,api_version=6), type: 'non_default'}
    end
  elsif system_milestone.default_image_exists?
    system_milestone_pictures = [default_image_url(milestone,api_version=6)]
    system_milestone_pictures.each do |picture|
      system_milstone_picture =  {
        _id: 'default_1',
        url_small: default_small_image_url(milestone,api_version=6),
        url_large: default_image_url(milestone,api_version=6),
        url_original: default_image_url(milestone,api_version=6),
        type: 'default'}
        milestone_pictures << system_milstone_picture
    end
  end
  milestone_attributes["pictures"] = milestone_pictures
  all_milestones << milestone_attributes
end
end
json.all_milestones all_milestones  
json.default_range @category
#json.recommended_pointers @recommended_pointer
json.don_not_tell_message "Accurate date of milestone is not available"
json.behind_message "Milestone achieved is little behind"
json.on_track_message "Milestone achieved in time"
json.ahead_message "Milestone achieved in advance"
json.status 200
json.system_tool_id @tool.id
json.show_subscription_box @show_subscription_box_status
json.free_trial_end_date_message @info_data[:free_trial_end_date_message] 
json.subscription_box_data @info_data[:subscription_box_data]
unless  params[:filter_by].present?
  json.milestone_category_stats  @milestone_category_stats
  json.graph do
    json.system_milestones_count @system_milestones_count
    json.added_milestones_count @added_milestones_count
    json.achieved_milestones_pecentage @achieved_percentage 
    json.expected_percentage @estimated_percentage #@uncomplete_percentage
  end
end
