json.milestone @milestone.attributes.merge(picture_saved: @picture_saved)
json.pictures @milestone.pictures do |picture|
	json.picture picture.attributes.merge(url_small: picture.photo_url_str(:small,api_version=6), url_large: picture.photo_url_str(:medium,api_version=6), url_original: picture.photo_url_str(:original,api_version=6),complete_url_small: picture.complete_image_url(:small,api_version=6), complete_url_large: picture.complete_image_url(:medium,api_version=6), complete_url_original: picture.complete_image_url(:original,api_version=6), aws_small_url: picture.aws_url(:small,api_version=6), aws_original_url: picture.aws_url(:medium,api_version=6), aws_original_url: picture.aws_url(:original,api_version=6) )
end
json.message @message
json.status @status