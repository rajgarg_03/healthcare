if @show_subscription_box_status == false
  json.all_milestones Milestone.order_by_date(@member.milestones).group_by{|m| m.date ? m.date.strftime("%b %Y") : 'No date'} do |milestones_group|
    json.month milestones_group[0]
    json.milestones(milestones_group[1]) do |milestone|
      milestone_attributes = milestone.attributes
      system_milestone = milestone.system_milestone
      milestone_attributes["description"] = milestone_attributes["description"].present? ? milestone_attributes["description"] : system_milestone.description
      milestone_attributes["title"] = system_milestone.get_dynamic_title(@member,@child_male)
      json.milestone milestone_attributes

      json.system_milestone do
        json.partial! '/api/v1/shared/system_milestones', system_milestone: system_milestone, child_member: @member
      end 


      if milestone.pictures.present?
        json.pictures milestone.pictures do |picture|
          json.picture picture.attributes.merge(url_small: picture.complete_image_url(:small,api_version=6),url_large: picture.complete_image_url(:medium,api_version=6),url_original: picture.complete_image_url(:original,api_version=6), aws_small_url: picture.aws_url(:small,api_version=6), aws_original_url: picture.aws_url(:original,api_version=6), type: 'non_default')
        end
      elsif system_milestone.default_image_exists?
        pictures = [default_image_url(milestone,api_version=6)]
        json.pictures pictures do |picture|
          json.picture do
            json._id 'default_1'
          json.url_small default_small_image_url(milestone,api_version=6)
          json.url_large default_image_url(milestone,api_version=6)
          json.url_original default_image_url(milestone,api_version=6)
          json.type 'default'
          end
        end 
      end
    end
  end
else
  json.all_milestones @member_data
end
json.system_tool_id @tool.id
json.show_subscription_box @show_subscription_box_status
json.default_range @category
json.free_trial_end_date_message @info_data[:free_trial_end_date_message]
json.subscription_box_data @info_data[:subscription_box_data]
#json.remommended_pointers @recommended_pointer
json.status 200
json.graph do
  json.system_milestones_count @system_milestones_count
  json.added_milestones_count @added_milestones_count
  json.percentage @percentage
end