		system_milestone = @milestone.system_milestone
		milestone_attributes = @milestone.attributes
		member = @milestone.member
		is_male = member.male?
		milestone_attributes["title"] = system_milestone.get_dynamic_title(member,is_male)

		json.milestone milestone_attributes
		json.status StatusCode::Status200
		json.system_milestone do
			json.partial! '/api/v1/shared/system_milestones', system_milestone: system_milestone, child_member: @milestone.member
		end	

		if @milestone.pictures.present?
			json.pictures @milestone.pictures do |picture|
				json.picture picture.attributes.merge(url_small: picture.complete_image_url(:small,api_version=6),url_large: picture.complete_image_url(:medium,api_version=6),url_original: picture.complete_image_url(:original,api_version=6), type: 'non_default',aws_small_url: picture.aws_url(:small,api_version=6), aws_large_url: picture.aws_url(:large,api_version=6))
			end
		elsif @milestone.system_milestone.default_image_exists?
			pictures = [default_image_url(@milestone,api_version=6)]
			json.pictures pictures do |picture|
			  json.picture do
			  	json._id 'default_1'
				json.url_small default_small_image_url(@milestone,api_version=6)
				json.url_large default_image_url(@milestone,api_version=6)
				json.url_original default_image_url(@milestone,api_version=6)
			  end
			end	
		end