if @timeline.errors.blank?
	#json.timeline @timeline
	json.status StatusCode::Status200
    json.message Message::API[:success][:delete_timeline]
else
	json.errors @timeline.errors
    json.message Message::API[:error][:delete_timeline]
	json.status StatusCode::Status100
end