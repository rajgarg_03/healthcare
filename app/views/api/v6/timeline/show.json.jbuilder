json.timeline @timeline
json.status StatusCode::Status200
#json.pictures @timeline.pictures do |picture|
#	json.picture picture.attributes.merge(url_small: picture.photo_url_str(:small,api_version=6), url_large: picture.photo_url_str(:medium,api_version=6))
#end

if @timeline.pictures.present?
  json.pictures @timeline.pictures.each do |picture|
	json.picture picture.attributes.merge(url_small: picture.complete_image_url(:small,api_version=6), url_large: picture.complete_image_url(:medium,api_version=6), url_original: picture.complete_image_url(:original,api_version=6), type: 'non_default')
  end
elsif (@timeline.system_entry == "true" rescue false)
  system_timeline = SystemTimeline.where(id: @timeline.timelined_id).first
  if system_timeline && system_timeline.default_image_exists?
    json.pictures [0].each do        
      picture = {}
      json.picture picture.merge(_id: 'default_1',:url_small => default_small_image_url(@timeline,api_version=6), :url_large =>default_image_url(@timeline,api_version=6), :url_original =>default_image_url(@timeline,api_version=6), type: 'default')
    end
  end       
elsif @timeline.timelined && @timeline.timelined.is_a?(Milestone)
  json.partial! 'milestone_picture', locals: { milestone: @timeline.timelined }  
end