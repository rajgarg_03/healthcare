if milestone.pictures.present?
	json.pictures milestone.pictures do |picture|
		json.picture picture.attributes.merge(url_small: picture.complete_image_url(:small,api_version=6),url_large: picture.complete_image_url(:medium,api_version=6), url_original: picture.complete_image_url(:original,api_version=6), type: 'non_default',aws_small_url: picture.aws_url(:small,api_version=6),aws_original_url: picture.aws_url(:original,api_version=6) )
	end
elsif milestone.system_milestone.default_image_exists?
	pictures = [default_image_url(milestone,api_version=6)]
	json.pictures pictures do |picture|
	  json.picture do
	  	json._id 'default_1'
		json.url_small default_small_image_url(milestone,api_version=6)
		json.url_large default_image_url(milestone,api_version=6)
		json.url_original default_image_url(milestone,api_version=6)
		json.type 'default'
	  end
	end	
end