round_off_val = round_off_val = (params[:decimalroundoff]|| 1).to_i rescue 1
json.timelines @timelines.group_by {|d| d.timeline_at.strftime("%b %Y") } do |created_at, data| 
json.month created_at
user_metric_system = current_user.metric_system
json.data data.each do |timeline|
    @timeline_member = timeline.member
    timeline.timelined_type = "Timeline" if timeline.timelined_type.blank?
    timeline.entry_from = "timeline" if timeline.entry_from.blank?

    timeline["post_title"] = "Post from #{@timeline_member.first_name}'s " +  (Timeline::PostTitle[timeline.timelined_type] || "Timeline")
    if timeline.timelined_type == "MedicalEvent"
      timeline["category"] = timeline.category rescue "nil"
    end
    if timeline.entry_from == "health" || timeline.timelined_type == "Health"
      health_unit = Health.health_unit(user_metric_system) 
      tmp = Hash[*(eval(timeline.desc))]
      tmp.each do |k,v|
       tmp[k] = ((tmp[k].to_f * Health::UNIT_VAL[health_unit[k]]) rescue nil).round_to(round_off_val) 
      end
      tmp["bmi"] = timeline.name.to_f.round_to(round_off_val)
      tmp["head_circum"] = tmp["head_circum"] || (timeline.timelined.head_circum.to_f * Health::UNIT_VAL[health_unit["head_circum"]] rescue nil).to_f.round_to(round_off_val)
      timeline.desc = tmp
      timeline.name = timeline.name.to_f.round_to(round_off_val)
    end
    json.timeline timeline.timeline_attributes
    
    if timeline.pictures.present?
      json.pictures timeline.pictures.each do |picture|
        json.picture picture.attributes.merge(url_small: picture.complete_image_url(:small,api_version=6), url_large: picture.complete_image_url(:medium,api_version=6), url_original: picture.complete_image_url(:original,api_version=6), type: 'non_default', aws_small_url: picture.aws_url(:small,api_version=6), aws_original_url: picture.aws_url(:original,api_version=6))
      end    
    elsif (timeline.system_entry == "true" rescue false)
      system_timeline = SystemTimeline.where(id: timeline.timelined_id).first
      if system_timeline && nutrient_default_image_exists?(timeline)
        json.pictures [0].each do        
          picture = {}
          json.picture picture.merge(_id: 'default_1',:url_small => default_small_image_url(timeline,api_version=6), :url_large =>default_image_url(timeline,api_version=6), :url_original =>default_image_url(timeline,api_version=6), type: 'default')
        end
      end
    elsif timeline.timelined && timeline.timelined.is_a?(Milestone)
      json.partial! 'milestone_picture', locals: { milestone: timeline.timelined } 
    elsif timeline.timelined && timeline.timelined.is_a?(MedicalEvent)
      json.pictures timeline.timelined.pictures do |picture|
        json.picture picture.attributes.merge(url_small: picture.complete_image_url(:small,api_version=6),url_large: picture.complete_image_url(:medium,api_version=6), url_original: picture.complete_image_url(:original,api_version=6), type: 'non_default',aws_small_url: picture.aws_url(:small,api_version=6),aws_original_url: picture.aws_url(:original,api_version=6))
      end
    end

  end
end
json.total_pages @total_page
json.current_page @current_page
json.next_page @next_page
json.status @status
json.erorr @error