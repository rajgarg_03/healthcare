json.notifications @notifications do |notification|
	 
	if notification.type != "family" && notification["child_member_id"].present?
      member = Member.where(id:notification["child_member_id"].to_s).first
    else
      member = notification.sender
    end
	if member && member.profile_image
		json.notification notification.attributes.merge(deeplink_params:notification.get_deep_link_data(current_user,@api_version), pic_url_small: member.profile_image.complete_image_url(:small,api_version=6), pic_url_large: member.profile_image.complete_image_url(:medium,api_version=6), pic_aws_small_url: member.profile_image.aws_url(:small,api_version=6), pic_aws_large_url: member.profile_image.aws_url(:large,api_version=6))
	else
		json.notification notification.attributes.merge(deeplink_params:notification.get_deep_link_data(current_user,@api_version))
	end
    

end
json.status StatusCode::Status200
json.total_pages @notifications.is_a?(WillPaginate::Collection) ? @notifications.total_pages : (@notifications.count/10.0).ceil
@notifications.select{|n| n.checked==false}.map{|n| n.update_attribute(:checked,true)}