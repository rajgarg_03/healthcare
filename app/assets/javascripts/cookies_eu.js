// Creare's 'Implied Consent' EU Cookie Law Banner v:2.4
// Conceived by Robert Kent, James Bavington & Tom Foyster
 
var dropCookie = true;                      // false disables the Cookie, allowing you to style the banner
var cookieDuration = 365;                    // Number of days before the cookie expires, and the banner reappears
var cookieName = 'complianceCookie';        // Name of our cookie
var cookieValue = 'on';                     // Value of cookie
 
function createDiv(){
    $(".cookie-msg").remove();
    
    var global_msg = document.getElementById("top")
    var div = document.createElement('div');
 //    div.setAttribute('id','cookie-law');
    div.setAttribute('class','cookie-msg')
 //    div.innerHTML = '<p>Our website uses cookies. By continuing we assume your permission to deploy cookies, as detailed in our <a href="/privacy-cookies-policy/" rel="nofollow" title="Privacy &amp; Cookies Policy">privacy and cookies policy</a>. <a class="close-cookie-banner" href="javascript:void(0);" onclick="removeMe();"><span>X</span></a></p>';    
 // // Be advised the Close Banner 'X' link requires jQuery
     
    // bodytag.appendChild(div); // Adds the Cookie Law Banner just before the closing </body> tag
 //    // or
     $(global_msg).after(div); // Adds the Cookie Law Banner just after the opening <body> tag
     
    // document.getElementsByTagName('body')[0].className+=' cookiebanner'; //Adds a class tothe <body> tag when the banner is visible
 // $(".cookie-msg").html('<p>Our website uses cookies. By continuing we assume your permission to use cookies, as detailed in our <a href="/home/privacy/" style="color:#cdcdcd !important;" rel="nofollow" title="Privacy &amp; Cookies Policy">privacy and cookies policy</a>. <a class="close-cookie-banner" href="javascript:void(0);" onclick="removeMe();"><b>Accept</b></a></p>')    
    $(".cookie-msg").html('<p>Our website uses cookies. By continuing we assume your permission to use cookies, as detailed in our <a href="/home/privacy/" rel="nofollow" title="Privacy &amp; Cookies Policy">privacy and cookies policy</a>&nbsp;</p><a class="close-cookie-banner js-accept" href="javascript:void(0);" onclick="removeMe();"><b><span class="">Accept</span></b></a>&nbsp;&nbsp;&nbsp;<a class="close-cookie-banner js-deny" style="background-color:#a0937c;" href="javascript:void(0);" onclick="denyCookie();"><b><span>Deny</span></b></a>') ;
    if(checkCookie(window.cookieName) == "on"){
      $(".js-accept").hide();
      $(".js-deny").show();
    }
    else if(checkCookie('cookie_deny') == "on"){
      // $(".js-deny").css("color","cadetblue")
      $(".js-accept").show();
      $(".js-deny").hide();

    }
    else{

    } 
     
 // $(".cookie-msg").html('<span><p>Our website uses cookies. By continuing we assume your permission to use cookies, as detailed in our <a href="/home/privacy/" rel="nofollow" title="Privacy &amp; Cookies Policy">privacy and cookies policy</a>. <a class="close-cookie-banner" href="javascript:void(0);" onclick="removeMe();"><b>Accept</b></a></p></span>')    
 $(".cookie-msg").show();
    // createCookie(window.cookieName,window.cookieValue, window.cookieDuration); // Create the cookie
}
 
 
function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000)); 
        var expires = "; expires="+date.toGMTString(); 
    }
    else var expires = "";
    if(window.dropCookie) { 
        document.cookie = name+"="+value+expires+"; path=/"; 
    }
}
 
function checkCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
 
function eraseCookie(name) {
    createCookie(name,"",-1);
}
 
window.onload = function(){
    if(checkCookie(window.cookieName) != window.cookieValue &&  (checkCookie('cookie_deny') != "on") ){
        createDiv(); 
    }
}

function removeMe(){
  // var element = document.getElementById('cookie-law');
  // element.parentNode.removeChild(element);
    createCookie(window.cookieName,window.cookieValue, window.cookieDuration); // Create the cookie
    eraseCookie("cookie_deny")
    $(".cookie-msg").html('');
    $(".cookie-msg").hide();
    window.location.reload();
}
  function deleteCookies() { 
    console.log('cookies getting deleted')
    var allCookies = document.cookie.split(';');
    try{
        cookie_list = $.cookie()
        $.each(cookie_list,function(cookie_name,cookie_val){
          console.log("deleting cookie" + cookie_name)
          $.removeCookie(cookie_name)
        })
    }
    catch(exec){

    }
    allCookies = document.cookie.split(';');

    for (var i = 0; i < allCookies.length; i++){
      document.cookie = allCookies[i] + "=;Expires=Thu, 01 Jan 1970 00:00:01 GMT"
      document.cookie = allCookies[i] + '=; Path=/; Domain=.'+window.location.host.replace('www.','')+'; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
      document.cookie = allCookies[i] + '=; Path=/; Domain=.'+window.location.host+'; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
      document.cookie = allCookies[i] + '=; Path=/; Domain='+window.location.host.replace('www.','')+'; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }
  } 

function denyCookie(){
    // eraseCookie(window.cookieName); // Create the cookie
    $(".cookie-msg").html('');
    $(".cookie-msg").hide();
    deleteCookies();
    createCookie("cookie_deny","on",365)
    window.location.reload();
}


