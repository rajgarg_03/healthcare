//  (function($) {
(function($,window,undefined){
$.fn.close = function(){
  $("#overLay").remove();
  $(".popupWrapper").remove();
  $("body").css({'overflow': 'auto','padding-right':'0px'});
},

$.fn.popclick = (function (options) {
  var settings = $.extend({}, $.fn.funboxPop.defaults, options);
      var options = options|| {};
      var dataId = "#html";
      var dataPopup = options.pop;
      var dataWidth = options.width;
      var dataHeight = options.height;
      var dataOverflow = options.overflow;
      var popupStatus = 0;
      dataId == undefined ? "aa" : '';
      if(popupStatus == 1){
        $this.$disablePopup();
      }

      if (popupStatus == 0) {
        var $backgroundOverLay = $('<div id="overLay"/>');
        $backgroundOverLay.css({
          'display': 'block',
          'position': 'fixed',
          'height': '100%',
          'width': '100%',
          'top': 0, 'left': 0,
          'background-color': 'rgba(0,0,0,0.3)',
          'z-index': 2, 'overflow': 'auto'
        });
        $("body").prepend($backgroundOverLay);

        //Close box
        var closeBox = '<a href="#." onClick="event.preventDefault(); $.fn.close();" class="pop-close"></a>';

        //popup data
        var $popupData = $('<div class="popupWrapper"/>');
        //popup holder
        var $popHolder = $('<div id="popupHolder"/>');
        $popupData.append($popHolder);


        $popupData.css({
          "position": "relative", "margin": "80px auto 50px auto", 'display': 'none', 'background-color': '#FFF', 'z-index': 3
        }).prepend(closeBox);

        if (dataWidth != "" && dataWidth != undefined) {
          $popupData.css({
            'width': dataWidth
          });
        } else {
          $popupData.css({
            'width': "500"
          });
        }
        if (dataHeight != "") {
          $popupData.css({
            'height': dataHeight
          });
        }
        if (dataOverflow != "") {
          $popupData.css({
            'overflow': dataOverflow
          });
        }

        $backgroundOverLay.prepend($popupData);

        if (dataPopup == "inline") {
          var popNewData = $(dataId).html()
          $this.data('popData', popNewData);
          $popHolder.html(popNewData)
          $(dataId).html('empty');
        } else {
          if (dataId == "#html") {
            $popHolder.load(dataPopup,function(){
            $backgroundOverLay.fadeIn();
            $popupData.fadeIn('',function(){
              $(this).find('input[type="text"]').eq( 0 ).focus()
            });
            settings.callBack()
      })
          } else {
            $popHolder.load(dataPopup + " " + dataId)
          }
        }
        
        popupStatus = 1;
      }
}) ;




  $.fn.funboxPop = function (settings) {
    // Pluginifying code...
    settings = $.extend({}, $.fn.funboxPop.defaults, settings);

    //pop public
    $this = $(this);

    var dataPopup = $this.attr('data-pop');
  
    //0 means disabled; 1 means enabled;
    var popupStatus = 0;

    if (dataPopup == "inline") {
      $(dataId).hide();
    }

    //popup call
    $this.click(function (event) {
      el = $(this);
      event.preventDefault();
      var dataId = el.attr('href');
      var dataPopup = el.attr('data-pop');
      var dataWidth = el.attr('data-width');
      var dataHeight = el.attr('data-height');
      var dataOverflow = el.attr('data-overflow');
      
      dataId == undefined ? dataId = el.attr('data-id') : '';
      if(popupStatus == 1){
        $this.$disablePopup();
      }

      if (popupStatus == 0) {
        var $backgroundOverLay = $('<div id="overLay"/>');
        $backgroundOverLay.css({
          'display': 'block',
          'position': 'fixed',
          'height': '100%',
          'width': '100%',
          'top': 0, 'left': 0,
          'background-color': 'rgba(0,0,0,0.3)',
          'z-index': 2, 'overflow': 'hidden'
        });
        $("body").prepend($backgroundOverLay);

        //Close box
        var closeBox = '<a href="#." onClick="event.preventDefault(); $.fn.close();" class="pop-close"></a>';

        //popup data
        var $popupData = $('<div class="popupWrapper"/>');
        //popup holder
        var $popHolder = $('<div id="popupHolder"/>');
        $popupData.append($popHolder);


        $popupData.css({
          "position": "relative", "margin": "80px auto 50px auto", 'display': 'none', 'background-color': '#FFF', 'z-index': 3
        }).prepend(closeBox);

        if (dataWidth != "" && dataWidth != undefined) {
          $popupData.css({
            'width': dataWidth
          });
        } else {
          $popupData.css({
            'width': "500"
          });
        }
        if (dataHeight != "") {
          $popupData.css({
            'height': dataHeight
          });
        }
         if (dataOverflow != "") {
          $popupData.css({
            'overflow': dataOverflow
          });
        }
        $backgroundOverLay.prepend($popupData);

        if (dataPopup == "inline") {
          var popNewData = $(dataId).html()
          $this.data('popData', popNewData);
          $popHolder.html(popNewData)
          $(dataId).html('empty');
        } else {
          if (dataId == "#html") {
            $popHolder.load(dataPopup,function(){
            settings.callBack()
            $backgroundOverLay.fadeIn();
            $popupData.fadeIn('',function(){
              $(this).find('input[type="text"]').eq( 0 ).focus()
            });
      })
          } else {
            $popHolder.load(dataPopup + " " + dataId)
          }
        }        
        popupStatus = 1;
      }

      // .............................................

      // .............................................

      //Disable Popup
      $this.$disablePopup = function () {
        $.fn.close();
      //disables popup only if it is enabled
      if (popupStatus == 1) {
        if (dataPopup == "inline") {
          $(dataId).html($this.data('popData'));
        }
        // $("#overLay").remove();
        // $(".popupWrapper").remove();
        // $("body").css({'overflow': 'auto','padding-right':'0px'});

        popupStatus = 0;
      }
    };
    });

  };

  // function options
  $.fn.funboxPop.defaults = {
    callBack: function(){ /* function come */ }
  };
 $(document).ready(function (e) {
    $('[data-pop]').funboxPop();
  });

})($);
