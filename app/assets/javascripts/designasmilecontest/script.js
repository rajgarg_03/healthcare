/*----------------------------------------------
1. Pre Loader 
2 Reveal Animation
---------------------------------------------[*/ 

/*------------------------------
  1. Pre Loader 
-------------------------------*/
  $(window).load(function() {
    // Animate loader off screen
    $(".se-pre-con").fadeOut("slow");

  });
  

/*---------------------------
2.Reveal Animation
-----------------------------*/ 
$(window).on('load', function(){

  window.sr = ScrollReveal({ mobile: false })

  sr.reveal('h1', {
    duration: 1000,
    origin: 'bottom',
    distance: '10px',
    scale: 1
  });
  sr.reveal('h2', {
    duration: 1000,
    origin: 'bottom',
    distance: '80px',
    scale: 1
  });
  sr.reveal('p', {
    duration: 1000,
    origin: 'bottom',
    delay: 300,
    distance: '10px',
    viewFactor: 0.1,
    scale: 1
  });
  
  sr.reveal('.social_icon li', {
    duration: 1000,
    origin: 'bottom',
    delay: 300,
    distance: '10px',
    viewFactor: 0.1,
    scale: 1
  });


  sr.reveal(' .submit-your-entry', {
    duration: 1000,
    origin: 'top',
    distance: '20px',
    scale: 1
  });
 
  

});

function uploadFile(target) {
  document.getElementById("file-name").innerHTML = target.files[0].name;
  $(".js-placeholder").html("");
}
/*-----------------------

------------------------*/ 

