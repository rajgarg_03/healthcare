//= require highcharts
//= require jquery.dataTables.min
var options = {
    chart: {

        // Edit chart spacing
        spacingBottom: 0,
        spacingTop: 5,
        spacingLeft: 0,
        spacingRight: 0,
        marginLeft:60
    },

    title: {
        text: 'BMI',
        x: -20 //center
    },
    xAxis: {
        title: {
            text: 'Months'
        }
    },
    yAxis: {
        title: {
            text: 'BMI Value'
        },
        startOnTick: false,
        plotLines: [
            {
                value: 0,
                width: -1,
                color: '#fdc065'
            }
        ]
    },
    tooltip: {
        valueSuffix: ''
    },
    // legend: {
    //     layout: 'vertical',
    //     align: 'right',
    //     verticalAlign: 'middle',
    //     borderWidth: 0
    // },
    legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },

    series: [
        {color: '#e3e3e3',
          marker: {
               enabled : false
        }
    },
        {color: '#cccccc',
    marker: {
               enabled : false
        }
    },
        {color: '#b6b6b6',
    marker: {
               enabled : false
        }
    },
        {color: '#9a9a9a',
    marker: {
               enabled : false
        }
    },
        {color: '#fdc065',
        marker: {
             symbol: 'circle'
            }
    
    }
    ]
};

$(function(){
  $("body").on("click",".js-health-link",function(){
    var id = $(this).data("id");
  $("li[data-type='"+ id + "']").click();
  });

  $("body").on("click",".js-health-tab", function(){
    var data_type = $(this).data("type"),
    member_id= $(this).data("member-id");
     $(".js-health-tab").removeClass("active");
    $(this).addClass("active");
    $.ajax({
      url: "/health/get_detail",
      type: "GET",
      data: {data_type: data_type, member_id:member_id},
      dataType: "script"
    });
  });
      
});


