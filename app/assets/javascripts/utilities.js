var whos=null;
var selected_whos=null;
function getplaces(gid,src,selected)
{
	whos = src
	selected_whos = selected;
	$.ajax({
     url: "/home/get_geoname",
     data: {gid: gid},
     success: function (response){
      listPlaces(response)
     }
    });
}

function listPlaces(jData)
{
	counts = jData.geonames.length<jData.totalResultsCount ? jData.geonames.length : jData.totalResultsCount
	// who = document.getElementById(whos)
	var selector = "#"+ whos;
	who = $(selector)[0]|| {};
	who.options = who.options || {}
	who.options.length = 0;
	
	if(counts)who.options[who.options.length] = new Option('Select','')
	else who.options[who.options.length] = new Option('No Data Available','')

	for(var i=0;i<counts;i++){
		var option_value = jData.geonames[i].name+"_"+jData.geonames[i].geonameId;
		var option = new Option(jData.geonames[i].name, option_value);
		if(jData.geonames[i].name == selected_whos)
			option.setAttribute("selected", "")
		who.options[who.options.length] = option;
	}
	delete jData;
	var element = "#"+whos;
	reloadChosen(element);  
	jData = whos = selected_whos = null;
	// aObj.removeScriptTag(); 
	$(element).change(); 
}

function reloadChosen(element){
   $(element+".chosen-select").trigger("chosen:updated");
}
function loadChosen(){
	$(".chosen-select").chosen({no_results_text: "No results matched", search_contains: true});
}
function deleteAct(id, actID, type) {
  console.log(id+' '+actID, +' '+ type);
  // var check_order = document.getElementsByClassName('active')[0].title;
  var confirmed = confirm('Are sure you want to Delete this subject');
  var url="", data="", request_type="GET";

  if(type == "Jab") {
    url = "/immunisation/delete_jab";
    data = {jab_id: actID} ;
  }
  if(type == "Event") {
    url = "/calendar/event_delete";
    data = {event_id: actID} ;
  }
   

  console.log(request_type+' '+url+' '+data);
  if (confirmed == true) {
    $.ajax({
      type: request_type,
      url: url,
      data: data,
        
      success: function () {
//        button(check_order);
      }
    });
  }
  return confirmed;
}


function moveEvent(event, dayDelta, minuteDelta, allDay){
	jQuery.ajax({
		data: 'id=' + event.id + '&title=' + event.title + '&day_delta=' + dayDelta + '&minute_delta=' + minuteDelta + '&all_day=' + allDay,
		dataType: 'script',
		type: 'post',
		url: "/calendar/move"
	});
}

function resizeEvent(event, dayDelta, minuteDelta){
	jQuery.ajax({
		data: 'id=' + event.id + '&title=' + event.title + '&day_delta=' + dayDelta + '&minute_delta=' + minuteDelta,
		dataType: 'script',
		type: 'post',
		url: "/calendar/resize"
	});
	}

function showEventDetails(event){
	$('#event_desc').html(event.description);
	$('#edit_event').html("<a href = 'javascript:void(0);' onclick ='editEvent(" + event.id + ")'>Edit</a>");
	if (event.recurring) {
		title = event.title + "(Recurring)";
		$('#delete_event').html("&nbsp; <a href = 'javascript:void(0);' onclick ='deleteEvent(" + event.id + ", " + false + ")'>Delete Only This Occurrence</a>");
		$('#delete_event').append("&nbsp;&nbsp; <a href = 'javascript:void(0);' onclick ='deleteEvent(" + event.id + ", " + true + ")'>Delete All In Series</a>")
		$('#delete_event').append("&nbsp;&nbsp; <a href = 'javascript:void(0);' onclick ='deleteEvent(" + event.id + ", \"future\")'>Delete All Future Events</a>")
	}
	else {
		title = event.title;
		$('#delete_event').html("<a href = 'javascript:void(0);' onclick ='deleteEvent(" + event.id + ", " + false + ")'>Delete</a>");
	}
	$('#desc_dialog').dialog({
		title: title,
		modal: true,
		width: 500,
		close: function(event, ui){
			$('#desc_dialog').dialog('destroy')
		}
	});
}


function editEvent(event_id){
	jQuery.ajax({
		data: 'id=' + event_id,
		dataType: 'script',
		type: 'get',
		url: "/calendar/edit"
	});
}

function deleteEvent(event_id, delete_all){
	jQuery.ajax({
		data: 'id=' + event_id + '&delete_all='+delete_all,
		dataType: 'script',
		type: 'post',
		url: "/calendar/destroy"
	});
}

function showPeriodAndFrequency(value){
	switch (value) {
		case 'Daily':
			$('#period').html('day');
			$('#frequency').show();
			break;
		case 'Weekly':
			$('#period').html('week');
			$('#frequency').show();
			break;
		case 'Monthly':
			$('#period').html('month');
			$('#frequency').show();
			break;
		case 'Yearly':
			$('#period').html('year');
			$('#frequency').show();
			break;

		default:
			$('#frequency').hide();
	}
}
