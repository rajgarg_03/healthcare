//   $(document).ready(function() {
//     var  url_for_event = $("#js-data-url").val();
//     $('#calendar').fullCalendar({
//       defaultView: 'month',
//       header: {
//         left: '',
//         center: 'prev title next today',
//         right: 'agendaDay,agendaWeek,month'
//       },
//       editable: false,
//       disableDragging: true,
//       events: "/calendar/" + url_for_event,
//       timeFormat: 'h:mm t{ - hh:mm tt} ',
//       dragOpacity: "0.5",
//       dayClick: function(date, allDay, jsEvent, view){
//       },
//       eventClick: function(calEvent, jsEvent, view){
//         view.calendar.changeView("agendaDay");
//         $("#calendar").fullCalendar("gotoDate",calEvent.start);
//         $(".calender-class[title= 'agendaDay']").click()
//       },
//       eventMouseover: function(event){
//         var orignal_event = $(this);
//         if($(this).find(".hoverBtns").length == 0)
//           $(this).find(".fc-event-inner.fc-event-skin").append("<span class='hoverBtns'><img class='deleteEventBtn' src='/assets/cross-image.png' style='display:inline;'> <img class='editEventBtn' src='/assets/edit.png' style='display:inline;'> </span>");
//           $(this).find(".hoverBtns").show();
//           $(".deleteEventBtn", this).on("click", function() {
//             if (event.handled != true) {
//               var param_id = $("#params_id").val();
//               if(deleteAct(param_id, event.id, event.type) == true)
//                 orignal_event.remove();
//               event.handled = true;
//             }
//           });
//           $(".editEventBtn", this).on("click",  function(click_event) {
//             var param_id = $("#params_id").val();
//             if (event.handled != true) {
//                 var url = "";
//                 if(event.type == "activity")
//                 url = "/add_child_activities_list?id="+param_id +"&test_report_id="+event.id;
//               else if(event.type == "exam")
//                 url = "/create_exam?id="+param_id+"&child_exam_id="+event.id;
//               else if(event.type == "exam_schedule")
//                 url = "/create_exam_schedule?exam_id="+event.exam_id+"&schedule_id="+event.id+"&edit=true";
//               else if(event.type == "holiday")
//                 url = "/school/edit_holiday?id="+event.id;
//               else if(event.type == "assignment")
//                 url = "/school/edit_assignment?id="+event.id;
//               console.log(url);
//               jQuery.facebox({ajax: url});
//               event.handled = true;
//             }
//            click_event.stopPropagation();
//           });
//         event.handled = false;

//       },
//       eventMouseout: function(event) {
//         $(this).find(".hoverBtns").hide();
//       }
//     });

//   $(".calender-class").on("click", function(){
//     $(".calender-class").removeClass("tgb-active");
//     $(this).addClass("tgb-active");
//     $('#calendar').fullCalendar("changeView",this.title);
//   });
// });
