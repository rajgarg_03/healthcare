$(function(){
  $.stellar({
    horizontalScrolling: false,
    verticalOffset: 40,
    responsive: true
  });

  /*/Click event to scroll*/
  $(".scroll").click(function(event){
    event.preventDefault();
    if(this.hash=='#service'){
        $('html,body').animate({scrollTop:$(this.hash).offset().top-$('#service').height()}, 1000);
    }else if(this.hash=='#organise' || this.hash=='#nurture' || this.hash=='#timeup' || this.hash=='#chrerish' ){
      if(Modernizr.mq('only screen and (max-width: 480px)')){
        if( this.hash=='#nurture' || this.hash=='#chrerish' ){
          $('html,body').animate({scrollTop:$(this.hash).offset().top-90}, 1000);
        }else{
          $('html,body').animate({scrollTop:$(this.hash).offset().top-78}, 1000);
        }
      }else{
        $('html,body').animate({scrollTop:$(this.hash).offset().top-95}, 1000);
      }
    }else if(this.hash=='#howit'){
      $('html,body').animate({scrollTop:$(this.hash).offset().top-50}, 1000);
    }
    else{
      $('html,body').animate({scrollTop:$(this.hash).offset().top}, 1000);
    }
  });
  //home page header size
   /* if(Modernizr.mq('only screen and (max-width: 480px)'))
        $('#home-header').css('height',$(window).height()-34);
    else
        $('#home-header').css('height',$(window).height()-55);*/

});
$(window).scroll(function(){

  var scrollTrack = $(this).scrollTop();
  var subStack = $('#service').offset().top-50;
  var subStackClose = $('#howit').offset().top-50

  if (scrollTrack >= 80) {
    $('.page-up').fadeIn();
  } else {
    $('.page-up').fadeOut();
  }

  //$('#testValue').text(scrollTrack+" "+ subStack +" "+ subStackClose);
    var scrollTop = $(window).height() - $('.stack-sub-replace').height()
  //console.log(scrollTrack,subStack,subStack);
  if(scrollTrack >= 28){
    $('.nav-stacked').hide().addClass('stack-header');
    $('.nav-replace').show();
    $('.stack-header').stop().animate({top:0},100);
  }

  if(scrollTrack <= 28){
    $('.stack-header').stop().animate({top:-53},300,function(){
      $(this).css('top','0');
      $('.nav-stacked').removeClass('stack-header').show();
      $('.nav-replace').hide();
    });
  }

  if(scrollTrack >= subStack){
    $('.stack-nav').addClass('stack-sub-nav');
    $('.stack-sub-replace').show();
    if(scrollTrack >= subStackClose){
      $('.stack-nav').slideUp(300,function(){
        $(this).removeClass('stack-sub-nav');
      });
    }else{
      $('.stack-nav').slideDown(300,function(){
        $(this).addClass('stack-sub-nav');
      })
    }
  }else{
    $('.stack-nav').removeClass('stack-sub-nav');
    $('.stack-sub-replace').hide();
  };
    $(window).resize(function(){
        // $('#home-header').css('height',$(window).height()-55);
        calculateHeight();
    });
});

