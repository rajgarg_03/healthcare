(function ($) {
  $.fn.funboxPop = function (settings) {
    // Pluginifying code...
    settings = $.extend({}, $.fn.funboxPop.defaults, settings);

    //pop public
    $this = $(this);

    //0 means disabled; 1 means enabled;
    $this.popupStatus = 0;

    $("a").each(function(index, value) {
      if($(this).attr('data-pop')=='inline'){
        $($(this).attr('href')).hide()
      }
    });

    //popup call
    $this.click(function () {

      el = $(this);
      var dataId = el.attr('href');
      var dataPopup = el.attr('data-pop')== undefined ? settings.dataPop : el.attr('data-pop');
      var dataWidth = el.attr('data-width');
      var dataHeight = el.attr('data-height');

      dataId == undefined ? dataId = el.attr('data-id') : '';

      if($this.popupStatus == 1){
        $this.$disablePopup();
      }

      if ($this.popupStatus == 0) {
        var $backgroundOverLay = $('<div id="overLay"/>');
        $backgroundOverLay.css({
          'display': 'block',
          'position': 'fixed',
          'height': '100%',
          'width': '100%',
          'top': 0, 'left': 0,
          'background-color': 'rgba(0,0,0,0.3)',
          'z-index': 9, 'overflow': 'auto'
        });
        //$("body").css({'overflow': 'hidden','margin-right':'17px'})
        $("body").prepend($backgroundOverLay);

        //Close box
        var closeBox = '<a href="javascript:$this.$disablePopup()" class="pop-close"></a>';

        //popup data
        var $popupData = $('<div class="popupWrapper"/>');
        //popup holder
        var $popHolder = $('<div id="popupHolder"/>');
        $popupData.append($popHolder);


        $popupData.css({
          "position": "relative", "margin": "80px auto 50px auto", 'display': 'none', 'background-color': '#FFF', 'z-index': 3
        }).prepend(closeBox);

        if (dataWidth != "" && dataWidth != undefined) {
          $popupData.css({
            'width': dataWidth
          });
        } else {
          $popupData.css({
            'width': "500"
          });
        }
        if (dataHeight != "") {
          $popupData.css({
            'height': dataHeight
          });
        }

        $backgroundOverLay.prepend($popupData);

        if (dataPopup == "inline") {
          var popNewData = $(dataId).html()
          $this.data('popData', popNewData);
          $popHolder.html(popNewData)
          $(dataId).html('empty');
        } else {
          if (dataId == "#html") {
            $popHolder.load(dataPopup,function(){
        settings.callBack()
      })
          } else {
            $popHolder.load(dataPopup + " " + dataId)
          }
        }
        $popupData.fadeIn();
        $this.popupStatus = 1;
      }
      //Disable Popup
      $this.$disablePopup = function () {
      //disables popup only if it is enabled
      if ($this.popupStatus == 1) {
        if (dataPopup == "inline") {
          $(dataId).html($this.data('popData'));
        }
        $("#overLay").remove();
        $(".popupWrapper").remove();
        //$("body").css({'overflow': 'auto','margin-right':'0px'});
        $this.popupStatus = 0;
      }
    };
    });
  }

  // function options
  $.fn.funboxPop.defaults = {
    dataPop:'',
	  callBack: function(){ /* function come */ }
  }
  $(document).ready(function (e) {
    $('[data-pop]').funboxPop();
  });
})(jQuery);