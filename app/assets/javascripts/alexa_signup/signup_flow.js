//= require jquery.cookie
//= require cookies_eu
function show_signup_form(){
  $(".js-user_name_form").hide()
  $(".js-devise_form").show()
  $(".js-user_location_form").hide()
}

function show_nick_name_form() {
  $(".js-alexa-signup-error").html("")
  // $(".js-user_name_form").show()
  $(".js-child_name_form").show();
  $(".js-sign_in_form").hide();
  $(".js-devise_form").hide()
  $(".js-user_location_form").hide()
}


 

function register_user(data,session_data) {
  var that = $(this);
  that.prop("disabled", true);
  $(".hidden").show();
  $(".js-alexa-signup-error").html("");

  $.ajax({
    url: "/api/v5/users",
    type: "Post",
    data: data,
    dataType: "JSON",
    complete: function(){
      that.prop("disabled", false);
      $(".hidden").hide();
    },
    success: function(response,textStatus,xhr) {
      if(response.status != 200){
        show_signup_form();
        handleErrorRespose(response.message);
      }
      else if(response.status == 200){
        session_data["token"] = response.auth_token;
        $(".js-alexa-signup-error").html("");
        if(response.auth_token == null && response.screen == "email_verification_screen"){
          window.screen_name = response.screen;
          data["add_signup_complete_event"] = true;
          get_user_info(data, session_data);
        }
        else{
          session_data["email"] = data["user"]["email"];
          sign_in_controller(response,session_data);
        }
      }
    } ,
    error: function(response){
      handleErrorRespose(error_message);
    }

  });
}



function sign_in_controller(form_data,session_data){
  if((session_data["token"] == null || session_data["token"].trim().length == 0) && jQuery.isEmptyObject(form_data) ){
   var url = alexa_signin_url(session_data);
    redirect_to_url(url);
  }
  else{
    var params_for_sign_in = {user:{}},signup_source=null;
    if(form_data["password"] != null){
      params_for_sign_in["email"] = form_data["email"]
      params_for_sign_in["password"] = form_data["password"]
      params_for_sign_in["user"]["email"] = form_data["email"]
      params_for_sign_in["user"]["password"] = form_data["password"]
       
    }

    else{
      params_for_sign_in["email"] =  session_data["email"]
      params_for_sign_in["auth_token"] = session_data["token"] ;
    }
    if(session_data["alexa_flow_status"] == "true"){
      signup_source = session_data["alexa_platform"]
    }
    params_for_sign_in["allow_access_to_dashboard"] = true;
    params_for_sign_in["redirect_uri"] = session_data["redirect_uri"];
    params_for_sign_in["state"] = session_data["state"];
    params_for_sign_in["platform"] = signup_source;
    params_for_sign_in["app_name"] = session_data["alexa_source_reference"];
    params_for_sign_in["alexa_platform"] = session_data["alexa_platform"];

    console.log("sign in user");
    var user_data = signin_api(params_for_sign_in,session_data);
  
  }
}

function run_basic_setup(basic_setup_response,session_data){
  console.log("running run_basic_setup");
  var params_for_create_family = {},
  basic_setup = basic_setup_response[0]["basic_setup"];
  enabled_controller = basic_setup_response[0];
  if(basic_setup == true && !jQuery.isEmptyObject(basic_setup_response[1]["NYHelpScreenController"]) ){
    var help_screen_data = basic_setup_response[1]["NYHelpScreenController"]
    show_help_screen(help_screen_data,session_data);
  }
  else if(enabled_controller["NYAddFamilyController"] == true){
    params_for_create_family["token"] = session_data["token"] 
    params_for_create_family["redirect_uri"] = session_data["redirect_uri"] 
    params_for_create_family["alexa_platform"] = session_data["alexa_platform"] 
    params_for_create_family["state"] = session_data["state"]
    get_create_family_form(data={},session_data);
  }
  else if(enabled_controller["NYAddChildController"] == true){
    var params_for_create_child = {}
    session_data["family_id"] = basic_setup_response[1]["NYAddChildController"]["family_id"]
    get_create_child_form(params_for_create_child,session_data);
  }
  
  else if(enabled_controller["NYWelcomeController"] == true){
     data = {};
  }
  else if(enabled_controller["NYAddSpouseController"] == true){
     data = {};
  }
  else{
    data = {};
    alexa_signin_api(data,session_data);
  }
}

function show_help_screen(data,session_data){
// show_help_screen();
var request_data = {help_data:{},session_data:session_data};
$(".js-alexa-signup-error").html("");
  request_data["help_data"] =  data;
  $.ajax({
      url: "/alexa/sign_up/finish_setup",
      type: "Get",
      data: request_data,
      dataType: "script",
      success: function(response){
        setTimeout(function(){ $(".js-alexa-signup-error").html("") }, 10000);
        history.pushState(null, '', "/alexa/sign_up/finish_setup");
        
      },
      error: function(response){
        handleErrorRespose(error_message);
      }
    });
}

function alexa_signin_url(session_data){
 var url = ["/alexa/sign_in?state=",session_data["state"],"&redirect_uri=",session_data["redirect_uri"]].join("")
  return url;
}

function alexa_signup_url(session_data){
 var url = ["/alexa/sign_in?state=",session_data["alexa_state"],"&redirect_uri=",session_data["alexa_redirect_uri"]].join("")
  return url;
}

function redirect_to_url(url){
  window.location.href = url;
}


function alexa_signin_api(data,session_data,redirect_url){
  var that = $(this);
      that.prop( "disabled", true );
      $(".hidden").show();
      if(data["password"] == null){
        data["auth_token"] = session_data["token"]
      }
      if(data["email"] == null){
        data["email"] = session_data["email"]
      }
  data["redirect_uri"] = session_data["redirect_uri"]
  data["platform"] = session_data["alexa_platform"]
  data["app_name"] = session_data[""]
  data["state"] = session_data["state"]
  $.ajax({
  url: "/api/v5/users/secure_alexa_sign_in",
    type: "Post",
    data: data,
    dataType: "JSON",
    complete: function(){
      that.prop( "disabled", false );
      $(".hidden").hide();
    },
    success: function(response,textStatus,xhr) {
      if(response.status != 200){
        user_alexa_signup_step = user_alexa_signup_step + 1
        handleErrorRespose(response.message);
        if(redirect_url != null){
          window.location.href = signup_url;
        }
      }
      else if(response.status == 200){
        $(".js-alexa-signup-error").html("");
         window.location.href = response.redirect_to;
      }
    },
    error: function(response){
      handleErrorRespose(error_message);
    }

  });
}

function resent_email(user_email,session_data){
  var data = {};
  data["email"] =  user_email;
  data["alexa_flow_status"] =  true;
  $.ajax({
      url: "/api/v5/users/resend_confirmation_link",
      type: "Get",
      data: data,
      dataType: "JSON",
      success: function(response){
        handleErrorRespose(response.message);
      },
      error: function(response){
        handleErrorRespose(error_message);
      } 
    });
}

function show_email_verification_screen(data,session_data){
  var data = data;
  session_data["email"] = window.user_email
  session_data["password"] = window.user_pwd
  data["session_data"] = session_data
  $(".hidden").show();
  $.ajax({
    url: "/alexa/sign_up/get_email_verification_screen",
    type: "Get",
    data: data,
    dataType: "script",
     complete: function(){
      $(".hidden").hide();
      if (session_data["signin_from_verificaton"] == "true"){
          handleErrorRespose("<span style='color:red'>Please verify your email ID through the link we have emailed to you. Do check your spam folder, if you haven't received the email</span>");
      }
    },
    success: function(response1){
      
      history.pushState(null, '', "/alexa/sign_up/get_email_verification_screen");
    }
  }); 
}

function add_fb_pixel_event(session_data,page_name){
  event_name = page_name
  current_user = {_id:session_data["user_detail"]["member_id"],user_id:session_data["user_detail"]["_id"]}
  if(session_data["user_detail"]["signup_source"] == "alexa" || session_data["user_detail"]["signup_source"] == "googleassistance"){
    if(checkCookie("complianceCookie") == "on"){
      fbq('trackCustom', event_name,{id:(current_user._id + current_user.user_id),app_name:session_data["alexa_source_reference"], platform:session_data["user_detail"]["signup_source"]});
    }
  }
}


function checkCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function signin_api(form_data,session_data){
  var that = $(this);
  var sign_in_response = {};
  that.prop( "disabled", true );
  $(".hidden").show();
 
  $.ajax({
    url: "/api/v5/users/sign_in",
    type: "Post",
    data: form_data,
    dataType: "JSON",
    complete: function(){
      that.prop( "disabled", false )
      $(".hidden").hide();

    },
    success: function(response,textStatus,xhr) {
      if(response.status == 500){
        var url = alexa_signin_url(session_data);
         redirect_to_url(url);
      }
      if(response.status == 100){

        window.screen_name = "email_verification_screen"
         
        get_user_info(form_data, session_data);
        
        // show_email_verification_screen(form_data,session_data);
        
        // handleErrorRespose(response.message);
      }
      if(response.status == 102 || response.status == 103 ){
        handleErrorRespose(response.message);
      }
      else if(response.status == 200){
        
        $(".js-alexa-signup-error").html("")
        session_data["email"] = form_data["email"]
        session_data["token"] = response["user_basic_info"]["token"];
        if(response["basic_setup_controller"] != null && response["basic_setup_controller"][0]["basic_setup"] == true){
          
          if ((form_data["auth_token"]|| "").length > 0 ){
            form_data["user"] = {email:session_data["email"]}
            form_data["user"]["password"] = null;
            form_data["allow_access_to_dashboard"] = true;
            form_data["redirect_uri"] = session_data["redirect_uri"];
            form_data["state"] = session_data["state"];
            form_data["platform"] = "alexa";
            form_data["app_name"] = session_data["alexa_source_reference"];
            form_data["alexa_platform"] = session_data["alexa_platform"];
            get_user_info(form_data, session_data);
              
          }
          else{
           run_basic_setup(response["basic_setup_controller"],session_data);
            
          }        
        }
        else{
          data = form_data;
          alexa_signin_api(data,session_data)
        }
      }
    },
    error: function(response){
      handleErrorRespose(error_message);
    }

  });
  
}

function get_create_family_form(data,session_data)
{
  var data = data
  $.ajax({
    url: "/alexa/sign_up/create_family",
    type: "Get",
    data: session_data,
    dataType: "script",
    success: function(response1){
      history.pushState(null, '', "/alexa/sign_up/create_family");
    }
  });
}
 function randomStr(m) {
    var m = m || 9; s = '', r = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    for (var i=0; i < m; i++) { s += r.charAt(Math.floor(Math.random()*r.length)); }
    return s;
  };


  function create_family_api(form_data,session_data){
    var data = {"family":{}};
    var that = $(this);
    // that.prop("disabled", true);
    $(".hidden").show();
    data["role"] = "Creator"
    data["family"]["family_uid"] = randomStr(8) + "-" + randomStr(4) + "-" + randomStr(4) + "-" + randomStr(4) + "-" + randomStr(12);
    data["family"]["created_by"] = "system"
    data["family"]["name"] = "My Family"
    data["token"] = session_data["token"];
    data["state"] = session_data["state"]
    data["redirect_uri"] = session_data["redirect_uri"]
    data["platform"] = session_data["alexa_platform"]
    data["allow_access_to_dashboard"] = true
    form_data["session_data"] = session_data;
    $.ajax({
      url: "/alexa/sign_up/get_user",
      type: "Get",
      data: form_data,
      dataType: "json",
      complete: function(){
        that.prop( "disabled", false )
        
      },
      success: function(response){ 
        window.basic_setup_status = response["basic_setup_status"]
        window.basic_setup_controller = response["controllers"]
        session_data["user_detail"] = response["user_detail"]
        console.log(session_data["user_detail"])
        add_fb_pixel_event(session_data,"Alexa add family");
        if(response["basic_setup_status"]["NYAddChildController"] == true){
          session_data["family_id"] = response["controllers"]["1"]["NYAddChildController"]["family_id"];
          $(".js-alexa-signup-error").html("");
          create_child(data,session_data)
        }
        else if(response["basic_setup_status"]["NYAddFamilyController"] == true)
        {  
          $.ajax({
            url: "/api/v5/families",
            type: "Post",
            data: data,
            dataType: "JSON",
            complete:function(){
              that.prop("disabled", false);
              },
              success: function(response,textStatus,xhr) {
                if(response.status == 100){
                  // show_create_family_form();
                  handleErrorRespose(response.message);
                }
                else if(response.status == 200){
                  session_data["family_id"] = response.family._id
                  $(".js-alexa-signup-error").html("");
                  create_child(data,session_data)
                }
              } ,
              error: function(response){
                handleErrorRespose(error_message);
              }
        
          });
        }
        else{
          show_screen(screen_name,data,session_data)
        }
      }
    })
  }

  function show_screen(screen_name,data,session_data){
    console.log("data.............")
    console.log(data)
    console.log("session data.............")
    console.log(session_data)
    $(".hidden").show();
    if(screen_name == "email_verification_screen"){
      show_email_verification_screen(data,session_data)
    }
    else{
      data["user"] = {email:session_data["email"]}
      get_basic_setup_data(data,session_data)
    }
  }

  function get_create_child_form(param_data,session_data){
    var data = {"token":session_data["token"]}
    data["state"] = session_data["state"];
    data["redirect_uri"] = session_data["redirect_uri"];
    data["platform"] = session_data["alexa_platform"];
    data["family_id"] = session_data["family_id"]
    $.ajax({
      url: "/alexa/sign_up/add_child",
      type: "Get",
      data: data,
      dataType: "script",
      success: function(response1){
        show_screen(window.screen_name,param_data,session_data)
         
        // history.pushState(null, '', "/alexa/sign_up/add_child");
      },
      error: function(response){
        handleErrorRespose(error_message);
      }
    });
  }

  function get_child_gender(name)
    {
      var gender = "Son";
      $.ajax({
        url: "https://api.genderize.io/?name=" + name,
        type: "Get",
        async: false,
        dataType: "JSON",
        success: function(response,textStatus,xhr) {
          if(response.gender == "female")
          {
            gender = "Daughter"
          }
          else{
            gender = "Son"
          }
        },
        error: function(response){
          gender = "Son"
        }
      });
      return gender
    }

  function create_child(param_data,session_data) {
    var data = {"Member":{"family_members":{}}};

      // $(".js-child_gender_submit").prop("disabled", true);
      $(".hidden").show();

      data["Member"]["first_name"] = $("#nick_name_input").val();
      // child_gender = get_child_gender(data["Member"]["first_name"])
      child_gender = $("input[name=gender]:checked").val();
      data["Member"]["last_name"] = ""
      data["Member"]["birth_date"] = $(".js-child_dob").val();
      data["Member"]["family_members"]["role"] = child_gender;
      data["Member"]["family_members"]["role_added_by"] = "system";
      data["token"] = $("#alexa_user_token").val();
      data["allow_access_to_dashboard"] = true
      data["signup_source"] = "alexa"
        $.ajax({
        url: "/api/v5/families/" + session_data["family_id"] + "/member/create_child.json",
        type: "Post",
        data: data,
        dataType: "JSON",
        complete: function(){
          $(".js-child_gender_submit").prop("disabled", false);
          $(".hidden").hide();

        },
        success: function(response,textStatus,xhr) {
          if(response.status == 100){
            // show_add_child_form();
            if (response.message == "Birth date add birth date")
            {
              handleErrorRespose("Please add birth date");
            }
            else{
              handleErrorRespose(response.message);
            }
          }
          else if(response.status == 200){
              window.k = response;
            data = {"token":session_data["token"]}
            data["child_member_id"] = response.family_members[0]._id;
            data["family_id"] = session_data["family_id"] ;
            data["state"] = session_data["state"]
            data["redirect_uri"] = param_data["redirect_uri"]
            add_fb_pixel_event(session_data,"Alexa add child");

            show_screen(window.screen_name,data,session_data)
          }
        },
        error: function(response){
          handleErrorRespose(error_message)
        }

      });
  }

  function get_basic_setup_data(data, session_data) {
    data["session_data"] = session_data
    $(".hidden").show();
    $.ajax({
      url: "/alexa/sign_up/get_user",
      type: "Get",
      data: data,
      dataType: "json",
      
      complete: function(){
        $(".hidden").hide();
      },      
      success: function(response){ 
        user_details_response = response
        session_data["user_detail"] = response["user_detail"]
        window.basic_setup_status = response["basic_setup_status"]
        window.basic_setup_controller = response["controllers"]
        run_basic_setup(window.basic_setup_controller,session_data)

      }
    })
  }

  function get_user_info(data, session_data) {
    data["session_data"] = session_data;
    $(".hidden").show();
    $.ajax({
      url: "/alexa/sign_up/get_user",
      type: "Get",
      data: data,
      dataType: "json",
      complete: function(){
        $(".hidden").hide();
      },      

      success: function(response){ 
        user_details_response = response

        $("#alexa_user_token").val(response["token"])
        window.basic_setup_status = response["basic_setup_status"]
        window.basic_setup_controller = response["controllers"]
        window.user_pwd = data["user"]["password"]
        window.user_email = data["user"]["email"]
        data["session_data"]["user_detail"] = response.user_detail
        if (data["add_signup_complete_event"] == true){
          add_fb_pixel_event(data["session_data"],"Alexa signup complete");
        }
        if (response["basic_setup_status"]["NYAddFamilyController"] == true || response["basic_setup_status"]["NYAddChildController"]== true)
          {
            show_nick_name_form();
          }
        else{
            if(window.screen_name == "email_verification_screen")
            {
              show_email_verification_screen(data,session_data);
            }
            else{
              run_basic_setup(response["controllers"],session_data);
            }
          // show_screen(session_data["screen_name"])

        }

      },
      error: function(response){
        handleErrorRespose(error_message);
      }
    });
  }

