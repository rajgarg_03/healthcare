// (function ($) {
  (function($,window,undefined){
  $.fn.accordianBox = function () {
    var el = $(this);
    // el.find('li.current').children('div.list-sub').show();

    el.find('li > a').on('click', function () {
        if (!($(this).parent().hasClass('current'))) {
          // $(this).parent().parent().find('div.list-sub').slideUp();
          // $(this).parent().parent().find('li.current').removeClass();
          $('div.list-sub').slideUp();
          $('li.current').removeClass("current");
          $(this).next('div').slideDown();
          $(this).parent().addClass('current');
        }
        else {
        $('div.list-sub').slideUp();
        $(this).parent().removeClass('current');
      }
      }
    );
     
  };

  $(document).ready(function (e) {
    $('[data-accordian]').accordianBox();
  });
})(jQuery);