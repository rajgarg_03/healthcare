function update_left_nav(member_id, nutrient){
  obj = $(".nav-list-1 li.current")
  obj.removeClass("current")
  obj.find(".list-sub").hide()

  obj = $("." + member_id).parent();
  obj.addClass("current")
  obj.find(".list-sub").show();
  obj.find(".list-sub .js-nav.current").removeClass("current");
  obj.find(".list-sub .js-nav.js-"+ nutrient).addClass("current")
  window.scrollTo(0,0);

}


function parentMember()
{
  $(".nav-list-1 li").first()
 obj = $(".nav-list-1 li.current")
  obj.removeClass("current")
  obj.find(".list-sub").hide()

  obj = $(".nav-list-1 li").first()

  obj.addClass("current")
  obj.find(".list-sub").show();
  obj.find(".js-dashboard").addClass("current")

}

$(function(){
  $("body").on("click",".js-ParentMember", function(){
  $(this).parent().find(".js-dashboard").addClass("current")
});
$("body").on("click",".accordion-type1 li",function(){
$(".accordion-type1 li.active").not($(this)).removeClass("active").find(".acc-content").hide()
 
})

  $(".scroll-type3").mCustomScrollbar({
        autoHideScrollbar: true,
        theme: "dark"
    });

  $("body").on("click",".js-recent",function(){
    var obj = $(this).parent().find("a.js-recent-content").first();
    console.log(obj);
    var member_id = obj.data("memberId"),
    nutrient = obj.data("nutrient");
   
    obj.click();
    update_left_nav(member_id, nutrient);
  });


$("body").on("click",".js-recent1",function(){
    var object_id = $(this).data("class")
     obj =   $("."+object_id).first();
    console.log(obj);
    var member_id = obj.data("memberId"),
    nutrient = obj.data("nutrient");
   
    obj.click();
    update_left_nav(member_id, nutrient);
  });

  $("body").on("click",".acc-main.js-non-acc",function(event){
    var obj = $(this).parent().find("a").first();

    if(obj.data("pop")){
      obj.click();
    }
    else{
    var member_id = obj.data("memberId"),
    nutrient = obj.data("nutrient");
 
    obj.click();
  }
    update_left_nav(member_id, nutrient);

    // .click();
  })

$("body").on("change",".js-checkbox",function(){
  var obj = $(this).next().find("a").first();
  var member_id = obj.data("memberId"),
  nutrient = obj.data("nutrient");
 
  obj.click();
  update_left_nav(member_id, nutrient);


});

// update cover image
$("body").on("change","#js-cover-image",function(){
$("#cover-image-form").submit();
})
// Home page welcome section 

$("body").on("click",'.accordion-type1 .js-acc-main',function(){
  window.ll = $(this)
  $(this).parent().toggleClass('active');
  $(this).next().slideToggle();
});



})
