$(document).ready(function(){
  
  $("body").on("click",".js-vaccin-done",function(){
    var selected_vaccin = [],

    member_id = $("#member_id").val();
    $(this).val("Saving..")
    $("input:checkbox:checked.js-vaccin").each(function(){ (selected_vaccin.push($(this).attr("id")))});
    $.ajax({
      url: "/vaccination/add_vaccin",
      type: "GET",
      data: { vaccin_id : selected_vaccin.join(","), member_id: member_id },
      dataType: "script"
    });
  });

  $(".scroll-type1").mCustomScrollbar({
    autoHideScrollbar: false,
    theme: "dark"
  });

  $("body").on("change",".js-jab-checkbox",function(){
    $(this).parent().find(".js-admin-link").first().click();
    $(this).parent().find(".js-admin-label").text("Administered")
    $(this).prop("checked","checked");
    $(this).parent().find(".js-date").show();
  });

  $("body").on("click",".js-admin-link",function(){
   // $(this).parent().parent().parent().find(".js-admin-label").text("Administered")
   // $(this).find("li").text("Administered")
  })
})