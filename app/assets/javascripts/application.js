//= require jquery 
//= require jquery_ujs
// require jquery.remotipart
//= require jquery-ui-1.10.3.custom.min
//= require jquery.funboxPop.1.0
//= require timeline
//= require member
//= require immunisation
//= require pagination

//= require jquery.mCustomScrollbar.concat.min

//= require jquery.dd.min
//= require health
//= require add_media
//= require users
//= require jquery.Jcrop.min
//= require jquery.lightbox
//= require jquery.placeholder
//= require fullcalendar
//= require utilities
//= require jquery.form
//= require jqueryuiTimepicker
//= require emotion
//= require jquery.cookie
//= require jstz.min
//= require set_time_zone
//= require cookies_eu
// require_tree .

$(window).unload(function() {
  $.rails.enableFormElements($($.rails.formSubmitSelector));
});
 $.rails.allowAction = function(link) {
  if (!link.attr('data-confirm')) {
    return true;
  }
  $.rails.showConfirmDialog(link);
  return false;
};

$.rails.confirmed = function(link) {
  link.removeAttr('data-confirm');
  return link.trigger('click.rails');
};


$.rails.showConfirmDialog = function(link) {
  var html, message;
  message = link.attr('data-confirm');
  html = "<div id=\"dialog-confirm\">\n  <p>" + message + "</p>\n</div>";
  return $(html).dialog({
    resizable: false,
    modal: true,
    buttons: {
      OK: function() {
        $.rails.confirmed(link);
        return $(this).dialog("close");
      },
      Cancel: function() {
        return $(this).dialog("close");
      }
    }
  });
};

// Notifications, custom scrollbar plugin
$(function(){
      $('.header-notification').click(function(){
          $(this).toggleClass('active');
          $('.notification-view').toggle();
      });
      $(".scroll-type5").mCustomScrollbar({
          autoHideScrollbar: false,
          theme: "dark"
      });
  });

$(document).mouseup(function (e)
{
    // console.log($(e.target).hasClass('js-notification-icon'));    
    var container = $(".notification-view");
    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0 && !$(e.target).hasClass('js-notification-icon')) // ... nor a descendant of the container
    {
        $('.header-notification').removeClass('active');
        $(".notification-view").hide();
    }
});
//////////////////////////////////////////

$(document).ready(function(){

  jQuery(function() {
    var tz = jstz.determine();
    $.cookie('timezone', tz.name(), { path: '/' });
  });

  if (history && history.pushState){
    $(function(){
     $('body').on('click', 'a.js-change-url', function(e){
        // $.getScript(this.href);
        history.pushState(null, '', this.href);
        page_name = this.href.split("/").pop()
     // track FB event
     // event_name  = page_name;
     // track_fb_pixel_custom_event(event_name,1)
     // 
      });
      $(window).bind("popstate", function(){
        $.getScript(location.href);
      });
    });
  }

  $("body").on("click",".js-remove-default-image", function(event){
    $(this).parent().remove();
    $('input#milestone_display_default_image').val(false);
  })

  $("body").on("click",".icon-times", function(event){
    event.preventDefault();
  })

  $('input, textarea').placeholder();

  $("body").on("click",".js-header-tab", function(event){
    $('.primNav .active').removeClass('active');
    $(this).addClass('active');
    // alert("hi");
  })
  

  $('.globel-msg').bind("DOMSubtreeModified",function(){  
    console.log('changed');
    setTimeout(function(){
      $('.globel-msg').html("");
    }, 5000);
  });

  $("body").on("keypress", "input.inputType1", function(event){
    return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 0)|| (event.charCode == 46)
  }) ;
 $("body").on("keypress", "input.inputType2", function(event){
     return (event.charCode >= 46 && event.charCode <= 57) || (event.charCode >= 64 && event.charCode <= 90) || (event.charCode >= 95 && event.charCode <= 122) || (event.charCode ==13)|| (event.charCode == 0) || (event.charCode == 32)|| (event.charCode ==13)  
    }) ;

$("body").on("focusout", "input.inputType1", function(event){
      var value = ($(this).val()),
        max_val =  $(this).data("max"),
        min_val = $(this).data("min");

      if(value > max_val){
        $(this).val(max_val);
      }
      else if(value < min_val){
        $(this).val(min_val)
      }
      else{
      $(this).val( Number(Math.round(value +'e1')+'e-1') );
    }
 });

 $("body").on("click",".uploader",function(){
     var count = $(this).data("count");
     $(".fileload"+count).click();
      
    });

 $("body").on("click",".document-form input[type=submit]",function(){
    if($('.js-name').val()=="")
    {
      // alert("blank");
      $('.js-name').addClass("border-red");   
      $('.js-name').after("<span class='error'>can't be blank</span>");  
      return false;
    }
 });

 $("body").on("click",".js-done",function(){
  $("#js-addTimeline").append($(".fileload"))
  $(".fileload").addClass("custom-file-2")
  $.fn.close();
 })
 
 $("body").on("drop",".uploader",function(){
     var image_count = $(this).data("count");
     $(".fileload"+ image_count).click();
    });
 
   PreviewImage = function() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uplodAvatar").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview").src = oFREvent.target.result;
        };
    };
   $("body").on("click","#uploadPreview",function(){
    $("#uplodAvatar").click();
   });
  $(document).ajaxComplete(function() {
    $('[data-pop]').unbind();
    $('[data-pop]').funboxPop();
    $('input, textarea').placeholder();
    $.rails.enableFormElements($($.rails.formSubmitSelector));
  }); 

 // Automatically cancel unfinished request

  $.xhrPool = [];
 
  $.xhrPool.abortAll = function() {
    $.each($.xhrPool, function(jqXHR,req) { 
        req.abort(); 
        $.xhrPool.pop();
    });
  };

  $(document).ajaxSend(function(e, jqx){
  // console.log($.xhrPool)
    $.xhrPool.abortAll();
    $.xhrPool.push(jqx);
  });

 //...................................

   
  $("body").on("click",".js-nav",function(){
    $(".js-nav.current").removeClass("current");
    $(this).addClass("current");
  });

  $('body').on('click','.js-close-pop', function(){
    $.fn.close()
  });

  $('body').on('click','.js-remove_invite_email', function(){
    $(this).parent().parent().remove();
    $("#new_invitations_count").text(parseInt($("#new_invitations_count").text())-1)
  });
  
  

  $('body').on('click','.js-copy-email-content',function(){
    // alert($('.js-invite-email-content').val());
    $("input#invite_email_text").val($('.js-invite-email-content').val());
    $.fn.close();
  });
  $('body').on('click',".timeInput",function(event){
    $(this).timepicker().focus();
  });
  
  $('body').on('click','.dateInput', 'input.dateInput , input.dateInput1', function(event) {
    if($(this).hasClass("js-dob1")){
      $(this).datepicker({
        showOn: 'focus',
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        yearRange: "-50:+20" 
      }).focus();
    }

    if($(this).hasClass("js-dob")){
      $(this).datepicker({
        showOn: 'focus',
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-50:+20",
        maxDate: 0
      }).focus();
    }
    else if ($(this).hasClass("js-event")){
      $(this).datepicker({
        showOn: 'focus',
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-50:+20",
        minDate: 0
      }).focus();
    }
    else{
      $(this).datepicker({
        showOn: 'focus',
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-50:+20",
      }).focus();
    }
  });

  // Read profile image on click
  $(document).on('change', "#js-add-profile-pic:input[type='file']", function(){
        console.log('changed')
        readURL(this);
        // $('.js-profile-image img').Jcrop({
        //     onChange: $.showCoords,
        //     onSelect: $.showCoords,
        //     aspectRatio: xsize / ysize,
        //     setSelect: [ 130, 0, 210, 210 ]
        // })

    });

  $(document).on('change', "#js-add-cover-image:input[type='file']", function(){
        console.log('changed')
        readURL_cover_image(this);
    });

$(window).scroll(function() {
    var scrollTrack = $(this).scrollTop();
    if (scrollTrack >= 80) {
        $('.page-up').fadeIn();
    } else {
        $('.page-up').fadeOut();
    }
});

});
 
function search_family(query) {
    if (jQuery.isNumeric(query)){
      query_length = 9
      query_param = 'family_uid'
    }
    else{
      query_length = 5
      query_param = 'email'
    }
    if (query.trim().length >= query_length) {
      url = '/families/search_family?' + query_param + '=' + query;
      $("ul#showList").load(url);
      $("ul#showList").show();
    }
    else{      
      $("ul#showList").html('');
    }
  }

  
function js_display_date(event){
  element = $("#" +event.target.id);
  if (element.is(':checked')== true){
      element.parent().parent().find('.action-panel').show();
  } else {
      element.parent().parent().find('.action-panel').hide();
  }
}

// Read profile image and display on upload
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            if ($('.js-profile-image img').length)
            {
              $('.js-profile-image img').remove();
            }
            
              $('.js-profile-image').html('<img src="#" style="width:auto;"></img>');
              // var img = new Image();
              // img.src =  
              $('.js-profile-image img').first().attr('src', e.target.result).load(function(){
                $(".profile-upload-img").removeClass("icon-user-child-boy");
                $(".profile-upload-img").removeClass("icon-user-child-girl");
                $(".profile-upload-img").removeClass("icon-user");

                if(parseInt(this.width) > parseInt(this.height)){
                  $('.js-profile-image img').width("100%")
                  $('.js-profile-image img').height("auto")
                }
                else{
                  $('.js-profile-image img').width("auto")
                  $('.js-profile-image img').height("100%")
                }
                  // $(".popupWrapper").width(this.width);
                  // $(".popupWrapper").height(this.height)

                // console.log(this);
               var that = this;
              $('.js-profile-image img').Jcrop({
                onChange: $.showCoords,
                onSelect: $.showCoords,
                allowResize: true,
                allowSelect: true,
                aspectRatio: 1,
                setSelect: [ 130, 0, 210, 210 ]
              })  
              });
        }
        reader.readAsDataURL(input.files[0]);
    }
}

//


// Read profile image and display on upload
function readURL_cover_image(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            if ($('.js-cover-image img').length)
            {
              $('.js-cover-image img').remove();
            }
            
              $('.js-cover-image').html('<img src="#" style="width:auto;"></img>');
              // var img = new Image();
              // img.src =  
              $('.js-cover-image img').first().attr('src', e.target.result).load(function(){
                if(parseInt(this.width) > parseInt(this.height)){
                  $('.js-cover-image img').width("100%")
                  $('.js-cover-image img').height("auto")
                }
                else{
                  $('.js-cover-image img').width("auto")
                  $('.js-cover-image img').height("100%")
                }
               var that = this;
              $('.js-cover-image img').Jcrop({
                onChange: $.showCoords,
                onSelect: $.showCoords,
                allowResize: true,
                allowSelect: true,
                aspectRatio: 608/280,
                setSelect: [ 130, 0, 440, 210 ]
              })  
              });
        }
        reader.readAsDataURL(input.files[0]);
    }
}


cord = {x:0 , y:0, w:0, h:0},
   
  $.showCoords = function showCoords(c)  
  {
      cord = c;
  $("#crop_x").val(c.x);
  $("#crop_y").val(c.y);
  $("#crop_w").val(c.w);
  $("#crop_h").val(c.h);
    // $.ajax({
    //   url: "/upload_images/preview", //sumbits it to the given url of the form
    //   data: {x: c.x, y: c.y, h: c.h, w: c.w,r: 0}  
    //   ,success: function(data){
    //     console.log(data);
    //     $('#croped').html(data);
    //   }
    // });
  };
$(function($){

   
 
    })
    
   
        // Create variables (in this scope) to hold the API and image size
        var jcrop_api,
                boundx,
                boundy,

        // Grab some information about the preview pane
                $preview = $('#preview-pane'),
                $pcnt = $('#preview-pane .preview-container'),
                $pimg = $('#preview-pane .preview-container img'),

                xsize = 110,
                ysize = 110;

        console.log('init',[xsize,ysize]);
        function updatePreview(c)
        {
            if (parseInt(c.w) > 0)
            {
                var rx = xsize / c.w;
                var ry = ysize / c.h;

                $pimg.css({
                    width: Math.round(rx * boundx) + 'px',
                    height: Math.round(ry * boundy) + 'px',
                    marginLeft: '-' + Math.round(rx * c.x) + 'px',
                    marginTop: '-' + Math.round(ry * c.y) + 'px'
                });
            }
        };

// Placeholder for IE
  $(function() {
    if (!$.support.placeholder) {
      var active = document.activeElement;
      
      $(':text').focus(function() {
        if ($(this).attr('placeholder') != '' && $(this).val() == $(this).attr('placeholder')) {
          $(this).val('').removeClass('hasPlaceholder');
        }
      }).blur(function() {
        if ($(this).attr('placeholder') != '' && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder'))) {
          $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
        }
      });
      $(':text').blur();
      $(active).focus();
      
    }
  });

  // textarea event handler to add support for maxlength attribute
$(document).on('input keyup', 'textarea[maxlength]', function(e) {
    // maxlength attribute does not in IE prior to IE10
    // http://stackoverflow.com/q/4717168/740639
    var $this = $(this);
    var maxlength = $this.attr('maxlength');
    if (!!maxlength) {
        var text = $this.val();
        
        if (text.length > maxlength) {
            // truncate excess text (in the case of a paste)
            $this.val(text.substring(0,maxlength));
            e.preventDefault();
        }
        
    }
    
});




//= require fullcalendar_engine/application



