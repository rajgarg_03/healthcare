 $(function(){


//  delete photo from pictures
$("body").on("click",".js-delete-photo",function(){
  var id= $(this).data("id"),
    this_obj = $(this);
  $.ajax({
    url: "delete_photo",
    data: {id:id},
    script: "script",
    success: function(){
      this_obj.parent().remove();
    }
  });
});

// Start of Add multi media file
    $("body").on("click",".js-remove-photo",function(){
      $(this).parent().remove();
    });

// media file js  
   $("body").on("click",".js-media",function(){
    if ($('.js-display-media-images:hidden').length)
    {
      $('.js-display-media-images').show();
    }
    // alert($("ul.milestone_pictures li.js-ss").length);
    if ($("ul li.js-ss").length >= 8){
      alert("Maximum 8 images are allowed")
    }
    else
    {     
      // if($($(".js-fileupload").last()[0]).val() !==""  ){
        $(".js-list").append('<li class="js-ss" style="display:none;"><span class="js-remove-photo icon-times"></span><input type="file" name="upload_media[]" accept="image/*"  class="custom-file-2 js-fileupload"/></a><img src="" alt="" style="display:none;"/></li>');
        $(".js-fileupload").last().click();
        var imageLoader = $(".js-fileupload").last()[0];
        imageLoader.addEventListener('change', handleImage, false);
// }
    }  
   });

  function handleImage(e) {
    var bevent = e;
    window.ll = bevent;
    var reader = new FileReader();
    reader.onload = function (event) {    
      $(bevent.target).parent().find("img").attr('src',event.target.result);//
      $(bevent.target).parent().find("img").show();
      $(bevent.target).parent().show();

    }
    reader.readAsDataURL(e.target.files[0]);
  };
// end of 

// document file js
$(document).on('change', "input#document_upload[type='file']", function(){
    var filename = this.value;
    var lastIndex = filename.lastIndexOf("\\");
    if (lastIndex >= 0) {
        filename = filename.substring(lastIndex + 1);
    }
    if($(".js-doc-file").length){
        $("li .js-doc-file .js-doc-filename").html(filename);
    }
    else{
        $("li.js-document").append('<div class="js-doc-file"><a href="#." class="icon-times js-remove-document"></a><span class="js-doc-filename">' + filename + '</span></div>');
    }
});

$("body").on("click",".js-remove-document",function(){
    $(this).parent().remove();
    control = $("input#document_upload");
    control.replaceWith( control = control.clone( true ) );
});
// end of

// script for Album
$("body").on("click",".js-get-album-detail",function(){
  var album_id= $(this).data("id");
  $.ajax({
      url: "/album/get_album_detail",
      type: "GET",
      data: {album_id:album_id},
      dataType: "script"
    });
});

$("body").on("click",".js-backto-album", function(){

});


})