 /*------------------------------------------------------------------
[ NURTUREY Script File]

Project:    NURTUREY
Version:    1
Starting Date: 11 March 2020
Last change Date:    --/--/----
Primary use:      Dedicated to Parents and Children
-------------------------------------------------------------------*/
/* ------------------------------------------------------------------

1. On scroll add navigation to Header



 ------------------------------------------------------------------ */


/* ------------------------------------------------------------------
1. On Scroll Add Fixed Class to Header
 ------------------------------------------------------------------ */


  $(function() {
    var header = $(".start-style");
    $(window).scroll(function() {    
      var scroll = $(window).scrollTop();
    
      if (scroll >= 10) {
        header.removeClass('start-style').addClass("scroll-on");
        $('.logo-scroll').show();
        $('.custom-logo').hide();
      } else {
        header.removeClass("scroll-on").addClass('start-style');
         $('.logo-scroll').hide();
        $('.custom-logo').show();
      }
    });
  });   


 jQuery(window).on('load', function() {
    if ('scrollRestoration' in history) {
      history.scrollRestoration = 'manual';
    }
    window.scrollTo(0,0);
  });

 
/* ------------------------------------------------------------------
    On Scroll Add Fixed Class to Header
 ------------------------------------------------------------------ */

