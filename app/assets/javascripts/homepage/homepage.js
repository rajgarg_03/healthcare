//= require jquery
//= require jquery.cookie
//= require cookies_eu

$(document).ready(function(){
  switch (getMobileOperatingSystem()){
    case 'I':
    case 'WP':
    case 'NF':
      addIphoneLink('.button.nav-text-link')
      break;
    case 'A':
      addAndroidLink('.button.nav-text-link')
      break;
    default:
      addIphoneLink('.button.nav-text-link')
      break;
  }
})

function addIphoneLink(className){
  $(className).attr('href', "https://itunes.apple.com/app/nurturey/id1023121839?ls=1&amp;mt=8")
}

function addAndroidLink(className){
  $(className).attr('href', "https://play.google.com/store/apps/details?id=com.nurturey.app")
}


function getMobileOperatingSystem() {
  var userAgent = navigator.userAgent || navigator.vendor || window.opera;

      // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
        return "WP";
    }

    if (/android/i.test(userAgent)) {
        return "A";
    }

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        return "I";
    }

    return "NF";
}

